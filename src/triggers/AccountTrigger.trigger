trigger AccountTrigger on Account (before insert,after update) {

//-- Instantiate the handler
    AccountTriggerHandler handler = AccountTriggerHandler.getInstance();
    
    //-- Before Insert
    if (Trigger.isInsert && Trigger.isBefore) {
        if(UtilityClass.isFirstRun()) {
            UtilityClass.setFirstRunFalse();
            handler.onBeforeInsert(Trigger.new);
        }
    }
    
    if (Trigger.isUpdate && Trigger.isAfter) {
        //system.assertEquals('condition', 'msg');
        set<id> setid = new set<id>();
        if(UtilityClass.isFirstRun()) {
            UtilityClass.setFirstRunFalse();
            handler.onAfterUpdate(Trigger.oldMap,Trigger.newMap);
            
        }
       

    }
    /*
    if (Trigger.isInsert && Trigger.isAfter) {
        //system.assertEquals('condition', 'msg');
        UtilityClass.setFirstRunTrue();
        if(UtilityClass.isFirstRun()) {
            system.debug('entered$$');
            UtilityClass.setFirstRunFalse();
            set<id> setid = new set<id>();
            for(Account acc:Trigger.new){
                setid.add(acc.id);
            }
            if(!(Test.isRunningTest())){
                AccountTriggerHandler.onAfterInsert(setid);
            }
        }
    } */

}