trigger SAPMaterialMaster on Material_Master_Sap__c (before insert) {

//-- Instantiate the handler
    TH_MaterialMaster handler = TH_MaterialMaster.getInstance();
 //-- Before Insert
    if (Trigger.isBefore) {
        if(UtilityClass.isFirstRun()) {
            UtilityClass.setFirstRunFalse();
            if(Trigger.isInsert){
                handler.onBeforeInsert(Trigger.new);
            }
            /*else if(Trigger.isUpdate){
                handler.onBeforeUpdate(Trigger.new);
            } */
        }
    }


}