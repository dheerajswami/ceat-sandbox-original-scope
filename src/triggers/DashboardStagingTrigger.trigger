trigger DashboardStagingTrigger on Dashboard_Staging__c (before Insert) {
    DashboardStagingTriggerHandler handler = new DashboardStagingTriggerHandler();
    list<Dashboard_Staging__c> listOfDashboardStaging = new list<Dashboard_Staging__c>();
    id rsmClaimRec = UtilityClass.getRecordTypeId('Dashboard_Staging__c','RSMClaim'); 
    if (Trigger.isInsert && Trigger.isBefore) {
    	for(Dashboard_Staging__c ds : Trigger.new){
    		if(ds.RecordTypeId != rsmClaimRec){
    			listOfDashboardStaging.add(ds);	
    		}
    	}
        if(UtilityClass.isFirstRun()) {
            UtilityClass.setFirstRunFalse();

            handler.onBeforeInsert(listOfDashboardStaging);
        }
    }
}