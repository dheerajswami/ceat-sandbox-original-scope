trigger PjpNormsTrigger on PJP_Norms_Main__c (before insert, before update, after delete) {
	if(Trigger.isBefore && Trigger.isInsert) {
		PjpNormsTriggerHandler.activateNorm(Trigger.new);
	}

	if(Trigger.isBefore && Trigger.isUpdate) {
		PjpNormsTriggerHandler.inActiveNorms(Trigger.new, Trigger.old);
	}

	if(Trigger.isAfter && Trigger.isDelete) {
		PjpNormsTriggerHandler.activateInactiveNorms(Trigger.old);
	}
}