trigger sendEmailOnNewComment on CaseComment (after insert) {
    if(trigger.isInsert){
        if(trigger.isAfter){
            
            CaseCommentHelper.sendEmailToAllLevelTO(trigger.new);
        }
    }
}