trigger ClaimTrigger on Claim__c (after insert, after update) {
    
    if(Trigger.isinsert) {        
        claimSmsTriggerHandler1.sendEmailToTlAndTld(trigger.New);
    }
    
    if(Trigger.isinsert || Trigger.isupdate) { 
        Set<ID> claimIds = new Set<ID>();
        for(Claim__c c : trigger.new){
            claimIds.add(c.id);
        }
        if(claimIds.size() > 0){
            ClaimTriggerHandler.sendClaimToSap(claimIds);
        }
    }
    
}