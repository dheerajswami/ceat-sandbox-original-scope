trigger VehicleServiceDataPassingCallOut on Vehicle_Service__c (after insert) {



     if(UtilityClass.getVehicleServiceRecursive()){
         UtilityClass.setVehicleServiceRecursive();
         
           map<id,CS_Vehicle__c> idVsCsVehicles;//=new map<id,CS_Vehicle__c>();
           
           set<id> csVehiclesId=new set<id>();
           for(Vehicle_Service__c vs: trigger.new){
             csVehiclesId.add(vs.Vehicle__c);
           }
           if(!csVehiclesId.isempty()){
             idVsCsVehicles=new map<id,CS_Vehicle__c>([select id,name,Vehicle_Registration_Number_Old__c,Make__c,
                Model__c,Last_Alignment__c,Last_Balancing_Change__c,CS_Customer__c from CS_Vehicle__c where id=:csVehiclesId]);
           csVehiclesId.clear();
           
           }
            
         
          map<id,CS_Customer__c> idVsCS_Customer;//=new map<id,CS_Customer__c>();
          set<id> CsCustomerIdSet=new set<id>();
          if(idVsCsVehicles!=null)
           for(id d:  idVsCsVehicles.keyset()){
           
             CsCustomerIdSet.add(idVsCsVehicles.get(d).CS_Customer__c);
           }  
          
          if(CsCustomerIdSet!=null){
             idVsCS_Customer=new map<id,CS_Customer__c>([select id,name,Mobile__c,
               Email__c,Next_Wheel_Alignment__c,Next_Balancing_Date__c,Ceat_Shoppe__c
              from CS_Customer__c where id=:CsCustomerIdSet]); 
          
          } 
         
         
           if(idVsCS_Customer!=null && idVsCsVehicles!=null){
           
             string finalJsonString='{"customersDetails":'+VehicleServiceHelp.RecordToJsonObjectMapper(trigger.new,idVsCsVehicles,idVsCS_Customer)+'}';
             system.debug('FINAL JSON STRING'+finalJsonString);
           
            VehicleServiceCallMaker.callIntiAlizer(finalJsonString);
            }   
     
     }
     






}