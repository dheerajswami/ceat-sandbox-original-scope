trigger TDS_Bulk_Uploader on Attachment(before insert, after insert) {
 set < string > attchids = new set < string > ();
    list<smagicinteract__smsMagic__c> smsmagclst = new  list<smagicinteract__smsMagic__c>();
 list < TDS__c > TdsList = new list < TDS__c > ();
 list < Contact > contactlist = new List < Contact > ();
 map < String, Id > TdsMap = new map < String, Id > ();
 String pan_num;
 set<id> portalUsername = new set<id>();
 set < String > tdsnames = new set < String > ();
 map < String, Attachment > attachmap = new map < String, Attachment > ();
 for (Attachment atch: trigger.new) {
  system.debug('in for loop attachment' + atch.name);
  if (atch.name.length() >= 21) {
   pan_num = atch.name.substring(0, 10);
   system.debug('pan_num' + pan_num);
   if (pan_num != null)
    attchids.add(pan_num);
   attachmap.put(pan_num, atch);
   tdsnames.add(atch.name.substring(14, 21));
  }
 }
 List < account > acclist = [select id, name, Pan_Number__c, (select id, name,phone,email,account.name,accountid from contacts) from account where Pan_Number__c in : attchids];
 system.debug('--acclist--' + acclist);
 List < TDS__c > eventList = [select id, name, Pan_Number__c from TDS__c where name in : tdsnames];
 Map < String, Account > accMap = new Map < String, Account > ();
 for (Account ac: acclist) {
  Attachment attch = attachmap.get(ac.Pan_Number__c);
  if (eventList.isempty()) {
   TDS__c expense = new TDS__c(Name = attch.name.substring(14, 21), TDS_Account__c = ac.Id, Pan_Number__c = ac.Pan_Number__c);
   TdsList.add(expense);
  }
  accMap.put(ac.Pan_Number__c, ac);
 }

 if (TdsList.isEmpty()) {
  TdsList.addAll(eventList);
 } else {
  insert TdsList;
 }
 for (TDS__c Tdsref: TdsList) {
  TdsMap.put(Tdsref.Pan_Number__c + '_' + Tdsref.name, Tdsref.Id);

 }
 if (Trigger.isBefore) {
  for (Attachment atchment: trigger.new) {
   if (atchment.name.length() >= 10)
    pan_num = atchment.name.substring(0, 10);
   if (accMap.get(pan_num) != null) {
    atchment.parentid = TdsMap.get(pan_num + '_' + atchment.name.substring(14, 21));
    system.debug('-atchment---'+atchment);

   }
  }
 }
 if (Trigger.isAfter) {
      list < Contact > EmailUserlsit= new list < Contact >();
  for (Attachment atchment: trigger.new) {
   if (atchment.name.length() >= 10)
    pan_num = atchment.name.substring(0, 10);
    
   if (accMap.get(pan_num) != null) {
    EmailUserlsit.addAll(accMap.get(pan_num).Contacts);
   }
    for(Contact con: EmailUserlsit)
    {
        portalUsername.add(con.id); 
        
    }
    }
    list<User> userlist = [select id,name,contactId,UserName from User where ContactId in:portalUsername];
    Map<Id,User> portaluser = new Map<Id,User>();
    for(User us: userlist)
    {
        portaluser.put(Us.ContactId,us);    
        
    }       
    
     String username;
    
     for (Contact con: EmailUserlsit) {
         if(con.phone != null)
         {
         if(portaluser.get(con.Id)!=null)
         username = portaluser.get(con.Id).username;
         else
         username = 'contact does not have portal user';
         system.debug('&&&&con.name---'+con.name);
        smagicinteract__smsMagic__c smsObj = new smagicinteract__smsMagic__c();
        smsObj.smagicinteract__PhoneNumber__c = con.phone;
        //smsObj.smagicinteract__SMSText__c = 'Dear '+con.name+', Attached is your latest TDS for ready reference. Your can also login to CEAT portal & download online. Portal Link= https://ceat.my.salesforce.com/secur/login_portal.jsp?orgId=00D900000009iz5&portalId=060900000005Dk7  and Your User Id is ='+username;         
        smsObj.smagicinteract__SMSText__c = 'Dear '+con.name+', '+label.Tds_SMS;
        smsObj.smagicinteract__senderId__c = 'smsMagic';
        smsObj.smagicinteract__Name__c = con.name;
        smsObj.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
         smsmagclst.add(smsObj);
         }
      con.Tds_Check__c = true;
      con.Portal_User_Name__c = username;
      contactlist.add(con);
     }
   
  
     if(!smsmagclst.isempty())
         insert smsmagclst;
  if (!contactlist.isEmpty())
   update contactlist;
 }
}