trigger PopulateShipToPartyCode on Proforma_Invoice__c (after insert, after update) {

	//PopulateShipToPartyCodeTriggerHandler handler = PopulateShipToPartyCodeTriggerHandler.getInstance();

	//-- After Insert
	if(Trigger.isInsert && Trigger.isAfter) {
		if(PItriggerContextUtility.isAIFirstRun()) {
            PItriggerContextUtility.setAIFirstRunFalse();
            PopulateShipToPartyCodeTriggerHandler.populateShipToPartyCode(Trigger.newMap.keySet());
        }
		
	}

	//-- After Update
	if(Trigger.isUpdate && Trigger.isAfter) {
		Integer counter = 0;
		if(PItriggerContextUtility.isAUFirstRun()) {
			counter ++;
			System.debug('==#1 '+counter);
            PItriggerContextUtility.setAUFirstRunFalse();
            if(!System.isFuture()){
            	PopulateShipToPartyCodeTriggerHandler.populateShipToPartyCode(Trigger.newMap.keySet());
            }
        }	
    }
}