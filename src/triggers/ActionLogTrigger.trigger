/*******************************************************************************************************
    Author  : Sneha Agrawal
    Date    : 12/1/2014
    Purpose : Trigger on Action Log to fetch SAP code, Segment, Territory from Customer
              To notify re-assigned user that action log has been escalated to him/her

********************************************************************************************************/

trigger ActionLogTrigger on Task (before insert,after insert,before update,after update) {

    if(Trigger.isInsert){
    
        if(Trigger.isBefore)
            ActionLogTriggerHandler.assignDL(Trigger.new);
        
        if(Trigger.isAfter)
          ActionLogTriggerHandler.notifyUserInsert(Trigger.new);
    }
    if(Trigger.isUpdate){
        
       if(Trigger.isBefore)
           ActionLogTriggerHandler.assignDL(Trigger.new);
       
       if(Trigger.isAfter)
          ActionLogTriggerHandler.notifyUser(Trigger.new,Trigger.old);
    }

}