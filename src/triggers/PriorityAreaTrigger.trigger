trigger PriorityAreaTrigger on Priority_Area__c (after insert, after update) {
    
   list<String> roleNames						= new list<String>();
   list<Territory2> userTerritory 				= new list<Territory2>();
   list<String> terrIdList						= new list<String>();
   list<UserTerritory2Association> zoneForRM1   = new list<UserTerritory2Association>();
   
   map<String,String> userTerritoryMap 			= new map<String,String>();
   map<String,String> userTerritoryRbmMap 		= new map<String,String>();
   map<String,String> userdetail 				= new map<String,String>();
   
   roleNames.add('RM');
   roleNames.add('RBM');
   roleNames.add('ZBM');
   
   List<User> uList=[Select Id,Name,UserRole.DeveloperName from User where UserRole.DeveloperName in : roleNames];
			//User Id and User Role 
	  for(User u:uList){
	    userdetail.put(u.Id,u.UserRole.DeveloperName);
	 }	
	        
	zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 in : roleNames];//UserId in : userdetail.keySet() and 
	for(UserTerritory2Association ut: zoneForRM1){
	      terrIdList.add(ut.Territory2Id);
	 }    
	userTerritory = [select name,id, DeveloperName from Territory2 where id IN :terrIdList];	
	            
	for(UserTerritory2Association ua:zoneForRM1){
	    for(Territory2 t:userTerritory){
	           if(ua.Territory2Id==t.Id && ua.RoleInTerritory2!='RBM'){
	                userTerritoryMap.put(t.DeveloperName,ua.UserId);	                
	                break;
	           }if(ua.RoleInTerritory2=='RBM'){
	              userTerritoryRbmMap.put(t.DeveloperName,ua.UserId);
	        	}
	    }
	}
	
	system.debug('userTerritoryMap----------'+userTerritoryMap);
   if(Trigger.isInsert){
    	List<Priority_Area__Share> sharesToPA = new List<Priority_Area__Share>();
    	
    	for(Priority_Area__c pa:Trigger.new){
    		
    		Priority_Area__Share pas = new Priority_Area__Share();
    		Priority_Area__Share pasreg = new Priority_Area__Share();
    		Priority_Area__Share pasrbm = new Priority_Area__Share();
    		//Sharing record with ZBM
    		if(pa.Zone1__c != null && userTerritoryMap.get(pa.Zone1__c)!=null){    			
    			pas.AccessLevel = 'Edit';
		        pas.ParentId = pa.Id;
		        pas.UserOrGroupId =  userTerritoryMap.get(pa.Zone1__c);
    			sharesToPA.add(pas);
    		}
    		//Sharing record with RM
    		if(pa.Region1__c != null && userTerritoryMap.get(pa.Region1__c)!=null){    			
    			pasreg.AccessLevel = 'Edit';
		        pasreg.ParentId = pa.Id;
		        pasreg.UserOrGroupId =  userTerritoryMap.get(pa.Region1__c);
    			sharesToPA.add(pasreg);    			
    		}
    		//Sharing record with RBM
    		if(pa.Region1__c != null && userTerritoryRbmMap.get(pa.Region1__c)!=null){    			
    			pasrbm.AccessLevel = 'Edit';
		        pasrbm.ParentId = pa.Id;
		        pasrbm.UserOrGroupId =  userTerritoryRbmMap.get(pa.Region1__c);
    			sharesToPA.add(pasrbm);    			
    		}  
    		system.debug(sharesToPA);  		
    	}
      try{	
	      if (!sharesToPA.isEmpty())	      
	      insert sharesToPA;
      }catch(Exception e){
      	
      }
   }
    if(Trigger.isUpdate){
    	list<Priority_Area__Share> sharesToPA = new list<Priority_Area__Share>();
    	list<ID> shareIdsToDelete = new list<ID>();
    	
    	for(Priority_Area__c pa:Trigger.new){
    		
    		Priority_Area__Share pas	= new Priority_Area__Share();
    		Priority_Area__Share pasreg = new Priority_Area__Share();
    		Priority_Area__Share pasrbm = new Priority_Area__Share();
    		
    		//Sharing record with ZBM
    		if(Trigger.oldMap.get(pa.id).Zone1__c != pa.Zone1__c || Trigger.oldMap.get(pa.id).Region1__c != pa.Region1__c ){
    			shareIdsToDelete.add(pa.id);    		
	    		if(pa.Zone1__c != null && userTerritoryMap.get(pa.Zone1__c)!=null){   	    				
	    			pas.AccessLevel = 'Edit';
			        pas.ParentId = pa.Id;
			        pas.UserOrGroupId =  userTerritoryMap.get(pa.Zone1__c);			       
	    			sharesToPA.add(pas);
	    		}
	    		if(pa.Region1__c != null && userTerritoryMap.get(pa.Region1__c)!=null){  
	    						
	    			pasreg.AccessLevel = 'Edit';
			        pasreg.ParentId = pa.Id;
			        pasreg.UserOrGroupId =  userTerritoryMap.get(pa.Region1__c);
	    			sharesToPA.add(pasreg);    			
	    		}
	    		//Sharing record with RBM
	    		if(pa.Region1__c != null && userTerritoryRbmMap.get(pa.Region1__c)!=null){  	    					
	    			pasrbm.AccessLevel = 'Edit';
			        pasrbm.ParentId = pa.Id;
			        pasrbm.UserOrGroupId =  userTerritoryRbmMap.get(pa.Region1__c);
	    			sharesToPA.add(pasrbm);    			
	    		} 
	    		system.debug(sharesToPA);  
    	  }   		
    	}
      try{
      	  
	      if (!shareIdsToDelete.isEmpty()){
	      	 List<Priority_Area__Share> lplist = [select id from Priority_Area__Share where ParentId IN :shareIdsToDelete AND RowCause = 'Manual'];	
      	     system.debug('lplist========='+lplist);
      	     delete lplist;
	      }
	      system.debug('sharesToPA'+sharesToPA);
	      if(!sharesToPA.isEmpty())
	      insert sharesToPA;
      }catch(Exception e){
         system.debug('Exception--'+e);
      }
   }

}