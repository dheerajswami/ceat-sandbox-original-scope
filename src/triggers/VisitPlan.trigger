trigger VisitPlan on Visits_Plan__c (before insert,before update) {

    	//-- Instantiate the handler
	TH_VisitPlanTriggerHandler handler = TH_VisitPlanTriggerHandler.getInstance();
	
	//-- Before Insert
	if (Trigger.isInsert && Trigger.isBefore) {
		handler.onBeforeInsert(Trigger.new);
	} else

	//-- Before Update
	if (Trigger.isUpdate && Trigger.isBefore) {
		handler.onBeforeUpdate(Trigger.new, Trigger.old,Trigger.newMap,Trigger.oldMap);
	}
    
}