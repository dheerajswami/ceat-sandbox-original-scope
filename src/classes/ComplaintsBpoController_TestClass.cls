/* ------ Author SUPRIYA ------- */
/* ------ Test Class : ComplaintsBpoController_TestClass ------*/
/* ------ Controllers: Precomplaint,ComplaintBpoController ----*/

@isTest 
private class ComplaintsBpoController_TestClass{

    
	//Test Method to Cover ComplaintBpoController
    static testmethod void TestMethodAsAdmin() {


    Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
    insert acc;
    acc.Sales_District_Text__c = 'B0164';
    acc.Sales_Group_Text__c ='JAB';
    update acc;
    Contact con = CEAT_InitializeTestData.createContact('mathur',acc.id);
    con.Email = 'kishlay.mathur@extentor.com';
    insert con;
    State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
    StateMaster.RO__c =  'JAB';
    insert StateMaster ;
    Material_Master_Sap__c  materialMaster = CEAT_InitializeTestData.createMaterialMaster('102','CeatZoom','Sample','MatGrpDe','000000000000101010');
    insert materialMaster;
    materialMaster.Mat_Grp2_Desc__c = 'Sample';
    materialMaster.Pattern_Brand__c = 'Pattern';
    update materialMaster;
	Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
    insert caseReplacement;
    caseReplacement.Nature__c = 'External';
    caseReplacement.Material__c =  '101010';
    update caseReplacement;
    
    Casecomment csecomment = new casecomment(CommentBody = 'Test',IsPublished = TRUE,ParentId =caseReplacement.Id);
    insert csecomment;
    Plant__c plt = new Plant__c(Name = 'KANC');
    insert plt;
    Defect_Type__c defectobj = new Defect_Type__c(Name = 'Test');
    insert defectobj;
    Disposition__c dispositionObj = new Disposition__c(Name ='disp');
    insert dispositionObj;

    ApexPages.StandardController sc = new ApexPages.standardController(caseReplacement);

    PageReference pgComp = Page.ComplaintBPOPage_V2;
    Test.setCurrentPage(pgComp);
	ComplaintBPOController complaintctrl = new ComplaintBPOController(sc);
    List<SelectOption> districtsOpt = ComplaintBPOController.getDistricts('MADHYA PRADESH');
    List<SelectOption> townOpt = ComplaintBPOController.getTowns('Balaghat');
    Set<SelectOption> tyrOpt = ComplaintBPOController.tyreSizeList('Sample');
    Set<SelectOption> tyrsizeOpt = ComplaintBPOController.tyrPatternList('102');
    List<State_Master__c> salesgrp = ComplaintBPOController.getSalesGroup1('MADHYA PRADESH', 'Balaghat');
    String cstlNameDisplay = ComplaintBPOController.getCSTLname('B0164');
    String complaintCustomerReg = ComplaintBPOController.saveComplaintCustomer('Test', '987654321','MADHYA PRADESH', 'Balaghat', 'Balaghat', 'Test@Ceat.com', '5th');
    List<Material_Master_Sap__c> matNumList = ComplaintBPOController.searchmaterialNum('Pattern', 'Replacements' ,'101' );
    List<Account> accAutocomplete = ComplaintBPOController.searchAccounts('Dealer','JAB', 'Test');
    List<Account> accAll = ComplaintBPOController.searchAccountsALL('Test');
    List<SelectOption> countrtyList = ComplaintBPOController.CountrysNameList('0MI9000000000XUGAY');
    List<Territory2> countryval = ComplaintBPOController.getSalesoff1('0MI9000000000aE');

    String saveCase = ComplaintBPOController.UpdatingCase(caseReplacement.Id, '12/12/2015', '10/12/2015', '15/12/2015', 'KANC', '10','12', '1212134561','2015', caseReplacement);
    String saveSecondEditCase = ComplaintBPOController.UpdateSecondEditCase(caseReplacement.Id, '30/12/2015', 'Not Available', 'In Progress','Defect', '15/12/2015', caseReplacement);
    //String savenewCase = ComplaintBPOController.UpdatingCase(null, '12/12/2015', '10/12/2015', '15/12/2015', 'KANC', '10','12', '1212134561','2015', caseReplacement);
    List<Account> oetype = ComplaintBPOController.searchAccountsOEorSpec('OE','test');
    }


    static testmethod void TestMethodAsBPO() {

    Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
    insert acc;
    acc.Sales_District_Text__c = 'B0164';
    acc.Sales_Group_Text__c ='JAB';
    update acc;
    Contact con = CEAT_InitializeTestData.createContact('mathur',acc.id);
    con.Email = 'kishlay.mathur@extentor.com';
    insert con;
    State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
    StateMaster.RO__c =  'JAB';
    insert StateMaster ;
    Material_Master_Sap__c  materialMaster = CEAT_InitializeTestData.createMaterialMaster('102','CeatZoom','Sample','MatGrpDe','000000000000101010');
    insert materialMaster;
    materialMaster.Mat_Grp2_Desc__c = 'Sample';
    materialMaster.Pattern_Brand__c = 'Pattern';
    update materialMaster;
    Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
    insert caseReplacement;
    caseReplacement.Nature__c = 'External';
    caseReplacement.Material__c =  '101010';
    caseReplacement.Customer_Phone__c = '98764321';
    caseReplacement.Comment__c = 'Testing';
    caseReplacement.Cluster__c='0MI9000000000XUGAY';
    update caseReplacement;
    
    Casecomment csecomment = new casecomment(CommentBody = 'Test',IsPublished = TRUE,ParentId =caseReplacement.Id);
    insert csecomment;
    Plant__c plt = new Plant__c(Name = 'KANC');
    insert plt;
    Defect_Type__c defectobj = new Defect_Type__c(Name = 'Test');
    insert defectobj;
    Disposition__c dispositionObj = new Disposition__c(Name ='disp');
    insert dispositionObj;
    User bpoUsr = [Select id,Name From User Where IsActive=true AND ProfileId = '00eO0000000I44kIAC' Limit 1 ];
    ApexPages.StandardController sc = new ApexPages.standardController(caseReplacement);

    PageReference pgComp = Page.ComplaintBPOPage_V2;
    Test.setCurrentPage(pgComp);
        system.runAs(bpoUsr){ 
        String saveCase = ComplaintBPOController.UpdatingCase(caseReplacement.Id, '12/12/2015', '10/12/2015', '15/12/2015', 'KANC', '10','12', '1212134561','2015', caseReplacement);
        String saveSecondEditCase = ComplaintBPOController.UpdateSecondEditCase(caseReplacement.Id, '30/12/2015', 'Not Available', 'In Progress','Defect', '15/12/2015', caseReplacement);
    
        }
        List<Account> searchSpecialty = ComplaintBPOController.searchAccounts('Specialty', 'JAB', 'Test');

    }


    /*static testmethod void TestMethodforNewCase() {

        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        acc.Sales_District_Text__c = 'B0164';
        acc.Sales_Group_Text__c ='JAB';
        update acc;
        Contact con = CEAT_InitializeTestData.createContact('mathur',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Material_Master_Sap__c  materialMaster = CEAT_InitializeTestData.createMaterialMaster('102','CeatZoom','Sample','MatGrpDe','000000000000101010');
        insert materialMaster;
        materialMaster.Mat_Grp2_Desc__c = 'Sample';
        materialMaster.Pattern_Brand__c = 'Pattern';
        update materialMaster;

        Plant__c plt = new Plant__c(Name = 'KANC');
        insert plt;
        Defect_Type__c defectobj = new Defect_Type__c(Name = 'Test');
        insert defectobj;
        Disposition__c dispositionObj = new Disposition__c(Name ='disp');
        insert dispositionObj;
        User bpoUsr = [Select id,Name From User Where IsActive=true AND ProfileId = '00eO0000000I44kIAC' Limit 1 ];
        Case caseObject = new Case();

            caseObject.Nature__c = 'External';
            caseObject.Complaint_Type__c = 'Sales';
            caseObject.Business_Category__c = 'Replacements';
            caseObject.State__c = 'MADHYA PRADESH';
            caseObject.Town__c = 'Balaghat';
            caseObject.District__c = 'Balaghat';  
            
            caseObject.AccountId = acc.id;
            caseObject.Customer_Email__c = 's@s.com';
            caseObject.Customer_Phone__c = '987411';
            
            caseObject.Keyword__c = 'Duplicate tyres/ tube' ;      
            caseObject.Comments__c = 'new comment';
            caseObject.Comment__c = 'new comment';
            caseObject.Cluster__c='0MI9000000000XUGAY';
            caseObject.Status = 'Open';
            system.runAs(bpoUsr){    
        String savenewCase = ComplaintBPOController.UpdatingCase(String.valueof(''), '12/12/2015', '10/12/2015', '15/12/2015', 'KANC', '10','12', '1212134561','2015', caseObject);
        Casecomment csecomment = new casecomment(CommentBody = 'Test',IsPublished = TRUE,ParentId =caseObject.Id);
        insert csecomment;
    }
    }*/

}