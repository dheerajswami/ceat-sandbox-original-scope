global class SP_BatchForSpecialty_OE_BU implements Database.Batchable<sObject> {    
    
    String exportDealer             = System.Label.Export_Dealer; 
    String exportForcast        = System.Label.Export_Forcast_BU;
    String recTypeExportForecast    = System.Label.Export_Forcast;
    String speSRMROle  = system.label.SRM_SpecialtyUrole;
    String speAMROle  = system.label.AM_SpecialtyURole;
    String speOEDealerRec = system.label.Specialty_OE;
    String speOEDealerBU = system.label.Specialty_OE_BU;
    String speOEForcastRec = system.label.Specialty_OE_Forcast;
    Static String SPECIALTY='Specialty';
    Static String REPLECMENT='Replacement';
    String catZoneSpe = '';
    Map<Id,Territory2> mapOFIdAndterritory = new Map<Id,Territory2>();
    Map<String,Territory2> mapOfNameAndTerritory = new Map<String,Territory2>();
    map<String,Sales_Planning__c> mapOfZoneForcast                = new map<String,Sales_Planning__c>();
    map<String,Sales_Planning__c> mapOfDealerAndSales             = new map<String,Sales_Planning__c>();
    Date d = system.today();
    String month = String.valueOf(d.month());
    String year = String.valueOf(d.Year());
    Set<String> catExpSet = new Set<String>();
    global SP_BatchForSpecialty_OE_BU (){
        mapOFIdAndterritory = new Map<Id,Territory2>([select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId from Territory2]);
        mapOfNameAndTerritory = new Map<String,Territory2>();
        
        for(Territory2 trr : mapOFIdAndterritory.values()){
            mapOfNameAndTerritory.put(trr.DeveloperName,trr);
        }
        system.debug(mapOfNameAndTerritory+'mapOfNameAndTerritory');
        system.debug(mapOfNameAndTerritory.size()+'mapOfNameAndTerritory');
        system.debug(mapOFIdAndterritory+'mapOFIdAndterritory');
        system.debug(mapOFIdAndterritory.size()+'mapOFIdAndterritory');
        mapOfZoneForcast                = new map<String,Sales_Planning__c>();
        mapOfDealerAndSales             = new map<String,Sales_Planning__c>();
    } 
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query;
        
        //getting all staging records with BU specialty and Forcast
       
        //Id recordTypeStagingId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeStagingLabel Limit 1].Id;
        query = 'SELECT Id,OwnerId,SKU_Name__c,ASP__c,Budget__c,Category__c,SYS_Used_IN_BatchSpecialtyOE__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,'
        +'Year__c,SPExternalIDTL__c,BU__c,NBP__c,Value__c,Value_L3M__c,Region_code__c,Reg_code__c,Value_LYCM__c,SKU__c,Zone__c FROM Sales_Planing_Staging__c WHERE BU__c =\''+speOEDealerBU+'\'   AND Processed__c = false AND Year__c =\''+year+'\' AND Month__c =\''+month+'\'' ;
        System.debug(Database.getQueryLocator(query)+'@@@query@@@@');
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        Id specailtyOEForcastId= [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEForcastRec Limit 1].Id;
        Id specailtyOEDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEDealerRec Limit 1].Id;
        list<UserTerritory2Association> terrUserListSpe = new list<UserTerritory2Association>();
        List<Sales_Planning__c> insertSaleplanningForcast = new list<Sales_Planning__c>();
        List<Sales_Planning__c> listOfSpeOEForcast = new list<Sales_Planning__c>();
        List<Sales_Planning__c> insertSaleplanningDealer = new list<Sales_Planning__c>();
        list<Territory2> listOFcluster =  new List<Territory2>();
        list<Territory2> countryUnderCluster =  new List<Territory2>();
        list<Sales_Planing_Staging__c> updateStaging = new list<Sales_Planing_Staging__c>();
        list<Territory2Type> terriType = new list<Territory2Type>();
        Map<String,Integer> mapOfZoneTotalLYCM                               = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalTarget                              = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalBudget                               = new map<String,Integer>(); 
        Map<String,Decimal> mapOfZoneTotalValL3m                              = new map<String,Decimal>(); 
        Map<String,Decimal> mapOfZoneTotalValLYCM                               = new map<String,Decimal>(); 
        Map<String,Integer> mapOfClusterTarget                  = new map<String,Integer>();
        Map<String,id> mapOfExportForcastID                 = new map<String,id>();
        Map<id,Set<String>> mapOfClusterIDAndCountry                 = new map<id,Set<String>>();
        Map<String,Set<String>> mapOfClusterCodeAndCountryCode                 = new map<String,Set<String>>();
        Map<String,id> mapOFforcastAndID    = new map<String,id>();
        Map<id,String> mapOfClusterIDAndCode                = new Map<id,String>();
        Map<String,String> mapOfSKUandSKUName = new map<String,String>();
        map<String,id> mapofUserAndTerr = new map<String,id>();
        Sales_Planning__c exportdea ;
        Sales_Planning__c exportforcast;
        Sales_Planning__c  spSpecialtyForcastSKUZoneWiseMap ;
        Sales_Planning__c  spSpecialtyZoneSKUWise ;
        Account dealer;
        String externalIDExport = '';
        set<String> countrySet;
        String keyspe = '';
        Integer target= 0;
        catZoneSpe = '';
        id userId;
        set<String> setkeySkuReg = new set<String>();
        //set<String> setkeySkuRegDealerNum = new set<String>();
        set<String> setexternalid = new set<String>();
        set<id> terrTypeID = new set<Id>();
        set<String> skusetKey =  new set<String>();
        Set<String> setkeySKUZone = new Set<String>();
        set<String> contrySetForCluster; 
        string keySKURegSpe = '';
        String keySKUZone = '';
        String setkeySkuRegDealerNum = '';
        String catZoneSpe = '';
        String setkeySkuRegDealer = '';
        String amRole = system.Label.AM_SpecialtyURole;
        String rmRoleSpecilaty  = system.label.Specialty_RM_Role;
        String srmRoleSpecilaty  = system.label.SRM_SpecialtyUrole;
         terrUserListSpe = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 =:amRole OR RoleInTerritory2 =:rmRoleSpecilaty OR RoleInTerritory2 =:srmRoleSpecilaty];
         if(terrUserListSpe.size()>0){
          for(UserTerritory2Association ut1: terrUserListSpe){
           mapofUserAndTerr.put(ut1.Territory2.name,ut1.UserId);                  
          }
         }
         
         
         for(sObject temp : scope){
             
                  Sales_Planing_Staging__c sps =(Sales_Planing_Staging__c)temp;
                  if( sps.Reg_code__c != null){
                      keySKURegSpe = sps.SKU__c+sps.Reg_code__c;
                      setkeySkuRegDealerNum = sps.Dealer_CustNumber__c+sps.SKU__c+sps.Reg_code__c;
                      setkeySkuReg.add(sps.SKU__c+':'+sps.Reg_code__c);
                      system.debug(sps.Reg_code__c+'sps.Reg_code__c');
                      system.debug(getTerritoryDevName(sps.Reg_code__c,SPECIALTY)+'methode return');
                      system.debug(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY))+'after that');
                      system.debug(mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name+'after that Name');
                      catZoneSpe = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
                      keySKUZone = sps.SKU__c+catZoneSpe;
                      setkeySKUZone.add(keySKUZone);
                      skusetKey.add(setkeySkuRegDealerNum);
                  }
                  
             
         }
        system.debug(setkeySkuRegDealerNum +'setkeySkuRegDealerNum');   
        system.debug(setkeySkuReg+'setkeySkuReg');   
        for(String s : setkeySkuReg){
            mapOfZoneTotalTarget.put(s,0);
        }
        
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch2_Specailty_OE__c,SYS_Used_IN_Batch_Specailty_OE__c,SYS_Used_IN_Batch__c,SYS_Used_IN_Batch2__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch2_Specailty_OE__c IN : setkeySKUZone AND RecordTypeId =: specailtyOEForcastId AND Month__c =:month  AND Year__c =: year ] ){
        mapOfZoneForcast.put(salesPlan.SYS_Used_IN_Batch2_Specailty_OE__c,salesPlan ); 
        }
        
       
        for(sObject te : scope){
        Sales_Planing_Staging__c temp =(Sales_Planing_Staging__c)te;   
        if(temp.Reg_code__c != null){
            catZoneSpe = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(temp.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
            keyspe = temp.SKU__c+catZoneSpe;
            
            system.debug(keyspe+'keyspe');
        } 
        
        
        if(mapOfZoneForcast.containsKey(keyspe)){
                system.debug('if me');
                spSpecialtyForcastSKUZoneWiseMap = new Sales_Planning__c();
                spSpecialtyForcastSKUZoneWiseMap = mapOfZoneForcast.get(keyspe);
                spSpecialtyForcastSKUZoneWiseMap.Target_Quantity__c += (temp.Target_Quantity__c>0?temp.Target_Quantity__c:0);
                spSpecialtyForcastSKUZoneWiseMap.NBP__c += (temp.NBP__c>0?temp.NBP__c:0);
                
                
                if(spSpecialtyForcastSKUZoneWiseMap.Value__c == null) spSpecialtyForcastSKUZoneWiseMap.Value__c =0;
                spSpecialtyForcastSKUZoneWiseMap.Value__c += ((temp.NBP__c != null && temp.Target_Quantity__c != null) ? (temp.NBP__c * temp.Target_Quantity__c) :0);
                //spSpecialtyForcastSKUZoneWiseMap.Value__c += (temp.NBP__c * temp.Target_Quantity__c );
                
                mapOfZoneForcast.put(keyspe,spSpecialtyForcastSKUZoneWiseMap);
        }else{
                system.debug('enter else');
                double val = 0;
                if(temp.NBP__c != null && temp.Target_Quantity__c != null){
                    val = (temp.NBP__c * temp.Target_Quantity__c);
                }
                
                if(mapofUserAndTerr.get(catZoneSpe)!=null){
                             userId = mapofUserAndTerr.get(catZoneSpe);
                            }else{
                             userId = UserInfo.getUserId();
                }
                spSpecialtyZoneSKUWise = new Sales_Planning__c(
                ownerID = userId,
                SKU__c= temp.SKU__c,
                SKU_Name__c = temp.SKU_Name__c,
                Month__c = temp.Month__c,Year__c = temp.Year__c,Zone__c = catZoneSpe,
                Target_Quantity__c = temp.Target_Quantity__c,
                BU__c = 'Specialty OE Forcast',
                Value__c = val,
                RecordTypeId = specailtyOEForcastId,
                NBP__c = temp.NBP__c,
                Region_code__c = temp.Reg_code__c,
                SPExternalIDTL__c = temp.SKU__c+catZoneSpe+temp.Month__c+temp.Year__c
                );
                mapOfZoneForcast.put(keyspe,spSpecialtyZoneSKUWise);
        }
        
        }
        /*
        for(Sales_Planing_Staging__c salesPlan : [SELECT Id,SKU_Name__c,OwnerId,ASP__c,Budget__c,Category__c,SYS_Used_IN_BatchSpecialtyOE__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,
        Year__c,SPExternalIDTL__c,BU__c,Value__c,Value_L3M__c,Value_LYCM__c,SKU__c,Zone__c FROM Sales_Planing_Staging__c WHERE  BU__c =: speOEDealerBU AND Year__c =:year  AND Month__c =: month AND SYS_Used_IN_BatchSpecialtyOE__c IN:setkeySkuZone] ) {
                if(mapOfZoneTotalTarget.containsKey(salesPlan.SYS_Used_IN_BatchSpecialtyOE__c)){
                    system.debug('inside first');
                    target = mapOfZoneTotalTarget.get(salesPlan.SYS_Used_IN_BatchSpecialtyOE__c);
                    if(salesPlan.Target_Quantity__c == null) salesPlan.Target_Quantity__c =0;
                    target += Integer.valueOF(salesPlan.Target_Quantity__c);
                    mapOfZoneTotalTarget.put(salesPlan.SYS_Used_IN_BatchSpecialtyOE__c,target);
                    mapOfSKUandSKUName.put(salesPlan.SKU__c,salesPlan.SKU_Name__c);           
                }
        }
            system.debug(mapOfZoneTotalTarget+'mapOfZoneTotalTarget');
        if(mapOfZoneTotalTarget.size() > 0){
            for(String key : mapOfZoneTotalTarget.keyset()){
                String [] st = key.split(':');
                if(st.size() == 2){
                    Sales_Planning__c speOEForcast = new Sales_Planning__c(SKU_Name__c=mapOfSKUandSKUName.get(st[0]),SPExternalIDTL__c= st[0]+st[1]+month+year,SKU__c = st[0],Zone__c = st[1],Year__c = year , Month__c = month,Target_Quantity__c = mapOfZoneTotalTarget.get(key),RecordTypeID = specailtyOEForcastId );
                    insertSaleplanningForcast.add(speOEForcast);
                }
            }   
        }*/
        if(mapOfZoneForcast.size() > 0){
              insertSaleplanningForcast = new list<Sales_Planning__c>();
              for(Sales_Planning__c temp : mapOfZoneForcast.values()){
                insertSaleplanningForcast.add(temp);
              }
              //upsert insertSaleplanningForcast SPExternalIDTL__c;
        }
        system.debug(insertSaleplanningForcast+'insertSaleplanningForcast');
        if(insertSaleplanningForcast.size() > 0){
            try{
                database.upsert (insertSaleplanningForcast ,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
            catch(Exception e){
                  System.debug(e.getMessage());
            }
            //upsert insertSaleplanningForcast SPExternalIDTL__c;
        } 
        for(Sales_Planning__c  sp : insertSaleplanningForcast){
            setexternalid.add(sp.SPExternalIDTL__c);
        }
        system.debug(setexternalid+'setexternalid');
        listOfSpeOEForcast = [select id,SPExternalIDTL__c,Zone__c ,SKU__c,Year__c ,Month__c,Target_Quantity__c from Sales_Planning__c Where SPExternalIDTL__c IN: setexternalid];
        if(listOfSpeOEForcast.size() > 0){
            for(Sales_Planning__c  temp : listOfSpeOEForcast ){
                mapOFforcastAndID.put(temp.SKU__c+temp.Zone__c , temp.id);
            }
            
        }
        for(sObject temp : scope){
            catZoneSpe = '';
            Sales_Planing_Staging__c sps =(Sales_Planing_Staging__c)temp;
            sps.Processed__c = true;
            updateStaging.add(sps);
            double valdealer = 0.0;
            if(sps.NBP__c != null && sps.Target_Quantity__c != null){
                    valdealer = (sps.NBP__c * sps.Target_Quantity__c);
            }
            if( sps.Reg_code__c != null){
                    catZoneSpe = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
            }
            if(mapofUserAndTerr.get(catZoneSpe)!=null){
                             userId = mapofUserAndTerr.get(catZoneSpe);
                            }else{
                             userId = UserInfo.getUserId();
            }
            dealer = new account(KUNNR__c=sps.Dealer_CustNumber__c);
            Sales_Planning__c speOEDealer = new Sales_Planning__c(OwnerID = userId, value__c = valdealer ,SKU_Name__c = sps.SKU_Name__c,NBP__c = sps.NBP__c,BU__c = 'Specialty OE',L3M__c=sps.L3M__c,Region_code__c = sps.Reg_code__c ,LYCM__c=sps.LYCM__c,Value_L3M__c=sps.Value_L3M__c,Value_LYCM__c=sps.Value_LYCM__c,Dealer__r = dealer,Parent_Sales_Planning__c=mapOFforcastAndID.get(sps.SKU__c+catZoneSpe),Dealer_CustNumber__c=sps.Dealer_CustNumber__c,SPExternalIDTL__c= sps.Dealer_CustNumber__c+sps.SKU__c+catZoneSpe+month+year,SKU__c = sps.SKU__c,Zone__c = catZoneSpe,Year__c = year , Month__c = month,Target_Quantity__c = sps.Target_Quantity__c ,RecordTypeID = specailtyOEDealerId);
            insertSaleplanningDealer.add(speOEDealer);
        }
        system.debug(insertSaleplanningDealer.size()+'SSSSSSSSSSSS');
        if(insertSaleplanningDealer.size() > 0){
            try{
                database.upsert (insertSaleplanningDealer,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
                catch(Exception e){
                      System.debug(e.getMessage());
            }
            //upsert insertSaleplanningDealer SPExternalIDTL__c;
        }
        if(updateStaging.size() > 0){
            update updateStaging;
        }
    }
    global void finish(Database.BatchableContext BC)
    {       
           // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForSpecialty_OE_BU Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForSpecialty_OE_BU" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
               
            
    }
    GLOBAL static string  getTerritoryDevName( String name,String OperationType){
        String fullName;
        
        String REPL_PREF='SPRP_';
        if(OperationType==SPECIALTY){
            
            fullName=REPL_PREF+name;
        }
        if(OperationType==REPLECMENT){
            
            fullName=name;
        }
        
       
        return fullname;
    }
    
}