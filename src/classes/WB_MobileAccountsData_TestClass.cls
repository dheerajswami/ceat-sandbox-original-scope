@isTest
private class WB_MobileAccountsData_TestClass {
    private static List<Account> acc;
    private static List<Contact> con;
    private static List<Customer_Location__c> loc;
    private static final Integer LOOP_VARIABLE = 10;
    
    private  static void init(){
        acc = new List<Account>();
        con = new List<Contact>();
        loc = new List<Customer_Location__c>();
        
        for(Integer i = 0;i<LOOP_VARIABLE;i++){
            Account a = new Account(Name = 'Test Acc'+i,KUNNR__c = '100'+i);
            acc.add(a);
        }
        
        insert acc;
        
        for(Integer i = 0;i<LOOP_VARIABLE;i++){
            Contact c = new Contact(LastName = 'Test Con'+i,AccountId = acc[i].Id );
            con.add(c);
        }
        
       // insert con;
        
        for(Integer i = 0;i<LOOP_VARIABLE;i++){
            Customer_Location__c cst = new Customer_Location__c(Customer__c = acc[i].Id,Location__latitude__s = 12.34566,Location__longitude__s = 77.78877);
            loc.add(cst);
        }
        
        //insert loc;
        
    }
    
    static testMethod void myUnitTest() {
        init();
        String accString = JSON.serialize(acc);
        String locString = JSON.serialize(loc);
        String conString = JSON.serialize(con);
        String jsonN = '{'+'\"accounts\":'+ accString +',\"custLocation\": '+locString+',\"contacts\":'+conString+'}';
        
        system.RestContext.request = new RestRequest();
        RestContext.request.requestBody = Blob.valueOf(jsonN);
      
        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/UpdateMobileAccounts'; 
        RestContext.request.httpMethod = 'POST';
        WB_MobileAccountData.getAccountListAndUpdate();
    }
    
    static testMethod void myUnitTest2() {
        init();
       // String accString = JSON.serialize(acc);
        //String locString = JSON.serialize(loc);
       // String conString = JSON.serialize(con);
        String jsonN = '{'+'\"accounts\":'+ acc +',\"custLocation\": '+loc+',\"contacts\":'+con+'}';
        
        system.RestContext.request = new RestRequest();
        RestContext.request.requestBody = Blob.valueOf(jsonN);
      
        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/UpdateMobileAccounts'; 
        RestContext.request.httpMethod = 'POST';
        WB_MobileAccountData.getAccountListAndUpdate();
    }
    

}