global class CS_RegisterCustomer_Controller {
    Id accountId;
    public CS_Customer__c csCustomer{get;set;}
    public newvehicle newveh{get;set;}
    public String mobileNo {get;set;}
    public string renderDetails{get;set;}
    Account acc;
    public boolean showhide{get;set;}
    public Date dob{get; set;}
    public String preferredComm{get; set;}
    public String town{get;set;}
    public String state{get; set;}
    public boolean show{get;set;}
    public boolean showvehicles{get;set;}
    public list<CS_Vehicle__c> veh {get;set;}
    public boolean test;
    public boolean showhidebtn{get;set;}
  
    
    public CS_RegisterCustomer_Controller(ApexPages.StandardController stdController){
        Id userId = UserInfo.getUserId();
       newveh =new newvehicle();
        renderDetails = 'false';
        test = false;
        show = false;
        showhide = false;
        showhidebtn = false;
        this.csCustomer = (CS_Customer__c)stdController.getRecord();
        try{
            if(csCustomer.Town__c != '') {
                town = csCustomer.Town__c;
            }
            accountId = [select AccountId from Contact where Id = :[select contactId from User where Id = :userId].ContactId].AccountId;
            if(accountId !=null){
                csCustomer.Ceat_Shoppe__c = accountId;
                acc = [select State__c,Town__c,District__c from Account where Id = :accountId];
            }
        }
        catch(Exception e){
        }
    }   
    
    //Passing district as param and getting all towns under that district
    @RemoteAction
    global static List<SelectOption> getTowns(String state){
        //system.debug('get town method........'+strDistrict);        
        List<SelectOption> options = new List<SelectOption>();
        Set<String> townSet=new Set<String>(); 
        options.add(new SelectOption('None','--- None ---')); 
        for(State_Master__c s:[Select Town__c from State_Master__c where State__c=:state order by Town__c asc]){
            options.add(new SelectOption(s.Town__c,s.Town__c));  
        }
        return options;
    }
    
    public pagereference editVehicle()
    {
        String vehid= ApexPages.currentPage().getParameters().get('vehid');     
        return new PageReference('/a0N/e?id='+vehid+'&retURL=/'+vehid+'');
    }
    public pagereference editVehiclemobile()
    {
        String vehid= ApexPages.currentPage().getParameters().get('vehid');     
        return new PageReference('https://ceat.force.com/customers/one/one.app#/sObject/'+vehid+'/view?t=1456124174071');
    }
     public pagereference deleteVehicle()
    {
        String vehid= ApexPages.currentPage().getParameters().get('vehid');
        System.debug('---------'+vehid);
        id vhid = vehid;  
          CS_Vehicle__c csveh = [select id from CS_Vehicle__c where id=:vhid];
          if(csveh != null)
          delete csveh;
           mobileNo=csCustomer.mobile__C;
          veh = [Select Name,Next_Alignment__c,Next_Balancing_Change__c,Make__c,Makes__c,Vehicle_Registration_Number_Old__c,Vehicle_Number_Reg__c,Model__c,Models__c,Avg_Monthly_Running__c from CS_Vehicle__c where CS_Customer__r.Mobile__c = :mobileNo];
          
        return null;
    }
   
    public pagereference newendcustomervehicle()
    {
        if(csCustomer!= null)
        {
            upsert csCustomer;
        }
            test = true;
        newveh =new newvehicle();
        system.debug('in==');
        show = true; 
        showhide = true;
           renderDetails = 'false'; 
           mobileNo=csCustomer.mobile__C;
           showhidebtn = true;
             
        return null;
    }
    
   
    public PageReference submit(){
        try{
            csCustomer.DOB__c = dob;
            csCustomer.Preferred_Communication_Method__c = preferredComm;
            csCustomer.State__c =  state;
            csCustomer.Town__c = town;
            System.debug('==#1 '+csCustomer.Preferred_Communication_Method__c + ' '+csCustomer.Town__c);
            database.upsert(csCustomer,CS_Customer__c.Mobile__c);
            PageReference pg = new PageReference('/'+csCustomer.Id);
            pg.setRedirect(true);
            return pg;
        }
        catch(Exception e){
            System.debug('======== '+e.getMessage());
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null; 
        }
    }

     // Added by Neha@Extentor 

    @RemoteAction
    global static String submitCustomer(CS_Customer__c csCustomer) {
        Id userId = UserInfo.getUserId();
        system.debug('acc id ='+userid);
        Id accountId = [select AccountId from Contact where Id = :[select contactId from User where Id = :userId].ContactId].AccountId;
       system.debug('acc id ='+accountId);
        try {
            CS_Customer__c tmpCustomer = new CS_Customer__c();
            tmpCustomer = csCustomer; 
            tmpCustomer.Ceat_Shoppe__c      = accountId;
            //tmpCustomer.DOB__c              = (dob == '' ? null : Date.parse(dob));
            //tmpCustomer.Anniversary__c      = (anniversary == '' ? null : Date.parse(anniversary));
            System.debug('==#0 '+csCustomer);
            database.upsert(tmpCustomer,CS_Customer__c.Mobile__c);
            System.debug('==#1 '+csCustomer.Id);
            return String.valueOf(csCustomer.Id);
            
            
            //return 'My Bad..';
        }
        catch(Exception e) {
            throw new customException(e.getMessage());
            return null;
        }
    }

    public PageReference cancel() {
        System.debug('==#1 ');
        renderDetails = 'false';
        show = false;
        showhide = false;
        accountId = [select AccountId from User where Id = :UserInfo.getUserId()].AccountId;
        PageReference pg = new PageReference('/'+'apex/CS_RegisterCustomer');
        mobileNo ='';
        return pg;
    }
    
    public void registeredvehicles()
    { 
        renderDetails = 'true';
         veh = new list<CS_Vehicle__c> ();
         veh = [Select Name,Next_Alignment__c,Next_Balancing_Change__c,Make__c,Makes__c,Vehicle_Registration_Number_Old__c,Vehicle_Number_Reg__c,Model__c,Models__c,Avg_Monthly_Running__c from CS_Vehicle__c where CS_Customer__r.Mobile__c = :mobileNo];
       
    }
    public PageReference search(){
        try{
           String checknum = String.valueof(mobileNo);
           if(checknum.length() == 10)
           {
            show = false;
              registeredvehicles();
            csCustomer.Mobile__c = mobileNo;
            renderDetails = 'true';
             showhide = true;
             }
            CS_Customer__c existingCustomer = [select Id,Name,Mobile__c,DOB__c,Ceat_Shoppe__c,Anniversary__c,Address__c,Town__c,State__c,Phone__c,Email__c,Preferred_Communication_Method__c from CS_Customer__c where Mobile__c = :mobileNo];
            system.debug('existingCust'+existingCustomer);
            if(existingCustomer != null){
                csCustomer = existingCustomer;
                csCustomer.Ceat_Shoppe__c = accountId;
            }
            
        }
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage());
            system.debug('error'+ e.getMessage());
            csCustomer = new CS_Customer__c();
            csCustomer.Mobile__c = mobileNo;
            csCustomer.Ceat_Shoppe__c = accountId;
            return null; 
        }
        //PageReference pg = new PageReference('/'+csCustomer.Id);
        return null;
    }
     public void savenewvehicle()
    {
        
         CS_Vehicle__c csv = new CS_Vehicle__c ();
         if(csCustomer.id != null)
         {
        system.debug('new vehicle--');  
         
        csv.CS_Customer__c = csCustomer.id;
        csv.Avg_Monthly_Running__c = newveh.csveh.Avg_Monthly_Running__c;
        csv.Makes__c = newveh.csveh.Makes__c;
        csv.Make__c = newveh.csveh.Make__c;
        csv.Last_Alignment__c= newveh.csveh.Last_Alignment__c;
        csv.Models__c = newveh.csveh.Models__c;
        csv.Model__c = newveh.csveh.Model__c;
        csv.NSD__c = newveh.csveh.NSD__c ;
        csv.Last_Balancing_Change__c= newveh.csveh.Last_Balancing_Change__c;
        csv.Vehicle_Registration_Number_Old__c = newveh.csveh.Vehicle_Registration_Number_Old__c;
            system.debug('new vehicle if--'+csv);  
        if(csv.Last_Alignment__c>system.today() || csv.Last_Balancing_Change__c>system.today())
            {
                
                   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Date Should not be Greater than Today' ));
                    newveh.csveh.Last_Alignment__c= null;
                    newveh.csveh.Last_Balancing_Change__c= null;
                      //newveh = new newvehicle ();
             }
             else
             {
                 upsert csv;
                  show = false;
             CS_Vehicle__c csvnew = new CS_Vehicle__c ();
           csvnew  = [Select Name,Next_Alignment__c,Next_Balancing_Change__c,Make__c,Makes__c,Vehicle_Registration_Number_Old__c,Vehicle_Number_Reg__c,Model__c,Models__c,Avg_Monthly_Running__c from  CS_Vehicle__c  where id = : csv.id];
            veh .add(csvnew);
            renderDetails = 'true';
            mobileNo=csCustomer.mobile__C;
             }
           }
            
       
    }
    
    public class newvehicle{
        public CS_Vehicle__c csveh{get;set;}
        public newvehicle()
        {
            csveh = new CS_Vehicle__c ();
        }
    }
    
    public class customException extends Exception{}
    
}