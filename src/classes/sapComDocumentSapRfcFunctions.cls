//Generated by wsdl2apex

public class sapComDocumentSapRfcFunctions {
    public class ZSFDC_MATERIAL_NBP_element {
        public sapComDocumentSapRfcFunctions.TABLE_OF_ZSFDC_MAT_NBP IT_OUTPUT;
        public String S_DATE;
        private String[] IT_OUTPUT_type_info = new String[]{'IT_OUTPUT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] S_DATE_type_info = new String[]{'S_DATE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_OUTPUT','S_DATE'};
    }
    public class TABLE_OF_ZSFDC_MAT_NBP {
        public sapComDocumentSapRfcFunctions.ZSFDC_MAT_NBP[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSFDC_MAT_NBP {
        public String MATERIAL_NUMBER;
        public String MATERIAL_DESP;
        public String MATERIAL_GROUP;
        public String MATERIAL_GROUP_DESCRIPTION;
        public String VALID_FROM;
        public String VALID_TO;
        public String CONDITION_RECORD;
        public String NBP_PRICE_WITHOUT_TAX;
        private String[] MATERIAL_NUMBER_type_info = new String[]{'MATERIAL_NUMBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATERIAL_DESP_type_info = new String[]{'MATERIAL_DESP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATERIAL_GROUP_type_info = new String[]{'MATERIAL_GROUP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATERIAL_GROUP_DESCRIPTION_type_info = new String[]{'MATERIAL_GROUP_DESCRIPTION','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VALID_FROM_type_info = new String[]{'VALID_FROM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VALID_TO_type_info = new String[]{'VALID_TO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CONDITION_RECORD_type_info = new String[]{'CONDITION_RECORD','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] NBP_PRICE_WITHOUT_TAX_type_info = new String[]{'NBP_PRICE_WITHOUT_TAX','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'MATERIAL_NUMBER','MATERIAL_DESP','MATERIAL_GROUP','MATERIAL_GROUP_DESCRIPTION','VALID_FROM','VALID_TO','CONDITION_RECORD','NBP_PRICE_WITHOUT_TAX'};
    }
    public class ZSFDC_MATERIAL_NBPResponse_element {
        public sapComDocumentSapRfcFunctions.TABLE_OF_ZSFDC_MAT_NBP IT_OUTPUT;
        private String[] IT_OUTPUT_type_info = new String[]{'IT_OUTPUT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_OUTPUT'};
    }
    public class ZWS_SFDC_MATERIAL_NBP {
        public String endpoint_x = 'http://CTECQHAP.ceat.in:8000/sap/bc/srt/rfc/sap/zws_sfdc_material_nbp/300/zws_sfdc_material_nbp/zws_sfdc_material_nbp';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'sapComDocumentSapRfcFunctions'};
        public sapComDocumentSapRfcFunctions.TABLE_OF_ZSFDC_MAT_NBP ZSFDC_MATERIAL_NBP(sapComDocumentSapRfcFunctions.TABLE_OF_ZSFDC_MAT_NBP IT_OUTPUT,String S_DATE) {
            sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBP_element request_x = new sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBP_element();
            request_x.IT_OUTPUT = IT_OUTPUT;
            request_x.S_DATE = S_DATE;
            sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBPResponse_element response_x;
            Map<String, sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBPResponse_element> response_map_x = new Map<String, sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBPResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:ZWS_SFDC_MATERIAL_NBP:ZSFDC_MATERIAL_NBPRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_MATERIAL_NBP',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_MATERIAL_NBPResponse',
              'sapComDocumentSapRfcFunctions.ZSFDC_MATERIAL_NBPResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.IT_OUTPUT;
        }
    }
}