global class CPORT_SalesRegisterTLDetails {

 /*
   * Auther      :- Sneha Agrawal
   * Purpose     :- Fetch details of Sales Register Details
   *                from SAP 
   * Modified By :- 
   * Date        :- 15/12/2014
   *
   */
   
    //-- ATTRIBUTES
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    /*   This method will return list of Sales register values
   */
   
  
  WebService static List<CPORT_SalesRegisterDetails.SalesRegisterMapping>  getAllSalesRegisterDetails(String territory_code, String fDate, String tDate){
    
      List<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesReg = new List<CPORT_SalesRegisterDetails.SalesRegisterMapping>();
      try{
          String customerId                 = UtilityClass.addleadingZeros(territory_code,Integer.valueOf(CUST_ID_LENGTH));
        
          SAPLogin__c saplogin               = SAPLogin__c.getValues('SAP Login');        
            
            String username                         = saplogin.username__c;
            String password                 = saplogin.password__c;
            Blob headerValue                 = Blob.valueOf(username + ':' + password);
            String authorizationHeader             = 'Basic '+ EncodingUtil.base64Encode(headerValue);
               
          sapZWS_SALES_REGISTER_TL tst           = new sapZWS_SALES_REGISTER_TL ();
          sapZWS_SALES_REGISTER_TL.ZWS_SALES_REGISTER_TL zws   = new sapZWS_SALES_REGISTER_TL.ZWS_SALES_REGISTER_TL ();
            
            zws.inputHttpHeaders_x               = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                   = 60000;
            
          sapZWS_SALES_REGISTER_TL.TableOfZsd05a tom     = new sapZWS_SALES_REGISTER_TL.TableOfZsd05a ();
        
          List<sapZWS_SALES_REGISTER_TL.Zsd05a> m       = new List<sapZWS_SALES_REGISTER_TL.Zsd05a>();        
            
          tom.item = m;        
            
           sapZWS_SALES_REGISTER_TL.TableOfZsd05a e = new  sapZWS_SALES_REGISTER_TL.TableOfZsd05a ();
           // e = zws.ZbapiSalesRegisterTl('D0061','2014-10-01',tom,'2014-10-31') ;
           e = zws.ZbapiSalesRegisterTl('D0041','2015-03-01',tom,'2015-03-31') ;
           system.debug(e);
           
           //Addde by Sneha
           if(e.item != null){            
                for(sapZWS_SALES_REGISTER_TL.Zsd05a z : e.item) {
                    salesReg.add(new CPORT_SalesRegisterDetails.SalesRegisterMapping(z.Arktx,z.Bzirk,z.Candoc,z.Ceat1,z.Erdat,z.Erzet,z.Fkart,z.Fkdat,z.Fkimg,z.KdgrpAuft,z.Kunag,z.Kvgr1,z.Mandt,z.Matkl,z.Matnr,z.Mwsbp,z.Nbpprice,z.Netwr,z.Ntgew,z.Posnr,z.Post,z.Pre,z.Town,z.Vbeln,z.Vkbur,z.Vkburtext,z.Vkgrp,z.Vkgrptext,z.Vbeln,z.Werks,z.Wgbez,z.Zterm));
                }
            }
            return salesReg;
         }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
         }
      
    } 
    
 }