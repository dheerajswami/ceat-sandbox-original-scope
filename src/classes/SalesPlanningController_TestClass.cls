@isTest
public class SalesPlanningController_TestClass{  
	
	  static user userTLRep = new user();
	  static user userTLDRep = new user();
	  static user userRMRep = new user();
	  static user specialtyUser = new user();
	  static user exportUser = new user();
	  static user adminUser = new user();
	  static String amRoleSpecilaty		= system.label.AM_SpecialtyURole;
      static String managerExports			= system.label.ExportSrManager;
	  
	  
	  static testmethod void createUserTest() {
	  	SalesPlanningController s = new SalesPlanningController();
	  	PJPPlanningController ps = new PJPPlanningController();
	  	
	  	userTLRep = CEAT_InitializeTestData.createUser('mm','userTLRep','sneha.agrawal@extentor.com','snehaTL81@ceat.com','12345','Replacements',null,'TL_Replacement');
        insert userTLRep;
       
        userTLDRep = CEAT_InitializeTestData.createUser('mm','userTLDRep','sneha.agrawal@extentor.com','snehaTLD81@ceat.com','123345','Replacements',null,'TLD');
        insert userTLDRep;
        
        userRMRep = CEAT_InitializeTestData.createUser('mm','userRMRep','sneha.agrawal@extentor.com','snehaRM81@ceat.com','122345','Replacements',null,'RM');
        insert userRMRep;
        
        User userRMRep1 = CEAT_InitializeTestData.createUser('mm','userRMRep1','sneha.agrawal@extentor.com','snehaRM111@ceat.com','122345','Replacements',null,'RBM_MA');
        insert userRMRep1;
       
        specialtyUser = CEAT_InitializeTestData.createUser('mm','specialtyUser','sneha.agrawal@extentor.com','specialtyUser@ceat.com','142345','Specialty',null,'RM_Specialty');
        insert specialtyUser;
        
        User specialtyUser1 = CEAT_InitializeTestData.createUser('mm','specialtyUser1','sneha.agrawal@extentor.com','specialtyUser1@ceat.com','142345','Specialty',null,'GM_Specialty');
        insert specialtyUser1;
        
        exportUser = CEAT_InitializeTestData.createUser('mm','exportUser','sneha.agrawal@extentor.com','exportUser@ceat.com','412345','Exports',null,'Sr_Manager_Exports');
        insert exportUser;
        
        User exportUser1 = CEAT_InitializeTestData.createUser('mm','exportUser1','sneha.agrawal@extentor.com','exportUser1@ceat.com','412345','Exports',null,'GM_Exports');
        insert exportUser1;
        
        adminUser = CEAT_InitializeTestData.createUser('mm','adminUser','sneha.agrawal@extentor.com','adminUser@ceat.com','412345','OE',null,'ED');
        insert adminUser;
        
        User adminUser1 = CEAT_InitializeTestData.createUser('mm','adminUser','sneha.agrawal@extentor.com','adminUser1@ceat.com','412345','System Administrator',null,'ED');
        insert adminUser1;
        
        system.runAs(userTLRep){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
        
        system.runAs(userTLDRep){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
        
        system.runAs(userRMRep){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
        system.runAs(userRMRep1){
        	PageReference pg=SalesPlanningController.getpage();
        }
        system.runAs(specialtyUser){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
        system.runAs(specialtyUser1){
        	PageReference pg=SalesPlanningController.getpage();
        }
        system.runAs(exportUser){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
        system.runAs(exportUser1){
        	PageReference pg=SalesPlanningController.getpage();
        }
        system.runAs(adminUser){
        	
        	PageReference pg1=PJPPlanningController.getpage();
        }
        system.runAs(adminUser1){
        	PageReference pg=SalesPlanningController.getpage();
        	PageReference pg1=PJPPlanningController.getpage();
        }
	  }
}