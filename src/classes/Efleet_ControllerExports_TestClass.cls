/*******************************************************************************
    Created By : Sneha Agrawal
    Date       : 14th July, 2015
    Purpose    : Test class for Efleet_HomeController_Exports
********************************************************************************/
@isTest
public class Efleet_ControllerExports_TestClass{
    static User init(){
        User tlUser = CEAT_InitializeTestData.createUser('Admin', 'Admin', 'admin@adminceat.com', 'admin@adminceat.com', '100098776', 'Exports', null, 'CSTL');
        insert tlUser;     
        
        Territory2Model terrModel = CEAT_InitializeTestData.createTerritoryModel('Model', 'India');
        insert terrModel;
        Id terrTypeTerr;
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');        
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            
        }
        
        Territory2 cstlTerr = CEAT_InitializeTestData.createTerritory('DI007', 'DI007' , terrTypeTerr, terrModel.Id, null);
        insert cstlTerr;
        UserTerritory2Association utass =  CEAT_InitializeTestData.createUserTerrAsso(tlUser.Id, cstlTerr.Id, 'CSTL');
        insert utass;
        return tlUser;
    }        
    
    static testmethod void testEfleetExports(){
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', '5000234509');
            //acc1.Fleet_Type__c = '';
            insert acc1;
            Account acc2 = CEAT_InitializeTestData.createAccount('Acc2', '5000234508');
            
            Contact con = CEAT_InitializeTestData.createContact('Con', acc1.Id);
            insert con;
            
            Vehicle__c veh = CEAT_InitializeTestData.createVehicle('On Road', 'Normal Load', 'Truck', '14', acc1.Id); 
            veh.Wheel_Type__c = '4W';
            //insert veh;
            Tyre__c newTyre = CEAT_InitializeTestData.createTyre('CEAT', 0, 'HCL', 'Front', '10.00-20', '1', veh.Id);            
            
            Efleet_HomeController_Exports.fetchAllFleets();
            Efleet_HomeController_Exports.fetchAllDealers();
            Efleet_HomeController_Exports.fetchTlTerritories();            
            Efleet_HomeController_Exports.fetchVehicalInformation(acc1.Id);
            
            Efleet_HomeController_Exports.saveVehicalInformation(veh);            
            Efleet_HomeController_Exports.addNewTyreInfo(newTyre);
            
            Efleet_HomeController_Exports.saveTyreInformation(string.valueof(newTyre),'12/12/2014','12/01/2015');     
            Efleet_HomeController_Exports.saveTyreInformation(string.valueof(newTyre),'','');    
             
             
            list<Tyre_Inspection__c> insList = new list<Tyre_Inspection__c>();
            Tyre_Inspection__c tyreInsp = new Tyre_Inspection__c();
            tyreInsp.Visit_Date__c = date.today();
            tyreInsp.Tyre__c = newTyre.Id;
            //tyreInsp.OwnerId = userInfo.getUserId();            
            
            insList.add(tyreInsp);
            Tyre_Inspection__c tyreInsp1 = new Tyre_Inspection__c();
            tyreInsp1.Visit_Date__c = date.today();
            tyreInsp1.Tyre__c = newTyre.Id;
        
            Efleet_HomeController_Exports.saveInspection(insList);
            Efleet_HomeController_Exports.saveInspectionSingle(tyreInsp1);
            
            Efleet_HomeController_Exports.fetchTyreAndInspections(veh.Id);
            Efleet_HomeController_Exports.fetchTyreSizes();
            
            Item_Master__c itemMaster = CEAT_InitializeTestData.createMaterialMaster('10.00-20', 'CEAT', 'HCL', 12000, 15, 'LUG', 'Bias');
            Item_Master__c itemMaster1 = CEAT_InitializeTestData.createMaterialMaster('10.00-20', 'CEAT', 'HCL-1', 12000, 15, 'LUG', 'Bias');
            List<Item_Master__c> itemMasterList = new List<Item_Master__c>();
            itemMasterList.add(itemMaster);
            itemMasterList.add(itemMaster1);
            insert itemMasterList;
            
            Efleet_HomeController_Exports.fetchPatterns();
            Efleet_HomeController_Exports.fetchTyrePatternSize();
            
            Efleet_HomeController_Exports.fetchRelatedPatterns('CEAT','10.00-20');
            
            OE_Dealer__c oed = CEAT_InitializeTestData.createOeDealer('Oe','TML');
            Efleet_HomeController_Exports.fetchOeDealers('TML');
            
            Efleet_HomeController_Exports.fetchCustomerTypes();
            Efleet_HomeController_Exports.fetchMaterialMaster();
            
            State_Master__c stateMaster = CEAT_InitializeTestData.createStateMaster('Karnataka', 'Bangalore', 'Koramangala');
            insert stateMaster;
            
            Efleet_HomeController_Exports.fetchCustomerCategories();
            Efleet_HomeController_Exports.fetchStates();
            
            Efleet_HomeController_Exports.fetchDistricts('Karnataka');
            Efleet_HomeController_Exports.fetchTowns('Bangalore');
            
            Fleet_Vehicle_Information__c vehInfo = CEAT_InitializeTestData.createVehicleInfo(acc1.Id);
            insert vehInfo;
            Scrap_Yard_Report__c scrapReport = CEAT_InitializeTestData.createScrapReport(acc1.Id);
            insert scrapReport;
            Fleet_Operation__c fleetOperation = CEAT_InitializeTestData.createOperation(acc1.Id);
            insert fleetOperation;
            
            Efleet_HomeController_Exports.fetchDefectTypes();
            Efleet_HomeController_Exports.fetchTruckMake();
            
            Efleet_HomeController_Exports.fetchTyreClasses();
            Efleet_HomeController_Exports.fetchMaterials();
            
            Haul_Type1__c haulMaster = CEAT_InitializeTestData.createHaulType('Short', 100);
            insert haulMaster;
            
            Efleet_HomeController_Exports.fetchHaulType(100);
            
            Payload_Type1__c payloadMaster = CEAT_InitializeTestData.createPayload('Payload', '6W', 100, 200, 300, 400);
            insert payloadMaster;            
            Efleet_HomeController_Exports.fetchLoadRating('Payload',100);
            
            Recommended_Tyre__c recomTyre = CEAT_InitializeTestData.createRecommendedTyre(fleetOperation.Id);
            insert recomTyre;
            
            Fitment_Survey__c fs = new Fitment_Survey__c();
            fs.Transporter_Name__c = 'Test';
            fs.Vehicle_No__c = 'KA2048';
            fs.Vehicle_Category__c = 'Two Wheeler';
            insert fs;
            
            Efleet_HomeController_Exports.fetchAllFitmentSurveys();         
            Efleet_HomeController_Exports.fetchFitmentTyres(fs.id);
            
            Efleet_HomeController_Exports.fetchVehicleCategories();
            Efleet_HomeController_Exports.fetchLinkedWheelTypes('Heavy Commercial');
            
            Efleet_HomeController_Exports.fetchLinkedWheelTypes('Light Commercial');
            Efleet_HomeController_Exports.fetchLinkedWheelTypes('PCR/UCR');
            Efleet_HomeController_Exports.fetchLinkedWheelTypes('Last Mile');
            Efleet_HomeController_Exports.fetchLinkedWheelTypes('Two Wheeler');
            
            Efleet_HomeController_Exports.fetchSelectedFleet(acc1.Id);
            Efleet_HomeController_Exports.fetchSelectedContact(acc1.Id);
            
            Efleet_HomeController_Exports.fetchSelectedVehicleInfo(acc1.Id);
            Efleet_HomeController_Exports.fetchSelectedScrap(acc1.Id);
            
            Efleet_HomeController_Exports.fetchSelectedOperation(acc1.Id);
            Efleet_HomeController_Exports.fetchSelectedRecommTyre(acc1.Id);
            
            Efleet_HomeController_Exports.saveFleetDetails(acc1,con);
            Efleet_HomeController_Exports.saveContactDetails(con,'30/12/1991','30/12/2014');
           
            List<Fleet_Vehicle_Information__c> vehInfoList = new List<Fleet_Vehicle_Information__c>();
            vehInfoList.add(vehInfo);
            List<Scrap_Yard_Report__c> scrapReportList = new List<Scrap_Yard_Report__c>();
            scrapReportList.add(scrapReport);
            
            Efleet_HomeController_Exports.saveVehicleInfo(acc1,vehInfoList);
            Efleet_HomeController_Exports.saveScrapReport(scrapReportList,acc1.Id,new List<String>{'20/04/2015'});
            Sales_Tracker__c stracker = CEAT_InitializeTestData.createSalesTracker(acc1.Id, 'January', '2014');
            
            Sales_Tracker_Item__c strackerItem11 = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id); 
            strackerItem11.Month__c ='june';
            strackerItem11.Year__c =string.valueof(date.today().year());
            insert strackerItem11;
            Efleet_HomeController_Exports.fetchRunningMonthSTIs(stracker.Id);
        
            
            Sales_Tracker_Item__c strackerItem = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id);            
            strackerItem.Month__c = Efleet_HomeController.calculateMonth(Date.today().month());
            strackerItem.Year__c = String.valueOf(Date.today().year());
            List<Sales_Tracker_Item__c> strackerItemList = new List<Sales_Tracker_Item__c>();            
            strackerItemList.add(strackerItem);
            
            Efleet_HomeController_Exports.saveSalesTracker(stracker, strackerItemList);
            
            Cost_Tracker__c ctracker = CEAT_InitializeTestData.createCostTracker(acc1.Id);    
            Cost_Tracker_Item__c ctrackerItem = CEAT_InitializeTestData.createCostTrackerItem(ctracker.Id);            
            List<Cost_Tracker_Item__c> ctrackerItemList = new List<Cost_Tracker_Item__c>();
            ctrackerItemList.add(ctrackerItem);
            Efleet_HomeController_Exports.saveCostTracker(ctracker, ctrackerItemList); 
            
            list<Fitment_Survey_Tyre__c> sftList = new list<Fitment_Survey_Tyre__c>();
            Fitment_Survey_Tyre__c sft = new Fitment_Survey_Tyre__c();
            sft.Brand__c ='CEAT';
            insert sft;
            sftList.add(sft);
            
            Efleet_HomeController_Exports.saveFitmentSurvey(fs,sftList);
            
            List<Recommended_Tyre__c> recomTyreList = new List<Recommended_Tyre__c>();
            recomTyreList.add(recomTyre);
            List<Recommended_Tyre__c> recomTyreList1 = new List<Recommended_Tyre__c>();
            recomTyreList1.add(recomTyre);
            Efleet_HomeController_Exports.savePreSalesApproach(fleetOperation, recomTyreList, recomTyreList1);
            
            Truck_Tyre_Matrix__c ttMatrix = CEAT_InitializeTestData.createTruckTyreMatrix('Truck', '6W', 'Bias', 'On Road', 'Normal Load', 'Short', '10.00-20', 'HCL', 'HCL-1');
            insert ttMatrix;    
            
            
            Efleet_HomeController_Exports.fetchRecommendations('Truck','6W','Bias','On Road', 'Normal Load', 'Short', '10.00-20');
            
            Efleet_HomeController_Exports.fetchRecommendedTyres('Truck', '6W', 'Bias', 'On Road', 'Normal Load', 'Short', '10.00-20');
            //List<Sales_Tracker_Item__c> strackerItemList = new List<Sales_Tracker_Item__c>();            
            //strackerItemList.add(strackerItem);
           
            Sales_Tracker_Item__c strackerItem1 = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id);
            insert strackerItem1;
            Efleet_HomeController_Exports.fetchModes();
          
            Efleet_HomeController_Exports.fetchRunningMonthST(acc1.Id);
            //Efleet_HomeController_Exports.fetchRunningMonthSTIs(stracker.Id); 
            Sales_Tracker_Item__c strackerItem112 = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id); 
            strackerItem112.Month__c ='May';
            strackerItem112.Year__c =string.valueof(date.today().year());
            insert strackerItem112;
            Efleet_HomeController_Exports.fetchRunningMonthSTIs(stracker.Id);            
        
        
            Efleet_HomeController_Exports.fetchCostTracker(acc1.Id); 
            
            Efleet_HomeController_Exports.fetchCostTrackerItems(ctracker.Id);
            Efleet_HomeController_Exports.fetchRetreadPatterns();
            
            Efleet_HomeController_Exports.fetchStateCodes();
            Efleet_HomeController_Exports.fetchRTOCodes();
            Efleet_HomeController_Exports.getTopFivePattern(acc1.Id);
            
            Fleet_Business_Card__c bc = new Fleet_Business_Card__c();
            bc.Tyre_Brand__c ='CEAT';
            insert bc;
            list<Fleet_Business_Card__c> bcList = new list<Fleet_Business_Card__c>();
            
            Efleet_HomeController_Exports.getBuisnessCardValues('June', 'May', acc1.Id);
            Efleet_HomeController_Exports.insertBuisnessCardValues(bcList,bc,'May','April',acc1.Id);
            
            Efleet_HomeController_Exports.getAllStateMaster();
             Efleet_HomeController_Exports.sortListAndOthers(new List<String>{'Other'});
    }
    
}