global class MobileDashboard_Controller{
   
   public static list<String> months                 = new List<String>{'April','May','June','July','August','September','October','November','December','January','February','March'};
   static String repDealerLabel                      = System.Label.Replacement_Dealer;
   public static list<Sales_Planning__c> salesList   {get;set;}    
  
   public static string jSONString                              {get;set;}
 
   public static map<String,list<Sales_Planning__c>> monthlySalesPlanningMap= new map<String,list<Sales_Planning__c>>();
   public static map<String,string> monthValues                             = new map<String,string>();
   
   public static map<string,Double> monthlyTotalTargetMap                   = new map<String, Double>();
   public static map<string,Double> monthlyTotalTargetMTDMap                = new map<String, Double>();
   public static map<string,Double> monthlyTotalActualsMap                  = new map<String, Double>();
   public static map<string,string> mapOfCatCodeAndCatName                  = new map<string,string>();
   
   public static map<string,string> mapOfDealerIdAndName;
   public static map<string,set<string>> mapOfMonthAndDealerIds;
   public static set<String> dealerSet ;
   
   static map<String, list<Decimal>> monthlySalesActualValues   {get;set;} 
   static map<String, String> dealerIdNameMap                   {get;set;}

   public static String loggedInUserTerritory  ;
   public static String loggedInUserId  ;
   
   public static String currentYr                               = string.valueof(Date.today().year()); 
   
   public static set<String> catSet                             {get;set;}  
   public static String selectedMonth                           {get;set;}
   public static List<DealerWrapper> dealerTargets              {get;set;} // By Vivek
   
   static Map<String, Map<String,Decimal>> dealerWisemonthlyTargets   = new Map<String, Map<String,Decimal>>();
   static Map<String, Map<String,Decimal>> dealerWisemonthlyTargetMTD = new Map<String, Map<String,Decimal>>();
   static Map<String, Map<String,Decimal>> dealerWisemonthlyActuals   = new Map<String, Map<String,Decimal>>();
   static Map<String, Map<String,Decimal>> dealerWisemonthlyAchievement = new Map<String, Map<String,Decimal>>();
  
  //CONSTRUCTOR
   public MobileDashboard_Controller (){  
       
   }
   public MobileDashboard_Controller(CPORT_PortalHomeController controller) {

    }
   global Class CatgoryWrapper{
      public string catCode     {get;set;}
      public string catName     {get;set;}
      public Double target      {get;set;}
      public Double targetMTD   {get;set;}
      public Double actual      {get;set;}
      public Double achievement {get;set;}
   }
   global Class DealerWrapper{
      public string dealerId    {get;set;}
      public string dealerName  {get;set;}
      public Double targets     {get;set;}
      public Double targetMTD   {get;set;}
      public Double actuals     {get;set;}
      public Double achievement {get;set;}
   }
   public static void spCalculation (){       
        
        salesList               = new list<Sales_Planning__c>();
        list<UserTerritory2Association> userTerrCode            = new list<UserTerritory2Association>();
        list<Territory2>  userTerritory           = new list<Territory2>();
        dealerIdNameMap         = new map<String, String>();
        list<Sales_Planning__c> salesListTemp ;
        dealerTargets = new List<DealerWrapper>();
        catSet = new set<string>();        
       
        mapOfDealerIdAndName                 = new map<string,string>();
        mapOfMonthAndDealerIds               = new map<string,set<string>>();
        
        String currentMon  = string.valueof(Date.today().month());
        String previousMon = string.valueof(Date.today().month()-1);
        
        /* Get month name from month(number) */
      
        monthValues.put('1','January');
        monthValues.put('2','February');
        monthValues.put('3','March');
        monthValues.put('4','April');
        monthValues.put('5','May');
        monthValues.put('6','June');
        monthValues.put('7','July');
        monthValues.put('8','August');
        monthValues.put('9','September');
        monthValues.put('10','October');
        monthValues.put('11','November');
        monthValues.put('12','December');        
       
        if(dealerWisemonthlyTargets == null){
           dealerWisemonthlyTargets = new Map<String, Map<String,Decimal>>();
        }
        dealerWisemonthlyTargets.put('April',new map<String,Decimal>());
        dealerWisemonthlyTargets.put('May',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('June',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('July',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('August',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('September',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('October',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('November',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('December',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('January',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('February',new Map<String,Decimal>());
        dealerWisemonthlyTargets.put('March',new Map<String,Decimal>());
        
        if(dealerWisemonthlyTargetMTD == null){
           dealerWisemonthlyTargetMTD = new Map<String, Map<String,Decimal>>();
        }
        dealerWisemonthlyTargetMTD.put('April',new map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('May',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('June',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('July',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('August',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('September',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('October',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('November',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('December',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('January',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('February',new Map<String,Decimal>());
        dealerWisemonthlyTargetMTD.put('March',new Map<String,Decimal>());
        
        if(dealerWisemonthlyActuals == null){
           dealerWisemonthlyActuals = new Map<String, Map<String,Decimal>>();
        }
        dealerWisemonthlyActuals.put('April',new map<String,Decimal>());
        dealerWisemonthlyActuals.put('May',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('June',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('July',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('August',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('September',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('October',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('November',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('December',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('January',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('February',new Map<String,Decimal>());
        dealerWisemonthlyActuals.put('March',new Map<String,Decimal>());
                
        if(dealerWisemonthlyAchievement == null){
           dealerWisemonthlyAchievement = new Map<String, Map<String,Decimal>>();
        }
        dealerWisemonthlyAchievement.put('April',new map<String,Decimal>());
        dealerWisemonthlyAchievement.put('May',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('June',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('July',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('August',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('September',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('October',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('November',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('December',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('January',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('February',new Map<String,Decimal>());
        dealerWisemonthlyAchievement.put('March',new Map<String,Decimal>());
        
        loggedInUserId  = UserInfo.getUserId();        
         
        userTerrCode = [select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId limit 1];  
        if(userTerrCode.size() > 0){
             userTerritory = [select name,id from Territory2 where id =: userTerrCode[0].Territory2Id limit 1];
             loggedInUserTerritory = userTerritory[0].name;
        }
       
       list<Sales_Planning_Categories__c> categoryList = Sales_Planning_Categories__c.getall().values();
        for(Sales_Planning_Categories__c cat:categoryList){
                if(cat.Include_in_Sales_Planning__c == true){
                    catSet.add(cat.Category_Code__c); 
                    mapOfCatCodeAndCatName.put(cat.Category_Code__c,cat.Name);  
                }
        }
        
       Id replaceDealerId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
      
        /* Getting sales planning Delaer Record */
        if(loggedInUserTerritory!='' && loggedInUserTerritory!=null ){
            salesList= [Select id,name,Actual_Sales_Value__c,Achievement_MTD__c,MTD_target_Val__c,Dealer__r.Id,Dealer__r.Name,Category_Description__c,PlannedAspValue__c,Category__c,Dealer__c,Month__c,Year__c,Active__c,Dealer__r.KUNNR__c from Sales_Planning__c  where Dealer__r.Sales_District_Text__c =: loggedInUserTerritory AND RecordTypeId=:replaceDealerId AND category__c IN : catSet AND (Month__c=:currentMon OR Month__c=:previousMon) AND Year__c=:currentYr];
        }
      
       /* Month wise Sales Planning (Current month and previous month) */
       
       if(salesList.size()>0){
            list<Sales_Planning__c> salesPlanningListTemp;
            for(Sales_Planning__c sp : salesList){
                catSet.add(sp.category__c);
                dealerIdNameMap.put(sp.Dealer__c,sp.Dealer__r.Name);
                mapOfDealerIdAndName.put(sp.Dealer__c,sp.Dealer__r.Name);
                
                if(mapOfMonthAndDealerIds.containsKey(monthValues.get(sp.Month__c))){
                    dealerSet = mapOfMonthAndDealerIds.get(monthValues.get(sp.Month__c));
                    dealerSet.add(sp.Dealer__c);
                    mapOfMonthAndDealerIds.put(monthValues.get(sp.Month__c),dealerSet);
                }else{
                    dealerSet                                    = new  set<String>();
                    dealerSet.add(sp.Dealer__c);
                    mapOfMonthAndDealerIds.put(monthValues.get(sp.Month__c),dealerSet);
                }               
                if(monthlySalesPlanningMap.containsKey(monthValues.get(sp.Month__c))){
                    salesPlanningListTemp = monthlySalesPlanningMap.get(monthValues.get(sp.Month__c));
                    salesPlanningListTemp.add(sp);
                    monthlySalesPlanningMap.put(monthValues.get(sp.Month__c),salesPlanningListTemp);
                }else{
                    salesPlanningListTemp = new list<Sales_Planning__c>();
                    salesPlanningListTemp.add(sp);
                    monthlySalesPlanningMap.put(monthValues.get(sp.Month__c),salesPlanningListTemp);
                }
            }
       }
              
      if(monthlySalesPlanningMap.size()>0){
          for(String mon : monthlySalesPlanningMap.keySet()){
                
                salesListTemp = new list<Sales_Planning__c>();
                salesListTemp = monthlySalesPlanningMap.get(mon);
                
                map<String,Decimal> monthlyTargets ;
                map<String,Decimal> monthlyTargetMTD ;
                map<String,Decimal> monthlyActuals ;
                map<String,Decimal> monthlyAchievement ;            
                
                monthlyTargets          = dealerWisemonthlyTargets.get(mon);
                monthlyTargetMTD        = dealerWisemonthlyTargetMTD.get(mon);
                monthlyActuals          = dealerWisemonthlyActuals.get(mon);
                monthlyAchievement      = dealerWisemonthlyAchievement.get(mon);                
                
                Double monTotTarget         = 0;
                Double monTotTargetMTD      = 0;
                Double monTotActual         = 0;
             
             for(Sales_Planning__c sp :salesListTemp){   
                
                 Double totalTarget      = 0;
                 Double totalTargetMTD   = 0;
                 Double totalActual      = 0;
                 Double totalAchv      = 0;
                 
                 string key     = monthValues.get(sp.Month__c) + sp.Dealer__c;
                 string catKey  = monthValues.get(sp.Month__c) + sp.category__c;
               
                /*    Montly Dealer wise Sales Target    */
                
                if(monthlyTargets.containsKey(key)){
                    totalTarget = monthlyTargets.get(key);
                    if(sp.PlannedAspValue__c>0 && sp.PlannedAspValue__c!=null){
                        totalTarget = totalTarget + sp.PlannedAspValue__c;
                    }else{
                        totalTarget = totalTarget + 0;
                    } 
                    monthlyTargets.put(key,totalTarget);
                }else{
                    if(sp.PlannedAspValue__c!=null && sp.PlannedAspValue__c>0){
                        totalTarget = sp.PlannedAspValue__c ;
                    }else{
                        totalTarget = 0 ;
                    }
                    monthlyTargets.put(key,totalTarget);
                }
                
                dealerWisemonthlyTargets.put(mon,monthlyTargets);
               
                /*    Montly Dealer wise Sales Target  MTD  */
                
                if(monthlyTargetMTD.containsKey(key)){
                    totalTargetMTD = monthlyTargetMTD.get(key);
                    if(sp.MTD_target_Val__c>0 && sp.MTD_target_Val__c!=null){
                        totalTargetMTD = totalTargetMTD + sp.MTD_target_Val__c;
                    }else{
                        totalTargetMTD = totalTargetMTD + 0;
                    } 
                    monthlyTargetMTD.put(key,totalTargetMTD);
                }else{
                    if(sp.MTD_target_Val__c!=null && sp.MTD_target_Val__c>0){
                        totalTargetMTD = sp.MTD_target_Val__c ;
                    }else{
                        totalTargetMTD = 0 ;
                    }
                    monthlyTargetMTD.put(key,totalTargetMTD);
                }
                
                dealerWisemonthlyTargetMTD.put(mon,monthlyTargetMTD);
               
                /*  Montly Sales Actuals */
       
                if(monthlyActuals.containsKey(key)){
                    totalActual =  monthlyActuals.get(key);
                   
                    if(sp.Actual_Sales_Value__c>0 && sp.Actual_Sales_Value__c!=null){
                        totalActual = totalActual + sp.Actual_Sales_Value__c; 
                    }else{
                        totalActual = totalActual + 0;
                    }                  
                    monthlyActuals.put(key,totalActual);
                }else{
                    if(sp.Actual_Sales_Value__c!=null && sp.Actual_Sales_Value__c>0){
                        totalActual = sp.Actual_Sales_Value__c ;
                    }else{
                        totalActual = 0 ;
                    }
                    monthlyActuals.put(key,totalActual);
                }
                
                dealerWisemonthlyActuals.put(mon,monthlyActuals);
                
                /*  Montly Achievements */
       
                if(monthlyAchievement.containsKey(key)){
                    totalAchv =  monthlyAchievement.get(key);
                   
                    if(sp.Achievement_MTD__c>0 && sp.Achievement_MTD__c!=null){
                        totalAchv = totalAchv + sp.Achievement_MTD__c; 
                    }else{
                        totalAchv = totalAchv + 0;
                    }                  
                    monthlyAchievement.put(key,totalAchv);
                }else{
                    if(sp.Achievement_MTD__c!=null && sp.Achievement_MTD__c>0){
                        totalAchv = sp.Achievement_MTD__c ;
                    }else{
                        totalAchv = 0 ;
                    }
                    monthlyAchievement.put(key,totalAchv);
                }                
                dealerWisemonthlyAchievement.put(mon,monthlyAchievement);
                
              
             }
          }
        }
        
        /* JSON Response contains Dealer Name, its Sales Target and Sale Actuals
            
        jSONString = '[';
        map<String,Decimal> monthlyTarget = dealerWisemonthlyTargets.get(selectedMonth);
        
        for(String accId : dealerIdNameMap.keySet()){
          DealerWrapper temp = new DealerWrapper();  // By Vivek
         
            // By Vivek
            temp.dealerName = dealerIdNameMap.get(accId);
            temp.targets = monthlyTarget.get(selectedMonth+accId);
            temp.actuals = monthlyTarget.get(selectedMonth+accId);
            dealerTargets.add(temp);
            
        }
       jSONString = jSONString.substring(0, jSONString.length() -1) + ']' ;*/
    } 
   
    @RemoteAction
    global static list<DealerWrapper>  getDealerTargetActual(string month){
        
        spCalculation();
        list<DealerWrapper> wrapperList = new list<DealerWrapper>();   
       
        map<String,Decimal> tempMap1 = dealerWisemonthlyTargets.get(month);
        map<String,Decimal> tempMapMTD = dealerWisemonthlyTargetMTD.get(month);
        map<String,Decimal> tempMap = dealerWisemonthlyActuals.get(month);
        map<String,Decimal> tempAchvMap = dealerWisemonthlyAchievement.get(month);
        set<string> dealerSetTemp= mapOfMonthAndDealerIds.get(month);
        
        for(string id : dealerSetTemp){
            DealerWrapper wrap = new DealerWrapper();
            
            wrap.dealerId = id;
            wrap.dealerName = mapOfDealerIdAndName.get(id);
            if(tempMap1.get(month+id) !=null){
                wrap.targets = tempMap1.get(month+id);
            }else{
                wrap.targets = 0;
            }
            if(tempMapMTD.get(month+id) !=null){
                wrap.targetMTD = tempMapMTD.get(month+id);
            }else{
                wrap.targetMTD = 0;
            }
            if(tempMap.get(month+id) !=null){
                wrap.actuals = tempMap.get(month+id);
            }else{
                wrap.actuals = 0;
            }
            if(tempAchvMap.get(month+id) !=null){
                wrap.achievement = tempAchvMap.get(month+id);
            }else{
                wrap.achievement = 0;
            }
            
            wrapperList.add(wrap);
            
        }
        return wrapperList;
    }
    @RemoteAction
    global static list<DealerWrapper> getTargetsVsActuals() {
        spCalculation();
        return dealerTargets;
    }
    @RemoteAction
    global static list<CatgoryWrapper>  getSelectedDealersSpCategoryWise(string month,string selectedDealer){
        
        spCalculation();
        list<CatgoryWrapper> wrapperList = new list<CatgoryWrapper>();
        map<string,list<Sales_Planning__c>> dealerWiseCategoryMap = new map<string,list<Sales_Planning__c>>();
        
        map<string,Decimal> dealerCategoryTargetMap    = new map<string,Decimal>();
        map<string,Decimal> dealerCategoryTargetMapMTD = new map<string,Decimal>();
        map<string,Decimal> dealerCategoryActualMap    = new map<string,Decimal>();
        map<string,Decimal> dealerCategoryAchievementMap    = new map<string,Decimal>();
        
        list<Sales_Planning__c> spTempList;
        list<Sales_Planning__c> salesPlanningTempList = new list<Sales_Planning__c>();
        
        salesPlanningTempList = monthlySalesPlanningMap.get(month);
                
        for(Sales_Planning__c sp : salesPlanningTempList){
            if(dealerWiseCategoryMap.containsKey(sp.Dealer__c)){
                spTempList = dealerWiseCategoryMap.get(sp.Dealer__c);
                spTempList.add(sp);
                dealerWiseCategoryMap.put(sp.Dealer__c,spTempList);
            }else{
                spTempList = new list<Sales_Planning__c>();
                spTempList.add(sp);
                dealerWiseCategoryMap.put(sp.Dealer__c,spTempList);
            }
        }       
        salesPlanningTempList = dealerWiseCategoryMap.get(selectedDealer);
        Decimal targetTotal = 0;
        Decimal targetTotalMTD = 0;
        Decimal actualTotal = 0;
        Decimal achvTotal = 0;
        
        for(Sales_Planning__c spTemp : salesPlanningTempList){
             //Total Target
            if(dealerCategoryTargetMap.containsKey(spTemp.category__c)){
                targetTotal = dealerCategoryTargetMap.get(spTemp.category__c);
                if(spTemp.PlannedAspValue__c > 0 && spTemp.PlannedAspValue__c!=null){
                        targetTotal = targetTotal + spTemp.PlannedAspValue__c;
                }else{
                        targetTotal = targetTotal + 0;
                }               
                dealerCategoryTargetMap.put(spTemp.category__c,targetTotal);
            }else{
                if(spTemp.PlannedAspValue__c>0 && spTemp.PlannedAspValue__c!=null){
                            targetTotal = spTemp.PlannedAspValue__c ;
                }else{
                            targetTotal = 0 ;
                }                
                dealerCategoryTargetMap.put(spTemp.category__c,targetTotal);
            }
             //Total Target MTD
            if(dealerCategoryTargetMapMTD.containsKey(spTemp.category__c)){
                targetTotalMTD = dealerCategoryTargetMapMTD.get(spTemp.category__c);
                if(spTemp.MTD_target_Val__c > 0 && spTemp.MTD_target_Val__c!=null){
                        targetTotalMTD = targetTotalMTD + spTemp.MTD_target_Val__c;
                }else{
                        targetTotalMTD = targetTotalMTD + 0;
                }               
                dealerCategoryTargetMapMTD.put(spTemp.category__c,targetTotalMTD);
            }else{
                if(spTemp.MTD_target_Val__c>0 && spTemp.MTD_target_Val__c!=null){
                            targetTotalMTD = spTemp.MTD_target_Val__c ;
                }else{
                            targetTotalMTD = 0 ;
                }                
                dealerCategoryTargetMapMTD.put(spTemp.category__c,targetTotalMTD);
            }
            //Actual
            if(dealerCategoryActualMap.containsKey(spTemp.category__c)){
                actualTotal = dealerCategoryActualMap.get(spTemp.category__c);
                if(spTemp.Actual_Sales_Value__c > 0 && spTemp.Actual_Sales_Value__c!=null){
                        actualTotal = actualTotal + spTemp.Actual_Sales_Value__c;
                }else{
                        actualTotal = actualTotal + 0;
                }               
                dealerCategoryActualMap.put(spTemp.category__c,actualTotal);
            }else{
                if(spTemp.Actual_Sales_Value__c>0 && spTemp.Actual_Sales_Value__c!=null){
                            actualTotal = spTemp.Actual_Sales_Value__c ;
                }else{
                            actualTotal = 0 ;
                }                
                dealerCategoryActualMap.put(spTemp.category__c,actualTotal);
            }
            //Achievement
            if(dealerCategoryAchievementMap.containsKey(spTemp.category__c)){
                achvTotal = dealerCategoryAchievementMap.get(spTemp.category__c);
                if(spTemp.Achievement_MTD__c > 0 && spTemp.Achievement_MTD__c!=null){
                        achvTotal = achvTotal + spTemp.Achievement_MTD__c;
                }else{
                        achvTotal = achvTotal + 0;
                }               
                dealerCategoryAchievementMap.put(spTemp.category__c,achvTotal);
            }else{
                if(spTemp.Achievement_MTD__c>0 && spTemp.Achievement_MTD__c!=null){
                            achvTotal = spTemp.Achievement_MTD__c ;
                }else{
                            achvTotal = 0 ;
                }                
                dealerCategoryAchievementMap.put(spTemp.category__c,achvTotal);
            }
            
        }       
        for(string cat : dealerCategoryTargetMap.keySet()){
            
            CatgoryWrapper wrap = new CatgoryWrapper();
            
            wrap.catCode = cat;
            wrap.catName = mapOfCatCodeAndCatName.get(cat);
            if(dealerCategoryTargetMap.get(cat) !=null){
                wrap.target = dealerCategoryTargetMap.get(cat);
            }else{
                wrap.target = 0;
            }
            if(dealerCategoryTargetMapMTD.get(cat) !=null){
                wrap.targetMTD = dealerCategoryTargetMapMTD.get(cat);
            }else{
                wrap.targetMTD = 0;
            }
            
            if(dealerCategoryActualMap.get(cat)!=null){
                wrap.actual = dealerCategoryActualMap.get(cat);
            }else{
                wrap.actual = 0;
            }
            if(dealerCategoryAchievementMap.get(cat)!=null){
                wrap.achievement    = dealerCategoryAchievementMap.get(cat);
            }else{
                wrap.achievement = 0;
            }
            
            wrapperList.add(wrap);  
        }       
        return wrapperList;
    }
}