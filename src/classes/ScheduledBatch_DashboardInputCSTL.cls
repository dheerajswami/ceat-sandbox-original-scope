global class ScheduledBatch_DashboardInputCSTL implements Schedulable{
   global void execute(SchedulableContext sc) {
      DashboardInputCSTL_BatchClass dashboardCSTLBatch = new DashboardInputCSTL_BatchClass(); 
      database.executebatch(dashboardCSTLBatch,1);
   }
}