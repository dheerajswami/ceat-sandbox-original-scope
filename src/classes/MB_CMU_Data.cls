/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By:
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchCMUData/*')

global with sharing class MB_CMU_Data {
    
    
    global class MobileData{
        public List<Account> accounts {get;set;}
        public List<Customer_Location__c> custLocation {get;set;}
        public List<Contact> contacts {get;set;}
        
        public MobileData(){
            accounts = new List<Account>();
            custLocation = new List<Customer_Location__c>();
            contacts = new List<Contact>();
        }
    }
    
    global class DataReturn{
        public List<Account> accounts {get;set;}
        public List<Contact> contacts {get;set;}
        public List<State_Master__c> picklistValues {get;set;}
        public String timeStamp {get;set;}
        
        public DataReturn(){
            accounts = new List<Account>();
            contacts = new List<Contact>();
            picklistValues = new List<State_Master__c>();
        }
    }
    
    @HttpPost
    global static DataReturn getAccountListAndUpdate(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response; 
        // Check time stamp to fetch only updated data
        String time_stamp = req.params.get('TimeStamp');
        DateTime appDateString = UtilityClass.correctDateTime(time_stamp);
        system.debug(appDateString);
        
        List<Account> allAccountsToSend = new List<Account>();
        List<Contact> contactsToSend = new List<Contact>();
        List<State_Master__c> statePickList = new List<State_Master__c>();

        DataReturn dt = new DataReturn();
        
        try{
            MobileData accountsToUpdate = new MobileData();
            //system.debug(req.requestBody.toString());
            accountsToUpdate = (MobileData)JSON.deserialize(req.requestBody.toString(),MobileData.class);
            
            update accountsToUpdate.accounts;
            insert accountsToUpdate.custLocation;
            insert accountsToUpdate.contacts;
            system.debug(accountsToUpdate);


            if(appDateString == null){
                allAccountsToSend = [SELECT Id,UniqueIdentifier__c,No_of_location_captured__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,Street_1__c,Street_2__c,State__c,Address__c,(SELECT Id,AccountId,Offline_Id__c,Account.Name,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
                for(Account a : allAccountsToSend){
                    if(a.Contacts != null)
                    contactsToSend.addAll(a.Contacts);
                }

                statePickList = getAllStateMaster(UserInfo.getUserId(),appDateString);

            }else{
                allAccountsToSend = [SELECT Id,UniqueIdentifier__c,No_of_location_captured__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,Street_1__c,Street_2__c,State__c,Address__c FROM Account WHERE LastModifiedDate > :appDateString order by Name];
                contactsToSend = [SELECT Id,Offline_Id__c,AccountId,Account.Name,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contact WHERE LastModifiedDate >:appDateString ];
                statePickList = getAllStateMaster(UserInfo.getUserId(),appDateString);
            }
            
            
            
            //allAccountsToSend = [SELECT Id,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
            dt.accounts = allAccountsToSend;
            dt.contacts = contactsToSend;
            dt.picklistValues = statePickList;
            dt.timeStamp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            system.debug('RETURN '+dt);
            return dt;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            
            if(appDateString == null){
                allAccountsToSend = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,Street_1__c,Street_2__c,State__c,Address__c,(SELECT Id,AccountId,Offline_Id__c,Account.Name,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
                for(Account a : allAccountsToSend){
                    if(a.Contacts != null)
                    contactsToSend.addAll(a.Contacts);
                }

                statePickList = getAllStateMaster(UserInfo.getUserId(),appDateString);

            }else{
                allAccountsToSend = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,Street_1__c,Street_2__c,State__c,Address__c FROM Account WHERE LastModifiedDate > :appDateString order by Name];
                contactsToSend = [SELECT Id,Offline_Id__c,AccountId,Account.Name,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contact WHERE LastModifiedDate >:appDateString ];
                statePickList = getAllStateMaster(UserInfo.getUserId(),appDateString);
            }
            
            
            
            //allAccountsToSend = [SELECT Id,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
            dt.accounts = allAccountsToSend;
            dt.contacts = contactsToSend;
            dt.picklistValues = statePickList;
            dt.timeStamp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            system.debug('RETURN '+dt);
            return dt;
        }
    }
    
    // Fetch All State Values
    global static List<State_Master__c> getAllStateMaster(String uId,DateTime dt){
        
        Set<String> territoryNames = new Set<String>();
        Set<String> parentTerritoryId = new Set<String>();
        Set<String> ro_Names = new Set<String>();
        for(UserTerritory2Association ul :[SELECT Id,IsActive,RoleInTerritory2,Territory2Id,Territory2.Name,UserId,User.Name FROM UserTerritory2Association WHERE UserId = :uId] ) {
            territoryNames.add(ul.Territory2.Name);
        }
        
        for(Territory2 tl : [SELECT Id,AccountAccessLevel,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId FROM Territory2 WHERE DeveloperName IN : territoryNames]){
            parentTerritoryId.add(tl.ParentTerritory2Id);
        }
        
        for(Territory2 tl : [SELECT Id,AccountAccessLevel,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId FROM Territory2 WHERE Id IN : parentTerritoryId]){
            ro_Names.add(tl.Name);
        }
        
        if(dt == null){
            return [SELECT Id,Town__c,District__c,State__c FROM State_Master__c WHERE RO__c IN : ro_Names ];
        }else{
            return [SELECT Id,Town__c,District__c,State__c FROM State_Master__c WHERE RO__c IN : ro_Names AND LastModifiedDate >:dt];
        }

    }
}