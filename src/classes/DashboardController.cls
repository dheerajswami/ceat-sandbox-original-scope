public class DashboardController{  
    
    public Boolean isCstl                    {get;set;}
    public Boolean isRm                      {get;set;}
    public Boolean isZgm                     {get;set;}
    
    public Boolean isNotCstl                 {get;set;}
    public Boolean guestUser                 {get;set;}    
    public static PageReference page         {get;set;}
    
    public String loggedInUserId             {get;set;}
    public String loggedInUserRole           {get;set;}
    
    public static String rmRole         = system.label.RM_Role;
    public static String tlRole         = system.label.TL_Role;
    public static String tldRole        = system.label.TLD_Role;
    String cstlRole                     = system.Label.CSTL_Role;  
    
    public Double totalInputScore         {get;set;}
    public Double totalOutputScore        {get;set;}
    
    public DashboardController(){        
        
        isCstl                  = false;
        isNotCstl               = false;
        isRm                    = false;
        guestUser               = true;
        isZgm                   = false;
        
        Integer month                   = Date.today().month();
        //String currentMonth             = monthYearMap.get(month);
        String currentYear              = String.valueOf((Date.today()).year());        
        
        /* Getting Logged in User Name and Territory */
        loggedInUserId   = UserInfo.getUserId();
        
        user loggin      = [SELECT id,name,FirstName,UserRole.Name From User Where id =:loggedInUserId];        
        loggedInUserRole = loggin.UserRole.Name;
        if(loggedInUserRole.contains('RM')){
             isCstl = false;
             isRm = true;
             isNotCstl = false;
             guestUser = false;
             loggedInUserRole = 'RM';           
        }else if(loggedInUserRole.contains('TL') && (!loggedInUserRole.contains('TLD')) && (!loggedInUserRole.contains('CSTL'))){
             loggedInUserRole = 'TL';
             isCstl = false;
             isRm = false;
             isNotCstl = true;
             guestUser = false;         
        }else if(loggedInUserRole.contains('TLD')){
             loggedInUserRole = 'TLD';  
             isCstl = false;
             isRm = false;
             isNotCstl = true;
             guestUser = false;     
        }else if(loggedInUserRole.contains('CSTL')){
            loggedInUserRole = 'CSTL';
             isCstl    = true;
             isNotCstl = false;
             isRm      = false;
             guestUser = false;         
        }else if(loggedInUserRole.contains('ZGM')){
            loggedInUserRole = 'ZGM';
             isZgm     = true; 
             isCstl    = false;
             isNotCstl = false;
             isRm      = false;
             guestUser = false;         
        }        
        
        list<Dashboard_Summary_Score__c> dashSummaryScore = new list<Dashboard_Summary_Score__c>();        
        
        if(!guestUser){                 
             // externalId = loggedInUserTerritory + month + currentYear ;                 
              dashSummaryScore    = [SELECT Total_Output_Score__c,Region_Code__c,ID,DSS_External_ID__c,Score__c,Input_Score__c,Territory_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE OwnerId =:loggedInUserId AND Month__c =:string.valueof(month) AND Year__c=:currentYear  limit 1];
        }
        /* Getting TL/TLD/RM/CSTL User's Total Input / Output Dashboard Score */
        if(dashSummaryScore.size()>0){
            if(isNotCstl == true || isRm == true || isZgm==true){
                if(dashSummaryScore[0].Input_Score__c == null){
                    totalInputScore = 0;
                }else{
                    totalInputScore = dashSummaryScore[0].Input_Score__c;
                }
                if(dashSummaryScore[0].Total_Output_Score__c == null){
                    totalOutputScore = 0;
                }else{
                    totalOutputScore = dashSummaryScore[0].Total_Output_Score__c;
                }               
                
            }
            if(isCstl == true){
                if(dashSummaryScore[0].Input_Score__c == null){
                    totalInputScore = 0; 
                }else{
                    totalInputScore = dashSummaryScore[0].Input_Score__c; 
                }               
            }
        }else{
                totalInputScore = 0;
                totalOutputScore = 0;
        }
    }
    
    @RemoteAction
    Public Static PageReference getPage(string loggedInUserRole,string selectedRole,string selectedUserRole,string month, string year) { 
       
       //--------------- Roles -----------------        
          String loggedInUserId = UserInfo.getUserId();      
          Server_Url__c serverUrl;
          string role  = '';
        
         if((selectedRole == 'All' && selectedUserRole != 'none') || (selectedRole != 'All' && selectedUserRole != 'none')){
             User u = [select id,name,UserRole.DeveloperName from User where id =:selectedUserRole limit 1];
             role = u.UserRole.DeveloperName ;
         }
         
         serverUrl=[select url__c from Server_Url__c limit 1];
        
               /* If loggedin user is RM, it should redirect to RM Dashboard page */
                    if(role.contains('ZGM')  || (loggedInUserRole == 'ZGM' && selectedUserRole == 'none' && selectedRole !='ZGM' && selectedRole =='All')){
                            page = new PageReference(serverUrl.url__c+'/apex/'+Constants.zgmDashboardPage);   
                                                                          
                    }else if(role.contains('RM')  || (loggedInUserRole == 'RM' && selectedUserRole == 'none' && selectedRole !='RM' && selectedRole =='All')){
                            page = new PageReference(serverUrl.url__c+'/apex/'+Constants.rmDashboardPage);   
                                                                          
                    }else if(((selectedRole =='TLD' || selectedRole =='TL') && role.contains('TL') && (!role.contains('TL'))) || (loggedInUserRole == 'TL' || loggedInUserRole == 'TLD') || ((role.contains('TL') || role.contains('TLD')) && (!role.contains('CSTL')) )){   
                       
                       /* If loggedin user is TL / TLD , it should redirect to TL/TLD Dashboard page */
                            page = new PageReference(serverUrl.url__c+'/apex/'+Constants.tlDashboardPage);                                 
                            
                    }else if((role.contains('CSTL') && selectedRole =='CSTL') || (loggedInUserRole == 'CSTL') || role.contains('CSTL')){                        
                    
                     /* If loggedin user is CSTL, it should redirect to CSTL Input Dashboard page */
                        page = new PageReference(serverUrl.url__c+'/apex/'+Constants.cstlDashboardPage);  
                                                                      
                   }else{
                      //else It should redirect to home page
                        page = new PageReference(serverUrl.url__c+'/home/home.jsp');                    
                   }
       return page;
    } 
    
    // Getting all subordinates under logged in user
    @RemoteAction
    Public Static list<RoleWrapperClass> fetchUsers(string selectedRole) { 
        Id loggedInUserId   = UserInfo.getUserId();
        list<RoleWrapperClass> RoleWrapperClassList    = new list<RoleWrapperClass>();        
        list<UtilityClass.RoleWrapper> RoleWrapperList = new list<UtilityClass.RoleWrapper>();
        
        RoleWrapperList = UtilityClass.getRoleSubordinateUsers(loggedInUserId);
        system.debug(RoleWrapperList+'RoleWrapperList');
        if(RoleWrapperList.size()>0){           
            for(UtilityClass.RoleWrapper rl :RoleWrapperList){   
                           
                if(selectedRole=='TL' && rl.role.contains(selectedRole) && (!rl.role.contains('CSTL')) && (!rl.role.contains('TLD'))){
                    RoleWrapperClass wrapper = new RoleWrapperClass();
                    wrapper.role = rl.userName;                  
                    wrapper.userId = rl.userId;
                    RoleWrapperClassList.add(wrapper);
                }else if(rl.role.contains(selectedRole) && selectedRole!='TL'){
                    RoleWrapperClass wrapper = new RoleWrapperClass();
                    wrapper.role = rl.userName;                  
                    wrapper.userId = rl.userId;
                    RoleWrapperClassList.add(wrapper);
                }                
                if(selectedRole=='All'){
                    RoleWrapperClass wrapper = new RoleWrapperClass();
                    wrapper.role = rl.userName;                  
                    wrapper.userId = rl.userId;
                    RoleWrapperClassList.add(wrapper);
                }
            }
        }
        RoleWrapperClassList.sort();
        return RoleWrapperClassList ;
    }
    public class RoleWrapperClass implements Comparable{
          public string role {get;set;}
          public Id userId {get;set;}
          
          public Integer compareTo(Object compareTo) {
            RoleWrapperClass compareToCat = (RoleWrapperClass)compareTo;
            if (role == compareToCat.role) return 0;
            if (role > compareToCat.role) return 1;
            return -1;        
        }
    }
   
}