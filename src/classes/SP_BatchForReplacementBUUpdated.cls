/* ================================================
    @Name:  SP_BatchForReplacementBUUpdated
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor tquila
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will run 1st of the month and create dealer wise sales planning records                                                                                               
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     22/07/2014      INITIAL DEVELOPMENT                                 
     2.0        Kishlay@extentor     10/02/2016      remove territory from external id of sp records 
     3.0        kishlay@extentor     29/02/2016      territory and region details taking from dealer lookup rather then staging records and added filter in                                              account query 
======================================================================= */


global class SP_BatchForReplacementBUUpdated implements Database.Batchable<sObject> {  
    String replacement              = System.Label.Replacement;
    String Forcast                  = System.Label.Forcast; 
    Static String REPLECMENT        ='Replacement';
    String repROLabel               = System.Label.Replacement_RO;
    String repDealerLabel           = System.Label.Replacement_Dealer;
    String tlRole                   = system.Label.TL_Role;
    String tldRole                  = system.Label.TLD_Role;
    List<Sales_Planning__c> replacementDealer = new List<Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOFDealerCatWiseRep = new map<String,Sales_Planning__c>();
    Set<String> catRepSet = new set<String>();
    List<Sales_Planning__c> salesPlanningRepForcast = new List<Sales_Planning__c>();
    List<Account> ListOfAccount = new list<account>();
    list<UserTerritory2Association> terrUserList;
    map<String,Sales_Planning__c> mapOfForcastAndTarget = new map<String,Sales_Planning__c>();
    Map<String,Account> mapOFcusNumAndID = new Map<String,Account>();
    Map<String,id> maoOfUserAndTerr;
    set<String>catsetKey = new set<String>();
    Date d = system.today();
    String month = String.valueOf(d.month());
    String year = String.valueOf(d.year());
    Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
    global SP_BatchForReplacementBUUpdated(){
        replacementDealer           = new list<Sales_Planning__c>();
        mapOFDealerCatWiseRep       = new map<String,Sales_Planning__c>();
        catRepSet                   = new set<String>();
        terrUserList                = new list<UserTerritory2Association>();
        salesPlanningRepForcast     = new List<Sales_Planning__c>();
        mapOfForcastAndTarget       = new map<String,Sales_Planning__c>();
        maoOfUserAndTerr = new Map<String,ID>();
        String keyfor = '';
        Id replaceROId      = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        list<Sales_Planning_Categories__c> spc = Sales_Planning_Categories__c.getall().values();
        for(Sales_Planning_Categories__c sp:spc){
            if(sp.Include_in_Sales_Planning__c == true){  
                catRepSet.add(sp.Category_Code__c); 
            }       
        }
        /*
        //getting all current month RO records for logged in user 
        salesPlanningRepForcast = [SELECT Id,ASP__c,Budget__c,Dealer__c,Territory__c,RecordTypeId,Region__c,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Year__c =:year AND Month__c =:month AND RecordTypeId =: replaceROId AND Category__c In :catRepSet];
        system.debug(salesPlanningRepForcast.size()+'salesPlanningRepForcast');
        for(Sales_Planning__c sprForcast : salesPlanningRepForcast){
            keyfor = sprForcast.Category__c+sprForcast.Region_code__c;
            if(!mapOfForcastAndTarget.containskey(keyfor)){
                mapOfForcastAndTarget.put(keyfor,sprForcast);
            }
        }*/
        //**********************Getting territory code of each user**********************
            terrUserList = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 = :tlRole OR RoleInTerritory2 =:tldRole];
            if(terrUserList.size()>0){
                for(UserTerritory2Association ut: terrUserList){
                    maoOfUserAndTerr.put(ut.Territory2.name,ut.UserId);                 
                }
            }
            system.debug('maoOfUserAndTerr--'+maoOfUserAndTerr);
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        String filter = '%EMP%';
        query = 'SELECT Id,OwnerId,ASP__c,Budget__c,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,'
        +'Territory_Code__c,NBP__c,Year__c,Reg_code__c,SPExternalIDTL__c,BU__c,SKU__c,MAKTX__c,Zone__c,Zone_Description__c,Value__c,Value_L3M__c,Value_LYCM__c '+
        'FROM Sales_Planing_Staging__c WHERE  BU__c = \''+replacement+'\' AND Category__c IN: catRepSet AND (NOT Dealer_CustNumber__c  like: filter) AND Processed__c = false AND Year__c =\''+year+'\' AND Month__c =\''+month+'\'' ;
        System.debug(Database.getQueryLocator(query)+'@@@query@@@@');
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        String month;
        String year;
        Date d = system.today();
        month = String.valueOf(d.month());
        year = String.valueOf(d.Year());
        Sales_Planning__c spReplacementCatWiseMap = new Sales_Planning__c();
        Sales_Planning__c spReplacementCatWise;
        Account dealer;
        list<Sales_Planing_Staging__c> updateStaging = new list<Sales_Planing_Staging__c>();
        String keyCatRegDea = '';
        ListOfAccount = [SELECT id,name,KUNNR__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c From Account WHERE (NOT KUNNR__c  like '6%') AND KUNNR__c != null ];
        for(Account acc : ListOfAccount){
            mapOFcusNumAndID.put(acc.KUNNR__c,acc);
        }
        for(sObject temp : scope){
            Sales_Planing_Staging__c spr =(Sales_Planing_Staging__c)temp;
            //if(mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).Sales_Group_Text__c != null){
                //keyCatRegDea = spr.Category__c+mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).Sales_Group_Text__c+spr.Dealer_CustNumber__c;
                //catsetKey.add(keyCatRegDea);
            //}else{
                keyCatRegDea = spr.Category__c+spr.Reg_code__c+spr.Dealer_CustNumber__c;
                catsetKey.add(keyCatRegDea);
            //}
            
        }
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Region_code__c ,Dealer__r.KUNNR__c,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch__c IN : catsetKey AND RecordTypeId =: replaceDealerId AND Month__c =: month AND Year__c =: year ] ){
        mapOFDealerCatWiseRep.put(salesPlan.SYS_Used_IN_Batch__c,salesPlan ); 
        }
        
        String keyCatRegDealer = '';
        Id userId ;
        for(sObject temp : scope) {
            Sales_Planing_Staging__c sp =(Sales_Planing_Staging__c)temp;
            //if(mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_Group_Text__c != null){
                //keyCatRegDealer = sp.Category__c+mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_Group_Text__c+sp.Dealer_CustNumber__c;
            //}else{
                keyCatRegDealer = sp.Category__c+sp.Reg_code__c+sp.Dealer_CustNumber__c;
            //}
            if(catRepSet.contains(sp.Category__c) && sp.Category__c<>null && sp.Region_code__c<>null ){
                
                if(mapOFDealerCatWiseRep.containsKey(keyCatRegDealer)){
                    System.debug('====keyCatRegDealer:'+keyCatRegDealer);
                    spReplacementCatWiseMap = new Sales_Planning__c();
                    spReplacementCatWiseMap = mapOFDealerCatWiseRep.get(keyCatRegDealer);
                    if(maoOfUserAndTerr.get(mapOFcusNumAndID.get(spReplacementCatWiseMap.Dealer__r.KUNNR__c).Sales_District_Text__c)!=null){
                                userId = maoOfUserAndTerr.get(mapOFcusNumAndID.get(spReplacementCatWiseMap.Dealer__r.KUNNR__c).Sales_District_Text__c);
                    }else{
                                userId = UserInfo.getUserId();
                    }
                    if(spReplacementCatWiseMap.L3M__c ==null)spReplacementCatWiseMap.L3M__c=0;
                    if(spReplacementCatWiseMap.LYCM__c ==null)spReplacementCatWiseMap.LYCM__c=0;
                    if(spReplacementCatWiseMap.Value_LYCM__c ==null)spReplacementCatWiseMap.Value_LYCM__c=0;
                    if(spReplacementCatWiseMap.Value_L3M__c ==null)spReplacementCatWiseMap.Value_L3M__c=0;
                    spReplacementCatWiseMap.L3M__c += (sp.L3M__c>0 && sp.L3M__c != null ?sp.L3M__c:0);
                    spReplacementCatWiseMap.LYCM__c += ((sp.LYCM__c != null && sp.LYCM__c>0) ?sp.LYCM__c:0);
                    spReplacementCatWiseMap.Value_LYCM__c += (sp.Value_LYCM__c>0 && sp.Value_LYCM__c != null ?sp.Value_LYCM__c:0);
                    spReplacementCatWiseMap.Value_L3M__c += (sp.Value_L3M__c>0 && sp.Value_L3M__c != null ?sp.Value_L3M__c:0);
                    spReplacementCatWiseMap.OwnerId = userId;
                    spReplacementCatWiseMap.Region_code__c = sp.Reg_code__c;
                    spReplacementCatWiseMap.Region_Description__c = sp.Region_Description__c;
                    spReplacementCatWiseMap.Dealer_CustNumber__c = spReplacementCatWiseMap.Dealer__r.KUNNR__c;
                    spReplacementCatWiseMap.Zone_Description__c = sp.Zone_Description__c;
                    spReplacementCatWiseMap.Dealer_Name__c =  sp.Dealer_Name__c;
                    spReplacementCatWiseMap.Category_Description__c = sp.Category_Description__c;
                    system.debug('==========<<<<<<<< '+sp.id+'----'+sp.Dealer_CustNumber__c+'========== '+sp.Category__c+'-'+sp.Region_code__c +'<=========>' +sp.L3M__c+'<----->'+sp.LYCM__c);
                    system.debug('==========>>>>>>> '+spReplacementCatWiseMap.id+'------'+spReplacementCatWiseMap.Dealer_CustNumber__c+'========== '+spReplacementCatWiseMap.Category__c+'-'+spReplacementCatWiseMap.Region_code__c +'<=========>' +spReplacementCatWiseMap.L3M__c+'<----->'+spReplacementCatWiseMap.LYCM__c);
                    mapOFDealerCatWiseRep.put(keyCatRegDealer,spReplacementCatWiseMap);
                }else{
                            if(mapOFcusNumAndID.containskey(sp.Dealer_CustNumber__c)){
                                if(maoOfUserAndTerr.get(mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_District_Text__c)!=null){
                                userId = maoOfUserAndTerr.get(mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_District_Text__c);
                                }
                            }
                            else{
                                userId = UserInfo.getUserId();
                            }
                            if(mapOFcusNumAndID.containskey(sp.Dealer_CustNumber__c)){
                                dealer = new Account (KUNNR__c = sp.Dealer_CustNumber__c);
                                spReplacementCatWise = new Sales_Planning__c(
                                Region_code__c = sp.Reg_code__c,
                                Region_Description__c = sp.Region_Description__c,
                                Category__c = sp.Category__c,
                                Category_Description__c = sp.Category_Description__c,
                                Month__c = sp.Month__c,Year__c = sp.Year__c,Zone__c = mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_Office_Text__c,
                                Territory_Code__c = mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).Sales_District_Text__c,
                                OwnerId=userId,
                                Dealer_CustNumber__c = sp.Dealer_CustNumber__c,
                                Dealer_Name__c =  sp.Dealer_Name__c,Dealer__r = dealer,
                                BU__c = sp.BU__c,Value_L3M__c = sp.Value_L3M__c,
                                Zone_Description__c = sp.Zone_Description__c,
                                L3M__c = sp.L3M__c,LYCM__c = sp.LYCM__c,
                                Value_LYCM__c = sp.Value_LYCM__c,
                                SPExternalIDTL__c = mapOFcusNumAndID.get(sp.Dealer_CustNumber__c).id+sp.Category__c+sp.Month__c+sp.Year__c,
                                UploadExternalID__c = sp.Dealer_CustNumber__c+sp.Category__c+sp.Month__c+sp.Year__c,
                                RecordTypeId = replaceDealerId
                                );
                                mapOFDealerCatWiseRep.put(keyCatRegDealer,spReplacementCatWise);
                            }     
                    }
                    
                }
            }
            if(mapOFDealerCatWiseRep.size() > 0){
                replacementDealer = new list<Sales_Planning__c>();
                for(Sales_Planning__c temp : mapOFDealerCatWiseRep.values()){
                    replacementDealer.add(temp);
                }
            }
            if(replacementDealer.size() > 0){
                try{
                  database.upsert (replacementDealer,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
                  }catch(Exception e){
                      System.debug(e.getMessage());
                  }
            }
            for(sObject temp : scope){
                Sales_Planing_Staging__c sps =(Sales_Planing_Staging__c)temp;
                sps.Processed__c = true;
                updateStaging.add(sps);
            }
            if(updateStaging.size() > 0){
                update updateStaging;
            }
    }
    global void finish(Database.BatchableContext BC){
        /*
        for(Sales_Planning__c s : mapOFDealerCatWiseRep.values()){
            system.debug('========== '+s.Dealer_CustNumber__c+'========== '+s.Category__c+'-'+s.Region_code__c +'<=========>' +s.L3M__c+'<----->'+s.Value_L3M__c+'<----->'+s.LYCM__c+'<----->'+s.Value_LYCM__c);
        }
        
        system.debug(replacementDealer.size()+'replacementDealer$$$$');
        
        system.debug(mapOfForcastAndTarget.size()+'mapOfForcastAndTarget');
        for(Sales_Planning__c s : mapOfForcastAndTarget.values()){
            system.debug('========== '+s.Target_Quantity__c+'========== '+s.Category__c+'-'+s.Region_code__c );
        }
            //system.debug(ex.size()+'@@@');
        */
    }

}