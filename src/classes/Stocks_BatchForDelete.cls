/* ================================================
@Name:     Stocks_BatchForDelete
@Purpose:  Batch Class To Delete All The Previous Day's Stocks
@Author:   Supriya Chakrapani
@=======================================================  */




         global with sharing class Stocks_BatchForDelete implements Database.Batchable<sObject>{

        
         
         global Stocks_BatchForDelete (){}
            
         global Database.QueryLocator start(Database.BatchableContext BC){
            
            String query='';
            if(Test.isRunningTest()){
            query='select id from Stocks__c ';
            }
            else{
            query='select id from Stocks__c where  CreatedDate <Today';
            }
            return Database.getQueryLocator(query);  
          } 
          
         global void execute(Database.BatchableContext BC, List<SObject> scope) {
            
            list<Stocks__c> stockListToDelete = new list<Stocks__c>();
            if(scope.size()>0){
                for(SObject stocksDel : scope){
                Stocks__c st1= (Stocks__c)stocksDel ;
                stockListToDelete.add(st1);
                }
            }
            if(stockListToDelete.size()>0){
            delete stockListToDelete;
            }
         }  
    
         global void finish(Database.BatchableContext BC) {}



        }