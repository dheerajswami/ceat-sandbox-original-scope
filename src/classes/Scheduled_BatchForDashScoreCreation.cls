global class Scheduled_BatchForDashScoreCreation implements Schedulable{
   @TestVisible private integer batchsize = 1000; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest())
      {
           batchsize = 2000;
      } 
      Sap_BatchForDashboardScoreCreation dashBatch= new Sap_BatchForDashboardScoreCreation(); 
      database.executebatch(dashBatch,batchsize);
   }
}