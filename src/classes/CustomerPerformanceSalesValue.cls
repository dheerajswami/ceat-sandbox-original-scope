public class CustomerPerformanceSalesValue {

   
    public List<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesData {get;set;}
    public static String jSONString{get;set;}
    private Account acct;
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    public String currentYr{get;set;}
    public String previousYr {get;set;}
    public static String selectedCategory{get;set;}
    private Date currentYrStart;
    static Map<String, list<Decimal>> monthlyCategoryValues {get;set;} 
    List<Decimal> valuesToDisplay = new List<Decimal>{0,0,0,0};
    static List<String> months = new List<String>{'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb','Mar'};
    public List<String> categoriesList {get;set;}
    
    
    public CustomerPerformanceSalesValue(ApexPages.StandardController stdController){
        jSONString='';
        this.acct = (Account)stdController.getRecord();
        String sapCustomerNumber = [select KUNNR__c from Account where Id = :acct.Id].KUNNR__c;
        Map<String,Decimal> categoryValues = new Map<String,Decimal>();         
        
        String customerId = UtilityClass.addleadingZeros(sapCustomerNumber,Integer.valueOf(CUST_ID_LENGTH));
        String fromDate,toDate;
          
        if(Date.today().month() < 4){
            fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-2,4,1));
            previousYr = String.valueOf(Date.today().year()-2) + ' - ' + String.valueOf(Date.today().year()-1);
            currentYr = String.valueOf(Date.today().year()-1) + ' - ' + Date.today().year();
            currentYrStart = Date.newInstance(Date.today().year()-1,4,1);
        }
        else{
            fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-1,4,1));
            previousYr = String.valueOf(Date.today().year()-1) + ' - ' + Date.today().year();
            currentYr = Date.today().year() + ' - ' + String.valueOf(Date.today().year()+1);
            currentYrStart = Date.newInstance(Date.today().year(),4,1);
        }        
        toDate =  UtilityClass.getStringDate(Date.today());
        calculateYTDValues(customerId,fromDate,toDate);
    }
    
    void calculateYTDValues(String customerId, String fromDate, String toDate){        
        if(monthlyCategoryValues == null){
           monthlyCategoryValues = new Map<String,List<Decimal>>();
        }
        monthlyCategoryValues.put('Apr',new List<Decimal>() );
        monthlyCategoryValues.put('May',new List<Decimal>()  );
        monthlyCategoryValues.put('Jun',new List<Decimal>()  );
        monthlyCategoryValues.put('Jul',new List<Decimal>()  );
        monthlyCategoryValues.put('Aug',new List<Decimal>()  );
        monthlyCategoryValues.put('Sep',new List<Decimal>() );
        monthlyCategoryValues.put('Oct',new List<Decimal>()  );
        monthlyCategoryValues.put('Nov',new List<Decimal>()  );
        monthlyCategoryValues.put('Dec',new List<Decimal>()  );
        monthlyCategoryValues.put('Jan',new List<Decimal>()  );
        monthlyCategoryValues.put('Feb',new List<Decimal>()  );
        monthlyCategoryValues.put('Mar',new List<Decimal>() );
       
        Set<String> uniqueMonths = new Set<String>();
        Set<Date> uniqueMonthValues = new Set<Date>();
        List<Date> sortedMonths = new List<Date>();
            

        for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesRecord: CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fromDate,toDate)){
            //system.debug('salesRecord----'+salesRecord);
           
            Date monthOfSale = UtilityClass.getDateFromString(salesRecord.fkDate).toStartOfMonth();
            Integer monthNumber = UtilityClass.getDateFromString(salesRecord.fkDate).month();
            String month = (monthNumber == 1)?'Jan':(monthNumber == 2)?'Feb':(monthNumber == 3)?'Mar':(monthNumber == 4)?'Apr':(monthNumber == 5)?'May':(monthNumber == 6)?'Jun':(monthNumber == 7)?'Jul':(monthNumber == 8)?'Aug':(monthNumber == 9)?'Sep':(monthNumber == 10)?'Oct' : (monthNumber == 11)?'Nov' : 'Dec';
           // system.debug('month----'+month);
            
            List<Decimal> actualValues = monthlyCategoryValues.get(month);
           // if(actualValues == null){
                actualValues = new List<Decimal>{0,0,0,0};
                //actualValues.addAll(new List<Decimal>{0,0,0,0});
                monthlyCategoryValues.put(month, actualValues);
            //}
            if(monthOfSale < currentYrStart){
                actualValues[0] += salesRecord.invoiceValue;
                actualValues[2] += Decimal.valueOf(salesRecord.fkimg);
            }
            else{
                actualValues[1] += salesRecord.invoiceValue;
                actualValues[3] += Decimal.valueOf(salesRecord.fkimg);
            }
           
            monthlyCategoryValues.put(month, actualValues);
           // system.debug('monthlyCategoryValues----'+monthlyCategoryValues);
        }
        
       jSONString = '[';
        for(String mon : months){
            valuesToDisplay = monthlyCategoryValues.get(mon);
            //List<Decimal> valuesToDisplay ;
            system.debug('monthlyCategoryValues.get(mon) ===--'+monthlyCategoryValues);
            if(valuesToDisplay == null || valuesToDisplay.size()==0){
                valuesToDisplay = new List<Decimal>{0,0,0,0};
                system.debug('valuesToDisplay=--'+valuesToDisplay);
            }
            else{
                valuesToDisplay = monthlyCategoryValues.get(mon);
                system.debug('---valuesToDisplay=--'+valuesToDisplay+'=========='+mon);
            }
            //system.debug('---valuesToDisplay=--'+valuesToDisplay+'=========='+mon);
            jSONString += '[\'' + mon + '\', ' + valuesToDisplay[0] + ', ' + valuesToDisplay[1] + '],';
        }
        jSONString = jSONString.substring(0, jSONString.length() -1) + ']' ;
          system.debug('---jSONString----'+jSONString);  
          
    }    
}