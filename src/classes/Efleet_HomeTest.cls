@isTest
public class Efleet_HomeTest {
    
    static User init(){
        User tlUser = CEAT_InitializeTestData.createUser('Admin', 'Admin', 'admin@adminceat.com', 'admin@adminceat.com', '100098776', 'Replacements', null, 'CSTL');
        insert tlUser;     
        
        Territory2Model terrModel = CEAT_InitializeTestData.createTerritoryModel('Model', 'India');
        insert terrModel;
        Id terrTypeTerr;
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');        
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            
        }
        
        Territory2 cstlTerr = CEAT_InitializeTestData.createTerritory('DI007', 'DI007' , terrTypeTerr, terrModel.Id, null);
        insert cstlTerr;
        UserTerritory2Association utass =  CEAT_InitializeTestData.createUserTerrAsso(tlUser.Id, cstlTerr.Id, 'CSTL');
        insert utass;
        return tlUser;
    }        
    
    static testmethod void testEfleet() {
        User tlUser=Efleet_HomeTest.init();
        Test.startTest();
        System.runAs(tlUser) {
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', '5000234509');
            insert acc1;
            Account acc2 = CEAT_InitializeTestData.createAccount('Acc2', '5000234508');
            Contact con = CEAT_InitializeTestData.createContact('Con', acc1.Id);
            insert con;
            Vehicle__c veh = CEAT_InitializeTestData.createVehicle('On Road', 'Normal Load', 'Truck', '14', acc1.Id); 
            veh.Wheel_Type__c = '4W';
             
            Item_Master__c itemMaster = CEAT_InitializeTestData.createMaterialMaster('10.00-20', 'CEAT', 'HCL', 12000, 15, 'LUG', 'Bias');
            Item_Master__c itemMaster1 = CEAT_InitializeTestData.createMaterialMaster('10.00-20', 'CEAT', 'HCL-1', 12000, 15, 'LUG', 'Bias');
            List<Item_Master__c> itemMasterList = new List<Item_Master__c>();
            itemMasterList.add(itemMaster);
            itemMasterList.add(itemMaster1);
            insert itemMasterList;
            State_Master__c stateMaster = CEAT_InitializeTestData.createStateMaster('Karnataka', 'Bangalore', 'Koramangala');
            insert stateMaster;
            Haul_Type1__c haulMaster = CEAT_InitializeTestData.createHaulType('Short', 100);
            insert haulMaster;
            Payload_Type1__c payloadMaster = CEAT_InitializeTestData.createPayload('Payload', '6W', 100, 200, 300, 400);
            insert payloadMaster;
            
            Fleet_Vehicle_Information__c vehInfo = CEAT_InitializeTestData.createVehicleInfo(acc1.Id);
            insert vehInfo;
            Scrap_Yard_Report__c scrapReport = CEAT_InitializeTestData.createScrapReport(acc1.Id);
            insert scrapReport;
            Fleet_Operation__c fleetOperation = CEAT_InitializeTestData.createOperation(acc1.Id);
            insert fleetOperation;
            Recommended_Tyre__c recomTyre = CEAT_InitializeTestData.createRecommendedTyre(fleetOperation.Id);
            insert recomTyre;
            Truck_Tyre_Matrix__c ttMatrix = CEAT_InitializeTestData.createTruckTyreMatrix('Truck', '6W', 'Bias', 'On Road', 'Normal Load', 'Short', '10.00-20', 'HCL', 'HCL-1');
            insert ttMatrix;    
            //Efleet_HomeController.getNBP('7.00-16','CEAT','BULAND Mile XL');
            //Efleet_HomeController.fetchOtherVehical('a19O0000001yb9iIAA','001O000000ncM2lIAE');
            
            Efleet_HomeController.fetchAllFleets();
            Efleet_HomeController.fetchAllDealers();
            Efleet_HomeController.fetchTyreSizes();
            Efleet_HomeController.fetchPatterns();
            Efleet_HomeController.fetchTyrePatternSize();
            Efleet_HomeController.fetchMaterialMaster();
            Efleet_HomeController.fetchRelatedPatterns('CEAT', '10.00-20');
            //Efleet_HomeController.fetchOeDealers(tMake);
            Efleet_HomeController.fetchStates();
            Efleet_HomeController.fetchDistricts(stateMaster.State__c);
            Efleet_HomeController.fetchTowns(stateMaster.District__c);
            Efleet_HomeController.fetchHaulType(50);
            Efleet_HomeController.fetchLoadRating('6W', 150);
            Efleet_HomeController.fetchLoadRating('6W', 50);
            Efleet_HomeController.fetchLoadRating('6W', 250);
            Efleet_HomeController.fetchLoadRating('6W', 350);    
            Efleet_HomeController.fetchCustomerTypes();
            Efleet_HomeController.fetchCustomerCategories();
            Efleet_HomeController.fetchDefectTypes();
            Efleet_HomeController.fetchTruckMake();
            Efleet_HomeController.fetchTyreClasses();
            Efleet_HomeController.fetchMaterials();
            Efleet_HomeController.fetchModes();
            Efleet_HomeController.fetchLinkedWheelTypes('Heavy Commercial');
            Efleet_HomeController.fetchLinkedWheelTypes('Light Commercial');
            Efleet_HomeController.fetchLinkedWheelTypes('PCR/UCR');
            Efleet_HomeController.fetchLinkedWheelTypes('Last Mile');
            Efleet_HomeController.fetchLinkedWheelTypes('Two Wheeler');
            
            Efleet_HomeController.saveVehicalInformation(veh, 'aaa');
            Tyre__c newTyre = CEAT_InitializeTestData.createTyre('CEAT', 0, 'HCL', 'Front', '10.00-20', '1', veh.Id);
            Efleet_HomeController.fetchTyreAndInspections(veh.Id);
            Efleet_HomeController.addNewTyreInfo(newTyre);
            Efleet_HomeController.fetchVehicalInformation(acc1.Id);
            Efleet_HomeController.fetchSelectedFleet(acc1.Id);
            Efleet_HomeController.fetchSelectedContact(acc1.Id);
            Efleet_HomeController.fetchSelectedVehicleInfo(acc1.Id);
            Efleet_HomeController.fetchSelectedScrap(acc1.Id);
            Efleet_HomeController.fetchSelectedOperation(acc1.Id);
            Efleet_HomeController.fetchSelectedRecommTyre(fleetOperation.Id);
            
            Efleet_HomeController.saveFleetDetails(acc1);
            Efleet_HomeController.saveFleetDetails(acc2);
            Efleet_HomeController.saveContactDetails(con, '20/04/2015', '20/04/2015');
            List<Fleet_Vehicle_Information__c> vehInfoList = new List<Fleet_Vehicle_Information__c>();
            vehInfoList.add(vehInfo);
            Efleet_HomeController.saveVehicleInfo(acc1, vehInfoList);
            List<Scrap_Yard_Report__c> scrapReportList = new List<Scrap_Yard_Report__c>();
            scrapReportList.add(scrapReport);
            Efleet_HomeController.saveScrapReport(scrapReportList, acc1.Id, new List<String>{'20/04/2015'});
            List<Recommended_Tyre__c> recomTyreList = new List<Recommended_Tyre__c>();
            recomTyreList.add(recomTyre);
            List<Recommended_Tyre__c> recomTyreList1 = new List<Recommended_Tyre__c>();
            recomTyreList1.add(recomTyre);
            Efleet_HomeController.savePreSalesApproach(fleetOperation, recomTyreList, recomTyreList1);
            
            Efleet_HomeController.fetchRecommendedTyres('Truck', '6W', 'Bias', 'On Road', 'Normal Load', 'Short', '10.00-20');
            
            Sales_Tracker__c stracker = CEAT_InitializeTestData.createSalesTracker(acc1.Id, 'January', '2014');    
            Sales_Tracker_Item__c strackerItem = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id);            
            strackerItem.Month__c = Efleet_HomeController.calculateMonth(Date.today().month());
            strackerItem.Year__c = String.valueOf(Date.today().year());
            List<Sales_Tracker_Item__c> strackerItemList = new List<Sales_Tracker_Item__c>();            
            strackerItemList.add(strackerItem);
            Efleet_HomeController.saveSalesTracker(stracker, strackerItemList);
            Sales_Tracker_Item__c strackerItem1 = CEAT_InitializeTestData.createSalesTrackerItem(stracker.Id);
            strackerItem1.Month__c = Efleet_HomeController.calculateMonth(Date.today().month());
            strackerItem1.Year__c = String.valueOf(Date.today().year());
            insert strackerItem1;            
            Efleet_HomeController.saveSalesTracker(stracker, strackerItemList); 
            Efleet_HomeController.fetchRunningMonthST(acc1.Id);
            Efleet_HomeController.fetchRunningMonthSTIs(stracker.Id);    
            strackerItem.Month__c = Efleet_HomeController.calculateMonth(Date.today().month()-1);
            update strackerItem;
            Efleet_HomeController.fetchRunningMonthSTIs(stracker.Id);    
            strackerItem.Month__c = Efleet_HomeController.calculateMonth(Date.today().month()-2);            
            update strackerItem;
            Efleet_HomeController.fetchRunningMonthSTIs(stracker.Id);    
            
            Set<String> cstlTerrs = Efleet_HomeController.fetchTlTerritories();    
            
            OE_Dealer__c oed = CEAT_InitializeTestData.createOeDealer('Oe', 'TML');
            Efleet_HomeController.fetchOeDealers('All');
            Efleet_HomeController.fetchOeDealers('TML');
            
            Efleet_HomeController.fetchVehicleCategories();
            Efleet_HomeController.fetchVehicleMakes('PCR/UCR');
            Efleet_HomeController.fetchVehicleModels('PCR/UCR', 'Maruti Suzuki');
            Efleet_HomeController.fetchAllFitmentSurveys();
            Efleet_HomeController.fetchFitmentTyres(null);
            
            Cost_Tracker__c ctracker = CEAT_InitializeTestData.createCostTracker(acc1.Id);    
            Cost_Tracker_Item__c ctrackerItem = CEAT_InitializeTestData.createCostTrackerItem(ctracker.Id);            
            List<Cost_Tracker_Item__c> ctrackerItemList = new List<Cost_Tracker_Item__c>();
            ctrackerItemList.add(ctrackerItem);
            Efleet_HomeController.saveCostTracker(ctracker, ctrackerItemList); 
            Cost_Tracker_Item__c ctrackerItem1 = CEAT_InitializeTestData.createCostTrackerItem(ctracker.Id);
            insert ctrackerItem1;
            Efleet_HomeController.saveCostTracker(ctracker, ctrackerItemList); 
            Efleet_HomeController.fetchCostTracker(acc1.Id);
            Efleet_HomeController.fetchCostTrackerItems(ctracker.Id);                
            
            Efleet_HomeController.fetchRetreadPatterns();
            Efleet_HomeController.fetchStateCodes();
            Efleet_HomeController.fetchRTOCodes();     
            
            Efleet_HomeController.getTopFivePattern(acc1.Id);
            Efleet_HomeController.getBuisnessCardValues('June', 'May', acc1.Id);
            Efleet_HomeController.getAllStateMaster();
            
            Efleet_HomeController.calculateMonth(1);
            Efleet_HomeController.calculateMonth(2);
            Efleet_HomeController.calculateMonth(3);
            Efleet_HomeController.calculateMonth(4);
            Efleet_HomeController.calculateMonth(5);
            Efleet_HomeController.calculateMonth(6);
            Efleet_HomeController.calculateMonth(7);
            Efleet_HomeController.calculateMonth(8);
            Efleet_HomeController.calculateMonth(9);
            Efleet_HomeController.calculateMonth(10);
            Efleet_HomeController.calculateMonth(11);
            Efleet_HomeController.calculateMonth(12);
        }
        Test.stopTest();
        //saveTyreInformation(String tyre,String dateOfFit,String dateOfRemoval)
        
    }
}