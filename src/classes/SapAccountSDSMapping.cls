global class SapAccountSDSMapping {
	    /*
* Auther  :- Neha Mishra
* Purpose :- Fetch SDS Accounts from SAP 
*
*
*/
    
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;

    
    WebService static List<sdsAccountMapping> getSDSaccounts(String cusNum, String fDate, String tDate){
        
        List<sdsAccountMapping> sds = new List<sdsAccountMapping>();
        String customerId  = '';
        try{
            
            if(cusNum!=null){
               customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
            }

            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
	        
	        String username                                 = saplogin.username__c;
	        String password                                 = saplogin.password__c;
	        Blob headerValue                                = Blob.valueOf(username + ':' + password);
	        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
	          
	        SapAccountSDS   scd                           = new SapAccountSDS ();
	        SapAccountSDS.ZWS_CUST_SDS_ACC_STATE    zws  = new SapAccountSDS.ZWS_CUST_SDS_ACC_STATE ();
	        
	        zws.inputHttpHeaders_x                          = new Map<String,String>();
	        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
	        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
	        zws.timeout_x                                   = 60000;
	        
	        SapAccountSDS.TABLE_OF_ZSFDC_CUST_SDS_ACC_STATE tab = new SapAccountSDS.TABLE_OF_ZSFDC_CUST_SDS_ACC_STATE();
	        
	        tab = zws.ZSFDC_CUST_SDS_ACC_STATE(fDate,tab,customerId,tDate);
	        
	        //system.debug(tab.item);
			if(tab.item != null) {
				for(SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE acSt : tab.item){
			    	//system.debug(acSt);
			    	sds.add(new sdsAccountMapping(acSt.PSTNG_DATE, acSt.ITEM_TEXT, acSt.AC_DOC_NO, acSt.DOC_NO, acSt.ORG_DOC_NO, 
			    									acSt.CHEC_NO, acSt.ITEM_TEXT1, acSt.SP_GL_IND, acSt.AMT_DOCCUR1, acSt.AMT_DOCCUR, acSt.BALANCE));
				

				}
			}
			return sds;
		}catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }	
			
    }

    global class sdsAccountMapping{
        public String PstngDate {get;set;}
        public String ItemText  {get;set;}
        public String AcDocNo {get;set;}
        public String DocNo     {get;set;}
        public String OrgDocNo {get;set;}
        public String ChecNo {get;set;}
        public String ItemText1 {get;set;}
        public String SpGlInd {get;set;}
        public String AmtDoccur1{get;set;}
        public String AmtDoccur {get;set;}
        public String Balance {get;set;}      

        public sdsAccountMapping(String PstngDate, String ItemText, String AcDocNo, String DocNo, String OrgDocNo,
        						 String ChecNo, String ItemText1, String SpGlInd, String AmtDoccur1,
        						 String AmtDoccur, String Balance){
            this.PstngDate = PstngDate;
            this.ItemText = ItemText;
            this.AcDocNo = AcDocNo;
            this.DocNo = DocNo;
            this.OrgDocNo = OrgDocNo;
            this.ChecNo = ChecNo;
            this.ItemText1 = ItemText1;
            this.SpGlInd = SpGlInd; 
            this.AmtDoccur1 = AmtDoccur1;
            this.AmtDoccur = AmtDoccur;
            this.Balance = Balance;
        }
    }
}