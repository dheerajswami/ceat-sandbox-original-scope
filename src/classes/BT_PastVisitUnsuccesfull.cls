global class BT_PastVisitUnsuccesfull implements Database.Batchable<sObject>{
    
    /*
	*  Created By Vivek
	*  To make all past visit unsuccessfull
	*/
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id,Visit_Day__c,Status__c FROM Visits_Plan__c WHERE Visit_Day__c < TODAY AND (Status__c = \'Scheduled\' OR Status__c = null) AND Record_Type_Name__c != \'MOR-OE-CompAct\' AND Check_In__c=false';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        List<Visits_Plan__c> visitsToUpdate = new List<Visits_Plan__c>();
        for(sObject vp : scope){
            Visits_Plan__c visit = (Visits_Plan__c)vp;            
            visit.Status__c = 'Unsuccessful';
            visitsToUpdate.add(visit);
        }
        
        update visitsToUpdate;
    }
    
    global void finish(Database.BatchableContext BC){
        
    }

}