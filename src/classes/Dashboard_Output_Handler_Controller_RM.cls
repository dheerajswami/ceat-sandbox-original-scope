public class Dashboard_Output_Handler_Controller_RM{        
    
    // Main Wrapper Class
    
    public class dashboardWrapperMain{
        public  List<dashboardWrapper> outputDb {get;set;}
        public  dashboardInputWrapper inputDb {get;set;}       
    } 
    
   //Wrapper Classes  For Input   
    public class dashboardInputWrapper{    
        public set<string> moduleSet     {get;set;}    
        public set<string> catSet     {get;set;}
        public list<Dashboard_Input_Score__c> dbInputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
    } 
       
    //Wrapper classes    
    public class dashboardWrapper implements Comparable{
        public String catName{get;set;}
        public Double score{get;set;}
        public Double catWeight{get;set;}
        public Decimal columnOrder{get;set;}
        public List<Dashboard_Score__c> dashboardScore{get;set;}
        public List<Dashboard_Output_Score__c> dashboardOutputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
        public Integer compareTo(Object compareTo) {
            dashboardWrapper compareToCat = (dashboardWrapper)compareTo;
            if (columnOrder == compareToCat.columnOrder) return 0;
            if (columnOrder > compareToCat.columnOrder) return 1;
            return -1;        
        }
    }
    
    public Dashboard_Output_Handler_Controller_RM(){
    }
    
    public  dashboardWrapperMain getDashboardDataRecords(Id userId, string selectedMonth, string selectedYear){
        
        String currentDay              = String.valueOf((Date.today()).day());
        String currentMonth            = String.valueOf((Date.today()).month());
        String currentYear             = String.valueOf((Date.today()).year());
        String catDashList             = '';
        list<Dashboard_Weightage_Master__c> dashboardWeight     = new List<Dashboard_Weightage_Master__c>(); 
        list<Dashboard_Score__c> dashboardScore                 = new List<Dashboard_Score__c>(); 
        list<Dashboard_Output_Score__c> dashboardOutputScore    = new List<Dashboard_Output_Score__c>(); 
        list<Dashboard_Score__c> dashScorList ;
        list<Dashboard_Output_Score__c> dashoutScore ;
        list<Dashboard_Master__c> dashboardMaster               = new list<Dashboard_Master__c>();
        list<string> dashcatSet                                 = new list<String>();
        list<String> catlist                                    = new List<String>();
        List<Dashboard_Score__c> daList                         = new list<Dashboard_Score__c>();
        List<Dashboard_Output_Score__c > daoutList              = new list<Dashboard_Output_Score__c >();
        list<UserTerritory2Association > userAssociation                = new list<UserTerritory2Association>();
        Dashboard_Summary_Score__c dashSummaryScore             = new Dashboard_Summary_Score__c();
        
        Set<String> SetOFCat                                    = new Set<String>();
        List<String> catsetParam                                = new List<String>();
        Set<String> setOFParametersAllCat                       = new Set<String>();
        List<String> catSet                                     = new List<String>();
        Set<String> catdashSet                                  = new Set<String>();
        Set<String> setOFParametersCatWise                      = new set<String>();
        Map<String,String> MapOFCatAndCode                                          = new Map<String,String>();
        Map<String,Double> MapOFCatAndWeigth                                        = new Map<String,Double>();
       
        Map<String,List<Dashboard_Score__c>> MapOFCatAndDashboardScore              = new Map<String,list<Dashboard_Score__c>>();
        Map<String,List<Dashboard_Output_Score__c >> MapOFCatAndDashboardOutScore   = new Map<String,list<Dashboard_Output_Score__c >>();
        Map<String,Dashboard_Weightage_Master__c> mapOFCatAndParam                  = new Map<String,Dashboard_Weightage_Master__c>();
        Map<String,List<String>> mapOFDashCatAndCatCode                             = new Map<String,List<String>>();
        Map<String,String> mapOFDashCatAndCatName                                   = new Map<String,String>();
        Dashboard_Score__c dsrecord ;
        
        //list<Sales_Planning_Categories__c> dashboardCat = Sales_Planning_Categories__c.getall().values();
            for(Sales_Planning_Categories__c sp:Sales_Planning_Categories__c.getall().values()){
              if(sp.Include_in_Dashboard__c == true){
                catdashSet.add(sp.Category_Code__c); 
                if(mapOFDashCatAndCatCode.containsKey(sp.Category_name_as_in_Dashboard__c)) {
                    catSet          = new List<String>();
                    catSet          = mapOFDashCatAndCatCode.get(sp.Category_name_as_in_Dashboard__c); 
                    catset.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }else{
                    catSet          = new List<String>();
                    catSet.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }
              }      
        }
        /*
        dashboardMaster             = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true AND Role__c = 'RM'];
        dashboardWeight = [SELECT id,Category__c,Parameters_Output__c,Role__c,Category_Code__c,Testing__c,Weightage__c FROM Dashboard_Weightage_Master__c where RecordType.Name='Output' AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c = 'RM'];
       
        for(Dashboard_Weightage_Master__c param : dashboardWeight){
            mapOFCatAndParam.put(param.Category_Code__c,param);
            catsetParam             = mapOFDashCatAndCatCode.get(param.Category__c);
            //dashcatSet.addAll(catsetParam);
            String[] paramSplit     = (param.Parameters_Output__c).split(';');
            if(paramSplit.size() > 0){
                for(String st : paramSplit){
                    setOFParametersAllCat.add(st);
                }
            }            
        } */
       
        if(userId!=null){ 
             system.debug('userId=-------'+userId);
             dashboardScore      = [SELECT id,Region_Code__c,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Role__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__r.Category__c,Dashboard_Weightage_Master__r.Parameters_Output__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c where User__c=:userId AND Month__c =: selectedMonth AND Year__c =: selectedYear /*AND Parameters__c IN:setOFParametersAllCat */];
             dashboardOutputScore = [SELECT ID,Region_Code__c,Total_Monthly_Target__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Final_Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c where User__c=:userId  AND Month__c =: selectedMonth AND Year__c =: selectedYear];
             dashSummaryScore     = [SELECT ID,Region_Code__c,Achievement__c,All_Category_Score1__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Weightage__c,DSS_External_ID__c,Month__c,Score__c,Territory_Code__c,Total_MTD_Actual__c,Total_MTD_Target__c,Total_Output_Score__c,Total_Value__c,Total_Value_L3M__c,Total_Value_LYCM__c,Year__c FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId  AND Month__c =: selectedMonth AND Year__c =: selectedYear limit 1];
        }else{
             dashboardScore  = [SELECT id,Region_Code__c,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Role__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__r.Category__c,Dashboard_Weightage_Master__r.Parameters_Output__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c WHERE Parameters__c IN:setOFParametersAllCat];
             dashboardOutputScore = [SELECT ID,Region_Code__c,Total_Monthly_Target__c,Final_Score__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c];
        }
        system.debug('dashSummaryScore-----'+dashSummaryScore);
        /*
        for(Dashboard_Weightage_Master__c ss : dashboardWeight ){
            SetOFCat.add(ss.Category__c);
            MapOFCatAndWeigth.put(ss.Category__c,ss.Weightage__c);
        }   */    
        
        for(Dashboard_Score__c ds : dashboardScore  ){
               dashScorList = new list<Dashboard_Score__c>();
               /*
               if(mapOFCatAndParam.containsKey(ds.Category__c)){
                   String[] paramSplit1 = (mapOFCatAndParam.get(ds.Category__c).Parameters_Output__c).split(';');
                   if(paramSplit1.size() > 0){
                        for(String st : paramSplit1){
                            setOFParametersCatWise.add(st);
                        }
                    }    
               }*/
               if(MapOFCatAndDashboardScore.containsKey(ds.Category__c) /*&& setOFParametersCatWise.contains(ds.Parameters__c)*/){
                   dashScorList = MapOFCatAndDashboardScore.get(ds.Category__c);
                   dashScorList.add(ds);
                   MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
                   SetOFCat.add(ds.Dashboard_Weightage_Master__r.Category__c);
                   MapOFCatAndWeigth.put(ds.Dashboard_Weightage_Master__r.Category__c,ds.Dashboard_Weightage_Master__r.Weightage__c);
               }else{
                   //if(setOFParametersCatWise.contains(ds.Parameters__c)){
                       dashScorList = new list<Dashboard_Score__c>();
                       dashScorList.add(ds);
                       MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
                       SetOFCat.add(ds.Dashboard_Weightage_Master__r.Category__c);
                       MapOFCatAndWeigth.put(ds.Dashboard_Weightage_Master__r.Category__c,ds.Dashboard_Weightage_Master__r.Weightage__c);
                   //}
               }
               //daList.add(ds);               
        }
       
        for(Dashboard_Output_Score__c temp : dashboardOutputScore ){
            dashoutScore = new list<Dashboard_Output_Score__c>();
            if(MapOFCatAndDashboardOutScore.containsKey(temp.Category__c)){
                   dashoutScore = MapOFCatAndDashboardOutScore.get(temp.Category__c);
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }else{
                   dashoutScore= new list<Dashboard_Output_Score__c>();
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }           
        }
        
        //MapOFCatAndDashboardOutScore.put('2010',daoutList);
        List<dashboardWrapper> dashboardRecord      = new List<dashboardWrapper>();
        system.debug('SetOFCat====='+SetOFCat);
        for(String str : SetOFCat){
             dashboardWrapper c         =   new dashboardWrapper();
             c.catName = str;
             c.columnOrder = (Sales_Planning_Categories__c.getValues(mapOFDashCatAndCatName.get(str))).Dashboard_Sort_Order__c;
            // c.score= 20;
             c.catWeight = MapOFCatAndWeigth.get(str);
             //catset = new Set<String>();
             catlist = new List<String>();
             if(mapOFDashCatAndCatCode.ContainsKey(str)){
                  catlist =    mapOFDashCatAndCatCode.get(str);
                  catlist.sort();
             }
             if(catlist.size() > 0){
                catDashList = '';
                for(String s : catlist){
                    if(catDashList == ''){
                        catDashList = s;
                    }else{
                        catDashList = catDashList +':'+ s;
                    }   
                }
            }
             c.dashboardScore = MapOFCatAndDashboardScore.get(catDashList);
             c.dashboardOutputScore  = MapOFCatAndDashboardOutScore.get(catDashList);
             //if(dashSummaryScore.size() > 0){
                c.dashboardSummaryScore = dashSummaryScore;
             //}
             
             if(MapOFCatAndDashboardOutScore.get(catDashList) !=null && MapOFCatAndDashboardScore.get(catDashList)!=null){
                 dashboardRecord.add(c);
                 
             }
        }
        
        dashboardRecord.sort();
        
        //*******************************************************************Input Section ****************************************************************************
        
         string dealers                  = system.Label.TL_PJP_nput;    
         string distributors             = system.Label.TL_Distributor;
         string truckCustomer            = system.Label.TL_Efleet; 
         string actionLog                = system.Label.TL_Action_Log;   
         
         String rmRole                       = system.Label.RM_Role; 
          map<Integer,String> monthYearMap = new map<Integer,String>();
         /*
                To convert current month into month name
        */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');
            
        Integer month                    = Date.today().month();
        String currentMonth1             = monthYearMap.get(month);
        String currentYear1              = String.valueOf((Date.today()).year());
        string TLInputRecType           = system.Label.Input_RecType;
        
        list<Dashboard_Input_Score__c> dashboardInputScore                = new List<Dashboard_Input_Score__c>(); 
        
        Dashboard_Summary_Score__c dashSummaryScore1                      = new Dashboard_Summary_Score__c();
        map<String,Double> MapOFCatAndWeigtage                            = new map<String,Double>();
          
       
        /* Getting  Input score / Summary score for particular territory */
        if(Integer.valueof(selectedMonth) == month){
            if(userId!=null){
                dashboardInputScore  = [SELECT ID,Achievement__c,Region_Code__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where RecordType.Name=:rmRole AND User__c=:userId  AND  Month__c =: monthYearMap.get(Integer.valueof(selectedMonth)) AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
                dashSummaryScore1    = [SELECT ID,DSS_External_ID__c,Input_Score__c,Territory_Code__c,Region_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId AND Month__c = :selectedMonth AND Year__c =: selectedYear limit 1];
            }else{
                 dashboardInputScore = [SELECT ID,Region_Code__c,ScoreMTD__c,Achievement__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c  where RecordType.Name=:rmRole AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
            } 
        }else {
            if(userId!=null){
                dashboardInputScore  = [SELECT ID,Achievement__c,Region_Code__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where RecordType.Name=:rmRole AND User__c=:userId  AND  Month__c =: monthYearMap.get(Integer.valueof(selectedMonth)) AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Weightage__c>0];
                dashSummaryScore1    = [SELECT ID,DSS_External_ID__c,Input_Score__c,Territory_Code__c,Region_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId AND Month__c = :selectedMonth AND Year__c =: selectedYear limit 1];
            }else{
                 dashboardInputScore = [SELECT ID,Region_Code__c,ScoreMTD__c,Achievement__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c  where RecordType.Name=:rmRole AND Dashboard_Weightage_Master__r.Weightage__c>0];
            }
        }
        /* Creating Input wrapper instance to create input Dashboard records */
        
        dashboardInputWrapper dashboardRecord1           = new dashboardInputWrapper();
        dashboardRecord1.catSet                          = new set<string>();
        dashboardRecord1.moduleSet                       = new set<string>();
        
        for(Dashboard_Input_Score__c ss : dashboardInputScore){
                dashboardRecord1.catSet.add(ss.RecordTypeName__c+'@@'+ss.Category__c);               
        } 
       
        dashboardRecord1.moduleSet.add(dealers);
        dashboardRecord1.moduleSet.add(distributors);                 
        dashboardRecord1.moduleSet.add(truckCustomer);
        dashboardRecord1.moduleSet.add(actionLog);  
        
        dashboardRecord1.dbInputScore               = dashboardInputScore;
        dashboardRecord1.dashboardSummaryScore      = dashSummaryScore1; 
        system.debug('---dashboardRecord------'+dashboardRecord1);
        system.debug('---dashboardRecord------'+dashboardRecord1);
        /* Creating Main Wrapper Record*/
        dashboardWrapperMain wrapper = new dashboardWrapperMain();
        wrapper.inputDb              = dashboardRecord1;
        wrapper.outputDb             = dashboardRecord;        
        return wrapper;    
    }
}