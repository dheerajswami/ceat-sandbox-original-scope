public with sharing class CaseCommentHelper {

    
    public static boolean run = true;
    public static string HighPriority;
    public static string  L;
    public static string sfdcbaseUrl = System.URL.getSalesforceBaseURL().toExternalForm();

    
    public static void sendEmailToAllLevelTO(list <CaseComment> caseCommentListNew){
        Set<ID> parentCaseId = new Set<ID>();
        List<Case> caseList = new list<Case>();
        Map<ID,Case> mapOfCaseIdAndCase = new Map<ID,Case>();
        for(caseComment casecom : caseCommentListNew){
            parentCaseId.add(casecom.ParentId); 
        }
        if(parentCaseId.size() > 0){
            caseList = [SELECT id,AllToEmailID__c,CaseNumber,isClosed,ContactId,Business_Category__c,Complaint_Type__c,Keyword__c,Level__c FROM Case WHERE ID IN: parentCaseId];
        }
        if(caseList.size() > 0){
            for(case c : caseList){
                mapOfCaseIdAndCase.put(c.id,c);
            }
            
        }
        EmailTemplate et=[Select id from EmailTemplate where name=:'Email for Escalation Matrix Case'];
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        for(CaseComment cNew: caseCommentListNew){
                
                if(!(String.isBlank(mapOfCaseIdAndCase.get(cNew.ParentId).AllToEmailID__c)) && mapOfCaseIdAndCase.get(cNew.ParentId).isClosed == false){
                    
                    String[] toEmail=mapOfCaseIdAndCase.get(cNew.ParentId).AllToEmailID__c.split(';');
                    Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
                    //singleMail.setTargetObjectId(mapOfCaseIdAndCase.get(cNew.ParentId).ContactId);
                    //singleMail.setTemplateId(et.Id);
                    String messageBody = '<html><body>Dear Team, <br> <br>  We have received following customer complaint which has not been attended within the specified time period & has escalated to L'+mapOfCaseIdAndCase.get(cNew.ParentId).Level__c +' <br> <br> <br> Following is the comment added at the current escalated Level: <br> <br> "'+cNew.CommentBody +'" <br> <br> For more information, please Visit ['+sfdcbaseUrl+'/'+mapOfCaseIdAndCase.get(cNew.ParentId).id+'] <br> <br>  We request you to contact the customer to resolve the issue and revert back with action taken. <br> <br> <br> <br> With Warm Regards <br>  CEAT ASSIST <br>  1800-221-213 (Toll Free) <br>  Customercare@ceat.in </body>  </html>';                   
                    system.debug('@@@@@@@#'+sfdcbaseUrl);
                    singleMail.setSubject('Similar to all other complaints as below  : ' + '/' + 
                    ('HighPriority') + '/' +
                     mapOfCaseIdAndCase.get(cNew.ParentId).CaseNumber + '/' +
                     mapOfCaseIdAndCase.get(cNew.ParentId).Business_Category__c + '/' +
                     mapOfCaseIdAndCase.get(cNew.ParentId).Complaint_Type__c + '/' + 
                     mapOfCaseIdAndCase.get(cNew.ParentId).Keyword__c + '/' + ('L') +
                     mapOfCaseIdAndCase.get(cNew.ParentId).Level__c);                   
                    singleMail.setHtmlBody(messageBody);
                    singleMail.setSaveAsActivity(false);
                    singleMail.setToAddresses(toEmail);
                    singleMail.setWhatId(mapOfCaseIdAndCase.get(cNew.ParentId).id);
                    emails.add(singleMail);
                }
        }
       // system.debug(emails.size()+'dddddddddd');
        if(emails.size() > 0){
        Messaging.sendEmail(emails);       
        }
    }
}