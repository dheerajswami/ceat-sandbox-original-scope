public class UpdateTownClass {
    
    @InvocableMethod(label='Update Town Geo-code' description='Updates the town\'s geo-code')
    public static List<Id> updateGeoCode(List<String> town) {
        List<Id> li = new List<Id>();
        try{
            system.debug('in here');
            if(town != null && town.size()>0) {
                List<String> parameters = town[0].split(';');
                State_Master__c stateRec = [select Id,Town_Geolocation__latitude__s,Town_Geolocation__longitude__s from State_Master__c where Town__c = :parameters[0]];
                if(stateRec.Town_Geolocation__latitude__s == null){
                    stateRec.Town_Geolocation__latitude__s = Decimal.valueOf(parameters[1]);
                    stateRec.Town_Geolocation__longitude__s = Decimal.valueOf(parameters[2]);
                    update stateRec;
                    li.add(stateRec.Id);
                }
            }            
        }
        catch(Exception e) {}
        return li;
    }            
}