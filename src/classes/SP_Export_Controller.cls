global class SP_Export_Controller {    
    
    public String loggedInUserName  {get;set;}
    public String loggedInUserCluster  {get;set;}
    public String loggedinuserID {get;set;}
    public Datetime myDatetime ;
    public String myDatetimeStr {get;set;}           
    User userName;
    String clustermaninterr                          = system.Label.terrSrManagerExport;
    List<UserTerritory2Association> clusterForExportManager1 = new List<UserTerritory2Association>();
    global  SP_Export_Controller() { 
        loggedinuserID  =ApexPages.currentPage().getParameters().get('id');
        system.debug(loggedinuserID+'loggedinuserID');
        if(loggedinuserID =='' ||  loggedinuserID ==null ){
            loggedinuserID  =UserInfo.getUserId();
        }
        myDatetime = Datetime.now();
        myDatetimeStr = myDatetime.format('MMMM, yyyy');
        userName=[select name,id from User where id=:UserInfo.getUserId() limit 1];
        clusterForExportManager1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: clustermaninterr ];
        if(clusterForExportManager1.size() > 0){
            loggedInUserCluster = clusterForExportManager1[0].Territory2.name;
        }
        system.debug(loggedInUserCluster+'loggedInUserCluster');
        //loggedInUserName=userName.Name;
    } 
    
    //Used to get list of TL or Dealer depending on which user logged in    
    @RemoteAction
    Public Static List<SP_HandlerFor_SP_Export_Controller.DealerWrapper> getDealersOfExportManager (String loggedInUserCluster,String loggedInUserId) {
            SP_HandlerFor_SP_Export_Controller handler = new SP_HandlerFor_SP_Export_Controller();
            return handler.getSrManagerWiseSalesPlanningRecords(loggedInUserCluster,loggedInUserId);
    }
    @RemoteAction
    Public Static void saveDealerExportRecord (Map<String,String> mapOfSalesPlanning,String loggedInUserCluster,String loggedInUserId) {
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String exportDealer             = System.Label.Export_Dealer; 
            String exportForcast            = System.Label.Export_Forcast_BU;
            String recTypeExportForecast    = System.Label.Export_Forcast;
            String exportApproverLabel  = System.Label.Export_Approver; 
            String loggedInUserTerritory;
            Datetime myDatetime = Datetime.now();
            String myDatetimeStr = myDatetime.format('MMMM, yyyy');
            String clustermaninterr                          = system.Label.terrSrManagerExport;
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfCatAndForcastId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOFCountryCodeAndName           = new map<String,String>();
            Account dealer;
            Server_Url__c serverUrl;
            List<Sales_Planning__c> listOfSalesPlanningDL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> clusterForExportManager             = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningExportForcase                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> countryUnderCluster                             = new List<Territory2>();
            List<Territory2> clusterNameExportManager                             = new List<Territory2>();
            Locking_Screen__c lsforRM;
            Sales_Planning__c exDLRecord;
            Id expoertdealerId           = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: exportDealer Limit 1].Id;
            Id exportForecastId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeExportForecast Limit 1].Id;
            Set<String> salesPlanningDLId           = new Set<String>();
            Set<String> clusterCode                  = new Set<String>();
            Set<Id> clusterId                        = new Set<Id>();
            Set<String> countryCode                 = new Set<String>();
            Set<String> catExpSet                   = new Set<String>();
            list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
            list<UserTerritory2Association> regionForExport            = new list<UserTerritory2Association>();
            regionForExport = [Select RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2=: exportApproverLabel ];
            User exportApproverUser=  [select name,id,email from User where id=:regionForExport[0].UserId];
            for(Export_Sales_Planning_Category__c sp:exportCat){
              if(sp.Include_in_Sales_Planning__c == true){
                catExpSet.add(sp.Category_Code__c);
                //mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);  
              }      
            }
             clusterForExportManager = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: clustermaninterr ];
             for(UserTerritory2Association reg : clusterForExportManager){
                        clusterId.add(reg.Territory2Id);
                        clusterCode.add(reg.Territory2.name);  
                        //mapOFClusterCodeAndName.put(reg.DeveloperName,country.Description);              
             }
             clusterNameExportManager =   [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id from Territory2 WHERE id IN: clusterId];       
             countryUnderCluster = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id from Territory2 WHERE ParentTerritory2Id IN: clusterId]; 
             for(Territory2 country : countryUnderCluster){
                    countryCode.add(country.DeveloperName);
                    mapOFCountryCodeAndName.put(country.DeveloperName,country.Description);
             }
           
            salesPlanningExportForcase = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c FROM Sales_Planning__c WHERE  Cluster_Code__c IN : clusterCode  AND Year__c =:year AND Month__c =:month AND RecordTypeId =: exportForecastId AND Category__c In :catExpSet];
            for(Sales_Planning__c temp :salesPlanningExportForcase){
                    mapOfCatAndForcastId.put(temp.Category__c,temp.id);
                    mapOfCatAndCatCode.put(temp.Category__c,temp.Category_Description__c);
            }
            try{
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                    }
                    else{
                            system.debug('inside if');
                            system.debug(listKey+'%%%%%');
                            dealer = new account(KUNNR__c=listKey[1]);
                            st = listKey[1] + listKey[4] + listKey[2]+listKey[3]+month +year ;
                            exDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfCatAndForcastId.get(listKey[4]),Category__c = listKey[4],Category_Description__c = mapOfCatAndCatCode.get(listKey[4]),
                            Month__c = month ,Country_Desc__c = mapOFCountryCodeAndName.get(listKey[3]),Year__c = year ,Dealer_Name__c= listKey[5],Cluster_Desc__c=clusterNameExportManager[0].Description,Dealer__r = dealer,Dealer_CustNumber__c= listKey[1],Cluster_Code__c = listKey[2],Country_Code__c=listKey[3],RecordTypeId = expoertdealerId,Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st);
                            listOfSalesPlanningDLForInsert.add(exDLRecord);
                            system.debug(st+'$$$$$');
                            system.debug(exDLRecord.OwnerId+'$$$$$');
                            system.debug(exDLRecord.Parent_Sales_Planning__c+'$$$$$');
                    
            }
            
            system.debug(listOfSalesPlanningDLForInsert+'listOfSalesPlanningDLForInsert');
            system.debug(salesPlanningDLId+'salesPlanningDLId');
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            system.debug(listOfSalesPlanningDL+'listOfSalesPlanningDL');
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String val = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Total_planned__c = Decimal.valueOf(val);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            system.debug(upsertsalesPlanningDLList.size()+'###');
            system.debug(upsertsalesPlanningDLList+'###');
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            upsertsalesPlanningDLList.clear();
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }        
    }
            }catch(Exception e){} 
    alreadyLocked = [SELECT id,name,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:UserInfo.getUserId() AND Month__c =: month AND Year__c =: year AND Submitted__c = true];
            if(!(alreadyLocked.size() > 0)){
                system.debug('enter');
                lsforRM= new Locking_Screen__c(User__c = UserInfo.getUserId(),Month__c = month ,Year__c = year,Submitted__c = true,Status__c = 'Submitted',Territory_Code__c=loggedInUserTerritory,View_Sales_Planning__c='/apex/SP_Export_v1?id='+loggedInUserId);
                insert lsforRM;
            }
            system.debug(alreadyLocked+'alreadyLocked');
            system.debug(lsforRM+'lsforRM');
         Id lockRecId=[select id from Locking_Screen__c where User__c =:loggedInUserId AND Month__c =:month AND Year__c =: year AND Submitted__c = true].Id;
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval...');
            req1.setObjectId(lockRecId);
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req1.setNextApproverIds(new Id[] {exportApproverUser.Id});
            Approval.ProcessResult result = Approval.process(req1);         
            serverUrl=[select url__c from Server_Url__c limit 1];
            FeedItem fitem=new FeedItem();
            fItem.parentId=exportApproverUser.Id;
            fItem.Title='Click here to Approve/Reject';
            fItem.body = 'Sales planning has been submitted for the month '+myDatetimeStr+' by Regional Manager of '+loggedInUserCluster+'. ';
            fItem.LinkUrl = serverUrl.url__c+'/apex/SP_Export_v1?id='+loggedInUserId;
            insert fItem;
    }
    @RemoteAction
    Public Static void saveAsDraftDealerExportRecord (Map<String,String> mapOfSalesPlanning,String loggedInUserCluster,String loggedInUserId) {
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String exportDealer             = System.Label.Export_Dealer; 
            String exportForcast            = System.Label.Export_Forcast_BU;
            String recTypeExportForecast    = System.Label.Export_Forcast;
            String loggedInUserTerritory;
            String clustermaninterr                          = system.Label.terrSrManagerExport;
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfCatAndForcastId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOFCountryCodeAndName           = new map<String,String>();
            Account dealer;
            List<Sales_Planning__c> listOfSalesPlanningDL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> clusterForExportManager             = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningExportForcase                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> countryUnderCluster                             = new List<Territory2>();
            List<Territory2> clusterNameExportManager                             = new List<Territory2>();
            Sales_Planning__c exDLRecord;
            Id expoertdealerId        = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: exportDealer Limit 1].Id;
            Id exportForecastId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeExportForecast Limit 1].Id;
            Set<String> salesPlanningDLId           = new Set<String>();
            Set<String> clusterCode                  = new Set<String>();
            Set<Id> clusterId                        = new Set<Id>();
            Set<String> countryCode                 = new Set<String>();
            Set<String> catExpSet                   = new Set<String>();
            list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
            for(Export_Sales_Planning_Category__c sp:exportCat){
              if(sp.Include_in_Sales_Planning__c == true){
                catExpSet.add(sp.Category_Code__c);
                //mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);  
              }      
            }
             clusterForExportManager = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: clustermaninterr ];
             for(UserTerritory2Association reg : clusterForExportManager){
                        clusterId.add(reg.Territory2Id);
                        clusterCode.add(reg.Territory2.name);  
                        //mapOFClusterCodeAndName.put(reg.DeveloperName,country.Description);              
             }
             clusterNameExportManager =   [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id from Territory2 WHERE id IN: clusterId];       
             countryUnderCluster = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id from Territory2 WHERE ParentTerritory2Id IN: clusterId]; 
             for(Territory2 country : countryUnderCluster){
                    countryCode.add(country.DeveloperName);
                    mapOFCountryCodeAndName.put(country.DeveloperName,country.Description);
             }
           
            salesPlanningExportForcase = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c FROM Sales_Planning__c WHERE  Cluster_Code__c IN : clusterCode  AND Year__c =:year AND Month__c =:month AND RecordTypeId =: exportForecastId AND Category__c In :catExpSet];
            
            for(Sales_Planning__c temp :salesPlanningExportForcase){
                    mapOfCatAndForcastId.put(temp.Category__c,temp.id);
                    mapOfCatAndCatCode.put(temp.Category__c,temp.Category_Description__c);
            }
            try{
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                    }
                    else{
                            system.debug('inside if');
                            system.debug(listKey+'%%%%%');
                            dealer = new account(KUNNR__c=listKey[1]);
                            st = listKey[1] + listKey[4] + listKey[2]+listKey[3]+month +year ;
                            exDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfCatAndForcastId.get(listKey[4]),Category__c = listKey[4],Category_Description__c = mapOfCatAndCatCode.get(listKey[4]),
                            Month__c = month ,Country_Desc__c = mapOFCountryCodeAndName.get(listKey[3]),Year__c = year ,Dealer_Name__c= listKey[5],Cluster_Desc__c=clusterNameExportManager[0].Description,Dealer__r = dealer,Dealer_CustNumber__c= listKey[1],Cluster_Code__c = listKey[2],Country_Code__c=listKey[3],RecordTypeId = expoertdealerId,Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st);
                            listOfSalesPlanningDLForInsert.add(exDLRecord);
                            system.debug(st+'$$$$$');
                            system.debug(exDLRecord.OwnerId+'$$$$$');
                            system.debug(exDLRecord.Parent_Sales_Planning__c+'$$$$$');
                    
            }
            system.debug(listOfSalesPlanningDLForInsert+'listOfSalesPlanningDLForInsert');
            system.debug(salesPlanningDLId+'salesPlanningDLId');
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            system.debug(listOfSalesPlanningDL+'listOfSalesPlanningDL');
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String val = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Total_planned__c = Decimal.valueOf(val);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            system.debug(upsertsalesPlanningDLList.size()+'###');
            system.debug(upsertsalesPlanningDLList+'###');
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            upsertsalesPlanningDLList.clear();
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }
    }
    }catch(Exception e){} 
    }
}