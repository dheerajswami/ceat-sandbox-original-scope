/* ================================================
    @Name:  Sap_BatchForAllPJPscores
    @Copyright notice: 
    Copyright (c) 2015, CEAT and developed by Extentor
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose:                                                                                             
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        neha@extentor     23/12/2015      INITIAL DEVELOPMENT                                 
   
    @=======================================================  */

global class Sap_BatchForAllPJPscores implements Database.Batchable<sObject>,Database.AllowsCallouts{
	Id recordTypeId;
	String query;
	String currentMonth;
	
	global Sap_BatchForAllPJPscores() {
		recordTypeId 		= UtilityClass.getRecordTypeId('PJP__c','Replacement');
		currentMonth 		= UtilityClass.fetchMonthName((Date.today().month()));
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query 				= 'SELECT Id,Name,No_Of_Visit_Planed__c,Actual_Visit__c,OwnerId,Owner_Role__c,Owner_Email__c,Visit_Plan_Month__c,Visit_Plan_Year__c FROM PJP__c WHERE RecordTypeId =:recordTypeId AND Visit_Plan_Month__c =:currentMonth';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

		SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue 								= Blob.valueOf(username + ':' + password);
        String authorizationHeader 						= 'Basic '+ EncodingUtil.base64Encode(headerValue);

        Sap_zws_sfdc_pjp_score ws 						= new Sap_zws_sfdc_pjp_score();
        Sap_zws_sfdc_pjp_score.zws_sfdc_pjp_score zws 	= new Sap_zws_sfdc_pjp_score.zws_sfdc_pjp_score();

        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 35000;

        List<Sap_zws_sfdc_pjp_score.ZSFDC_PJP> zs 		 = new List<Sap_zws_sfdc_pjp_score.ZSFDC_PJP>();
        Sap_zws_sfdc_pjp_score.ZSFDC_PJP s;
        Sap_zws_sfdc_pjp_score.TABLE_OF_ZSFDC_PJP pjpTab = new Sap_zws_sfdc_pjp_score.TABLE_OF_ZSFDC_PJP();

        for(sObject tmpRec : scope) {
        	s 					= new Sap_zws_sfdc_pjp_score.ZSFDC_PJP();
        	PJP__c tmpPJP 		= (PJP__c)tmpRec;
        	System.debug('==#2 '+tmpPJP);			
        	s.OWNER_EMAIL 		= tmpPJP.Owner_Email__c;
        	s.VISIT_PLANNED 	= String.valueOf(tmpPJP.No_Of_Visit_Planed__c);
        	s.VISIT_COMP 		= String.valueOf(tmpPJP.Actual_Visit__c);
        	s.PJP_OWNER 		= tmpPJP.Owner_Role__c;
        	s.MNTH 				= String.valueOf(UtilityClass.fetchMonthVal(tmpPJP.Visit_Plan_Month__c));
        	s.Y_YEAR 			= String.valueOf(tmpPJP.Visit_Plan_Year__c);

        	zs.add(s);
        }
        pjpTab.item   			= zs;

        Sap_zws_sfdc_pjp_score.ZSFDC_PJP_SCOREResponse_element elem = new Sap_zws_sfdc_pjp_score.ZSFDC_PJP_SCOREResponse_element();
        elem = zws.ZSFDC_PJP_SCORE(pjpTab);
        System.debug('====#1 ' +elem.RETURN_x);
        
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}