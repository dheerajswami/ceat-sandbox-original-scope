@isTest 
private class SP_BatchForSpecialtyBU_Updated_TestClass{
    static testmethod void testLoadData() {
    	String special                  = System.Label.Specialty;
    	String replacement              = System.Label.Replacement;
    	String Forcast                  = System.Label.Forcast; 
    	String recTypeSpeZoneLabel    = System.Label.Specialty_Forcast_Zone;
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
    	String SPECIALTY='Specialty';
    	String REPLECMENT='Replacement';
    	String repROLabel = System.Label.Replacement_RO;
    	String speForcastLabel = System.Label.Specialty_Forcast;
    	Id replaceROId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        Id specailtyForcastId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speForcastLabel Limit 1].Id;
        Id specailtyForcastZoneId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeSpeZoneLabel Limit 1].Id;
        Id specailtyDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: special Limit 1].Id;
        // Load the test Sales planning Staging from the static resource
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatReplacement');
        List<sObject> listCatSpecialty = Test.loadData(Speciality_Sales_Planning_Categories__c.sObjectType, 'CatSpecialty');
        
        List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountData');
        
        List<sObject> listSpecialtyDealerData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'SpecialtyDealerData');
        for(sObject li : listSpecialtyDealerData){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listSpecialtyDealerData;
        List<sObject> listSPData = Test.loadData(Sales_Planning__c.sObjectType, 'SP_Forcast_Data');
        
        for(sObject li : listSPData){
        		Sales_Planning__c temp = (Sales_Planning__c)li;
        		if(temp.BU__c == REPLECMENT){
        			temp.RecordTypeId = replaceROId;
                    temp.Month__c = month;
            		temp.Year__c = year;
        		}else if(temp.BU__c == SPECIALTY){
        			temp.RecordTypeId = specailtyForcastId;
                    temp.Month__c = month;
            		temp.Year__c = year;
        		}
        		
        }
        update listSPData;
        
        List<sObject> listSPData1 = Test.loadData(Sales_Planning__c.sObjectType, 'SP_Specailty_Data');
        for(sObject li : listSPData1){
        		Sales_Planning__c temp = (Sales_Planning__c)li;
        		if(temp.BU__c == SPECIALTY){
        			temp.RecordTypeId = specailtyDealerId;
                    temp.Month__c = month;
            		temp.Year__c = year;
        		}
        		
        }
        update listSPData1;

        List<sObject> listForcastData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'ForcastData');
        for(sObject li : listForcastData){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listForcastData;
        
        SP_BatchForSpecialtyBU_Updated nn = new SP_BatchForSpecialtyBU_Updated();
        database.executebatch(nn,200); 
    }
}