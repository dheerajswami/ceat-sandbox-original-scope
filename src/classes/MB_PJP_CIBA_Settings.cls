/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:  To setting of mobile like dynamic values for status etc. 
||
||    Modefied By:  Vivek
|| 
||         Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchCIBASettings/*')

global with sharing class MB_PJP_CIBA_Settings {
    
    global class MobileSettings{
        
        public List<String> actionToBeTaken     {get;set;}
        public List<User> users                 {get;set;}
        public String timeStamp                 {get;set;}
        public String UserType                  {get;set;}
        public List<String> statusValues        {get;set;}
        public String userterritory             {get;set;}
        public List<String> cibaSettingSync     {get;set;}
        public List<String> cibaDataSync        {get;set;}    
        
        public MobileSettings(){
            actionToBeTaken = new List<String>(); 
            users           = new List<User>();
            statusValues    = new List<String>();
            cibaDataSync    = new List<String>();
            cibaSettingSync = new List<String>();
        }
    }
    
    @HttpPost
    global static MobileSettings getMobileDataPJP(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
                
        try{
            
            MobileSettings mobileSetting = new MobileSettings();
            
            String userRole = [SELECT Id,Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
            // Fetch action to be taken vales 
            String[] pickVals       = new List<String>();
            
            

            if(userRole.contains('TLD')){
                pickVals        = Label.Action_To_Be_Replacement.split(';'); 
                mobileSetting.UserType = 'TLD';
            }else if(userRole.contains('Specialty')){
                pickVals        = Label.Action_To_Be_Taken_Specialty.split(';');
                mobileSetting.UserType = 'Specialty';
            }else{
                pickVals        = Label.Action_To_Be_Replacement.split(';');
                mobileSetting.UserType = 'Replacement';
            }                        
            
            /*
            Populate the data into the wrapper 
            */
            
            
            mobileSetting.users            = getAllUsers(UserInfo.getUserId());
           // mobileSetting.UserType         = userRole;
            mobileSetting.actionToBeTaken  = pickVals;
            mobileSetting.statusValues     = Label.CIBA_ActionlogStatus.split(';');
            mobileSetting.cibaSettingSync  = Label.CIBA_TypeOfInfluencer.split(';');
            mobileSetting.cibaDataSync     = Label.CIBA_DataSyncMsg.split(';');
            mobileSetting.timeStamp        = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            mobileSetting.userterritory    = '';//[SELECT Territory2.Name FROM UserTerritory2Association WHERE UserId = :UserInfo.getUserId()].Territory2.Name;
            
            
            return mobileSetting;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage()+ ' Cause' + e.getCause());
            try{
                Error_Log__c errorLog = new Error_Log__c(Class_Name__c = 'MB_PJP_MobileDataDummy',Line_Number__c = Integer.valueOf(e.getLineNumber()),Error_Message__c = e.getMessage());
                insert errorLog;
            }catch(Exception ex){
                system.debug(ex.getLineNumber() + ' Error -'+ ex.getMessage()+ ' Cause' + ex.getCause());
            }
            
            return null;
        }
        
        
    }
    
    /*
* Fetch all users who are above in hierachy
*/
    global static List<User> getAllUsers(Id userId){
        
        Set<Id> userIds = new Set<Id>();
        Set<Id> territoriesId = new Set<Id>();
        List<UserTerritory2Association> territoryId = new List<UserTerritory2Association>();
        
        territoryId = [SELECT Territory2Id FROM UserTerritory2Association WHERE UserId = :userId];
        
        for(UserTerritory2Association uA : territoryId){
            territoriesId.add(uA.Territory2Id);
        }
        
        List<Territory2> territoryList = new List<Territory2>();        
        territoryList = [SELECT Id, DeveloperName, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoriesId];
        
        for(Territory2 tr : territoryList){
            territoriesId.add(tr.ParentTerritory2Id);
            territoriesId.add(tr.ParentTerritory2.ParentTerritory2Id);
        }
        
        for(UserTerritory2Association uMap : [SELECT Id,Territory2Id,UserId FROM UserTerritory2Association WHERE Territory2Id IN :territoriesId OR Territory2.DeveloperName ='HO_Replacement' ]){
            userIds.add(uMap.UserId);
        }
        
        return [SELECT Id,Name FROM User WHERE isActive = true AND Id IN :userIds];
    }
    
}