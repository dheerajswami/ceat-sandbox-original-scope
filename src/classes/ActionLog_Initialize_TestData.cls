public class ActionLog_Initialize_TestData {
  //Methods called from action log test classes
  public static Account createAccount(String Name,String terrCode,String segment,String sapCode){
        Account tempAccount= new Account();    
        tempAccount.Name         = Name;
        //tempAccount.Territory_Code__c  = terrCode;
        tempAccount.Type        = segment;
        tempAccount.KUNNR__c      = sapCode;
        
        return tempAccount;
    }
  public static User createUser(String profName,String email){
      
      User user = new User();
      Profile p = [SELECT Id FROM Profile WHERE Name=:profName]; 
      user = new User(Alias = 'std', Email=email, 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=email);
      
      return user;
    }
  public static Task createActionLog(String status,Date deadline){
      Task task = new Task();
      date mydate = deadline;//date.parse(deadline);      
      task.Status       = status;
      task.ActivityDate    = mydate;      
      return task;
    }
    
    public static Prospect_Customer_Counter__c createCustomerSetP(){
        Prospect_Customer_Counter__c p = new Prospect_Customer_Counter__c();
        p.Name = 'TEST_NAME';
        p.CustomerCounter__c = 4;
        p.ProspectCounter__c = 3;
        
        return p;
        
    }
   
}