@isTest(seeAllData=false)
private class WB_FetchCustomerDetail_Test {
    private static Account testAccount1;
    private static Account testAccount2;
    private static Account testAccount3; 
    private static Contact testContact1;
    private static Contact testContact2;
    private static Contact testContact3;
    private static Employee_Master__c testEmployeeMaster1;
    private static Employee_Master__c testEmployeeMaster2;
    private static Employee_Master__c testEmployeeMaster3;

    static void init() {
        //testAccount = new List<Account>();
        testAccount1 = new Account(Name = 'Test Acc',KUNNR__c = '55000038',Preferred_Language__c = 'Hindi',Phone  = '9897656785');
        //testAccount.add(tmpAcc);   
        insert testAccount1;
        testAccount2 = new Account(Name = 'Test Acc1',KUNNR__c = '55000039',Preferred_Language__c = 'Hindi',Phone  = '9897656784');  
        insert testAccount2;
        testAccount3 = new Account(Name = 'Test Acc2',KUNNR__c = '55000040',Preferred_Language__c = 'Hindi',Phone  = '9897656784');  
        insert testAccount3;

        testContact1 = new Contact(LastName = 'Test Con1',AccountId = testAccount1.Id,Phone = '9897656787');
        insert testContact1;
        testContact2 = new Contact(LastName = 'Test Con1',AccountId = testAccount1.Id,Phone = '9897656786');
        insert testContact2;
        testContact3 = new Contact(LastName = 'Test Con1',AccountId = testAccount1.Id,Phone = '9897656786');
        insert testContact3;

        testEmployeeMaster1 = new Employee_Master__c(Name = 'Test Employee1',EmployeeId__c = '20987654',Preferred_Language__c = 'Hindi',Mobile__c = '9897656788');
        insert testEmployeeMaster1;
        testEmployeeMaster2 = new Employee_Master__c(Name = 'Test Employee2',EmployeeId__c = '20987655',Preferred_Language__c = 'Hindi',Mobile__c = '9897656788');
        insert testEmployeeMaster2;
        testEmployeeMaster3 = new Employee_Master__c(Name = 'Test Employee3',EmployeeId__c = '20987656',Preferred_Language__c = 'Hindi',Mobile__c = '9897656790');
        insert testEmployeeMaster3;
    }

    static testMethod void unitTest1() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656785');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Id [] fixedSearchResults= new Id[1];
       fixedSearchResults[0] = testAccount1.Id;
       Test.setFixedSearchResults(fixedSearchResults);
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest2() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656784');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Id [] fixedSearchResults= new Id[2];
       fixedSearchResults[0] = testAccount2.Id;
       fixedSearchResults[1] = testAccount3.Id;
       Test.setFixedSearchResults(fixedSearchResults);
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }


    static testMethod void unitTest3() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656787');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Id [] fixedSearchResults= new Id[1];
       fixedSearchResults[0] = testContact1.Id;
       Test.setFixedSearchResults(fixedSearchResults);
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest4() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656786');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Id [] fixedSearchResults= new Id[2];
       fixedSearchResults[0] = testContact2.Id;
       fixedSearchResults[1] = testContact3.Id;
       Test.setFixedSearchResults(fixedSearchResults);
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest5() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656788');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        Id [] fixedSearchResults= new Id[2];
       fixedSearchResults[0] = testContact2.Id;
       fixedSearchResults[1] = testContact3.Id;
       Test.setFixedSearchResults(fixedSearchResults);
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest6() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656789');
        req.addParameter('CustomerType', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest7() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656788');
        req.addParameter('CustomerType', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest8() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656790');
        req.addParameter('CustomerType', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        /*Id [] fixedSearchResults= new Id[1];
       fixedSearchResults[0] = testContact1.Id;
       Test.setFixedSearchResults(fixedSearchResults);*/
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest9() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', '9897656789');
        req.addParameter('CustomerType', 'F');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

    static testMethod void unitTest10() {
        init();
        WB_FetchCustomerDetail.OutPutData outPut = new WB_FetchCustomerDetail.OutPutData();
        outPut.customer_sf_id ='';
        outPut.customerSfId_flag ='';
        outPut.registration_flag ='';
        outPut.customer_sap_code ='';
        outPut.preferred_lang ='';

        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/FetchCustomerId';  
        req.addParameter('CustomerPhone', null);
        req.addParameter('CustomerType', 'F');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        outPut  = WB_FetchCustomerDetail.getCustomerDetails();
    }

}