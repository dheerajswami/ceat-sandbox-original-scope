/**
 * cMapping class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates cMapping class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
global class OrderPlacementContoller_TestClass{
    static Account acc ;
    static order ord ;
    static Order_Line_Items__c ordLineItem1 ; 
    static Order_Line_Items__c ordLineItem2 ;
    static Order_Line_Items__c ordLineItem3 ;
    static Account accwithPass ;
    static Contact con ;
    static Contact con1 ;
    static User portalUser ;
    static User portalUserExports ;
    static list<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesReg1 = new list<CPORT_SalesRegisterDetails.SalesRegisterMapping> ();
    static List<String> catStr = new list<String>();  
    static List<Order_Line_Items__c> orderLineItem = new List<Order_Line_Items__c>();
    static list<Material_Master_Sap__c> matMasterList = new list<Material_Master_Sap__c> ();
    static Product_Division__c productDiv;

    public static void init(){
         
       SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        
       // Export Categories
       List<sObject> listCatExport = Test.loadData(Export_Sales_Planning_Category__c.sObjectType, 'Export_Category');
        
        // Create Account
        acc = CEAT_InitializeTestData.createAccount('Test_Account','50003327');
        insert acc;
        acc.Distribution_Channel_Text__c = String.ValueOf(1);
        acc.Division_Text__c    = String.ValueOf(4);
        acc.UniqueIdentifier__c = '50003327';
        update acc;


        productDiv = new Product_Division__c(Name = 'Truck',Code__c = '01');
        insert productDiv;
        //Create Account with Passcode
        accwithPass = CEAT_InitializeTestData.createAccount('Test_Account','50003326');
        insert accwithPass;
        accwithPass.Passcode__c = '1234';
        update accwithPass;

        //Create order 

        ord = CEAT_InitializeTestData.createOrder('ZSTD','1000',acc.id,'Submitted to SAP','');
        insert ord;
        //ord.Description = ''; 
        //Create Order Line Item
        
        ordLineItem1 = CEAT_InitializeTestData.createOrderLineItem('Alternate','100450','AURC','FGSL',10,ord.id);
        //insert ordLineItem1;
        orderLineItem.add(ordLineItem1);
        ordLineItem2 = CEAT_InitializeTestData.createOrderLineItem('Send to SAP','100450','AURC','FGSL',10,ord.id);
        //insert ordLineItem2;
        orderLineItem.add(ordLineItem2);
        ordLineItem3 = CEAT_InitializeTestData.createOrderLineItem('Send to SAP','100450','AURC','FGSL',10,ord.id);
        //insert ordLineItem3;
        orderLineItem.add(ordLineItem3);
        // Create Contact
        con = CEAT_InitializeTestData.createContact('Test_Contact',acc.Id);
        insert con;
        
        // Create Contact
        con1 = CEAT_InitializeTestData.createContact('Test_Contact1',acc.Id);
        insert con1;
        
        //Create Exports Portal User
        portalUserExports = CEAT_InitializeTestData.createPortalUserExports(con.Id);
        insert portalUserExports;
        
        //Create  Portal User
        portalUser = CEAT_InitializeTestData.createPortalUserReplacement(con1.Id);
        insert portalUser;
        
        List<Material_Master_Sap__c> sapMatMaster = new List<Material_Master_Sap__c>();

        sapMatMaster.add(new Material_Master_Sap__c(Name = 'MILE XL', Material_Group__c = '2010',Mat_Grp_Desc__c ='TRUCK', Material_Number__c = '123213123', Tyre_Size__c= '9.00',Ply_Rating__c = '12P' ,Pattern_Brand__c='CEAT',Division__c='01'));
        sapMatMaster.add(new Material_Master_Sap__c(Name = 'XL PRO', Material_Group__c = '2010', Mat_Grp_Desc__c ='TRUCK',Material_Number__c = '54643532', Tyre_Size__c= '10000',Ply_Rating__c = '12P',Pattern_Brand__c='CEAT',Division__c='01'));
        sapMatMaster.add(new Material_Master_Sap__c(Name = 'CENTURA ', Material_Group__c = '2200', Mat_Grp_Desc__c ='CAR CON',Material_Number__c = '743532423', Tyre_Size__c= '12000',Ply_Rating__c = '18P',Pattern_Brand__c='CEAT',Division__c='04'));
        sapMatMaster.add(new Material_Master_Sap__c(Name = 'HCL RIB', Material_Group__c = '2010', Mat_Grp_Desc__c ='TRUCK',Material_Number__c = '74353252', Tyre_Size__c= '10010',Ply_Rating__c = '14P',Pattern_Brand__c='CEAT',Division__c='01'));
        insert sapMatMaster;

        Item_Master__c matMaster = CEAT_InitializeTestData.createMaterialMasterRecord('2010','1000372');
        insert matMaster;  
        Item_Master__c matMaster1 = CEAT_InitializeTestData.createMaterialMasterRecord('2010','1000373');
        insert matMaster1;  

        
       
   }
   public static testMethod void testCaseOrder1(){ 
        init();  
        test.startTest();  
        OrderPlacementContoller.getAccountInfo(acc.id,false);
        OrderPlacementContoller.getAccountInfo(accwithPass.Passcode__c,true);
        OrderPlacementContoller.getMaterialDetails('2010','01');
        catStr = OrderPlacementContoller.getMaterialGroup('01');
        matMasterList = OrderPlacementContoller.fetchProductsCategoryW('TRUCK','01');
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplUnRestricted()); 
        OrderPlacementContoller.fetchUnrestrictedStock('B0001');
        OrderPlacementContoller.getProductDivision();

        OrderPlacementContoller.fetchProductsSize('10000','TRUCK');
        OrderPlacementContoller.fetchProductsPlayRating('12P','10000','TRUCK');

        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplOrder()); 
        OrderPlacementContoller.ReponseOrderCreation resp = new OrderPlacementContoller.ReponseOrderCreation('Order not created.','',false);
        resp = OrderPlacementContoller.submitOrder(acc,orderLineItem,'Address1','01');
        
       
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplShipTOAddress()); 
        OrderPlacementContoller.fetchShipToAddress('50003326');
        
        sapShipToAddress.ZSFDC_STP_ADDRESSResponse_element elem = new sapShipToAddress.ZSFDC_STP_ADDRESSResponse_element();
        sapShipToAddress.ZSFDC_STP ad = new sapShipToAddress.ZSFDC_STP();
       
        test.stopTest();
  }

  public static testMethod void testCaseOrder2(){ 
        init();  
        test.startTest();  
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplOrderExce()); 
        OrderPlacementContoller.ReponseOrderCreation resp = new OrderPlacementContoller.ReponseOrderCreation('Order not created.','',false);
        resp = OrderPlacementContoller.submitOrder(acc,orderLineItem,'Address1','04');
        test.stopTest();
  }

  // Creating Response for UnRestricted web service mapping class  
   private class WebServiceMockImplUnRestricted implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
            list<SapUnrestrictedStockSoap.ZbapiMatStock> unResList = new list<SapUnrestrictedStockSoap.ZbapiMatStock>();
            SapUnrestrictedStockSoap.ZbapiMatStock unRes = new SapUnrestrictedStockSoap.ZbapiMatStock();
            unRes.Matnr = '2220';
            unRes.Werks = '2220';
            unRes.Lgort = '2220';
            unRes.Labst = '2220';
            unRes.Maktx = '2220';
            unRes.Maktx = '2220';
            unRes.Wgbez = '2220';
            unRes.Lgobe = '2220';
            unRes.Name1 = '2220';
            unRes.Bismt = '2220';
            unResList.add(unRes);
            SapUnrestrictedStockSoap.TableOfZbapiMatStock tab = new SapUnrestrictedStockSoap.TableOfZbapiMatStock();
            tab.item = unResList;
            SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element elm = new SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element();
            elm.Itab = tab;
            SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element e = new SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element();
            e.Bzirk = '2220';
            e.Itab = tab;
            e.Matkl = '2220';
            e.Matnr = '2220';
            response.put('response_x', elm);
            
            
            return;
        }     
    }


    // Creating Response for Order web service mapping class  
   private class WebServiceMockImplOrder implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
            list<SapOrderCreationSoap.ZSFDC_SO_H> orderHList = new list<SapOrderCreationSoap.ZSFDC_SO_H >();
            SapOrderCreationSoap.ZSFDC_SO_H  orderH = new SapOrderCreationSoap.ZSFDC_SO_H ();
            orderH.AUART = '2220';
            orderH.VKORG = '2220';
            orderH.VTWEG = '2220';
            orderH.SPART = '2220';
            orderH.AUDAT = '2220';
            orderH.KUNNR = '2220';
            
            orderHList.add(orderH);

            list<SapOrderCreationSoap.ZSFDC_SO_I> orderIList = new list<SapOrderCreationSoap.ZSFDC_SO_I >();
            SapOrderCreationSoap.ZSFDC_SO_I  orderI = new SapOrderCreationSoap.ZSFDC_SO_I ();
            orderI.MATNR = '2220';
            orderI.WERKS = '2220';
            orderI.LGORT = '2220';
            orderI.MENGE = '2220';
            
            orderIList.add(orderI);

            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H  tabH = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H ();
            tabH.item = orderHList;

            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I  tabI = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I ();
            tabI.item = orderIList;

            SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element  elm = new SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element();
            elm.IT_OUT_H = tabH;
            elm.IT_OUT_I = tabI;
            elm.RETURN_x = 'Order Created Successfuly';
            SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATE_element  e = new SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATE_element ();
            
            e.IT_OUT_H = tabH;
            e.IT_OUT_I = tabI;

            response.put('response_x', elm);
            
            
            return;
        }     
    }

    // Creating Response for Order web service mapping class  
   private class WebServiceMockImplOrderExce implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
            list<SapOrderCreationSoap.ZSFDC_SO_H> orderHList = new list<SapOrderCreationSoap.ZSFDC_SO_H >();
            SapOrderCreationSoap.ZSFDC_SO_H  orderH = new SapOrderCreationSoap.ZSFDC_SO_H ();
            orderH.AUART = '2220';
            orderH.VKORG = '2220';
            orderH.VTWEG = '2220';
            orderH.SPART = '2220';
            orderH.AUDAT = '2220';
            orderH.KUNNR = '2220';
            
            orderHList.add(orderH);

            list<SapOrderCreationSoap.ZSFDC_SO_I> orderIList = new list<SapOrderCreationSoap.ZSFDC_SO_I >();
            SapOrderCreationSoap.ZSFDC_SO_I  orderI = new SapOrderCreationSoap.ZSFDC_SO_I ();
            orderI.MATNR = '2220';
            orderI.WERKS = '2220';
            orderI.LGORT = '2220';
            orderI.MENGE = '2220';
            
            orderIList.add(orderI);

            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H  tabH = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H ();
            tabH.item = orderHList;

            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I  tabI = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I ();
            tabI.item = orderIList;

            SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element  elm = new SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element();
            elm.IT_OUT_H = tabH;
            elm.IT_OUT_I = tabI;
            //elm.RETURN_x = 'Order Created Successfuly';
            SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATE_element  e = new SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATE_element ();
            
            e.IT_OUT_H = tabH;
            e.IT_OUT_I = tabI;

            response.put('response_x', elm);
            
            
            return;
        }     
    }

    // Creating Response for Shipping Address web service mapping class  
   private class WebServiceMockImplShipTOAddress implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
            list<sapShipToAddress.ZSFDC_STP > shipTOAddressList = new list<sapShipToAddress.ZSFDC_STP  >();
            sapShipToAddress.ZSFDC_STP   shipTOAddress = new sapShipToAddress.ZSFDC_STP  ();
            shipTOAddress.KUNNR = '2220';
            shipTOAddress.ADDRESS = '2220';
            
            
            shipTOAddressList.add(shipTOAddress);


            sapShipToAddress.TABLE_OF_ZSFDC_STP  tabH = new sapShipToAddress.TABLE_OF_ZSFDC_STP();
            tabH.item = shipTOAddressList;


            sapShipToAddress.ZSFDC_STP_ADDRESS_element   elm = new sapShipToAddress.ZSFDC_STP_ADDRESS_element ();
            elm.IT_FINAL = tabH;
            elm.CUSTOMERCODE = '0050003326';
            sapShipToAddress.ZSFDC_STP_ADDRESSResponse_element   e = new sapShipToAddress.ZSFDC_STP_ADDRESSResponse_element();
            
            e.IT_FINAL = tabH;
           
            response.put('response_x', elm);
            
            return;
        }     
    }

}