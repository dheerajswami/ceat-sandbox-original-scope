global with sharing class BatchClass_Role_Dashboard implements Database.Batchable<sObject>{
    
     public List<DashboardRoleRecordType__c> customsettingDashboard;  
     public List<UserTerritory2Association> zoneForRM1;
    
     Set<String> recordTypeNames;
     Set<String> dashboardOutputKeySet;
     set<String>catsetKey = new set<String>();
     
     String currentMonth;
     String currentDay;
     String currentYear;
     String OutputDashboardRecType=System.Label.OutputRecType;
     
     List<Territory2> userTerritory;     
     List<String> roleNames;
     List<Dashboard_Weightage_Master__c> dwm;
     List<Dashboard_Master_Category_Code__c> dmCategoryCS;
     List<Dealer_Type_Field_Matching__c> dtfm;
     
     map<String,String> userTerritoryMap;
     map<String,Decimal> categoryCodeMap;
     map<String,List<Sales_Planning__c>> mapDealerTypeAndSp;
     map<String,String> mapTotalsAndSp;
     map<String,String> userdetail;
     map<String,Sales_Planning__c> mapOFDealerCatWiseRep;
     
     String repDealerLabel           = System.Label.Replacement_Dealer;
     String spDealerLabel            = System.Label.Specialty;
     //Getting Record type Ids of Sales planning
     Id replaceDealerId    = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
     Id specialtyDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: spDealerLabel Limit 1].Id;
     
     global BatchClass_Role_Dashboard(){
     	
	        roleNames				= new List<String>();
	        recordTypeNames			= new Set<String>();
	        dashboardOutputKeySet   = new Set<String>();
	        userTerritory			= new List<Territory2>();
	        userTerritoryMap		= new Map<String,String>();
	        mapDealerTypeAndSp		= new Map<String,List<Sales_Planning__c>> ();
	        mapTotalsAndSp 			= new Map<String,String>();
	        userdetail				= new Map<String,String>();
	        categoryCodeMap			= new Map<String,Decimal>();
	        dtfm					= new List<Dealer_Type_Field_Matching__c>();
	        dmCategoryCS			= new List<Dashboard_Master_Category_Code__c>();
	        mapOFDealerCatWiseRep   = new map<String,Sales_Planning__c>();
	
			currentDay				= String.valueOf((Date.today()).day());
	        currentMonth			= String.valueOf((Date.today()).month());
	        currentYear				= String.valueOf((Date.today()).year());
	        
	        customsettingDashboard=DashboardRoleRecordType__c.getall().values();
		    dtfm				=Dealer_Type_Field_Matching__c.getall().values();
	        dmCategoryCS=Dashboard_Master_Category_Code__c.getall().values();
	        
	        for(Dashboard_Master_Category_Code__c dmcc:dmCategoryCS){
            	categoryCodeMap.put(dmcc.Category__c,dmcc.Category_Code__c);
       		 }

	        dwm	= new List<Dashboard_Weightage_Master__c>();
	        dwm	= [Select Id,Category__c,Parameters_Output__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c WHERE RecordType.Name =:OutputDashboardRecType];
	
	        for(Dashboard_Weightage_Master__c db:dwm){
	            roleNames.add(db.Role__c);
	            if(db.Role__c=='TL_Replacement'){
	                recordTypeNames.add('Replacement - Dealer');
	            }
	            if(db.Role__c=='RM'){
	                recordTypeNames.add('Replacement - RO');
	            }
	        }
	        List<User> uList=[Select Id,Name,UserRole.DeveloperName from User where UserRole.DeveloperName in : roleNames];
			//Id and rolename       
	        for(User u:uList){
	            userdetail.put(u.Id,u.UserRole.DeveloperName);
	        }	        
	        List<String> terrIdList	= new List<String>();
	        zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId in : userdetail.keySet() and RoleInTerritory2 in : userdetail.values()];
	        
	        for(UserTerritory2Association ut: zoneForRM1){
	            terrIdList.add(ut.Territory2Id);
	        }
	        userTerritory = [select name, id, DeveloperName from Territory2 where id IN :terrIdList];
	        
	        for(UserTerritory2Association ua:zoneForRM1){
	            for(Territory2 t:userTerritory){
	                if(ua.Territory2Id==t.Id){
	                    userTerritoryMap.put(t.DeveloperName,ua.UserId);
	                    break;
	                }
	            }
	        }
	        system.debug('Territory Map.......'+userTerritoryMap);        
     }
    
     global Database.QueryLocator start(Database.BatchableContext BC){
       
        system.debug('roleNames.........'+recordTypeNames);
        String query	= 'Select Id,Name,Territory_Code__c, Category_Description__c,LYCM__c,L3M__c,SYS_Used_IN_Dashboard__c,Actual_Quantity__c,Category__c,Skew_Final__c,Month__c,Year__c,Total_Planned__c,RecordType.Name';
        for(Dealer_Type_Field_Matching__c dtfmcs:dtfm){
            query=query + ', Dealer__r.'+dtfmcs.Dealer_Field_API_Name__c;
        }
         query = query+' from Sales_Planning__c where RecordTypeId =: replaceDealerId AND Month__c =: currentMonth AND Year__c =: currentYear'; 
       // system.debug('query..............'+query);
        return Database.getQueryLocator(query);   
        
    }    
     global void execute(Database.BatchableContext BC, List<Sales_Planning__c> scope) {
         
        system.debug('scope List......'+scope.size());
        map<Id,Account> mapAccount 		= new map<Id,Account>();
        list<Account> AccountSet		= new list<Account>();
        Set<Id> acciD					= new Set<Id>();
        String st	=	'';
        String keyCatRegDea = '';
        
        list<Sales_Planning__c> tempSpList;
        list<Sales_Planning__c> SpecialtySalesList=new list<Sales_Planning__c>();
        list<Sales_Planning__c> tempSpecialtyList;
        list<Sales_Planning__c> tempSpecialtyList1;
        list<Sales_Planning__c> SpList = new list<Sales_Planning__c>();
        
        map<String,List<Sales_Planning__c>> spSpecialtyMap = new map<String,list<Sales_Planning__c>>();
        map<String,Double> spL3mMap = new map<String,Double>();
        map<String,Double> spLycmMap = new map<String,Double>();
        
        for(sObject temp : scope){
            Sales_Planning__c spr =(Sales_Planning__c)temp;
            keyCatRegDea = spr.category__c+spr.Dealer__r.Customer_Segment__c+spr.Territory_Code__c+currentMonth+currentYear;
            catsetKey.add(keyCatRegDea);
        }
        // Querying Sales planning Replacement      
        system.debug('catsetKey---'+catsetKey);
        for(Sales_Planning__c salesPlan : [Select Id,Dealer__c,Value_L3M__c,Value_LYCM__c,Name,Territory_Code__c, Category_Description__c,SYS_Used_IN_Dashboard__c,Dealer__r.Customer_Segment__c,LYCM__c,L3M__c,Actual_Quantity__c,Category__c,Skew_Final__c,Month__c,Year__c,Total_Planned__c,
        RecordType.Name from Sales_Planning__c where RecordTypeId =: replaceDealerId AND Month__c =: currentMonth AND Year__c =: currentYear AND SYS_Used_IN_Dashboard__c IN : catsetKey] ){
      		  mapOFDealerCatWiseRep.put(salesPlan.SYS_Used_IN_Dashboard__c,salesPlan ); 
      		  SpList.add(salesPlan);
        }
        //Querying Specialty Sales planning for calculating total Value L3M and LYCM
        SpecialtySalesList = [Select Id,Dealer__c,Value_L3M__c,Value_LYCM__c,Name,Territory_Code__c, Category_Description__c,SYS_Used_IN_Dashboard__c,Dealer__r.Customer_Segment__c,LYCM__c,L3M__c,Actual_Quantity__c,Category__c,Skew_Final__c,Month__c,Year__c,Total_Planned__c,
        RecordType.Name from Sales_Planning__c where RecordTypeId =: specialtyDealerId AND Month__c =: currentMonth AND Year__c =: currentYear] ;
		system.debug('SpecialtySalesList.size()---'+SpecialtySalesList.size());      
        
        if(SpecialtySalesList.size()>0){
       	 	for(Sales_Planning__c salesPlanSp :SpecialtySalesList ){      		 
	      		 	if(spSpecialtyMap.containsKey(salesPlanSp.category__c+'@@'+salesPlanSp.Territory_Code__c)){    
		            	tempSpecialtyList	= mapDealerTypeAndSp.get(salesPlanSp.category__c+'@@'+salesPlanSp.Territory_Code__c);
		            	tempSpecialtyList.add(salesPlanSp);
		            	spSpecialtyMap.put(salesPlanSp.category__c+'@@'+salesPlanSp.Territory_Code__c,tempSpecialtyList);
		            	
		            }else{
		            	tempSpecialtyList=new List<Sales_Planning__c>();
		            	tempSpecialtyList.add(salesPlanSp);
		            	spSpecialtyMap.put(salesPlanSp.category__c+'@@'+salesPlanSp.Territory_Code__c,tempSpecialtyList);
		            }	
       		}
         }       
        for(String key:spSpecialtyMap.keyset()){
        	tempSpecialtyList1 = new List<Sales_Planning__c>();
        	Double tempSpL3m=0;
        	Double tempSpLycm=0;
        	tempSpecialtyList1	= mapDealerTypeAndSp.get(key);
        	for(Sales_Planning__c sp1:tempSpecialtyList1){
        		if(sp1.Value_L3M__c!=null){
        			tempSpL3m = tempSpL3m + sp1.Value_L3M__c;
        		}else{
        			tempSpL3m = tempSpL3m + 0;
        		}
        		if(sp1.Value_LYCM__c!=null){
        			tempSpLycm = tempSpLycm + sp1.Value_LYCM__c;
        		}else{
        			tempSpLycm = tempSpLycm + 0;
        		}
        	}
        	spL3mMap.put(key,tempSpL3m);
        	spLycmMap.put(key,tempSpLycm);
        }
        system.debug('=====mapOFDealerCatWiseRep===='+mapOFDealerCatWiseRep);
        for(Sales_Planning__c spAccId : SpList){
            acciD.add(spAccId.Dealer__c);
            st = spAccId.category__c+'@@'+spAccId.Dealer__r.Customer_Segment__c+'@@'+spAccId.Territory_Code__c;
           
            if(mapDealerTypeAndSp.containsKey(st)){    
            	tempSpList	= mapDealerTypeAndSp.get(st);
            	tempSpList.add(spAccId);
            	mapDealerTypeAndSp.put(st,tempSpList);
            	
            }else{
            	tempSpList=new List<Sales_Planning__c>();
            	tempSpList.add(spAccId);
            	mapDealerTypeAndSp.put(st,tempSpList);
            }
        }
       system.debug('mapDealerTypeAndSp--'+mapDealerTypeAndSp.size());
       
        for(String st1:mapDealerTypeAndSp.keyset()){
        	List<Sales_Planning__c> tempSpList1=new List<Sales_Planning__c>();
        	String temp='';
        	Integer totalL3M=0;
        	Integer totalLYCM=0;
        	Integer totalValueL3M=0;
        	Integer totalValueLYCM=0;
        	Integer totalPlanned=0;
        	Integer totalActual=0;
        	Integer totalMtd=0;
        	Integer plannedCount=0;
        	Integer actualCount=0;
        	
        	tempSpList1 = mapDealerTypeAndSp.get(st1);
        	system.debug('tempSpList1----'+tempSpList1.size());
        	system.debug('st1-------'+st1);
        	for(Sales_Planning__c sp:tempSpList1){
        		plannedCount = plannedCount + 1;
        		if(Integer.valueof(sp.L3M__c) >=0){
        			totalL3M  		 = totalL3M + Integer.valueof(sp.L3M__c);
        			system.debug('----sp.L3M__c-----'+sp.L3M__c);
        		}else{
        			totalL3M  		 = totalL3M + 0;
        		}
        		if(Integer.valueof(sp.Value_L3M__c) >=0){
        			totalValueL3M  		 = totalValueL3M + Integer.valueof(sp.Value_L3M__c);
        			
        		}else{
        			totalValueL3M  		 = totalL3M + 0;
        		}
        		if(Integer.valueof(sp.Value_LYCM__c) >=0){
        			totalValueLYCM  		 = totalValueLYCM + Integer.valueof(sp.Value_LYCM__c);
        			
        		}else{
        			totalValueLYCM  		 = totalValueLYCM + 0;
        		}
        		if(sp.LYCM__c >=0){
        			totalLYCM  		 = totalLYCM + Integer.valueof(sp.LYCM__c);
        			system.debug('----sp.LYCM__c-----'+sp.LYCM__c);
        		}else{
        			totalLYCM  		 = totalLYCM + 0;
        		}        		
        		if(sp.Total_planned__c >=0){
        			totalPlanned     = totalPlanned + Integer.valueof(sp.Total_planned__c);
        			totalMtd   		 = totalMtd + (Integer.valueof(sp.Skew_Final__c) * Integer.valueof(sp.Total_planned__c));
        		}else{
        			totalPlanned     = totalPlanned + 0;
        			totalMtd   		 = totalMtd + (Integer.valueof(sp.Skew_Final__c) * 0);
        		}        		        		
        		if(Integer.valueof(sp.Actual_Quantity__c) > 0){
        			actualCount = actualCount + 1;
        			totalActual = totalActual + Integer.valueof(sp.Actual_Quantity__c);
        		}else{        			
        			totalActual = totalActual + 0;
        		}
        	}
        	temp= totalL3M+'@@'+totalLYCM+'@@'+totalPlanned+'@@'+totalMtd+'@@'+plannedCount+'@@'+actualCount+'@@'+totalActual+'@@'+totalValueL3M+'@@'+totalValueLYCM;
        	system.debug('----totalL3M-----'+totalL3M+'---totalLYCM--'+totalLYCM);
        	mapTotalsAndSp.put(st1,temp);
        }
        system.debug('mapTotalsAndSp-----'+mapTotalsAndSp);
        String accQuery='Select Id';
        for(Dealer_Type_Field_Matching__c dtfmcs:dtfm){
            accQuery=accQuery+ ', '+dtfmcs.Dealer_Field_API_Name__c;
        }
        
        accQuery=accQuery+' from Account where id IN: acciD';
        AccountSet=Database.query(accQuery);
        for(Account m:AccountSet){
            mapAccount.put(m.Id, m);
        }
        List<Dashboard_Score__c> dsnewList=new List<Dashboard_Score__c>();
        
        for(Dashboard_Weightage_Master__c d:dwm){
        	String[] key;
        	String[] values;
        	String temp = '';
            for(String st2:mapTotalsAndSp.keySet()){
            	//category__c + Customer_Segment__c + Territory_Code__c
            	key=st2.split('@@');	
            	
                if(Integer.valueof(categoryCodeMap.get(d.Category__c)) == Integer.valueof(key[0]) && d.Role__c=='TL_Replacement'){
                    boolean paramMatch=false;
                  
                    /*Account a=new Account();
                    a=mapAccount.get(sp.Dealer__c);
                    
                    for(Dealer_Type_Field_Matching__c d1:dtfm){
                        String strParam=(String)a.get(d1.Dealer_Field_API_Name__c);
                        system.debug('strParam..............'+strParam+'......'+d1.Dealer_Field_API_Name__c);
	                    if(strParam!=null){      
	                    if((d.Parameters_Output__c).containsIgnoreCase(strParam)){
	                            paramMatch=true;
	                            break;
	                        }
	                      }
                    }*/                   
                    if((d.Parameters_Output__c).containsIgnoreCase(key[1])){
	                            paramMatch=true;	                            
	                        }  
	                 // Creating Dashboard Score Record                  
                    if(userTerritoryMap.get(key[2]) != null && paramMatch){
                    	temp=mapTotalsAndSp.get(st2);
                    	//totalL3M + totalLYCM + totalPlanned + totalMtd + plannedCount + actualCount + totalActual +totalValueL3M +totalValueLYCM
                    	values=temp.split('@@');
                        Dashboard_Score__c ds=new Dashboard_Score__c();
                        ds.User__c							= userTerritoryMap.get(key[2]);
                        ds.Dashboard_Weightage_Master__c	= d.Id;
                        ds.Territory_Code__c				= key[2];
                        ds.Month__c							= currentMonth;
                        ds.Year__c							= currentYear;
                        ds.Parameters__c					= key[1];
                        ds.Category__c						= key[0];                       
                        ds.L3M__c 							= Integer.valueof(values[0]);
                        ds.LYCM__c							= Integer.valueof(values[1]);
                        ds.Value_L3M__c 							= Integer.valueof(values[7]);
                        ds.Value_LYCM__c							= Integer.valueof(values[8]);
                        
                        ds.No_of_Dealer_Distributor_Plan__c			= Integer.valueof(values[4]);
                        ds.No_of_Dealer_Distributor_Billed_Since__c	=Integer.valueof( values[5]);                        
                        ds.Monthly_Target__c				= Integer.valueof(values[2]);                                             
                        ds.MTD_Target__c					= Integer.valueof(values[3]);
                        ds.Actual_MTD__c					= Integer.valueof(values[6]);  
                        // Category__c  & Parameters__c  &  Territory_Code__c  &  Text(Month__c) &  Text(Year__c)      
                        ds.DBExternalId__c 				    = key[0] + key[1] + key[2] + currentMonth + currentYear;     
                        dashboardOutputKeySet.add(key[0]+key[2]+currentMonth+currentYear);           
                        dsnewList.add(ds);
                    }
                }
            }            
        }
        if(dsnewList.size() > 0){
                try{
                		system.debug('----------------dsnewList==================='+dsnewList);
                  		database.upsert(dsnewList,Dashboard_Score__c.Fields.DBExternalId__c,false) ;
                  }catch(Exception e){
                      System.debug(e.getMessage());
                  }
        }
        
        List<Dashboard_Score__c> dsnewListTemp=new List<Dashboard_Score__c>();
        List<Dashboard_Output_Score__c> dos=new List<Dashboard_Output_Score__c>();
        
        dsnewListTemp = [Select Category__c,Value_LYCM__c,Value_L3M__c,Dashboard_Weightage_Master__c,User__c,DBExternalId__c,Actual_MTD__c,MTD_Target__c,Monthly_Target__c,Parameters__c,L3M__c,Territory_Code__c,LYCM__c,No_of_Dealer_Distributor_Plan__c,No_of_Dealer_Distributor_Billed_Since__c from Dashboard_Score__c where SYS_Used_IN_DashboardOutput__c IN : dashboardOutputKeySet];
        
        if(dsnewListTemp.size()>0){
        for(Dashboard_Weightage_Master__c dm:dwm){
         	for(String terr:userTerritoryMap.keyset()){           
           		Dashboard_Output_Score__c doObj=new Dashboard_Output_Score__c();
	              // Creating Dashboard Output Score Record         
	            for(Dashboard_Score__c d:dsnewListTemp){	            	
	                if(terr == d.Territory_Code__c && Integer.valueof(categoryCodeMap.get(dm.Category__c))== Integer.valueof(d.Category__c)){
	                    system.debug('....terr..inside if..........'+terr);
	                    doObj.User__c										= d.User__c;
	                    doObj.Category__c									= d.Category__c;
	                    doObj.Territory_Code__c								= terr;
	                    doObj.Dashboard_Weightage_Master__c 				= d.Dashboard_Weightage_Master__c;
	                    doObj.DBExternalId__c								= d.Category__c + terr +  currentMonth + currentYear;
	                    if(doObj.Total_L3M__c==null){
	                    	doObj.Total_L3M__c = 0;
	                    }
	                    if(doObj.Total_LYCM__c==null){
	                    	doObj.Total_LYCM__c = 0;
	                    }
	                    if(doObj.Total_No_of_dealers_distributors_planned__c==null){
	                    	doObj.Total_No_of_dealers_distributors_planned__c = 0;
	                    }
	                    if(doObj.Total_dealers_distributors_billed_since__c==null){
	                    	doObj.Total_dealers_distributors_billed_since__c = 0;
	                    }	                    
	                    if(doObj.Total_Actual_MTD__c==null){
	                        doObj.Total_Actual_MTD__c=0;
	                    }
	                    if(doObj.Total_MTD_Target__c==null){
	                        doObj.Total_MTD_Target__c=0;
	                    }
	                    if(doObj.Total_Value_L3M__c==null){
	                        doObj.Total_Value_L3M__c=0;
	                    }
	                    if(doObj.Total_Value_LYCM__c==null){
	                        
	                        doObj.Total_Value_LYCM__c=0;
	                    }
	                    if( d.Value_L3M__c==null){
	                         d.Value_L3M__c=0;
	                    }
	                    if( d.Value_LYCM__c ==null){
	                        d.Value_LYCM__c =0;
	                    }
	                    doObj.Total_L3M__c									= doObj.Total_L3M__c + d.L3M__c;
	                    doObj.Total_LYCM__c									= doObj.Total_LYCM__c + d.LYCM__c;
	                    if(spL3mMap.get(d.Category__c+'@@'+terr)!=null){
	                  	  doObj.Total_Value_L3M__c							= doObj.Total_Value_L3M__c + d.Value_L3M__c + spL3mMap.get(d.Category__c+'@@'+terr) ;
	                   	
	                    }else{
	                    	doObj.Total_Value_L3M__c							= doObj.Total_Value_L3M__c + d.Value_L3M__c + 0 ;	                   
	                    }
	                    if(spL3mMap.get(d.Category__c+'@@'+terr)!=null){
	                  	   doObj.Total_Value_LYCM__c							= doObj.Total_Value_LYCM__c + d.Value_LYCM__c + spL3mMap.get(d.Category__c+'@@'+terr);	                   	
	                    }else{
	                    	 doObj.Total_Value_LYCM__c							= doObj.Total_Value_LYCM__c + d.Value_LYCM__c + 0;
	                   
	                    }	                    
	                    doObj.Total_No_of_dealers_distributors_planned__c	= doObj.Total_No_of_dealers_distributors_planned__c + d.No_of_Dealer_Distributor_Plan__c;	                    
	                    doObj.Total_dealers_distributors_billed_since__c	= doObj.Total_dealers_distributors_billed_since__c + d.No_of_Dealer_Distributor_Billed_Since__c;
	                    
	                    doObj.Total_Actual_MTD__c							= doObj.Total_Actual_MTD__c + d.Actual_MTD__c;
	                    doObj.Total_MTD_Target__c							= doObj.Total_MTD_Target__c + d.MTD_Target__c;    
	                }
	            }	            
            if(doObj.Total_Actual_MTD__c!=null && doObj.Total_MTD_Target__c!=null){
            	system.debug('doObj............'+doObj.Total_Actual_MTD__c+'...........'+doObj.Total_MTD_Target__c);
            	if(doObj.Total_MTD_Target__c==0){
            		doObj.Achievement_MTD__c=0;
            	}else{
            		doObj.Achievement_MTD__c=(doObj.Total_Actual_MTD__c)*100/doObj.Total_MTD_Target__c;	
            	}	            
	            dos.add(doObj);
            }           
          }
        }
        }		
		if(dos.size() > 0){
                try{
                		system.debug('===============dos==================='+dos);
                  		database.upsert (dos,Dashboard_Output_Score__c.Fields.DBExternalId__c,false) ;
                  }catch(Exception e){
                      System.debug(e.getMessage());
                  }
         }        
     }
     global void finish(Database.BatchableContext BC) {
        //put your email address to get exception notifications
    }   
}