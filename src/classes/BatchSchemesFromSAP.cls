global class BatchSchemesFromSAP implements Database.Batchable<sap_disc_master.ZsdDiscMaster> { 

    public sap_disc_master.ZsfdcDiscMasterResponse_element res {get;set;}
    public list<Scheme__c> schemeList {get;set;}
    
    public BatchSchemesFromSAP(sap_disc_master.ZsfdcDiscMasterResponse_element x) {
        res = x;        
    }
    public Iterable<sap_disc_master.ZsdDiscMaster> start(Database.BatchableContext BC)
    { 
		return res.ItOutput.item;
    }
    public void execute(Database.BatchableContext BC,list<sap_disc_master.ZsdDiscMaster>  scope)
    {
        schemeList = new list<Scheme__c>();
        Scheme__c scheme;
        
        for(sap_disc_master.ZsdDiscMaster dm : scope) {
        	system.debug('response---'+dm);
        	 scheme = new Scheme__c();
        	 
        	 
        	 scheme.Circular_Number__c 				= dm.Srnum ;
        	 scheme.Discount_name__c 				= dm.Sapdisc ;
        	 scheme.Discount_Subcode__c 			= dm.Subcod;
        	 scheme.Scheme_Narration__c 			= dm.Zscheme;
        	 scheme.Short_Text_for_Fixed_Values__c  = dm.Srnum;
        	 scheme.Start_Date__c 					= date.valueof(dm.Begda1);
        	 scheme.End_Date__c 					= date.valueof(dm.Endda);
        	 
        	 schemeList.add(scheme);
        }
        
        if(schemeList.size()>0)
        Database.upsert(schemeList,Scheme__c.Fields.Circular_Number__c,false);  
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }

}