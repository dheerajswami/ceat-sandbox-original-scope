global class Scheduled_BatchForDashboardOutputTerr implements Schedulable{
   @TestVisible private integer batchsize = 1; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest())
      {
           batchsize = 2000;
      } 
      SP_BatchForDashboardOutputTerr dashBatch= new SP_BatchForDashboardOutputTerr(); 
      database.executebatch(dashBatch,batchsize);
   }
}