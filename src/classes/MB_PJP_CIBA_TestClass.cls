/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class MB_PJP_CIBA_TestClass {

	private static List<Task> tasks;	
	private static List<Task> tasksWithouId;	
	private static PJP__c pjp;
	private static List<Visits_Plan__c> visits;
	private static List<Visits_Plan__c> visitsWithoutId;
	private static User rmUser;
	private static User tlUser;
	private static User tldUser;
	private static User speacialityUser;
	
	
	public static void initializeTestData(String usertype){

		String tempNumber = String.valueOf(Integer.valueOf(Math.random()*1000));
		
		Map<String,Id> roleId 		= new Map<String,Id>();
		Map<String,Id>  profileIds 	= new Map<String,Id>();
		
		for(UserRole role : [SELECT Id,Name FROM UserRole]){
			roleId.put(role.Name,role.Id);
		}
		
		for(Profile prof : [SELECT Id,Name FROM Profile]){
			profileIds.put(prof.Name,prof.Id);
		}

		rmUser = CEAT_InitializeTestData.createUserPJP('RM'+tempNumber,'TEST','testrm.'+tempNumber+'@test.com','testrm.'+tempNumber+'@test.com',tempNumber,profileIds.get('Replacements'),null,roleId.get('RM - Replacement'));
		database.insert(rmUser);

		tlUser = CEAT_InitializeTestData.createUserPJP('TL'+tempNumber,'TEST','test.'+tempNumber+'@test.com','test.'+tempNumber+'@test.com',tempNumber+'TL',profileIds.get('Replacements'),rmUser.Id,roleId.get('TL - Replacement'));
		database.insert(tlUser);

		tldUser = CEAT_InitializeTestData.createUserPJP('TLD'+tempNumber,'TEST','testtld.'+tempNumber+'@test.com','testtld.'+tempNumber+'@test.com',tempNumber+'TLD',profileIds.get('Replacements'),rmUser.Id,roleId.get('TLD'));
		database.insert(tldUser);

		speacialityUser = CEAT_InitializeTestData.createUserPJP('SP_TLD'+tempNumber,'SP_TEST','Sp_testtld.'+tempNumber+'@test.com','sp_testtld.'+tempNumber+'@test.com',tempNumber+'sp_TLD',profileIds.get('Specialty'),rmUser.Id,roleId.get('AM Specialty'));
		database.insert(speacialityUser);


		if(usertype == 'TLD'){
			system.runAs(tldUser){

				Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
		        database.upsert(counterL);
		        
		        Account account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
		        database.insert(account);     

		        pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
		        database.insert(pjp);
		        
		        visits = new List<Visits_Plan__c>();
		        visits = CEAT_InitializeTestData.createMultipleVisits(pjp,account.Id,null,false);
		        database.insert(visits);

		        tasks = new List<Task>();
		        tasks = CEAT_InitializeTestData.createMultipleTasks(account.Id,null,tlUser.Id,false);
		        database.insert(tasks);
			
			}
		}else if(usertype == 'TL'){
			system.runAs(tlUser){

				Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
		        database.upsert(counterL);
		        
		        Account account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
		        database.insert(account);     

		        pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
		        database.insert(pjp);
		        
		        visits = new List<Visits_Plan__c>();
		        visits = CEAT_InitializeTestData.createMultipleVisits(pjp,account.Id,null,false);
		        database.insert(visits);

		        tasks = new List<Task>();
		        tasks = CEAT_InitializeTestData.createMultipleTasks(account.Id,null,tlUser.Id,false);
		        database.insert(tasks);
			
			}
		}else if(usertype == 'Specialty'){
			system.runAs(speacialityUser){

				Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
		        database.upsert(counterL);
		        
		        Account account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
		        database.insert(account);     

		        pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
		        database.insert(pjp);
		        
		        visits = new List<Visits_Plan__c>();
		        visits = CEAT_InitializeTestData.createMultipleVisits(pjp,account.Id,null,false);
		        database.insert(visits);

		        tasks = new List<Task>();
		        tasks = CEAT_InitializeTestData.createMultipleTasks(account.Id,null,speacialityUser.Id,false);
		        database.insert(tasks);
			
			}
		}
	
	
	}
    
	// TL Users

	static testMethod void myUnitTestWithTimeStamp() {
    	Test.startTest();
    		initializeTestData('TL');
	    	system.runAs(tlUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void myUnitTestWithOutTimeStamp() {
    	Test.startTest();
    		initializeTestData('TL');
	    	system.runAs(tlUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',null);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void myUnitTestTLexception() {
    	Test.startTest();
    		initializeTestData('TL');
	    	system.runAs(tlUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	
		    	for(Visits_Plan__c vp : visits){
		    		vp.Master_Plan__c = null;
		    	}

		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);


		    	
		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void myUnitTestTLexception2() {
    	Test.startTest();
    		initializeTestData('TL');
	    	system.runAs(tlUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();

		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);

		    	List<PJP__c> pjps = [SELECT Id,Sys_ApprovalStatus__c FROM PJP__c];
		    	for(PJP__c acc : pjps){
		    		acc.Sys_ApprovalStatus__c = 'Submitted';
		    	}
		    	
		    	update pjps;

		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void cibaSyncDataSetting() {
    	Test.startTest();
    		initializeTestData('TL');
	    	system.runAs(tlUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);	
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBASettings'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Settings.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }


    // FOR TLD

    static testMethod void myUnitTestTLD() {
    	Test.startTest();
    		initializeTestData('TLD');
	    	system.runAs(tldUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',null);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }


    static testMethod void myUnitTestWithTimeStampTLD() {
    	Test.startTest();
    		initializeTestData('TLD');
	    	system.runAs(tldUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void cibaSyncDataSettingTLD() {
    	Test.startTest();
    		initializeTestData('TLD');
	    	system.runAs(tldUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);	
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBASettings'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Settings.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    // SPEACILITY

    static testMethod void myUnitTestSpeciality() {
    	Test.startTest();
    		initializeTestData('Specialty');
	    	system.runAs(speacialityUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',null);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }


    static testMethod void myUnitTestWithTimeStampSpeciality() {
    	Test.startTest();
    		initializeTestData('Specialty');
	    	system.runAs(speacialityUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBAData'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Data.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }

    static testMethod void cibaSyncDataSettingSpecialty() {
    	Test.startTest();
    		initializeTestData('Specialty');
	    	system.runAs(speacialityUser){
	    		MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
		    	mobReq.visitsPlans.addAll(visits);
		    	//mobReq.visitsPlans.addAll(visitsWithoutId);
		    	mobReq.tasks.addAll(tasks);
		    	//mobReq.tasks.addAll(tasksWithouId);
		    	
		    	String josn = JSON.serialize(mobReq);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);	
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCIBASettings'; 
		        RestContext.request.httpMethod = 'POST';
		        
		        MB_PJP_CIBA_Settings.getMobileDataPJP();
		    }
    	Test.stopTest();
       
    }
}