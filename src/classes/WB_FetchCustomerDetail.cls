/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||         Modified by : Neha Mishra
||        Purpose: get Customer based on Mobile number used for CTI  
||         version 3 : by kishlay 
||         Added Customer Name and Salesforce baseURL in response.
||    
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchCustomerId/*')

global with sharing class WB_FetchCustomerDetail{
    
    global class OutPutData{
        public String customer_sf_id     {get;set;}
        public String customerSfId_flag  {get;set;}
        public String registration_flag  {get;set;}
        public String customer_sap_code  {get;set;}
        public String preferred_lang     {get;set;}
        public String customer_Name      {get;set;}
        public String sfdcBaseURL        {get;set;}
    }
    
    @HttpGet
    global static OutPutData getCustomerDetails(){
        RestRequest req         = RestContext.request;
        RestResponse res        = RestContext.response;
        String customer_phone   = req.params.get('CustomerPhone');
        String customer_type   = req.params.get('CustomerType');
        try{
            OutPutData result = new OutPutData();
            if(customer_phone != null){
                customer_phone = customer_phone.replaceAll('[+,.\\[\\]()-]','');
                if(customer_type == 'D'){
                    List<List<SObject>> searchList = new List<List<SObject>>();
                    
                    String query = 'FIND \''+customer_phone+'\' IN ALL FIELDS RETURNING  Account (Id,Name,KUNNR__c,Preferred_Language__c),Contact(Id,Name,AccountId,Account.KUNNR__c,Account.Preferred_Language__c)';
                    searchList = search.query(query); 
                    
                    List<Account> accounts = new List<Account>();
                    List<Contact> contacts = new List<Contact>();
                    String sapCode = '';
                    String sfId = '';
                    String sfIdFlag = '';
                    String sfaccName = '';
                    
                    if(!searchList.isEmpty()){
                        accounts = (List<Account>)searchList[0];
                        contacts = (List<Contact>)searchList[1];
                        
                        if(!accounts.isEmpty()) {
                            result.customer_sf_id       = accounts[0].Id;
                            result.registration_flag    = 'Y';
                            sfIdFlag                    = 'S';
                            result.customer_Name        = accounts[0].Name;
                            for(Account tmpAcc : accounts) {
                                if(sfId == '') {
                                    sfId = tmpAcc.Id;
                                    sfaccName = tmpAcc.Name;
                                }else {
                                    sfId += ';' + tmpAcc.Id;
                                    sfaccName += ';' + tmpAcc.Name;
                                    sfIdFlag = 'M';
                                    
                                }
                                if(tmpAcc.KUNNR__c != NULL) {
                                    if(sapCode == '') {
                                        sapCode = tmpAcc.KUNNR__c; 
                                    }else {
                                        sapCode += ';' + tmpAcc.KUNNR__c;
                                    }    
                                }
                            }
                            result.customer_sf_id       = sfId;
                            result.customerSfId_flag    = sfIdFlag;
                            result.customer_sap_code    = sapCode;
                            result.preferred_lang       = (accounts[0].Preferred_Language__c != null ? accounts[0].Preferred_Language__c : '');
                            result.customer_Name        = sfaccName;
                            result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/'+accounts[0].Id;
                        }else if(!contacts.isEmpty()){
                            result.customer_sf_id       = contacts[0].AccountId;
                            result.registration_flag    = 'Y';
                            result.customer_Name        = contacts[0].Name;
                            for(Contact tmpCon : contacts) {
                                if(tmpCon.Account.KUNNR__c != NULL) {
                                    if(sapCode == '') {
                                        sapCode = tmpCon.Account.KUNNR__c;
                                    }else {
                                        sapCode += ';' + tmpCon.Account.KUNNR__c;
                                    }
                                }
                            }
                            result.customer_sap_code    = sapCode;
                            result.customerSfId_flag    = 'S';
                            //result.customer_sap_code    = (contacts[0].Account.KUNNR__c != null ? contacts[0].Account.KUNNR__c : '');
                            result.preferred_lang       = (contacts[0].Account.Preferred_Language__c != null ? contacts[0].Account.Preferred_Language__c : '');
                             result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/'+accounts[0].Id;
                           
                            
                        }else{
                            
                            result.customer_sf_id       = '';
                            result.customerSfId_flag    = '';
                            result.registration_flag    = 'N';
                            result.customer_sap_code    = '';
                            result.preferred_lang       = '';
                            result.customer_Name        = '';
                            result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                        }
                        
                    }/*else{
                        result.customer_sf_id       = '';
                        result.customerSfId_flag    = '';
                        result.registration_flag    = 'N';
                        result.customer_sap_code    = '';
                        result.preferred_lang       = '';
                    }
                    
                    system.debug(searchList);*/
                   
                    return result;
                }
                else if(customer_type == 'E'){
                    try{
                        List<Employee_Master__c> emp =   [select Id,Name,EmployeeId__c,Preferred_Language__c from Employee_Master__c where Mobile__c = :customer_phone];
                        if(emp != null){
                            result.registration_flag    = 'Y';
                            if(emp.size() == 1){
                                result.customerSfId_flag    = 'S';
                                result.customer_sf_id       = emp[0].Id;
                                result.customer_sap_code    = emp[0].EmployeeId__c;
                                result.preferred_lang       = emp[0].Preferred_Language__c;
                                result.customer_Name        = emp[0].Name;
                                
                               
                               result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/'+emp[0].Id;
                                }
                            
                            else{
                                result.customerSfId_flag    = 'M';
                                String empCodes = '';
                                String empIds = '';
                                for(Employee_Master__c eRec : emp){
                                    empCodes = empCodes + eRec.EmployeeId__c + ';';
                                    empIds = empIds + eRec.Id + ';';
                                }
                                result.customer_sf_id       = empIds;
                                result.customer_sap_code    = empCodes;
                                result.preferred_lang       = emp[0].Preferred_Language__c;
                                result.customer_Name        = emp[0].Name;
                              
                               result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/'+emp[0].Id;
                               
                            }
                            
                        }
                    }
                    catch(Exception e){
                        result.customer_sf_id       = '';
                        result.customerSfId_flag    = '';
                        result.registration_flag    = 'N';
                        result.customer_sap_code    = '';
                        result.preferred_lang       = '';
                        result.customer_Name        = '';
                        result.sfdcBaseURL          = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                        system.debug(e.getMessage());
                    }
                    return result;
                }
                
                else{
                    return null;
                }
            }
            else{
                return null;
            }
            
            
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            return null; 
        }
        
        
    }
}