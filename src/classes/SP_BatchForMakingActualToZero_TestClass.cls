@isTest(SeeAllData=False)
 public class SP_BatchForMakingActualToZero_TestClass{

  public  static String query;
  public  static String month_val;
  public  static String year_val;
  
  Static testMethod void SalesPlanningObjectTest(){
      
             Sales_Planning__c Sp = new Sales_Planning__c();
             sp.Actual_Quantity__c = 2;
             sp.Actual_Sales_Value__c=3000;
             sp.Month__c= String.valueOf(Date.Today().Month());
             sp.Year__c= String.valueOf(Date.Today().Year());
             insert sp;
                
             test.startTest();
             SP_BatchForMakingActualValueToZero d = new SP_BatchForMakingActualValueToZero();
             Database.executeBatch(d);
             test.stopTest();
   }


   Static testMethod void SalesPlanningObjectTest1(){
          
          month_val = String.valueOf(Date.Today().Month()-1);
          year_val = String.valueOf(Date.Today().Year()-1);
        
             Sales_Planning__c Sp = new Sales_Planning__c();
             sp.Actual_Quantity__c = 1;
             sp.Actual_Sales_Value__c=1000;
             sp.Month__c= month_val;
             sp.Year__c= year_val;
             insert sp;
         
             test.startTest();
             SP_BatchForMakingActualValueToZero d = new SP_BatchForMakingActualValueToZero();
             Database.executeBatch(d);
             test.stopTest();
    }
 }