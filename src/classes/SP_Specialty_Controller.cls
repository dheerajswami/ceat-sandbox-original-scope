public with sharing class SP_Specialty_Controller { 
    public String loggedInUserId    {get;set;}
    public String loggedInUserName { get; set; }
    public String loggedInTerritory  {get;set;}
    public Datetime myDatetime ;
    public String myDatetimeStr {get;set;}
    list<UserTerritory2Association> userTerrCode = new list<UserTerritory2Association>();
    list<Territory2> userTerritory = new list<Territory2>();
   
     public SP_Specialty_Controller() {
        myDatetime = Datetime.now();
        myDatetimeStr = myDatetime.format('MMMM, yyyy');
        String rmSpeLabel          = System.Label.Specialty_RM_Role;
        loggedInUserId=ApexPages.currentPage().getParameters().get('id');
        // String url=ApexPages.currentPage().getURL(); 
        // system.debug('-------------------------url--'+url);
        if(loggedInUserId=='' || loggedInUserId==null ){
            loggedInUserId=UserInfo.getUserId();
        }
        system.debug(loggedInUserId+'loggedInUserId');
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId and (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole) limit 1];
        if(userTerrCode.size() > 0){
            userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id limit 1];
        }
        if(userTerritory.size() > 0){
            loggedInTerritory=userTerritory[0].Name;
        }
        
     } 
    //Used to get list of TL or Dealer depending on which user logged in
    //List<SP_HandlerForSP_Specialty_Controller.tlWrapper>
    @RemoteAction
    Public Static List<SP_HandlerForSP_Specialty_Controller.dlWrapper>  getTLsOfRM (String loggedInUserId) {
            SP_HandlerForSP_Specialty_Controller handler = new SP_HandlerForSP_Specialty_Controller();
            return handler.getRMWiseSalesPlanningDealerRecords(loggedInUserId);
        
    }
 
    
    @RemoteAction
    Public Static void saveDLSpRecord (Map<String,String> mapOfSalesPlanningValue,Map<String,String> mapOfSalesPlanning,String loggedInUserId) {
            system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
            system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
            Date d = system.today();
            Datetime myDatetime = Datetime.now();
            String myDatetimeStr = myDatetime.format('MMMM, yyyy');
            String month       = String.valueOf(d.month());
            String year       = String.valueOf(d.Year());
            String rmSpeLabel          = System.Label.Specialty_RM_Role;
            String rmLabel          = System.Label.RM_Role;
            String SpecialtyDealerLabel     = System.Label.Specialty;
            String Specialty_Forcast            = System.Label.Specialty_Forcast;
            String Specialty_Forcast_Zone       = System.Label.Specialty_Forcast_Zone;
            String specialtyDealerCat           = '';
            String loggedInTerritory;
            Server_Url__c serverUrl;
            String gmLabel                 =System.Label.gm_label_terr;
            String sprmLabel             = system.label.RM_Specialty;
            String spRMpageName                                = System.Label.Sales_Planning_Sp_RM_Page;
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,String> mapOfDLIDAndValue            = new Map<String,String>();
            Map<String,ID> mapOfCatAndROId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOfRegAndRegCode           = new map<String,String>();
            Map<String,String> mapOfAccAndRegCode           = new map<String,String>();
            Map<String,id> mapOfTerritoryAndUserID          = new Map<String,ID>();
            Map<String,String> mapOfCatNameAndCatCode       = new map<String,String>();
            Map<String,ID> mapOfCatAndZoneId                = new map<String,ID>();
            Map<String,ID> mapOfAccAndAccID                 = new map<String,ID>();
            List<Sales_Planning__c> listOfSalesPlanningDL   = new List<Sales_Planning__c>();
            List<UserTerritory2Association> zoneForRM       = new List<UserTerritory2Association>();
            List<Sales_Planning__c> listOfSalesPlanningDLValue       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> regionForRM       = new List<UserTerritory2Association>();
            List<UserTerritory2Association> regionForGM       = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList     = new List<Sales_Planning__c>();
            List<Sales_Planning__c> upsertsalesPlanningDLListForVal     = new List<Sales_Planning__c>();
            List<Account> listOFAccForRO     = new List<Account>();
            Locking_Screen__c lsforRM= new Locking_Screen__c();
            List<Sales_Planning__c> salesPlanningRO         = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked           = new list<Locking_Screen__c>(); 
            List<Territory2> listOfTerr               = new List<Territory2>();
            List<Territory2> zoneTerritory            = new List<Territory2>();
            List<Territory2> gmTerritory            = new List<Territory2>();
            List<Sales_Planning__c> salesPlanningSpeForcastZone             = new List<Sales_Planning__c>();
           // List<UserRole> userRMRol                                        = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName =: sprmLabel]; 
            Sales_Planning__c spDLRecord;
            Account dealer; 
            set<String> setOFDLNUmber                   = new set<String>();
            Set<String> salesPlanningDLId               = new Set<String>();
            Set<String> salesPlanningDLIdValue          = new Set<String>();
            Set<String> regionCode                      = new Set<String>();
            Set<Id> regionId                            = new Set<Id>();
            Set<Id> zoneId                              = new Set<Id>();
            set<String> zoneCode                        = new set<String>();
            Set<String> catSet                          = new set<String>();
            Id specialtyDealerId            = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: SpecialtyDealerLabel Limit 1].Id;
            Id specialtyForcastROId         = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast Limit 1].Id;
            Id specialtyForcastZoneId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast_Zone Limit 1].Id;
            List<Speciality_Sales_Planning_Categories__c> spc = Speciality_Sales_Planning_Categories__c.getall().values();
                for(Speciality_Sales_Planning_Categories__c sp : spc) {
                if(sp.Include_in_Sales_Planning__c == true){
                    mapOfCatNameAndCatCode.put(sp.Name,sp.Category_Code__c);  
                    catSet.add(sp.Category_Code__c);   
                }     
            }
             List<UserRole> userGMRol = [SELECT DeveloperName,Id FROM UserRole WHERE  DeveloperName =: gmLabel ];        
       
            List<UserRole> userSpeRol  = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName  =: System.Label.SRM_SpecialtyUrole OR DeveloperName  =: System.Label.AM_SpecialtyURole OR DeveloperName  =: System.Label.RM_Specialty];
       
             //getting Sp. Zone for logged in user 
            if(userSpeRol.size() > 0 && (UserInfo.getUserRoleId() == userSpeRol[0].Id || UserInfo.getUserRoleId() == userSpeRol[1].Id || UserInfo.getUserRoleId() == userSpeRol[2].Id || UserInfo.getUserRoleId() == userGMRol[0].Id)){
              zoneForRM = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:loggedInUserId and (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole) ];
                for(UserTerritory2Association reg : zoneForRM){
                    zoneId.add(reg.Territory2Id);
                    zoneCode.add(reg.Territory2.name);
                    
                }
            
            }
             //Getting RM of loggedin TL to send email notification by submitting the sales plan
             zoneTerritory=  [Select ParentTerritory2Id,id,name from Territory2 where Id IN : zoneId];
             if(zoneTerritory.size()>0){
                loggedInTerritory =zoneTerritory[0].name;
             }
             gmTerritory=  [Select id,name from Territory2 where id =:zoneTerritory[0].ParentTerritory2Id];
             regionForGM = [Select RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where Territory2Id =:gmTerritory[0].id and RoleInTerritory2=: gmLabel];
             User gmUser=  [select name,id,email from User where id=:regionForGM[0].UserId];
             
            //getting all current month Zone forcast records for logged in user 
            salesPlanningSpeForcastZone = [SELECT Id,NBP__c,Discount_On_NBP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,Month__c,Zone__c,Region_code__c,Region_Description__c,SPExternalIDTL__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Zone__c IN:zoneCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specialtyForcastZoneId AND Category__c In :catSet];
            if(salesPlanningSpeForcastZone.size() > 0){
                for(Sales_Planning__c temp :salesPlanningSpeForcastZone){
                    mapOfCatAndZoneId.put(temp.Category__c,temp.id);
                }
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String[] listKey1 = temp.split('@@');
                    setOFDLNUmber.add(listKey1[3]);
                    
            }
            listOFAccForRO = [SELECT id,KUNNR__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c From Account /*WHERE KUNNR__c IN:setOFDLNUmber AND Active__c=true*/];
            for(Account acc : listOFAccForRO){
                if(acc.Sales_Group_Text__c != null){
                    mapOfAccAndRegCode.put(acc.id,acc.Sales_Group_Text__c);
                    //mapOfAccAndAccID.put(acc.KUNNR__c,acc.ID);
                }
                
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    String [] subStringDea;
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                    }
                    
                    else{
                            subStringDea = listKey[1].split(':');
                            if(mapOfAccAndRegCode.containsKey(subStringDea[1])){
                                st = subStringDea[1] + mapOfAccAndRegCode.get(subStringDea[1])+mapOfCatNameAndCatCode.get(listKey[2]) + month +year ;
                            }else{
                            st = subStringDea[1] +mapOfCatNameAndCatCode.get(listKey[2]) + month +year ;
                            }
                            //dealer = new account(KUNNR__c=listKey[3]);
                            String firstz = new List<String> (zoneCode).get(0);
                            spDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfCatAndZoneId.get(mapOfCatNameAndCatCode.get(listKey[2])),Category__c = mapOfCatNameAndCatCode.get(listKey[2]),Category_Description__c = listKey[2],
                            Month__c = month ,Year__c = year ,Zone__c = firstz,Dealer_Name__c= subStringDea[0],Dealer_CustNumber__c= listKey[3],Dealer__c = subStringDea[1],
                            RecordTypeId = specialtyDealerId,Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st,Value__c = Decimal.ValueOF(mapOfSalesPlanningValue.get(temp)));
                            
                            listOfSalesPlanningDLForInsert.add(spDLRecord);
                    }
                    system.debug(listOfSalesPlanningDLForInsert+'######');
                    
            
            }
            for(String temp : mapOfSalesPlanningValue.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLIdValue.add(listKey[0]);
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                            
                    }
            
            }
            listOfSalesPlanningDLValue = [SELECT id,SPExternalIDTL__c,Value__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLIdValue];
            system.debug(salesPlanningDLId+'salesPlanningDLId');
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            system.debug(listOfSalesPlanningDL+'listOfSalesPlanningDL');
            system.debug(mapOfSalesPlanningValue+'%%%%%');
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            //String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            
                            //sp.Value__c = Decimal.ValueOf(val);
                            sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            if(listOfSalesPlanningDLValue.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDLValue){
                            //String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            system.debug('-sp.SPExternalIDTL__c------------'+sp.SPExternalIDTL__c+'---------val------'+val);
                            sp.Value__c = Decimal.ValueOf(val);
                            //sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLListForVal.add(sp);
                    }
            }
            system.debug(upsertsalesPlanningDLList+'upsertsalesPlanningDLList');
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            system.debug(upsertsalesPlanningDLListForVal+'upsertsalesPlanningDLListForVal');
            if(upsertsalesPlanningDLListForVal.size() > 0 ){
                    upsert upsertsalesPlanningDLListForVal SPExternalIDTL__c;
            }
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }
            system.debug(mapOfSalesPlanning+'!!!!!!!!!!');
            
            alreadyLocked = [SELECT id,name,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND (status__c='Rejected' OR status__c='Submitted')];
            if(!(alreadyLocked.size() > 0)){
                
                lsforRM= new Locking_Screen__c(User__c = loggedInUserId,Month__c = month ,Year__c = year,Submitted__c = true,Status__c='Submitted',Territory_Code__c=loggedInTerritory, View_Sales_Planning__c='/apex/SP_Specialty?id='+loggedInUserId);
                insert lsforRM;
            }else{
                alreadyLocked[0].Status__c = 'Submitted';
                update alreadyLocked;
            }
            system.debug(alreadyLocked +'alreadyLocked');
            system.debug(lsforRM+'lsforRM');
            Id lockRecId=[select id from Locking_Screen__c where User__c =:loggedInUserId AND Month__c =:month AND Year__c =: year AND Submitted__c = true AND (status__c='Rejected' OR status__c='Submitted') LIMIT 1].Id;
            system.debug(lockRecId+'lockRecId');
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval...');
            req1.setObjectId(lockRecId);
           
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            //req2.setComments('Approving request.');
            //req2.setAction('Approve');
            req1.setNextApproverIds(new Id[] {gmUser.Id});
            Approval.ProcessResult result = Approval.process(req1);
            
             serverUrl=[select url__c from Server_Url__c limit 1];
             FeedItem fitem=new FeedItem();
             fItem.parentId=gmUser.Id;
             fItem.Title='Click here to Approve/Reject';
             fItem.body = 'Sales planning has been submitted for the month '+myDatetimeStr+' by Specialty Manager of '+loggedInTerritory+'. ';
             fItem.LinkUrl = serverUrl.url__c+'/apex/SP_Specialty?id='+loggedInUserId;
             insert fItem;
              
              
              system.debug(fItem+'fItem');
            /*
             Messaging.SingleEmailMessage mailHandler =new Messaging.SingleEmailMessage();
                        String name=UserInfo.getName();  
                        String[] mail=new String[]{gmUser.email};//
                        String Body = 'Sales planning has been submitted for the month '+month+' by Specialty RM '+UserInfo.getName()+'.<br><br>';
                  body+='To get details click on link <br><br>';
                  body+='<a href="https://c.cs5.visual.force.com/apex/SP_Specialty?id='+loggedInUserId+'">https://c.cs5.visual.force.com/apex/SP_Specialty?id='+loggedInUserId+'</a><br><br>';
                  body+='Regards,'+'<br><br>';
                  body+='Specialty RM<br>'+name;                
                 //mail=UserInfo.getUserEmail();
            mailHandler.setToAddresses(mail);
            mailHandler.setHtmlBody (body);
            mailHandler.setReplyTo(UserInfo.getUserEmail());
            mailHandler.setSubject('Sales planning has been submitted for the month '+month);
            try{
              Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailHandler});
            }
            catch(Exception e){
            } 
            system.debug('-------To-mail address'+mail); */
            
    }
    @RemoteAction
    Public Static void saveAsDraftSpRecord (Map<String,String> mapOfSalesPlanningValue,Map<String,String> mapOfSalesPlanning,String loggedInUserId) {
            system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
            system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
            system.debug(loggedInUserId+'loggedInUserId');
            Date d = system.today();
            String month       = String.valueOf(d.month());
            String year       = String.valueOf(d.Year());
            String rmSpeLabel          = System.Label.Specialty_RM_Role;
            String sprmLabel             = system.label.RM_Specialty;
            String SpecialtyDealerLabel     = System.Label.Specialty;
            String Specialty_Forcast            = System.Label.Specialty_Forcast;
            String Specialty_Forcast_Zone       = System.Label.Specialty_Forcast_Zone;
            String specialtyDealerCat           = '';            
            String gmLabel                 =System.Label.gm_label_terr;
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,String> mapOfDLIDAndValue            = new Map<String,String>();
            Map<String,ID> mapOfCatAndROId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOfRegAndRegCode           = new map<String,String>();
            Map<String,String> mapOfAccAndRegCode           = new map<String,String>();
            Map<String,id> mapOfTerritoryAndUserID          = new Map<String,ID>();
            Map<String,String> mapOfCatNameAndCatCode       = new map<String,String>();
            Map<String,ID> mapOfCatAndZoneId                = new map<String,ID>();
            Map<String,ID> mapOfAccAndAccID                 = new map<String,ID>();
            
            //Map<String,ID> mapOfAccId                = new map<String,ID>();
           // Map<String,ID> mapOfAccAndAccID                 = new map<String,ID>();
            List<Sales_Planning__c> listOfSalesPlanningDL   = new List<Sales_Planning__c>();
            List<UserTerritory2Association> zoneForRM       = new List<UserTerritory2Association>();
            List<Sales_Planning__c> listOfSalesPlanningDLValue       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> regionForRM       = new List<UserTerritory2Association>();
            List<UserTerritory2Association> regionForGM       = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList     = new List<Sales_Planning__c>();
            List<Sales_Planning__c> upsertsalesPlanningDLListForVal     = new List<Sales_Planning__c>();
            List<Account> listOFAccForRO     = new List<Account>();
            List<Sales_Planning__c> salesPlanningRO         = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked           = new list<Locking_Screen__c>(); 
            List<Territory2> listOfTerr               = new List<Territory2>();
            List<Territory2> zoneTerritory            = new List<Territory2>();
            List<Territory2> gmTerritory            = new List<Territory2>();
            List<Sales_Planning__c> salesPlanningSpeForcastZone             = new List<Sales_Planning__c>();
           // List<UserRole> userRMRol                                        = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName =: sprmLabel]; 
            Sales_Planning__c spDLRecord;
            Account dealer; 
            set<String> setOFDLNUmber                   = new set<String>();
            Set<String> salesPlanningDLId               = new Set<String>();
            Set<String> salesPlanningDLIdValue          = new Set<String>();
            Set<String> regionCode                      = new Set<String>();
            Set<Id> regionId                            = new Set<Id>();
            Set<Id> zoneId                              = new Set<Id>();
            set<String> zoneCode                       = new set<String>();
            Set<String> catSet                          = new set<String>();
            Id specialtyDealerId            = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: SpecialtyDealerLabel Limit 1].Id;
            Id specialtyForcastROId         = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast Limit 1].Id;
            Id specialtyForcastZoneId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast_Zone Limit 1].Id;
            List<Speciality_Sales_Planning_Categories__c> spc = Speciality_Sales_Planning_Categories__c.getall().values();
                for(Speciality_Sales_Planning_Categories__c sp : spc) {
                if(sp.Include_in_Sales_Planning__c == true){
                    mapOfCatNameAndCatCode.put(sp.Name,sp.Category_Code__c);  
                    catSet.add(sp.Category_Code__c);   
                }     
            }
            List<UserRole> userGMRol = [SELECT DeveloperName,Id FROM UserRole WHERE  DeveloperName =: gmLabel ];        
       
            List<UserRole> userSpeRol  = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName  =: System.Label.SRM_SpecialtyUrole OR DeveloperName  =: System.Label.AM_SpecialtyURole OR DeveloperName  =: System.Label.RM_Specialty];
       
             //getting Sp. Zone for logged in user 
            if(userSpeRol.size() > 0 && (UserInfo.getUserRoleId() == userSpeRol[0].Id || UserInfo.getUserRoleId() == userSpeRol[1].Id || UserInfo.getUserRoleId() == userSpeRol[2].Id || UserInfo.getUserRoleId() == userGMRol[0].Id)){
                zoneForRM = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:loggedInUserId and (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole)];
                for(UserTerritory2Association reg : zoneForRM){
                    zoneId.add(reg.Territory2Id);
                    zoneCode.add(reg.Territory2.name);
                    
                }
            
            }
            system.debug(zoneId+'zoneId'+'*****'+zoneCode);
             //Getting RM of loggedin TL to send email notification by submitting the sales plan
             zoneTerritory=  [Select ParentTerritory2Id,id,name from Territory2 where Id IN : zoneId];
             
             gmTerritory=  [Select id,name from Territory2 where id =:zoneTerritory[0].ParentTerritory2Id];
             regionForGM = [Select RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where Territory2Id =:gmTerritory[0].id and RoleInTerritory2=: gmLabel];
             User gmUser=  [select name,id,email from User where id=:regionForGM[0].UserId];
             system.debug(zoneTerritory+'zoneTerritory'+'*****'+gmTerritory+'gmTerritory');
             system.debug(regionForGM +'regionForGM'+'*****'+gmUser+'gmUser');
            //getting all current month Zone forcast records for logged in user 
            salesPlanningSpeForcastZone = [SELECT Id,NBP__c,Discount_On_NBP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,Month__c,Zone__c,Region_code__c,Region_Description__c,SPExternalIDTL__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Zone__c IN:zoneCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specialtyForcastZoneId AND Category__c In :catSet];
            if(salesPlanningSpeForcastZone.size() > 0){
                for(Sales_Planning__c temp :salesPlanningSpeForcastZone){
                    mapOfCatAndZoneId.put(temp.Category__c,temp.id);
                }
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String[] listKey1 = temp.split('@@');
                    setOFDLNUmber.add(listKey1[3]);
                    
            }
            listOFAccForRO = [SELECT id,KUNNR__c,Prospect_No__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c From Account /*WHERE KUNNR__c IN:setOFDLNUmber And Active__c=true*/];
            for(Account acc : listOFAccForRO){
                if(acc.Sales_Group_Text__c != null){
                    mapOfAccAndRegCode.put(acc.id,acc.Sales_Group_Text__c);
                    //mapOfAccAndAccID.put(acc.KUNNR__c,acc.ID);
                }
                
            }
            system.debug('setOFDLNUmber'+setOFDLNUmber);
            system.debug('listOFAccForRO'+listOFAccForRO);
            system.debug('mapOfAccAndAccID'+mapOfAccAndAccID);
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    String [] subStringDea;
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                    }
                    
                    else{
                            subStringDea = listKey[1].split(':');
                            if(mapOfAccAndRegCode.containsKey(subStringDea[1])){
                                st = subStringDea[1] + mapOfAccAndRegCode.get(subStringDea[1])+mapOfCatNameAndCatCode.get(listKey[2]) + month +year ;
                            }else{
                            st = listKey[3] +mapOfCatNameAndCatCode.get(listKey[2]) + month +year ;
                            }
                            
                            //dealer = new account(KUNNR__c=listKey[3]);
                            String firstz = new List<String> (zoneCode).get(0);
                            system.debug('&&&'+firstz);
                            spDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfCatAndZoneId.get(mapOfCatNameAndCatCode.get(listKey[2])),Category__c = mapOfCatNameAndCatCode.get(listKey[2]),Category_Description__c = listKey[2],
                            Month__c = month ,Year__c = year ,Zone__c = firstz,Dealer_Name__c= subStringDea[0],Dealer_CustNumber__c= listKey[3],Dealer__c = subStringDea[1],
                            RecordTypeId = specialtyDealerId,Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st,Value__c = Decimal.ValueOF(mapOfSalesPlanningValue.get(temp)));
                            
                            listOfSalesPlanningDLForInsert.add(spDLRecord);
                    }
                    system.debug(listOfSalesPlanningDLForInsert.size()+'$$$'+listOfSalesPlanningDLForInsert+'######');
                    
            
            }
            for(String temp : mapOfSalesPlanningValue.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLIdValue.add(listKey[0]);
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                            
                    }
            
            }
            listOfSalesPlanningDLValue = [SELECT id,SPExternalIDTL__c,Value__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLIdValue];
            
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            //String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            
                            //sp.Value__c = Decimal.ValueOf(val);
                            sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            if(listOfSalesPlanningDLValue.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDLValue){
                            //String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            system.debug('-sp.SPExternalIDTL__c------------'+sp.SPExternalIDTL__c+'---------val------'+val);
                            sp.Value__c = Decimal.ValueOf(val);
                            //sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLListForVal.add(sp);
                    }
            }
            
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            system.debug(upsertsalesPlanningDLListForVal+'upsertsalesPlanningDLListForVal');
            if(upsertsalesPlanningDLListForVal.size() > 0 ){
                    upsert upsertsalesPlanningDLListForVal SPExternalIDTL__c;
            }
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }
            system.debug(mapOfSalesPlanning+'!!!!!!!!!!');  
    }
    
   
    
    
}