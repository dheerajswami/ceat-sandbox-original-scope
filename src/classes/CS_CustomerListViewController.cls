public class CS_CustomerListViewController {
    
    public boolean Column1Sort{get;set;}
    public boolean SortAscending{get;set;}
    public boolean SortDescending{get;set;}
    public string orderBy{get;set;}
    transient List<CS_Customer__c> listOfCustomers ;
    Public Integer noOfRecords{get; set;}
    string orderByAscDesc;
    Id accountId;
    String baseSoql;
    String finalSoql;
    
    
    public CS_CustomerListViewController(){
        Id userId = UserInfo.getUserId();
        listOfCustomers = new List<CS_Customer__c>();
        baseSoql = 'select Id,Name,Mobile__c,Next_Wheel_Alignment__c,Next_Balancing_Date__c,Preferred_Communication_Method__c from CS_Customer__c where Ceat_Shoppe__c =  ';
        try{
            accountId = [select AccountId from Contact where Id = :[select contactId from User where Id = :userId].ContactId].AccountId;
            if(accountId !=null){
                doQuery();
            }
        }
        catch(Exception e){
        }
    }
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            try{
            if(con == null) {
                doQuery();
                con = new ApexPages.StandardSetController(Database.getQueryLocator(finalSoql));
                // sets the number of records in each page set
                con.setPageSize(50);
                noOfRecords = con.getResultSize();
            }
            }
            catch(Exception e){
                system.debug(e.getMessage());
                return null;
            }
            return con;
        }
        set;
    }
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         con.first();
     }

     // returns the last page of records
     public void last() {
         con.last();
     }

     // returns the previous page of records
     public void previous() {
         con.previous();
     }

     // returns the next page of records
     public void next() {
         con.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public void cancel() {
         con.cancel();
     }
    
    
    private pageReference sortByValue(string value){
        SortAscending = false;
        SortDescending = false;
        if(orderBy == value){
            if(orderByAscDesc == 'Asc'){
                orderByAscDesc = 'Desc';
                SortDescending = true;
            }else{
                orderByAscDesc = 'Asc';
                SortAscending = true;
            }
        }else{
            orderByAscDesc = 'Asc';
            SortAscending = true;
        }
        orderBy = value;
        con = null;  //Routine that does your query that loads your table.  It should use the orderBy string for the 'order by' clause
        return null;
    }
    
    public pageReference sortByColumn1(){  
        Column1Sort = true;
        return sortByValue('Name');
    }
    
    
    public void doQuery(){
        if(orderBy == null){
            orderBy = 'Name';
            orderByAscDesc = 'Asc';
        }
        finalSoql = baseSoql + '\'' + accountId + '\'' + ' order by ' + orderBy + ' ' + orderByAscDesc;
        //listofCustomers = database.query(soql);
        
    }
    
    public List<CS_Customer__c> getListOfCustomers(){
        try{
            doQuery();
            listOfCustomers = new List<CS_Customer__c>();
            for (CS_Customer__c acc : (List<CS_Customer__c>)con.getRecords()){
                listOfCustomers.add(acc);
            }
        }
        catch(Exception e){
            system.debug(e.getMessage());
        }
        return listOfCustomers;
    }
    
}