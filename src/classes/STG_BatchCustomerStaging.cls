global class STG_BatchCustomerStaging implements Database.Batchable<Customer_Staging__c>,Database.AllowsCallouts{

  global Iterable<Customer_Staging__c> start(Database.BatchableContext info){ 
       return STG_CustomerStagingClass.getAllCustomerInformation(); 
     }
     
     
     public void execute(Database.BatchableContext BC, List<Customer_Staging__c> scope){
       system.debug(scope);
        upsert scope Customer_Number__c;
     }
     
     global void finish(Database.BatchableContext BC){
     
      // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: STG_BatchCustomerStaging Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "STG_BatchCustomerStaging processed" ' + a.TotalJobItems +'/n'+ ' batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n');  
           
                 if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }
        
    }

}