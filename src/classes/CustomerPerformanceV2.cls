global class CustomerPerformanceV2 {

  public static Set<String> categories {get;set;}
   @TestVisible static List<sapSalesRegisterTrend_Prod_Mapping.SalesRegisterTrend_Prod_Mapping> salesRecords;
  
   public static String jSONString{get;set;}
   public static String jSONString1{get;set;}
   
   
  private Account acct;
  private static String recTypeName;
  public string custNum {get;set;}
  public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
  public static Date currentYrStart;  
  
  public static String currentYr{get;set;}
  public static String currentMonth{get;set;}
  public static String previousYr {get;set;}
  public static String selectedCategory{get;set;}
  
  public static map<String, Map<String, List<Decimal>>> monthlyCategoryValues {get;set;} 
  public static map<String, list<Decimal>> monthlySalesValues {get;set;} 
   
  static list<String> months = new List<String>{'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb','Mar'};
  public static List<String> categoriesList {get;set;}   

   public with sharing class JSONWrapper{
            public  List<String> jSONStringList{get;set;}
            //public  String jSONString5{get;set;}
    }     

  global CustomerPerformanceV2(ApexPages.StandardController stdController){       
         this.acct = (Account)stdController.getRecord();
         Account accRec = new Account();
         accRec = [select KUNNR__c,RecordType.DeveloperName from Account where Id = :acct.Id];
         String sapCustomerNumber = accRec.KUNNR__c;
         recTypeName = accRec.RecordType.DeveloperName;
         custNum  = sapCustomerNumber;
         currentMonth = UtilityClass.getMonthName(Date.today().month());
         if(Test.isRunningTest()){
            return;
         }
        CustomerPerformanceV2.calculateYTDValues(sapCustomerNumber);
        
}


public static void calculateYTDValues(String customerId){  
         

         String fromDate,toDate;         
            Map<String,Decimal> categoryValues = new Map<String,Decimal>();      
            if(Date.today().month() < 4){
                fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-2,4,1));
                previousYr = String.valueOf(Date.today().year()-2) + ' - ' + String.valueOf(Date.today().year()-1);
                currentYr = String.valueOf(Date.today().year()-1) + ' - ' + Date.today().year();
                currentYrStart = Date.newInstance(Date.today().year()-1,4,1);
            }
            else{
                fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-1,4,1));
                previousYr = String.valueOf(Date.today().year()-1) + ' - ' + Date.today().year();
                currentYr = Date.today().year() + ' - ' + String.valueOf(Date.today().year()+1);
                currentYrStart = Date.newInstance(Date.today().year(),4,1);
            }        
            toDate =  UtilityClass.getStringDate(Date.today());
            if(monthlySalesValues == null){
           monthlySalesValues = new Map<String,list<Decimal>>();
        }
        monthlySalesValues.put('Apr',new List<Decimal>() );
        monthlySalesValues.put('May',new List<Decimal>()  );
        monthlySalesValues.put('Jun',new List<Decimal>()  );
        monthlySalesValues.put('Jul',new List<Decimal>()  );
        monthlySalesValues.put('Aug',new List<Decimal>()  );
        monthlySalesValues.put('Sep',new List<Decimal>() );
        monthlySalesValues.put('Oct',new List<Decimal>()  );
        monthlySalesValues.put('Nov',new List<Decimal>()  );
        monthlySalesValues.put('Dec',new List<Decimal>()  );
        monthlySalesValues.put('Jan',new List<Decimal>()  );
        monthlySalesValues.put('Feb',new List<Decimal>()  );
        monthlySalesValues.put('Mar',new List<Decimal>() );
              
        if(monthlyCategoryValues == null){
           monthlyCategoryValues = new Map<String, Map<String, List<Decimal>>>();
        }
        monthlyCategoryValues.put('Apr',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('May',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Jun',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Jul',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Aug',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Sep',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Oct',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Nov',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Dec',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Jan',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Feb',new Map<String, List<Decimal>>() );
        monthlyCategoryValues.put('Mar',new Map<String, List<Decimal>>() );
        
         categories = new Set<String>(); 
         salesRecords= sapSalesRegisterTrend_Prod_Mapping.getAllSalesRegisterTread(fromDate,toDate,customerId);
         for(sapSalesRegisterTrend_Prod_Mapping.SalesRegisterTrend_Prod_Mapping salesRecord: salesRecords){           
         categories.add(salesRecord.WGBEZ); 
          Date monthOfSale= Date.newInstance(Integer.valueOf(salesRecord.YEAR), Integer.valueOf(salesRecord.MONTH), 1);
         Integer monthNumber = Integer.ValueOf(salesRecord.MONTH);
         String month = (monthNumber == 1)?'Jan':(monthNumber == 2)?'Feb':(monthNumber == 3)?'Mar':(monthNumber == 4)?'Apr':(monthNumber == 5)?'May':(monthNumber == 6)?'Jun':(monthNumber == 7)?'Jul':(monthNumber == 8)?'Aug':(monthNumber == 9)?'Sep':(monthNumber == 10)?'Oct' : (monthNumber == 11)?'Nov' : 'Dec';
         Integer mm = date.today().month();
         Integer currentYear = date.today().year();
          
           //************Monthly Category Sales Value Performance************
            
             Map<String,List<Decimal>> catValues = monthlyCategoryValues.get(month);
            List<Decimal> actualValues = catValues.get(salesRecord.WGBEZ);
            
            if(actualValues == null){
                actualValues = new List<Decimal>{0,0,0,0};
                catValues.put(salesRecord.WGBEZ, actualValues);
            }
            if(monthOfSale < currentYrStart){
               actualValues[2] += Decimal.valueOf(salesRecord.FKIMG); 
            }
            else{
                actualValues[3] += Decimal.valueOf(salesRecord.FKIMG);
            }
            catValues.put(salesRecord.WGBEZ, actualValues);           
            monthlyCategoryValues.put(month,catValues);
         //************Monthly Sales Value Performance************
            
            List<Decimal> saleValues = monthlySalesValues.get(month);           
            if(saleValues == null || saleValues.isEmpty()){          
                saleValues = new List<Decimal>{0,0,0,0};  
                monthlySalesValues.put(month, saleValues);
            }
            if(monthOfSale < currentYrStart){
            
           saleValues[0] += Decimal.valueOf(salesRecord.NETWR)/Constants.convertTolakhs;
           saleValues[2] += Decimal.valueOf(salesRecord.FKIMG); 
              }
            else{
                
                saleValues[1] += Decimal.valueOf(salesRecord.NETWR)/Constants.convertTolakhs;
                saleValues[3] += Decimal.valueOf(salesRecord.FKIMG);
            }           
            monthlySalesValues.put(month, saleValues);
            
            
}
            
            if(categoriesList == null && categories.size() >0 ){
                categoriesList = new List<String>();
                //categoriesList.add('---None---');
                categoriesList.addAll(categories);
                categoriesList.sort();
                selectedCategory = categoriesList[0];
               
          }
     //Monthly category wise Sales value (Quantity)
        
        jSONString = '[';
         for(String mon : months){
           Map<String,List<Decimal>> values = monthlyCategoryValues.get(mon);
            List<Decimal> valuesToDisplay ;
           if(values == null || values.get(selectedCategory) == null){
                    valuesToDisplay = new List<Decimal>{0,0,0,0};
                }
            else{
                    valuesToDisplay = values.get(selectedCategory);
            }
             
           
             jSONString += '[\'' + mon + '\', ' + valuesToDisplay[2] + ', ' + valuesToDisplay[3] + ', \'A\' ,' + valuesToDisplay[3] + '],';
         }
         jSONString = jSONString.substring(0, jSONString.length() -1) + ']' ;
         system.debug('333'+jSONString);
          
     //Monthly Sales value
        jSONString1 = '[';
        for(String mon : months){
            List<Decimal> valuesToDisplay1 ;
            valuesToDisplay1 = monthlySalesValues.get(mon);                        
            if(valuesToDisplay1 == null || valuesToDisplay1.size()==0){
                valuesToDisplay1 = new List<Decimal>{0,0,0,0};
            }
            else{
                valuesToDisplay1 = monthlySalesValues.get(mon);
            }
            
            jSONString1 += '[\'' + mon + '\', ' + valuesToDisplay1[0] + ', ' + valuesToDisplay1[1] + ',\'' + mon + '\''+',\'' + mon + '\'],';
        }
        
        jSONString1 = jSONString1.substring(0, jSONString1.length() -1) + ']' ;
        
        system.debug('2222'+jSONString1 );
        
    

  }
@RemoteAction
    global static list<WrapperClassMain> setCategory(String catName,map<String,string> catValue ) {
        
        list<WrapperClassMain> finalValues = new list<WrapperClassMain>();
        
        
        String jSONString4 ='';
        jSONString4 = '[';//[\'Month\',\'Previous Yr\', \'Current Yr\'],
        
       
        for(String str : catValue.keySet()){
            
        }
        
        JSONWrapper jsonw = new JSONWrapper();
        jsonw.jSONStringList= new List<string>();
        
        decimal salesVle1;
        decimal salesVle2;
        
        
        for(string mon:months){
        selectedCategory =mon +'@@'+ catName; 
        
        //getNewValues();
        String mycategory = catValue.get(selectedCategory);
        
        if(mycategory!= null){
        String myCat = mycategory.substring(mycategory.indexOf('[') + 1, mycategory.indexOf(']'));
        myCat = myCat.trim();
        String[] deci= myCat.split(',');
        if(deci[2] == ' 0') deci[2] = String.ValueOf(0.0);
        if(deci[3] == ' 0') deci[3] = String.ValueOf(0.0);
        salesVle1 = decimal.ValueOf(deci[2].trim());
        salesVle2 = decimal.ValueOf(deci[3].trim());
        }
        List<Decimal> valuesToDisplay = new List<Decimal>{0,0,0,0}; 
        
        if(catValue.get(selectedCategory) == null){
                valuesToDisplay = new List<Decimal>{0,0,0,null};
            }
        else{
                valuesToDisplay[2]= salesVle1;
                valuesToDisplay[3]= salesVle2 ;
                
            }
            jSONString4 += '[\'' + mon + '\', ' + valuesToDisplay[2] + ', ' + valuesToDisplay[3] + '],';
            
                   
            finalValues.add(new WrapperClassMain(mon,valuesToDisplay[2],valuesToDisplay[3],catName));    
        } 
       
        jSONString4 = jSONString4.substring(0, jSONString4.length() -1) + ']' ;   
        
        
         system.debug('-----onload---jSONString1===='+jSONString1);
    

        
        
        
        
        return finalValues;
    }

     
   
    global class WrapperClassMain {
       
        public String monthName {get;set;}
        public Decimal valOne {get;set;}
        public Decimal valTwo {get;set;}
        public string category  {get;set;} 
       
        public WrapperClassMain(String mn,Decimal fo,Decimal sO,String category){
            this.monthName = mn;
            this.valOne = fo;
            this.valTwo = sO;
            this.category  = category  ;
        }
        
    }



}