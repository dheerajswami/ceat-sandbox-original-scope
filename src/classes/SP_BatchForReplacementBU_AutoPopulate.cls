global class SP_BatchForReplacementBU_AutoPopulate implements Database.Batchable<sObject> { 
        
        String special                  = System.Label.Specialty;
        String replacement              = System.Label.Replacement;
        String Forcast                  = System.Label.Forcast; 
        String repDealerLabel   = System.Label.Replacement_Dealer;
        String repROLabel       = System.Label.Replacement_RO;
        String repTLLabel       = System.Label.Replacement_TL; 
        Static String SPECIALTY='Specialty';
        Static String REPLECMENT='Replacement';
        String tlLabel                  = System.Label.TL_Role;
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
        String catZoneSpe = '';  
        String SpecialtyDealerLabel     = System.Label.Specialty;
        String Specialty_Forcast_Zone       = System.Label.Specialty_Forcast_Zone;
        List<Sales_Planning__c> salesPlanningRO             = new List<Sales_Planning__c>();
        List<Sales_Planning__c> salesPlanningSpeDealerUpdate            = new List<Sales_Planning__c>();
        List<Territory2> listOfTerr                         = new List<Territory2>();
        List<Sales_Planning__c> listOfSalesPlanningDealer   = new List<Sales_Planning__c>();
        List<Sales_Planning__c> insertSalesPlanningTL   = new List<Sales_Planning__c>();
        List<Sales_Planning__c> gettingSalesPlanningTL   = new List<Sales_Planning__c>();
        List<Sales_Planning__c> updateSalesPlanningTL   = new List<Sales_Planning__c>();
        List<Sales_Planning__c> updateSalesPlanningDealer   = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOfSalesPlanningDealer1;
        List<Sales_Planning__c> salesPlan12;
        List<Sales_Planning__c> salesPlanDealerToUpdate;
        List<Sales_Planning__c>  salesPlanningTLList                    = new List<Sales_Planning__c>();
        Sales_Planning__c salesPlanningTL;
        Sales_Planning__c salesPlanningTL1 ;
        Integer totalL3M = 0;
        Integer totalL3MFinal = 0;
        Integer totalLYCM = 0;
        Integer totalBudget = 0;
        Integer totalL3MAllTL       = 0;
        Integer totalL3MAllTL1      = 0;
        Decimal totalValL3m = 0;
        Decimal totalValLYCM = 0;
        
        Map<String,Integer> mapOfZoneTarget                                 = new map<String,Integer>();
        Map<String,Integer> mapOfExistingTLTarget                                   = new map<String,Integer>();
        Map<String,id> mapOfZoneID                                          = new map<String,id>();
        Map<String,Integer> mapOfZoneTotalLYCM                               = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalL3M1                              = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalBudget                               = new map<String,Integer>(); 
        Map<String,Decimal> mapOfZoneTotalValL3m                              = new map<String,Decimal>(); 
        Map<String,Decimal> mapOfZoneTotalValLYCM                               = new map<String,Decimal>(); 
        Map<String,List<Sales_Planning__c>> mapOfTerrSales          = new map<String,List<Sales_Planning__c>>();
        Map<String,Integer> mapOfTLL3MTotal                         = new map<String,Integer>();
        Map<String,Sales_Planning__c> mapOfZoneForcast = new Map<String,Sales_Planning__c>();
        Map<String,id> mapOfTerritoryAndUserID                      = new Map<String,ID>();
        Map<String,String> mapOfTerrAndTL                           = new map<String,String>();
        Map<String,ID> mapOfCatAndROId                              = new map<String,ID>();
        Map<String,Sales_Planning__c> mapOfTLAndId                                 = new map<String,Sales_Planning__c>();
        Map<String,Sales_Planning__c> mapOfTLSP;
        Id specialtyDealerId            = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: SpecialtyDealerLabel Limit 1].Id;
        //Id specialtyForcastROId         = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast Limit 1].Id;
        Id specialtyForcastZoneId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast_Zone Limit 1].Id;
        Id replaceDealerId              = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        Id replaceROId                  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        Id replaceTLId                  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
        Set<String> catSet                                              = new set<String>();
        Set<String> newTLSet                                             = new set<String>();
        Set<ID> tlIDSet                                              = new set<ID>();
        Set<String> regionCode                                           = new set<String>();
        Set<String> territoryCode                                           = new set<String>();
        Set<String> setkeyCatZone = new Set<String>();
        Set<String> setkeyCatZone1 = new Set<String>();
        Set<String> setOFTerritoryCode  = new Set<String>();
        String ss ='';
       global Database.QueryLocator start(Database.BatchableContext BC){
            String query;
            //getting Specialty Category from custom setting  
            
            //getting all staging records with BU specialty and Forcast
            String PAT           = 'PAT';
            String MUM           = 'MUM';
            String Cat           = '2010';
            query = 'SELECT id,Parent_Sales_Planning__c,Customer_group__c,SYS_TL_CAT_REG__c,Value__c,SYS_Used_IN_Batch2__c,Dealer__c,SYS_TL_CAT__c,Dealer_CustNumber__c,Dealer_Name__c,Category__c,Category_Description__c,Month__c ,Year__c,Region_code__c ,Region_Description__c,RecordTypeId,'
            +'Territory_Code__c,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Budget__c,Value_L3M__c,Value_LYCM__c,Zone__c '+
            'FROM Sales_Planning__c WHERE (Region_code__c =  \'MUM\' ) AND Year__c =\''+year+'\' AND Month__c =\''+month+'\' AND RecordTypeId =\''+replaceDealerId+'\' AND Total_planned__c = null Order By Category__c' ;
            /*
            query = 'SELECT id,Parent_Sales_Planning__c,Customer_group__c,SYS_TL_CAT_REG__c,Value__c,SYS_Used_IN_Batch2__c,Dealer__c,SYS_TL_CAT__c,Dealer_CustNumber__c,Dealer_Name__c,Category__c,Category_Description__c,Month__c ,Year__c,Region_code__c ,Region_Description__c,RecordTypeId,'
            +'Territory_Code__c,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Budget__c,Value_L3M__c,Value_LYCM__c,Zone__c '+
            'FROM Sales_Planning__c WHERE  Year__c =\''+year+'\' AND Month__c =\''+month+'\' AND RecordTypeId =\''+replaceDealerId+'\' AND Total_planned__c = null' ; */
            System.debug(Database.getQueryLocator(query)+'@@@query@@@@');
            
             
                return Database.getQueryLocator(query);
         }
         global void execute(Database.BatchableContext BC,List<sObject> scope){
            /*
            List<Speciality_Sales_Planning_Categories__c> spc = Speciality_Sales_Planning_Categories__c.getall().values();
                for(Speciality_Sales_Planning_Categories__c sp : spc) {
                    if(sp.Include_in_Sales_Planning__c == true){
                        catSet.add(sp.Category_Code__c);
                        //mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);     
                    }     
                }
           */
            
            listOfTerr = [select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId,(Select Territory2Id,UserID,User.name from UserTerritory2Associations Where RoleInTerritory2=: tlLabel ) from Territory2];
       
           // system.debug(listOfTerr+'listOfTerr');
            if(listOfTerr.size() > 0){
                for(Territory2 temp : listOfTerr){
                            setOFTerritoryCode.add(temp.name);
                            for(UserTerritory2Association s : temp.UserTerritory2Associations){
                                mapOfTerritoryAndUserID.put(temp.Name,s.UserID);
                                //setOfTL.add(s.User.name);
                                mapOfTerrAndTL.put(s.User.name,temp.Name);
                            }
                        
                    }
                    
                
            }
           
            //system.debug(catSet+'catSet');
            for(sObject temp : scope){
                Sales_Planning__c sps =(Sales_Planning__c)temp;
                setkeyCatZone.add(sps.SYS_TL_CAT__c);
                setkeyCatZone1.add(sps.SYS_TL_CAT_REG__c);
                regionCode.add(sps.Region_code__c);
                territoryCode.add(sps.Territory_Code__c);
            }
            system.debug(setkeyCatZone1+'setkeyCatZone1');
            for(String s : setkeyCatZone){
                mapOfZoneTotalL3M1.put(s,0);
                mapOfZoneTotalLYCM.put(s,0);
                mapOfZoneTotalBudget.put(s,0);
                mapOfZoneTotalValL3m.put(s,0);
                mapOfZoneTotalValLYCM.put(s,0);
            }
            for(String s : setkeyCatZone1){
                mapOfTLL3MTotal.put(s,0);
            }
            
            for(Sales_Planning__c salesPlan : [SELECT Id,Name,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
                Month__c ,Year__c ,Zone__c ,Budget__c,Territory_Code__c ,SYS_TL_CAT_REG__c,SYS_TL_CAT__c,SYS_Used_IN_Batch__c,Total_planned__c,SYS_Used_IN_Batch2__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE  SYS_TL_CAT__c IN : setkeyCatZone AND RecordTypeId =: replaceDealerId AND Year__c =:year  AND Month__c =: month] ){
                if(mapOfTLL3MTotal.containsKey(salesPlan.SYS_TL_CAT_REG__c)){
                    totalL3MFinal = 0;
                    totalL3MFinal = mapOfTLL3MTotal.get(salesPlan.SYS_TL_CAT_REG__c);
                    if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                    totalL3MFinal = totalL3MFinal +Integer.valueOf(salesPlan.L3M__c);
                    mapOfTLL3MTotal.put(salesPlan.SYS_TL_CAT_REG__c,totalL3MFinal);
                    
                }
                if(mapOfZoneTotalL3M1.containsKey(salesPlan.SYS_TL_CAT__c)){
                    totalL3M = 0;
                    totalL3M = mapOfZoneTotalL3M1.get(salesPlan.SYS_TL_CAT__c);
                    if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                    totalL3M = totalL3M +Integer.valueOf(salesPlan.L3M__c);
                    mapOfZoneTotalL3M1.put(salesPlan.SYS_TL_CAT__c,totalL3M);
                    if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                        ss = ss+salesPlan.Name+'<------L3m---->'+mapOfZoneTotalL3M1.get('B00212050');
                    }
                    
                }
                if(mapOfZoneTotalLYCM.containsKey(salesPlan.SYS_TL_CAT__c)){
                    totalLYCM = 0;
                    totalLYCM = mapOfZoneTotalLYCM.get(salesPlan.SYS_TL_CAT__c);
                    system.debug(totalLYCM+'totalLYCM');
                    if(salesPlan.LYCM__c == null) salesPlan.LYCM__c = 0;
                        totalLYCM = totalLYCM +Integer.valueOf(salesPlan.LYCM__c);
                        system.debug(totalLYCM+'totalLYCMnull');
                    
                    mapOfZoneTotalLYCM.put(salesPlan.SYS_TL_CAT__c,totalLYCM);
                    if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                        ss = ss+'<------LYCM------>'+mapOfZoneTotalLYCM.get('B00212050');
                    }
                    system.debug(mapOfZoneTotalLYCM+'mapOfZoneTotalLYCM');
                }
                if(mapOfZoneTotalBudget.containsKey(salesPlan.SYS_TL_CAT__c)){
                    totalBudget = 0;
                    totalBudget = mapOfZoneTotalBudget.get(salesPlan.SYS_TL_CAT__c);
                    if(salesPlan.Budget__c == null) salesPlan.Budget__c = 0;
                        totalBudget = totalBudget +Integer.valueOf(salesPlan.Budget__c);
                    
                    mapOfZoneTotalBudget.put(salesPlan.SYS_TL_CAT__c,totalBudget);
                    if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                        ss = ss+'<------Budget------>'+mapOfZoneTotalBudget.get('B00212050');
                    }
                }
                if(mapOfZoneTotalValL3m.containsKey(salesPlan.SYS_TL_CAT__c)){
                    totalValL3m = 0.0;
                    totalValL3m = mapOfZoneTotalValL3m.get(salesPlan.SYS_TL_CAT__c);
                    if(salesPlan.Value_L3M__c == null) salesPlan.Value_L3M__c =0;
                        totalValL3m = totalValL3m +salesPlan.Value_L3M__c;
                    
                    mapOfZoneTotalValL3m.put(salesPlan.SYS_TL_CAT__c,totalValL3m);
                    if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                        ss = ss+'<------ValL3m------>'+mapOfZoneTotalValL3m.get('B00212050');
                    }
                }
                if(mapOfZoneTotalValLYCM.containsKey(salesPlan.SYS_TL_CAT__c)){
                    totalValLYCM = 0.0;
                    totalValLYCM = mapOfZoneTotalValLYCM.get(salesPlan.SYS_TL_CAT__c);
                    if(salesPlan.Value_LYCM__c == null) salesPlan.Value_LYCM__c = 0;
                        totalValLYCM = totalValLYCM +salesPlan.Value_LYCM__c;
                    
                    mapOfZoneTotalValLYCM.put(salesPlan.SYS_TL_CAT__c,totalValLYCM);
                    if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                        ss = ss+'<------ValLYCM------>'+mapOfZoneTotalValLYCM.get('B00212050');
                    }
                }
                if(mapOfTerrSales.containsKey(salesPlan.SYS_TL_CAT__c)){
                    listOfSalesPlanningDealer = mapOfTerrSales.get(salesPlan.SYS_TL_CAT__c);
                    listOfSalesPlanningDealer.add(salesPlan);
                }else{
                    listOfSalesPlanningDealer1 = new list<Sales_Planning__c>();
                    listOfSalesPlanningDealer1.add(salesPlan);
                    mapOfTerrSales.put(salesPlan.SYS_TL_CAT__c,listOfSalesPlanningDealer1);
                     }
                    
            }
            system.debug(mapOfTerrSales+'mapOfTerrSales');
            
            //getting all current month Zone forcast records for logged in user 
            
            salesPlanningRO = [SELECT Id,ASP__c,Budget__c,Dealer__c,SYS_TL_CAT_REG__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: replaceROId ];
            salesPlanningTLList = [SELECT Id,ASP__c,Total_L3M__c,SPExternalIDTL__c,Budget__c,Total_planned__c,Dealer__c,SYS_TL_CAT_REG__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: replaceTLId AND Territory_Code__c IN: territoryCode];
            //salesPlanningSpeForcastZone = [SELECT Id,NBP__c,Discount_On_NBP__c,SYS_Used_IN_Batch2__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,Month__c,Zone__c,Region_code__c,Region_Description__c,SPExternalIDTL__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE  Zone__c IN:setOFzone AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specialtyForcastZoneId];
            for(Sales_Planning__c sp : salesPlanningTLList){
                String unKey = '';
                //unKey = sp.SYS_TL_CAT__c + sp.Month__c + sp.Year__c;
                mapOfExistingTLTarget.put(sp.SPExternalIDTL__c,Integer.valueOF(sp.Total_planned__c));
                mapOfTLAndId.put(sp.SPExternalIDTL__c,sp);
            }
            for(Sales_Planning__c sp : salesPlanningRO){
                mapOfZoneTarget.put(sp.SYS_TL_CAT_REG__c,Integer.valueOF(sp.Target_Quantity__c));
                mapOfZoneID.put(sp.SYS_TL_CAT_REG__c,sp.id);
                mapOfCatAndROId.put(sp.SYS_TL_CAT_REG__c,sp.id);
            }
            if(mapOfTerrSales.size() > 0){
                mapOfTLSP = new map<String,Sales_Planning__c>(); 
                    for (String key : mapOfTerrSales.keySet()) {
                        if(!(mapOfExistingTLTarget.containsKey(key+month+year))){
                            salesPlan12 = new list<Sales_Planning__c>();
                            salesPlan12 = mapOfTerrSales.get(key);
                            Boolean flag = true;
                            for(Sales_Planning__c temp: salesPlan12){
                                    String st = '';
                                    id spROID;
                                    id usID;
                                    if(flag){
                                            st = temp.Territory_Code__c + temp.Category__c + temp.Month__c+temp.Year__c;
                                            newTLSet.add(st);
                                            spROID = mapOfCatAndROId.get(temp.SYS_TL_CAT_REG__c);
                                            system.debug(mapOfCatAndROId.get(temp.SYS_TL_CAT_REG__c)+'@@@');
                                            system.debug(mapOfTerritoryAndUserID.get(temp.Territory_Code__c)+'%%%%');
                                            if(mapOfTerritoryAndUserID.get(temp.Territory_Code__c) == null){
                                                usID = UserInfo.getUserId();
                                            }else{
                                                usID = mapOfTerritoryAndUserID.get(temp.Territory_Code__c);
                                            }
                                            salesPlanningTL = new Sales_Planning__c(OwnerId = usID,Parent_Sales_Planning__c = spROID,Category__c = temp.Category__c,Category_Description__c = temp.Category_Description__c,
                                                                                    Month__c = temp.Month__c,Year__c = temp.Year__c,Region_code__c = temp.Region_code__c,Region_Description__c = temp.Region_Description__c,
                                                                                    RecordTypeId = replaceTLId, Territory_Code__c = temp.Territory_Code__c,SPExternalIDTL__c = st);
                                    }
                                    flag = false;
                                }
                            
                                if(mapOfZoneTotalL3M1.containsKey(key)){
                                     salesPlanningTL.Total_L3M__c = mapOfZoneTotalL3M1.get(key);   
                                }else{
                                    salesPlanningTL.Total_L3M__c = 0;
                                }
                                if(mapOfZoneTotalLYCM.containsKey(Key)){
                                    salesPlanningTL.Total_LYCM__c= mapOfZoneTotalLYCM.get(key);
                                }else{
                                    salesPlanningTL.Total_LYCM__c = 0;
                                }
                                if(mapOfZoneTotalBudget.containskey(key)){
                                    salesPlanningTL.Total_Budget__c= mapOfZoneTotalBudget.get(key);
                                }else{
                                    salesPlanningTL.Total_Budget__c= 0;
                                }
                                if(mapOfZoneTotalValLYCM.containsKey(key)){
                                    salesPlanningTL.Value_LYCM__c= mapOfZoneTotalValLYCM.get(key);    
                                }else{
                                    salesPlanningTL.Value_LYCM__c= 0;
                                }
                                if(mapOfZoneTotalValL3m.containsKey(key)){
                                     salesPlanningTL.Value_L3M__c= mapOfZoneTotalValL3m.get(key);   
                                }else{
                                    salesPlanningTL.Value_L3M__c= 0;
                                }
                                //if(salesPlanningTL.Total_L3M__c != null && mapOfZoneTarget.containsKey(sps.key) && mapOfZoneTotalL3M1.containsKey(spTL.SYS_Used_IN_Batch2__c) && mapOfZoneTotalL3M1.get(spTL.SYS_Used_IN_Batch2__c)>0){

                                //}
                                //salesPlanningTL.Total_planned__c = ((salesPlanningTL.Total_L3M__c * mapOfZoneTarget.get(sps.key))/mapOfZoneTotalL3M1.get(sps.SYS_Used_IN_Batch2__c)).round();
                                mapOfTLSP.put(key,salesPlanningTL);
                        }
                    }
                    /*
                for(sObject temp : scope){
                    Sales_Planning__c sps =(Sales_Planning__c)temp;
                    if(sps.L3M__c != null && mapOfZoneTarget.containsKey(sps.SYS_Used_IN_Batch2__c) && mapOfZoneTotalL3M1.containsKey(sps.SYS_Used_IN_Batch2__c)){
                        system.debug('InsideIf');
                        sps.Total_planned__c = ((sps.L3M__c * mapOfZoneTarget.get(sps.SYS_Used_IN_Batch2__c))/mapOfZoneTotalL3M1.get(sps.SYS_Used_IN_Batch2__c)).round();
                        sps.Parent_Sales_Planning__c = mapOfZoneID.get(sps.SYS_Used_IN_Batch2__c);
                    }else if(sps.L3M__c == null){
                        sps.Total_planned__c = 0;
                        sps.Parent_Sales_Planning__c = mapOfZoneID.get(sps.SYS_Used_IN_Batch2__c);
                        system.debug('InsideElse');
                    }
                    salesPlanningSpeDealerUpdate.add(sps);
                    system.debug(sps.L3M__c+'!!!!!!!!!!'+sps.Total_planned__c+'##########');
                }*/
            }
            if(mapOfTLSP.size() > 0){
                for(String key : mapOfTLSP.keySet()){
                        salesPlanningTL1 = new Sales_Planning__c();
                        salesPlanningTL1 = mapOfTLSP.get(key);
                        insertSalesPlanningTL.add(salesPlanningTL1);
                }    
            }
            
            if(insertSalesPlanningTL.size() > 0){
                database.upsert (insertSalesPlanningTL , Sales_Planning__c.Fields.SPExternalIDTL__c,false);
               // for(Sales_Planning__c sa : insertSalesPlanningTL){
                    //tlIDSet.add(sa.id);
               // }
            }
            system.debug(newTLSet+'newTLSet');
            system.debug(newTLSet.size()+'newTLSet');
            gettingSalesPlanningTL = [SELECT Id,Name,Total_L3M__c,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
                Month__c ,Year__c ,Zone__c ,Budget__c,Territory_Code__c ,SYS_TL_CAT_REG__c,SYS_TL_CAT__c,SYS_Used_IN_Batch__c,Total_planned__c,SYS_Used_IN_Batch2__c,SPExternalIDTL__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c  ,RecordTypeId FROM Sales_Planning__c WHERE  SPExternalIDTL__c IN: newTLSet AND RecordTypeId =: replaceTLId AND Year__c =:year  AND Month__c =: month ];
            system.debug(gettingSalesPlanningTL.size() +'gettingSalesPlanningTL');
           /* for(Sales_Planning__c salesPlan : gettingSalesPlanningTL ){
                if(mapOfTLL3MTotal.containsKey(salesPlan.SYS_TL_CAT_REG__c)){
                    //totalL3MAllTL = 0;
                    totalL3MAllTL = mapOfTLL3MTotal.get(salesPlan.SYS_TL_CAT_REG__c);
                    if(salesPlan.Total_L3M__c == null) {
                        salesPlan.Total_L3M__c = 0;
                    }
                    totalL3MAllTL = totalL3MAllTL +Integer.valueOf(salesPlan.Total_L3M__c);
                    mapOfTLL3MTotal.put(salesPlan.SYS_TL_CAT_REG__c,totalL3MAllTL);
                }
             } */
             system.debug(mapOfTLL3MTotal+'mapOfTLL3MTotal');
             system.debug(mapOfTLL3MTotal.get('2015MUM')+'mapOfTLL3MTotal');
             updateSalesPlanningTL = new list<Sales_Planning__c>();
             for(Sales_Planning__c spTL : gettingSalesPlanningTL){
                if(spTL.SPExternalIDTL__c == 'B0003201052015'){
                    system.assertEquals(spTL.Total_L3M__c, 533);
                    system.assertEquals(mapOfZoneTarget.get(spTL.SYS_TL_CAT_REG__c), 9402);
                    system.assertEquals(mapOfTLL3MTotal.get(spTL.SYS_TL_CAT_REG__c), 12667);
                }
                
                if(spTL.Total_L3M__c != null && mapOfZoneTarget.containsKey(spTL.SYS_TL_CAT_REG__c) && mapOfTLL3MTotal.containsKey(spTL.SYS_TL_CAT_REG__c) && mapOfTLL3MTotal.get(spTL.SYS_TL_CAT_REG__c)>0){   
                    spTL.Total_planned__c = ((spTL.Total_L3M__c * mapOfZoneTarget.get(spTL.SYS_TL_CAT_REG__c))/mapOfTLL3MTotal.get(spTL.SYS_TL_CAT_REG__c)).round();
                }else if(spTL.Total_L3M__c == null){
                    spTL.Total_planned__c = 0;
                }
                if(spTL.SPExternalIDTL__c == 'B0003201052015'){
                    system.assertEquals(spTL.Total_planned__c, 396);
                }
                updateSalesPlanningTL.add(spTL);
                mapOfExistingTLTarget.put(spTL.SPExternalIDTL__c,Integer.valueOF(spTL.Total_planned__c));
                mapOfTLAndId.put(spTL.SPExternalIDTL__c,spTL);
             }
             if(updateSalesPlanningTL.size() > 0){
                database.upsert (updateSalesPlanningTL ,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
             }
             for(String te : mapOfTLAndId.keySet()){
                 salesPlanDealerToUpdate = new list<Sales_Planning__c>();
                 if(mapOfTerrSales.containsKey(mapOfTLAndId.get(te).SYS_TL_CAT__c)){
                            salesPlanDealerToUpdate = mapOfTerrSales.get(mapOfTLAndId.get(te).SYS_TL_CAT__c);
                            for(Sales_Planning__c temp : salesPlanDealerToUpdate){
                                temp.Parent_Sales_Planning__c = mapOfTLAndId.get(te).id;
                                if(temp.L3M__c != null && mapOfTLAndId.get(te).Total_L3M__c > 0 && mapOfTLAndId.get(te).Total_planned__c != null){
                                temp.Total_planned__c = ((temp.L3M__c * mapOfTLAndId.get(te).Total_planned__c)/mapOfTLAndId.get(te).Total_L3M__c).round();
                                }else if(temp.L3M__c == null){
                                    temp.Total_planned__c = 0;
                                }
                                updateSalesPlanningDealer.add(temp);
                            }
                        }
             }
             /*
             for(Sales_Planning__c tlRecord : updateSalesPlanningTL){
                        String formap ='';
                        formap = tlRecord.Territory_Code__c+tlRecord.Category__c;
                        salesPlanDealerToUpdate = new list<Sales_Planning__c>();
                        if(mapOfTerrSales.containsKey(tlRecord.SYS_TL_CAT__c)){
                            salesPlanDealerToUpdate = mapOfTerrSales.get(tlRecord.SYS_TL_CAT__c);
                            for(Sales_Planning__c temp : salesPlanDealerToUpdate){
                                temp.Parent_Sales_Planning__c = tlRecord.id;
                                if(temp.L3M__c != null && tlRecord.Total_L3M__c > 0 && tlRecord.Total_planned__c != null){
                                temp.Total_planned__c = ((temp.L3M__c * tlRecord.Total_planned__c)/tlRecord.Total_L3M__c).round();
                                }else if(temp.L3M__c == null){
                                    temp.Total_planned__c = 0;
                                }
                                updateSalesPlanningDealer.add(temp);
                            }
                        }
                       
             }*/
             if(updateSalesPlanningDealer.size() > 0){
                database.upsert (updateSalesPlanningDealer, Sales_Planning__c.Fields.SPExternalIDTL__c,false);
             }
             system.debug(mapOfTerrSales+'mapOfTerrSales');
             system.debug(updateSalesPlanningDealer+'updateSalesPlanningDealer');
             system.debug(updateSalesPlanningDealer.size()+'updateSalesPlanningDealer');
         }
         global void finish(Database.BatchableContext BC)
         {       
            system.debug(salesPlanningSpeDealerUpdate.size()+'salesPlanningSpeDealerUpdate');
            
             // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForReplacementBU_AutoPopulate Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForReplacementBU_AutoPopulate" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
            
         }
}