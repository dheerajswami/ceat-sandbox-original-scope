public class UpdateMaterialDescription{
    
    @InvocableMethod(label='Update Material Description' description='Removes the special characters from the material description')
    public static List<Id> updatematerialDesc(List<ID> materialId) {
        List<Id> li = new List<Id>();
        try{
            system.debug('in here');
            if(materialId!= null && materialId.size()>0) {
               Material_Master_Sap__c rec = [select Name,Plain_Desc__c from Material_Master_Sap__c where Id = :materialId[0] ];
               rec.Plain_Desc__c = rec.Name.replaceAll('[^a-zA-Z0-9]','');
               update rec;
            }            
        }
        catch(Exception e) {}
        return li;
    }            
}