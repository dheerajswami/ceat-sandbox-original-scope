@isTest
public class DashboardStagingTriggerHandler_TestClass{ 
    static testmethod void testmethode() {
            
            list<Id> terrTypeID                            = new list<Id>();
            list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
            list<Id> terrId                                = new list<Id>();
            for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
            }  
        
            
            list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID and Name like '%DI001%'];       
            //list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
            for(Territory2 temp : terrList){
                terrId.add(temp.id);
            } 
            list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId AND RoleInTerritory2='TLD'];    
            User u = [Select id, name,username,Email from User where id = :userTerrAssList[0].UserId limit 1];
            Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
            dwm.Testing__c = true;
            dwm.Role__c = 'TLD';
            dwm.Parameters_Inout__c = 'Number of P1 customers met';
            dwm.Category__c = 'PJP adherance';
            dwm.Weightage__c = 10;
            insert dwm;
            Dashboard_Staging__c ds1  = new Dashboard_Staging__c();
            ds1.Category__c = 'PJP adherance';
            ds1.Email_Id__c = u.username;
            ds1.Month__c = 'July';
            ds1.parameters__c = 'Number of P1 customers met';
            ds1.Total_Actual_MTD__c = 12;
            ds1.Total_Monthly_Target__c = 200;
            ds1.Total_MTD_Target__c = 12;
            ds1.Year__c = '2015';
            insert ds1;
            
    }
    static testmethod void testmethode1() {
            list<Id> terrTypeID                            = new list<Id>();
            list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
            list<Id> terrId                                = new list<Id>();
            for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
            }  
        
            String[] filters = new String[]{'SPRP%'};
            list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID and Name like '%B0044%'];       
            //list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
            for(Territory2 temp : terrList){
                terrId.add(temp.id);
            } 
            list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId AND RoleInTerritory2='CSTL'];    
            User u = [Select id, name,username,Email from User where id = :userTerrAssList[0].UserId limit 1];
            Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
            dwm.Testing__c = true;
            dwm.Role__c = 'CSTL';
            dwm.Parameters_Inout__c = 'Number of P1 customers met';
            dwm.Category__c = 'PJP adherance';
            dwm.Weightage__c = 10;
            insert dwm;
            Dashboard_Staging__c ds2  = new Dashboard_Staging__c();
            ds2.Category__c = 'PJP adherance';
            ds2.Email_Id__c = u.username;
            ds2.Month__c = 'July';
            ds2.parameters__c = 'Number of P1 customers met';
            ds2.Total_Actual_MTD__c = 12;
            ds2.Total_Monthly_Target__c = 200;
            ds2.Total_MTD_Target__c = 12;
            ds2.Year__c = '2015';
            insert ds2;
    }
}