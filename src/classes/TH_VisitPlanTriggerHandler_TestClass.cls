/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TH_VisitPlanTriggerHandler_TestClass {
	
	//private static Account account;	
	//private static Prospect_Customer_Counter__c counterL;
	private static PJP__c pjp;
	private static List<Visits_Plan__c> visits;
	private static DSE_Beat__c dse_beat;


    public static void initializeTestData(){
        Integer tempNumber = Integer.valueOf(Math.random()*100000000);
        
        Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
        database.upsert(counterL);
        
        Account account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
        database.insert(account);  

        dse_beat = CEAT_InitializeTestData.createDseBeat('BEAT01',account.Id,'DSC01',account.Name,'SFS','HTM','33DF');
        //database.insert(dse_beat);     

        pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
        database.insert(pjp);
        
        visits = CEAT_InitializeTestData.createMultipleVisits(pjp,null,dse_beat.Id,true);
        database.insert(visits);
        
        for(Visits_Plan__c vp : visits){
            vp.Status__c = 'Successful';
        }
        
        database.update(visits);
    }    
    
    static testMethod void myUnitTest() {
        Test.startTest();
    	initializeTestData();
        Test.stopTest();
    }
       
}