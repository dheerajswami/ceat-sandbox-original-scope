/* ================================================
@Name:  ProformaInvoiceDetailViewController
@Copyright notice: 
Copyright (c) 2015, CEAT and developed by Extentor
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are not permitted.                                                                                                    
@====================================================
@====================================================
@Purpose: This Controller class serves as Extension to Proforma_Invoice__c Standard Controller (Used in ProformaInvoiceDetailView_V2.Page)                                                                                                                 
@====================================================
@====================================================                                                                                                                                                                                                                                         
@AUTHOR______________LAST MODIFIED DATE______________DETAIL                   
Neha@extentor        05/08/2015        INITIAL DEVELOPMENT                                 

@=======================================================  */

global class ProformaInvoiceDetailViewController {
    public String proformaInvoiceId {get; set;}
    public String quotationDate {get; set;}
    public String pricingDate {get; set;}
    public String requestedDelvDate {get; set;}
    public Boolean showConfirmButton {get; set;}
    public Proforma_Invoice__c proformaInvoice {get; set;}
    public List<PI_Line_Item__c> piLineItemList {get; set;}

    public ProformaInvoiceDetailViewController(ApexPages.StandardController stdController) {
        proformaInvoiceId = ApexPages.currentPage().getParameters().get('Id');
        if(Network.getNetworkId() == null) {
            showConfirmButton = true;
        }
        proformaInvoice = [SELECT Adanced_payment_details__c,Advance_Payment__c,Clubbing_details__c,
                                Clubbing_of_containers__c,Customer_Name__c,Customer_Number__c,Customer__c,
                                Customized_Container__c,Date_for_pricing_and_exchange_rate__c,
                                Date_until_which_bid_quotation_binding__c,Distribution_Channel__c,Division__c,
                                Import_License_Requirement__c,Incoterms_part_1__c,Incoterms_Part_2__c,
                                Incoterms__c,Inco_Terms_Name__c,Inspection_Agency_Name__c,Inspection_Required__c,
                                LC_Required__c,Mixing_of_Pro_forma__c,Nomination_of_shipping_line__c,
                                No_of_Containers__c,Packing_List_Code__c,Packing_List_Requirement__c,
                                Packing_List_Req_Name__c,Port_of_Destination__c,Port_of_Discharge_Name__c,
                                Price_Reference__c,Quotation_Inquiry_is_valid_from__c,Requested_delivery_date__c,
                                Sales_Document_Type__c,Sales_Group__c,Sales_Office__c,Sales_Organization__c,
                                SD_Document_Currency_Code__c,SD_Document_Currency_Name__c,SD_Document_Currency__c,
                                Shipping_Line_Name__c,Ship_to_party__c,Vehicle_Type_Code__c,
                                Vehicle_Type_Name__c,Vehicle_type__c 
                                FROM Proforma_Invoice__c WHERE Id =: proformaInvoiceId];
        if(proformaInvoice.Date_until_which_bid_quotation_binding__c != null) {
            quotationDate = (proformaInvoice.Date_until_which_bid_quotation_binding__c).format();
        }   
        pricingDate = (proformaInvoice.Date_for_pricing_and_exchange_rate__c).format();
        requestedDelvDate = (proformaInvoice.Requested_delivery_date__c).format();
        System.debug('==# Container at invoice level '+ proformaInvoice.No_of_Containers__c);
        fetchRelatedLineItems(proformaInvoiceId);

    }

    public List<PI_Line_Item__c> fetchRelatedLineItems(Id proformaInvoiceId) {
        piLineItemList = new List<PI_Line_Item__c>();

        for(PI_Line_Item__c tmpPli : [SELECT Brand__c,Category__c,Material_Master__r.Name,
                        Material_Number__c,Name,Quantity__c,Price__c,Proforma_Invoice__c,No_of_Container__c,Total_Price__c
                        FROM PI_Line_Item__c WHERE Proforma_Invoice__c =: proformaInvoiceId]) {
            piLineItemList.add(tmpPli);

        }
        System.debug('==# No Of Container '+piLineItemList);
        return piLineItemList;
    }


    @RemoteAction
    global static String submitInvoiceToSAP(String recordId) {
                System.debug('======= '+recordId);
                try {
                        Proforma_Invoice__c proformaInvoice = [SELECT Adanced_payment_details__c,Advance_Payment__c,Clubbing_details__c,
                                        Clubbing_of_containers__c,Customer_Name__c,Customer_Number__c,Customer__c,
                                        Customized_Container__c,Date_for_pricing_and_exchange_rate__c,
                                        Date_until_which_bid_quotation_binding__c,Distribution_Channel__c,Division__c,
                                        Import_License_Requirement__c,Incoterms_part_1__c,Incoterms_Part_2__c,
                                        Incoterms__c,Inco_Terms_Name__c,Inspection_Agency_Name__c,Inspection_Required__c,
                                        LC_Required__c,Mixing_of_Pro_forma__c,Nomination_of_shipping_line__c,
                                        No_of_Containers__c,Packing_List_Code__c,Packing_List_Requirement__c,
                                        Packing_List_Req_Name__c,Port_of_Destination__c,Port_of_Discharge_Name__c,
                                        Price_Reference__c,Quotation_Inquiry_is_valid_from__c,Requested_delivery_date__c,
                                        Sales_Document_Type__c,Sales_Group__c,Sales_Office__c,Sales_Organization__c,
                                        SD_Document_Currency_Code__c,SD_Document_Currency_Name__c,SD_Document_Currency__c,
                                        Shipping_Line_Name__c,Ship_to_party__c,SAP_Quote_Number__c,Distribution_Channel_PickList__c,
                                        Vehicle_Type_Code__c,DivisionPickList__c, 
                                        Vehicle_Type_Name__c,Vehicle_type__c 
                                        FROM Proforma_Invoice__c WHERE Id =: Id.valueOf(recordId)];
                        System.debug('====== '+proformaInvoice);

                        List<PI_Line_Item__c> lineItemList = [SELECT Brand__c,Category__c,Material_Master__r.Name,
                                                Material_Number__c,Price__c,Name,Quantity__c,Proforma_Invoice__c
                                                FROM PI_Line_Item__c WHERE Proforma_Invoice__c =: proformaInvoice.Id];
                    List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H> pInvoice_Header = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H>();
                        List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I> pInvoice_Items = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I>();

                        SapProformaInvoiceSoap.ZSFDC_QUATATION_H zh = new SapProformaInvoiceSoap.ZSFDC_QUATATION_H();
                        zh.AUART = proformaInvoice.Sales_Document_Type__c; //cluster manager
                        zh.VKORG = Label.Sales_Organisation;
                        zh.VTWEG = proformaInvoice.Distribution_Channel_PickList__c;  //picklist 07/09
                        zh.SPART = proformaInvoice.DivisionPickList__c;  //picklist 01/02/03/04....11
                        zh.PRSDT = String.valueOf(proformaInvoice.Date_for_pricing_and_exchange_rate__c);
                        zh.ANGDT = String.valueOf(proformaInvoice.Quotation_Inquiry_is_valid_from__c); //cluster manager '2015-07-07'
                        zh.BNDDT = String.valueOf(proformaInvoice.Date_until_which_bid_quotation_binding__c);
                        zh.EDATU = String.valueOf(proformaInvoice.Requested_delivery_date__c);
                        zh.WAERK = proformaInvoice.SD_Document_Currency_Code__c; //USD
                        zh.DZTERM = '';
                        zh.INCO1 = proformaInvoice.Incoterms_part_1__c;
                        zh.INCO2 = proformaInvoice.Incoterms_Part_2__c;//'ALEXANDRA';
                        zh.KUNNR = UtilityClass.addleadingZeros(proformaInvoice.Customer_Number__c, 10); //'0056000039'
                        zh.ZNUMCNTNR = '0'+String.valueOf(Math.floor(proformaInvoice.No_of_Containers__c));
                        zh.ZIMPRTLCSREQ = proformaInvoice.Import_License_Requirement__c.substring(0, 1);
                        zh.ZPCKGREQ = proformaInvoice.Packing_List_Code__c;
                        zh.ZINSPREQ = proformaInvoice.Inspection_Required__c.substring(0, 1);
                        zh.ZINSPAGNCY = (proformaInvoice.Inspection_Agency_Name__c == null ? 'NA':proformaInvoice.Inspection_Agency_Name__c);
                        zh.ZSIZEOFCNTNR = proformaInvoice.Vehicle_Type_Code__c;
                        zh.ZCUSTCNTNR = proformaInvoice.Customized_Container__c.substring(0, 1);
                        zh.ZPRCREF = Label.Price_Reference;//'CURR.'
                        zh.KUNNR1 = (proformaInvoice.Ship_to_party__c != null ? UtilityClass.addleadingZeros(proformaInvoice.Ship_to_party__c, 10):'');
                        zh.ZNOMSHP = proformaInvoice.Nomination_of_shipping_line__c.substring(0, 1);
                        zh.ZMIXPI = proformaInvoice.Mixing_of_Pro_forma__c.substring(0, 1);
                        zh.ZCLUBCNTNR = proformaInvoice.Clubbing_of_containers__c.substring(0, 1);
                        zh.ZADVPAYMNT = '';
                        zh.VKGRP = '';
                        zh.VKBUR = '';
                        zh.ZLICSNUM = '';
                        pInvoice_Header.add(zh);
                        System.debug('=== PIinvoiceHeader '+pInvoice_Header);
                        for(PI_Line_Item__c piLi : lineItemList) {
                            SapProformaInvoiceSoap.ZSFDC_QUATATION_I zi = new SapProformaInvoiceSoap.ZSFDC_QUATATION_I();
                            zi.MATNR = UtilityClass.addleadingZeros(piLi.Material_Number__c, 18);
                            zi.WERKS = Label.Plant;//'1010'
                            zi.LGORT = Label.Storage_Location;//'FGSL'
                            zi.MENGE = String.valueOf(piLi.Quantity__c);
                            pInvoice_Items.add(zi);
                        }
                        System.debug('=== pInvoice_Items '+pInvoice_Items);
                        String responseMessage = '';
                        String quotationNumber ='';
                        String sfdcResp ='';
                        responseMessage = GetProformaInvoiceFromSap.CallWebServiceGetUnResStock(pInvoice_Header, pInvoice_Items);
                        
                        if(responseMessage.contains('Had been Successfuly generat')) {
                            quotationNumber = responseMessage.split(' ')[1];
                            sfdcResp = 'Proforma Invoice has been created in SAP. Please note your Quotation Number '+quotationNumber+' for future reference.';
                            try {
                                proformaInvoice.SAP_Quote_Number__c = quotationNumber;
                                update proformaInvoice;
                            }
                            catch (Exception e) {
                                throw new customException('Quotation Number could not be saved in SFDC but has been created in SAP. Please note your Quotation Number '+quotationNumber);
                            }
                            return sfdcResp;
                        }
                        else {
                            sfdcResp = 'Proforma Invoice could not be created in SAP. Please contact your Administrator for more details.';
                           return sfdcResp; 
                        }
                        
                        //return '';
                }
                catch(Exception e) {
                        throw new customException('Something went wrong. Quotation could not be created in SAP. Please contact your Administrator.');
                }
          //return '';
    }



    public class customException extends Exception{}

}