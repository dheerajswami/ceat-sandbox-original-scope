/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By:
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/UpdateMobileAccounts/*')

global with sharing class WB_MobileAccountData {
    
    
    global class MobileData{
        public List<Account> accounts {get;set;}
        public List<Customer_Location__c> custLocation {get;set;}
        public List<Contact> contacts {get;set;}
        
        public MobileData(){
            accounts = new List<Account>();
            custLocation = new List<Customer_Location__c>();
            contacts = new List<Contact>();
        }
    }
    
    global class DataReturn{
        public List<Account> accounts {get;set;}
        public List<State_Master__c> picklistValues {get;set;}
        
        public DataReturn(){
            accounts = new List<Account>();
            picklistValues = new List<State_Master__c>();
        }
    }
    
    @HttpPost
    global static DataReturn getAccountListAndUpdate(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response; 
        List<Account> allAccountsToSend = new List<Account>();
        DataReturn dt = new DataReturn();
        try{
            MobileData accountsToUpdate = new MobileData();
            system.debug(req.requestBody.toString());
            accountsToUpdate = (MobileData)JSON.deserialize(req.requestBody.toString(),MobileData.class);
            
            List<Customer_Location__c> loc = new List<Customer_Location__c>();
            update accountsToUpdate.accounts;
            
            insert accountsToUpdate.custLocation;
            insert accountsToUpdate.contacts;
            system.debug(req.requestBody.toString());
            
            allAccountsToSend = [SELECT Id,No_of_location_captured__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
            dt.accounts = allAccountsToSend;
            dt.picklistValues = getAllStateMaster(UserInfo.getUserId());
            return dt;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            if(!allAccountsToSend.isEmpty()){
                allAccountsToSend = [SELECT Id,No_of_location_captured__c,Customer_Segment__c,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,FirstName,LastName,Salutation,Birthdate,Type__c FROM  Contacts) FROM Account ];
                dt.accounts = allAccountsToSend;
                return dt; 
            }else{
                return dt;
            }
        }
    }
    
    // Fetch All State Values
    global static List<State_Master__c> getAllStateMaster(String uId){
        
        Set<String> territoryNames = new Set<String>();
        Set<String> parentTerritoryId = new Set<String>();
        Set<String> ro_Names = new Set<String>();
        for(UserTerritory2Association ul :[SELECT Id,IsActive,RoleInTerritory2,Territory2Id,Territory2.Name,UserId,User.Name FROM UserTerritory2Association WHERE UserId = :uId] ) {
            territoryNames.add(ul.Territory2.Name);
        }
        
        for(Territory2 tl : [SELECT Id,AccountAccessLevel,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId FROM Territory2 WHERE DeveloperName IN : territoryNames]){
            parentTerritoryId.add(tl.ParentTerritory2Id);
        }
        
        for(Territory2 tl : [SELECT Id,AccountAccessLevel,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId FROM Territory2 WHERE Id IN : parentTerritoryId]){
            ro_Names.add(tl.Name);
        }
        
        
        
        
            
        return [SELECT Id,Town__c,District__c,State__c FROM State_Master__c WHERE RO__c IN : ro_Names];
    }
}