public with sharing class Dashboard_Output_Handler_Controller{        
    
    //Wrapper classes    
    public class dashboardWrapper implements Comparable{
        public String catName{get;set;}
        public Double score{get;set;}
        public String catWeight{get;set;}
        public Decimal columnOrder{get;set;}
        public List<Dashboard_Score__c> dashboardScore{get;set;}
        public List<Dashboard_Output_Score__c> dashboardOutputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
        public Integer compareTo(Object compareTo) {
            dashboardWrapper compareToCat = (dashboardWrapper)compareTo;
            if (columnOrder == compareToCat.columnOrder) return 0;
            if (columnOrder > compareToCat.columnOrder) return 1;
            return -1;        
        }
    }
    
    public Dashboard_Output_Handler_Controller(){
    }
     /*
    * Method Name   : getDashboardDataRecords
    * Description   : It will return Dashboard score, dashboard output score and dashboard summary score 
    *                 related to loggedin user territory for current month 
    * @param        : Loggedin User territory ex B0001
    * @returns      : List<dashboardWrapper>
    */
    public  List<dashboardWrapper> getDashboardDataRecords(String loggedInUserTerritory){
        String currentDay              = String.valueOf((Date.today()).day());
        String currentMonth            = String.valueOf((Date.today()).month());
        String currentYear             = String.valueOf((Date.today()).year());
        String catDashList             = '';
        list<Dashboard_Weightage_Master__c> dashboardWeight = new List<Dashboard_Weightage_Master__c>(); 
        list<Dashboard_Score__c> dashboardScore = new List<Dashboard_Score__c>(); 
        list<Dashboard_Output_Score__c> dashboardOutputScore = new List<Dashboard_Output_Score__c>(); 
        list<Dashboard_Score__c> dashScorList ;
        list<Dashboard_Output_Score__c> dashoutScore ;
        list<Dashboard_Master__c> dashboardMaster               = new list<Dashboard_Master__c>();
        list<string> dashcatSet                                 = new list<String>();
        list<String> catlist = new List<String>();
        Dashboard_Summary_Score__c dashSummaryScore = new Dashboard_Summary_Score__c();
        Set<String> SetOFCat = new Set<String>();
        List<String> catsetParam                                 = new List<String>();
        Set<String> setOFParametersAllCat                       = new Set<String>();
        Map<String,String> MapOFCatAndCode = new Map<String,String>();
        List<String> catSet                                      = new List<String>();
        Set<String> catdashSet                                  = new Set<String>();
        Map<String,Double> MapOFCatAndWeigth = new Map<String,Double>();
        map<string,string> mapofTerritoryType = new map<String,string>();
        Map<String,String> mapOFDashCatAndCatName                                   = new Map<String,String>();
        List<Dashboard_Score__c> daList = new list<Dashboard_Score__c>();
        List<Dashboard_Output_Score__c > daoutList = new list<Dashboard_Output_Score__c >();
        list<UserTerritory2Association > userASS = new list<UserTerritory2Association>();
        Dashboard_Score__c dsrecord ;
        Map<String,List<Dashboard_Score__c>> MapOFCatAndDashboardScore = new Map<String,list<Dashboard_Score__c>>();
        Map<String,List<Dashboard_Output_Score__c >> MapOFCatAndDashboardOutScore = new Map<String,list<Dashboard_Output_Score__c >>();
        Map<String,Dashboard_Weightage_Master__c> mapOFCatAndParam                  = new Map<String,Dashboard_Weightage_Master__c>();
        Map<String,List<String>> mapOFDashCatAndCatCode          = new Map<String,List<String>>();
        list<Sales_Planning_Categories__c> dashboardCat = Sales_Planning_Categories__c.getall().values();
            for(Sales_Planning_Categories__c sp:dashboardCat){
              if(sp.Include_in_Dashboard__c == true){
                catdashSet.add(sp.Category_Code__c); 
                if(mapOFDashCatAndCatCode.containsKey(sp.Category_name_as_in_Dashboard__c)) {
                    catSet          = new List<String>();
                    catSet          = mapOFDashCatAndCatCode.get(sp.Category_name_as_in_Dashboard__c); 
                    catset.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }else{
                    catSet          = new List<String>();
                    catSet.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }
              }      
        }
        userASS = [SELECT IsActive, RoleInTerritory2, Territory2Id, Territory2.DeveloperName FROM UserTerritory2Association WHERE (RoleInTerritory2 = 'TLD' OR RoleInTerritory2 = 'TL_Replacement')];
        if(userASS.size() > 0){
            for(UserTerritory2Association u : userAss){
                mapofTerritoryType.put(u.Territory2.DeveloperName,u.RoleInTerritory2);           
            }
        }
        dashboardMaster             = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true AND Role__c =:  mapofTerritoryType.get(loggedInUserTerritory)];
        if(mapofTerritoryType.containsKey(loggedInUserTerritory)){
                 dashboardWeight = [SELECT id,Category__c,Parameters_Output__c,Role__c,Testing__c,Weightage__c FROM Dashboard_Weightage_Master__c where RecordType.Name='Output' AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c =:  mapofTerritoryType.get(loggedInUserTerritory)];
        }
        system.debug('dashboardWeight'+dashboardWeight.size());
        for(Dashboard_Weightage_Master__c param : dashboardWeight){
            mapOFCatAndParam.put(param.Category__c,param);
            catsetParam             = mapOFDashCatAndCatCode.get(param.Category__c);
            dashcatSet.addAll(catsetParam);
            String[] paramSplit     = (param.Parameters_Output__c).split(';');
            if(paramSplit.size() > 0){
                for(String st : paramSplit){
                    setOFParametersAllCat.add(st);
                }
            }  
        }
        if(loggedInUserTerritory!=null){
             dashboardScore  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c where Territory_Code__c=:loggedInUserTerritory AND Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN : setOFParametersAllCat];
            dashboardOutputScore = [SELECT ID,Total_Monthly_Target__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Final_Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c where Territory_Code__c=:loggedInUserTerritory  AND Month__c =: currentMonth AND Year__c =: currentYear];//where DBExternalId__c='2010B008242015'
            dashSummaryScore = [SELECT ID,Achievement__c,All_Category_Score1__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Weightage__c,DSS_External_ID__c,Month__c,Score__c,Territory_Code__c,Total_MTD_Actual__c,Total_MTD_Target__c,Total_Output_Score__c,Total_Value__c,Total_Value_L3M__c,Total_Value_LYCM__c,Year__c FROM Dashboard_Summary_Score__c WHERE Territory_Code__c=:loggedInUserTerritory  AND Month__c =: currentMonth AND Year__c =: currentYear];
        }else{
             dashboardScore  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c];
             dashboardOutputScore = [SELECT ID,Total_Monthly_Target__c,Final_Score__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c];
        
        }
        for(Dashboard_Weightage_Master__c ss : dashboardWeight ){
            SetOFCat.add(ss.Category__c);
            MapOFCatAndWeigth.put(ss.Category__c,ss.Weightage__c);
        }       
        
        for(Dashboard_Score__c ds : dashboardScore  ){
               dashScorList = new list<Dashboard_Score__c>();
               if(MapOFCatAndDashboardScore.containsKey(ds.Category__c)){
                   dashScorList = MapOFCatAndDashboardScore.get(ds.Category__c);
                   dashScorList.add(ds);
                   MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
               }else{
                   dashScorList = new list<Dashboard_Score__c>();
                   dashScorList.add(ds);
                   MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
               }
               //daList.add(ds);               
        }
       // MapOFCatAndDashboardScore.put('2010',daList);
        for(Dashboard_Output_Score__c temp : dashboardOutputScore ){
            dashoutScore = new list<Dashboard_Output_Score__c>();
            if(MapOFCatAndDashboardOutScore.containsKey(temp.Category__c)){
                   dashoutScore = MapOFCatAndDashboardOutScore.get(temp.Category__c);
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }else{
                   dashoutScore= new list<Dashboard_Output_Score__c>();
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }
           // daoutList .add(temp);
        }
        
        //MapOFCatAndDashboardOutScore.put('2010',daoutList);
        List<dashboardWrapper> dashboardRecord      = new List<dashboardWrapper>();
        
        for(String str : SetOFCat){
             dashboardWrapper c         =   new dashboardWrapper();
             c.catName = str;
            // c.score= 20;
             c.catWeight = String.ValueOF(MapOFCatAndWeigth.get(str));
             c.columnOrder = (Sales_Planning_Categories__c.getValues(mapOFDashCatAndCatName.get(str))).Dashboard_Sort_Order__c;
             catset = new list<String>();
             catlist = new List<String>();
             if(mapOFDashCatAndCatCode.ContainsKey(str)){
                  catlist=    mapOFDashCatAndCatCode.get(str);
                  catlist.sort();
             }
             if(catlist.size() > 0){
                catDashList = '';
                for(String s : catlist){
                    if(catDashList == ''){
                        catDashList = s;
                    }else{
                        catDashList = catDashList +':'+ s;
                    }   
                }
            }
             c.dashboardScore = MapOFCatAndDashboardScore.get(catDashList);
             c.dashboardOutputScore  = MapOFCatAndDashboardOutScore.get(catDashList);
             c.dashboardSummaryScore = dashSummaryScore;
             
             if(MapOFCatAndDashboardOutScore.get(catDashList) !=null && MapOFCatAndDashboardScore.get(catDashList)!=null){
                 dashboardRecord.add(c);
             }
             dashboardRecord.sort();
        }
        return dashboardRecord;    
    }
}