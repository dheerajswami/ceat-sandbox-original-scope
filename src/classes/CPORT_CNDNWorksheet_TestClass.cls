/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CPORT_CNDNWorksheet_TestClass {

    public static void init(){
    	SAPLogin__c saplogin = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
    }


    static testMethod void myUnitTest() {
    	init();
    	Test.startTest();    	    	        
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockImplCNDNWorkSheet());
        	CPORT_CNDNWorksheet.getAllDiscounts('50003327');
        Test.stopTest();
    }

    private class WebServiceMockImplCNDNWorkSheet implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            SapCNDN_WorkSheet   tst                         = new SapCNDN_WorkSheet();
		    SapCNDN_WorkSheet.ZWS_SFDC_WORKSHEET   zws      = new SapCNDN_WorkSheet.ZWS_SFDC_WORKSHEET();
		    
		    SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_NEW PPD_NEW 				= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_NEW();
		    SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_PRO PRODUCT_DISCOUNT     = new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_PRO();        
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_AD AD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_AD();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB DEBIT_NOTE_SLAB 	= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PLD PLD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PLD();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD PPD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_DEBIT PPD_DEBIT 			= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_DEBIT();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET SLAB 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET();
			SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_TLD TLD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_TLD();
			SapCNDN_WorkSheet.ZSFDC_WORKSHEETResponse_element elem 					= new SapCNDN_WorkSheet.ZSFDC_WORKSHEETResponse_element();
            



            SapCNDN_WorkSheet.ZSFDC_WORKDHEET_AD AD_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_AD();

            AD_item.CIRCULAR = '0050023231';
			AD_item.CUSTOMER = 'NAME';
			AD_item.NAME = 'NAME';
			AD_item.SCHEMENAME = 'NAME';
			AD_item.APPT_DATE = 'DATE';
			AD_item.BASE_SDS = 'BASE_SDS';
			AD_item.TARGET = 'TARGET';
			AD_item.PRE_TAX = 'PRE TAX'; 
			AD_item.POST_TAX = 'POST TAX';
			AD_item.ROTATION = 'ROTATION';
			AD_item.ROTATION_COMPLETED = 'ROTATION';
			AD_item.AVG_SDS = 'AVG_SDS';
			AD_item.INCENTIVE_RATE = 'public String ' ;
			AD_item.INCENTIVE = 'INCENTIVE';
			AD_item.ELIGIBLE = 'ELIGIBLE';

			AD.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_AD>();
			AD.item.add(AD_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_DEBIT_SLAB debit_slab_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_DEBIT_SLAB();

            debit_slab_item.SCHEMENAME = 'SCHEMENAME';
			debit_slab_item.PRETAX = 'PRETAX';
			debit_slab_item.POSTTAX = 'POSTTAX';
			debit_slab_item.DISCOUNT = 'DISCOUNT';
			debit_slab_item.QUANTITY = 'QUANTITY';
			debit_slab_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			debit_slab_item.ELIGIBILITY = 'ELIGIBILITY';
			debit_slab_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			DEBIT_NOTE_SLAB.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_DEBIT_SLAB>();
			DEBIT_NOTE_SLAB.item.add(debit_slab_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PLD pld_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PLD();

			pld_item.SCHEMENAME = 'SCHEMENAME';
			pld_item.PRETAX = 'PRETAX';
			pld_item.POSTTAX = 'POSTTAX';
			pld_item.AVG_SALES_VALUE = 'AVG_SALES_VALUE';
			pld_item.SALES_VALUE_DISB = 'SALES_VALUE_DISB';
			pld_item.SALES_QTY_DISB = 'SALES_QTY_DISB';
			pld_item.AVG_QTY = 'AVG_QTY';
			pld_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			pld_item.DISCOUNT = 'DISCOUNT';
			pld_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			PLD.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PLD>();
			PLD.item.add(pld_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD ppd_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD();

			ppd_item.SCHEMENAME = 'SCHEMENAME';
			ppd_item.BILLING_DOCUMENT = 'BILLING_DOCUMENT';
			ppd_item.BILLING_DATE = 'BILLING_DATE';
			ppd_item.POSTING_DATE = 'POSTING_DATE';
			ppd_item.DUE_DATE = 'DUE_DATE';
			ppd_item.PAYMENT_TERMS = 'PAYMENT_TERMS';
			ppd_item.PAYMENT_DATE = 'PAYMENT_DATE';
			ppd_item.PAYMENT_DAYS = 23;
			ppd_item.PRE_TAX = 'PRE_TAX';
			ppd_item.POST_TAX = 'POST_TAX';
			ppd_item.DISCOUNT = 'DISCOUNT';
			ppd_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			ppd_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';
			
			PPD.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD>();
			PPD.item.add(ppd_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_DEBIT ppd_debit_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_DEBIT();

			ppd_debit_item.SCHEMENAME = 'SCHEMENAME';
			ppd_debit_item.PAYMENT_DONE = 'BILLING_DOCUMENT';
			ppd_debit_item.BODY_OF_INVOICE = 'BILLING_DATE';
			ppd_debit_item.POSTING_DATE = 'POSTING_DATE';
			ppd_debit_item.DUE_DATE = 'DUE_DATE';
			ppd_debit_item.ELIGIBILITY = 'PAYMENT_TERMS';
			ppd_debit_item.PAYMENT_DATE = 'PAYMENT_DATE';
			ppd_debit_item.PAYMENT_DAYS = 23;
			ppd_debit_item.PRETAX = 'PRE_TAX';
			ppd_debit_item.POSTTAX = 'POST_TAX';
			ppd_debit_item.DISCOUNT = 'DISCOUNT';
			ppd_debit_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			ppd_debit_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			PPD_DEBIT.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_DEBIT>();
			PPD_DEBIT.item.add(ppd_debit_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_NEW ppd_new_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_NEW();
			
			ppd_new_item.SCHEMENAME = 'SCHEMENAME';
			ppd_new_item.CIRCULAR = 'CIRCULAR';
			ppd_new_item.CUSTOMER = 'CUSTOMER';
			ppd_new_item.NAME = 'NAME';
			ppd_new_item.BILLING_DATE = 'BILLING_DATE';
			ppd_new_item.DUE_DATE = 'DUE_DATE';
			ppd_new_item.PAYMENT_TERMS = 'PAYMENT_TERMS';
			ppd_new_item.PAYMENT_DATE = 'PAYMENT_DATE';
			ppd_new_item.PAYMENT_DAYS = 12;
			ppd_new_item.PRE_TAX = 'PRE_TAX';
			ppd_new_item.POST_TAX = 'POST_TAX';
			ppd_new_item.DISCOUNT ='DISCOUNT_TYPE';
			ppd_new_item.DISCOUNT_RATE = 'DISCOUNT_TYPE';
			ppd_new_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			PPD_NEW.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_NEW>();
			PPD_NEW.item.add(ppd_new_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_PRO ppd_pro_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_PRO();

			ppd_pro_item.CIRCULAR = 'CIRCULAR';
			ppd_pro_item.CUSTOMER = 'CUSTOMER';
			ppd_pro_item.NAME = 'NAME';
			ppd_pro_item.SCHEMENAME = 'SCHEMENAME';
			ppd_pro_item.QUANTITY = 'QUANTITY';
			ppd_pro_item.PRE_TAX = 'PRE_TAX';
			ppd_pro_item.POST_TAX = 'POST_TAX';
			ppd_pro_item.DISCOUNT = 'DISCOUNT_TYPE';
			ppd_pro_item.DISCOUNT_TYPE = 'DISCOUNT';

			PRODUCT_DISCOUNT.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_PRO>();
			PRODUCT_DISCOUNT.item.add(ppd_pro_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET slab_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET();

			slab_item.CIRCULAR = 'CIRCULAR';
			slab_item.CUSTOMER = 'CUSTOMER';
			slab_item.NAME = 'NAME';
			slab_item.SCHEMENAME = 'SCHEMENAME';
			slab_item.AVG_SALE_QTY = 'AVG_SALE_QTY';
			slab_item.DISB_SALE_QTY = 'DISCOUNT_TYPE';
			slab_item.DISB_SALE_VAL = 'DISB_SALE_VAL';
			slab_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			slab_item.DISCOUNT = 'DISCOUNT';
			slab_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			SLAB.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET>();
			SLAB.item.add(slab_item);

			SapCNDN_WorkSheet.ZSFDC_WORKDHEET_TLD tld_item = new SapCNDN_WorkSheet.ZSFDC_WORKDHEET_TLD();

			tld_item.SCHEMENAME = 'SCHEMENAME';
			tld_item.SALES_VALUE = 'SALES_VALUE';
			tld_item.DISB_VALUE = 'DISB_QTY';
			tld_item.SALES_QTY = 'SALES_QTY';
			tld_item.DISB_QTY = 'DISB_QTY';
			tld_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			tld_item.DISCOUNT = 'DISCOUNT';
			tld_item.DISCOUNT_TYPE = 'DISCOUNT_RATE';

			TLD.item = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_TLD>();
			TLD.item.add(tld_item);

            elem.AD = AD;
            elem.DEBIT_NOTE_SLAB = DEBIT_NOTE_SLAB;
            elem.PLD = PLD;
            elem.PPD = PPD;
            elem.PPD_DEBIT = PPD_DEBIT;
            elem.PPD_NEW = PPD_NEW;
            elem.PRODUCT_DISCOUNT = PRODUCT_DISCOUNT;
            elem.SLAB = SLAB;
            elem.TLD = TLD;
            
            SapCNDN_WorkSheet.ZSFDC_WORKSHEET_element req = new SapCNDN_WorkSheet.ZSFDC_WORKSHEET_element();

            req.AD = AD;
            req.DEBIT_NOTE_SLAB = DEBIT_NOTE_SLAB;
            req.DOCUEMENT_NO = '50003327';
            req.PLD = PLD;
            req.PPD = PPD;
            req.PPD_DEBIT = PPD_DEBIT;
            req.PPD_NEW = PPD_NEW;
            req.PRODUCT_DISCOUNT = PRODUCT_DISCOUNT;
            req.SLAB = SLAB;
            req.TLD = TLD;
            
            response.put('response_x', elem);
            return;
        }
        
        
    }
}