global class Claim_getClaimStatus {
	
    global class claimStatusMapping {
    	public string claim_notificationNumber {get;set;}
    	public string claim_plantForNotification {get;set;}
    	public string claim_recordCreatedDate {get;set;}
    	public string claim_soldToParty {get;set;}
    	public string claim_soldToPartyName {get;set;}
    	public string claim_customerName {get;set;}
    	public string claim_customerAddress {get;set;}
    	public string claim_materialNumber {get;set;}
    	public string claim_materialGroup {get;set;}
    	public string claim_materialDescription {get;set;}
    	public string claim_salesGroup {get;set;}
    	public string claim_serialNumber {get;set;}
    	public string claim_inspectionDate {get;set;} 
    	public string claim_codeGroupProblem {get;set;}
    	public string claim_problemOrDamageCode {get;set;}
    	public string claim_defectCode {get;set;}
    	public decimal claim_actualNSD {get;set;}
    	public decimal claim_wearPercent {get;set;}
    	public decimal claim_nbp {get;set;}
    	public decimal claim_replOffer {get;set;}
    	public decimal claim_claimLoss {get;set;}
    	public string claim_invoiceCL10 {get;set;}
    	public string claim_cl9Nd6 {get;set;}
    	public string claim_cl9Nd6Date {get;set;}
    	public string claim_invoiceCL10Date {get;set;} 
    	public string claim_insepectedBy {get;set;}
    	public string claim_docketNumber {get;set;}
    	public string claim_mobileNumber {get;set;} 
    	public string claim_salesDistrict {get;set;}
    	public string claim_outDoorInspection {get;set;}
    	public string claim_billingDateforBilling {get;set;}
    	
    	public claimStatusMapping (String claim_notificationNumber,String claim_plantForNotification,String claim_recordCreatedDate,
    	String claim_soldToParty, String claim_soldToPartyName,String claim_customerName,String claim_customerAddress,String claim_materialNumber, String claim_materialGroup,
    	String claim_materialDescription, String claim_salesGroup, String claim_serialNumber, String claim_inspectionDate, String claim_codeGroupProblem,
    	string claim_problemOrDamageCode,string claim_defectCode,string claim_actualNSD,string claim_wearPercent,string claim_nbp,string claim_replOffer,string claim_claimLoss,
    	string claim_invoiceCL10,string claim_cl9Nd6,string claim_cl9Nd6Date,string claim_invoiceCL10Date,string claim_insepectedBy,string claim_docketNumber,string claim_mobileNumber,
    	string claim_salesDistrict,string claim_outDoorInspection,string claim_billingDateforBilling ){
			this.claim_notificationNumber = claim_notificationNumber;	
    		this.claim_plantForNotification = claim_plantForNotification;
    		this.claim_recordCreatedDate = claim_recordCreatedDate;
    		this.claim_soldToParty = claim_soldToParty;
    		this.claim_soldToPartyName = claim_soldToPartyName;
    		this.claim_customerName = claim_customerName;
    		this.claim_customerAddress = claim_customerAddress;
    		this.claim_materialNumber = claim_materialNumber;
    		this.claim_materialGroup = claim_materialGroup;
    		this.claim_materialDescription = claim_materialDescription;
    		this.claim_salesGroup = claim_salesGroup;
    		this.claim_serialNumber = claim_serialNumber;
    		this.claim_inspectionDate = claim_inspectionDate;
    		this.claim_codeGroupProblem = claim_codeGroupProblem;
    		this.claim_problemOrDamageCode =claim_problemOrDamageCode;
    		this.claim_defectCode = claim_defectCode;
    		this.claim_actualNSD = decimal.valueof(claim_actualNSD);
    		this.claim_wearPercent = decimal.valueof(claim_wearPercent);
    		this.claim_nbp = decimal.valueof(claim_nbp);
    		this.claim_replOffer = decimal.valueof(claim_replOffer);
    		this.claim_claimLoss = decimal.valueof(claim_claimLoss);
    		this.claim_invoiceCL10 = claim_invoiceCL10;
    		this.claim_cl9Nd6 =claim_cl9Nd6;
    		this.claim_cl9Nd6Date = claim_cl9Nd6Date;
    		this.claim_invoiceCL10Date = claim_invoiceCL10Date;
    		this.claim_insepectedBy = claim_insepectedBy;
    		this.claim_docketNumber = claim_docketNumber;
    		this.claim_mobileNumber = claim_mobileNumber;	
    		this.claim_salesDistrict = claim_salesDistrict;
    		this.claim_outDoorInspection = claim_outDoorInspection;
    		this.claim_billingDateforBilling = claim_billingDateforBilling;
    	}
    }
    
    @RemoteAction 
    WebService static List<claimStatusMapping> fetchClaimStatus (String claimNumber){
    system.debug('thisisClain '+claimNumber);
    List<claimStatusMapping> csm = new List<claimStatusMapping>(); 
    if(claimNumber != null){
    	try{
	        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
	        String username                   = saplogin.username__c;
	        String password                   = saplogin.password__c;
	        Blob headerValue                  = Blob.valueOf(username + ':' + password);
	        String authorizationHeader     = 'Basic '+ EncodingUtil.base64Encode(headerValue);
	        Sap_GetClaimStatusFromSAP_Updated ws = new Sap_GetClaimStatusFromSAP_Updated();
	        Sap_GetClaimStatusFromSAP_Updated.ZWS_SFDC_ZSD232_CLAIM clsInstance = new Sap_GetClaimStatusFromSAP_Updated.ZWS_SFDC_ZSD232_CLAIM();
	        clsInstance.inputHttpHeaders_x = new Map<String,String>();
            clsInstance.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            clsInstance.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            clsInstance.timeout_x = 60000;
	        Sap_GetClaimStatusFromSAP_Updated.TABLE_OF_ZSD232_OUTPUT IT_OUTPUT = new Sap_GetClaimStatusFromSAP_Updated.TABLE_OF_ZSD232_OUTPUT();
	        Sap_GetClaimStatusFromSAP_Updated.ZSFDC_ZSD232_CLAIMResponse_element claimResponseElement = new  Sap_GetClaimStatusFromSAP_Updated.ZSFDC_ZSD232_CLAIMResponse_element();
	        claimResponseElement = clsInstance.ZSFDC_ZSD232_CLAIM(IT_OUTPUT,claimNumber);
	        if(claimResponseElement.RETURN_x != ''){
	        	system.debug('eee'+claimResponseElement.RETURN_x);
	        	 throw new myException(string.valueof(claimResponseElement.RETURN_x));
	        }
	        Sap_GetClaimStatusFromSAP_Updated.TABLE_OF_ZSD232_OUTPUT claimResponses = new Sap_GetClaimStatusFromSAP_Updated.TABLE_OF_ZSD232_OUTPUT();
	        if(claimResponseElement != null){
	        	claimResponses = claimResponseElement.IT_OUTPUT;
	        	if(claimResponses.item != null){
	        		for(Sap_GetClaimStatusFromSAP_Updated.ZSD232_OUTPUT z : claimResponses.item){
	        			if(z != null){
	        				String yr;
                            String month;
                            String day;
                            String erdat;
                            system.debug('shwoDate  '+z.DATE_x); 
                            if(z.DATE_x != '') {
                                yr          = z.DATE_x.substring(0, 4);
                                month       = z.DATE_x.substring(5, 7);
                                day         = z.DATE_x.substring(8, 10);
                                erdat      = day+'.'+month+'.'+yr;
                            }else {
                                erdat      = z.DATE_x;
                            }
                            csm.add(new claimStatusMapping(z.NOTIFICATION_NUMBER,z.PLANT,erdat,z.SOLD_TO_PARTY,z.SOLD_TO_PARTY_NAME,z.CUSTOMER_NAME,z.CUSTOMER_ADDRESS,
                            z.MATERIAL_NUMBER,z.MATERIAL_GROUP,z.MATERIAL_DESCRIPTION,z.SALES_GROUP,z.SERIAL_NUMBER,z.INSPECTION_DATE,z.DISPOSITION,z.DAMAGE_CODE,
                            z.DAMAGE_DESP,z.ACTUAL_NSD,z.WEAR_PERCENTAGE,z.NBP_PRICE,z.REPL_OFFER,z.CLAIM_LOSS,z.INVOICE_OR_CL10,z.CL9_OR_ND6,z.CL9_OR_ND6_DATE,
                            z.INVOICE_OR_CL10_DATE,z.INSPECTED_BY,z.DOCKET_NUMBER,z.MOBILE_NUMBER,z.SALES_DISTRICT,z.OUT_DOOR_INSPECTION_SLIP_NO,z.OUT_DOOR_INSPECTION_SLIP_DATE));
	        			}
	        		}
	        	}
	        }
    	}
    	catch(Exception e){
            system.debug(e.getMessage());
            throw new myException(e.getMessage());
        }
    }
    return csm;
    }
    
    public class myException extends Exception {}
    
}