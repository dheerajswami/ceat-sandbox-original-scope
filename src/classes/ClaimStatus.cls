public class ClaimStatus
{
    public void executeWebserviceClaimStatus()
    {
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        String username                   = saplogin.username__c;
        String password                   = saplogin.password__c;
        Blob headerValue                  = Blob.valueOf(username + ':' + password);
        String authorizationHeader     = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        GetClaimStatusfromSap cs = new GetClaimStatusfromSap();
        GetClaimStatusfromSap.ZWS_SFDC_ZSD232_CLAIM zwscs = new GetClaimStatusfromSap.ZWS_SFDC_ZSD232_CLAIM();
        zwscs.inputHttpHeaders_x = new Map<String,String>();
        zwscs.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zwscs.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zwscs.timeout_x = 60000;
        GetClaimStatusfromSap.TABLE_OF_ZSD232_OUTPUT output = new GetClaimStatusfromSap.TABLE_OF_ZSD232_OUTPUT();
        GetClaimStatusfromSap.ZSFDC_ZSD232_CLAIMResponse_element cop = new GetClaimStatusfromSap.ZSFDC_ZSD232_CLAIMResponse_element();
        GetClaimStatusfromSap.ZSFDC_ZSD232_CLAIMResponse_element statusresponse = zwscs.ZSFDC_ZSD232_CLAIM(output , 'C-00000101');
        system.debug('========'+statusresponse.RETURN_x );
        if(statusresponse.RETURN_x != ''){
                system.debug('status response returnx-----'+statusresponse.RETURN_x);
                
            }
            GetClaimStatusfromSap.TABLE_OF_ZSD232_OUTPUT claimResponses = new GetClaimStatusfromSap.TABLE_OF_ZSD232_OUTPUT();
            if(statusresponse!= null){
                claimResponses = statusresponse.IT_OUTPUT;
                system.debug('claimResponses=='+claimResponses);
                if(claimResponses.item != null){
                    for(GetClaimStatusfromSap.ZSD232_OUTPUT z : claimResponses.item){
                        system.debug('----claim Number---'+z.CLAIM_NUMBER);
                    }
        } 
           }
}}