public class EfleetController {
    public static PageReference page {get; set;}

    public EfleetController(){}
    @RemoteAction 

    public static PageReference getPage(){
        
        /** Profile **/
        
        String replacementProfile    =      system.label.Replacement_Profile;
        String exportProfile         =      system.label.Export_Profile;
        String specialtyProfile      =      system.label.Specialty;
        
        
        /** Pages **/
        
        String rmEfleetpage         =       system.label.Efleet_Replacement_Page;
        String expEfleetpage        =       system.label.Efleet_Exports_Page;
        String speEfleetpage        =       system.label.Efleet_Specialty_Page;
        
        
        String loggedInUserID;
        
        /** Getting logged in User details **/
        
        loggedInUserID = UserInfo.getUserId();
        
        
        /** Querying logged in user details **/
        
        User usrDetail = [Select ProfileId, Name, Id, email from User where id =: loggedInUserID];
        
        
        /** Obtaining the profile of the logged in User **/
        
        String profileName = [Select Id, Name from Profile where Id=:usrDetail.profileId].Name;
        
        
        /** Redirecting page based on Profile Name **/
        
        if(profileName == replacementProfile){
            page= new PageReference('/apex/'+rmEfleetpage);
            page.setRedirect(true);
        }
        
        else if (profileName == exportProfile){
            page= new PageReference('/apex/'+expEfleetpage);
            page.setRedirect(true);
        }
        
        else if (profileName == specialtyProfile){
            page= new PageReference('/apex/'+speEfleetpage);
            page.setRedirect(true);
        }
        
        /** Redirect to home page**/
        else {
                page = new PageReference('/home/home.jsp');
                page.setRedirect(true);  
             }
    
        return page;
    }

}