global class scheduledBatch_ZO_Output_Dashboard implements Schedulable{
   @TestVisible private integer batchsize = 1; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest()){
           batchsize = 2000;
      } 
      Dashboard_BatchForDashboardOutputZO b = new Dashboard_BatchForDashboardOutputZO();
      database.executeBatch(b,batchsize);
   }
}