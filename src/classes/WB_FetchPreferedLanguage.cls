/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By: Fetch Mobile Data for PJP App for OE
||    v1.1 by Kishlay -  added for employee
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchPreferedLanguage/*')

global with sharing class WB_FetchPreferedLanguage {
    
    global class customerDetails{
            public String reference_Code     {get;set;}
            public String preferred_lang     {get;set;}
    }
    
    @HttpGet
    global static customerDetails getCustomerLanguage(){
        RestRequest req             = RestContext.request;
        RestResponse res            = RestContext.response;
        String reference_Code       = req.params.get('reference_Code');
        String flag                 = req.params.get('flag');
        
        
        try{
           if(flag != null && flag == 'D'){
               Account account = [SELECT Id,Name,Preferred_Language__c,KUNNR__c FROM Account WHERE KUNNR__c=:reference_Code];
               if(account.Preferred_Language__c != null){
                   customerDetails cusdetails =  new customerDetails();
                   cusdetails.reference_Code  =  account.KUNNR__c;
                   cusdetails.preferred_lang  =  account.Preferred_Language__c;  
                   return cusdetails; 
               }else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.reference_Code  =  '';
                   cusdetails.preferred_lang  =  '';  
                   return cusdetails; 
               }
               
           }else if(flag != null && flag == 'E'){
               Employee_Master__c employee = [SELECT id,Preferred_Language__c,Name,Passcode__c,EmployeeId__c FROM Employee_Master__c WHERE EmployeeId__c =: reference_Code];
               if(employee.Preferred_Language__c != null){
                   customerDetails cusdetails =  new customerDetails();
                   cusdetails.reference_Code  =  employee.EmployeeId__c ;
                   cusdetails.preferred_lang  =  employee.Preferred_Language__c;  
                   return cusdetails; 
               }else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.reference_Code  =  '';
                   cusdetails.preferred_lang  =  '';  
                   return cusdetails; 
               }
              
           }else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.reference_Code  =  '';
                   cusdetails.preferred_lang  =  '';  
                   return cusdetails; 
           }
           
           
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            customerDetails cusdetails = new customerDetails();
            cusdetails.reference_Code  =  '';
            cusdetails.preferred_lang  =  '';  
            return cusdetails; 
        }
        
       
    }
}