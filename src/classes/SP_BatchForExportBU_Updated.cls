global class SP_BatchForExportBU_Updated implements Database.Batchable<sObject> {    
    
    String exportDealer             = System.Label.Export_Dealer; 
    String exportForcast            = System.Label.Export_Forcast_BU;
    String recTypeExportForecast    = System.Label.Export_Forcast;
    Static String SPECIALTY='Specialty';
    Static String REPLECMENT='Replacement';
    String catZoneSpe = '';
    id exportRecTypeId;
    public String query;
    Date d = system.today();
    String month = String.valueOf(d.month());
    String year = String.valueOf(d.Year());
    Set<String> catExpSet = new Set<String>();
    id userId;
    list<Territory2Type> terriType                          = new list<Territory2Type>();
    list<Territory2Type> terriTypeCountry                   = new list<Territory2Type>();
    list<Territory2> listOFcluster                          =  new List<Territory2>();
    list<UserTerritory2Association> terrUserList            = new list<UserTerritory2Association>();
    set<id> terrTypeID                                      = new set<Id>();
    set<id> terrTypeIDCountry                               = new set<Id>();
    Map<id,String> mapOfClusterIDAndCode                    = new Map<id,String>();
    map<String,id> maoOfUserAndTerr                     = new Map<String,id>();
    String exportManager                                    = system.label.terrSrManagerExport;
    global SP_BatchForExportBU_Updated (){
        terriType               = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'Cluster'];
        exportRecTypeId = UtilityClass.getRecordTypeId('Account','Exports'); 
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        
        terriTypeCountry = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'Country'];
        for(Territory2Type temp : terriTypeCountry ){
            terrTypeIDCountry.add(temp.id);
            //mapOFterrIDAndName.put(temp.id,temp.DeveloperName);
        }
        terrUserList = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 = :exportManager ];
        if(terrUserList.size()>0){
            for(UserTerritory2Association ut: terrUserList){
                maoOfUserAndTerr.put(ut.Territory2.name,ut.UserId);              
            }
        }
        
    } 
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        
        
        list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
        for(Export_Sales_Planning_Category__c sp:exportCat){
            if(sp.Include_in_Sales_Planning__c == true){
                catExpSet.add(sp.Category_Code__c);  
            }      
        }
        system.debug(terriType+'terriType');
        system.debug(terriTypeCountry+'terriTypeCountry');
        
        String SPRP            = 'SPRP';
        String B0082           = 'AF03';
        String[] filters = new String[]{'SPRP%'};
            //query                    = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Name =  \'AF03\' AND Territory2TypeId IN: terrTypeID '; 
            query                  = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID '; 
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        Id exportForcastClusterId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeExportForecast Limit 1].Id;
        Id exportDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: exportDealer Limit 1].Id;
        List<Sales_Planing_Staging__c> listOfExportDealer = new list<Sales_Planing_Staging__c>();
        List<Sales_Planing_Staging__c> listOfExportForcast1 = new list<Sales_Planing_Staging__c>();
        List<Sales_Planing_Staging__c> listOfExportForcast = new list<Sales_Planing_Staging__c>();
        List<Sales_Planning__c> listOfExportForcastSalesPlanning = new list<Sales_Planning__c>();
        List<Sales_Planning__c> listOfExportDealersSalesPlanning = new list<Sales_Planning__c>();
        List<Sales_Planning__c> getExportforcastSalesPlanning = new list<Sales_Planning__c>();
        list<Territory2> listOFcluster =  new List<Territory2>();
        list<String> territoryList                              = new list<String>();
        list<Territory2> countryUnderCluster =  new List<Territory2>();
        list<String> clustername = new list<String>();
        list<Territory2Type> terriType = new list<Territory2Type>();
        list<Account> listOFAccount = new list<Account>();
        list<String> ddddd = new list<string>();
        Map<String,Integer> mapOfZoneTotalLYCM                               = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalL3M                              = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalBudget                               = new map<String,Integer>(); 
        Map<String,Decimal> mapOfZoneTotalValL3m                              = new map<String,Decimal>(); 
        Map<String,Decimal> mapOfZoneTotalValLYCM                               = new map<String,Decimal>(); 
        Map<String,Integer> mapOfClusterTarget                                  = new map<String,Integer>();
        Map<String,id> mapOfExportForcastID                                 = new map<String,id>();
        Map<id,Set<String>> mapOfClusterIDAndCountry                                = new map<id,Set<String>>();
        Map<String,Set<String>> mapOfClusterCodeAndCountryCode                              = new map<String,Set<String>>();
        Map<String,id> mapOfCountrycodeAndClusterID                         = new map<String,id>();
        Map<id,String> mapOfClusterIDAndCode                                = new Map<id,String>();
        list<Sales_Planing_Staging__c> updateStaging = new list<Sales_Planing_Staging__c>();
        Sales_Planning__c exportdea ;
        Sales_Planning__c exportforcast;
        Account dealer;
        String externalIDExport = '';
        set<String> countrySet;
        Integer totalL3M = 0;
        Integer totalLYCM = 0;
        Integer totalBudget = 0;
        Integer totalL3MAllTL       = 0;
        Integer totalL3MAllTL1      = 0;
        Decimal totalValL3m = 0;
        Decimal totalValLYCM = 0;
        set<String> setkeyCatCluster = new set<String>();
        set<id> setOfClusterID = new set<id>();
        set<id> terrTypeID = new set<Id>();
        set<String> contrySetForCluster; 
        set<String> clusterSet = new set<String>();
        set<String> countrySet1 = new set<String>();
        set<String> setOFCustomerNumber = new set<String>();
        for(sObject temp : scope){
            Territory2 sps =(Territory2)temp;
            clusterSet.add(sps.Name);      
            setOfClusterID.add(sps.id);  
            territoryList.addall(clusterSet);
            clustername.add(sps.Description);
        }
        
        listOFcluster = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE DeveloperName IN : clusterSet];
        for(Territory2 clu : listOFcluster){
            mapOfClusterIDAndCode.put(clu.id,clu.DeveloperName);
        }
        
        //querying all the country
        countryUnderCluster = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeIDCountry];
        for(Territory2 terr : countryUnderCluster){
            mapOfCountrycodeAndClusterID.put(terr.DeveloperName,terr.ParentTerritory2Id);
            if(mapOfClusterIDAndCountry.containsKey(terr.ParentTerritory2Id)){
                countrySet = new set<String>();
                countrySet = mapOfClusterIDAndCountry.get(terr.ParentTerritory2Id);
                countrySet.add(terr.DeveloperName);
                mapOfClusterIDAndCountry.put(terr.ParentTerritory2Id,countrySet);
                //mapOfCountrycodeAndClusterID.put(terr.DeveloperName,terr.ParentTerritory2Id);
            }else{
                countrySet = new set<String>();
                countrySet.add(terr.DeveloperName);
                mapOfClusterIDAndCountry.put(terr.ParentTerritory2Id,countrySet);
                
            }
        }
        
        //getting cluster code
        
        for(String temp1 : mapOfClusterIDAndCode.keySet()){
            
            if(mapOfClusterIDAndCountry.containsKey(temp1)){
                String code = mapOfClusterIDAndCode.get(temp1);
                mapOfClusterCodeAndCountryCode.put(code,mapOfClusterIDAndCountry.get(temp1));
                
            }
        }
        
        
        countrySet1 = mapOfClusterCodeAndCountryCode.get(territoryList[0]);
        listOFAccount = [SELECT id,KUNNR__c , Ship_to_Country__c FROM Account WHERE Ship_to_Country__c != '' AND  RecordTypeId =: exportRecTypeId];
        if(listOFAccount.size() > 0){
            for(Account acc : listOFAccount){
                
                String[] country = acc.Ship_to_Country__c.split(';');
                if(country.size() > 0){
                    for(String str : country){
                        if(countrySet1.contains(str)){
                        setOFCustomerNumber.add(acc.KUNNR__c);  
                        }
                    }
                }
               
            }    
        }
        
        //getting Replacement Category from Custom setting
        list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
        for(Export_Sales_Planning_Category__c sp:exportCat){
            if(sp.Include_in_Sales_Planning__c == true){
                catExpSet.add(sp.Category_Code__c);  
            }      
        }
        listOfExportForcast = [SELECT Id,ASP__c,Name,Dealer_CustNumber__c,Value__c,Target_Quantity__c  ,Category__c ,Category_Description__c ,Month__c ,Year__c  ,Budget__c ,Total_planned__c,Dealer_Name__c , SYS_Used_IN_BatchExport__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c,BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c  FROM Sales_Planing_Staging__c WHERE BU__c = 'Export Forcast' AND Year__c =:year  AND Month__c =: month AND Cluster_Code__c IN: clusterSet AND Category__c IN: catExpSet AND Processed__c = false];
        if(listOfExportForcast.size() > 0){
            for(Sales_Planing_Staging__c sp : listOfExportForcast){
                String keyset = sp.Category__c + sp.Cluster_Code__c;
                Integer target = 0;
                if(mapOfClusterTarget.containsKey(keyset)){
                    target = mapOfClusterTarget.get(keyset);
                    if(sp.Target_Quantity__c != null){
                        mapOfClusterTarget.put(keyset,target+Integer.valueOF(sp.Target_Quantity__c));
                    }else{
                        mapOfClusterTarget.put(keyset,target);
                    }
                }else{
                    if(sp.Target_Quantity__c != null){
                        mapOfClusterTarget.put(keyset,Integer.valueOF(sp.Target_Quantity__c));
                    }else{
                        mapOfClusterTarget.put(keyset,0);
                    }
                }
            }   
        }
       
        listOfExportDealer = [SELECT Id,Name,Dealer_CustNumber__c,Target_Quantity__c  ,Category__c ,Category_Description__c ,Month__c ,Year__c  ,Budget__c ,Total_planned__c,Dealer_Name__c , SYS_Used_IN_BatchExport__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c,BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c  FROM Sales_Planing_Staging__c WHERE  BU__c =: exportDealer AND Year__c =:year  AND Month__c =: month AND Dealer_CustNumber__c IN: setOFCustomerNumber  AND Country_Code__c IN : mapOfClusterCodeAndCountryCode.get(territoryList[0]) AND Category__c IN: catExpSet AND Processed__c = false];
        if(listOfExportDealer.size() > 0){
            for(Sales_Planing_Staging__c salesPlan : listOfExportDealer ){
                String keyForTotal = salesPlan.Category__c + territoryList[0];
                Integer l3m = 0 ;
                if(mapOfZoneTotalL3M.containsKey(keyForTotal)){
                    l3m = mapOfZoneTotalL3M.get(keyForTotal);
                    if(salesPlan.L3M__c != null){
                        mapOfZoneTotalL3M.put(keyForTotal,l3m+Integer.valueOF(salesPlan.L3M__c));
                    }else{
                        mapOfZoneTotalL3M.put(keyForTotal,l3m);
                    }
                }else{
                    if(salesPlan.L3M__c != null){
                        mapOfZoneTotalL3M.put(keyForTotal,Integer.valueOF(salesPlan.L3M__c));
                    }else{
                        mapOfZoneTotalL3M.put(keyForTotal,0);
                    }
                }
            }
        }
        if(listOfExportForcast.size() > 0){
            
            for(Sales_Planing_Staging__c spexportForcast : listOfExportForcast){
                if(maoOfUserAndTerr.get(spexportForcast.Cluster_Code__c)!=null){
                    userId = maoOfUserAndTerr.get(spexportForcast.Cluster_Code__c);
                }else{
                    userId = UserInfo.getUserId();
                }
                externalIDExport = spexportForcast.Category__c+spexportForcast.Cluster_Code__c+spexportForcast.Month__c+spexportForcast.Year__c;
                exportforcast = new  Sales_Planning__c(OwnerID = userId,ASP__c= spexportForcast.ASP__c,Target_Quantity__c= spexportForcast.Target_Quantity__c,RecordTypeId= exportForcastClusterId,Category__c = spexportForcast.Category__c,Category_Description__c = spexportForcast.Category_Description__c,Month__c = spexportForcast.Month__c,Year__c = spexportForcast.Year__c ,SPExternalIDTL__c = externalIDExport,BU__c = spexportForcast.BU__c,Value__c = spexportForcast.Value__c,Cluster_Code__c = spexportForcast.Cluster_Code__c,Cluster_Desc__c = spexportForcast.Cluster_Desc__c);
                listOfExportForcastSalesPlanning.add(exportforcast);
                //
                spexportForcast.Processed__c = true;
            }
        }
        update listOfExportForcast;
        if(listOfExportForcastSalesPlanning.size() > 0){
            try{
                database.upsert (listOfExportForcastSalesPlanning ,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
            }catch(Exception e){
                System.debug(e.getMessage());
            }
            //upsert listOfExportForcastSalesPlanning SPExternalIDTL__c;
        }
        if(listOfExportDealer.size() > 0){
            for(Sales_Planing_Staging__c spexportDealer : listOfExportDealer){
                if(maoOfUserAndTerr.get(territoryList[0])!=null){
                    userId = maoOfUserAndTerr.get(territoryList[0]);
                }else{
                    userId = UserInfo.getUserId();
                }
                dealer = new Account (KUNNR__c = spexportDealer.Dealer_CustNumber__c);
                externalIDExport = spexportDealer.Dealer_CustNumber__c+spexportDealer.Category__c+spexportDealer.Cluster_Code__c+spexportDealer.Country_Code__c+spexportDealer.Month__c+spexportDealer.Year__c;
                exportdea = new  Sales_Planning__c(OwnerID = userId,Dealer__r = dealer,RecordTypeId= exportDealerId,Category__c = spexportDealer.Category__c,Category_Description__c = spexportDealer.Category_Description__c,Dealer_CustNumber__c = spexportDealer.Dealer_CustNumber__c,Dealer_Name__c = spexportDealer.Dealer_Name__c,L3M__c = spexportDealer.L3M__c,LYCM__c = spexportDealer.LYCM__c,Month__c = spexportDealer.Month__c,Year__c = spexportDealer.Year__c ,SPExternalIDTL__c = externalIDExport,BU__c = spexportDealer.BU__c,Value_L3M__c = spexportDealer.Value_L3M__c,Value_LYCM__c= spexportDealer.Value_LYCM__c,Cluster_Code__c =spexportDealer.Cluster_Code__c ,Cluster_Desc__c = spexportDealer.Cluster_Desc__c,Country_Code__c = spexportDealer.Country_Code__c,Country_Desc__c =spexportDealer.Country_Desc__c);
                if(exportdea.L3M__c != null && mapOfClusterTarget.containsKey(spexportDealer.Category__c+territoryList[0]) && mapOfZoneTotalL3M.containsKey(spexportDealer.Category__c+territoryList[0]) && mapOfZoneTotalL3M.get(spexportDealer.Category__c+territoryList[0]) > 0){
                    exportdea.Total_planned__c = ((exportdea.L3M__c * mapOfClusterTarget.get(spexportDealer.Category__c+territoryList[0]))/mapOfZoneTotalL3M.get(spexportDealer.Category__c+territoryList[0])).round();
                }else{
                    exportdea.Total_planned__c = 0;
                }
                listOfExportDealersSalesPlanning.add(exportdea);
                spexportDealer.Processed__c = true;
            }
        }
        if(listOfExportForcastSalesPlanning.size() > 0){
            try{
                database.upsert (listOfExportForcastSalesPlanning ,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
            }catch(Exception e){
                System.debug(e.getMessage());
            }
            //upsert listOfExportForcastSalesPlanning SPExternalIDTL__c;
        }
        update listOfExportDealer;
        getExportforcastSalesPlanning = [SELECT Id,Name,Dealer_CustNumber__c,Target_Quantity__c  ,Category__c ,Category_Description__c ,Month__c ,Year__c  ,Budget__c ,Total_planned__c,Dealer_Name__c , SYS_Used_IN_BatchExport__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c,BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c  FROM Sales_Planning__c WHERE  Cluster_Code__c =: territoryList[0] AND RecordTypeId=: exportForcastClusterId AND Year__c =:year  AND Month__c =: month];
        for(Sales_Planning__c exportfor : getExportforcastSalesPlanning){
            mapOfExportForcastID.put(exportfor.SYS_Used_IN_BatchExport__c,exportfor.id);
        }
        for(Sales_Planning__c exportdealer1 : listOfExportDealersSalesPlanning){
            if(mapOfExportForcastID.containsKey(exportdealer1.Category__c+territoryList[0])){
                exportdealer1.Parent_Sales_Planning__c = mapOfExportForcastID.get(exportdealer1.Category__c+territoryList[0]);
            }
        }
        if(listOfExportDealersSalesPlanning.size() > 0){
            try{
                database.upsert (listOfExportDealersSalesPlanning ,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
            }catch(Exception e){
                System.debug(e.getMessage());
            }
            //upsert listOfExportDealersSalesPlanning SPExternalIDTL__c;
        }
        
        
        
    }
    global void finish(Database.BatchableContext BC)
    {       
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForExportBU_Updated Status:' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForExportBU_Updated" processed ' + a.TotalJobItems + '/n'+'  batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n');  
             
               if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }
             
        
    }
}