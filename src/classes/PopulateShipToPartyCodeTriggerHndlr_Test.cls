/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class PopulateShipToPartyCodeTriggerHndlr_Test {
	public static Packing_List__c pc;
	public static Account acc1;
	public static Account acc2;
	public static Contact contact;
	public static Port__c prt1;
	public static Material_Master_Sap__c  mmSap;
	public static Material_Master_Sap__c mmSap2;
	public static Vehicle_Type__c vt;
	public static Inco_Terms__c it;
	public static Document_Currency__c dc;
	public static Loadability__c loadability;
	public static String accid;
	public static void init() {
		acc1 = CEAT_InitializeTestData.createAccForExportsRecType('Acc1', '5000234509','ro','zne','Karnataka');
		acc1.UniqueIdentifier__c = '5000234509';
	    insert acc1;
	    System.debug('==#Acc1 '+acc1);
		contact = CEAT_InitializeTestData.createContact('TestCon', acc1.Id);
		insert contact;
		/*portalUserExports = CEAT_InitializeTestData.createPortalUserExports(contact.Id);
        insert portalUserExports;*/
  		loadability =CEAT_InitializeTestData.createLoadability('100', 'grp', 'MM', '101164');
  		insert loadability;
		pc = CEAT_InitializeTestData.createPacking_List('packingListName','pack');
	    insert pc;
        
        
        acc2 = CEAT_InitializeTestData.createAccForExportsRecType('Acc2', '5000234510','ro1','zn1','TamilNadu');
        acc2.parentId=acc1.id;
        acc2.Active__c=True;
        acc2.UniqueIdentifier__c = '5000234510';
        insert acc2;
        System.debug('==#Acc2 '+acc2);
	    accid= acc2.id;
	    
	    prt1 = CEAT_InitializeTestData.createPort('01', 'USA', 'Flag','Locode','USAPort' ,'1234');
	    insert prt1;
	    
	    mmSap = CEAT_InitializeTestData.createMaterialMasterSap('101164','10', 'MM', 'MatGrp', 'MatGrpDes','DIEN');
	    insert mmSap;

	    mmSap2 = CEAT_InitializeTestData.createMaterialMasterSap('101160', '11', 'MN', 'MatGrp1', 'MatGrpDes1', 'ZFGS');
	    insert mmSap2;

	    vt = CEAT_InitializeTestData.createVehicleType('veh');
	    insert vt;	

	    it = CEAT_InitializeTestData.createIncoTerms('inco');
	    insert it;

	    dc = CEAT_InitializeTestData.createDocumentCurrency('doc');
	    insert dc; 

	}
	
	@isTest static void test_method_one() {
		SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
		init();
		

			Test.startTest();
				System.Test.setMock(WebServiceMock.class, new ShipToPartAddress());         
        		//PopulateShipToPartyCodeTriggerHandler.populateShipToPartyCode();

        		Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
				//proformaInvoice.Customer_Number__c = '5000234509';
				proformaInvoice.Customer__c = acc1.Id ;
				proformaInvoice.Division__c = '07';
				proformaInvoice.Requested_delivery_date__c = System.today();
				proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
				proformaInvoice.Sales_Document_Type__c = '01';
				proformaInvoice.SD_Document_Currency__c = dc.Id;
				//proformaInvoice.Terms_of_Payment__c = tp.Id;
				proformaInvoice.Incoterms__c = it.Id;
				proformaInvoice.Advance_Payment__c = 'No';
				proformaInvoice.Mixing_of_Pro_forma__c = 'No';
				proformaInvoice.Port_of_Destination__c = prt1.Id;
				proformaInvoice.Ship_to_party__c = acc2.Id;
				proformaInvoice.Import_License_Requirement__c = 'No';
				proformaInvoice.Packing_List_Requirement__c = pc.Id;
				proformaInvoice.Vehicle_type__c = vt.Id;
				proformaInvoice.Inspection_Required__c = 'No';
				proformaInvoice.Customized_Container__c = 'No';
				proformaInvoice.Nomination_of_shipping_line__c = 'No';
				proformaInvoice.Clubbing_of_containers__c = 'No';
				proformaInvoice.LC_Required__c = 'No';

				insert proformaInvoice;

				PI_Line_Item__c piLineItem = new PI_Line_Item__c();
				piLineItem.Brand__c = 'CET';
				piLineItem.Quantity__c = 200;
				piLineItem.Material_Number__c = '000000000000101164';
				piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

				insert piLineItem;
        	Test.stopTest();			


		
	}
	

	private class ShipToPartAddress implements WebServiceMock {
		public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

			Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP tom = new Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP();
			List<Sap_zws_sfdc_stp_address.ZSFDC_STP> m 	   	= new List<Sap_zws_sfdc_stp_address.ZSFDC_STP>();
			Sap_zws_sfdc_stp_address.ZSFDC_STP tm 			= new Sap_zws_sfdc_stp_address.ZSFDC_STP();

			tm.KUNNR 	= '0060000089';
			tm.ADDRESS 	= 'False Address';
			m.add(tm);

			tom.item = m;
			Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP elem = new Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP();

			String custNum 		= '005000234509';
			String portName		= 'testPort';

			Sap_zws_sfdc_stp_address.ZSFDC_STP_ADDRESSResponse_element e = new Sap_zws_sfdc_stp_address.ZSFDC_STP_ADDRESSResponse_element();
			e.IT_FINAL = tom;

			Sap_zws_sfdc_stp_address.ZSFDC_STP_ADDRESS_element addElement = new Sap_zws_sfdc_stp_address.ZSFDC_STP_ADDRESS_element();
			addElement.CUSTOMERCODE 	= custNum;
			addElement.IT_FINAL 		= elem;
			addElement.PORT 			= portName;
			response.put('response_x', e);

			return;


		}
	}
	
}