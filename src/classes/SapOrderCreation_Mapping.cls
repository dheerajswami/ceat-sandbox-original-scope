global class SapOrderCreation_Mapping {

    
    /*
     * Author  :- Swayam Arora  
     * Purpose :- Order Creation to SAP 
     * Date    :- 10/July/2015
     *
     */
    
    //-- ATTRIBUTES    
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static String createOrder(List<SapOrderCreationSoap.ZSFDC_SO_H> lh, List<SapOrderCreationSoap.ZSFDC_SO_I> li){
        
        //List<UnrestrictedStockMapping> unrestrictedStock = new List<UnrestrictedStockMapping>();        
        try{
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            SapOrderCreationSoap tst                    = new SapOrderCreationSoap();
            SapOrderCreationSoap.zws_sfdc_sales_order_create zws  = new SapOrderCreationSoap.zws_sfdc_sales_order_create();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
            
            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H toh      = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_H();
            SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I toi      = new SapOrderCreationSoap.TABLE_OF_ZSFDC_SO_I();
            
            
            toh.item = lh;
            toi.item = li;
            
            SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element e = new SapOrderCreationSoap.ZSFDC_SALES_ORDER_CREATEResponse_element();
            
            //system.debug('HEADER ' + toh);
            
            //system.debug('ITEMS ' + toi);
            
            e = zws.ZSFDC_SALES_ORDER_CREATE(toh, toi);
            system.debug('swayam----------------'+e);
            
            return e.RETURN1 +' - '+e.RETURN_x;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }    
}