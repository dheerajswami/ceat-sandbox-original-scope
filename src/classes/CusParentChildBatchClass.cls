global class CusParentChildBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts{
    global String query;
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    global CusParentChildBatchClass(String q){
        this.query = q;
    }
    global CusParentChildBatchClass(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(query == null){
        
            query = 'select Id,KUNNR__c from Account where RecordType.Name = \'OE\' AND Active__c = true ';
        }
        return Database.getQueryLocator(query);
    }
    
     global void execute(Database.BatchableContext BC, List<sObject> scope){
          for(sobject s : scope){
            Account acc = (Account)s;
            String customerId = UtilityClass.addleadingZeros(acc.KUNNR__c,Integer.valueOf(CUST_ID_LENGTH));    
            List<Sap_Sfdc_Customer_Parent_Child_Mapping.Sfdc_Customer_Parent_Child_Mapping> listOfChd = Sap_Sfdc_Customer_Parent_Child_Mapping.getAllCustomerParentChild(customerId);
            system.debug(listOfChd);
            
            Id prntId = [Select Id from Account where Id=: s.Id limit 1].Id;
            system.debug('success---1');
                for(Sap_Sfdc_Customer_Parent_Child_Mapping.Sfdc_Customer_Parent_Child_Mapping chd : listOfChd){
                 if(chd.CHILD_CODE != null){
                 Integer i = integer.ValueOf(chd.CHILD_CODE);
                 system.debug('0000'+i);
                 String chdcode = string.ValueOf(i);
                 system.debug('9999'+chdcode);
                 Account acc1 = new Account();
                 acc1 = [Select id from Account where KUNNR__C =: chdcode];
                 acc1.parentId = prntId;
                 update acc1;
                 system.debug('success===2'+acc1.ParentId);
                 }
                }
                
              }
            } 
          
            global void finish(Database.BatchableContext BC){
    
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
             mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: CusParentChildBatchClass Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "CusParentChildBatchClass" processed ' + a.TotalJobItems + '</br> batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
    }  
}