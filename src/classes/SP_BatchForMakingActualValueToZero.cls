Global with Sharing Class SP_BatchForMakingActualValueToZero Implements Database.Batchable<sObject>
 /*
  Author : PALLAVI S
  purpose : Batch Class For Making Actual Sal val and Actual Quantity To Zero Of All The Sales Planning Records
 */
{
  String query;
  public  String month_val;
  public  String year_val;
  
  /* Start method that will fetch all records or object's and pass it into execute method (batch size 1) */  
      global Database.QueryLocator start(Database.BatchableContext BC){
         
          if(month_val == NULL && year_val == NULL)
          {
            month_val = String.valueOf(Date.Today().Month());
            year_val =  String.valueOf(Date.today().year());
          }
          query = 'SELECT ID,Actual_Quantity__c,Actual_Sales_Value__c,Month__c, Year__c FROM Sales_Planning__c WHERE (Actual_Sales_Value__c>0 OR Actual_Sales_Value__c<0 OR Actual_Quantity__c>0 OR Actual_Quantity__c<0)';
          query+= ' AND Month__c = :month_val';
          query+= ' AND Year__c =: year_val';    
          system.debug('values'+query);
          Return Database.getQueryLocator(query);
      }
      
  /*Execute method is called for each batch of record's passed to the method */    
     global void execute(Database.BatchableContext BC,List<Sales_Planning__c> salesPlanningList)
     {
        System.debug('^^^^^^^'+salesPlanningList);
        List<Sales_Planning__c> listOfSalesPlanningobj = new List<Sales_Planning__c>();
       
         for(Sales_Planning__c salesPlanning : salesPlanningList)
         {
           if(salesPlanning.Actual_Sales_Value__c>0 || salesPlanning.Actual_Quantity__c>0)
             
           {
               system.debug('actualVal'+salesPlanning);
               salesPlanning.Actual_Sales_Value__c = 0;
               salesPlanning.Actual_Quantity__c = 0;
               listOfSalesPlanningobj.add(salesPlanning);
               system.debug('ListSales'+listOfSalesPlanningobj);
            
            }
          }
          if(listOfSalesPlanningobj.size()>0){
           update listOfSalesPlanningobj;
           system.debug('updateList'+listOfSalesPlanningobj);
          } 
      }
   /*Finish method is used to send confirmation emails */      
       global void finish( Database.BatchableContext BC){
       }
   }