global class CPORT_NBPPriceListMapping{

    /*
   * Auther      :- Sneha Agrawal
   * Purpose     :- Fetch NBP Price details
   *                from SAP 
   * Modified By :- 
   * Date        :- 18/6/2015
   *
   */
   
   //-- ATTRIBUTES
    
    public static final Integer MATNR_LENGTH 	= 18;
    public static final Integer MATKL_LENGTH 	= 9;
    
    WebService static List<NbpPriceMapping> getAllNBPPriceDetails(String matkl,String matnr){
    	 List<NbpPriceMapping> nbpList = new List<NbpPriceMapping>();
         //matnr - 18             matkl -  9
        try{
        	String matnrWithZero						    = UtilityClass.addleadingZeros(matnr,Integer.valueOf(MATNR_LENGTH));
        	String matklWithZero						    = UtilityClass.addleadingZeros(matkl,Integer.valueOf(MATKL_LENGTH));
        	
        	//if(matnrWithZero == null || matnrWithZero =='000000000000000000'){
        		//matnrWithZero = '';
        	//}
        	if(matklWithZero == null){
        		matklWithZero = '';
        	}
        	SAPLogin__c saplogin 							= SAPLogin__c.getValues('SAP Login');
        	
        	String username                 				= saplogin.username__c;
            String password 								= saplogin.password__c;
            Blob headerValue 								= Blob.valueOf(username + ':' + password);
            String authorizationHeader 						= 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            
            sap_zws_sfdc_nbp  tst           = new sap_zws_sfdc_nbp ();
            sap_zws_sfdc_nbp.zws_sfdc_nbp  zws   = new sap_zws_sfdc_nbp.zws_sfdc_nbp ();
            
            zws.inputHttpHeaders_x               = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                   = 60000;
            sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP tom     = new sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP();            
            
            List<sap_zws_sfdc_nbp.ZSFDC_NBP>   m       = new  List<sap_zws_sfdc_nbp.ZSFDC_NBP> ();       
            
            tom.item = m;            
            sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP  e = new sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP ();
             
            if(matnrWithZero =='000000000000000000'){
            	 system.debug('----if------matnrWithZero-------'+matnrWithZero);
                  system.debug('--if--matklWithZero-------------'+matklWithZero);
        		 e = zws.ZSFDC_NBP(tom,matkl,'');
        	}else{
        		 system.debug('----else------matnrWithZero-------'+matnrWithZero);
                  system.debug('-else-matklWithZero-------------'+matklWithZero);
        		 e = zws.ZSFDC_NBP(tom,matklWithZero,matnrWithZero);
        	}
           
            //e = zws.ZSFDC_NBP(tom,'2010','101933'); //String MATKL,String MATNR
            system.debug('-----------------'+e.item);
			 if(e.item != null){            
                for(sap_zws_sfdc_nbp.ZSFDC_NBP z : e.item) {
                    nbpList.add(new NbpPriceMapping(z.matnr,z.matnr_desc, z.tyre_price, z.tube, z.tube_desc, z.tube_price, z.flap, z.flap_desc, z.set_price, z.pair_price, z.matkl, z.matkl_desc,z.FLAP_PRICE));
                }
            }
            return nbpList;
        	
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
   //-- WRAPPER CLASS
    
    global class NbpPriceMapping{    	
        
        public String matnr          {get;set;}
        public String matnr_desc     {get;set;}
        public String tyre_price     {get;set;}
        public String tube 			 {get;set;}
        public String tube_desc  	 {get;set;}
        public String tube_price     {get;set;}
        public String flap     		 {get;set;}
        public String flap_desc      {get;set;}
        public String flap_price     {get;set;}
        public String set_price      {get;set;}
        public String pair_price     {get;set;}
        public String matkl 		 {get;set;}
        public String matkl_desc     {get;set;}          

        public NbpPriceMapping(String matnr,String matnr_desc, String tyre_price, String tube, String tube_desc, string tube_price,String flap, String flap_desc, String set_price, String pair_price, String matkl, String matkl_desc,String flap_price){
            this.matnr 			= matnr;
            this.matnr_desc 	= matnr_desc;
            this.tyre_price 	= tyre_price;
            this.tube 			= tube;
            this.tube_desc 		= tube_desc;
            this.tube_price 	= tube_price;
            this.flap 			= flap;
            this.flap_desc 		= flap_desc;
            this.set_price 		= set_price;
            this.pair_price 	= pair_price;
            this.matkl 			= matkl;
            this.matkl_desc 	= matkl_desc;
            this.flap_price 	= flap_price;
           
        }
    }
    
}