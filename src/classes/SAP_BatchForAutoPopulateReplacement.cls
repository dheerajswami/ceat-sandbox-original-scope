global class SAP_BatchForAutoPopulateReplacement implements Database.Batchable<sObject> {    
    /* ================================================
    @Name:  SAP_BatchForCustomerSDS
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will update SDS of Customer on daily basis                                                                                            
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     11/05/2015      INITIAL DEVELOPMENT                                 
   
    @=======================================================  */
    
    //*********************SETS*******************************
    Set<String> territorySet                                = new Set<String>();
    set<id> terrTypeID                                      = new set<Id>();
    set<id> terrTypeRegID                                      = new set<Id>();
    
    set<String> regionSet                                   = new set<String>();
    Set<String> newTLSet                                    = new set<String>();
    Set<String> setOFTerritoryCode                          = new Set<String>();
    set<String> setOFkey                                    = new set<String>();
    //*********************LISTS*******************************
    list<String> territoryList                              = new list<String>(); 
    list<String> regionList                                 = new list<String>();
    list<Account> listofCustomer                              = new list<Account>();
    list<Account> updatelistofCustomer                    = new list<Account>();
    list<Territory2Type> terriType                          = new list<Territory2Type>();
    list<Territory2Type> terriTypeReg                          = new list<Territory2Type>();
    
    List<Territory2> listOfTerr                         = new List<Territory2>();
    list<Sales_Planning__c> salesPlanningDealer             = new list<Sales_Planning__c>();
    list<Sales_Planning__c> salesPlanningDealer1            = new list<Sales_Planning__c>();
    list<Sales_Planning__c> salesPlanningTLList             = new list<Sales_Planning__c>();
    list<Sales_Planning__c> listOfSalesPlanningDealer       = new list<Sales_Planning__c>();
    List<Sales_Planning__c> salesPlanningRO                 = new List<Sales_Planning__c>();
    list<Sales_Planning__c> insertSalesPlanningTL           = new list<Sales_Planning__c>();
    list<Sales_Planning__c> updateSalesPlanningDealer       = new list<Sales_Planning__c>();
    list<Sales_Planning__c> updateSalesPlanningTLGap0       = new list<Sales_Planning__c>();
    list<Sales_Planning__c> listOfSalesPlanningTLgap0       = new list<Sales_Planning__c>();
    list<Sales_Planning__c> listtoUpdateSPTLgap0        = new list<Sales_Planning__c>();
    list<Sales_Planning__c> salesPlan12;
    list<Sales_Planning__c> listOfSalesPlanningDealer1;
    list<Sales_Planning__c> listOfSalesPlanningTLgap01;
    List<Sales_Planning__c> salesPlanDealerToUpdate;
    list<Sales_Planning__c> tempList;
    list<Sales_Planning__c> gaplistPositive ;
    //*********************MAPS********************************
    Map<String,Integer> mapOfZoneTarget                                 = new map<String,Integer>();
    Map<String,Integer> mapOfExistingTLTarget                                   = new map<String,Integer>();
    Map<String,id> mapOfZoneID                                          = new map<String,id>();
    Map<String,Integer> mapOfZoneTotalLYCM                               = new map<String,Integer>(); 
    Map<String,Integer> mapOfZoneTotalL3M1                              = new map<String,Integer>(); 
    Map<String,Integer> mapOfZoneTotalBudget                               = new map<String,Integer>(); 
    Map<String,Decimal> mapOfZoneTotalValL3m                              = new map<String,Decimal>(); 
    Map<String,Decimal> mapOfZoneTotalValLYCM                               = new map<String,Decimal>(); 
    Map<String,List<Sales_Planning__c>> mapOfTerrSales          = new map<String,List<Sales_Planning__c>>();
    Map<String,List<Sales_Planning__c>> mapOfROTerr          = new map<String,List<Sales_Planning__c>>();
    map<String,Integer> mapOfrunningTotal                       = new map<String,integer>();
    Map<String,Integer> mapOfTLL3MTotal                         = new map<String,Integer>();
    Map<String,Sales_Planning__c> mapOfZoneForcast = new Map<String,Sales_Planning__c>();
    Map<String,id> mapOfTerritoryAndUserID                      = new Map<String,ID>();
    Map<String,String> mapOfTerrAndTL                           = new map<String,String>();
    Map<String,ID> mapOfCatAndROId                              = new map<String,ID>();
    Map<String,Sales_Planning__c> mapOfTLAndId                                 = new map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOfTLSP;
        
    Id replaceDealerId ;
    Id replaceTLId;
    Id replaceROId;
    String repDealerLabel                                   = System.Label.Replacement_Dealer;
    String repTLLabel                                       = System.Label.Replacement_TL; 
    String repROLabel                                       = System.Label.Replacement_RO;
    String tlLabel                  = System.Label.TL_Role;
    String currentMonth;
    String currentDay;
    String currentYear;
    String ss ='';
     
    Integer totalL3M = 0;
    Integer totalL3MFinal = 0;
    Integer totalLYCM = 0;
    Integer totalBudget = 0;
    Integer totalL3MAllTL       = 0;
    Integer totalL3MAllTL1      = 0;
    Decimal totalValL3m = 0;
    Decimal totalValLYCM = 0;
    Integer runningTLPlannTotal = 0;
    Sales_Planning__c salesPlanningTL;
    Sales_Planning__c salesPlanningTL1;
    global SAP_BatchForAutoPopulateReplacement (){
        //getting territory type of Territory
        currentDay                  = String.valueOf((Date.today()).day());
        currentMonth                = String.valueOf((Date.today()).month());
        currentYear                 = String.valueOf((Date.today()).year());
        replaceDealerId             = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        replaceTLId                 = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
        replaceROId                 = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        terriType                   = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        terriTypeReg                = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'RO'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        for(Territory2Type temp : terriTypeReg){
            terrTypeRegID.add(temp.id);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         String query;
         String SPRP                          = 'SPRP';
         String B0001                         = 'B0001';
         
         String[] filters = new String[]{'SPRP%'};
         query                    = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,ParentTerritory2.DeveloperName,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeRegID AND (NOT DeveloperName LIKE :  filters'+')'; 
         //query                    = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId,ParentTerritory2.DeveloperName from Territory2 WHERE (Name =  \'B0001\' OR Name =  \'B0003\' OR Name =  \'B0021\' OR Name =  \'B0024\' OR Name =  \'DI004\' OR Name =  \'DI041\' ) AND Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters'+')'; 
         //query                                = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,ParentTerritory2.DeveloperName,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters'+')'; 
         return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC,List<sObject> scope){
        for(sObject temp : scope){
            Territory2 sps                                =(Territory2)temp;
            territorySet.add(sps.Name);
            regionSet.add(sps.ParentTerritory2.DeveloperName);
            regionList.addall(regionSet);
            territoryList.addall(territorySet);
        }
         listOfTerr = [select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId,(Select Territory2Id,UserID,User.name from UserTerritory2Associations Where RoleInTerritory2=: tlLabel ) from Territory2];
       
           // system.debug(listOfTerr+'listOfTerr');
            if(listOfTerr.size() > 0){
                for(Territory2 temp : listOfTerr){
                            setOFTerritoryCode.add(temp.name);
                            for(UserTerritory2Association s : temp.UserTerritory2Associations){
                                mapOfTerritoryAndUserID.put(temp.Name,s.UserID);
                                //setOfTL.add(s.User.name);
                                mapOfTerrAndTL.put(s.User.name,temp.Name);
                            }
                        
                    }
                    
                
            }
          salesPlanningRO = [SELECT Id,ASP__c,Budget__c,Dealer__c,SYS_TL_CAT_REG__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionSet AND Year__c =:currentYear AND Month__c =:currentMonth AND RecordTypeId =: replaceROId ];
          for(Sales_Planning__c sp : salesPlanningRO){
                setOFkey.add(sp.SYS_TL_CAT_REG__c);
                mapOfZoneTarget.put(sp.SYS_TL_CAT_REG__c,Integer.valueOF(sp.Target_Quantity__c));
                mapOfZoneID.put(sp.SYS_TL_CAT_REG__c,sp.id);
                mapOfCatAndROId.put(sp.SYS_TL_CAT_REG__c,sp.id);
          }
          salesPlanningTLList = [SELECT Id,ASP__c,Total_L3M__c,SPExternalIDTL__c,Budget__c,Total_planned__c,Dealer__c,SYS_TL_CAT_REG__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionSet AND Year__c =:currentYear AND Month__c =:currentMonth AND RecordTypeId =: replaceTLId AND Territory_Code__c IN: territorySet];
          if(salesPlanningTLList.size() > 0){
                for(Sales_Planning__c sp : salesPlanningTLList){
                    mapOfExistingTLTarget.put(sp.SPExternalIDTL__c,Integer.valueOF(sp.Total_planned__c));
                    mapOfTLAndId.put(sp.SPExternalIDTL__c,sp);
                }
          }
          salesPlanningDealer1 = [SELECT id,name,Parent_Sales_Planning__c,Customer_group__c,SYS_TL_CAT_REG__c,Value__c,SYS_Used_IN_Batch2__c,Dealer__c,SYS_TL_CAT__c,Dealer_CustNumber__c,Dealer_Name__c,Category__c,Category_Description__c,Month__c ,Year__c,Region_code__c ,Region_Description__c,RecordTypeId,Territory_Code__c,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Budget__c,Value_L3M__c,Value_LYCM__c,Zone__c FROM Sales_Planning__c WHERE Region_code__c IN: regionSet AND Territory_Code__c IN: territorySet AND Year__c =: currentYear AND Month__c =: currentMonth AND RecordTypeId =: replaceDealerId AND Total_planned__c = null];
          for (Sales_Planning__c salesPlan:salesPlanningDealer1) {
                if(mapOfTerrSales.containsKey(salesPlan.SYS_TL_CAT__c)){
                    listOfSalesPlanningDealer = mapOfTerrSales.get(salesPlan.SYS_TL_CAT__c);
                    listOfSalesPlanningDealer.add(salesPlan);
                    mapOfTerrSales.put(salesPlan.SYS_TL_CAT__c,listOfSalesPlanningDealer);
                }else{
                    listOfSalesPlanningDealer1 = new list<Sales_Planning__c>();
                    listOfSalesPlanningDealer1.add(salesPlan);
                    mapOfTerrSales.put(salesPlan.SYS_TL_CAT__c,listOfSalesPlanningDealer1);
                }
          }
          salesPlanningDealer = [SELECT id,name,Parent_Sales_Planning__c,Customer_group__c,SYS_TL_CAT_REG__c,Value__c,SYS_Used_IN_Batch2__c,Dealer__c,SYS_TL_CAT__c,Dealer_CustNumber__c,Dealer_Name__c,Category__c,Category_Description__c,Month__c ,Year__c,Region_code__c ,Region_Description__c,RecordTypeId,Territory_Code__c,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Budget__c,Value_L3M__c,Value_LYCM__c,Zone__c FROM Sales_Planning__c WHERE Region_code__c IN: regionSet AND Year__c =: currentYear AND Month__c =: currentMonth AND RecordTypeId =: replaceDealerId AND Total_planned__c = null];
          for (Sales_Planning__c salesPlan:salesPlanningDealer) {
            if(mapOfTLL3MTotal.containsKey(salesPlan.SYS_TL_CAT_REG__c)){
                    totalL3MFinal = 0;
                    totalL3MFinal = mapOfTLL3MTotal.get(salesPlan.SYS_TL_CAT_REG__c);
                    if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                    totalL3MFinal = totalL3MFinal +Integer.valueOf(salesPlan.L3M__c);
                    mapOfTLL3MTotal.put(salesPlan.SYS_TL_CAT_REG__c,totalL3MFinal);
                    
            }else{
                    if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                    mapOfTLL3MTotal.put(salesPlan.SYS_TL_CAT_REG__c,Integer.ValueOF(salesPlan.L3M__c));
            }
            if(mapOfZoneTotalL3M1.containsKey(salesPlan.SYS_TL_CAT__c)){
                totalL3M = 0;
                totalL3M = mapOfZoneTotalL3M1.get(salesPlan.SYS_TL_CAT__c);
                if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                totalL3M = totalL3M +Integer.valueOf(salesPlan.L3M__c);
                mapOfZoneTotalL3M1.put(salesPlan.SYS_TL_CAT__c,totalL3M);
                if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                    ss = ss+salesPlan.Name+'<------L3m---->'+mapOfZoneTotalL3M1.get('B00212050');
                } 
            }else{
                if(salesPlan.L3M__c == null) salesPlan.L3M__c = 0;
                mapOfZoneTotalL3M1.put(salesPlan.SYS_TL_CAT__c,Integer.ValueOF(salesPlan.L3M__c));
            }
            if(mapOfZoneTotalLYCM.containsKey(salesPlan.SYS_TL_CAT__c)){
                totalLYCM = 0;
                totalLYCM = mapOfZoneTotalLYCM.get(salesPlan.SYS_TL_CAT__c);
                system.debug(totalLYCM+'totalLYCM');
                if(salesPlan.LYCM__c == null) salesPlan.LYCM__c = 0;
                    totalLYCM = totalLYCM +Integer.valueOf(salesPlan.LYCM__c);
                    system.debug(totalLYCM+'totalLYCMnull');
                
                mapOfZoneTotalLYCM.put(salesPlan.SYS_TL_CAT__c,totalLYCM);
                if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                    ss = ss+'<------LYCM------>'+mapOfZoneTotalLYCM.get('B00212050');
                }
                system.debug(mapOfZoneTotalLYCM+'mapOfZoneTotalLYCM');
            }else{
                if(salesPlan.LYCM__c == null) salesPlan.LYCM__c = 0;
                mapOfZoneTotalLYCM.put(salesPlan.SYS_TL_CAT__c,Integer.ValueOF(salesPlan.LYCM__c));
            }
            if(mapOfZoneTotalBudget.containsKey(salesPlan.SYS_TL_CAT__c)){
                totalBudget = 0;
                totalBudget = mapOfZoneTotalBudget.get(salesPlan.SYS_TL_CAT__c);
                if(salesPlan.Budget__c == null) salesPlan.Budget__c = 0;
                    totalBudget = totalBudget +Integer.valueOf(salesPlan.Budget__c);
                
                mapOfZoneTotalBudget.put(salesPlan.SYS_TL_CAT__c,totalBudget);
                if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                    ss = ss+'<------Budget------>'+mapOfZoneTotalBudget.get('B00212050');
                }
            }else{
                if(salesPlan.Budget__c == null) salesPlan.Budget__c = 0;
                mapOfZoneTotalBudget.put(salesPlan.SYS_TL_CAT__c,Integer.ValueOF(salesPlan.Budget__c));
            }
            if(mapOfZoneTotalValL3m.containsKey(salesPlan.SYS_TL_CAT__c)){
                totalValL3m = 0.0;
                totalValL3m = mapOfZoneTotalValL3m.get(salesPlan.SYS_TL_CAT__c);
                if(salesPlan.Value_L3M__c == null) salesPlan.Value_L3M__c =0;
                    totalValL3m = totalValL3m +salesPlan.Value_L3M__c;
                
                mapOfZoneTotalValL3m.put(salesPlan.SYS_TL_CAT__c,totalValL3m);
                if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                    ss = ss+'<------ValL3m------>'+mapOfZoneTotalValL3m.get('B00212050');
                }
            }else{
                if(salesPlan.Value_L3M__c == null) salesPlan.Value_L3M__c =0;
                mapOfZoneTotalValL3m.put(salesPlan.SYS_TL_CAT__c,salesPlan.Value_L3M__c);
            }
            if(mapOfZoneTotalValLYCM.containsKey(salesPlan.SYS_TL_CAT__c)){
                totalValLYCM = 0.0;
                totalValLYCM = mapOfZoneTotalValLYCM.get(salesPlan.SYS_TL_CAT__c);
                if(salesPlan.Value_LYCM__c == null) salesPlan.Value_LYCM__c = 0;
                    totalValLYCM = totalValLYCM +salesPlan.Value_LYCM__c;
                
                mapOfZoneTotalValLYCM.put(salesPlan.SYS_TL_CAT__c,totalValLYCM);
                if(salesPlan.SYS_TL_CAT__c == 'B00212050'){
                    ss = ss+'<------ValLYCM------>'+mapOfZoneTotalValLYCM.get('B00212050');
                }
            }else{
                if(salesPlan.Value_LYCM__c == null) salesPlan.Value_LYCM__c = 0;
                mapOfZoneTotalValLYCM.put(salesPlan.SYS_TL_CAT__c,salesPlan.Value_LYCM__c);
            }
            
          }
          system.debug(mapOfTLL3MTotal+'mapOfTLL3MTotal');
          system.debug(mapOfZoneTotalL3M1+'mapOfZoneTotalL3M1');
          system.debug(mapOfZoneTotalLYCM+'mapOfZoneTotalLYCM');
          system.debug(mapOfZoneTotalValLYCM+'mapOfZoneTotalValLYCM');
          system.debug(mapOfZoneTarget+'mapOfZoneTarget');
          system.debug(mapOfTerrSales+'mapOfTerrSales');
           if(mapOfTerrSales.size() > 0){
                mapOfTLSP = new map<String,Sales_Planning__c>(); 
                    for (String key : mapOfTerrSales.keySet()) {
                        if(!(mapOfExistingTLTarget.containsKey(key+currentmonth+currentyear))){
                            salesPlan12 = new list<Sales_Planning__c>();
                            salesPlan12 = mapOfTerrSales.get(key);
                            Boolean flag = true;
                            for(Sales_Planning__c temp: salesPlan12){
                                    String st = '';
                                    id spROID;
                                    id usID;
                                    if(flag){
                                            st = temp.Territory_Code__c + temp.Category__c + temp.Month__c+temp.Year__c;
                                            newTLSet.add(st);
                                            spROID = mapOfCatAndROId.get(temp.SYS_TL_CAT_REG__c);
                                            system.debug(mapOfCatAndROId.get(temp.SYS_TL_CAT_REG__c)+'@@@');
                                            system.debug(mapOfTerritoryAndUserID.get(temp.Territory_Code__c)+'%%%%');
                                            if(mapOfTerritoryAndUserID.get(temp.Territory_Code__c) == null){
                                                usID = UserInfo.getUserId();
                                            }else{
                                                usID = mapOfTerritoryAndUserID.get(temp.Territory_Code__c);
                                            }
                                            salesPlanningTL = new Sales_Planning__c(OwnerId = usID,Parent_Sales_Planning__c = spROID,Category__c = temp.Category__c,Category_Description__c = temp.Category_Description__c,
                                                                                    Month__c = temp.Month__c,Year__c = temp.Year__c,Region_code__c = temp.Region_code__c,Region_Description__c = temp.Region_Description__c,
                                                                                    RecordTypeId = replaceTLId, Territory_Code__c = temp.Territory_Code__c,SPExternalIDTL__c = st);
                                    }
                                    flag = false;
                                }
                            
                                if(mapOfZoneTotalL3M1.containsKey(key)){
                                     salesPlanningTL.Total_L3M__c = mapOfZoneTotalL3M1.get(key);   
                                }else{
                                    salesPlanningTL.Total_L3M__c = 0;
                                }
                                if(mapOfZoneTotalLYCM.containsKey(Key)){
                                    salesPlanningTL.Total_LYCM__c= mapOfZoneTotalLYCM.get(key);
                                }else{
                                    salesPlanningTL.Total_LYCM__c = 0;
                                }
                                if(mapOfZoneTotalBudget.containskey(key)){
                                    salesPlanningTL.Total_Budget__c= mapOfZoneTotalBudget.get(key);
                                }else{
                                    salesPlanningTL.Total_Budget__c= 0;
                                }
                                if(mapOfZoneTotalValLYCM.containsKey(key)){
                                    salesPlanningTL.Value_LYCM__c= mapOfZoneTotalValLYCM.get(key);    
                                }else{
                                    salesPlanningTL.Value_LYCM__c= 0;
                                }
                                if(mapOfZoneTotalValL3m.containsKey(key)){
                                     salesPlanningTL.Value_L3M__c= mapOfZoneTotalValL3m.get(key);   
                                }else{
                                    salesPlanningTL.Value_L3M__c= 0;
                                }
                                system.debug(key.substring(5,9)+regionList[0]+'VVVVVVV');
                                system.debug(mapOfZoneTarget.containsKey(key.substring(5, 8)+regionList[0])+'PPPP');
                                system.debug(mapOfTLL3MTotal.get(key.substring(5, 8)+regionList[0])+'RRRRR');
                                system.debug(salesPlanningTL.Total_L3M__c +'MMMMM');
                                if(salesPlanningTL.Total_L3M__c != null && mapOfZoneTarget.containsKey(key.substring(5, 9)+regionList[0]) && mapOfTLL3MTotal.containsKey(key.substring(5, 9)+regionList[0]) && mapOfTLL3MTotal.get(key.substring(5, 9)+regionList[0])>0){
                                    salesPlanningTL.Total_planned__c = ((salesPlanningTL.Total_L3M__c * mapOfZoneTarget.get(key.substring(5, 9)+regionList[0]))/mapOfTLL3MTotal.get(key.substring(5, 9)+regionList[0])).round();
                                }else{
                                    salesPlanningTL.Total_planned__c = 0;
                                }
                                mapOfTLSP.put(key,salesPlanningTL);
                                mapOfExistingTLTarget.put(salesPlanningTL.SPExternalIDTL__c,Integer.valueOF(salesPlanningTL.Total_planned__c));
                                mapOfTLAndId.put(salesPlanningTL.SPExternalIDTL__c,salesPlanningTL);
                        }
                    }
           }
           if(mapOfTLSP.size() > 0){
                for(String key : mapOfTLSP.keySet()){
                        salesPlanningTL1 = new Sales_Planning__c();
                        salesPlanningTL1 = mapOfTLSP.get(key);
                        insertSalesPlanningTL.add(salesPlanningTL1);
                }    
            }
            system.debug(insertSalesPlanningTL+'insertSalesPlanningTL');
            if(insertSalesPlanningTL.size() > 0){
                database.upsert (insertSalesPlanningTL , Sales_Planning__c.Fields.SPExternalIDTL__c,false);   
                updateSalesPlanningTLGap0 = [SELECT Id,ASP__c,Total_L3M__c,SPExternalIDTL__c,Budget__c,Total_planned__c,Dealer__c,SYS_TL_CAT_REG__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionSet AND Year__c =:currentYear AND Month__c =:currentMonth AND RecordTypeId =: replaceTLId AND SYS_TL_CAT_REG__c IN : setOFkey];
                if(updateSalesPlanningTLGap0.size() > 0){
                    for (Sales_Planning__c salesPlan:updateSalesPlanningTLGap0) {
                        if(mapOfROTerr.containsKey(salesPlan.SYS_TL_CAT_REG__c)){
                            listOfSalesPlanningTLgap0 = mapOfROTerr.get(salesPlan.SYS_TL_CAT_REG__c);
                            listOfSalesPlanningTLgap0.add(salesPlan);
                            mapOfROTerr.put(salesPlan.SYS_TL_CAT_REG__c,listOfSalesPlanningTLgap0);
                        }else{
                            listOfSalesPlanningTLgap01 = new list<Sales_Planning__c>();
                            listOfSalesPlanningTLgap01.add(salesPlan);
                            mapOfROTerr.put(salesPlan.SYS_TL_CAT_REG__c,listOfSalesPlanningTLgap01);
                        }
                    }
                }    
            }
            if(mapOfROTerr.size() > 0){
                for(String temp : mapOfROTerr.keyset()){
                    runningTLPlannTotal = 0;
                    tempList =  new list<Sales_Planning__c>();
                    tempList = mapOfROTerr.get(temp);
                    if(tempList.size() > 0){
                        for(Sales_Planning__c sales : tempList){
                            runningTLPlannTotal += Integer.valueOf(sales.Total_planned__c);
                            mapOfrunningTotal.put(temp,runningTLPlannTotal);
                        }
                    }
                }
            }
            if(mapOfrunningTotal.size() > 0){
                for(String ke : mapOfrunningTotal.keyset()){
                    if(mapOfZoneTarget.containsKey(ke)){
                        integer gap = 0;
                        gap = mapOfZoneTarget.get(ke)- mapOfrunningTotal.get(ke);
                        if(gap != 0){
                            if(gap > 0){
                                gaplistPositive = new list<Sales_Planning__c>();
                                gaplistPositive = mapOfROTerr.get(ke);
                                if(gaplistPositive.size() > 0){
                                    //for(Sales_Planning__c sales : gaplistPositive){
                                        gaplistPositive[0].Total_planned__c += gap;
                                        listtoUpdateSPTLgap0.add(gaplistPositive[0]);
                                    //}
                                }
                            }else{
                                gaplistPositive = new list<Sales_Planning__c>();
                                gaplistPositive = mapOfROTerr.get(ke);
                                if(gaplistPositive.size() > 0){
                                    boolean isgap0 = false ; 
                                    for(Sales_Planning__c sales : gaplistPositive){
                                        if(isgap0 == false){
                                            if(gap  <= sales.Total_planned__c){
                                                sales.Total_planned__c = sales.Total_planned__c - gap;
                                                isgap0 = true;
                                                listtoUpdateSPTLgap0.add(sales);
                                            }
                                        }           
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(listtoUpdateSPTLgap0.size() > 0){
                //database.upsert (listtoUpdateSPTLgap0 , Sales_Planning__c.Fields.SPExternalIDTL__c,false); 
            }
           if(mapOfTLAndId.size() > 0){
                for(String te : mapOfTLAndId.keySet()){
                 salesPlanDealerToUpdate = new list<Sales_Planning__c>();
                 if(mapOfTerrSales.containsKey(mapOfTLAndId.get(te).SYS_TL_CAT__c)){
                            salesPlanDealerToUpdate = mapOfTerrSales.get(mapOfTLAndId.get(te).SYS_TL_CAT__c);
                            for(Sales_Planning__c temp : salesPlanDealerToUpdate){
                                temp.Parent_Sales_Planning__c = mapOfTLAndId.get(te).id;
                                if(temp.L3M__c != null && mapOfTLAndId.get(te).Total_L3M__c > 0 && mapOfTLAndId.get(te).Total_planned__c != null){
                                temp.Total_planned__c = ((temp.L3M__c * mapOfTLAndId.get(te).Total_planned__c)/mapOfTLAndId.get(te).Total_L3M__c).round();
                                }else if(temp.L3M__c == null){
                                    temp.Total_planned__c = 0;
                                }
                                updateSalesPlanningDealer.add(temp);
                            }
                        }
            }
           }
           if(updateSalesPlanningDealer.size() > 0){
                database.upsert (updateSalesPlanningDealer, Sales_Planning__c.Fields.SPExternalIDTL__c,false);
           }
           
     }
      global void finish(Database.BatchableContext BC)
    {       
            
            
    }
}