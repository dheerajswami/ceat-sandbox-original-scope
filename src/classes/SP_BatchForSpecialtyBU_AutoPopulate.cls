global class SP_BatchForSpecialtyBU_AutoPopulate implements Database.Batchable<sObject> { 
        String recTypeStagingLabel      = System.Label.Staging;
        String special                  = System.Label.Specialty;
        String replacement              = System.Label.Replacement;
        String Forcast                  = System.Label.Forcast; 
        String recTypeSpeZoneLabel    = System.Label.Specialty_Forcast_Zone;
        Static String SPECIALTY='Specialty';
        Static String REPLECMENT='Replacement';
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
        String catZoneSpe = '';  
        String SpecialtyDealerLabel     = System.Label.Specialty;
        String Specialty_Forcast_Zone       = System.Label.Specialty_Forcast_Zone;
        List<Sales_Planning__c> salesPlanningSpeForcastZone             = new List<Sales_Planning__c>();
        List<Sales_Planning__c> salesPlanningSpeDealerUpdate            = new List<Sales_Planning__c>();
        Integer totalL3M = 0;
        Integer totalL3M1 = 0;
        Map<String,Integer> mapOfZoneTarget                                 = new map<String,Integer>();
        Map<String,id> mapOfZoneID                                          = new map<String,id>();
        Map<String,Integer> mapOfZoneTotalL3M                               = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneTotalL3M1                              = new map<String,Integer>(); 
        Map<String,Integer> mapOfZoneCatNBP                                 = new map<String,Integer>();
        Map<String,Sales_Planning__c> mapOfZoneForcast = new Map<String,Sales_Planning__c>();
        Id specialtyDealerId            = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: SpecialtyDealerLabel Limit 1].Id;
        //Id specialtyForcastROId         = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast Limit 1].Id;
        Id specialtyForcastZoneId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: Specialty_Forcast_Zone Limit 1].Id;
        Set<String> catSet                                              = new set<String>();
        Set<String> setOFzone                                           = new set<String>();
        Set<String> setkeyCatZone = new Set<String>();
       global Database.QueryLocator start(Database.BatchableContext BC){
            String query;
            //getting Specialty Category from custom setting  
            
            //getting all staging records with BU specialty and Forcast
            
           Id recordTypeStagingId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeStagingLabel Limit 1].Id;
            query = 'SELECT id,Parent_Sales_Planning__c,Customer_group__c,Value__c,SYS_Used_IN_Batch2__c,Dealer__c,SYS_TL_CAT__c,Dealer_CustNumber__c,Dealer_Name__c,Category__c,Category_Description__c,Month__c ,Year__c,Region_code__c ,Region_Description__c,RecordTypeId,'
            +'Territory_Code__c,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Budget__c,Value_L3M__c,Value_LYCM__c,Zone__c '+
            'FROM Sales_Planning__c WHERE  Year__c =\''+year+'\' AND Month__c =\''+month+'\' AND RecordTypeId =\''+specialtyDealerId+'\' AND Total_planned__c = null' ;
            System.debug(Database.getQueryLocator(query)+'@@@query@@@@');
            
             /*
            //Id recordTypeStagingId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeStagingLabel Limit 1].Id;
            
            query = 'SELECT Id,OwnerId,ASP__c,Budget__c,Reg_code__c,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,'
            +'Territory_Code__c,NBP__c,Year__c,SYS_Used_IN_Batch2__c,SPExternalIDTL__c,BU__c,SKU__c,MAKTX__c,Zone__c,Zone_Description__c,Value__c,Value_L3M__c,Value_LYCM__c '+
            'FROM Sales_Planing_Staging__c WHERE ( BU__c = \''+special+'\' )AND Year__c =\''+year+'\' AND Month__c =\''+month+'\' and Reg_code__c =\'PAT\'' ;
            */
                return Database.getQueryLocator(query);
         }
         global void execute(Database.BatchableContext BC,List<sObject> scope){
            /*
            List<Speciality_Sales_Planning_Categories__c> spc = Speciality_Sales_Planning_Categories__c.getall().values();
                for(Speciality_Sales_Planning_Categories__c sp : spc) {
                    if(sp.Include_in_Sales_Planning__c == true){
                        catSet.add(sp.Category_Code__c);
                        //mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);     
                    }     
                }
           */
            //system.debug(catSet+'catSet');
            for(sObject temp : scope){
                Sales_Planning__c sps =(Sales_Planning__c)temp;
                setkeyCatZone.add(sps.SYS_Used_IN_Batch2__c);
                setOFzone.add(sps.Zone__c);
            }
            system.debug(setkeyCatZone+'setkeyCatZone');
            for(String s : setkeyCatZone){
                mapOfZoneTotalL3M1.put(s,0);
            }
            system.debug(mapOfZoneTotalL3M1+'mapOfZoneTotalL3M1Before');
            for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
                Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,Total_planned__c,SYS_Used_IN_Batch2__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE  SYS_Used_IN_Batch2__c IN : setkeyCatZone AND RecordTypeId =: specialtyDealerId AND Year__c =:year  AND Month__c =: month] ){
                if(mapOfZoneTotalL3M1.containsKey(salesPlan.SYS_Used_IN_Batch2__c)){
                    totalL3M = 0;
                    totalL3M = mapOfZoneTotalL3M1.get(salesPlan.SYS_Used_IN_Batch2__c);
                    totalL3M = totalL3M +Integer.valueOf(salesPlan.L3M__c);
                    mapOfZoneTotalL3M1.put(salesPlan.SYS_Used_IN_Batch2__c,totalL3M);
                }/*else {
                    mapOfZoneTotalL3M1.put(salesPlan.SYS_Used_IN_Batch2__c,salesPlan.L3M__c);
                }*/
                //mapOfZoneForcast.put(salesPlan.SYS_Used_IN_Batch2__c,salesPlan ); 
            }
            system.debug(mapOfZoneTotalL3M1+'mapOfZoneTotalL3M1After');
            
            system.debug(scope+'scope');
            system.debug(setOFzone+'setOFzone');
            system.debug(mapOfZoneTotalL3M1+'mapOfZoneTotalL3M');
            //getting all current month Zone forcast records for logged in user 
            salesPlanningSpeForcastZone = [SELECT Id,NBP__c,Discount_On_NBP__c,SYS_Used_IN_Batch2__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,Month__c,Zone__c,Region_code__c,Region_Description__c,SPExternalIDTL__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE  Zone__c IN:setOFzone AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specialtyForcastZoneId];
            for(Sales_Planning__c sp : salesPlanningSpeForcastZone){
                mapOfZoneTarget.put(sp.SYS_Used_IN_Batch2__c,Integer.valueOF(sp.Target_Quantity__c));
                mapOfZoneID.put(sp.SYS_Used_IN_Batch2__c,sp.id);
                mapOfZoneCatNBP.put(sp.SYS_Used_IN_Batch2__c,Integer.valueOf(sp.Discount_On_NBP__c));
            }
            system.debug(mapOfZoneTarget+'mapOfZoneTarget');
            system.debug(salesPlanningSpeForcastZone.size()+'salesPlanningSpeForcastZone');
            system.debug(mapOfZoneTotalL3M1.size()+'VVVV');
            if(mapOfZoneTotalL3M1.size() > 0){
                for(sObject temp : scope){
                    Sales_Planning__c sps =(Sales_Planning__c)temp;
                    if(sps.L3M__c != null && mapOfZoneTarget.containsKey(sps.SYS_Used_IN_Batch2__c) && mapOfZoneTotalL3M1.containsKey(sps.SYS_Used_IN_Batch2__c)){
                        system.debug('InsideIf');
                        sps.Total_planned__c = ((sps.L3M__c * mapOfZoneTarget.get(sps.SYS_Used_IN_Batch2__c))/mapOfZoneTotalL3M1.get(sps.SYS_Used_IN_Batch2__c)).round();
                        sps.Parent_Sales_Planning__c = mapOfZoneID.get(sps.SYS_Used_IN_Batch2__c);
                        sps.Value__c = mapOfZoneCatNBP.get(sps.SYS_Used_IN_Batch2__c) * sps.Total_Planned__c;
                    }else if(sps.L3M__c == null){
                        sps.Total_planned__c = 0;
                        sps.Value__c = 0;
                        sps.Parent_Sales_Planning__c = mapOfZoneID.get(sps.SYS_Used_IN_Batch2__c);
                        system.debug('InsideElse');
                    }
                    salesPlanningSpeDealerUpdate.add(sps);
                    system.debug(sps.L3M__c+'!!!!!!!!!!'+sps.Total_planned__c+'##########');
                }
            }
            system.debug(salesPlanningSpeDealerUpdate.size()+'salesPlanningSpeDealerUpdate');
            system.debug(salesPlanningSpeDealerUpdate+'salesPlanningSpeDealerUpdate');
            if(salesPlanningSpeDealerUpdate.size() > 0){
                upsert salesPlanningSpeDealerUpdate SPExternalIDTL__c;
            }
         }
         global void finish(Database.BatchableContext BC)
         {       
            system.debug(salesPlanningSpeDealerUpdate.size()+'salesPlanningSpeDealerUpdate');
            
             // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForSpecialtyBU_AutoPopulate Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForSpecialtyBU_AutoPopulate" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
            
         }
}