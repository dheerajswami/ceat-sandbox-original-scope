global with sharing class ProspectControllerExport {
    id recId;
    public  Account accObj{get;set;}
    public string selectedState{get;set;}
    public List<String> statesList {get;set;}
    public List<Territory2> territoryList = new list<Territory2>();
    public List<Territory2> countryList  = new list<Territory2>();
    
    public Set<String> territorySet {get;set;}
    public Set<String> countrySet    {get;set;}
    public String prospectId{get;set;}   
    public Boolean redirectPage{get;set;}   
    static Account accRec;
    set<id> terrTypeID                     = new set<Id>();
    set<id> counypeID                      = new set<Id>();
    list<Territory2Type> terriType         = new list<Territory2Type>();
    list<Territory2Type> countryType        = new list<Territory2Type>();
    Map<String,String> mapOfidAndClustercode    = new map<String,String>();
    public ProspectControllerExport (ApexPages.StandardController con){
        //Quering id of Prospect record type
        recId=[select id,DeveloperName from RecordType where SobjectType='Account' AND DeveloperName='Prospect' limit 1].Id;
        accObj=new Account(RecordTypeId=recId);
        redirectPage = false;
        countryType         = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Country'];
        for(Territory2Type temp : countryType){
            counypeID.add(temp.id);
        }
        countryList = [Select id,DeveloperName,ParentTerritory2Id,Territory2TypeId,name from Territory2 where Territory2TypeId IN : counypeID];
        countrySet = new set<String>();
        for(Territory2  tr : countryList ){
            countrySet.add(tr.DeveloperName);
        }
        terriType         = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Cluster'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        territoryList = [Select id,DeveloperName,ParentTerritory2Id,Territory2TypeId,name from Territory2 where Territory2TypeId IN : terrTypeID];
        territorySet = new set<String>();
        for(Territory2  tr : territoryList){
            territorySet.add(tr.DeveloperName);
            mapOfidAndClustercode.put(tr.id,tr.DeveloperName);
        }
    }

    public List<SelectOption> getClusters(){
        List<SelectOption> options = new List<SelectOption>();
        list<Territory2>TerritoryCluster = new list<Territory2>();
        list<Territory2Type> terriType                          = new list<Territory2Type>();
        set<id> terrTypeID                                      = new set<Id>();
        //options.add(new SelectOption('None','--- None ---'));     
        Set<String> stSet=new Set<String>();
        terriType               = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'Cluster'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        TerritoryCluster = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID];
        for(Territory2 masterRec: TerritoryCluster){
            options.add(new SelectOption(String.valueOf(masterRec.id),String.valueOf(masterRec.DeveloperName)));        
        }
        options.sort();
        system.debug(options+'options');
        return options;
        
    }
    //Passing cluster as param and getting all country under that cluster
    @RemoteAction
    global static List<SelectOption> CountrysNameList(String strclusterId){
        system.debug(strclusterId+'strclusterId');
        List<SelectOption> options = new List<SelectOption>();
        Set<String> countrySet1=new Set<String>();
        options.add(new SelectOption('None','--- None ---')); 
        for(Territory2 s:[SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE ParentTerritory2Id =: strclusterId order by DeveloperName asc]){
            if(!countrySet1.contains(s.DeveloperName)){
                options.add(new SelectOption(s.DeveloperName,s.Description));
                countrySet1.add(s.DeveloperName);
            }
        }   
        return options;
    }
  
    //Creating Prospect in account having record type prospect
    public void createProspect(){
        List<Territory2> territoryId                      = new List<Territory2>(); 
        List<Territory2> roTerritoryId                    = new List<Territory2>(); 
        List<Territory2> zoTterritoryId                   = new List<Territory2>(); 
        
        system.debug(accObj.Name+'accout');
        if(accObj.Name == ''){
            accObj.Name.addError('enter acc name');
        }
        
        accObj.Sales_Office_Text__c = mapOfidAndClustercode.get(selectedState);
        accObj.LAND1_GP__c =Apexpages.currentPage().getParameters().get('myCountry'); 
        accObj.Type = 'Prospect';
        //if(territorySet.contains(accObj.Sales_Office_Text__c.toUpperCase()) && countrySet.contains(accObj.Country__c.toUpperCase())){
            redirectPage = true;
            try{
                
                accObj.Sales_Office_Text__c = accObj.Sales_Office_Text__c.toUpperCase();
                accObj.LAND1_GP__c= accObj.LAND1_GP__c.toUpperCase();
                accObj.Business_Units__c = 'Exports';
                insert accObj;
            }
            catch(Exception e){}
            prospectId=accObj.Id;
        //}else{
           //redirectPage = false;
           //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Please enter Account name'));
           //system.debug();
        }
        
    
}