global class Dashboard_Output_Controller_RM { 
    
    public String loggedInUserName    {get;set;}
    public String loggedInUserRO  {get;set;}
    public string loggedInUserZone {get;set;}
    public string loggedInUserRole {get;set;}
    public Static String lastDateRun {get;set;}
    
    public Static id userId          {get;set;}  
    public Static string selectedRegion {get;set;}    
    public Static string selectedMonth {get;set;}
    public Static string selectedYear {get;set;}
    
    public Static String currentDay              = String.valueOf((Date.today()).day());
    public Static String currentMonth            = String.valueOf((Date.today()).month());
    public Static String currentYear             = String.valueOf((Date.today()).year());
    
    //String loggedInUserRO ;
    List<UserTerritory2Association> userTerrCode;
    List<Territory2> userRegion;
    
    list<Dashboard_Score__c> dashboardScore1 = new list<Dashboard_Score__c>();
    global  Dashboard_Output_Controller_RM () { 
        
        userId            = ApexPages.currentPage().getParameters().get('Id');
        selectedMonth     = ApexPages.currentPage().getParameters().get('month');
        selectedYear      = ApexPages.currentPage().getParameters().get('year');
        
        Id loggedInUserId = UserInfo.getUserId();
        if(selectedMonth== null || selectedYear==null){
                selectedMonth = currentMonth ;
                selectedYear = currentYear;
        }
        
        user loggin      = [SELECT id,name,FirstName From User Where id =: userId ];
        loggedInUserName = loggin.FirstName;  
        userTerrCode=[select territory2Id,id,RoleInTerritory2 from UserTerritory2Association where UserId=:userId AND RoleInTerritory2 = 'RM'  limit 1];  
                 if(userTerrCode.size() > 0){
                     userRegion=[select name, id,ParentTerritory2.name from Territory2 where id=:userTerrCode[0].Territory2Id];
                 }
                 if(userRegion.size() > 0){
                     loggedInUserRO   = userRegion[0].Name;
                     loggedInUserZone = userRegion[0].ParentTerritory2.name;
                     loggedInUserRole = userTerrCode[0].RoleInTerritory2 ;
                 }
       dashboardScore1  = [SELECT id,Actual_MTD__c,Region_Code__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,User__c,LastModifiedDate FROm Dashboard_Score__c where User__c=:userId AND Month__c =: selectedMonth AND Year__c =: selectedYear Limit 1];
       if(dashboardScore1.size()>0){
            lastDateRun = dashboardScore1[0].LastModifiedDate.date().adddays(-1).format() ;
       }
       if(selectedMonth != string.valueof(date.today().month())){
            Integer numberOfDays = date.daysInMonth(integer.valueof(selectedYear), integer.valueof(selectedMonth));
            Date lastDayOfMonth  = date.newInstance(integer.valueof(selectedYear), integer.valueof(selectedMonth), numberOfDays);   
            lastDateRun          = string.valueof(lastDayOfMonth.format());
        }
              
    }
    @RemoteAction
    Public Static Dashboard_Output_Handler_Controller_RM.dashboardWrapperMain  getDashboardData (Id uId, string month, string year) {
           
            Dashboard_Output_Handler_Controller_RM handler = new Dashboard_Output_Handler_Controller_RM();
            return handler.getDashboardDataRecords(uId,month,year);
            
    }
}