Global class Scheduled_BatchForMakingActualToZero implements Schedulable{
     global void execute(SchedulableContext sc) {      
      SP_BatchForMakingActualValueToZero spMakingActualToZero = new SP_BatchForMakingActualValueToZero (); 
      database.executebatch(SpMakingActualToZero ,150);
   }

}