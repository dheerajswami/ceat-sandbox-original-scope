global with sharing class Efleet_HomeController_Specialty {
    @RemoteAction
    global static List<Account> fetchAllFleets(){
        return [SELECT Id,Name,Town__c,KUNNR__c,Customer_Segment__c,District__c,Sales_Group_Text__c,Sales_District_Text__c,Fleet_Size_Self__c,Year_of_Estbm__c,TIN_No__c,VAT_No__c, UniqueIdentifier__c, BillingState, Nature_of_business__c, Size_Of_Business__c, Fleet_Type__c, Business_Type__c FROM Account Order by Name];     
    }
    
    @RemoteAction
    global static List<Account> fetchAllDealers(){               
        return [SELECT Id,Name,Town__c,KUNNR__c,Customer_Segment__c,District__c,Sales_District_Text__c,Fleet_Size_Self__c,Year_of_Estbm__c,TIN_No__c,VAT_No__c, UniqueIdentifier__c, BillingState FROM Account WHERE KUNNR__c != null Order by Name];     
    }
    
    @RemoteAction
    global static List<String> fetchCustomerTypes(){        
        List<String> customerTypes = new List<String>();
        for(Schema.PicklistEntry pe : Account.getSObjectType().getDescribe().fields.getMap().get('Fleet_Type__c').getDescribe().getPickListValues()) {
            customerTypes.add(pe.getValue());
        }
        return customerTypes;
    }
    
    @RemoteAction
    global static List<String> fetchVehicleTypes(String str){        
        List<String> vehicleTypes = new List<String>();
        if(str == 'OTR') {
            vehicleTypes = System.Label.OTR_Vehicle_Types.split(',');
        } else if(str == 'Farm') {
            vehicleTypes = System.Label.Farm_Vehicle_Types.split(',');
        }
        return vehicleTypes;
    }
    
    @RemoteAction
    global static List<String> fetchSalesGroups(){        
        List<String> salesGroups = new List<String>();
        
        UserTerritory2Association uZone = [Select Territory2Id from UserTerritory2Association where UserId = :UserInfo.getUserId()];
        List<Territory2> regions = [Select Id, Name from Territory2 where ParentTerritory2Id = :uZone.Territory2Id];
        
        for(Territory2 terr : regions) {
            salesGroups.add(terr.Name);
        }
        
        return salesGroups;
    }
    
    @RemoteAction
    global static List<String> fetchSalesDistricts(String sGroup){        
        List<String> salesDistricts = new List<String>();
        
        UserTerritory2Association uZone = [Select Territory2Id from UserTerritory2Association where UserId = :UserInfo.getUserId()];
        Territory2 region = [Select Id from Territory2 where Name = :sGroup And ParentTerritory2Id = :uZone.Territory2Id];
        List<Territory2> territories = [Select Id, Name from Territory2 where ParentTerritory2Id = :region.Id];
        
        for(Territory2 terr : territories) {
            salesDistricts.add(terr.Name);
        }
        
        return salesDistricts;
    }
    
    @RemoteAction
    global static List<String> fetchTyreBrands(String str){        
        List<String> tyreBrands = new List<String>();
        if(str == 'OTR') {
            tyreBrands = System.Label.OTR_Tyre_Brands.split(',');
        } else if(str == 'Farm') {
            tyreBrands = System.Label.Farm_Tyre_Brands.split(',');
        }
        return tyreBrands;
    }
    
    @RemoteAction
    global static List<String> fetchCeatSubBrands(String str){        
        List<String> ceatSubBrands = new List<String>();
        if(str == 'OTR') {
            ceatSubBrands = System.Label.OTR_CEAT_SubBrands.split(',');
        } else if(str == 'Farm') {
            ceatSubBrands = System.Label.Farm_CEAT_SubBrands.split(',');
        }
        return ceatSubBrands;
    }
    
    @RemoteAction
    global static List<EFleet_Specialty_Business_Info__c> fetchSizeOfBusiness(String str){        
        List<EFleet_Specialty_Business_Info__c> businesses = new List<EFleet_Specialty_Business_Info__c>();
        List<EFleet_Specialty_Business_Info__c> allBusinesses = new List<EFleet_Specialty_Business_Info__c>();
        Map<String, EFleet_Specialty_Business_Info__c> allBusinessesMap = EFleet_Specialty_Business_Info__c.getAll();
        allBusinesses = allBusinessesMap.values();
        
        for(EFleet_Specialty_Business_Info__c esbi : allBusinesses) {
            if(esbi.OTR_Farm__c == str) {
                businesses.add(esbi);
            }
        }        
        
        return businesses;
    }
    
    @RemoteAction
    global static List<String> fetchStates(){
        Set<String> states = new Set<String>();
        List<String> listStates = new List<String>();
        for(State_Master__c sm : [SELECT Id, State__c FROM State_Master__c Order By State__c]) {
            states.add(sm.State__c);
        }
        listStates.addAll(states);
        listStates.sort();
        
        return listStates;
    }
    
    @RemoteAction
    global static List<String> fetchDistricts(String state){
        Set<String> districts = new Set<String>();
        List<String> listDistricts = new List<String>();
        for(State_Master__c sm : [SELECT Id, District__c FROM State_Master__c where State__c = :state Order By District__c]) {
            districts.add(sm.District__c);
        }
        listDistricts.addAll(districts);
        listDistricts.sort();
        return listDistricts;
    }
    
    @RemoteAction
    global static List<String> fetchTowns(String district){
        Set<String> towns = new Set<String>();
        List<String> listTowns = new List<String>();
        for(State_Master__c sm : [SELECT Id, Town__c FROM State_Master__c where District__c = :district Order By Town__c]) {
            towns.add(sm.Town__c);
        }
        listTowns.addAll(towns);
        listTowns.sort();
        return listTowns;
    }
    
    @RemoteAction
    global static List<String> fetchVehicleMaker_Type(){
        Set<String> vTypes = new Set<String>();
        List<String> listTypes = new List<String>();
        for(Fleet_Vehicle_Maker_Specialty__c sm : [SELECT Id, Vehicle_Type__c FROM Fleet_Vehicle_Maker_Specialty__c Order By Vehicle_Type__c]) {
            vTypes.add(sm.Vehicle_Type__c);
        }
        listTypes.addAll(vTypes);
        listTypes.sort();
        listTypes.add('Other');
        
        return listTypes;
    }
    
    @RemoteAction
    global static List<String> fetchVehicleMaker_Maker(String vType){
        Set<String> vMakers = new Set<String>();
        List<String> listMakers = new List<String>();
        
        String query = 'Select Id, Vehicle_Maker__c FROM Fleet_Vehicle_Maker_Specialty__c';
        if(vType != 'Other') {
            query += ' where Vehicle_Type__c = :vType';
        }        
        query += ' Order By Vehicle_Maker__c';
        
        for(Fleet_Vehicle_Maker_Specialty__c sm : Database.Query(query)) {
            vMakers.add(sm.Vehicle_Maker__c);
        }
        listMakers.addAll(vMakers);
        listMakers.sort();
        listMakers.add('Other');
        
        return listMakers;
    }
    
    @RemoteAction
    global static List<String> fetchVehicleMaker_TyreSize(String vType, String vMaker){
        Set<String> tyreSizes = new Set<String>();
        List<String> listSizes = new List<String>();
        
        String query = 'Select Id, Vehicle_Tyre_Size__c FROM Fleet_Vehicle_Maker_Specialty__c';
        if(vType != 'Other') {
            query += ' where Vehicle_Type__c = :vType';
        }
        if(vMaker != 'Other') {
            if(vType != 'Other') {
                query += ' And';
            } else {
                query += ' Where';
            }
            query += ' Vehicle_Maker__c = :vMaker';
        }
        query += ' Order By Vehicle_Maker__c';
        
        for(Fleet_Vehicle_Maker_Specialty__c sm : Database.Query(query)) {
            tyreSizes.add(sm.Vehicle_Tyre_Size__c);
        }
        listSizes.addAll(tyreSizes);
        listSizes.sort();
        listSizes.add('Other');
        
        return listSizes;
    }
    
    @RemoteAction
    global static Account fetchSelectedFleet(Id accId){             
        return [Select Id, Name, Year_of_Estbm__c, VAT_No__c, TIN_No__c, UniqueIdentifier__c, Office_Area__c, Active_Months_Specialty__c, 
                Service_Area__c, Distance_Nearest_CEAT_Dealer__c, Distance_Nearest_Non_CEAT_Dealer__c, Fleet_Category__c, 
                Fleet_Type__c, BillingState, BillingCity, BillingStreet, Office_TL_Territory__c, BillingPostalCode, Sales_Group_Text__c, 
                ShippingState, ShippingCity, ShippingStreet, Service_TL_Territory__c, ShippingPostalCode, Business_Type__c, Sales_District_Text__c, 
                Nearest_Non_CEAT_Dealer__c, Nearest_CEAT_Dealer__c, Fleet_Size_Attached__c, Fleet_Size_Self__c, Nature_of_business__c, Size_Of_Business__c from Account where Id=:accId];
    }
    
    @RemoteAction
    global static List<Fleet_Crop_Details__c> fetchSelectedCrops(Id accId) {
        return [Select Id, Acreage__c, Active_Seasons__c, Crops__c from Fleet_Crop_Details__c where Transporter_Name__c=:accId];
    }
    
    @RemoteAction
    global static Contact fetchSelectedContact(Id accId){               
        return [Select Id, LastName, Phone, Email, Birthdate, Wedding_Anniversary__c, Manager_Name__c, 
                Manager_No__c, Manager_Email__c, AssoCEAT_Member__c, AssoCEAT_Num__c from Contact where AccountId = :accId Limit 1];       
    }
    
    @RemoteAction
    global static List<Fleet_Vehicle_Type_Specialty__c> fetchSelectedVehicleType(Id accId){               
        return [Select Id, No_of_Vehicles__c, Front_Average_Price_Tyre__c, Front_No_of_Tyres_Vehicle__c, Front_Replacement_Cycle_Months__c, 
                Rear_Average_Price_Tyre__c, Rear_No_of_Tyres_Vehicle__c, Rear_Replacement_Cycle_Months__c, Vehicle_Maker__c,  
                Size_of_Wallet_Per_Month__c, Vehicle_Type__c, Transporter_Name__c, Vehicle_Tyre_Size__c 
                from Fleet_Vehicle_Type_Specialty__c where Transporter_Name__c = :accId];
    }
    
    @RemoteAction
    global static List<WrapperVehicleType> fetchVehicleTypeSummary(Id accId) {        
        List<Fleet_Vehicle_Type_Specialty__c> allVehicles = [Select Id, No_of_Vehicles__c, Front_Average_Price_Tyre__c, Front_No_of_Tyres_Vehicle__c, Front_Replacement_Cycle_Months__c, 
                Rear_Average_Price_Tyre__c, Rear_No_of_Tyres_Vehicle__c, Rear_Replacement_Cycle_Months__c, Vehicle_Maker__c,  
                Size_of_Wallet_Per_Month__c, Vehicle_Type__c, Transporter_Name__c, Vehicle_Tyre_Size__c 
                from Fleet_Vehicle_Type_Specialty__c where Transporter_Name__c = :accId];
        
        Map<String, WrapperVehicleType> mapVehicleType = new Map<String, WrapperVehicleType>();
        
        for(Fleet_Vehicle_Type_Specialty__c vt : allVehicles) {
            if(vt.No_of_Vehicles__c == null)
                vt.No_of_Vehicles__c = 0;
            if(vt.Front_No_of_Tyres_Vehicle__c == null)
                vt.Front_No_of_Tyres_Vehicle__c = 0;
            if(vt.Rear_No_of_Tyres_Vehicle__c == null)
                vt.Rear_No_of_Tyres_Vehicle__c = 0;
            if(vt.Size_of_Wallet_Per_Month__c == null)
                vt.Size_of_Wallet_Per_Month__c = 0;
            
            if(mapVehicleType.containsKey(vt.Vehicle_Type__c)) {
                mapVehicleType.get(vt.Vehicle_Type__c).noOfVehicles += Integer.valueOf(vt.No_of_Vehicles__c);
                mapVehicleType.get(vt.Vehicle_Type__c).noOfTyres += Integer.valueOf(vt.No_of_Vehicles__c) * (vt.Front_No_of_Tyres_Vehicle__c + vt.Rear_No_of_Tyres_Vehicle__c);
                mapVehicleType.get(vt.Vehicle_Type__c).walletSize += vt.Size_of_Wallet_Per_Month__c;
            } else {
                WrapperVehicleType wvt = new WrapperVehicleType();
                wvt.vType = vt.Vehicle_Type__c;
                wvt.noOfVehicles = Integer.valueOf(vt.No_of_Vehicles__c);
                wvt.noOfTyres = wvt.noOfVehicles * (vt.Front_No_of_Tyres_Vehicle__c + vt.Rear_No_of_Tyres_Vehicle__c);
                wvt.walletSize = vt.Size_of_Wallet_Per_Month__c;
                mapVehicleType.put(vt.Vehicle_Type__c, wvt);
            }
        }
        
        return mapVehicleType.values();
    }
    
    global class WrapperVehicleType{
        global String vType {get;set;}
        global Integer noOfVehicles {get;set;}
        global Decimal noOfTyres {get;set;}
        global Decimal walletSize {get;set;}
    }
    
    @RemoteAction
    global static List<Fleet_Vehicle_Information_Specialty__c> fetchSelectedVehicleInfo(Id accId){               
        return [Select Id, Current_Tyre_Brand__c, Date_Installed__c, Make_Model__c, N_1_Tyre_Brand__c, N_1_Tyre_Life__c, 
                N_2_Tyre_Brand__c, N_2_Tyre_Life__c, Purpose__c, Registration_No__c, Transporter_Name__c, Vehicle_Type__c
                from Fleet_Vehicle_Information_Specialty__c where Transporter_Name__c = :accId];
    }
    
    @RemoteAction
    global static List<Fleet_Ceat_Vehicle_Specialty__c> fetchCeatRelationships(Id accId){   
        
        List<Fleet_Vehicle_Information_Specialty__c> fvisList =  fetchSelectedVehicleInfo(accId);
        List<Fleet_Ceat_Vehicle_Specialty__c> ceatVehicles = new List<Fleet_Ceat_Vehicle_Specialty__c>();
        Set<Id> fvisIds = new Set<Id>();
        for(Fleet_Vehicle_Information_Specialty__c fvis : fvisList) {
            if(fvis.Current_Tyre_Brand__c.equalsIgnoreCase('CEAT'))            
                fvisIds.add(fvis.Id);            
        }
        
        ceatVehicles = [Select Id, CEAT_Sub_Brand__c, Comments__c, Location__c, No__c, Under_PE__c, Vehicle__c, Registration_No__c 
                        from Fleet_Ceat_Vehicle_Specialty__c where Vehicle__c in :fvisIds];
        
        if(ceatVehicles.size() == 0) {            
            Fleet_Ceat_Vehicle_Specialty__c newCeatVehicle;
            for(Fleet_Vehicle_Information_Specialty__c fvis : fvisList) {                
                if(fvis.Current_Tyre_Brand__c.equalsIgnoreCase('CEAT')) {
                    newCeatVehicle = new Fleet_Ceat_Vehicle_Specialty__c();
                    newCeatVehicle.Registration_No__c = fvis.Registration_No__c;
                    newCeatVehicle.Vehicle__c = fvis.Id;
                    newCeatVehicle.Location__c = 'Front';
                    ceatVehicles.add(newCeatVehicle);
                    
                    newCeatVehicle = new Fleet_Ceat_Vehicle_Specialty__c();
                    newCeatVehicle.Registration_No__c = fvis.Registration_No__c;
                    newCeatVehicle.Vehicle__c = fvis.Id;
                    newCeatVehicle.Location__c = 'Rear';
                    ceatVehicles.add(newCeatVehicle);
                }                    
            }            
        }
        
        return ceatVehicles;
    }
    
    @RemoteAction
    global static Account saveFleetDetails(Account fleet, List<Fleet_Crop_Details__c> crops){     
        if(fleet.Id == null) {
            RecordType rType = [Select Id from RecordType where SObjectType = 'Account' And DeveloperName = 'Specialty'];
            //fleet.Customer_Segment__c = 'P4';
            fleet.RecordTypeId = rType.Id;
            //fleet.Sales_District_Text__c = fleet.Office_TL_Territory__c;
            //fleet.Customer_Group__c = 'FL';
            fleet.Type = 'Customer';
        }
        upsert fleet;       
        for(Fleet_Crop_Details__c v : crops) {
            v.Transporter_Name__c = fleet.Id;
        }
        if(fleet.Business_Type__c == 'Farm') {
            List<Fleet_Crop_Details__c> existingList = [Select Id from Fleet_Crop_Details__c where Transporter_Name__c = :fleet.Id];       
            List<Fleet_Crop_Details__c> deleteCropInfo = new List<Fleet_Crop_Details__c>();
                        
            for(Fleet_Crop_Details__c e : existingList) {
                Boolean isExist = false;
                
                for(Fleet_Crop_Details__c v : crops) {
                    if(e.Id == v.Id) {
                        isExist = true;
                        break;
                    }
                }    
                if(!isExist) {
                    deleteCropInfo.add(e);
                }
            }
            
            delete deleteCropInfo;
            upsert crops;
        }        	
        
        return fleet;
    }
    
    @RemoteAction
    global static void saveContactDetails(Contact con, String birthDate, String weddingAnniversary){                
        if(birthDate != null && birthDate != '') {             
            con.Birthdate = Date.newInstance(Integer.valueOf(birthDate.split('/')[2]), Integer.valueOf(birthDate.split('/')[1]), Integer.valueOf(birthDate.split('/')[0]));    
        } else         
            con.Birthdate = null;
        if(weddingAnniversary != null && weddingAnniversary != '') {
            con.Wedding_Anniversary__c = Date.newInstance(Integer.valueOf(weddingAnniversary.split('/')[2]), Integer.valueOf(weddingAnniversary.split('/')[1]), Integer.valueOf(weddingAnniversary.split('/')[0]));   
        } else         
            con.Wedding_Anniversary__c = null;
        
        upsert con;
    }
    
    @RemoteAction
    global static void saveVehicleType(Id accId, Fleet_Vehicle_Type_Specialty__c vehicleType){ 
        Account acc = [Select Id, Size_Of_Wallet__c from Account where Id=:accId];
        if(acc.Size_Of_Wallet__c == null)
            acc.Size_Of_Wallet__c = 0;
        
        if(vehicleType.Id != null) {        
            Fleet_Vehicle_Type_Specialty__c cVehicleType = [Select Id, Size_of_Wallet_Per_Month__c from Fleet_Vehicle_Type_Specialty__c where Id = :vehicleType.Id];
            acc.Size_Of_Wallet__c = acc.Size_Of_Wallet__c + vehicleType.Size_of_Wallet_Per_Month__c - cVehicleType.Size_of_Wallet_Per_Month__c;
        } else {
            acc.Size_Of_Wallet__c = acc.Size_Of_Wallet__c + vehicleType.Size_of_Wallet_Per_Month__c;
        }
        update acc;
        upsert vehicleType;
        /*
        List<Fleet_Vehicle_Type_Specialty__c> existingList = [Select Id from Fleet_Vehicle_Type_Specialty__c where Transporter_Name__c = :accId And Vehicle_Type__c = :vehicleInfo[0].Vehicle_Type__c];       
        List<Fleet_Vehicle_Type_Specialty__c> deleteVehInfo = new List<Fleet_Vehicle_Type_Specialty__c>();
        
        for(Fleet_Vehicle_Type_Specialty__c e : existingList) {
            Boolean isExist = false;
            for(Fleet_Vehicle_Type_Specialty__c v : vehicleInfo) {
                if(e.Id == v.Id) {
                    isExist = true;
                    break;
                }
            }    
            if(!isExist) {
                deleteVehInfo.add(e);
            }
        }
        
        delete deleteVehInfo;
        upsert vehicleInfo;*/
    }
    
    @RemoteAction
    global static void saveVehicleInfo(Id accId, List<Fleet_Vehicle_Information_Specialty__c> vehicleInfo, List<String> datesInstalled){                
        
        List<Fleet_Vehicle_Information_Specialty__c> existingList = [Select Id from Fleet_Vehicle_Information_Specialty__c where Transporter_Name__c = :accId And Vehicle_Type__c = :vehicleInfo[0].Vehicle_Type__c];       
        List<Fleet_Vehicle_Information_Specialty__c> deleteVehInfo = new List<Fleet_Vehicle_Information_Specialty__c>();
        
        for(Integer i=0; i<vehicleInfo.size(); i++) {
            if(datesInstalled[i] != null && datesInstalled[i] != '') {             
                vehicleInfo[i].Date_Installed__c = Date.newInstance(Integer.valueOf(datesInstalled[i].split('/')[2]), Integer.valueOf(datesInstalled[i].split('/')[1]), Integer.valueOf(datesInstalled[i].split('/')[0]));    
            }          	
        }
        
        for(Fleet_Vehicle_Information_Specialty__c e : existingList) {
            Boolean isExist = false;
            for(Fleet_Vehicle_Information_Specialty__c v : vehicleInfo) {
                if(e.Id == v.Id) {
                    isExist = true;
                    break;
                }
            }    
            if(!isExist) {
                deleteVehInfo.add(e);
            }
        }
        
        delete deleteVehInfo;
        upsert vehicleInfo;
    }
    
    @RemoteAction
    global static void saveCeatRelationships(List<Fleet_Ceat_Vehicle_Specialty__c> ceatVehicles){                
        upsert ceatVehicles;
    }
    
    public static void sortListAndOthers(List<String> gotList) {
        String otherString = '';
        for(Integer i=0; i<gotList.size(); i++) {
            if(gotList[i].equalsIgnoreCase('Other') || gotList[i].equalsIgnoreCase('Others')) {
                otherString = gotList[i];
                gotList.remove(i);
                i--;
            }
        }
        gotList.sort();
        gotList.add(otherString);
    }
}