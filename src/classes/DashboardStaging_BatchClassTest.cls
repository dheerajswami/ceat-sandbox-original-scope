@isTest
private class DashboardStaging_BatchClassTest {
    static testmethod void test() {
        // The query used by the batch job.
        
        String query = 'SELECT ID,Name FROM Dashboard_Staging__c WHERE CreatedDate = today';
        
        // Create some test items to be deleted
        
        //   by the batch job.
        
        Dashboard_Staging__c[] ds = new List<Dashboard_Staging__c>();
        
        for (Integer i=0;i<10;i++) {
            
            Dashboard_Staging__c d = new Dashboard_Staging__c(
                
                Category__c='Some category',
                
                Email_Id__c='supriya@s.com');
            
            ds.add(d);
        }
        insert ds;
        
        Test.startTest();
        
        DashboardStaging_BatchClass c = new DashboardStaging_BatchClass();
        
        Database.executeBatch(c);
        
        Test.stopTest();
        
        
        
        
        Integer i = [SELECT COUNT() FROM Dashboard_Staging__c];
        System.assertEquals(i, 0);
    }
}