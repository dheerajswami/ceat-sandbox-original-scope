/**
 * cMapping class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates cMapping class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class CFormDetails_TestClass{
    
    
     // Creating Response for web service mapping class
    private class WebServiceMockImpl implements WebServiceMock
     {        
        date myDate = date.newInstance(2015, 2, 24);
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {  
            //CFormDetails.CFormMapping cMapping = new CFormDetails.CFormMapping('51000460','360 DEGREE WHEELS','2015-02-24','February','0020305529','100457','BR 600-16 TF','100457','100457','4.0','49409.28','49409.28','55585.44','B0041','Karnataka','100457');
            
            list<SAP_ZWS_CFORM.ZSFDC_SALES> emList = new list<SAP_ZWS_CFORM.ZSFDC_SALES>();
            SAP_ZWS_CFORM.ZSFDC_SALES elem = new SAP_ZWS_CFORM.ZSFDC_SALES();
            elem.CUSTOMER = '51000460';
            elem.CUSTOMER_NAME = '360 DEGREE WHEELS';
            elem.INVOICE_DATE = '2015-02-24';
            elem.MONTH ='February';
            elem.INVOICE ='0020305529';
            elem.ITEM = '1223';
            elem.MATERIAL  ='0020305529';
            elem.MATERIAL_DESCRIPTION  ='0020305529';
            elem.CUST_PART_NUMBER  ='0020305529';
            elem.PO_NO  ='0020305529';
            elem.INVOICE_QTY  ='5529';
            elem.PRE_TAX ='5529';
            elem.POST_TAX = '5529';
            elem.TAX_VALUE  ='5529';
            elem.SALES_OFFICE = 'B0041';
            elem.SELLER_STATE ='Karnataka';
            elem.SELLER_TIN_NO ='100457';
            elem.MBLNR = '1191';
            elem.BUDAT = '2015-01-01';
            
            emList.add(elem);
            
            SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES zws = new SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES();
            zws.item = emList;
            
            SAP_ZWS_CFORM.ZSFDC_CFORM_SALESResponse_element elem1 = new SAP_ZWS_CFORM.ZSFDC_CFORM_SALESResponse_element();
            elem1.IT_FINAL = zws;
            
            response.put('response_x', elem1);
         
            return;
        }     
    }
    // Test Method
     public static testMethod void showSalesValue(){      
       
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        CFormDetails cf = new CFormDetails();
       
        List<CFormDetails.CFormMapping> cList = CFormDetails.getAllCFormDetails('51000460','2015-02-24','2015-04-24');        
     }
}