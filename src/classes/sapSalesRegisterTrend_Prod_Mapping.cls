global class sapSalesRegisterTrend_Prod_Mapping {

  /*
* Author  :- Supriya Chakrapani 
* Purpose :- Fetch Sales register trends Monthwise 
* Date    :- 14/Sept/2015
*
*/
    
    //-- ATTRIBUTES    
   public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    //-- CONSTRUCTOR
    
    //-- Methods
    
   WebService static List<SalesRegisterTrend_Prod_Mapping> getAllSalesRegisterTread(String Fdate,String Tdate,String custID){
         
         String customerId                   = UtilityClass.addleadingZeros(custID,Integer.valueOf(CUST_ID_LENGTH)); 

        List<SalesRegisterTrend_Prod_Mapping> salesRegisterTread = new List<SalesRegisterTrend_Prod_Mapping>();        
        try{
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
                        
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            sapSalesRegisterTrend_Prod tst                    = new sapSalesRegisterTrend_Prod();
            sapSalesRegisterTrend_Prod.ZWS_ZSFDC_SALES_REGISTER_GROUP zws  = new sapSalesRegisterTrend_Prod.ZWS_ZSFDC_SALES_REGISTER_GROUP();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 120000;
            
            sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER tom      = new sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER();
            sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER e      = new sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER();
            List<sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER> m             = new List<sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER>();        
            

            tom.item = m;
            
            //sapSalesRegisterTread.ZSFDC_SALES_REGISTER_GROUPResponse_element e = new sapSalesRegisterTread.ZSFDC_SALES_REGISTER_GROUPResponse_element();
            
            e = zws.ZSFDC_SALES_REGISTER_GROUP(Fdate, tom,customerid,Tdate);
            //String Bzirk,SapUnrestrictedStockSoap.TableOfZbapiMatStock Itab,String Matkl,String Matnr,SapUnrestrictedStockSoap.TableOfBapireturn Return_x
            system.debug(e+'dddddd');
            if(e.item != null){
                for(sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER z : e.item){
                    salesRegisterTread.add(new SalesRegisterTrend_Prod_Mapping(customerid,z.MATKL,z.WGBEZ,z.FKIMG,z.NETWR,z.MONTH,z.YEAR));
                }
            }
             
            system.debug('salesRegister----------------'+salesRegisterTread.size());        
            return salesRegisterTread;
            }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
            }
        }
    
    
     global class SalesRegisterTrend_Prod_Mapping{       
        
        public String KUNNR;
        //public String NAME1;
        public String MATKL;
        public String WGBEZ;
        public String FKIMG;
        public String NETWR;
        public String MONTH;
        public String YEAR;
       
        public SalesRegisterTrend_Prod_Mapping(String KUNNR, String MATKL, String WGBEZ, String FKIMG, String NETWR, String MONTH, String YEAR){
            this.KUNNR = KUNNR;
            //this.NAME1 = NAME1;             
            this.MATKL = MATKL; 
            this.WGBEZ = WGBEZ;
            this.FKIMG = FKIMG;
            this.NETWR = NETWR;
            this.MONTH = MONTH;     
            this.YEAR  = YEAR;
            
            
        }
    }
    
 
}