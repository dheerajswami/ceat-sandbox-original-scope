global class scheduledBatch_Export implements Schedulable{
   @TestVisible private integer batchsize = 1; 
   
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest())
      {
           batchsize = 2000;
      } 
      SP_BatchForExportBU_Updated b = new SP_BatchForExportBU_Updated(); 
      database.executebatch(b,batchsize);
   }
}