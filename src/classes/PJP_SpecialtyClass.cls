global with sharing class PJP_SpecialtyClass { 
    public static final String salesforceBaseUrl {get;set;}

    static {
        salesforceBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    }
     
    public PJP_SpecialtyClass() {}
    
    @RemoteAction
    global static WrapperPjpDetails getQuarterMonth(String gotMonth, String gotYear, String gotOwner) {
        Date todayDate = Date.today();
        String yearString;
        List<ACW__c> window = new List<ACW__c>();
        WrapperPjpDetails wpd = new WrapperPjpDetails();
        if(gotMonth == null) {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Specialty' And Active__c = true And From_Date__c <= :todayDate And To_Date__c >= :todayDate];
            
            if(window.size() == 0) {
                wpd.monthString = calculateMonth(todayDate.month());
                wpd.monthInt = todayDate.month();
            } else {
                wpd.monthString = window[0].Month_Text__c;
                wpd.monthInt = Integer.valueOf(window[0].Month__c);                
            }
        } else {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Specialty' And Active__c = true And Month_Text__c = :gotMonth And Year__c = :gotYear]; 

            wpd.monthString = window[0].Month_Text__c;
            wpd.monthInt = Integer.valueOf(window[0].Month__c);
        }
        wpd.quarterString = calculateQuarter(wpd.monthString);
        return wpd;
    }
    
    @RemoteAction
    global static WrapperPjpDetails getWriteAccess(String gotMonth, String gotYear, String gotOwner) {
        if(gotOwner != null)
            gotOwner = Id.valueOf(gotOwner);                
        
        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();
        WrapperPjpDetails wpd = new WrapperPjpDetails();                        

        Date todayDate = Date.today();
        String yearString;
        PJP__c newPJP;

        wpd.monthString = gotMonth;        
        wpd.monthInt = calculateMonthInt(gotMonth);
        
        List<ACW__c> window = new List<ACW__c>();        
        window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
            where Page__c = 'PJP Specialty' And Active__c = true And From_Date__c <= :todayDate And To_Date__c >= :todayDate]; //Month_Text__c = :gotMonth
        
        if(window.size() > 0) {
            wpd.yearInt = Integer.valueOf(window[0].Year__c);
            if(window[0].Month__c <= wpd.monthInt) {
                wpd.isReadOnly = false;
            } else {
                wpd.isReadOnly = true;
            }
        } else {
            wpd.yearInt = todayDate.year();
            wpd.isReadOnly = true;
        }
        
        
        yearString = String.valueOf(wpd.yearInt);
        wpd.quarterString = calculateQuarter(wpd.monthString);
        
        if(gotOwner == null) {
            wpd.ownerId = UserInfo.getUserId();
        } else {
            wpd.ownerId = gotOwner;
        }
        
        if(wpd.ownerId != UserInfo.getUserId()){
            wpd.isReadOnly = true;
        }        
        
        try {            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c
                where Visit_Plan_Month__c = :wpd.monthString And Visit_Plan_Year__c = :yearString And OwnerId = :wpd.ownerId limit 1];
            wpd.pjpId = newPJP.Id;
            wpd.pjpComments = newPJP.Comments__c;
            if(newPJP.OwnerId != UserInfo.getUserId() || newPJP.Sys_ApprovalStatus__c == 'Submitted' || newPJP.Sys_ApprovalStatus__c == 'Approved') {
                wpd.isReadOnly = true;
            }
            if(newPJP.PJP_Norms__c != null) {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                               FROM PJP_Norms_Main__c where Id = :newPJP.PJP_Norms__c];
            } else {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                               FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = 'Specialty' Limit 1];
            }            
        } catch(Exception e) {
            activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                           FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = 'Specialty' Limit 1];
        }    
        for(PJP_Norms_Main__c pnm : activeNorms) {            
            wpd.isOE = pnm.Is_OE_Days__c;
            wpd.isMOR = pnm.Is_MOR__c;
            wpd.isCompAct = pnm.Is_Company_Activities__c;
            wpd.oeDays = Integer.valueOf(pnm.OE_Days__c);
            wpd.morDays = Integer.valueOf(pnm.MOR__c);
            wpd.compActDays = Integer.valueOf(pnm.Company_Activities__c);   
            wpd.workshopDays = Integer.valueOf(pnm.No_Of_Workshops__c);
        }
        return wpd;
    }
    
    @RemoteAction
    global static List<WrapperVisitPlanDays> getRelatedAccounts(Integer yr, Integer mn, String mnth, String quar, String ownerId) {
                  
        List<AggregateResult> allAccounts = new List<AggregateResult>(); 
        List<String> deType = new List<String>();        
        List<WrapperVisitPlanDays> tableList = new List<WrapperVisitPlanDays>();        
        Map<Integer,Holiday__c>  dlMap = new Map<Integer,Holiday__c>();
        Set<Integer>  hlList = new Set<Integer>(); 
        Integer en =  date.daysInMonth(yr, mn);
        List<DayList> listOfWeekndsAndHoliday;
        Integer day29;
        Integer day30;
        Integer day31;
        Set<Id> accountIdSet = new Set<id>();
        List<AvailableDay> availableDaysList;
       
        List<Visits_Plan__c> visitplanList = [Select Check_In__c, Dealer__c, Dealer__r.Name, Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                              from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c =: String.valueOf(yr) And Master_Plan__r.OwnerId =: ownerId];
        List<PJP__c> newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c
                where Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :String.valueOf(yr) And OwnerId = :ownerId limit 1];
        
        User ownerUser = [Select Id, Name, EmployeeNumber, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :ownerId];        

        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();
        if(newPJP.size() > 0 && newPJP[0].PJP_Norms__c != null) {
            activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                           FROM PJP_Norms_Main__c where Id = :newPJP[0].PJP_Norms__c];
        } else {
            activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                           FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = 'Specialty' Limit 1];
        }
        
        UserTerritory2Association userTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:ownerId Limit 1];
        //Territory2 userTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerrAss.Territory2Id Limit 1];
        Territory2 userRegion = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerrAss.Territory2Id Limit 1];
        Territory2 userZone = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerrAss.Territory2Id Limit 1];
        List<Territory2> childRegions = [select id,Description,Name from Territory2 where ParentTerritory2Id = :userZone.Id];
        
        Set<String> regions = new Set<String>();
        for(Territory2 t : childRegions) {
            regions.add(t.Name);
        }

        List<AccountShare> accShare = [Select AccountId from AccountShare where UserOrGroupId = :ownerUser.Id];
        Set<Id> accIds = new Set<Id>();
        
        for(AccountShare ash : accShare) {
            accIds.add(ash.AccountId);
        }
        
        if(visitplanList.size() > 0) {
            for(Visits_Plan__c vp : visitplanList) {
                if(!accountIdSet.contains(vp.Dealer__c)) {
                    accountIdSet.add(vp.Dealer__c);       
                } 
            }
        }
        
        for(Holiday__c hol : [Select Name, Active__c, Date__c, Name_of_Holiday__c, Description__c, Employee_No__c, Start_Date__c, End_Date__c, RecordType.Name From Holiday__c where (((RecordType.Name = 'National' Or (RecordType.Name = 'Regional' And Region__c = :userRegion.Name)) And Date__c >= :date.newinstance(yr,mn,1) And Date__c <= :date.newinstance(yr,mn,en)) Or (RecordType.Name = 'Planned Leaves' And Employee_No__c = :ownerUser.EmployeeNumber))]){
            if(hol.RecordType.Name == 'Planned Leaves') {               
                for(Date d = hol.Start_Date__c; d <= hol.End_Date__c; d = d.addDays(1)) {                   
                    if(d.month() == Date.newInstance(yr, mn, 1).month()) {
                        dlMap.put(d.day(), hol);
                    }
                } 
                            
            } else {
                dlMap.put(hol.Date__c.day(), hol);
            }            
        }
        
        List<String> accountRecordTypes = 'Specialty Distributor,Specialty Dealer,Institutional Sales,Prospect,Specialty Plant'.split(',');
        System.debug('AccIds   '+accIds);
        
        List<String> customerGroups = 'DL,MB,CS,SI,5S,DC,C2'.split(',');
        System.debug('customerGroups   swayam'+customerGroups);
        allAccounts = [Select Id, Name, Type, KUNNR__c, RecordType.Name RtName, Town__c from Account where Sales_Group_Text__c in :regions And (Type in :accountRecordTypes OR Customer_Group__c in :customerGroups) /*And Active__c = true*/ group by Type, Town__c, Name, Id, RecordType.Name, KUNNR__c order by Town__c, Name];
        
        if(allAccounts.size() > 0){
            List<String> weekendList = System.Label.PJP_Weekends.split(',');
            if(weekendList.size() == 0) {
                weekendList.add('Sunday');
            }
            listOfWeekndsAndHoliday = new List<DayList>();
            for(Integer i=1; i<=en; i++) {                        
                DayList dl = new DayList();
                DateTime dt=DateTime.newInstance(date.newInstance(yr,mn,i), Time.newInstance(0, 0, 0, 0));
                String dayOfWeek = dt.format('EEE');

                if(dlMap.containsKey(i)) {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'PublicHoliday';
                        dl.holReason = '';
                        if(dlMap.containsKey(i)) {
                            dl.holReason = dlMap.get(i).Name;
                        }
                    }                        
                } else {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'Normal'; 
                    }  
                }
                
                listOfWeekndsAndHoliday.add(dl);
            }

            if(en == 29) {
                day29 = 29;
            } else if(en == 30) {
                day30 = 30;
            } else if(en == 31) {
                day31 = 31;
            }     

            Map<Id, Integer> gotQuarVisits = getQuarterVisits(yr, quar, ownerId);            

            for(AggregateResult  d : allAccounts)
            {                
                Integer target;
                availableDaysList = new List<AvailableDay>(35);
                                                                                              
                for(PJP_Norms_Main__c pnm : activeNorms) {
                    for(PJP_Segment_Norm__c psn : pnm.PJP_Segment_Norm__r) {
                        if(psn.Segment_Name__c == d.get('Type')) {                            
                            target = Integer.valueOf(psn.Value__c);
                        }
                    }
                }                                                              
                                
                for(integer i = 0; i < 32; i++) {
                    AvailableDay avail = new AvailableDay(i);
                    avail.value = i;
                    avail.dateAvailable = false;
                    avail.planVisitId = null;
                    availableDaysList.set(i,avail);                    
                }
                
                if(availableDaysList.size() > 0) {
                    for(Visits_Plan__c vp : visitplanList) {                         
                        if((d.id == vp.Dealer__c)) {
                            Date d1 = vp.Visit_Day__c;
                            AvailableDay availInstance = new AvailableDay(d1.day());
                            availInstance.value = d1.day();
                            availInstance.typeOfDay = vp.Type_of_Day__c;
                            availInstance.dateAvailable= true;
                            availInstance.planVisitId = vp.id;
                            availableDaysList.set(availInstance.value-1,availInstance);                
                        }
                    } 
                }
                Integer quarPlannedVisits = 0;
                if(gotQuarVisits != null) {
                    if(gotQuarVisits.containsKey(String.ValueOf(d.get('Id')))) {
                        quarPlannedVisits = gotQuarVisits.get(String.ValueOf(d.get('Id'))); 
                    }               
                } 

                String accType = '';
                if(d.get('Type') == null) {             
                    accType = 'General Dealer';
                } else {
                accType = String.ValueOf(d.get('Type'));
                }

                tableList.add(new WrapperVisitPlanDays(String.ValueOf(d.get('Id')),String.ValueOf( d.get('Name')),String.ValueOf(d.get('KUNNR__c')), accType, String.ValueOf(d.get('RtName')), String.ValueOf(d.get('Town__c')), 'Visit Day', target, listOfWeekndsAndHoliday, day29, day30, day31, availableDaysList, quarPlannedVisits));
            }       
            
            List<AvailableDay> morAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> oeAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> compActAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> workshopVisits = new List<AvailableDay>(35);
                        
            for(integer i = 0; i < 32; i++) {
                AvailableDay avail = new AvailableDay(i);
                avail.value = i;
                avail.dateAvailable = false;
                avail.planVisitId = null;
                morAvailableDays.set(i,avail);
                oeAvailableDays.set(i,avail);
                compActAvailableDays.set(i,avail);  
                workshopVisits.set(i,avail);  
            }

            Map<String, Integer> gotQuarActi = getQuarterActivities(yr, quar, ownerId, mnth);            
            for(Visits_Plan__c vp : visitplanList) {
                if(vp.Type_of_Day__c != 'Visit Day') {
                    Date d1 = vp.Visit_Day__c;
                    AvailableDay availInstance = new AvailableDay(d1.day());
                    availInstance.value = d1.day();
                    availInstance.typeOfDay = vp.Type_of_Day__c;
                    availInstance.dateAvailable= true;
                    availInstance.planVisitId = vp.id;

                    if(vp.Type_of_Day__c == 'MOR') {                                        
                        morAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'OE') {
                        oeAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Company Activity') {
                        compActAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Workshop') {
                        workshopVisits.set(availInstance.value-1, availInstance);
                    }
                }
            }
            Integer quarPlannedMor = 0, 
                    quarPlannedOe = 0,
                    quarPlannedCompAct = 0,
                    quarPlannedWShop = 0;
            if(gotQuarActi != null) {
                if(gotQuarActi.containsKey('MOR'))
                    quarPlannedMor = gotQuarActi.get('MOR');
                if(gotQuarActi.containsKey('OE'))
                    quarPlannedOe = gotQuarActi.get('OE');
                if(gotQuarActi.containsKey('Company Activity'))
                    quarPlannedCompAct = gotQuarActi.get('Company Activity');   
                if(gotQuarActi.containsKey('Workshop'))
                    quarPlannedWShop = gotQuarActi.get('Workshop');             
            }
            
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'MOR', null, listOfWeekndsAndHoliday, day29, day30, day31, morAvailableDays, quarPlannedMor));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'OE', null, listOfWeekndsAndHoliday, day29, day30, day31, oeAvailableDays, quarPlannedOe));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'Company Activity', null, listOfWeekndsAndHoliday, day29, day30, day31, compActAvailableDays, quarPlannedCompAct));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'Workshop', null, listOfWeekndsAndHoliday, day29, day30, day31, workshopVisits, quarPlannedWShop));            
        }        
        return tableList; 
    } 
    
    @RemoteAction
    global static String saveVisitPlans(List<Visits_Plan__c> visitplanList, Integer yr, Integer mn, String mnth, String buttonClicked, String comments) {        
        User currentUser = [Select Id, Name, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :UserInfo.getUserId()];        
        RecordType rtype = [Select Id, Name, DeveloperName from RecordType where DeveloperName = 'Specialty' And SobjectType = 'PJP__c'];
        
        List<RecordType> rtList = [Select Id, Name, DeveloperName from RecordType where SobjectType = 'Visits_Plan__c'];
        Id rtCompAct = null;
        Id rtWorkshop = null;        
        Id rtVisit = null;
        for(RecordType rt : rtList) {
            if(rt.Name == 'MOR-OE-CompAct') {
                rtCompAct = rt.Id;
            } else if(rt.Name == 'Workshop') {
                rtWorkshop = rt.Id;            
            } else if(rt.Name == 'Visit') {
                rtVisit = rt.Id;
            }
        }

        PJP__c newPJP ;
        Date visitPlanDate;
        List<Visits_Plan__c> visitplantoupsert = new List<Visits_Plan__c>();
        List<PJP_Norms_Main__c> activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,Visits__c, Active__c, No_Of_Workshops__c
                           FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = 'Specialty' Limit 1];
        try{            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c from PJP__c where RecordTypeId = :rtype.Id And Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :String.valueOf(yr) And OwnerId = :UserInfo.getUserId() limit 1];
            if(newPJP.PJP_Norms__c == null) {
                if(activeNorms.size() > 0) {
                    newPJP.PJP_Norms__c = activeNorms[0].Id;
                }
            }
            newPJP.Comments__c = comments;
            update newPJP;
            PJP_CalculateSummary.calculateSummary(newPJP.Id);            
        }catch(Exception e){            
            newPJP = new PJP__c();
            if(activeNorms.size() > 0) {
                newPJP.PJP_Norms__c = activeNorms[0].Id;
            }
            newPJP.Visit_Plan_Month__c = mnth;
            newPJP.Visit_Plan_Year__c = String.valueOf(yr);                        
            newPJP.Visit_Plan_Date_del__c = Date.newInstance(yr, mn, 1);        
            newPJP.Comments__c = comments; 
            newPJP.Owner_s_Manager__c = currentUser.ManagerId; 
            newPJP.RecordTypeId = rtype.Id;  
            List<UserTerritory2Association> listUserTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:currentUser.Id];
            Set<Id> terrIds = new Set<Id>();
            for(UserTerritory2Association uta : listUserTerrAss) {
                terrIds.add(uta.Territory2Id);
            }
            List<Territory2> listUserTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id in :terrIds];
            for(Integer i=0; i<listUserTerritory.size(); i++) {
                if(i == 0) {
                    newPJP.Territory__c = listUserTerritory[i].Name;
                } else {
                    newPJP.Territory__c += '; ' + listUserTerritory[i].Name;
                }                
            }
            insert newPJP;      
            PJP_CalculateSummary.calculateSummary(newPJP.Id);      
        }
        
        for(Visits_Plan__c temp1: visitplanList){            
            temp1.Visit_Day__c = Date.newinstance(Integer.ValueOf(yr),Integer.ValueOf(mn),Integer.ValueOf(temp1.Day__c));
            temp1.Master_Plan__c = newPJP.Id;   
            temp1.Status__c = 'Scheduled';
            if(temp1.Type_of_Day__c == 'MOR' || temp1.Type_of_Day__c == 'OE' || temp1.Type_of_Day__c == 'Company Activity') {                        
                temp1.RecordTypeId = rtCompAct;
            } else if(temp1.Type_of_Day__c == 'Workshop') {
                temp1.RecordTypeId = rtWorkshop;
            } else {
                temp1.RecordTypeId = rtVisit;
            }

            visitplantoupsert.add(temp1);            
        }
        boolean flag = false;
        if(visitplantoupsert.size() > 0) {
            try {

                List<Visits_Plan__c> previousVisitPlans = [Select Id from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c =: String.valueOf(yr) And Master_Plan__r.OwnerId =: UserInfo.getUserId()];
                List<Visits_Plan__c> deleteVisitPlans = new List<Visits_Plan__c>();

                for(Visits_Plan__c prevVp : previousVisitPlans) {
                    Boolean idFound = false;
                    for(Visits_Plan__c newVp : visitplantoupsert) {
                        if(prevVp.Id == newVp.Id) {
                            idFound = true;
                            break;
                        }
                    }
                    if(!idFound) {
                        deleteVisitPlans.add(prevVp);
                    }
                }

                delete deleteVisitPlans;
                upsert visitplantoupsert;
                         
                if(buttonClicked == 'Submit') {
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting request for approval.');
                    req1.setObjectId(newPJP.Id);                                    
                    Approval.ProcessResult result = Approval.process(req1);
                    FeedItem fitem=new FeedItem();
                    fItem.parentId=currentUser.ManagerId;
                    fItem.Title='Click here to Approve/Reject';
                    fItem.body = 'PJP has been submitted for the month '+newPJP.Visit_Plan_Month__c+' by '+currentUser.Name+'. ';
                    fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newPJP.Id;
                    insert fItem;
                    //User currentUser = [Select Id, Name, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :UserInfo.getUserId()];        
                    //String[] toAddresses = new String[] {currentUser.Manager.Email}; 
                    //sendemail(newPJP.Id, toAddresses);
                }
                       
                flag = true;
            } catch(Exception e) {
                system.debug(e.getMessage());
            }
        }
        return buttonClicked+'; '+newPJP.Id;
    }
       
    public static void sendemail(Id gotId, String[] toAddresses) {        
        //String[] toAddresses = new String[] {'swayam.arora@extentor.com'}; 
        System.debug('Swayam Email '+toAddresses);
        EmailTemplate emailTemplateInstance = [select Id from EmailTemplate where Name='Report Test'];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplateInstance.Id);
        email.setToAddresses(toAddresses);
        email.setTargetObjectId(UserInfo.getUserId());
        //email.setWhatId(gotId);
        email.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.Email[] { email } );            
        System.debug('Swayam Email2 '+toAddresses);
    }  
    
    public static Map<Id, Integer> getQuarterVisits(Integer yr, String quar, String ownerId) {
        
        List<String> quarMonths = new List<String>();
        if(quar == 'Q1') {
            quarMonths.add('April');
            quarMonths.add('May');
            quarMonths.add('June');
        } else if(quar == 'Q2') {
            quarMonths.add('July');
            quarMonths.add('August');
            quarMonths.add('September');
        } else if(quar == 'Q3') {
            quarMonths.add('October');
            quarMonths.add('November');
            quarMonths.add('December');
        } else if(quar == 'Q4') {
            quarMonths.add('January');
            quarMonths.add('February');
            quarMonths.add('March');
        }

        List<Visits_Plan__c> visitplanList = [Select Check_In__c, Dealer__c, Dealer__r.Name, Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                              from Visits_Plan__c where Type_of_Day__c = 'Visit Day' And Master_Plan__r.Visit_Plan_Month__c in :quarMonths And Master_Plan__r.Visit_Plan_Year__c =: String.valueOf(yr) And Master_Plan__r.OwnerId =: ownerId];

        Map<Id, Integer> dealerPlannedQuar;
        
        if(visitplanList.size() > 0) {
            dealerPlannedQuar = new Map<Id, Integer>();
            for(Visits_Plan__c vp : visitplanList) {
                if(dealerPlannedQuar.containsKey(vp.Dealer__c)) {
                    dealerPlannedQuar.put(vp.Dealer__c, dealerPlannedQuar.get(vp.Dealer__c) + 1);
                } else {
                    dealerPlannedQuar.put(vp.Dealer__c, 1);
                }
            }
        }                              

        return dealerPlannedQuar;
    }

    public static Map<String, Integer> getQuarterActivities(Integer yr, String quar, String ownerId, String gotMonth) {
        
        List<String> quarMonths = new List<String>();
        if(quar == 'Q1') {
            quarMonths.add('April');
            quarMonths.add('May');
            quarMonths.add('June');
        } else if(quar == 'Q2') {
            quarMonths.add('July');
            quarMonths.add('August');
            quarMonths.add('September');
        } else if(quar == 'Q3') {
            quarMonths.add('October');
            quarMonths.add('November');
            quarMonths.add('December');
        } else if(quar == 'Q4') {
            quarMonths.add('January');
            quarMonths.add('February');
            quarMonths.add('March');
        }

        List<Visits_Plan__c> visitplanList = [Select Check_In__c, Dealer__c, Dealer__r.Name, Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                              from Visits_Plan__c where Type_of_Day__c != 'Visit Day' And Master_Plan__r.Visit_Plan_Month__c = :gotMonth And Master_Plan__r.Visit_Plan_Year__c =: String.valueOf(yr) And Master_Plan__r.OwnerId =: ownerId];

        Map<String, Integer> activityPlannedQuar;
        
        if(visitplanList.size() > 0) {
            activityPlannedQuar = new Map<String, Integer>();
            for(Visits_Plan__c vp : visitplanList) {
                if(activityPlannedQuar.containsKey(vp.Type_of_Day__c)) {
                    activityPlannedQuar.put(vp.Type_of_Day__c, activityPlannedQuar.get(vp.Type_of_Day__c) + 1);
                } else {
                    activityPlannedQuar.put(vp.Type_of_Day__c, 1);
                }
            }
        }                              

        return activityPlannedQuar;
    }

    public static String calculateQuarter(String mnth) {
        if(mnth == 'January' || mnth == 'February' || mnth == 'March') {
            return 'Q4';
        } else if(mnth == 'April' || mnth == 'May' || mnth == 'June') {
            return 'Q1';
        } else if(mnth == 'July' || mnth == 'August' || mnth == 'September') {
            return 'Q2';
        } else {
            return 'Q3';
        }
    }

    public static String calculateMonth(Integer mm) {
        if(mm == 1) 
            return 'January';
        if(mm == 2) 
            return 'February';
        if(mm == 3) 
            return 'March';
        if(mm == 4) 
            return 'April';
        if(mm == 5) 
            return 'May';
        if(mm == 6) 
            return 'June';
        if(mm == 7) 
            return 'July';
        if(mm == 8) 
            return 'August';
        if(mm == 9) 
            return 'September';
        if(mm == 10) 
            return 'October';
        if(mm == 11) 
            return 'November';
        if(mm == 12) 
            return 'December';

        return '';
    }

    public static Integer calculateMonthInt(String mnth) {
        if(mnth == 'January') 
            return 1;
        if(mnth == 'February') 
            return 2;
        if(mnth == 'March') 
            return 3;
        if(mnth == 'April') 
            return 4;
        if(mnth == 'May') 
            return 5;
        if(mnth == 'June') 
            return 6;
        if(mnth == 'July') 
            return 7;
        if(mnth == 'August') 
            return 8;
        if(mnth == 'September') 
            return 9;
        if(mnth == 'October') 
            return 10;
        if(mnth == 'November') 
            return 11;
        if(mnth == 'December') 
            return 12;

        return 0;
    }

    //Wrapper Class

    public class DayList{
        public Integer dayNumber {get;set;}
        public String typeCol {get;set;}
        public String holReason {get;set;}
    }

    public class AvailableDay {
        public integer value {get;set;}
        public boolean dateAvailable {get;set;}
        public id planVisitId {get;set;}
        public String typeOfDay{get;set;}
        
        public AvailableDay(integer i) {
            value = i;
            dateAvailable = false;
            planVisitId = null;
            typeOfDay = null;
        }
    }

    global class WrapperPjpDetails {        
        public Boolean isOE {get;set;}
        public Boolean isMOR {get;set;}
        public Boolean isCompAct {get;set;}
        public Integer oeDays {get;set;}
        public Integer morDays {get;set;}
        public Integer compActDays {get;set;}        
        public String monthString {get;set;}
        public Integer yearInt {get;set;}
        public Integer monthInt {get;set;}
        public String pjpComments {get;set;}
        public String pjpId {get;set;}
        public String ownerId {get;set;}
        public Boolean isReadOnly {get;set;}
        public String quarterString {get;set;}        
        public Integer workshopDays {get;set;}        
    }
    
    global class WrapperVisitPlanDays {
        public String accountId{get;set;}
        public String accountName{get;set;}
        public String accountSapCode{get;set;}
        public String accountType{get;set;}
        public String accountRecordType{get;set;}
        public String accountTown{get;set;}
        public String typeOfDay{get;set;}          
        public List<DayList> dayMon{get;set;}
        public Integer day1{get;set;}
        public Integer day2{get;set;}
        public Integer day3{get;set;}
        public Integer day4{get;set;}
        public Integer day5{get;set;}
        public Integer day6{get;set;}
        public Integer day7{get;set;}
        public Integer day8{get;set;}
        public Integer day9{get;set;}
        public Integer day10{get;set;}
        public Integer day11{get;set;}
        public Integer day12{get;set;}
        public Integer day13{get;set;}
        public Integer day14{get;set;}
        public Integer day15{get;set;}
        public Integer day16{get;set;}
        public Integer day17{get;set;}
        public Integer day18{get;set;}
        public Integer day19{get;set;}
        public Integer day20{get;set;}
        public Integer day21{get;set;}
        public Integer day22{get;set;}
        public Integer day23{get;set;}
        public Integer day24{get;set;}
        public Integer day25{get;set;}
        public Integer day26{get;set;}
        public Integer day27{get;set;}
        public Integer day28{get;set;}
        public Integer day29{get;set;}
        public Integer day30{get;set;}
        public Integer day31{get;set;}
        public List<AvailableDay> availableDayList{get;set;}        
        public Integer targetDays{get;set;}
        public Integer plannedDays{get;set;}
        public Integer quarPlannedVisits{get;set;}
        
        public WrapperVisitPlanDays(String dId, String dName, String sapCode, String aType, String recType, String accTown, String tod, Integer tDays, List<DayList> dt, Integer day29, Integer day30, Integer day31, List<AvailableDay> availableDaysVar, Integer quarVisits)
        {
            dayMon = new List<DayList>();
            accountId = dId;
            accountName = dName;
            accountSapCode = sapCode;
            accountType = aType;
            accountRecordType = recType;
            accountTown = accTown;          
            typeOfDay = tod;         
            targetDays = tDays;
            dayMon = dt;
            day1 = 1;
            day2 = 2;
            day3 = 3;
            day4 = 4;
            day5 = 5;
            day6 = 6;
            day7 = 7;
            day8 = 8;
            day9 = 9;
            day10 = 10;
            day11 = 11;
            day12 = 12;
            day13 = 13;
            day14 = 14;
            day15 = 15;
            day16 = 16;
            day17 = 17;
            day18 = 18;
            day19 = 19;
            day20 = 20;
            day21 = 21;
            day22 = 22;
            day23 = 23;
            day24 = 24;
            day25 = 25;
            day26 = 26;
            day27 = 27;
            day28 = 28;
            this.day29 = day29;
            if(day30 == 30) {
                this.day29 = 29;
                this.day30 = day30;
            } if(day31 == 31) {
                this.day29 = 29;
                this.day30 = 30;
                this.day31 = day31;
            }
            availableDayList = new List<AvailableDay>();
            availableDayList = availableDaysVar;
            quarPlannedVisits = quarVisits;
        }
    }
}