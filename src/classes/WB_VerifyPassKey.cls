/*+----------------------------------------------------------------------
||         Author:  Kishlay Mathur
||
||        Purpose:To Verify PassKey of the customer
||        version: Included for employee as well  
||        Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/VerifyPassKey/*')

global with sharing class WB_VerifyPassKey {
    global class customerDetails{
            public String yORn;
            public String preferred_lang;
            public String sfdcCustomer_Code;
            Public String passKey;
            public String CustomerName;
            public String sfdcBaseURL;
            
    }
    
    @HttpGet
    global static customerDetails getCustomerDetails(){
        RestRequest req           = RestContext.request;
        RestResponse res          = RestContext.response;
        String reference_Code      = req.params.get('reference_Code');
        String passKey   = req.params.get('passKey');
        String flag               = req.params.get('flag');
        system.debug(reference_Code+'$$$$$'+passKey+'!!!!!!'+flag+'%%%%');
        try{
           if(flag != null && flag == 'D'){
               Account account = [SELECT Id,Name,Preferred_Language__c,Passcode__c FROM Account WHERE KUNNR__c =:reference_Code];
               system.debug(account.Passcode__c+'########'+passKey);
               if(account.Passcode__c == passKey){
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.yORn = 'Y';
                   cusdetails.preferred_lang = account.Preferred_Language__c;
                   cusdetails.sfdcCustomer_Code = account.id;
                   cusdetails.passKey= account.Passcode__c;
                   cusdetails.CustomerName =account.Name;
                   cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                   
                   return cusdetails ; 
               }else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.yORn = 'N';
                   cusdetails.preferred_lang = '';
                   cusdetails.sfdcCustomer_Code = '';
                   cusdetails.passKey= '';
                   cusdetails.CustomerName='';
                   cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                   return cusdetails ; 
               }
           }else if(flag != null && flag == 'E'){
                Employee_Master__c employee = [SELECT id,Preferred_Language__c,Name,Passcode__c,EmployeeId__c FROM Employee_Master__c WHERE EmployeeId__c =: reference_Code];
                if(employee.Passcode__c == passKey){
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.yORn = 'Y';
                   cusdetails.preferred_lang = employee.Preferred_Language__c;
                   cusdetails.sfdcCustomer_Code = employee.id;
                   cusdetails.passKey= employee.Passcode__c;
                   cusdetails.CustomerName= employee.Name;
                   cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                   
                   return cusdetails ; 
                }else{
                  customerDetails cusdetails = new customerDetails();
                   cusdetails.yORn = 'N';
                   cusdetails.preferred_lang = '';
                   cusdetails.sfdcCustomer_Code = '';
                   cusdetails.passKey= '';
                   cusdetails.CustomerName='';
                   cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                   return cusdetails ; 
                }   
           }else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.yORn = 'N';
                   cusdetails.preferred_lang = '';
                   cusdetails.sfdcCustomer_Code = '';
                   cusdetails.passKey= '';
                   cusdetails.CustomerName='';
                   cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
                   return cusdetails ; 
           }
           
        }catch(Exception e){
               system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
               customerDetails cusdetails = new customerDetails();
               cusdetails.yORn = 'N';
               cusdetails.preferred_lang = '';
               cusdetails.sfdcCustomer_Code = '';
               cusdetails.passKey= '';
               cusdetails.CustomerName='';
               cusdetails.sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm()+'/';
               return cusdetails ; 
        }
        
       
    }
}