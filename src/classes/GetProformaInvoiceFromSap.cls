global class GetProformaInvoiceFromSap {
    global GetProformaInvoiceFromSap() {
        
    }
 
    WebService static String CallWebServiceGetUnResStock(List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H> pInvoice_Header, List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I> pInvoice_Items){
        try {

            SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');
            String username                     = saplogin.username__c;
            String password                     = saplogin.password__c;
            Blob headerValue                    = Blob.valueOf(username + ':' + password);
            String authorizationHeader          = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            SapProformaInvoiceSoap ws = new SapProformaInvoiceSoap();    
            
            SapProformaInvoiceSoap.ZWS_SFDC_QUATATION_CREATE zws = new SapProformaInvoiceSoap.ZWS_SFDC_QUATATION_CREATE();
            zws.inputHttpHeaders_x = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x = 12000;
            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H tob = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H();
            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I tom = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I();        
            
            //List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H> b = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H>();
            //List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I> m = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I>();  
            
            //SapProformaInvoiceSoap.ZSFDC_QUATATION_H zh = new SapProformaInvoiceSoap.ZSFDC_QUATATION_H();
            //zh.AUART = 'ZEX0';
            //zh.VKORG = '1000';
            //zh.VTWEG = '07';
            //zh.SPART = '01';
            ////zh.VKGRP = '';
            ////zh.VKBUR = '';
            //zh.PRSDT = '2015-07-07';
            //zh.ANGDT = '2015-07-07';
            //zh.BNDDT = '2015-07-07';
            //zh.EDATU = '2015-07-15';
            //zh.WAERK = 'USD';
            //zh.DZTERM = 'ZW17';
            //zh.INCO1 = 'CFR';
            //zh.INCO2 = 'ALX';//'ALEXANDRA';
            //zh.KUNNR = '0056000039';
            //zh.ZNUMCNTNR = '02';
            //zh.ZIMPRTLCSREQ = 'N';
            //zh.ZPCKGREQ = 'WR';
            //zh.ZINSPREQ = 'N';
            //zh.ZINSPAGNCY = 'NA';
            //zh.ZSIZEOFCNTNR = 'HQ';
            //zh.ZCUSTCNTNR = 'N';
            //zh.ZPRCREF = 'CURR.';
            //zh.KUNNR1 = '0056000039';
            //zh.ZNOMSHP = 'N';
            //zh.ZMIXPI = 'Y';
            //zh.ZCLUBCNTNR = 'N';
            ////zh.ZLICSNUM = '';
            ////zh.ZADVPAYMNT = '';
            //b.add(zh);
            
            //SapProformaInvoiceSoap.ZSFDC_QUATATION_I zi = new SapProformaInvoiceSoap.ZSFDC_QUATATION_I();
            //zi.MATNR = '000000000000101485';
            //zi.WERKS = '1010';
            //zi.LGORT = 'FGSL';
            //zi.MENGE = '750';
            //m.add(zi);
            
            tob.item = pInvoice_Header;
            tom.item = pInvoice_Items;
            
            SapProformaInvoiceSoap.ZSFDC_QUATATION_CREATEResponse_element e = new SapProformaInvoiceSoap.ZSFDC_QUATATION_CREATEResponse_element();        
            e = zws.ZSFDC_QUATATION_CREATE(tob, tom);
    		System.debug('==#H   '+ e.IT_OUT_H);
    		System.debug('==#I   '+ e.IT_OUT_I);
            System.debug('==# RETURN_x   '+ e.RETURN_x);

            return e.RETURN_x;
            //System.debug('SwayamI   '+ e.IT_OUT_I);
            
            /*String unResStockExt = '';
            List<Un_Restricted_Stock__c> unResStock = new List<Un_Restricted_Stock__c>(); 
            
            if(e.Itab.item != null){
                for(SapProformaInvoiceSoap.ZSFDC_QUATATION_I z : e.Itab.item) {
                    unResStockExt = z.Matnr+z.Werks;
                    Material_Price__c matPrice = new Material_Price__c(Material_Number__c = z.Matnr);
                    Sales_District__c salesDis = new Sales_District__c(BZIRK__c = saleDis);
                    unResStock.add(new Un_Restricted_Stock__c(Lgobe__c = z.Lgobe,Maktx__c = z.Maktx,Matkl__c = z.Matkl,Wgbez__c = z.Wgbez,
                                                              Material_Price__r = matPrice,Bismt__c = z.Bismt,Werks__c = z.Werks,Lgort__c = z.Lgort,Labst__c =Decimal.Valueof(z.Labst),
                                                              Un_Restricted_Stock_Ext_ID__c = unResStockExt,Customer__c = z.Name1,Sales_District__r = salesDis));
                }
            }
            if(unResStock.size() > 0){
                upsert unResStock Un_Restricted_Stock_Ext_ID__c;
            }*/
        }
        catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
}