global with sharing class CPORT_CNDNWorksheet {

    // ATTRIBUTES --
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    public static WorkSheet workSheet {get;set;}


WebService static WorkSheet getAllDiscounts(String documentNo){
	workSheet = new WorkSheet();
	
	SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
	String username                   	= saplogin.username__c;
	String password                   	= saplogin.password__c;
	Blob headerValue = Blob.valueOf(username + ':' + password);
	String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);

    SapCNDN_WorkSheet   tst                         = new SapCNDN_WorkSheet();
    SapCNDN_WorkSheet.ZWS_SFDC_WORKSHEET   zws      = new SapCNDN_WorkSheet.ZWS_SFDC_WORKSHEET();
    
    SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_NEW PPD_NEW 				= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_NEW();
    SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_PRO PRODUCT_DISCOUNT     = new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_PRO();        
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_AD AD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_AD();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB DEBIT_NOTE_SLAB 	= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PLD PLD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PLD();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD PPD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_DEBIT PPD_DEBIT 			= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_PPD_DEBIT();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET SLAB 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET();
	SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_TLD TLD 						= new SapCNDN_WorkSheet.TABLE_OF_ZSFDC_WORKDHEET_TLD();
	SapCNDN_WorkSheet.ZSFDC_WORKSHEETResponse_element elm 					= new SapCNDN_WorkSheet.ZSFDC_WORKSHEETResponse_element();

	String DOCUEMENT_NO = UtilityClass.addleadingZeros(documentNo,Integer.valueOf(CUST_ID_LENGTH));

    zws.inputHttpHeaders_x                          = new Map<String,String>();
    zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
    zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
    zws.timeout_x                                   = 60000;
    
    elm = zws.ZSFDC_WORKSHEET(AD,DEBIT_NOTE_SLAB,DOCUEMENT_NO,PLD,PPD,PPD_DEBIT,PPD_NEW,PRODUCT_DISCOUNT,SLAB,TLD);



    if(elm.AD != null){
	   //system.debug(' OUTPUT AD' + elm.AD.item); 
    	workSheet.ad = elm.AD.item;
	}
	if(elm.PLD != null){
	   //system.debug(' OUTPUT PLD' + elm.PLD.item);
	   workSheet.pld = elm.PLD.item; 
	}
	if(elm.PPD != null){
	   //system.debug(' OUTPUT PPD' + elm.PPD.item);
	   workSheet.ppd = elm.PPD.item;
	}
	if(elm.SLAB != null){
	   //system.debug(' OUTPUT SLAB' + elm.SLAB.item);
		workSheet.slab = elm.SLAB.item;
	}
	if(elm.DEBIT_NOTE_SLAB  != null){
		workSheet.debit_slab = elm.DEBIT_NOTE_SLAB.item;
	}
	if(elm.PPD_DEBIT != null){
		workSheet.ppd_slab  = elm.PPD_DEBIT.item;
	}
	if(elm.TLD != null){
		workSheet.tld = elm.TLD.item;
	}
	if(elm.PPD_NEW != null){
		workSheet.ppd_new = elm.PPD_NEW.item;
	}
	if(elm.PRODUCT_DISCOUNT != null){
		workSheet.ppd_pro = elm.PRODUCT_DISCOUNT.item;
	}

	return workSheet;
}

 @RemoteAction
 global static WorkSheet getDiscounts(String documentNo){
    return getAllDiscounts(documentNo);
 }

// Mapping Class

global class WorkSheet{
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_AD> 	ad  				{get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD> 	ppd 				{get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PLD> 	pld 				{get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET> 		slab 				{get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_DEBIT_SLAB> debit_slab	{get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_DEBIT> ppd_slab	    {get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_TLD> 		tld 		    {get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_NEW> 		ppd_new     {get;set;}
	public List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_PRO> 		ppd_pro 	{get;set;}

	public WorkSheet(){
		ad 			= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_AD>();
		ppd 		= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD>();
		pld 		= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PLD>();
		slab 		= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET>();
		debit_slab 	= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_DEBIT_SLAB>();
		ppd_slab    = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_DEBIT>();
		tld 		= new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_TLD>(); 
		ppd_new     = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_NEW>();
		ppd_pro     = new List<SapCNDN_WorkSheet.ZSFDC_WORKDHEET_PPD_PRO>(); 
	}

}

}