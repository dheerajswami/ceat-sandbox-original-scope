@isTest(seeAllData = false)
public class CPORT_CustomerDetailController_Test{
    
    static Account acc ;
    static Contact con ;
    static Contact con1 ;
    static User portalUser ;
    
    
    public static void init(){
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        
        // Create Account
        acc = CEAT_InitializeTestData.createAccount('Test_Account','50003327');
        insert acc;
        
        // Create Contact
        con1 = CEAT_InitializeTestData.createContact('Test_Contact',acc.Id);
        insert con1;
        
        //Create  Portal User
        portalUser = CEAT_InitializeTestData.createPortalUserReplacement(con1.Id);
        insert portalUser;
        
    }
    
    public static testMethod void runDetailsAsPortalUser_Postitive(){ 
        init();  
        test.startTest(); 
        System.runAs(portalUser) {
            
            PageReference pageRef = Page.CustomerDetailsPortalPage;
            Test.setCurrentPage(pageRef);
            String testUrl = CPORT_CustomerDetailController.redirectToPortalPage();
            system.assertNotEquals(testUrl,null);
        }    
        test.stopTest();  
    }
    
}