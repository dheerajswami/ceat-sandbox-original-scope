public class AccountTriggerHandler{
    
    //-- SINGLETON PATTERN
    private static AccountTriggerHandler instance;
    public static AccountTriggerHandler getInstance() {
        if (instance == null) instance = new AccountTriggerHandler();
        return instance;
    }
    
    private AccountTriggerHandler(){}
    
    public void onBeforeInsert(final List<Account> newRecords){
        //retrieve record type id for customers and prospect
        Id prospectRecTypeId,custRecTypeId;
        prospectRecTypeId = UtilityClass.getRecordTypeId('Account','Prospect'); 
        custRecTypeId = UtilityClass.getRecordTypeId('Account','Customer'); 
        Prospect_Customer_Counter__c counterValues = new Prospect_Customer_Counter__c();
        
        //counterValues = [SELECT Id,customerCounter__c,ProspectCounter__c FROM Prospect_Customer_Counter__c LIMIT 1 FOR UPDATE ];
        counterValues = Prospect_Customer_Counter__c.getOrgDefaults();
        
        
        if(counterValues.customerCounter__c != null && counterValues.prospectCounter__c!=null){
            for(Account acc: newRecords){
                if(acc.RecordTypeId == prospectRecTypeId){
                    acc.Prospect_no__c = 'P-'+ string.valueof(Integer.valueOf(counterValues.ProspectCounter__c)) ;
                    counterValues.ProspectCounter__c++;
                }
                else if(acc.RecordTypeId == custRecTypeId){
                    acc.Customer_no__c = 'C-'+ string.valueof(Integer.valueOf(counterValues.CustomerCounter__c)) ;
                    counterValues.CustomerCounter__c++;
                }
                //system.assertEquals(acc.RecordTypeId,null);
            }
            update counterValues;
        }
        //system.assertEquals(counterValues,null);
        
    }
    /*
    @future(callout=true)
    public Static void onAfterInsert( set<id> accIDs){
        id exportRecTypeId = UtilityClass.getRecordTypeId('Account','Exports'); 
        map<String,Set<String>> mapofCustShipToCountry       = new Map<String,Set<String>>();
        list<Account> updatelistofCustomer              = new list<Account>();
        list<Account> newRecords                        = new list<Account>();
        set<String> setOFCountry;
        set<String> setOFCountrytemp;
        newRecords = [SELECT id,KUNNR__c,Ship_to_Country__c from Account Where id IN: accIDs AND RecordTypeId =: exportRecTypeId];
        if(newRecords.size() > 0){
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            for(Account acc: newRecords){
                sap_zws_cust_shtp  ws = new sap_zws_cust_shtp ();

                sap_zws_cust_shtp.ZWS_CUST_SHTP  zws = new sap_zws_cust_shtp.ZWS_CUST_SHTP();
                zws.inputHttpHeaders_x = new Map<String,String>();
                zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
                zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
                zws.timeout_x = 35000;
                  
                sap_zws_cust_shtp.TableOfZsdCustShtp tom2 = new sap_zws_cust_shtp.TableOfZsdCustShtp();     

                sap_zws_cust_shtp.TableOfZsdCustShtp elem = new sap_zws_cust_shtp.TableOfZsdCustShtp(); 
                if(acc.KUNNR__c != null){
                    String cusNum = UtilityClass.addleadingZeros(acc.KUNNR__c,10);  
                    elem = zws.ZsdCustShtp(tom2,cusNum);
                    setOFCountry = new set<String>();
                    if(elem.item != null){

                        for(sap_zws_cust_shtp.ZsdCustShtp z : elem.item){
                            if(z.Kunn2.startsWith('00'))
                                z.Kunn2                       = z.Kunn2.subString(2);
                                
                                
                                setOFCountry.add(z.Land1);
                                mapofCustShipToCountry.put(acc.KUNNR__c,setOFCountry);
                        }
                    }
                }
            }
        }
        if(mapofCustShipToCountry.size() > 0){

                for(Account acc : newRecords){
                   
                    if(mapofCustShipToCountry.containsKey(acc.KUNNR__c)){
                        setOFCountrytemp = new set<String>();
                        setOFCountrytemp                  = mapofCustShipToCountry.get(acc.KUNNR__c);
                        String counCode = '';
                        for(String str : setOFCountrytemp){
                            if(counCode == ''){
                                counCode =  str;
                            }else{
                                counCode = counCode +';'+ str;
                            }  
                        }
                        acc.Ship_to_Country__c            = counCode;
                        updatelistofCustomer.add(acc);
                    }
                }
        }
        system.debug(updatelistofCustomer+'updatelistofCustomer');
        system.debug(mapofCustShipToCountry+'mapofCustShipToCountry');
        if(updatelistofCustomer.size() > 0){
                upsert updatelistofCustomer KUNNR__c;
        } 
        
    }
    
    @future(callout=true)
    public Static void onAfterUpdateShipTO( set<id> accIDs){
        id exportRecTypeId = UtilityClass.getRecordTypeId('Account','Exports'); 
        map<String,String> mapofCustShipToCountry       = new Map<String,String>();
        list<Account> updatelistofCustomer              = new list<Account>();
        list<Account> newRecords                        = new list<Account>();
        String oldshipCountry = '';
        newRecords = [SELECT id,KUNNR__c,Ship_to_Country__c from Account Where id IN: accIDs AND RecordTypeId =: exportRecTypeId];
        if(newRecords.size() > 0){
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            for(Account acc: newRecords){
                sap_zws_cust_shtp  ws = new sap_zws_cust_shtp ();

                sap_zws_cust_shtp.ZWS_CUST_SHTP  zws = new sap_zws_cust_shtp.ZWS_CUST_SHTP();
                zws.inputHttpHeaders_x = new Map<String,String>();
                zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
                zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
                zws.timeout_x = 35000;
                  
                sap_zws_cust_shtp.TableOfZsdCustShtp tom2 = new sap_zws_cust_shtp.TableOfZsdCustShtp();     

                sap_zws_cust_shtp.TableOfZsdCustShtp elem = new sap_zws_cust_shtp.TableOfZsdCustShtp(); 
                if(acc.KUNNR__c != null){
                    String cusNum = UtilityClass.addleadingZeros(acc.KUNNR__c,10);  
                    elem = zws.ZsdCustShtp(tom2,cusNum);
                    String counCode = '';
                    if(elem.item != null){

                        for(sap_zws_cust_shtp.ZsdCustShtp z : elem.item){
                            if(z.Kunn2.startsWith('00'))
                                z.Kunn2                       = z.Kunn2.subString(2);
                                if(counCode == ''){
                                   counCode =  z.Land1;
                                     
                                }else{
                                        counCode = counCode +';'+ z.Land1;
                                }   
                                mapofCustShipToCountry.put(acc.KUNNR__c,counCode);
                        }
                    }
                }
            }
        }
        if(mapofCustShipToCountry.size() > 0){

                for(Account acc : newRecords){
                    if(mapofCustShipToCountry.containsKey(acc.KUNNR__c)){
                        if(acc.Ship_to_Country__c == ''){
                            acc.Ship_to_Country__c            = mapofCustShipToCountry.get(acc.KUNNR__c);    
                        }else{
                            acc.Ship_to_Country__c += mapofCustShipToCountry.get(acc.KUNNR__c);
                        }
                        updatelistofCustomer.add(acc);
                    }
                }
        }
        system.debug(updatelistofCustomer+'updatelistofCustomer');
        system.debug(mapofCustShipToCountry+'mapofCustShipToCountry');
        if(updatelistofCustomer.size() > 0){
                upsert updatelistofCustomer KUNNR__c;
        } 
        
    }*/
    public void onAfterUpdate(Map<Id,Account> oldValues, Map<Id,Account> newValues){
        //system.assertEquals('expected', 'actual');
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails =   new List<Messaging.SingleEmailMessage>();
        for(Id recId :oldValues.keySet()){
            if(oldValues.get(recId).Convert__c == false && newValues.get(recId).Convert__c == true && (newValues.get(recId).Business_Units__c == 'Replacement'  || newValues.get(recId).Business_Units__c == 'Specialty' )){
                //send a single email message
                // Step 1: Create a new Email
                Messaging.SingleEmailMessage mail = 
                    new Messaging.SingleEmailMessage();
                
                // Step 2: Set list of people who should get the email
                List<String> sendTo = new List<String>();
                RCM_Settings__c emailSetting = RCM_Settings__c.getValues(newValues.get(recId).Sales_Group_Text__c);
                if(emailSetting == null ){
                    continue;
                }
                sendTo.add(emailSetting.Email__c);
                mail.setToAddresses(sendTo);
                
                // Step 3: Set who the email is sent from
                //mail.setReplyTo('sirdavid@bankofnigeria.com');
                //mail.setSenderDisplayName('Official Bank of Nigeria');
                
                // (Optional) Set list of people who should be CC'ed
                //List<String> ccTo = new List<String>();
                //ccTo.add('business@bankofnigeria.com');
                //mail.setCcAddresses(ccTo);
                
                // Step 4. Set email contents - you can use variables!
                mail.setSubject(' New Dealer to be created in SAP');
                String body = 'Hi ' +  + ', <br/>';
                body += 'Please create a new dealer in SAP with the following details <br/>';
                body += 'Name : '+newValues.get(recId).Name + ' <br/>'  ; 
                body += 'SFDC Reference Code : '+newValues.get(recId).UniqueIdentifier__c + ' <br/>'  ; 
                body += 'Street 1: '+newValues.get(recId).Street_1__c + ' <br/>' ; 
                body += 'Street 2: '+  newValues.get(recId).Street_2__c + ' <br/>'  ; 
                body += 'Town: '+ newValues.get(recId).Town__c + ' <br/>'  ; 
                body += 'District: '+ newValues.get(recId).District__c + ' <br/>'  ; 
                body += 'State: '+ newValues.get(recId).State__c + ' <br/>'  ; 
                body += 'Sales District: '+ newValues.get(recId).Sales_District_Text__c + ' <br/>'  ; 
                body += 'Pin Code: '+ newValues.get(recId).PIN_code__c + ' <br/>'  ; 
                body += 'Phone 1: '+ newValues.get(recId).Phone + ' <br/>'  ; 
                body += 'Phone 2: '+ newValues.get(recId).AD_TLNMBR__c + ' <br/>'  ; 
                body += 'Email: '+ newValues.get(recId).Email__c + ' <br/>'  ; 
                body += 'Bank Name: '+ newValues.get(recId).Bank_Name__c + ' <br/>'  ; 
                body += 'Bank IFSC Code: '+ newValues.get(recId).IFSC_Code__c + ' <br/>'  ; 
                body += 'Bank Branch: '+ newValues.get(recId).Bank_branch__c + ' <br/>'  ; 
                body += 'Bank Account Number: '+ newValues.get(recId).Bank_Account_no__c + ' <br/>'  ; 
                body += 'Security Deposit: '+ newValues.get(recId).Security_Deposit__c + ' <br/>'  ; 
                body += 'PAN Number: '+ newValues.get(recId).PAN_Number__c + ' <br/>'  ; 
                body += 'TIN Number: '+ newValues.get(recId).TIN_Number__c + ' <br/>'  ;
                mail.setHtmlBody(body);
                
                // Step 5. Add your email to the master list
                mails.add(mail);
            }
            
            if(oldValues.get(recId).Convert__c == false && newValues.get(recId).Convert__c == true && newValues.get(recId).Business_Units__c == 'Exports'){
                //send a single email message
                // Step 1: Create a new Email
                Messaging.SingleEmailMessage mail = 
                    new Messaging.SingleEmailMessage();
                
                // Step 2: Set list of people who should get the email
                List<String> sendTo = new List<String>();
                RCM_Settings__c emailSetting = RCM_Settings__c.getValues('Export');
                if(emailSetting == null ){
                    continue;
                }
                sendTo.add(emailSetting.Email__c);
                mail.setToAddresses(sendTo);
                
                // Step 3: Set who the email is sent from
                //mail.setReplyTo('sirdavid@bankofnigeria.com');
                //mail.setSenderDisplayName('Official Bank of Nigeria');
                
                // (Optional) Set list of people who should be CC'ed
                //List<String> ccTo = new List<String>();
                //ccTo.add('business@bankofnigeria.com');
                //mail.setCcAddresses(ccTo);
                
                // Step 4. Set email contents - you can use variables!
                mail.setSubject(' New Dealer to be created in SAP');
                String body = 'Hi ' +  + ', <br/>';
                body += 'Please create a new dealer in SAP with the following details <br/>';
                body += 'Name : '+newValues.get(recId).Name + ' <br/>'  ; 
                body += 'SFDC Reference Code : '+newValues.get(recId).UniqueIdentifier__c + ' <br/>'  ; 
               
                body += 'Sales Office: '+ newValues.get(recId).Sales_Office_Text__c+ ' <br/>'  ; 
                body += 'Country: '+ newValues.get(recId).Country__c+ ' <br/>'  ; 
                body += 'Phone 1: '+ newValues.get(recId).Phone + ' <br/>'  ; 
                body += 'Phone 2: '+ newValues.get(recId).AD_TLNMBR__c + ' <br/>'  ; 
                body += 'Email: '+ newValues.get(recId).Email__c + ' <br/>'  ; 
                body += 'Bank Name: '+ newValues.get(recId).Bank_Name__c + ' <br/>'  ; 
                body += 'Bank IFSC Code: '+ newValues.get(recId).IFSC_Code__c + ' <br/>'  ; 
                body += 'Bank Branch: '+ newValues.get(recId).Bank_branch__c + ' <br/>'  ; 
                body += 'Bank Account Number: '+ newValues.get(recId).Bank_Account_no__c + ' <br/>'  ; 
                body += 'Security Deposit: '+ newValues.get(recId).Security_Deposit__c + ' <br/>'  ; 
                body += 'PAN Number: '+ newValues.get(recId).PAN_Number__c + ' <br/>'  ; 
                body += 'TIN Number: '+ newValues.get(recId).TIN_Number__c + ' <br/>'  ;
                mail.setHtmlBody(body);
                
                // Step 5. Add your email to the master list
                mails.add(mail);
            }
        }
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }
    
}