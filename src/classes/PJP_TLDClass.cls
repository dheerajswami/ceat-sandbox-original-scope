global with sharing class PJP_TLDClass {    
    
    public static final String salesforceBaseUrl {get;set;}

    static {
        salesforceBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    }

    public PJP_TLDClass() {}             
    
    @RemoteAction
    global static WrapperPjpDetails getWriteAccess(String gotMonth, String gotYear, String gotOwner) {
        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();                
        WrapperPjpDetails wpd = new WrapperPjpDetails();                             
        Date todayDate = Date.today();
        String yearString;
        PJP__c newPJP;
        List<ACW__c> window = new List<ACW__c>();                                        

        if(gotMonth == null) {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Replacement' And Active__c = true And From_Date__c <= :todayDate And To_Date__c >= :todayDate];            

            if(window.size() == 0) {
                wpd.monthInt = todayDate.month();
                wpd.monthString = calculateMonth(todayDate.month());
                wpd.yearInt = todayDate.year();
                yearString = String.valueOf(wpd.yearInt);
                wpd.isReadOnly = true;                
            } else {
                wpd.monthString = window[0].Month_Text__c;
                wpd.monthInt = Integer.valueOf(window[0].Month__c);
                wpd.yearInt = Integer.valueOf(window[0].Year__c);
                yearString = String.valueOf(wpd.yearInt);
                wpd.isReadOnly = false;
            }
        } else {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Replacement' And Active__c = true And Month_Text__c = :gotMonth And Year__c = :gotYear]; 

            wpd.monthString = window[0].Month_Text__c;
            wpd.monthInt = Integer.valueOf(window[0].Month__c);
            wpd.yearInt = Integer.valueOf(window[0].Year__c);
            yearString = String.valueOf(wpd.yearInt);

            if(window[0].From_Date__c <= todayDate && window[0].To_Date__c >= todayDate) {
                wpd.isReadOnly = false;
            } else {
                wpd.isReadOnly = true;
            }         
        }
        
        if(gotOwner == null) {
            wpd.ownerId = UserInfo.getUserId();
        } else {
            wpd.ownerId = Id.valueOf(gotOwner);
        }
        
        try {            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c where Visit_Plan_Month__c = :wpd.monthString And Visit_Plan_Year__c = :yearString And OwnerId = :wpd.ownerId limit 1];            
            wpd.pjpId = newPJP.Id;
            wpd.pjpComments = newPJP.Comments__c;
            if(newPJP.OwnerId != UserInfo.getUserId() || newPJP.Sys_ApprovalStatus__c == 'Submitted' || newPJP.Sys_ApprovalStatus__c == 'Approved') {
                wpd.isReadOnly = true;
            }
            if(newPJP.PJP_Norms__c != null) {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c FROM PJP_Norms_Main__c where Id = :newPJP.PJP_Norms__c];
            } else {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = 'TLD' Limit 1];
            }
        } catch(Exception e) {
            activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = 'TLD' Limit 1];
        }
        

        for(PJP_Norms_Main__c pnm : activeNorms) {            
            System.debug('activeNorms  '+pnm.Id+'  1   '+pnm.Is_MOR__c+'  2    '+pnm.Is_OE_Days__c+'  3   '+pnm.Is_Company_Activities__c);
            wpd.isOE = pnm.Is_OE_Days__c;
            wpd.isMOR = pnm.Is_MOR__c;
            wpd.isCompAct = pnm.Is_Company_Activities__c;
            wpd.oeDays = Integer.valueOf(pnm.OE_Days__c);
            wpd.morDays = Integer.valueOf(pnm.MOR__c);
            wpd.compActDays = Integer.valueOf(pnm.Company_Activities__c);  
            wpd.dseBeatTarget = Integer.valueOf(pnm.No_Of_DSE_Beat_Visits__c);          
        }
        
        return wpd;
    }

    @RemoteAction
    global static List<WrapperVisitPlanDays> getRelatedDseBeats(Integer yr, Integer mn, String mnth, String ownerId) {                  
        List<AggregateResult> allDseBeats = new List<AggregateResult>(); 
        List<String> deType = new List<String>();
        
        List<WrapperVisitPlanDays> tableList = new List<WrapperVisitPlanDays>();        
        Map<Integer,Holiday__c>  dlMap = new Map<Integer,Holiday__c>();
        Set<Integer>  hlList = new Set<Integer>(); 

        Integer en =  date.daysInMonth(yr, mn);
        List<DayList> listOfWeekndsAndHoliday;
        Integer day29;
        Integer day30;
        Integer day31;
        Set<Id> dseBeatIdSet = new Set<id>();
        List<AvailableDay> availableDaysList;
        
        User ownerUser = [Select Id, Name, Profile.Name, EmployeeNumber, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :ownerId];

        List<Visits_Plan__c> visitplanList = [Select Check_In__c, DSE_Beat__c, DSE_Beat__r.Name, Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                              from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c = :String.valueOf(yr) And Master_Plan__r.OwnerId =: ownerId];

        List<PJP__c> newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c
                where Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :String.valueOf(yr) And OwnerId = :ownerId limit 1];
        
        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();
        if(newPJP.size() > 0 && newPJP[0].PJP_Norms__c != null) {
            activeNorms = [SELECT Id, Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c                
                FROM PJP_Norms_Main__c where Id = :newPJP[0].PJP_Norms__c];
        } else {
            activeNorms = [SELECT Id, Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c 
                FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = 'TLD' Limit 1];
        }
        
        UserTerritory2Association userTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:ownerId Limit 1];
        Territory2 userTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerrAss.Territory2Id Limit 1];
        Territory2 userRegion = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerritory.ParentTerritory2Id Limit 1];
        Territory2 userZone = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userRegion.ParentTerritory2Id Limit 1];        

        if(visitplanList.size() > 0) {
            for(Visits_Plan__c vp : visitplanList) {
                if(!dseBeatIdSet.contains(vp.DSE_Beat__c)) {
                    dseBeatIdSet.add(vp.DSE_Beat__c);       
                } 
            }
        }
        
        for(Holiday__c hol : [Select Name, Active__c, Date__c, Name_of_Holiday__c, Description__c, Employee_No__c, Start_Date__c, End_Date__c, RecordType.Name From Holiday__c where (((RecordType.Name = 'National' Or (RecordType.Name = 'Regional' And Region__c = :userRegion.Name)) And Date__c >= :date.newinstance(yr,mn,1) And Date__c <= :date.newinstance(yr,mn,en)) Or (RecordType.Name = 'Planned Leaves' And Employee_No__c = :ownerUser.EmployeeNumber))]){
            if(hol.RecordType.Name == 'Planned Leaves') {               
                for(Date d = hol.Start_Date__c; d <= hol.End_Date__c; d = d.addDays(1)) {                       
                    if(d.month() == Date.newInstance(yr, mn, 1).month()) {
                        dlMap.put(d.day(), hol);
                    }
                } 
                            
            } else {
                dlMap.put(hol.Date__c.day(), hol);
            }            
        }
        
        List<String> dseBeatRecordTypes = 'DSE-Beat'.split(',');
        
        allDseBeats = [Select Id, Name, Beat_Code__c, DSE_Code__c, Distributor__r.Town__c distTown, TLD_Employee_Code__c, DSE_Name__c, Distributor_Name__c, TLD_Territory_Code__c from DSE_Beat__c where TLD_Territory_Code__c = :userTerrAss.Territory2.Name And  Distributor__r.Active__c = true group by Name, Id, Beat_Code__c, DSE_Code__c, TLD_Employee_Code__c, TLD_Territory_Code__c, Distributor__r.Town__c, DSE_Name__c, Distributor_Name__c order by Name, Beat_Code__c, DSE_Code__c, TLD_Employee_Code__c];
        
        if(allDseBeats.size() > 0){
            List<String> weekendList = System.Label.PJP_Weekends.split(',');
            if(weekendList.size() == 0) {
                weekendList.add('Sunday');
            }
            listOfWeekndsAndHoliday = new List<DayList>();
            for(Integer i=1; i<=en; i++) {                        
                DayList dl = new DayList();
                DateTime dt=DateTime.newInstance(date.newInstance(yr,mn,i), Time.newInstance(0, 0, 0, 0));
                String dayOfWeek = dt.format('EEE');

                if(dlMap.containsKey(i)) {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'PublicHoliday';
                        dl.holReason = '';
                        if(dlMap.containsKey(i)) {
                            dl.holReason = dlMap.get(i).Name;
                        }
                    }                        
                } else {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'Normal'; 
                    }  
                }                
                listOfWeekndsAndHoliday.add(dl);
            }

            if(en == 29) {
                day29 = 29;
            } else if(en == 30) {
                day30 = 30;
            } else if(en == 31) {
                day31 = 31;
            }     

            for(AggregateResult  d : allDseBeats)
            {                
                Integer target = 0;
                availableDaysList = new List<AvailableDay>(35);    
                System.debug('activeNorms.size()'+activeNorms.size());
                if(activeNorms.size() > 0)
                    target = Integer.valueOf(activeNorms[0].No_Of_DSE_Beat_Visits__c);

                for(integer i = 0; i < 32; i++) {
                    AvailableDay avail = new AvailableDay(i);
                    avail.value = i;
                    avail.dateAvailable = false;
                    avail.planVisitId = null;
                    availableDaysList.set(i,avail);                    
                }
                
                if(availableDaysList.size() > 0) {
                    for(Visits_Plan__c vp : visitplanList) {                         
                        if((d.id == vp.DSE_Beat__c)) {
                            Date d1 = vp.Visit_Day__c;
                            AvailableDay availInstance = new AvailableDay(d1.day());
                            availInstance.value = d1.day();
                            availInstance.typeOfDay = vp.Type_of_Day__c;
                            availInstance.dateAvailable= true;
                            availInstance.planVisitId = vp.id;
                            availableDaysList.set(availInstance.value-1,availInstance);                
                        }
                    } 
                }
                                                                                            
                tableList.add(new WrapperVisitPlanDays(String.ValueOf(d.get('Id')), String.ValueOf( d.get('Name')), String.ValueOf( d.get('distTown')), String.ValueOf( d.get('Distributor_Name__c')), String.ValueOf( d.get('DSE_Name__c')), String.ValueOf( d.get('Beat_Code__c')), 'Visit Day', target, listOfWeekndsAndHoliday, day29, day30, day31, availableDaysList));
            }       
            
            List<AvailableDay> morAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> oeAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> compActAvailableDays = new List<AvailableDay>(35);            
                        
            for(integer i = 0; i < 32; i++) {
                AvailableDay avail = new AvailableDay(i);
                avail.value = i;
                avail.dateAvailable = false;
                avail.planVisitId = null;
                morAvailableDays.set(i,avail);
                oeAvailableDays.set(i,avail);
                compActAvailableDays.set(i,avail);                  
            }

            for(Visits_Plan__c vp : visitplanList) {
                if(vp.Type_of_Day__c != 'Visit Day') {
                    Date d1 = vp.Visit_Day__c;
                    AvailableDay availInstance = new AvailableDay(d1.day());
                    availInstance.value = d1.day();
                    availInstance.typeOfDay = vp.Type_of_Day__c;
                    availInstance.dateAvailable= true;
                    availInstance.planVisitId = vp.id;

                    if(vp.Type_of_Day__c == 'MOR') {                                        
                        morAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'OE') {
                        oeAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Company Activity') {
                        compActAvailableDays.set(availInstance.value-1, availInstance);
                    }
                }
            }

            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'MOR', null, listOfWeekndsAndHoliday, day29, day30, day31, morAvailableDays));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, 'OE', null, listOfWeekndsAndHoliday, day29, day30, day31, oeAvailableDays));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null,  'Company Activity', null, listOfWeekndsAndHoliday, day29, day30, day31, compActAvailableDays));            
        }        
        return tableList; 
    } 
    
    @RemoteAction
    global static String saveVisitPlans(List<Visits_Plan__c> visitplanList, Integer yr, Integer mn, string mnth, String buttonClicked, String comments) {        
        User currentUser = [Select Id, Name, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :UserInfo.getUserId()];        
        RecordType rtype = [Select Id, Name, DeveloperName from RecordType where DeveloperName = 'Replacement' And SobjectType = 'PJP__c'];
        
        List<RecordType> rtList = [Select Id, Name, DeveloperName from RecordType where SobjectType = 'Visits_Plan__c'];
        Id rtCompAct = null;        
        Id rtDseBeat = null;
        for(RecordType rt : rtList) {
            if(rt.Name == 'MOR-OE-CompAct') {
                rtCompAct = rt.Id;
            } else if(rt.Name == 'DSE-Beat') {
                rtDseBeat = rt.Id;
            } 
        }

        PJP__c newPJP ;
        Date visitPlanDate;
        List<Visits_Plan__c> visitplantoupsert = new List<Visits_Plan__c>();

        List<PJP_Norms_Main__c> activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, No_Of_DSE_Beat_Visits__c FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = 'TLD' Limit 1];
        
        try{            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c from PJP__c where RecordTypeId = :rtype.Id And Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :String.valueOf(yr) And OwnerId = :UserInfo.getUserId() limit 1];
            if(newPJP.PJP_Norms__c == null) {
                if(activeNorms.size() > 0) {
                    newPJP.PJP_Norms__c = activeNorms[0].Id;
                }
            }            
            newPJP.Comments__c = comments;            
            update newPJP;
            PJP_CalculateSummary.calculateSummary(newPJP.Id);
            
        }catch(Exception e){            
            newPJP = new PJP__c();
            if(activeNorms.size() > 0) {
                newPJP.PJP_Norms__c = activeNorms[0].Id;
            }
            newPJP.Visit_Plan_Month__c = mnth;
            newPJP.Visit_Plan_Year__c = String.valueOf(yr);                        
            newPJP.Visit_Plan_Date_del__c = Date.newInstance(yr, mn, 1);        
            newPJP.Comments__c = comments; 
            newPJP.Owner_s_Manager__c = currentUser.ManagerId;
            newPJP.RecordTypeId = rtype.Id;   
            List<UserTerritory2Association> listUserTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:currentUser.Id];
            Set<Id> terrIds = new Set<Id>();
            for(UserTerritory2Association uta : listUserTerrAss) {
                terrIds.add(uta.Territory2Id);
            }
            List<Territory2> listUserTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id in :terrIds];
            for(Integer i=0; i<listUserTerritory.size(); i++) {
                if(i == 0) {
                    newPJP.Territory__c = listUserTerritory[i].Name;
                } else {
                    newPJP.Territory__c += '; ' + listUserTerritory[i].Name;
                }                
            }
            if(listUserTerritory.size() > 0) {
                Territory2 userRegion = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :listUserTerritory[0].ParentTerritory2Id Limit 1];
        Territory2 userZone = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userRegion.ParentTerritory2Id Limit 1];   
                newPJP.Region__c = userRegion.Name;
                newPJP.Zone__c = userZone.Name;
            }
            insert newPJP;        
            PJP_CalculateSummary.calculateSummary(newPJP.Id);    
        }
        
        for(Visits_Plan__c temp1: visitplanList){            
            temp1.Visit_Day__c = Date.newinstance(Integer.ValueOf(yr),Integer.ValueOf(mn),Integer.ValueOf(temp1.Day__c));
            temp1.Master_Plan__c = newPJP.Id;
            
            if(temp1.Type_of_Day__c == 'MOR' || temp1.Type_of_Day__c == 'OE' || temp1.Type_of_Day__c == 'Company Activity') {                        
                temp1.RecordTypeId = rtCompAct;
            } else {
                temp1.RecordTypeId = rtDseBeat;
            }

            visitplantoupsert.add(temp1);            
        }
        boolean flag = false;
        if(visitplantoupsert.size() > 0) {
            try {

                List<Visits_Plan__c> previousVisitPlans = [Select Id from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c =: String.valueOf(yr) And Master_Plan__r.OwnerId =: UserInfo.getUserId()];
                List<Visits_Plan__c> deleteVisitPlans = new List<Visits_Plan__c>();

                for(Visits_Plan__c prevVp : previousVisitPlans) {
                    Boolean idFound = false;
                    for(Visits_Plan__c newVp : visitplantoupsert) {
                        if(prevVp.Id == newVp.Id) {
                            idFound = true;
                            break;
                        }
                    }
                    if(!idFound) {
                        deleteVisitPlans.add(prevVp);
                    }
                }

                delete deleteVisitPlans;
                upsert visitplantoupsert;
                         
                if(buttonClicked == 'Submit') {
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting request for approval.');
                    req1.setObjectId(newPJP.Id);                                        
                    Approval.ProcessResult result = Approval.process(req1);
                    FeedItem fitem=new FeedItem();
                    fItem.parentId=currentUser.ManagerId;
                    fItem.Title='Click here to Approve/Reject';
                    fItem.body = 'PJP has been submitted for the month '+newPJP.Visit_Plan_Month__c+' by '+currentUser.Name+'. ';
                    //fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/PJP_TLD?owner='+currentUser.Id+'&month='+newPJP.Visit_Plan_Month__c+'&year='+newPJP.Visit_Plan_Year__c;
                    fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newPJP.Id;
                    insert fItem;
                    //User currentUser = [Select Id, Name, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :UserInfo.getUserId()];        
                    //String[] toAddresses = new String[] {currentUser.Manager.Email}; 
                    //sendemail(newPJP.Id, toAddresses);
                }
                       
                flag = true;
            } catch(Exception e) {
                system.debug(e.getMessage());
            }
        }
        return buttonClicked+'; '+newPJP.Id;
    }
       /*
    public static void sendemail(Id gotId, String[] toAddresses) {        
        //String[] toAddresses = new String[] {'swayam.arora@extentor.com'}; 
        System.debug('Swayam Email '+toAddresses);
        EmailTemplate emailTemplateInstance = [select Id from EmailTemplate where Name='Report Test'];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplateInstance.Id);
        email.setToAddresses(toAddresses);
        email.setTargetObjectId(UserInfo.getUserId());
        //email.setWhatId(gotId);
        email.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.Email[] { email } );            
        System.debug('Swayam Email2 '+toAddresses);
    }  
    */
    public static String calculateMonth(Integer mm) {
        if(mm == 1) 
            return 'January';
        if(mm == 2) 
            return 'February';
        if(mm == 3) 
            return 'March';
        if(mm == 4) 
            return 'April';
        if(mm == 5) 
            return 'May';
        if(mm == 6) 
            return 'June';
        if(mm == 7) 
            return 'July';
        if(mm == 8) 
            return 'August';
        if(mm == 9) 
            return 'September';
        if(mm == 10) 
            return 'October';
        if(mm == 11) 
            return 'November';
        if(mm == 12) 
            return 'December';

        return '';
    }

    //Wrapper Class

    public class DayList{
        public Integer dayNumber {get;set;}
        public String typeCol {get;set;}
        public String holReason {get;set;}
    }

    public class AvailableDay {
        public integer value {get;set;}
        public boolean dateAvailable {get;set;}
        public id planVisitId {get;set;}
        public String typeOfDay{get;set;}
        
        public AvailableDay(integer i) {
            value = i;
            dateAvailable = false;
            planVisitId = null;
            typeOfDay = null;
        }
    }

    global class WrapperPjpDetails {
        public Integer dailyTarget {get;set;}
        public Boolean isOE {get;set;}
        public Boolean isMOR {get;set;}
        public Boolean isCompAct {get;set;}
        public Integer oeDays {get;set;}
        public Integer morDays {get;set;}
        public Integer compActDays {get;set;}
        public Integer influDays {get;set;}
        public String dailyTargets {get;set;}
        public String monthString {get;set;}
        public Integer yearInt {get;set;}
        public Integer monthInt {get;set;}
        public String pjpComments {get;set;}
        public String pjpId {get;set;}
        public String ownerId {get;set;}
        public Boolean isReadOnly {get;set;}
        public Integer dseBeatTarget {get;set;}
    }
    
    global class WrapperVisitPlanDays {
        public String dseBeatId{get;set;}
        public String dseBeatName{get;set;}
        public String distributorName{get;set;}
        public String dseName{get;set;}
    public String accountTown{get;set;}
        public String beatCode{get;set;}       
        public String typeOfDay{get;set;}          
        public List<DayList> dayMon{get;set;}
        public Integer day1{get;set;}
        public Integer day2{get;set;}
        public Integer day3{get;set;}
        public Integer day4{get;set;}
        public Integer day5{get;set;}
        public Integer day6{get;set;}
        public Integer day7{get;set;}
        public Integer day8{get;set;}
        public Integer day9{get;set;}
        public Integer day10{get;set;}
        public Integer day11{get;set;}
        public Integer day12{get;set;}
        public Integer day13{get;set;}
        public Integer day14{get;set;}
        public Integer day15{get;set;}
        public Integer day16{get;set;}
        public Integer day17{get;set;}
        public Integer day18{get;set;}
        public Integer day19{get;set;}
        public Integer day20{get;set;}
        public Integer day21{get;set;}
        public Integer day22{get;set;}
        public Integer day23{get;set;}
        public Integer day24{get;set;}
        public Integer day25{get;set;}
        public Integer day26{get;set;}
        public Integer day27{get;set;}
        public Integer day28{get;set;}
        public Integer day29{get;set;}
        public Integer day30{get;set;}
        public Integer day31{get;set;}
        public List<AvailableDay> availableDayList{get;set;}        
        public Integer targetDays{get;set;}
        public Integer plannedDays{get;set;}        
        
        public WrapperVisitPlanDays(String dId, String dName, String accTown, String distName, String dsName, String bCode, String tod, Integer tDays, List<DayList> dt, Integer day29, Integer day30, Integer day31, List<AvailableDay> availableDaysVar)
        {
            dayMon = new List<DayList>();
            dseBeatId = dId;
            dseBeatName = dName;   
            distributorName = distName;   
            dseName = dsName;   
      accountTown = accTown;
            beatCode = bCode;         
            typeOfDay = tod;         
            targetDays = tDays;
            dayMon = dt;
            day1 = 1;
            day2 = 2;
            day3 = 3;
            day4 = 4;
            day5 = 5;
            day6 = 6;
            day7 = 7;
            day8 = 8;
            day9 = 9;
            day10 = 10;
            day11 = 11;
            day12 = 12;
            day13 = 13;
            day14 = 14;
            day15 = 15;
            day16 = 16;
            day17 = 17;
            day18 = 18;
            day19 = 19;
            day20 = 20;
            day21 = 21;
            day22 = 22;
            day23 = 23;
            day24 = 24;
            day25 = 25;
            day26 = 26;
            day27 = 27;
            day28 = 28;
            this.day29 = day29;
            if(day30 == 30) {
                this.day29 = 29;
                this.day30 = day30;
            } if(day31 == 31) {
                this.day29 = 29;
                this.day30 = 30;
                this.day31 = day31;
            }
            availableDayList = new List<AvailableDay>();
            availableDayList = availableDaysVar;            
        }
    }
}