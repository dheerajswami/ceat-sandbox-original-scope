/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CPORT_DISC_PLD_TestClass {

	public static void init(){
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
    }

    static testMethod void testMethodFor_PLDNew() {
        init();
    	Test.startTest();   
    	    //  FOR SapDiscountsPLDnew Class	    	        
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSapDiscountsPLDnew());
        	CPORT_DISC_PLD.getAllDiscountsPLDM('000001323','50003327');

        	// FOR Sap_DN_Slab_Disc_Prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSlab_Disc_Prod());
        	CPORT_DISC_PLD.getDebitNoteSlabM('000001323','50003327');

        	// FOR Sap_Slab_Disc_Prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSap_Slab_Disc_Prod());
        	CPORT_DISC_PLD.getDiscountSlabM('000001323','50003327');
        	
        	// FOR SAP_DISCOUNT_1504
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSap_Slab_Disc_1504());
        	CPORT_DISC_PLD.getDiscountSlab1504M('000001323','50003327');

        	// FOR Sap_Collection_Sales_Disc_Prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockCollectoionSales());
        	CPORT_DISC_PLD.getCollectoionSalesM('000001323','50003327');

        	// FOR Sap_CS_Sales_Disc_Prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSap_CS_Sales_Disc_Prod());
        	CPORT_DISC_PLD.getCSSalesDiscountsM('000001323','50003327');

        	// FOR sap_tld_disc_Prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSapDiscProd());
        	CPORT_DISC_PLD.getSlabTldM('000001323','50003327');

        	// FOR sap_disc_master_prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSapDiscMaster());
        	CPORT_DISC_TLD.getDiscountMaster('000001323','50003327');

        	// FOR sap_ppd_disc_prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSapPPDDisc());
        	CPORT_DISC_PLD.getPPDDiscountMasterM('000001323','50003327');

        	// FOR sap_ad_disc_prod Class
        	System.Test.setMock(WebServiceMock.class, new WebServiceMockSapADDisc());
        	CPORT_DISC_PLD.getSapAdDiscountsM('000001323','50003327');


        Test.stopTest();
    }

    private class WebServiceMockSapADDisc implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            sap_ad_disc_prod scd  = new sap_ad_disc_prod();

            sap_ad_disc_prod.ZsfdcAdDisc1Response_element reponse_element  = new sap_ad_disc_prod.ZsfdcAdDisc1Response_element();
            sap_ad_disc_prod.TableOfZsdfcAdDiscount1  tom = new sap_ad_disc_prod.TableOfZsdfcAdDiscount1();
            
            sap_ad_disc_prod.ZsdfcAdDiscount1 pd_item = new sap_ad_disc_prod.ZsdfcAdDiscount1();

	        pd_item.Crnum='DATA';
	        pd_item.Kunnr='DATA';
	        pd_item.Mwskz='DATA';
	        pd_item.RunDate='DATA';
	        pd_item.Name1='DATA';
	        pd_item.Office='DATA';
	        pd_item.Zgroup='DATA';
	        pd_item.Dwerk='DATA';
	        pd_item.Apdat='DATA';
	        pd_item.Sdsas='DATA';
	        pd_item.Zavg='DATA';
	        pd_item.Sdsgr='DATA';
	        pd_item.Sdsat='DATA';
	        pd_item.Pre='DATA';
	        pd_item.Post='DATA';
	        pd_item.Kwert='DATA';
	        pd_item.Rot='DATA';
	        pd_item.Targt='DATA';
	        pd_item.Reqto='DATA';
	        pd_item.Addin='DATA';
	        pd_item.Sds='DATA';
	        pd_item.Tov='DATA';
	        pd_item.Crval='DATA';
	        pd_item.Disc='DATA';
	        pd_item.Indic='DATA';

			tom.item = new List<sap_ad_disc_prod.ZsdfcAdDiscount1>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            sap_ad_disc_prod.ZsfdcAdDisc1_element m = new sap_ad_disc_prod.ZsfdcAdDisc1_element();
            m.Crnum ='2015-01-10';
            m.ItOutput = tom;
            m.Kunnr = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

    private class WebServiceMockSapPPDDisc implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            sap_ppd_disc_prod scd  = new sap_ppd_disc_prod();

            sap_ppd_disc_prod.ZsfdcPpdDiscResponse_element   reponse_element  = new sap_ppd_disc_prod.ZsfdcPpdDiscResponse_element();
            sap_ppd_disc_prod.TableOfZsdDiscountRfc  tom = new sap_ppd_disc_prod.TableOfZsdDiscountRfc();
            sap_ppd_disc_prod.TableOfZsdDisbRfc  tom1 = new sap_ppd_disc_prod.TableOfZsdDisbRfc();
            
            sap_ppd_disc_prod.ZsdDiscountRfc pd_item = new sap_ppd_disc_prod.ZsdDiscountRfc();

	        pd_item.Cirno='DATA';
			pd_item.Kunag='DATA';
			pd_item.Name1='DATA';
			pd_item.Vbelv='DATA';
			pd_item.Pretax='DATA';
			pd_item.Posttax='DATA';
			pd_item.Discount='DATA';

			tom.item = new List<sap_ppd_disc_prod.ZsdDiscountRfc>();
			tom.item.add(pd_item);

			sap_ppd_disc_prod.ZsdDisbRfc pd_item1 = new sap_ppd_disc_prod.ZsdDisbRfc();

	        pd_item1.Cirno='DATA';
			pd_item1.Kunag='DATA';
			pd_item1.InvoiceNumber='DATA';
			pd_item1.Posnr='DATA';
			pd_item1.DateOfBilling='DATA';
			pd_item1.InvoiceValue='DATA';
			pd_item1.DiscRate='DATA';
			pd_item1.Discount='DATA';
			pd_item1.Payment='DATA';
			pd_item1.PaymentDate='DATA';
			pd_item1.Duedat='DATA';
			pd_item1.PostingDate='DATA';

			tom1.item = new List<sap_ppd_disc_prod.ZsdDisbRfc>();
			tom1.item.add(pd_item1);

            reponse_element.ItOutput = tom;
            reponse_element.ItOutput1 = tom1;
            reponse_element.Return_x = 'some text';
            
            sap_ppd_disc_prod.ZsfdcPpdDisc_element m = new sap_ppd_disc_prod.ZsfdcPpdDisc_element();
            m.Cirno ='2015-01-10';
            m.ItOutput = tom;
            m.ItOutput1 = tom1;
            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

    private class WebServiceMockSapDiscMaster implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            sap_disc_master_prod scd  = new sap_disc_master_prod();

            sap_disc_master_prod.ZsfdcDiscMasterResponse_element   reponse_element  = new sap_disc_master_prod.ZsfdcDiscMasterResponse_element();
            sap_disc_master_prod.TableOfZsdDiscMaster  tom = new sap_disc_master_prod.TableOfZsdDiscMaster();
            
            sap_disc_master_prod.ZsdDiscMaster pd_item = new sap_disc_master_prod.ZsdDiscMaster();

	        pd_item.Srnum = 'DATA';
			pd_item.Zscheme = 'DATA';
			pd_item.Sapdisc = 'DATA';
			pd_item.Subcod = 'DATA';
			pd_item.Ddtext = 'DATA';
			pd_item.Begda1 = 'DATA';
			pd_item.Endda = 'DATA';

			tom.item = new List<sap_disc_master_prod.ZsdDiscMaster>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            sap_disc_master_prod.ZsfdcDiscMaster_element m = new sap_disc_master_prod.ZsfdcDiscMaster_element();
            //m.Cirno ='2015-01-10';
            m.ItOutput = tom;
            //m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

    private class WebServiceMockSapDiscProd implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            sap_tld_disc_prod scd  = new sap_tld_disc_prod();

            sap_tld_disc_prod.ZsfdcTldDiscResponse_element   reponse_element  = new sap_tld_disc_prod.ZsfdcTldDiscResponse_element();
            sap_tld_disc_prod.TableOfZsd62Data  tom = new sap_tld_disc_prod.TableOfZsd62Data();
            
            sap_tld_disc_prod.Zsd62Data pd_item = new sap_tld_disc_prod.Zsd62Data();

	        pd_item.Mandt = 'DATA';
	        pd_item.Cirno = 'DATA';
	        pd_item.Kunag = 'DATA';
	        pd_item.Name1 = 'DATA';
	        pd_item.Superq = 'DATA';
	        pd_item.Pre = 'DATA';
	        pd_item.Post = 'DATA';
	        pd_item.Kwert = 'DATA';
	        pd_item.Disc = 'DATA';
	        pd_item.Dwerk = 'DATA';
	        pd_item.Fkimg = 'DATA';
	        pd_item.Fkimg1 = 'DATA';
	        pd_item.Discount = 'DATA';
	        pd_item.Type2 = 'DATA';
	        pd_item.Value = 'DATA';
	        pd_item.Target = 'DATA';
	        pd_item.Avgs = 'DATA';
	        pd_item.Type_x = 'DATA';
	        pd_item.Vkbur = 'DATA';
	        pd_item.Vkgrp = 'DATA';
	        pd_item.Office = 'DATA';
	        pd_item.Groups = 'DATA';
	        pd_item.Boi = 'DATA';

			tom.item = new List<sap_tld_disc_prod.Zsd62Data>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            sap_tld_disc_prod.ZsfdcTldDisc_element m = new sap_tld_disc_prod.ZsfdcTldDisc_element();
            m.Cirno ='2015-01-10';
            m.ItOutput = tom;
            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

    private class WebServiceMockSap_CS_Sales_Disc_Prod implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            Sap_CS_Sales_Disc_Prod scd  = new Sap_CS_Sales_Disc_Prod();

            Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDiscResponse_element   reponse_element  = new Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDiscResponse_element();
            Sap_CS_Sales_Disc_Prod.TableOfZsd47Data  tom = new Sap_CS_Sales_Disc_Prod.TableOfZsd47Data();
            
            Sap_CS_Sales_Disc_Prod.Zsd47Data pd_item = new Sap_CS_Sales_Disc_Prod.Zsd47Data();

            pd_item.Mandt = 'DATA';
	        pd_item.Crnum = 'DATA';
	        pd_item.Kunag = 'DATA';
	        pd_item.Name1 = 'DATA';
	        pd_item.Pre = 'DATA';
	        pd_item.Post = 'DATA';
	        pd_item.Kwerg = 'DATA';
	        pd_item.Kwert = 'DATA';
	        pd_item.Kwert1 = 'DATA';
	        pd_item.Fkimg = 'DATA';
	        pd_item.Fkimg1 = 'DATA';
	        pd_item.Discount = 'DATA';
	        pd_item.Type2 = 'DATA';
	        pd_item.Value = 'DATA';
	        pd_item.Type_x = 'DATA';
	        pd_item.Dwerk = 'DATA';
	        pd_item.Superq = 'DATA';
	        pd_item.Superd = 'DATA';
	        pd_item.Kvgr3 = 'DATA';
	        pd_item.Grthv = 'DATA';
	        pd_item.Grthq = 'DATA';
	        pd_item.Vkgrp = 'DATA';
	        pd_item.Vkbur = 'DATA';
	        pd_item.Sel = 'DATA';

			tom.item = new List<Sap_CS_Sales_Disc_Prod.Zsd47Data>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            
            
            Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDisc_element m = new Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDisc_element();
	            m.Crnum ='2015-01-10';
	            m.ItOutput = tom;
	            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

    private class WebServiceMockCollectoionSales implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            Sap_Collection_Sales_Disc_Prod scd  = new Sap_Collection_Sales_Disc_Prod();

            Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDiscResponse_element   reponse_element  = new Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDiscResponse_element();
            Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key  tom = new Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key();
            
            Sap_Collection_Sales_Disc_Prod.Zsd3021key pd_item = new Sap_Collection_Sales_Disc_Prod.Zsd3021key();

            pd_item.Mandt= 'DATA';
			pd_item.Vbeln= 'DATA';
			pd_item.Posnr= 'DATA';
			pd_item.Kunag= 'DATA';
			pd_item.Crnum= 'DATA';
			pd_item.Bezei1= 'DATA';
			pd_item.Bezei2= 'DATA';
			pd_item.Dwerk= 'DATA';
			pd_item.Superd= 'DATA';
			pd_item.Fkdat= 'DATA';
			pd_item.Postdat= 'DATA';
			pd_item.Duedat= 'DATA';
			pd_item.Zterm= 'DATA';
			pd_item.Paydat= 'DATA';
			pd_item.Zdays= 23;
			pd_item.Fkimg= 'DATA';
			pd_item.Pre= 'DATA';
			pd_item.Post= 'DATA';
			pd_item.Disc= 'DATA';
			pd_item.Type2= 'DATA';
			pd_item.Discount= 'DATA';
			pd_item.Indcr= 'DATA';
			pd_item.Werks= 'DATA';
			pd_item.PayAmount= 'DATA';
			pd_item.Augbl= 'DATA';

			tom.item = new List<Sap_Collection_Sales_Disc_Prod.Zsd3021key>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            
            
            Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDisc_element m = new Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDisc_element();
	            m.Crnum ='2015-01-10';
	            m.ItOutput = tom;
	            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }    

    private class WebServiceMockSap_Slab_Disc_Prod implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            Sap_Slab_Disc_Prod scd  = new Sap_Slab_Disc_Prod();

            Sap_Slab_Disc_Prod.ZsfdcSlabDiscResponse_element   reponse_element  = new Sap_Slab_Disc_Prod.ZsfdcSlabDiscResponse_element();
            Sap_Slab_Disc_Prod.TableOfZsd308Data  tom = new Sap_Slab_Disc_Prod.TableOfZsd308Data();
            
            Sap_Slab_Disc_Prod.Zsd308Data pd_item = new Sap_Slab_Disc_Prod.Zsd308Data();

            pd_item.Mandt= 'DATA';
			pd_item.Cirno= 'DATA';
			pd_item.Kunag= 'DATA';
			pd_item.Name1= 'DATA';
			pd_item.Vkbur= 'DATA';
			pd_item.Avgsds= 'DATA';
			pd_item.Vkgrp= 'DATA';
			pd_item.Werks= 'DATA';
			pd_item.Svdisb= 'DATA';
			pd_item.Pretax= 'DATA';
			pd_item.Posttax= 'DATA';
			pd_item.Multi= 'DATA';
			pd_item.Indcr= 'DATA';
			pd_item.Ratio= 'DATA';
			pd_item.Avgqty= 'DATA';
			pd_item.Dfkimg= 'DATA';
			pd_item.Clubt= 'DATA';
			pd_item.Fkimg= 'DATA';
			pd_item.Bqty= 'DATA';
			pd_item.WGr= 'DATA';
			pd_item.Appdate= 'DATA';
			pd_item.Discount= 'DATA';
			pd_item.Disc= 'DATA';
			pd_item.Type2= 'DATA';
			pd_item.Bod= 'DATA';
			pd_item.Dcind= 'DATA';
			pd_item.Value= 'DATA';
			pd_item.KwertN= 'DATA';
			pd_item.Mwskz= 'DATA';
			pd_item.Pcr= 'DATA';

			tom.item = new List<Sap_Slab_Disc_Prod.Zsd308Data>();
			tom.item.add(pd_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'some text';
            
            
            
            Sap_Slab_Disc_Prod.ZsfdcSlabDisc_element m = new Sap_Slab_Disc_Prod.ZsfdcSlabDisc_element();
	            m.Cirno ='2015-01-10';
	            m.ItOutput = tom;
	            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }

private class WebServiceMockSap_Slab_Disc_1504 implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            SapDiscount1504New scd  = new SapDiscount1504New();

            SapDiscount1504New.ZSFDC_BPLOU_DISCOUNTResponse_element   reponse_element  = new SapDiscount1504New.ZSFDC_BPLOU_DISCOUNTResponse_element();
            SapDiscount1504New.TABLE_OF_ZSDSLAB  tom = new SapDiscount1504New.TABLE_OF_ZSDSLAB();
            
            SapDiscount1504New.ZSDSLAB pd_item = new SapDiscount1504New.ZSDSLAB();

            pd_item.MANDT='DATA';
            pd_item.CIRCULAR_NO='DATA';
            pd_item.GROUP_AC='DATA';
            pd_item.CUSTOMER='DATA';
            pd_item.CUSTOMER_NAME='DATA';
            pd_item.SALES_OFFICE='DATA';
            pd_item.SALES_GROUP='DATA';
            pd_item.PLANT='DATA';
            pd_item.BPLOU_NON_BPLOU='DATA';
            pd_item.BASE_VAL='DATA';
            pd_item.Q1_AVGBPLOU='DATA';
            pd_item.Q2_AVGBPLOU='DATA';
            pd_item.Q3_AVGBPLOU='DATA';
            pd_item.Q4_AVGBPLOU='DATA';
            pd_item.FEB_SDS='DATA';
            pd_item.MAY_SDS='DATA';
            pd_item.Q1_TARGET='DATA';
            pd_item.Q2_TARGET='DATA';
            pd_item.Q3_TARGET='DATA';
            pd_item.Q4_TARGET='DATA';
            pd_item.SDS_TARGET='DATA';
            pd_item.REV_SDS_TARGET='DATA';
            pd_item.NON_BPLOU_TARGET='DATA';
            pd_item.Q1_CUSTPRE_SALE='DATA';
            pd_item.Q2_CUSTPRE_SALE='DATA';
            pd_item.Q3_CUSTPRE_SALE='DATA';
            pd_item.Q4_CUSTPRE_SALE='DATA';
            pd_item.Q1_CUSTPST_SALE='DATA';
            pd_item.Q2_CUSTPST_SALE='DATA';
            pd_item.Q3_CUSTPST_SALE='DATA';
            pd_item.Q4_CUSTPST_SALE='DATA';
            pd_item.Q1_GRP_CUST_PRE='DATA';
            pd_item.Q2_GRP_CUST_PRE='DATA';
            pd_item.Q3_GRP_CUST_PRE='DATA';
            pd_item.Q4_GRP_CUST_PRE='DATA';
            pd_item.Q1_GRP_CUST_PST='DATA';
            pd_item.Q2_GRP_CUST_PST='DATA';
            pd_item.Q3_GRP_CUST_PST='DATA';
            pd_item.Q4_GRP_CUST_PST='DATA';
            pd_item.AVG_APR_SDS='DATA';
            pd_item.AVG_MAY_SDS='DATA';
            pd_item.AVG_JUN_SDS='DATA';
            pd_item.AVG_JUL_SDS='DATA';
            pd_item.AVG_AUG_SDS='DATA';
            pd_item.AVG_SEP_SDS='DATA';
            pd_item.AVG_OCT_SDS='DATA';
            pd_item.AVG_NOV_SDS='DATA';
            pd_item.AVG_DEC_SDS='DATA';
            pd_item.AVG_JAN_SDS='DATA';
            pd_item.AVG_FEB_SDS='DATA';
            pd_item.AVG_MAR_SDS='DATA';
            pd_item.AVG_SDS='DATA';
            pd_item.Q1_QUAL='DATA';
            pd_item.Q2_QUAL='DATA';
            pd_item.Q3_QUAL='DATA';
            pd_item.Q4_QUAL='DATA';
            pd_item.CUST_Q1_DISC='DATA';
            pd_item.CUST_Q2_DISC='DATA';
            pd_item.CUST_Q3_DISC='DATA';
            pd_item.CUST_Q4_DISC='DATA';
            pd_item.GRPCUST_Q1_DISC='DATA';
            pd_item.GRPCUST_Q2_DISC='DATA';
            pd_item.GRPCUST_Q3_DISC='DATA';
            pd_item.GRPCUST_Q4_DISC='DATA';
            pd_item.CUST_BONUS='DATA';
            pd_item.GRPCUST_BONUS='DATA';
            pd_item.FUL_YR_BPLOU_ACHIV='DATA';
            pd_item.FUL_YR_SDS_PERCENT_ACHIV='DATA';
            pd_item.BONUS_REV_SDSTAR_Q4='DATA';
            pd_item.BONUS_SDS_PERCENT='DATA';
            pd_item.BONUS_VALUE_PERCENT='DATA';

			tom.item = new List<SapDiscount1504New.ZSDSLAB>();
			tom.item.add(pd_item);

            reponse_element.IT_OUTPUT = tom;
            reponse_element.Return_x = 'some text';
            
            
            
            SapDiscount1504New.ZSFDC_BPLOU_DISCOUNT_element m = new SapDiscount1504New.ZSFDC_BPLOU_DISCOUNT_element();
	            m.Cirno ='2015-01-10';
	            m.IT_OUTPUT = tom;
	            m.Kunag = '50003327';
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }
    private class WebServiceMockSapDiscountsPLDnew implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            SapDiscountsPLDnew scd  = new SapDiscountsPLDnew ();

            SapDiscountsPLDnew.ZWS_ZSFDC_PLD_DISC zws = new SapDiscountsPLDnew.ZWS_ZSFDC_PLD_DISC();
            SapDiscountsPLDnew.ZSFDC_PLD_DISCResponse_element   reponse_element  = new SapDiscountsPLDnew.ZSFDC_PLD_DISCResponse_element();
            SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB  tom = new SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB();
            SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_PLD  jerry = new SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_PLD();
            
            SapDiscountsPLDnew.ZSFDC_WORKDHEET_DEBIT_SLAB debit_item = new SapDiscountsPLDnew.ZSFDC_WORKDHEET_DEBIT_SLAB();

            debit_item.SCHEMENAME = 'SCHEMENAME';
			debit_item.PRETAX = 'PRETAX';
			debit_item.POSTTAX = 'POSTTAX';
			debit_item.DISCOUNT = 'DISCOUNT_RATE';
			debit_item.QUANTITY = '34';
			debit_item.DISCOUNT_RATE = 'DISCOUNT_RATE';
			debit_item.ELIGIBILITY = 'ELIGIBILITY';
			debit_item.DISCOUNT_TYPE = 'DISCOUNT_TYPE';

			tom.item = new List<SapDiscountsPLDnew.ZSFDC_WORKDHEET_DEBIT_SLAB>();
			tom.item.add(debit_item);

			SapDiscountsPLDnew.ZSFDC_WORKDHEET_PLD ppd_item = new SapDiscountsPLDnew.ZSFDC_WORKDHEET_PLD();

			ppd_item.SCHEMENAME = 'THIS';
	        ppd_item.PRETAX = 'VALUES';
	        ppd_item.POSTTAX = 'ARE';
	        ppd_item.AVG_SALES_VALUE = 'NOT';
	        ppd_item.SALES_VALUE_DISB = 'IMPORTANT';
	        ppd_item.SALES_QTY_DISB = 'I DONT';
	        ppd_item.AVG_QTY = 'KNOW';
	        ppd_item.DISCOUNT_RATE = 'WHY WE';
	        ppd_item.DISCOUNT = 'HAVE TO';
	        ppd_item.DISCOUNT_TYPE = 'DO THIS';

	        jerry.item = new List<SapDiscountsPLDnew.ZSFDC_WORKDHEET_PLD>();
	        jerry.item.add(ppd_item);

            reponse_element.DEBIT_NOTE_SLAB = tom;
            reponse_element.PLD = jerry ;
            
            
            
            SapDiscountsPLDnew.ZSFDC_PLD_DISC_element m = new SapDiscountsPLDnew.ZSFDC_PLD_DISC_element();
	            m.CIRNO ='2015-01-10';
	            m.DEBIT_NOTE_SLAB = tom;
	            m.KUNAG = '50003327';
	            m.PLD = jerry;
            
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }


    private class WebServiceMockSlab_Disc_Prod implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            Sap_DN_Slab_Disc_Prod scd  = new Sap_DN_Slab_Disc_Prod ();

            Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDiscResponse_element   reponse_element  = new Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDiscResponse_element();
            Sap_DN_Slab_Disc_Prod.TableOfZsd2851key  tom = new Sap_DN_Slab_Disc_Prod.TableOfZsd2851key();
            
            Sap_DN_Slab_Disc_Prod.Zsd2851key pr_item = new Sap_DN_Slab_Disc_Prod.Zsd2851key();

            pr_item.Mandt = 'DATA' ;
			pr_item.Kunag = 'DATA' ;
			pr_item.Crnum = 'DATA' ;
			pr_item.Name1 = 'DATA' ;
			pr_item.Sds = 'DATA' ;
			pr_item.Pre = 'DATA' ;
			pr_item.Post = 'DATA' ;
			pr_item.Kwert = 'DATA' ;
			pr_item.Disc = 'DATA' ;
			pr_item.Dwerk = 'DATA' ;
			pr_item.Fkimg = 'DATA' ;
			pr_item.Fkimg1 = 'DATA' ;
			pr_item.Discount = 'DATA' ;
			pr_item.Type2 = 'DATA' ;
			pr_item.Value = 'DATA' ;
			pr_item.Indcr = 'DATA' ;
			pr_item.Multi = 'DATA' ;
			pr_item.Vkbur = 'DATA' ;
			pr_item.Vkgrp = 'DATA' ;
			pr_item.Office = 'DATA' ;
			pr_item.Zgroup = 'DATA' ;
			pr_item.Superq = 'DATA' ;
			pr_item.Kwert1 = 'DATA' ;
			pr_item.Kwert2 = 'DATA' ;
			pr_item.Diff = 'DATA' ;
			pr_item.Flag = 'DATA' ;
			pr_item.Mwskz = 'DATA' ;
			pr_item.Nbp = 'DATA' ;

			tom.item = new List<Sap_DN_Slab_Disc_Prod.Zsd2851key>();
			tom.item.add(pr_item);

            reponse_element.ItOutput = tom;
            reponse_element.Return_x = 'SOME RESPONSE';
            
            
            
            Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDisc_element m = new Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDisc_element();
	            m.Crnum ='2015-01-10';
	            m.ItOutput = tom;
	            m.Kunag = '50003327';
            
            
            response.put('response_x', reponse_element);
            return;
        }
        
        
    }
}