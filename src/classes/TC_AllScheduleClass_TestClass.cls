/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TC_AllScheduleClass_TestClass {

    
    static testMethod void myUnitTest2() {
        Test.startTest();
        scheduledBatch_DeleteActuals temp = new scheduledBatch_DeleteActuals();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    
    static testMethod void myUnitTest3() {
        Test.startTest();
        scheduledBatch_Export temp = new scheduledBatch_Export();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    static testMethod void myUnitTest4() {
        Test.startTest();
        ScheduledBatch_ForSalesActual temp = new ScheduledBatch_ForSalesActual ();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    static testMethod void myUnitTest5() {
        Test.startTest();
        scheduledBatch_Rep_Dealer temp = new scheduledBatch_Rep_Dealer();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    static testMethod void myUnitTest6() {
        Test.startTest();
        scheduledBatch_Rep_Spe_Forcast temp = new scheduledBatch_Rep_Spe_Forcast();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    
    
    static testMethod void myUnitTest7() {
        Test.startTest();
        Scheduled_BatchForDashScoreCreation temp = new Scheduled_BatchForDashScoreCreation();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    /*
    static testMethod void myUnitTest8() {
        Test.startTest();
        scheduledBatch_SDS temp = new scheduledBatch_SDS ();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    */
    static testMethod void myUnitTest9() {
        Test.startTest();
        scheduledBatch_SpecialtyOE temp = new scheduledBatch_SpecialtyOE ();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    
    static testMethod void myUnitTest10() {
        Test.startTest();
        Scheduled_SAP_BatchForExCustShipTOCoun temp = new Scheduled_SAP_BatchForExCustShipTOCoun();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
     
     static testMethod void myUnitTest11() {
        Test.startTest();
        Schedule_DashboardStaging_BatchClass temp = new Schedule_DashboardStaging_BatchClass();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
    //Added by supriya
    //Delete sp staging of prevous month
    static testMethod void myUnitTest12() {
        Test.startTest();
        ScheduledBatch_ForSPStagingForDelete temp = new ScheduledBatch_ForSPStagingForDelete();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp); 
        Test.stopTest();
    }
     
}