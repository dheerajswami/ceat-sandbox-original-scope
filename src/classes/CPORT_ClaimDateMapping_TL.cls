global class CPORT_ClaimDateMapping_TL {

  /*
* Auther  :- Sneha Agrawal	
* Purpose :- Fetch Claim Details from SAP 
* Date 	  :- 17/June/2015
*
*/
    
    //-- ATTRIBUTES
    
    public static final String CUST_ID_LENGTH 	= Label.CPORT_LengthOfCustomerId;
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static List<CPORT_ClaimDateMapping.ClaimDataMapping> getAllClaimData(String territory_code, String fDate, String tDate){
        
        List<CPORT_ClaimDateMapping.ClaimDataMapping> claimData = new List<CPORT_ClaimDateMapping.ClaimDataMapping>();        
        try{
            
            SAPLogin__c saplogin 							= SAPLogin__c.getValues('SAP Login');
                        
            String username                 				= saplogin.username__c;
            String password 								= saplogin.password__c;
            Blob headerValue 								= Blob.valueOf(username + ':' + password);
            String authorizationHeader 						= 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            sapZWS_CLAIM_DATA_TL tst 					= new sapZWS_CLAIM_DATA_TL();
            sapZWS_CLAIM_DATA_TL.ZWS_CLAIM_DATA_TL zws 	= new sapZWS_CLAIM_DATA_TL.ZWS_CLAIM_DATA_TL();
            
            zws.inputHttpHeaders_x 							= new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x 									= 60000;
            sapZWS_CLAIM_DATA_TL.TableOfZsd232 tom 		= new sapZWS_CLAIM_DATA_TL.TableOfZsd232();
            
            
            List<sapZWS_CLAIM_DATA_TL.Zsd232> m 			= new List<sapZWS_CLAIM_DATA_TL.Zsd232>();        
            
            tom.item = m;
            
            sapZWS_CLAIM_DATA_TL.TableOfZsd232 e = new sapZWS_CLAIM_DATA_TL.TableOfZsd232();
            
            e = zws.ZbapiClaimDataTl(territory_code, fDate, tom, tDate);
            
            if(e.item != null){            
                for(sapZWS_CLAIM_DATA_TL.Zsd232 z : e.item) {
                    claimData.add(new CPORT_ClaimDateMapping.ClaimDataMapping(z.Mandt, z.Exnum, z.Qmnum, z.Mawerk, z.Erdat, z.Kunum, z.Name1, z.Zname1, z.Addr, z.Matnr, z.Matkl, z.Maktx, z.Vkgrp, z.Zserialnr, z.Insdate, z.Fegrp, z.Fecod, z.Text, z.Actnsd, z.Vper, z.Kbetr, z.Netwr, z.Closs, z.Vbeln, z.Vbeln1, z.Fkdat1, z.Fkdat, z.Inspby, z.Docket, z.Telf2));
                }
            }            
            return claimData;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
 
}