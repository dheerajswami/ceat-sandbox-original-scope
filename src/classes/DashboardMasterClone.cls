public  class DashboardMasterClone { 
    Dashboard_Master__c currentRecord;
    id dsa;
    //Standard constructor method
    public DashboardMasterClone(ApexPages.StandardController controller) 
    {
        currentRecord = (Dashboard_Master__c)controller.getRecord();
       // dsa = ApexPages.currentPage().getParameters().get('id');
        //System.debug('---------------------debug');
    }
    public PageReference cloneRecord(){
        Dashboard_Master__c newRecord;
        Savepoint sp = Database.setSavepoint();
        try{
            currentRecord = [Select id, Role__c, Active__c from Dashboard_Master__c where Dashboard_Master__c.id =:currentRecord.Id];
            newRecord = currentRecord.clone(false);
            insert newRecord;
            currentRecord.Active__c=false;
            upsert currentRecord;
            
            List<Dashboard_Weightage_Master__c> cons = new List<Dashboard_Weightage_Master__c>();  
            List<Dashboard_Weightage_Master__c> updatecon = new List<Dashboard_Weightage_Master__c>();    
            List<Dashboard_Weightage_Master__c> con = [SELECT  id,Testing__c,Category__c,Category_Code__c,Parameters_Inout__c,Parameters_Output__c,Role__c,Sort_Order__c,SYS_Used_For_TLD__c,Weightage__c  FROM Dashboard_Weightage_Master__c WHERE Dashboard_Master__c = : currentRecord.Id];
            for(Dashboard_Weightage_Master__c c : con){
                Dashboard_Weightage_Master__c conCopy = c.clone(false);
                c.Testing__c = false;
                updatecon.add(c);
                conCopy.Dashboard_Master__c = newRecord.Id;
                conCopy.Testing__c = true;
                cons.add(conCopy);
            }
            insert cons;
            update updatecon;
            System.debug('---------------------debug'+currentRecord.id);
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
        return new PageReference('/'+newRecord.id); 
        
    }
    
}