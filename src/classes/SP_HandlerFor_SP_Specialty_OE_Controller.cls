public with sharing class SP_HandlerFor_SP_Specialty_OE_Controller{
    
    //Wrapper classes
    public class DealerWrapper{
        public String dlName {get;set;}
        public String link {get;set;}
        public Double plannedTargetRunning {get;set;}
        public Double plannedTargetRunningValue {get;set;}
        public Double plannedTargetValue {get;set;}
        public String dealerTotalPlan{get;set;}
        public String dealerTotalPlanValue{get;set;}
        public String dlNumber {get;set;}
        public String countryCode{get;set;}
        //public String dlCountryCode {get;set;}
        public boolean lockedTL{get;set;}
        public List<categoryWrapper> catList{get;set;}
    }    
    public class categoryWrapper {
        public String skuName{get;set;}
        public String skuCode{get;set;}        
        public String skuTarget{get;set;}
        public String skuRunning{get;set;}
        
        public String totalL3M{get;set;}
        public String totalLYCM{get;set;}
        public String skuGap{get;set;}
        public Double skuValueTarget{get;set;}
        public String skuASP{get;set;}
        public String budget{get;set;}
        public String Lycm{get;set;}
        public String L3M{get;set;}
        public String planned{get;set;}
        public String dlName{get;set;}
        public String spDLID{get;set;}
        public String zoneCode{get;set;}
        public String catPlannedVal{get;set;}
        //public Decimal columnOrder{get;set;}
        
        public Boolean saveit{get;set;}     
        /*public Integer compareTo(Object compareTo) {
categoryWrapper compareToCat = (categoryWrapper)compareTo;
if (columnOrder == compareToCat.columnOrder) return 0;
if (columnOrder > compareToCat.columnOrder) return 1;
return -1;        
}  */
    }
    public List<DealerWrapper> getRMWiseSalesPlanningRecords(String loggedInUserZone,String loggedInUserId){
        
        String managerLabel = System.Label.ExportSrManager; 
        
        String month;
        String year;
        Integer day;    
        Integer fromDay;
        Integer toDay; 
        Integer unlFromDay;
        Integer unlToDay;    
        Integer totalPlannedOneDL       = 0;
        Integer totalPlannedOneDL1      = 0;
        Integer totalPlannedAllDL   = 0;
        Integer totalPlannedAllDL1  = 0;
        Integer totalL3MAllDL       = 0;
        Integer totalL3MAllDL1      = 0;
        Integer totalLYCMAllDL      = 0;
        Integer totalLYCMAllDL1     = 0;
        Integer totalGapAllDL       = 0;
        Integer totalTargetGap      = 0;
        Integer totalRunningGap     = 0;
        Integer plan                = 0;
        Integer makeGapzero         = 0;
        Double totalTargetValAllDL = 0;
        Double totalTargetValAllDL1 = 0;
        Double plannedTargetValue1  = 0;
        Double plannedTargetValue2  = 0;
        Decimal totalPlannedOneValueDL  = 0.0;
        Decimal totalPlannedOneValueDL1 = 0.0;
        List<UserTerritory2Association> territoryForExportManager   = new List<UserTerritory2Association>();
        String exportDealer             = System.Label.Export_Dealer; 
        String exportForcast            = System.Label.Export_Forcast_BU;
        
        
        String recTypeExportForecast    = System.Label.Export_Forcast;
        String AcwLockedRecTypeLabel                     = System.Label.Locking;
        String AcwUnlockedRecTypeLabel                   = System.Label.Unlocking;
        String speOEPage                                = System.Label.Specialty_OEPage;
        String clustermaninterr                          = system.Label.terrSrManagerExport;
        String speOEDealerRec = system.label.Specialty_OE;
        String speOEForcastRec = system.label.Specialty_OE_Forcast;
        String gmLabel                 =System.Label.Sp_GM_Role;
        Double runningTotal                              = 0;
        Decimal totalVal                                 = 0;
        String dlsTemp;
        Map<String,Decimal> mapOFPlannedValueDealer                     = new map<String,Decimal>();
        Map<String,List<Sales_Planning__c>> mapOfDLSales  = new map<String,List<Sales_Planning__c>>();       
        Map<String,Sales_Planning__c> mapOfDLSP           = new Map<String,Sales_Planning__c>();
        Map<String,Integer> mapSkuWithTotalL3M            = new map<String,Integer>();
        Map<String,Integer> mapSkuWithTotalLYCM            = new map<String,Integer>();
        Map<String,Id> mapDLNameId                        =new Map<String,Id>();
        Map<String,Integer> mapOfDLPlannedTotal           = new map<String,Integer>();
        Map<String,Double> mapOfPlannedTargetRunning      = new map<String,Double>();
        Map<String,Double> mapOfPlannedValue             = new map<String,Double>();
        Map<String,Double> mapSkuWithTotalTargetVal        = new map<String,Double>(); 
        ACW__c AcwLockedRecord                            = new ACW__c();  
        ACW__c AcwUnlockedRecord                          = new ACW__c();  
        List<Locking_Screen__c> alreadyLocked             = new list<Locking_Screen__c>(); 
        List<Locking_Screen__c> alreadyLocked1             = new list<Locking_Screen__c>();
        List<Locking_Screen__c> alreadyLocked2             = new list<Locking_Screen__c>();  
        
        
        List<Territory2> countryUnderCluster  =new List<Territory2>();
        
        List<String> parentRegIdList                      =new List<String>();
        List<String> parentRegCodeList                    =new List<String>();         
        List<Territory2> tlTerritory                      = new List<Territory2>();
        List<Territory2> rmTerritory                      = new List<Territory2>(); 
        List<UserTerritory2Association> clusterForExportManager = new List<UserTerritory2Association>();
        List<UserTerritory2Association> zoneForRM1 = new List<UserTerritory2Association>();
        list<Sales_Planning__c > salesPlanningSpeOEForcase = new list<Sales_Planning__c>();
        Sales_Planning__c salesPlanTemp ;
        Sales_Planning__c salesPlanningDL ;
        Sales_Planning__c salesPlanningDL1 ;
        Sales_Planning__c salesPlanningDLRec;
        Sales_Planning__c salesPlanningDLTemp;
        Sales_Planning__c salesPlanningDealerplan;
        Sales_Planning__c salesPlanningDealerplanValue;
        List<Sales_Planning__c> salesPlan;
        Server_Url__c serverUrl;
        List<Sales_Planning__c> existingSalesPlanningDLSpeOE = new list<Sales_Planning__c>();  
        Map<String,Integer> mapOFPlannedDealer                          = new map<String,Integer>();
        Map<String,Integer> mapOfSDS ;
        Map<String,Integer> mapOfSpecialtyVal ;
        Map<String,Integer> mapOfSpeOESKUTarget             = new map<String,Integer>();
        Map<String,Integer> mapOfSpeOEForSKUASP                = new map<String,Integer>();
        Map<String,String> mapOfDLAndNum ;
        Map<String,String> mapOfCatCodeAndCatName        = new map<String,String>();
        Map<String,String> mapOfDLAndSysTL               = new Map<String,String>();
        Map<String,String> mapOfDLNameAndRegCode         = new Map<String,String>();
        Map<String,Integer> mapOfTLPlannedTotal          = new map<String,Integer>();
        Map<String,Integer> mapOfDLGapTotal              = new map<String,Integer>();
        Map<String,Sales_Planning__c> mapOfCatAdDL       = new Map<String,Sales_Planning__c> ();
        Map<String,String> mapOfDLAndCluster             = new Map<String,String>();
        Map<String,String> mapOfDLAndCountry             = new Map<String,String>();
        
        Set<String> skuSet= new set<String>();
        Set<Id> clusterId                                = new Set<Id>();
        Set<String> zoneCodeset                           = new Set<String>();
        Set<String> setOfDL                              =new Set<String>();        
        Set<String> countryCode                            = new set<String>();
        Id specailtyOEForcastId= [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEForcastRec Limit 1].Id;
        Id specailtyOEDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEDealerRec Limit 1].Id;
        //Id replaceTLId           = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
        Id acwLockedRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =: AcwLockedRecTypeLabel Limit 1].Id;
        Id acwUnlockedRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =: AcwUnlockedRecTypeLabel Limit 1].Id;
        
        Date d                  = system.today();              
        month                   = String.valueOf(d.month());
        year                    = String.valueOf(d.Year());
        day                     = d.day();
        
        try{
            AcwLockedRecord=[Select Page__c,Sales_Planning__c,From_Date__c,To_Date__c,RecordTypeId from ACW__c where RecordTypeId=:acwLockedRecTypeId  AND Page__c=:speOEPage AND Sales_Planning__c=true AND Month__c=:d.month() AND Year__c=:String.valueof(d.year()) limit 1];
            if(AcwLockedRecord!=null){
                date fromDate = AcwLockedRecord.From_Date__c;
                fromDay = fromDate.day();
                date toDate = AcwLockedRecord.To_Date__c;
                toDay = toDate.day();
            }
            AcwUnlockedRecord=[Select Page__c,User__c,Sales_Planning__c,Month__c,Year__c,From_Date__c,To_Date__c,RecordTypeId from ACW__c where RecordTypeId=:acwUnlockedRecTypeId AND Page__c=:speOEPage AND Sales_Planning__c=true AND user__c=:loggedInUserId AND (From_Date__c <= TODAY OR To_Date__c >= TODAY) AND Month__c=:d.month() AND Year__c=:String.valueOf(d.year()) limit 1];
            system.debug('---AcwUnlockedRecord-------------'+AcwUnlockedRecord);
            if(AcwUnlockedRecord!=null){
                date unFromDate = AcwUnlockedRecord.From_Date__c;
                unlFromDay = unFromDate.day();
                date unToDate = AcwUnlockedRecord.To_Date__c;
                unlToDay = unToDate.day();
            }
        }catch(Exception e){}     
        List<UserRole> userTLRol1 = [SELECT DeveloperName,Id FROM UserRole WHERE  DeveloperName =: gmLabel ];       
        List<UserRole> userSpeOERol  = [SELECT DeveloperName,Id FROM UserRole WHERE (DeveloperName  =: System.Label.SRM_SpecialtyUrole OR DeveloperName  =: System.Label.AM_SpecialtyURole OR DeveloperName  =: System.Label.RM_Specialty)];
        system.debug(userSpeOERol+'userSpeOERol');
        //List<UserRole> userTLRol1 = [SELECT DeveloperName,Id FROM UserRole WHERE  DeveloperName =: rmLabel ];        
        if(userSpeOERol.size() > 0 && (UserInfo.getUserRoleId() == userSpeOERol[0].Id || UserInfo.getUserRoleId() == userSpeOERol[1].Id || UserInfo.getUserRoleId() == userSpeOERol[2].Id ||  UserInfo.getUserRoleId() == userTLRol1[0].Id)){ 
            
            zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:loggedInUserId and  (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole)];
            for(UserTerritory2Association  te : zoneForRM1){
                zoneCodeset.add(te.Territory2.name);
            }
            system.debug(zoneCodeset+'zoneCodeset');
        }
        salesPlanningSpeOEForcase = [SELECT Id,Value__c,SKU_Name__c,ASP__c,SKU__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Zone__c  FROM Sales_Planning__c WHERE  Zone__c IN : zoneCodeset AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specailtyOEForcastId];
        system.debug(salesPlanningSpeOEForcase.size()+'salesPlanningSpeOEForcase');
        system.debug(salesPlanningSpeOEForcase+'salesPlanningSpeOEForcase');
        if(salesPlanningSpeOEForcase.size() > 0){
            for(Sales_Planning__c temp :salesPlanningSpeOEForcase ){      
                skuSet.add(temp.SKU__c);          
                mapOfSpeOEForSKUASP.put(temp.SKU__c,Integer.valueOf(temp.Value__c));
                mapOfCatCodeAndCatName.put(temp.SKU__c,temp.SKU_Name__c);
                mapOfSpeOESKUTarget.put(temp.SKU__c,Integer.valueOf(temp.Target_Quantity__c));
                //mapOfPlannedValue.put(temp.Category__c,decimal.ValueOf((Integer.valueOf(temp.ASP__c)*Integer.valueOf(temp.Target_Quantity__c))));
            }
        }
        system.debug(mapOfSpeOESKUTarget+'mapOfSpeOESKUTarget');
        
        alreadyLocked = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Approved' AND BU__c = 'OE'];
        alreadyLocked1 = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Submitted' AND BU__c = 'OE'];
        alreadyLocked2 = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Rejected' AND BU__c = 'OE'];
        //system.debug('---------alreadyLocked'+alreadyLocked); 
        
        existingSalesPlanningDLSpeOE = [SELECT id,Value__c,SKU__c,NBP__c,Zone__c,Account_Name__c,Dealer__r.Name,Dealer_CustNumber__c,Dealer_Name__c,Parent_Sales_Planning__c ,Category__c,Category_Description__c ,Month__c ,Year__c  ,RecordTypeId  ,SPExternalIDTL__c,Target_Quantity__c,LYCM__c,L3M__c,Total_LYCM__c,Total_L3M__c,Budget__c FROM Sales_Planning__c WHERE Zone__c IN : zoneCodeset AND Year__c =:year AND Month__c =:month AND  RecordTypeId =: specailtyOEDealerId AND SKU__c In :skuSet AND Target_Quantity__c >=0];
        
        system.debug(existingSalesPlanningDLSpeOE.size()+'existingSalesPlanningDLSpeOE');
        system.debug(existingSalesPlanningDLSpeOE+'existingSalesPlanningDLSpeOE');
        if(existingSalesPlanningDLSpeOE .size() > 0){
            mapOfDLSP = new map<String,Sales_Planning__c>(); 
            mapOfDLAndNum = new map<String,String>();
            setOfDL = new Set<String>();
            String key;
            for(Sales_Planning__c temp: existingSalesPlanningDLSpeOE ){ 
                
                key=temp.Dealer_CustNumber__c +temp.Zone__c+ temp.SKU__c;                  
                mapOfDLSP.put(key,temp);
                setOfDL.add(temp.Account_Name__c+':'+temp.Dealer_CustNumber__c);//temp.Dealer__r.Name
                system.debug(temp+'SSSSS');
                mapOfDLAndNum.put(temp.Account_Name__c+':'+temp.Dealer_CustNumber__c,temp.Dealer_CustNumber__c);
                
                system.debug(temp.Dealer__r.Name+'Account_Name__c');////temp.Dealer__r.Name
                system.debug('------------------------------key'+key);
                //mapOfDLAndSysTL.put(temp.Dealer__r.Name,temp.Territory_Code__c); 
                //mapOfDLNameAndRegCode.put(temp.Dealer__r.Name,temp.Region_Code__c);            
            }
        }
        System.debug(mapOfDLSP+'mapOfDLSP');
        //calculating Total Plan for each Dealer
        if(mapOfDLSP.size() > 0){
            mapOFPlannedDealer =  new map<String,Integer>();
            String st = '';
            for(String key : mapOfDLSP.keySet()){
                totalPlannedOneDL = 0;
                totalPlannedOneDL1 = 0;
                salesPlanningDealerplan = new Sales_Planning__c();
                salesPlanningDealerplan = mapOfDLSP.get(key);
                //system.debug(key+'GGGGG');
                //system.debug(mapOfDLSP.get(key)+'%%%%%%');
                //system.debug(mapOfDLSP.get(key).Target_Quantity__c+'$$$$$');
                
                if(mapOFPlannedDealer.containsKey(salesPlanningDealerplan.Dealer_CustNumber__c)){
                    totalPlannedOneDL = mapOFPlannedDealer.get(salesPlanningDealerplan.Dealer_CustNumber__c);
                    system.debug(mapOFPlannedDealer.get(salesPlanningDealerplan.Dealer_CustNumber__c)+'GGGG');
                    if(salesPlanningDealerplan.Target_Quantity__c != null){
                        system.debug(Integer.ValueOF(salesPlanningDealerplan.Target_Quantity__c)+'HHHH');
                        totalPlannedOneDL = totalPlannedOneDL + Integer.ValueOF(salesPlanningDealerplan.Target_Quantity__c);
                    }
                    system.debug(Integer.ValueOF(salesPlanningDealerplan.Target_Quantity__c)+'HHHH');
                    mapOFPlannedDealer.put(salesPlanningDealerplan.Dealer_CustNumber__c,totalPlannedOneDL);
                }else{
                    if(salesPlanningDealerplan.Target_Quantity__c != null){
                        totalPlannedOneDL1 = Integer.ValueOF(salesPlanningDealerplan.Target_Quantity__c);
                    }
                    mapOFPlannedDealer.put(salesPlanningDealerplan.Dealer_CustNumber__c,totalPlannedOneDL1);
                }
            }
        }
        
        //calculating Total Plan Value for each Dealer
        if(mapOfDLSP.size() > 0){
            mapOFPlannedValueDealer =  new map<String,Decimal>();
            
            for(String key : mapOfDLSP.keySet()){
                totalPlannedOneValueDL = 0;
                totalPlannedOneValueDL1 = 0;
                salesPlanningDealerplanValue = new Sales_Planning__c();
                salesPlanningDealerplanValue = mapOfDLSP.get(key);
                system.debug(key+'GGGGG');
                system.debug(mapOfDLSP.get(key)+'%%%%%%');
                system.debug(mapOfDLSP.get(key).Value__c+'$$$$$');
                
                if(mapOFPlannedValueDealer.containsKey(salesPlanningDealerplanValue.Dealer_CustNumber__c)){
                    totalPlannedOneValueDL = mapOFPlannedValueDealer.get(salesPlanningDealerplanValue.Dealer_CustNumber__c);
                    if(salesPlanningDealerplanValue.Value__c != null){
                        totalPlannedOneValueDL = totalPlannedOneValueDL + salesPlanningDealerplanValue.Value__c;
                    }
                    mapOFPlannedValueDealer.put(salesPlanningDealerplanValue.Dealer_CustNumber__c,totalPlannedOneValueDL);
                }else{
                    if(salesPlanningDealerplanValue.Value__c != null){
                        totalPlannedOneValueDL1 = salesPlanningDealerplanValue.Value__c;
                    }
                    mapOFPlannedValueDealer.put(salesPlanningDealerplanValue.Dealer_CustNumber__c,totalPlannedOneValueDL1);
                }
            }
        }
        //Calculating planned target running values
        if(mapOfDLSP.size() > 0){
            for (String key : mapOfDLSP.keySet()) {
                salesPlanTemp = new Sales_Planning__c();
                salesPlanTemp = mapOfDLSP.get(key);
                dlsTemp= salesPlanTemp.Account_Name__c+':'+salesPlanTemp.Dealer_CustNumber__c;  //changed      
                
                if(mapOfPlannedTargetRunning.containsKey(dlsTemp)){
                    runningTotal=mapOfPlannedTargetRunning.get(dlsTemp);  
                    if(salesPlanTemp.Target_Quantity__c == null) salesPlanTemp.Target_Quantity__c = 0;
                    //runningTotal += salesPlanTemp.Target_Quantity__c;
                    if(salesPlanTemp.NBP__c != null){
                        runningTotal += salesPlanTemp.Target_Quantity__c * salesPlanTemp.NBP__c;
                    }
                    
                    // system.debug('---salesPlanTemp.Target_Quantity__c'+salesPlanTemp.Target_Quantity__c);
                    //system.debug('---mapOfTLCatASP.get(salesPlanTemp.category__c)'+mapOfTLCatASP.get(salesPlanTemp.category__c));
                    mapOfPlannedTargetRunning.put(dlsTemp,runningTotal);
                    runningTotal=0;
                }
                else{      
                    if(salesPlanTemp.Target_Quantity__c == null) salesPlanTemp.Target_Quantity__c = 0;
                    if(salesPlanTemp.NBP__c != null){                       
                        runningTotal += salesPlanTemp.Target_Quantity__c * salesPlanTemp.NBP__c;
                    }
                    mapOfPlannedTargetRunning.put(dlsTemp,runningTotal);                    
                }                    
            }  
        }       
        
        //calculating Running total for each category for each DL under that category
        if(mapOfDLSP.size() > 0){
            for(String key : mapOfDLSP.keySet()){
                
                totalPlannedAllDL = 0;
                totalPlannedAllDL1 = 0;
                
                salesPlanningDL1 = new Sales_Planning__c();
                salesPlanningDL1 = mapOfDLSP.get(key);
                
                if(mapOfDLPlannedTotal.containsKey(salesPlanningDL1.SKU__c)){
                    totalPlannedAllDL1 = mapOfDLPlannedTotal.get(salesPlanningDL1.SKU__c);
                    totalPlannedAllDL1 += Integer.ValueOf(salesPlanningDL1.Target_Quantity__c);
                    mapOfDLPlannedTotal.put(salesPlanningDL1.SKU__c,totalPlannedAllDL1);
                    totalPlannedAllDL1 = 0;
                }
                else{
                    if(salesPlanningDL1.Target_Quantity__c != null){
                        totalPlannedAllDL += Integer.ValueOf(salesPlanningDL1.Target_Quantity__c);
                        mapOfDLPlannedTotal.put(salesPlanningDL1.SKU__c,totalPlannedAllDL);
                    }
                }                      
            }  
            //system.debug('------------------mapOfDLPlannedTotal'+mapOfDLPlannedTotal);                                    
        } 
        //calculating total target value 
        if(mapOfDLSP.size() > 0){
            
            for(String key : mapOfDLSP.keySet()){
                totalTargetValAllDL = 0;
                totalTargetValAllDL1 = 0;
                
                
                salesPlanningDL1 = new Sales_Planning__c();
                salesPlanningDL1 = mapOfDLSP.get(key);
                system.debug(salesPlanningDL1 +'salesPlanningDL1');
                if(mapSkuWithTotalTargetVal.containsKey(salesPlanningDL1.SKU__c)){
                    
                    
                    totalTargetValAllDL1 = mapSkuWithTotalTargetVal.get(salesPlanningDL1.SKU__c);
                    //if(salesPlanningDL1.NBP__c!= null && salesPlanningDL1.Target_Quantity__c!= null){
                    system.debug(totalTargetValAllDL1 +'totalTargetValAllDL1');
                    if(salesPlanningDL1.NBP__c==null)salesPlanningDL1.NBP__c=0;
                    if(salesPlanningDL1.Target_Quantity__c==null)salesPlanningDL1.Target_Quantity__c=0;
                    //system.debug();
                    totalTargetValAllDL1 += Integer.ValueOf(salesPlanningDL1.Target_Quantity__c) * Integer.ValueOf(salesPlanningDL1.NBP__c);
                    //}
                    system.debug(salesPlanningDL1.Target_Quantity__c +'salesPlanningDL1.Target_Quantity__c');
                    system.debug(salesPlanningDL1.NBP__c +'salesPlanningDL1.NBP__c');
                    mapSkuWithTotalTargetVal.put(salesPlanningDL1.SKU__c,totalTargetValAllDL1 );
                    totalTargetValAllDL1 = 0;
                }
                else{
                    
                    
                    if(salesPlanningDL1.NBP__c==null)salesPlanningDL1.NBP__c=0;
                    if(salesPlanningDL1.Target_Quantity__c==null)salesPlanningDL1.Target_Quantity__c=0;
                    if(salesPlanningDL1.NBP__c != null && salesPlanningDL1.Target_Quantity__c != null){
                        totalTargetValAllDL += Integer.ValueOf(salesPlanningDL1.Target_Quantity__c) * Integer.ValueOf(salesPlanningDL1.NBP__c);
                    }else{
                        totalTargetValAllDL = 0;    
                    }
                    
                    
                    mapSkuWithTotalTargetVal.put(salesPlanningDL1.SKU__c,totalTargetValAllDL);
                    
                }                      
            }  
            system.debug('------------------mapSkuWithTotalTargetVal'+mapSkuWithTotalTargetVal);                                    
        } 
        
        if(mapSkuWithTotalTargetVal.size() > 0){
            plannedTargetValue2 = 0;
            for(String key : mapSkuWithTotalTargetVal.keySet()){
                plannedTargetValue2 += mapSkuWithTotalTargetVal.get(key);
                
            }
        }
        //for cal plannedTargetValue 
        if(mapOfSpeOEForSKUASP.size() > 0){
            plannedTargetValue1 = 0;
            for(String key : mapOfSpeOEForSKUASP.keySet()){
                if(mapOfSpeOEForSKUASP.get(key) != null){
                    plannedTargetValue1 += mapOfSpeOEForSKUASP.get(key);
                }
                
                
            }
        }
        if(mapOfDLSP.size() > 0){
            
            for(String key : mapOfDLSP.keySet()){
                totalL3MAllDL = 0;
                totalL3MAllDL1 = 0;
                
                
                salesPlanningDL1 = new Sales_Planning__c();
                salesPlanningDL1 = mapOfDLSP.get(key);
                system.debug(salesPlanningDL1 +'salesPlanningDL1');
                if(mapSkuWithTotalL3M.containsKey(salesPlanningDL1.SKU__c)){
                    
                    
                    totalL3MAllDL1 = mapSkuWithTotalL3M.get(salesPlanningDL1.SKU__c);
                    if(salesPlanningDL1.L3M__c!= null){
                        totalL3MAllDL1 += Integer.ValueOf(salesPlanningDL1.L3M__c);
                    }
                    
                    mapSkuWithTotalL3M.put(salesPlanningDL1.SKU__c,totalL3MAllDL1);
                    totalL3MAllDL1 = 0;
                }
                else{
                    
                    if(salesPlanningDL1.L3M__c!= null){
                        totalL3MAllDL += Integer.ValueOf(salesPlanningDL1.L3M__c);
                        
                    }
                    
                    mapSkuWithTotalL3M.put(salesPlanningDL1.SKU__c,totalL3MAllDL);
                }                      
            }  
            system.debug('------------------mapSkuWithTotalL3M'+mapSkuWithTotalL3M);                                    
        } 
        
        if(mapOfDLSP.size() > 0){
            
            for(String key : mapOfDLSP.keySet()){
                totalLYCMAllDL = 0;
                totalLYCMAllDL1 = 0;
                
                
                salesPlanningDL1 = new Sales_Planning__c();
                salesPlanningDL1 = mapOfDLSP.get(key);
                system.debug(salesPlanningDL1 +'salesPlanningDL1');
                if(mapSkuWithTotalLYCM.containsKey(salesPlanningDL1.SKU__c)){
                    
                    
                    totalLYCMAllDL1 = mapSkuWithTotalLYCM.get(salesPlanningDL1.SKU__c);
                    if(salesPlanningDL1.LYCM__c!= null){
                        totalLYCMAllDL1+= Integer.ValueOf(salesPlanningDL1.LYCM__c);
                    }
                    
                    mapSkuWithTotalLYCM.put(salesPlanningDL1.SKU__c,totalLYCMAllDL1);
                    totalLYCMAllDL1= 0;
                }
                else{
                    
                    if(salesPlanningDL1.LYCM__c!= null){
                        totalLYCMAllDL += Integer.ValueOf(salesPlanningDL1.LYCM__c);
                        
                    }
                    
                    mapSkuWithTotalLYCM.put(salesPlanningDL1.SKU__c,totalLYCMAllDL);
                }                      
            }  
            system.debug('------------------mapSkuWithTotalL3M'+mapSkuWithTotalL3M);                                    
        }  
        //Calculate Gap for each category from each DL under that category
        if(mapOfDLSP.size() > 0){
            for(String key : mapOfSpeOESKUTarget.keySet()){
                totalGapAllDL = 0;
                totalTargetGap = 0;
                totalRunningGap = 0;               
                
                if(mapOfSpeOESKUTarget.get(key) != null && mapOfDLPlannedTotal.get(key) != null){
                    totalTargetGap =mapOfSpeOESKUTarget.get(key) ;
                    totalRunningGap =mapOfDLPlannedTotal.get(key) ;
                    totalGapAllDL = totalTargetGap - totalRunningGap ;
                    
                }else if(mapOfSpeOESKUTarget.get(key)  != null && mapOfDLPlannedTotal.get(key)  == null){                
                    totalTargetGap = mapOfSpeOESKUTarget.get(key) ;
                    totalRunningGap = 0;
                    totalGapAllDL = totalTargetGap - totalRunningGap ;
                }
                mapOfDLGapTotal.put(key,totalGapAllDL);                
            }               
        } 
        system.debug('------------------mapOfDLGapTotal'+mapOfDLGapTotal); 
        system.debug('------------------mapOfDLAndNum'+mapOfDLAndNum); 
        system.debug(setOfDL+'setOfDL');
        List<String> dlList=new List<String>();
        dlList.addAll(setOfDL);
        dlList.sort();        
        system.debug('dlList---'+dlList);       
        List<DealerWrapper> dlWiseRecord      = new List<DealerWrapper>();  
        String dd=''; 
        for(String key : mapOfPlannedValue.keySet()){
            
            totalVal += mapOfPlannedValue.get(key);
        }
        serverUrl=[select url__c from Server_Url__c limit 1]; 
        for(String dls : dlList){
            DealerWrapper c       =   new DealerWrapper();  
            String[] str1          =   dls.split(':');        
            c.dlName              =   str1[0];            
            c.catList             =   new List<categoryWrapper>();
            c.dlNumber            =   mapOfDLAndNum.get(dls);  
            if(plannedTargetValue1 != null){
                c.plannedTargetValue  = plannedTargetValue1;
            }else{
                c.plannedTargetValue  = 0;
            }
            
            c.plannedTargetRunningValue = plannedTargetValue2; 
            c.dealerTotalPlan       = String.valueOf(mapOFPlannedDealer.get(mapOfDLAndNum.get(dls)));
            if(String.valueOf(mapOFPlannedValueDealer.get(mapOfDLAndNum.get(dls))) != null){
                c.dealerTotalPlanValue  = String.valueOf(mapOFPlannedValueDealer.get(mapOfDLAndNum.get(dls)));
            }else{
                c.dealerTotalPlanValue = String.valueOf(0);
            }
            if(mapOfPlannedTargetRunning.get(dls)!=null){
                
                c.plannedTargetRunning=mapOfPlannedTargetRunning.get(dls);
            }else{
                c.plannedTargetRunning        =   0;
            }
            
            if(UserInfo.getUserId() != loggedInUserId){
                if(alreadyLocked1.size() > 0){
                    // c.link = 'https://cs5.salesforce.com/'+alreadyLocked1[0].id;
                    c.link = serverUrl.url__c+'/'+alreadyLocked1[0].id;
                }else if(alreadyLocked.size() > 0){
                    //c.link = 'https://cs5.salesforce.com/'+alreadyLocked[0].id;
                    c.link = serverUrl.url__c+'/'+alreadyLocked[0].id;
                }else if(alreadyLocked2.size() > 0){
                    //c.link = 'https://cs5.salesforce.com/'+alreadyLocked2[0].id;
                    c.link = serverUrl.url__c+'/'+alreadyLocked2[0].id;
                }
            }else{
                c.link = '';
            }
            
            if(loggedInUserId == UserInfo.getUserId())  {
                
                if(alreadyLocked1.size() > 0){ 
                    //c.lockedTL          =    true;
                    
                    if(unlFromDay!=null && unlToDay!=null && day >= unlFromDay && day <= unlToDay){
                        c.lockedTL          =    false;
                    }else{
                        c.lockedTL          =    alreadyLocked1[0].Submitted__c;
                    }
                }
                
                else{
                    if(day >= fromDay && day <= toDay){  
                        system.debug('------Inside-----else if----------day'+day);
                        c.lockedTL          =    false;
                    }else if(unlFromDay!=null && unlToDay!=null && day >= unlFromDay && day <= unlToDay){
                        c.lockedTL          =    false;
                    }else{
                        c.lockedTL          =    true;
                    }               
                }
                
                if(alreadyLocked2.size() > 0 && day >= fromDay && day <= toDay){
                    c.lockedTL          =    false;
                }
                if(alreadyLocked.size() > 0 ){
                    c.lockedTL          =    true;
                }
            }
            else{
                c.lockedTL          =    true;
            }
            //system.debug('------------------mapOfDLAndNum'+mapOfDLAndNum); 
            for(String catname:    skuSet ){              
                
                categoryWrapper cc = new categoryWrapper();
                cc.totalL3M = String.valueOf(mapSkuWithTotalL3M.get(catname));
                cc.totalLYCM = String.valueOf(mapSkuWithTotalLYCM.get(catname));
                cc.skuValueTarget= Double.valueOf(mapOfSpeOEForSKUASP.get(catname));
                
                if(mapOfSpeOESKUTarget.containsKey(catname) && mapOfSpeOESKUTarget.get(catname) > 0){
                    
                    cc.saveit    = false;
                    cc.skuName   = mapOfCatCodeAndCatName.get(catname);
                    
                    cc.skuCode   = catname; 
                     
                    cc.dlName    = str1[0];
                    cc.skuTarget = String.valueOf(mapOfSpeOESKUTarget.get(catname));
                    //cc.clusterCode  = mapOfDLAndCluster.get(dls);
                    cc.zoneCode  = loggedInUserZone;
                    
                    
                    if(cc.skuTarget==null){
                        cc.skuTarget=String.valueOf(0);
                    }
   
                    cc.skuGap           = String.valueOf(0); 
                    cc.skuRunning         = String.valueOf(mapOfSpeOESKUTarget.get(catname));                                  

                    system.debug(dls+'DLS'+mapOfDLAndNum.get(dls)+'number'+loggedInUserZone+'loggedInUserZone#'+catname+'catname#'+mapOfDLAndNum.get(dls)+loggedInUserZone+catname+'final');         
                    system.debug('----------IIII'+mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname)); 
                    //system.debug('------------------Cat'+mapOfDLAndNum.get(dls) + catname+dls);         
                    system.debug('before');
                    if(mapOfDLSP.size() > 0 && mapOfDLSP != null && mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname) != null){
                        
                        system.debug('insideif');
                        if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Value__c != null){                            
                            cc.catPlannedVal = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Value__c); 
                            
                        }else{
                            cc.catPlannedVal = String.valueOf(0);                             
                        }
                        if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).NBP__c != null){
                            cc.skuASP    = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).NBP__c);
                        }else{
                            cc.skuASP    = String.valueOf(0);    
                        }
                        if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Budget__c != null){                            
                            cc.budget = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Budget__c);                             
                        }else{
                            cc.budget = String.valueOf(0);                             
                        }
                        if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).LYCM__c) != null){                           
                            cc.Lycm = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).LYCM__c);                              
                            
                        }else{
                            cc.Lycm = String.valueOf(0);
                        }
                        if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).L3M__c) != null){
                            system.debug('inside l3m');
                            cc.L3M = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).L3M__c); 
                            system.debug(cc.L3M);
                        }else{
                            system.debug('else l3m');
                            cc.L3M = String.valueOf(0);
                        }                        
                        if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Target_Quantity__c) != null){
                            system.debug(catname+'XXXX'+mapOfDLGapTotal.get(catname)+'vvvvvv');
                            system.debug('------------------HHHH'+mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Target_Quantity__c); 
                            system.debug('------------------Cat'+mapOfDLAndNum.get(dls) + catname);
                            plan = 0;
                            makeGapzero = 0;
                            cc.planned = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).Target_Quantity__c); 
                            system.debug(cc.planned+'RRRR');  
                        }else{
                            cc.planned = String.valueOf(0);
                        }
                        if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).SPExternalIDTL__c) != null){
                            cc.spDLID = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname).SPExternalIDTL__c);                              
                            system.debug(cc.spDLID+'cc.spDLID');
                        }else{
                            cc.spDLID = String.valueOf(0);
                        } 
                    }
                    else if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+loggedInUserZone+catname) == null){   
                        system.debug('after');                      
                        cc.budget = String.valueOf(0);
                        cc.L3M = String.valueOf(0);
                        cc.Lycm = String.valueOf(0);
                        cc.skuASP = String.ValueOf(0);
                        cc.catPlannedVal = String.valueOf(0);
                        cc.planned = String.valueOf(0);
                        
                    }
                    c.catList.add(cc);
                    //c.catList.sort();
                }
            }
            dlWiseRecord.add(c);
        }
        return dlWiseRecord;
        
    }
}