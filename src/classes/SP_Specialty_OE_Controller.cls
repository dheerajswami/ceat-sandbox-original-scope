global class SP_Specialty_OE_Controller {    
    
    //public String loggedInUserId    {get;set;}
    public String loggedInUserName  {get;set;}
    public String loggedInUserZone  {get;set;}
    public String loggedinUserID {get;set;}
    public Datetime myDatetime ;
    public String myDatetimeStr {get;set;}
    User userName;
    String zoneOFRMinterr                          = system.Label.Specialty_RM_Role;
    
    List<UserTerritory2Association> zoneForRM1 = new List<UserTerritory2Association>();
    global  SP_Specialty_OE_Controller () { 
        /*
        loggedinuserID  =ApexPages.currentPage().getParameters().get('id');
        if(loggedinuserID =='' ||  loggedinuserID ==null ){
            loggedinuserID  =UserInfo.getUserId();
        }*/
        myDatetime = Datetime.now();
        myDatetimeStr = myDatetime.format('MMMM, yyyy');
        loggedInUserId=ApexPages.currentPage().getParameters().get('id');
        if(loggedInUserId=='' || loggedInUserId==null ){
            loggedInUserId=UserInfo.getUserId();
        }
        /*
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId and RoleInTerritory2=: zoneOFRMinterr limit 1];
        if(userTerrCode.size() > 0){
            userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id limit 1];
        }
        if(userTerritory.size() > 0){
            loggedInTerritory=userTerritory[0].Name;
        }*/
        userName=[select name,id from User where id=:loggedInUserId limit 1];
        zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:loggedInUserId and  (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole)];
        loggedInUserZone  = zoneForRM1[0].Territory2.name;
        //loggedInUserName=userName.Name;
    } 
    
    //Used to get list of TL or Dealer depending on which user logged in    
    @RemoteAction
    Public Static List<SP_HandlerFor_SP_Specialty_OE_Controller.DealerWrapper>  getDealersOfExportManager (String loggedInUserZone,String loggedInUserId) {
            SP_HandlerFor_SP_Specialty_OE_Controller handler = new SP_HandlerFor_SP_Specialty_OE_Controller();
            return handler.getRMWiseSalesPlanningRecords(loggedInUserZone,loggedInUserId);
    }
    
    @RemoteAction
    Public Static void saveDealerExportRecord (Map<String,String> mapOfSalesPlanningValue,Map<String,String> mapOfSalesPlanning,String loggedInUserZone,String loggedInUserId) {
            system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
            system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
            
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String speOEDealerRec = system.label.Specialty_OE;
            String speOEForcastRec = system.label.Specialty_OE_Forcast;
            String recTypeExportForecast    = System.Label.Export_Forcast;
            String clustermaninterr                          = system.Label.terrSrManagerExport;
            String gmLabel                 =System.Label.gm_label_terr;
            Datetime myDatetime = Datetime.now();
            String myDatetimeStr = myDatetime.format('MMMM, yyyy');
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfSKUAndForcastId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOFCountryCodeAndName           = new map<String,String>();
            Map<String,String> mapOfDLIDAndValue            = new Map<String,String>();
            Account dealer;
            
            Server_Url__c serverUrl;
            List<Territory2> zoneTerritory            = new List<Territory2>();
            List<Sales_Planning__c> listOfSalesPlanningDLValue       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<Sales_Planning__c> upsertsalesPlanningDLListForVal     = new List<Sales_Planning__c>();
            List<UserTerritory2Association> zoneForRM1             = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningSpeOEForcase                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> countryUnderCluster                             = new List<Territory2>();
            List<Territory2> clusterNameExportManager                             = new List<Territory2>();
            List<UserTerritory2Association> regionForGM       = new List<UserTerritory2Association>();
            List<Territory2> gmTerritory            = new List<Territory2>();
            Locking_Screen__c lsforRM;
            Sales_Planning__c exDLRecord;
            Id specailtyOEForcastId= [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEForcastRec Limit 1].Id;
            Id specailtyOEDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEDealerRec Limit 1].Id;
            Set<Id> zoneId                              = new Set<Id>();
            Set<String> salesPlanningDLId           = new Set<String>();
            Set<String> zoneCodeset                  = new Set<String>();
            Set<String> salesPlanningDLIdValue          = new Set<String>();
            Set<Id> clusterId                        = new Set<Id>();
            Set<String> countryCode                 = new Set<String>();
            Set<String> catExpSet                   = new Set<String>();
            //list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
            
            zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and (RoleInTerritory2=: System.label.Specialty_RM_Role OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole)];
            for(UserTerritory2Association  te : zoneForRM1){
                zoneId.add(te.Territory2Id);
                zoneCodeset.add(te.Territory2.name);
            }
            zoneTerritory=  [Select ParentTerritory2Id,id,name from Territory2 where Id IN : zoneId];
            loggedInUserZone  =zoneTerritory[0].name;
            gmTerritory=  [Select id,name from Territory2 where id =:zoneTerritory[0].ParentTerritory2Id];
            regionForGM = [Select RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where Territory2Id =:gmTerritory[0].id and RoleInTerritory2=: gmLabel];
            User gmUser=  [select name,id,email from User where id=:regionForGM[0].UserId];
            salesPlanningSpeOEForcase = [SELECT Id,ASP__c,SKU__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Zone__c  FROM Sales_Planning__c WHERE  Zone__c IN : zoneCodeset AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specailtyOEForcastId];
            
            for(Sales_Planning__c temp :salesPlanningSpeOEForcase){
                    mapOfSKUAndForcastId.put(temp.SKU__c,temp.id);
                    //mapOfCatAndCatCode.put(temp.SKU__c,temp.Category_Description__c);
            }
            system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
            
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                    }
                    
                    else{
                            system.debug('inside if');
                            system.debug(listKey+'%%%%%');
                            dealer = new account(KUNNR__c=listKey[1]);
                            String upcase = listKey[2].toUpperCase(); 
                            st = listKey[1] + listKey[3]+listKey[2]+month +year ;
                            exDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfSKUAndForcastId.get(listKey[3]),SKU__c = listKey[3],
                            Month__c = month ,Zone__c = listKey[2],Year__c = year ,Dealer_Name__c= listKey[4],Dealer__r = dealer,Dealer_CustNumber__c= listKey[1],RecordTypeId = specailtyOEDealerId,Target_Quantity__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st,Value__c = Decimal.ValueOF(mapOfSalesPlanningValue.get(temp)));
                            listOfSalesPlanningDLForInsert.add(exDLRecord);
                            system.debug(st+'$$$$$');
                            system.debug(exDLRecord.OwnerId+'$$$$$');
                            system.debug(exDLRecord.Parent_Sales_Planning__c+'$$$$$');
                    }    
            }
            for(String temp : mapOfSalesPlanningValue.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLIdValue.add(listKey[0]);
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                            
                    }
            
            }
            listOfSalesPlanningDLValue = [SELECT id,SPExternalIDTL__c,Value__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLIdValue];
            system.debug(listOfSalesPlanningDLForInsert+'listOfSalesPlanningDLForInsert');
            system.debug(salesPlanningDLId+'salesPlanningDLId');
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Target_Quantity__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            system.debug(listOfSalesPlanningDL+'listOfSalesPlanningDL');
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String val = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Target_Quantity__c = Decimal.valueOf(val);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            if(listOfSalesPlanningDLValue.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDLValue){
                            //String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            
                            sp.Value__c = Decimal.ValueOf(val);
                            //sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLListForVal.add(sp);
                    }
            }
            system.debug(upsertsalesPlanningDLList.size()+'###');
            system.debug(upsertsalesPlanningDLList+'###');
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            if(upsertsalesPlanningDLListForVal.size() > 0 ){
                    upsert upsertsalesPlanningDLListForVal SPExternalIDTL__c;
            }
            upsertsalesPlanningDLList.clear();
             
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }
    
            alreadyLocked = [SELECT id,name,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:UserInfo.getUserId() AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND (Status__c = 'Submitted' OR Status__c = 'Rejected')];
            if(!(alreadyLocked.size() > 0)){
                system.debug('enter');
                lsforRM= new Locking_Screen__c(User__c = UserInfo.getUserId(),Month__c = month ,Year__c = year,Submitted__c = true,Status__c='Submitted',Territory_Code__c=loggedInUserZone, BU__c='OE',View_Sales_Planning__c='/apex/SP_Specialty_OE_v1?id='+loggedInUserId);
                insert lsforRM;
            }else{
                alreadyLocked[0].Status__c = 'Submitted';
                update alreadyLocked;
            }
            system.debug(alreadyLocked+'alreadyLocked');
            system.debug(lsforRM+'lsforRM');
            system.debug(loggedInUserId +'loggedInUserId');
            Id lockRecId=[select id from Locking_Screen__c where User__c =:loggedInUserId AND Month__c =:month AND Year__c =: year AND Submitted__c = true AND (status__c='Rejected' OR status__c='Submitted') LIMIT 1].Id;
            system.debug(lockRecId+'lockRecId');
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval...');
            req1.setObjectId(lockRecId);
           
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            //req2.setComments('Approving request.');
            //req2.setAction('Approve');
            req1.setNextApproverIds(new Id[] {gmUser.Id});
            Approval.ProcessResult result = Approval.process(req1);
            
             serverUrl=[select url__c from Server_Url__c limit 1];
             FeedItem fitem=new FeedItem();
             fItem.parentId=gmUser.Id;
             fItem.Title='Click here to Approve/Reject';
             fItem.body = 'Sales planning has been submitted for the month '+myDatetimeStr+' by Specialty Manager of '+loggedInUserZone+'. ';
             fItem.LinkUrl = serverUrl.url__c+'/apex/SP_Specialty_OE_v1?id='+loggedInUserId;
             insert fItem;
    }
    
    @RemoteAction
    Public Static void saveAsDraftDealerExportRecord (Map<String,String> mapOfSalesPlanningValue,Map<String,String> mapOfSalesPlanning,String loggedInUserZone,String loggedInUserId) {
            system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
            system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String speOEDealerRec = system.label.Specialty_OE;
            String speOEForcastRec = system.label.Specialty_OE_Forcast;
            String recTypeExportForecast    = System.Label.Export_Forcast;
            String clustermaninterr                          = system.Label.terrSrManagerExport;
            Map<String,String> mapOfDLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfSKUAndForcastId                  = new map<String,ID>();
            Map<String,String> mapOfCatAndCatCode           = new map<String,String>();
            Map<String,String> mapOFCountryCodeAndName           = new map<String,String>();
            Map<String,String> mapOfDLIDAndValue            = new Map<String,String>();
            Account dealer;
            
            List<Sales_Planning__c> listOfSalesPlanningDL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLValue       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> zoneForRM1             = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningDLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningSpeOEForcase                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> countryUnderCluster                             = new List<Territory2>();
            List<Territory2> clusterNameExportManager                             = new List<Territory2>();
            List<Sales_Planning__c> upsertsalesPlanningDLListForVal     = new List<Sales_Planning__c>();
            Locking_Screen__c lsforRM;
            Sales_Planning__c exDLRecord;
            Id specailtyOEForcastId= [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEForcastRec Limit 1].Id;
            Id specailtyOEDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speOEDealerRec Limit 1].Id;
            
            Set<String> salesPlanningDLId           = new Set<String>();
            Set<String> zoneCodeset                  = new Set<String>();
            Set<String> salesPlanningDLIdValue          = new Set<String>();
            
            //list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
            
            zoneForRM1 = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and (RoleInTerritory2=: System.Label.RM_Specialty OR RoleInTerritory2=:System.Label.AM_SpecialtyURole OR RoleInTerritory2=:System.Label.SRM_SpecialtyUrole)];
            for(UserTerritory2Association  te : zoneForRM1){
                zoneCodeset.add(te.Territory2.name);
            }
            salesPlanningSpeOEForcase = [SELECT Id,ASP__c,SKU__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Zone__c  FROM Sales_Planning__c WHERE  Zone__c IN : zoneCodeset AND Year__c =:year AND Month__c =:month AND RecordTypeId =: specailtyOEForcastId];
            
            for(Sales_Planning__c temp :salesPlanningSpeOEForcase){
                    mapOfSKUAndForcastId.put(temp.SKU__c,temp.id);
                    //mapOfCatAndCatCode.put(temp.SKU__c,temp.Category_Description__c);
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    //system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    system.debug(Decimal.ValueOF(mapOfSalesPlanningValue.get(temp))+'$$$$$');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLId.add(listKey[0]);
                            mapOfDLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                            
                    }
                    else{
                           //system.debug(Decimal.ValueOF(mapOfSalesPlanningValue.get(temp))+'$$$$$');
                           // system.debug(listKey+'%%%%%');
                            dealer = new account(KUNNR__c=listKey[1]);
                            String upcase = listKey[2].toUpperCase(); 
                            //system.debug(upcase+'upcase');
                            st = listKey[1] + listKey[3]+listKey[2]+month +year ;
                           // system.debug(st+'EEEE');
                            exDLRecord = new Sales_Planning__c(Parent_Sales_Planning__c = mapOfSKUAndForcastId.get(listKey[3]),SKU__c = listKey[3],
                            Month__c = month ,Zone__c = listKey[2],Year__c = year ,Dealer_Name__c= listKey[4],Dealer__r = dealer,Dealer_CustNumber__c= listKey[1],RecordTypeId = specailtyOEDealerId,Target_Quantity__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st,Value__c = Decimal.ValueOF(mapOfSalesPlanningValue.get(temp)));
                            listOfSalesPlanningDLForInsert.add(exDLRecord);
                           
                            //system.debug(exDLRecord.OwnerId+'$$$$$');
                            //system.debug(exDLRecord.Parent_Sales_Planning__c+'$$$$$');
                    }  
            }
           for(String temp : mapOfSalesPlanningValue.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningDLIdValue.add(listKey[0]);
                            mapOfDLIDAndValue.put(listKey[0],mapOfSalesPlanningValue.get(temp));
                            
                    }
            
            }
            listOfSalesPlanningDLValue = [SELECT id,SPExternalIDTL__c,Value__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLIdValue];
           
            //system.debug(listOfSalesPlanningDLForInsert+'listOfSalesPlanningDLForInsert');
            //system.debug(salesPlanningDLId+'salesPlanningDLId');
            listOfSalesPlanningDL = [SELECT id,SPExternalIDTL__c,Target_Quantity__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningDLId];
            //system.debug(listOfSalesPlanningDL+'listOfSalesPlanningDL');
            if(listOfSalesPlanningDL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDL){
                            String val = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Target_Quantity__c = Decimal.valueOf(val);
                            upsertsalesPlanningDLList.add(sp);
                    }
            }
            //system.debug(upsertsalesPlanningDLList.size()+'###');
            //system.debug(upsertsalesPlanningDLList+'###');
             if(listOfSalesPlanningDLValue.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningDLValue){
                            //String plan = mapOfDLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            String val = mapOfDLIDAndValue.get(sp.SPExternalIDTL__c);
                            
                            sp.Value__c = Decimal.ValueOf(val);
                            //sp.Total_planned__c = Decimal.valueOf(plan);
                            upsertsalesPlanningDLListForVal.add(sp);
                    }
            }
            if(upsertsalesPlanningDLList.size() > 0){
                    upsert upsertsalesPlanningDLList SPExternalIDTL__c;
            }
            if(upsertsalesPlanningDLListForVal.size() > 0 ){
                    upsert upsertsalesPlanningDLListForVal SPExternalIDTL__c;
            }
            upsertsalesPlanningDLList.clear();
            if(listOfSalesPlanningDLForInsert.size() > 0){
                    upsert listOfSalesPlanningDLForInsert SPExternalIDTL__c;
            }
            
             
            
            
    
    }
}