@isTest 
private class SP_Specialty_Controller_TestClass{
    static testmethod void testLoadData() {
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBURep = new Territory2 ();
        Territory2 terrtypeBUSpe = new Territory2 ();
        Territory2 terrtypeBUSpeCountry = new Territory2();
        Territory2 terrtypeZoneSpeBi = new Territory2 ();
        Territory2 terrtypeZoneSpeCentral = new Territory2 ();
        Territory2 terrtypeZoneSpePAT = new Territory2 ();
        Territory2 terrtypeZoneSpeRAN = new Territory2 ();
        Territory2 terrtypeZoneSpeBHP = new Territory2 ();
        Territory2 terrtypeZoneSpeIND = new Territory2 ();
        Territory2 terrtypeZoneSpePATC0081 = new Territory2 ();
        Territory2 terrtypeZoneSpeRANC0101 = new Territory2 ();
        Territory2 terrtypeZoneSpeBHPB0144 = new Territory2 ();
        Territory2 terrtypeZoneSpeINDB0141 = new Territory2 ();
        Account accrecord = new Account();
        Account accrecord1 = new Account();
        Account accrecord2 = new Account();
        Account accrecord3 = new Account();
        Discount_For_Specialty__c dis =  new Discount_For_Specialty__c();
        user userRMData = new user();
        user userRMPATData = new user();
        user userRMRANData = new user();
        user userGMData = new user();
        UserTerritory2Association spRMuser = new UserTerritory2Association ();
        UserTerritory2Association spTLPATuser = new UserTerritory2Association ();
        UserTerritory2Association spTLRANuser = new UserTerritory2Association ();
        UserTerritory2Association spGMuser   = new UserTerritory2Association();
        List<SP_HandlerForSP_Specialty_Controller.dlWrapper> Sprecords = new List<SP_HandlerForSP_Specialty_Controller.dlWrapper>();
        List<Sales_Planning__c> listOFSPSpeDealer = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeForZone = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeForZonequery = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeDealerquery = new List<Sales_Planning__c>();
        set<ID> setOFId =  new set<ID>();
        set<ID> setOFIDDeaker =  new set<ID>();
        Map<String,String> mapOfSalesPlanningValue = new Map<String,String>();
        Map<String,String> mapOfSalesPlanning = new Map<String,String>();
        String teststring ='';
        String sprmLabel             = system.label.RM_Specialty;
        String gmLabel                 =System.Label.gm_label_terr;
        id terrTypeBU ;
        id terrTypeRO ;
        id terrTypeTerr ;
        id terrTypeZone ;
        id terrTypeCountry ;
        // Load the test Sales planning Staging from the static resource
        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        insert terrModel;
        
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
        system.debug(listTerritoryType+'listTerritoryType');
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            if(t.DeveloperName == 'BUTest'){
                terrTypeBU = t.id;
            }
            if(t.DeveloperName == 'ROTestTest'){
                terrTypeRO = t.id;
            }
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            if(t.DeveloperName == 'ZoneTest'){
                terrTypeZone = t.id;
            }
            if(t.DeveloperName == 'CountryTest'){
                terrTypeCountry = t.id;
            }
        }
        system.debug(terrTypeBU+'terrTypeBU');
        //Created BU Territory
        terrtypeBURep =  CEAT_InitializeTestData.createTerritory('ReplacementsTest','ReplacementsTest',terrTypeBU,terrModel.id,null);
        insert terrtypeBURep;
        terrtypeBUSpe =  CEAT_InitializeTestData.createTerritory('Specialty - Replacement','Specialty_Replacement',terrTypeBU,terrModel.id,null);
        insert terrtypeBUSpe;
        terrtypeBUSpeCountry =  CEAT_InitializeTestData.createTerritory('HO','HO_test',terrTypeCountry,terrModel.id,terrtypeBUSpe.id);
        insert terrtypeBUSpeCountry;
        //Created Zone Territory
        terrtypeZoneSpeBi =  CEAT_InitializeTestData.createTerritory('Bihar/Jharkhand','BiharJharkhand_test',terrTypeZone,terrModel.id,terrtypeBUSpeCountry.id);
        insert terrtypeZoneSpeBi;
        terrtypeZoneSpeCentral =  CEAT_InitializeTestData.createTerritory('Central','Central_test',terrTypeZone,terrModel.id,terrtypeBUSpe.id);
        insert terrtypeZoneSpeCentral;
        //Created RO Territory
        terrtypeZoneSpePAT =  CEAT_InitializeTestData.createTerritory('PAT','PAT_test',terrTypeRO,terrModel.id,terrtypeZoneSpeBi.id);
        insert terrtypeZoneSpePAT;
        terrtypeZoneSpeRAN =  CEAT_InitializeTestData.createTerritory('RAN','RAN_test',terrTypeRO,terrModel.id,terrtypeZoneSpeBi.id);
        insert terrtypeZoneSpeRAN;
        terrtypeZoneSpeBHP =  CEAT_InitializeTestData.createTerritory('BHP','BHP_test',terrTypeRO,terrModel.id,terrtypeZoneSpeCentral.id);
        insert terrtypeZoneSpeBHP;
        terrtypeZoneSpeIND =  CEAT_InitializeTestData.createTerritory('IND','IND_test',terrTypeRO,terrModel.id,terrtypeZoneSpeCentral.id);
        insert terrtypeZoneSpeIND;
        //Created District Territory
        terrtypeZoneSpePATC0081 =  CEAT_InitializeTestData.createTerritory('C0081','C0081_test',terrTypeTerr,terrModel.id,terrtypeZoneSpePAT.id);
        insert terrtypeZoneSpePATC0081;
        terrtypeZoneSpeRANC0101 =  CEAT_InitializeTestData.createTerritory('C0101','C0101_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeRAN.id);
        insert terrtypeZoneSpeRANC0101;
        terrtypeZoneSpeBHPB0144 =  CEAT_InitializeTestData.createTerritory('B0144','B0144_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeBHP.id);
        insert terrtypeZoneSpeBHPB0144;
        terrtypeZoneSpeINDB0141 =  CEAT_InitializeTestData.createTerritory('B0141','B0141_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeIND.id);
        insert terrtypeZoneSpeINDB0141;
        userGMData = CEAT_InitializeTestData.createUser('mm','TestGM','kishlay.mathur@extentor.com','kishlayGM@ceat.com','1234','Specialty',null,'GM_Specialty');
        insert userGMData;
        userRMData = CEAT_InitializeTestData.createUser('mm','CCCCC','kishlay.mathur@extentor.com','kishlayRM@ceat.com','12235','Specialty',null,'RM_Specialty');
        insert userRMData;
        userRMPATData = CEAT_InitializeTestData.createUser('mm','TestRM','kishlay.mathur@extentor.com','kishlayRMPAT@ceat.com','4567','Specialty',null,'TL_Replacement');
        insert userRMPATData;
        userRMRANData = CEAT_InitializeTestData.createUser('mm','TestRM','kishlay.mathur@extentor.com','kishlayRMRAN@ceat.com','567','Specialty',null,'TL_Replacement');
        insert userRMRANData;
        spGMuser = CEAT_InitializeTestData.createUserTerrAsso(userGMData.id, terrtypeBUSpeCountry.id, 'GM_Specialty');
        insert spGMuser;
        spRMuser = CEAT_InitializeTestData.createUserTerrAsso(userRMData.id, terrtypeZoneSpeBi.id, 'RM_Specialty');
        insert spRMuser;
        spTLPATuser = CEAT_InitializeTestData.createUserTerrAsso(userRMPATData.id, terrtypeZoneSpePAT.id, 'Replacement_TL');
        insert spTLPATuser;
        spTLRANuser = CEAT_InitializeTestData.createUserTerrAsso(userRMRANData.id, terrtypeZoneSpeRAN.id, 'Replacement_TL');
        insert spTLRANuser;
        system.debug(userRMData.id+'userRMData');
        System.runAs(userRMData) {
             accrecord = CEAT_InitializeTestData.createAccountSpecialty('Specialty','AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081','5');
              accrecord1 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','PATNA TYRE HOUSE', '50014542', 'PAT', 'ZE01', 'C0081','5');
              accrecord2 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081','5');
              accrecord3 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','TYRE AVENUE', '50003565', 'PAT', 'ZE01', 'C0081','5');
              insert accrecord;
              insert accrecord1;
              insert accrecord2;
              insert accrecord3;
              system.debug(accrecord+'accrecord');
             Server_Url__c url= new Server_Url__c(Url__c = 'https://cs5.salesforce.com');
             insert url;
             List<sObject> listCatSpecialty = Test.loadData(Speciality_Sales_Planning_Categories__c.sObjectType, 'CatSpecialty');
             system.debug(listCatSpecialty +'listCatSpecialty');
             dis = CEAT_InitializeTestData.createDiscountCustomSetting();
             insert dis;
             listOFSPSpeForZone = CEAT_InitializeTestData.createSalesPlanningsSpeForcastZone('Specialty_Forcast_Zone');
             insert listOFSPSpeForZone;
             for(Sales_Planning__c sp : listOFSPSpeForZone){
                    setOFID.add(sp.id);
             }
             listOFSPSpeForZonequery = [Select Discount_On_NBP__c,id,Target_Quantity__c,Category__c from Sales_Planning__c Where id IN:setOFID];
             listOFSPSpeDealer = CEAT_InitializeTestData.createSalesPlanningsSpeDealer('Specialty');
             insert listOFSPSpeDealer;
             for(Sales_Planning__c sp : listOFSPSpeDealer){
                 sp.Dealer__c = accrecord1.id;
             }
             update listOFSPSpeDealer;
             system.debug(listOFSPSpeDealer+'listOFSPSpeDealer');
             Sprecords = SP_Specialty_Controller.getTLsOfRM(userRMData.id);
             teststring = '';
             for(Sales_Planning__c sp : listOFSPSpeDealer){
                setOFIDDeaker.add(sp.id);
             }
             listOFSPSpeDealerquery = [Select id,Value__c,Total_planned__c,Dealer__r.id,Dealer__c,Dealer_CustNumber__c,Dealer_Name__c,SPExternalIDTL__c,Category__c from Sales_Planning__c Where id IN:setOFIDDeaker];
             system.debug(listOFSPSpeDealerquery+'listOFSPSpeDealerquery');
             for(Sales_Planning__c sp : listOFSPSpeDealerquery){
                if(sp.SPExternalIDTL__c == '50015640PAT2240122014'){
                    teststring = sp.SPExternalIDTL__c+'@@'+sp.Dealer_Name__c+'@@'+sp.Category__c+'@@'+sp.Dealer_CustNumber__c+'@@'+sp.Dealer__r.id;
                    sp.Total_planned__c = 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(sp.Total_planned__c));
                    sp.Value__c = 12345;
                    mapOfSalesPlanningValue.put(teststring,String.ValueOf(sp.Value__c));
                }
             }
              String accid = accrecord.id;
              String accnameId = 'AJIT TYRES PVT. LIMITED'+':'+accrecord.id;
              teststring = 'undefined'+'@@'+accnameId+'@@'+'2240'+'@@'+'51000047'+'@@'+accid;
              mapOfSalesPlanning.put(teststring,'55');
              mapOfSalesPlanningValue.put(teststring,'45789');
              
              system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
              system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
              SP_Specialty_Controller.saveAsDraftSpRecord (mapOfSalesPlanningValue, mapOfSalesPlanning,userRMData.id);
             SP_Specialty_Controller.saveDLSpRecord(mapOfSalesPlanningValue, mapOfSalesPlanning,userRMData.id);
         // The following code runs as user 'u' 
         System.debug('Current User: ' + listOFSPSpeForZonequery);
         System.debug('Current Profile: ' + listOFSPSpeForZone); 
        }
        system.debug(spRMuser+'spRMuser');
        
        
    }
    static testmethod void testLoadData1() {
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBURep = new Territory2 ();
        Territory2 terrtypeBUSpeCountry = new Territory2();
        Territory2 terrtypeBUSpe = new Territory2 ();
        Territory2 terrtypeZoneSpeBi = new Territory2 ();
        Territory2 terrtypeZoneSpeCentral = new Territory2 ();
        Territory2 terrtypeZoneSpePAT = new Territory2 ();
        Territory2 terrtypeZoneSpeRAN = new Territory2 ();
        Territory2 terrtypeZoneSpeBHP = new Territory2 ();
        Territory2 terrtypeZoneSpeIND = new Territory2 ();
        Territory2 terrtypeZoneSpePATC0081 = new Territory2 ();
        Territory2 terrtypeZoneSpeRANC0101 = new Territory2 ();
        Territory2 terrtypeZoneSpeBHPB0144 = new Territory2 ();
        Territory2 terrtypeZoneSpeINDB0141 = new Territory2 ();
        Account accrecord = new Account();
        Account accrecord1 = new Account();
        Account accrecord2 = new Account();
        Account accrecord3 = new Account();
        Discount_For_Specialty__c dis =  new Discount_For_Specialty__c();
        user userRMData = new user();
        user userRMPATData = new user();
        user userRMRANData = new user();
        user userGMData = new user();
        UserTerritory2Association spRMuser = new UserTerritory2Association ();
        UserTerritory2Association spTLPATuser = new UserTerritory2Association ();
        UserTerritory2Association spTLRANuser = new UserTerritory2Association ();
        UserTerritory2Association spGMuser   = new UserTerritory2Association();
        List<SP_HandlerForSP_Specialty_Controller.dlWrapper> Sprecords = new List<SP_HandlerForSP_Specialty_Controller.dlWrapper>();
        List<Sales_Planning__c> listOFSPSpeDealer = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeForZone = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeForZonequery = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeDealerquery = new List<Sales_Planning__c>();
        set<ID> setOFId =  new set<ID>();
        set<ID> setOFIDDeaker =  new set<ID>();
        Map<String,String> mapOfSalesPlanningValue = new Map<String,String>();
        Map<String,String> mapOfSalesPlanning = new Map<String,String>();
        String teststring ='';
        String sprmLabel             = system.label.RM_Specialty;
        id terrTypeBU ;
        id terrTypeRO ;
        id terrTypeTerr ;
        id terrTypeZone ;
        id terrTypeCountry;
        // Load the test Sales planning Staging from the static resource
        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        insert terrModel;
        
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
        system.debug(listTerritoryType+'listTerritoryType');
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            if(t.DeveloperName == 'BUTest'){
                terrTypeBU = t.id;
            }
            if(t.DeveloperName == 'ROTestTest'){
                terrTypeRO = t.id;
            }
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            if(t.DeveloperName == 'ZoneTest'){
                terrTypeZone = t.id;
            }
            if(t.DeveloperName == 'CountryTest'){
                terrTypeCountry = t.id;
            }
        }
        system.debug(terrTypeBU+'terrTypeBU');
        //Created BU Territory
        terrtypeBURep =  CEAT_InitializeTestData.createTerritory('ReplacementsTest','ReplacementsTest',terrTypeBU,terrModel.id,null);
        insert terrtypeBURep;
        terrtypeBUSpe =  CEAT_InitializeTestData.createTerritory('Specialty - Replacement','Specialty_Replacement',terrTypeBU,terrModel.id,null);
        insert terrtypeBUSpe;
        terrtypeBUSpeCountry =  CEAT_InitializeTestData.createTerritory('HO','HO_test',terrTypeCountry,terrModel.id,terrtypeBUSpe.id);
        insert terrtypeBUSpeCountry;
        //Created Zone Territory
        terrtypeZoneSpeBi =  CEAT_InitializeTestData.createTerritory('Bihar/Jharkhand','BiharJharkhand_test',terrTypeZone,terrModel.id,terrtypeBUSpeCountry.id);
        insert terrtypeZoneSpeBi;
        terrtypeZoneSpeCentral =  CEAT_InitializeTestData.createTerritory('Central','Central_test',terrTypeZone,terrModel.id,terrtypeBUSpe.id);
        insert terrtypeZoneSpeCentral;
        //Created RO Territory
        terrtypeZoneSpePAT =  CEAT_InitializeTestData.createTerritory('PAT','PAT_test',terrTypeRO,terrModel.id,terrtypeZoneSpeBi.id);
        insert terrtypeZoneSpePAT;
        terrtypeZoneSpeRAN =  CEAT_InitializeTestData.createTerritory('RAN','RAN_test',terrTypeRO,terrModel.id,terrtypeZoneSpeBi.id);
        insert terrtypeZoneSpeRAN;
        terrtypeZoneSpeBHP =  CEAT_InitializeTestData.createTerritory('BHP','BHP_test',terrTypeRO,terrModel.id,terrtypeZoneSpeCentral.id);
        insert terrtypeZoneSpeBHP;
        terrtypeZoneSpeIND =  CEAT_InitializeTestData.createTerritory('IND','IND_test',terrTypeRO,terrModel.id,terrtypeZoneSpeCentral.id);
        insert terrtypeZoneSpeIND;
        //Created District Territory
        terrtypeZoneSpePATC0081 =  CEAT_InitializeTestData.createTerritory('C0081','C0081_test',terrTypeTerr,terrModel.id,terrtypeZoneSpePAT.id);
        insert terrtypeZoneSpePATC0081;
        terrtypeZoneSpeRANC0101 =  CEAT_InitializeTestData.createTerritory('C0101','C0101_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeRAN.id);
        insert terrtypeZoneSpeRANC0101;
        terrtypeZoneSpeBHPB0144 =  CEAT_InitializeTestData.createTerritory('B0144','B0144_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeBHP.id);
        insert terrtypeZoneSpeBHPB0144;
        terrtypeZoneSpeINDB0141 =  CEAT_InitializeTestData.createTerritory('B0141','B0141_test',terrTypeTerr,terrModel.id,terrtypeZoneSpeIND.id);
        insert terrtypeZoneSpeINDB0141;
        userGMData = CEAT_InitializeTestData.createUser('mm','TestGM','kishlay.mathur@extentor.com','kishlayGM@ceat.com','2345','Specialty',null,'GM_Specialty');
        insert userGMData;
        userRMData = CEAT_InitializeTestData.createUser('mm','TestRM','kishlay.mathur@extentor.com','kishlayRM@ceat.com','45','Specialty',null,'RM_Specialty');
        insert userRMData;
        userRMPATData = CEAT_InitializeTestData.createUser('mm','TestRM','kishlay.mathur@extentor.com','kishlayRMPAT@ceat.com','4567','Specialty',null,'TL_Replacement');
        insert userRMPATData;
        userRMRANData = CEAT_InitializeTestData.createUser('mm','TestRM','kishlay.mathur@extentor.com','kishlayRMRAN@ceat.com','4566','Specialty',null,'TL_Replacement');
        insert userRMRANData;
        spRMuser = CEAT_InitializeTestData.createUserTerrAsso(userRMData.id, terrtypeZoneSpeBi.id, 'RM_Specialty');
        insert spRMuser;
        spGMuser = CEAT_InitializeTestData.createUserTerrAsso(userGMData.id, terrtypeBUSpeCountry.id, 'GM_Specialty');
        insert spGMuser;
        spTLPATuser = CEAT_InitializeTestData.createUserTerrAsso(userRMPATData.id, terrtypeZoneSpePAT.id, 'TL_Replacement');
        insert spTLPATuser;
        spTLRANuser = CEAT_InitializeTestData.createUserTerrAsso(userRMRANData.id, terrtypeZoneSpeRAN.id, 'TL_Replacement');
        insert spTLRANuser;
       
        System.runAs(userRMData) {
              accrecord = CEAT_InitializeTestData.createAccountSpecialty('Specialty','AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081','5');
              accrecord1 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','PATNA TYRE HOUSE', '50014542', 'PAT', 'ZE01', 'C0081','5');
              accrecord2 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081','5');
              accrecord3 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','TYRE AVENUE', '50003565', 'PAT', 'ZE01', 'C0081','5');
              insert accrecord;
              insert accrecord1;
              insert accrecord2;
              insert accrecord3;
             ACW__c acw=new ACW__C();
             Id lockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName ='Locking' Limit 1].Id;
         
             acw.Page__c='SP_Specialty';
             acw.Sales_Planning__c=true;
             acw.From_Date__c= system.today(); 
             acw.To_Date__c = system.today()+1;  
             acw.RecordTypeId =lockingRecType;//locking Recored type 012O00000000bkYIAQ
             insert acw;  
                
             Server_Url__c url= new Server_Url__c(Url__c = 'https://cs5.salesforce.com');
             insert url;
             List<sObject> listCatSpecialty = Test.loadData(Speciality_Sales_Planning_Categories__c.sObjectType, 'CatSpecialty');
             dis = CEAT_InitializeTestData.createDiscountCustomSetting();
             insert dis;
             listOFSPSpeForZone = CEAT_InitializeTestData.createSalesPlanningsSpeForcastZone('Specialty_Forcast_Zone');
             insert listOFSPSpeForZone;
             for(Sales_Planning__c sp : listOFSPSpeForZone){
                    setOFID.add(sp.id);
             }
             listOFSPSpeForZonequery = [Select Discount_On_NBP__c,id,Target_Quantity__c,Category__c from Sales_Planning__c Where id IN:setOFID];
             listOFSPSpeDealer = CEAT_InitializeTestData.createSalesPlanningsSpeDealerWithoutPlanned('Specialty');
             insert listOFSPSpeDealer;
             for(Sales_Planning__c sp : listOFSPSpeDealer){
                 sp.Dealer__c = accrecord1.id;
             }
             update listOFSPSpeDealer;
             Sprecords = SP_Specialty_Controller.getTLsOfRM(userRMData.id);
             teststring = '';
             for(Sales_Planning__c sp : listOFSPSpeDealer){
                setOFIDDeaker.add(sp.id);
             }
             listOFSPSpeDealerquery = [Select id,Value__c,Dealer__c,Dealer__r.id,Total_planned__c,Dealer_CustNumber__c,Dealer_Name__c,SPExternalIDTL__c,Category__c from Sales_Planning__c Where id IN:setOFIDDeaker];
             system.debug(listOFSPSpeDealerquery+'listOFSPSpeDealerquery');
             for(Sales_Planning__c sp : listOFSPSpeDealerquery){
                if(sp.SPExternalIDTL__c == '50015640PAT2240122014'){
                    teststring = sp.SPExternalIDTL__c+'@@'+sp.Dealer_Name__c+'@@'+sp.Category__c+'@@'+sp.Dealer_CustNumber__c+'@@'+sp.Dealer__r.id;
                    sp.Total_planned__c = 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(sp.Total_planned__c));
                    sp.Value__c = 12345;
                    mapOfSalesPlanningValue.put(teststring,String.ValueOf(sp.Value__c));
                }
             }
             String accid = accrecord.id;
             String accnameId= 'AJIT TYRES PVT. LIMITED'+':'+accrecord.id;
              teststring = 'undefined'+'@@'+accnameId+'@@'+'2240'+'@@'+'51000047'+'@@'+accid;
              mapOfSalesPlanning.put(teststring,'55');
              mapOfSalesPlanningValue.put(teststring,'45789');
              //accrecord = CEAT_InitializeTestData.createAccount('AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081');
              //insert accrecord;
              system.debug(mapOfSalesPlanning+'mapOfSalesPlanning');
              system.debug(mapOfSalesPlanningValue+'mapOfSalesPlanningValue');
              SP_Specialty_Controller speController = new SP_Specialty_Controller();
              SP_Specialty_Controller.saveAsDraftSpRecord (mapOfSalesPlanningValue, mapOfSalesPlanning,userRMData.id);
             SP_Specialty_Controller.saveDLSpRecord(mapOfSalesPlanningValue, mapOfSalesPlanning,userRMData.id);
         // The following code runs as user 'u' 
         System.debug('Current User: ' + listOFSPSpeForZonequery);
         System.debug('Current Profile: ' + listOFSPSpeForZone); 
        }
        system.debug(spRMuser+'spRMuser');
        
        
    }
}