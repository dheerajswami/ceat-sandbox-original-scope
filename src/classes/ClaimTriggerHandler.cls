public class ClaimTriggerHandler {
    @future(callout=true)
    public static void sendClaimToSap(Set<Id> newClaimIds) {
        Map<String, String> matTypeToMatTypeCodeMap = new Map<String, String>();

        for(Material_Master_Sap__c tempMmS : [SELECT material_type__c,Material_Group_1__c,Material_Group_2__c,Mat_Grp2_Desc__c,Mat_Grp_Desc__c,Mat_Grp1_Desc__c FROM Material_Master_Sap__c]) {
            if(!matTypeToMatTypeCodeMap.containsKey(tempMmS.Mat_Grp2_Desc__c) && tempMmS.Mat_Grp2_Desc__c != null) {
               matTypeToMatTypeCodeMap.put(tempMmS.material_type__c, tempMmS.Material_Group_2__c);
            }
        }

        List<Claim__c> newClaims = [Select Actual_Inspection_Date__c,Year__c,Year_of_Mfg__c,Material_Description__c,Material_Group_Description__c,Dealer_Name__c,SAP_Code__c,Name,Material_Type__c,Material_Size__c,Material_Pattern__c,Material_Code__c,Serial_Number__c,Fitment__c,Inspection__c,
        							Dealer__c,Customer_Name__c,Customer_Mobile__c,Customer_Email__c,Disposition__c,Percentage_Wear__c,Defect_Type__c,NSD__c,Chassis_No__c,
        							Claim_Type__c,State__c,Street_1__c,Street_2__c,District__c,Town__c,Vehicle_Kms__c,Make_Model__c,Plant__c,Rim_Size__c,
        							Ply_Rating__c,Tyre_Type__c,Material_Group__c,Complaint__c,Pin_Code__c,Replacement_Amount__c,
        							NBP__c,Original_NSD__c, id from Claim__c where Id In : newClaimIds];
        
 

        sapClaimCL0.TableOfZsdcl0 toZsdc = new sapClaimCL0.TableOfZsdcl0();
        
        List<sapClaimCL0.Zsdcl0> zsdc = new List<sapClaimCL0.Zsdcl0>();  
        sapClaimCL0 sd = new sapClaimCL0();
        sapClaimCL0.Zsdcl0 sz;
        for(Claim__c c : newClaims) {
        	string inspectionDate;
        	if(c.Actual_Inspection_Date__c != null){
	        	string year = string.valueof((c.Actual_Inspection_Date__c).year());
	        	string month = string.valueof((c.Actual_Inspection_Date__c).month());
	        	if(month.length() == 1){
	        		month = '0'+ month; 
	        	}
	        	string day = string.valueof((c.Actual_Inspection_Date__c).day());
	        	if(day.length() == 1){
	        		day = '0'+ day;
	        	}
	        	inspectionDate = year+'-'+month+'-'+day;
        	}else{
        		inspectionDate = '';
        	}
        	
            sz = new sapClaimCL0.Zsdcl0();
            sz.DOCKET					= 	replaceNullfields(c.Name);
			sz.DealerSapCode			=	replaceNullfields(c.SAP_Code__c);
			sz.DealerName				= 	replaceNullfields(c.Dealer_Name__c);
			sz.CustomerName				= 	replaceNullfields(c.Customer_Name__c);
			sz.ClaimType				=	replaceNullfields(c.Claim_Type__c);
			sz.Street1					= 	replaceNullfields(c.Street_1__c);
			sz.Street2					= 	replaceNullfields(c.Street_2__c);
			sz.Telephone				= 	replaceNullfields(c.Customer_Mobile__c);
			sz.EMailAddress				= 	replaceNullfields(c.Customer_Email__c);
			sz.MaterialSize				= 	replaceNullfields(c.Material_Size__c);
			sz.TyrePattern				= 	replaceNullfields(c.Material_Pattern__c);
			sz.MaterialNumber			=	replaceNullfields(c.Material_Code__c);
			sz.MaterialDescription		=	replaceNullfields(c.Material_Description__c);
			sz.Fitment					=	replaceNullfields(c.Fitment__c);
			sz.VehicleModel				=	replaceNullfields(c.Make_Model__c);
			sz.YearOfManufacture		= 	replaceNullfields(c.Year_of_Mfg__c);
			sz.VehicleKilometer			= 	replaceNullfields(string.valueof(c.Vehicle_Kms__c));
			sz.Chassis					= 	replaceNullfields(c.Chassis_No__c);
			sz.Disposition				=	replaceNullfields(c.Disposition__c);
			sz.WearPercentage			=	replaceNullfields(string.valueof(c.Percentage_Wear__c));
			sz.DefectType				=	replaceNullfields(c.Defect_Type__c);
			sz.InspectedBy				= 	'';
			sz.InspectionDate			=	replaceNullfields(inspectionDate);
			sz.ActualNsd				=	replaceNullfields(c.Original_NSD__c);
			sz.NbpPrice					= 	replaceNullfields(string.valueof(c.NBP__c));
			sz.State					=	replaceNullfields(c.State__c);
			sz.District					=	replaceNullfields(c.District__c);
			sz.Town						=	replaceNullfields(c.Town__c);
			sz.TypeOrTubeOrFlap			=	matTypeToMatTypeCodeMap.get(replaceNullfields(c.Material_Type__c));
			sz.Rimsize					=	replaceNullfields(c.Rim_Size__c);
			sz.PlyRating				=	replaceNullfields(c.Ply_Rating__c);
			sz.TyreType					=	replaceNullfields(c.Tyre_Type__c);
			sz.MateialGroup				=	replaceNullfields(c.Material_Group__c);
			sz.MaterialGroupDescription	=	replaceNullfields(c.Material_Group_Description__c); 
			sz.Plant					=	replaceNullfields(c.Plant__c);
			sz.SlNo						=	replaceNullfields(c.Serial_Number__c);
			sz.Date1					=	replaceNullfields(c.Year__c);
            sz.Mandt = '300';
            system.debug('checkfina '+sz);
            //if(c.Outer_Inspect__c == true)
               // sz.OuterInspect = String.valueOf('X');//c.Outer_Inspect__c);
            //else
                //sz.OuterInspect = String.valueOf('');//c.Outer_Inspect__c);
            // Date format :- dd/mm/yyyy (10 chars)
            //sz.Erdat = leftPadding(String.valueOf(c.CreatedDate.day()), 2)+'/'+leftPadding(String.valueOf(c.CreatedDate.month()), 2)+'/'+leftPadding(String.valueOf(c.CreatedDate.year()), 4);
            //sz.Erdat = leftPadding(String.valueOf(c.CreatedDate.year()), 4) + '-' + leftPadding(String.valueOf(c.CreatedDate.month()), 2) + '-' + leftPadding(String.valueOf(c.CreatedDate.day()), 2);
            // Time format :- hh:mm:ss (10 chars), as it is 10 chars so I have added two spaces in the begining
            //sz.Etime = '  '+leftPadding(String.valueOf(c.CreatedDate.hour()), 2)+':'+leftPadding(String.valueOf(c.CreatedDate.minute()), 2)+':'+leftPadding(String.valueOf(c.CreatedDate.second()), 2);
            //sz.OuterInspect = String.valueOf('X');
            zsdc.add(sz);
        }                      
        toZsdc.item = zsdc;
        GetClaim0FromSAP.callWebServiceGetClaim0(toZsdc);
    }
    
    public static string replaceNullfields(string field){
    	string retString;
    	if(field != null){
    		retString = field;
    	}else{
    		retString = '';
    	}
    	return retString;
    }
    
    public static String leftPadding(String str, Integer digits) {
        if(str.length() < digits) {
            for(Integer i = digits-str.length(); i > 0; i--) {
                str = '0'+str;
            }    
        }
        return str;
        
    }
}