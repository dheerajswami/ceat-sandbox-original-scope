global class ScheduledBatch_ForSPStagingForDelete implements Schedulable{
   global void execute(SchedulableContext sc) {
      SP_Staging_BatchForDelete stagingBatch = new SP_Staging_BatchForDelete(); 
      database.executebatch(stagingBatch,200);
   }
}