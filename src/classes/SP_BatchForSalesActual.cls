/* ================================================
    @Name:  SP_BatchForSalesActual
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor tquila
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will run daily and update sales actual qty and sales actual value on daily basis in sp dealer wise records                                                                                                
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     22/07/2014      INITIAL DEVELOPMENT                                 
     2.0        Kishlay@extentor     10/02/2016      remove territory from external id of sp records 
     3.0        kishlay@extentor     29/02/2016      added region code 
======================================================================= */

global class SP_BatchForSalesActual implements Database.Batchable<sObject> {  
    String actual = 'Actual Replacement';
    Date d = system.today().adddays(-1);
    String month = String.valueOf(d.month());
    String year = String.valueOf(d.year());
    String repDealerLabel           = System.Label.Replacement_Dealer;
    Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
    List<Account> ListOfAccount = new list<account>();
    List<Sales_Planning__c> salesplanningList = new list<Sales_Planning__c>();
    List<Sales_Planning__c> updateSalesplanningList = new list<Sales_Planning__c>();
    List<Sales_Planning__c> insertSalesplanningList = new list<Sales_Planning__c>();
    set<String> setOfkey = new Set<String>();
    set<String> setOfExternalkey = new Set<String>();
    set<String> setOfKeyNotMatch = new Set<String>();
    Map<String,Account> mapOFcusNumAndID = new Map<String,Account>();
    Map<String,Decimal> mapOFSalesQuaActual = new Map<String,Decimal>();
    Map<String,Decimal> mapOFSalesValActual = new Map<String,Decimal>();
    Map<String,String> mapOFkeyandColonKey = new Map<String,String>();
    Map<String,Sales_Planing_Staging__c> mapOFkeyStagingrec = new Map<String,Sales_Planing_Staging__c>();
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        query = 'SELECT Id,OwnerId,ASP__c,Budget__c,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,'
        +'Territory_Code__c,NBP__c,Year__c,Reg_code__c,SPExternalIDTL__c,BU__c,SKU__c,MAKTX__c,Zone__c,Zone_Description__c,Value__c,Value_L3M__c,Value_LYCM__c '+
        'FROM Sales_Planing_Staging__c WHERE  BU__c = \''+actual+'\' AND Processed__c = false AND Year__c =\''+year+'\' AND Month__c =\''+month+'\'' ;
        
        return Database.getQueryLocator(query);
        //AND Reg_code__c = \''+KLP+'\' 
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        String month;
        String year;
        Date d = system.today().adddays(-1);
        month = String.valueOf(d.month());
        year = String.valueOf(d.Year());
        String keyCatRegDea = '';
        String keyCatRegDeaSemi = '';
        Account dealer;
        Sales_Planning__c spReplacementCatWise = new Sales_Planning__c();
        list<Sales_Planing_Staging__c> updateStaging = new list<Sales_Planing_Staging__c>();
        ListOfAccount = [SELECT id,name,Sales_District_Text__c,KUNNR__c,Sales_Group_Text__c From Account WHERE (NOT KUNNR__c  like '6%') AND KUNNR__c != null  ];
        for(Account acc : ListOfAccount){
            mapOFcusNumAndID.put(acc.KUNNR__c,acc);
        }
         for(sObject temp : scope){
            Sales_Planing_Staging__c spr =(Sales_Planing_Staging__c)temp;
            if(mapOFcusNumAndID.containsKey(spr.Dealer_CustNumber__c)){
                keyCatRegDea = mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).id+spr.Category__c+spr.Month__c+spr.Year__c ;
                keyCatRegDeaSemi = mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).id+':'+mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).Sales_Group_Text__c+':'+mapOFcusNumAndID.get(spr.Dealer_CustNumber__c).Sales_District_Text__c+':'+spr.Category__c+':'+spr.Month__c+':'+spr.Year__c ;
                mapOFSalesQuaActual.put(keyCatRegDea,spr.Target_Quantity__c);
                mapOFSalesValActual.put(keyCatRegDea,spr.Value__c);
                setOfkey.add(keyCatRegDea);
                mapOFkeyandColonKey.put(keyCatRegDea,keyCatRegDeaSemi);
                mapOFkeyStagingrec.put(keyCatRegDea,spr);
            }
           // 
            //catsetKey.add(keyCatRegDea);
        }
       
        
        salesplanningList = [SELECT id,UploadExternalID__c,Name,Dealer_CustNumber__c,Dealer__r.KUNNR__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,SAP_Actual__c,Territory_Code__c ,Actual_Sales_Value__c,Actual_Quantity__c,SYS_Used_IN_Batch__c,Dealer_Name__c ,Dealer__c,Dealer__r.Sales_District_Text__c,Dealer__r.Sales_Group_Text__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SPExternalIDTL__c IN : mapOFSalesQuaActual.keyset() AND Month__c =: month AND Year__c =: year];
        if(salesplanningList.size() > 0){
            for(Sales_Planning__c sp:salesplanningList){
                setOfExternalkey.add(sp.SPExternalIDTL__c);
                sp.Actual_Quantity__c = mapOFSalesQuaActual.get(sp.SPExternalIDTL__c);
                sp.Actual_Sales_Value__c = mapOFSalesValActual.get(sp.SPExternalIDTL__c);
                sp.Territory_Code__c = sp.Dealer__r.Sales_District_Text__c;
                sp.Region_Code__c = sp.Dealer__r.Sales_Group_Text__c;
                sp.UploadExternalID__c = sp.Dealer__r.KUNNR__c+sp.Category__c+sp.Month__c+sp.Year__c;
                updateSalesplanningList.add(sp);
            }   
        }
        
        if(updateSalesplanningList.size() > 0){
            try{
                database.upsert(updateSalesplanningList,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
            }catch(Exception e){
                      System.debug(e.getMessage());
            }
        }
        if(setOfExternalkey.size() > 0){
            for(String kext : mapOFSalesQuaActual.keyset()){
                if(setOfExternalkey.contains(kext)){
                    continue;
                }else{
                    setOfKeyNotMatch.add(kext);
                }
            }
        }
        system.debug(setOfKeyNotMatch.size()+'setOfKeyNotMatch');
        if(setOfKeyNotMatch.size() > 0){
            String keySet = '';
            String[] temp ;
            for(String str:setOfKeyNotMatch){
                keySet = mapOFkeyandColonKey.get(str);
                temp = keySet.split(':');
                
                if(temp.size() == 6 && mapOFkeyStagingrec.containskey(str)){
                    dealer = new Account (KUNNR__c = mapOFkeyStagingrec.get(str).Dealer_CustNumber__c);
                    spReplacementCatWise = new Sales_Planning__c(
                            Region_code__c = temp[1],
                            SAP_Actual__c = true,
                            Category__c = temp[3],
                            Category_Description__c = mapOFkeyStagingrec.get(str).Category_Description__c,
                            Month__c = mapOFkeyStagingrec.get(str).Month__c,Year__c = mapOFkeyStagingrec.get(str).Year__c,
                            Territory_Code__c = temp[2],
                            Dealer_CustNumber__c = mapOFkeyStagingrec.get(str).Dealer_CustNumber__c,
                            Dealer_Name__c =  mapOFkeyStagingrec.get(str).Dealer_Name__c,Dealer__r = dealer,
                            SPExternalIDTL__c = mapOFcusNumAndID.get(mapOFkeyStagingrec.get(str).Dealer_CustNumber__c).id+temp[3]+temp[4]+temp[5],
                            UploadExternalID__c = mapOFkeyStagingrec.get(str).Dealer_CustNumber__c+temp[3]+temp[4]+temp[5],
                            RecordTypeId = replaceDealerId,
                            Actual_Quantity__c = mapOFkeyStagingrec.get(str).Target_Quantity__c,
                            Actual_Sales_Value__c = mapOFkeyStagingrec.get(str).Value__c
                            );
                     insertSalesplanningList.add(spReplacementCatWise);
                }
                
            }
        }
        if(insertSalesplanningList.size() > 0){
            try{
                database.upsert(insertSalesplanningList,Sales_Planning__c.Fields.SPExternalIDTL__c,false);
            }catch(Exception e){
                      System.debug(e.getMessage());
            }
        }
        for(sObject temp : scope){
                Sales_Planing_Staging__c sps =(Sales_Planing_Staging__c)temp;
                sps.Processed__c = true;
                updateStaging.add(sps);
        }
        if(updateStaging.size() > 0){
            update updateStaging;
        }
    } 
    
    global void finish(Database.BatchableContext BC){
    
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForSalesActual Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForSalesActual" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
    
    }
}