public with sharing class PJP_CalculateSummary {
    
    public static void calculateSummary(Id pjpId) {
        User currentUser = [Select Id, Name, UserRole.Name from User where Id = :UserInfo.getUserId()];
        PJP__c currentPjp = [Select Id, RecordTypeId, RecordType.Name from PJP__c where Id =:pjpId];        

        String summary = '';

        if(currentPjp.RecordType.Name == Constants.buReplacement) {
            if(currentUser.UserRole.Name == Constants.replacementRoleTL || currentUser.UserRole.Name == Constants.replacementRoleCSTL) {
                summary = replacementSummaryTL(pjpId);
            } else if(currentUser.UserRole.Name == Constants.replacementRoleTLD) {
                summary = replacementSummaryTLD(pjpId);
            }
        } else if(currentPjp.RecordType.Name == Constants.buSpecialty) {
            summary = specialtySummary(pjpId);
        } else if(currentPjp.RecordType.Name == Constants.buExports) {

        } else if(currentPjp.RecordType.Name == Constants.buOE) {

        }       
        currentPjp.Summary__c = summary;
        update currentPjp;
    }

    public static String replacementSummaryTL(Id pjpId) {       
        AggregateResult[] groupedResults;
        Map<String, Integer> segmentPlannedMap = new Map<String, Integer>();
        Map<String, Integer> plannedMap = new Map<String, Integer>();
        String summary = '';

        groupedResults = [SELECT Count(Id) num, Dealer__r.Customer_Segment__c segment FROM Visits_Plan__c where Master_Plan__c = :pjpId And (Dealer__r.RecordType.Name = 'Dealer' Or Dealer__r.RecordType.Name = 'Customer') GROUP BY Dealer__r.Customer_Segment__c];
        String str;
        for(AggregateResult ar : groupedResults) {
            if(String.valueOf(ar.get('segment')) == null || String.valueOf(ar.get('segment')) == '') {
                str = 'Others';
            } else {
                str = String.valueOf(ar.get('segment'));
            }
            segmentPlannedMap.put(str, Integer.valueOf(ar.get('num')));            
        }       
        for(String plan : segmentPlannedMap.keySet()) {
            summary += plan + ' : ' + segmentPlannedMap.get(plan) + '\r\n';         
        }       

        groupedResults = [SELECT Count(Id) num, Dealer__r.RecordType.Name rtName FROM Visits_Plan__c where Master_Plan__c = :pjpId And Dealer__r.RecordType.Name = 'Prospect' Group By Dealer__r.RecordType.Name];
        for(AggregateResult ar : groupedResults) {
            plannedMap.put(String.valueOf(ar.get('rtName')), Integer.valueOf(ar.get('num')));          
        }
        
        groupedResults = [SELECT Count(Id) num, RecordType.Name rtName FROM Visits_Plan__c where Master_Plan__c = :pjpId And RecordType.Name = 'Influencer Visit' Group By RecordType.Name];
        for(AggregateResult ar : groupedResults) {
            plannedMap.put(String.valueOf(ar.get('rtName')), Integer.valueOf(ar.get('num')));          
        }
        for(String plan : plannedMap.keySet()) {        
            summary += plan + ' : ' + plannedMap.get(plan) + '\r\n';            
        }
        return summary;     
    }

    public static String replacementSummaryTLD(Id pjpId) {   
        String summary = '';        
        List<Visits_Plan__c> visitPlans = [SELECT Id FROM Visits_Plan__c where Master_Plan__c = :pjpId And RecordType.Name = 'DSE-Beat'];        
        summary = 'DSE - Beat Visits : '+visitPlans.size();
        return summary;     
    }

    public static String specialtySummary(Id pjpId) {   
        String summary = '';        
        Map<String, Integer> segmentPlannedMap = new Map<String, Integer>();
        Map<String, Integer> plannedMap = new Map<String, Integer>();

        AggregateResult[] groupedResults = [SELECT Count(Id) num, Dealer__r.Type segment FROM Visits_Plan__c where Master_Plan__c = :pjpId And RecordType.Name = 'Visit' GROUP BY Dealer__r.Type];
        String str;

        for(AggregateResult ar : groupedResults) {
            if(String.valueOf(ar.get('segment')) == null || String.valueOf(ar.get('segment')) == '') {
                str = 'Others';
            } else {
                str = String.valueOf(ar.get('segment'));
            }
            segmentPlannedMap.put(str, Integer.valueOf(ar.get('num')));            
        }       
        for(String plan : segmentPlannedMap.keySet()) {
            summary += plan + ' : ' + segmentPlannedMap.get(plan) + '\r\n';         
        }

        List<Visits_Plan__c> visitPlans = [SELECT Id FROM Visits_Plan__c where Master_Plan__c = :pjpId And RecordType.Name = 'Workshop'];        
        summary += 'Workshops : '+visitPlans.size();
        return summary;     
    }
}