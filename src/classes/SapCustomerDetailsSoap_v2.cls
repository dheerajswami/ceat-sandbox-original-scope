//Generated by wsdl2apex

public class SapCustomerDetailsSoap_v2 {
    public class TABLE_OF_ZBAPI_CUST_STR1 {
        public SapCustomerDetailsSoap_v2.ZBAPI_CUST_STR1[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class TABLE_OF_ZBAPI_CUST_STR {
        public SapCustomerDetailsSoap_v2.ZBAPI_CUST_STR[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class BAPIRETURN {
        public String TYPE_x;
        public String CODE;
        public String MESSAGE;
        public String LOG_NO;
        public String LOG_MSG_NO;
        public String MESSAGE_V1;
        public String MESSAGE_V2;
        public String MESSAGE_V3;
        public String MESSAGE_V4;
        private String[] TYPE_x_type_info = new String[]{'TYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CODE_type_info = new String[]{'CODE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_type_info = new String[]{'MESSAGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_NO_type_info = new String[]{'LOG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_MSG_NO_type_info = new String[]{'LOG_MSG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V1_type_info = new String[]{'MESSAGE_V1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V2_type_info = new String[]{'MESSAGE_V2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V3_type_info = new String[]{'MESSAGE_V3','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V4_type_info = new String[]{'MESSAGE_V4','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'TYPE_x','CODE','MESSAGE','LOG_NO','LOG_MSG_NO','MESSAGE_V1','MESSAGE_V2','MESSAGE_V3','MESSAGE_V4'};
    }
    public class ZBAPI_GET_CUSTOMER_DETAILS_element {
        public SapCustomerDetailsSoap_v2.TABLE_OF_BAPIRETURN RETURN_x;
        public SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR T_CUST;
        public SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR1 T_CUST1;
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] T_CUST_type_info = new String[]{'T_CUST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] T_CUST1_type_info = new String[]{'T_CUST1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'RETURN_x','T_CUST','T_CUST1'};
    }
    public class ZBAPI_CUST_STR {
        public String KUNNR;
        public String NAME1;
        public String TELF1;
        public String ORT01;
        public String REGIO;
        public String PSTLZ;
        public String LAND1;
        public String PARNR;
        public String SMTP_ADDR;
        public String FAX_NUMBER;
        public String TEL_NUMBER;
        public String PARVW;
        public String STR_SUPPL1;
        public String STR_SUPPL2;
        public String BZIRK;
        public String WAERS;
        public String KDGRP;
        public String VKBUR;
        public String VKGRP;
        public String VTWEG;
        public String SPART;
        public String KVGR1;
        public String KVGR2;
        public String KVGR3;
        public String LOEVM;
        public String CLUBT;
        public String PROS_CODE;
        public String CLUBT_D;
        private String[] KUNNR_type_info = new String[]{'KUNNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] NAME1_type_info = new String[]{'NAME1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TELF1_type_info = new String[]{'TELF1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ORT01_type_info = new String[]{'ORT01','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] REGIO_type_info = new String[]{'REGIO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PSTLZ_type_info = new String[]{'PSTLZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LAND1_type_info = new String[]{'LAND1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PARNR_type_info = new String[]{'PARNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SMTP_ADDR_type_info = new String[]{'SMTP_ADDR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] FAX_NUMBER_type_info = new String[]{'FAX_NUMBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TEL_NUMBER_type_info = new String[]{'TEL_NUMBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PARVW_type_info = new String[]{'PARVW','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STR_SUPPL1_type_info = new String[]{'STR_SUPPL1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STR_SUPPL2_type_info = new String[]{'STR_SUPPL2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BZIRK_type_info = new String[]{'BZIRK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WAERS_type_info = new String[]{'WAERS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KDGRP_type_info = new String[]{'KDGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKBUR_type_info = new String[]{'VKBUR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKGRP_type_info = new String[]{'VKGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VTWEG_type_info = new String[]{'VTWEG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPART_type_info = new String[]{'SPART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KVGR1_type_info = new String[]{'KVGR1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KVGR2_type_info = new String[]{'KVGR2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KVGR3_type_info = new String[]{'KVGR3','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOEVM_type_info = new String[]{'LOEVM','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CLUBT_type_info = new String[]{'CLUBT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PROS_CODE_type_info = new String[]{'PROS_CODE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CLUBT_D_type_info = new String[]{'CLUBT_D','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'KUNNR','NAME1','TELF1','ORT01','REGIO','PSTLZ','LAND1','PARNR','SMTP_ADDR','FAX_NUMBER','TEL_NUMBER','PARVW','STR_SUPPL1','STR_SUPPL2','BZIRK','WAERS','KDGRP','VKBUR','VKGRP','VTWEG','SPART','KVGR1','KVGR2','KVGR3','LOEVM','CLUBT','PROS_CODE','CLUBT_D'};
    }
    public class ZBAPI_GET_CUSTOMER_DETAILSResponse_element {
        public SapCustomerDetailsSoap_v2.TABLE_OF_BAPIRETURN RETURN_x;
        public SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR T_CUST;
        public SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR1 T_CUST1;
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] T_CUST_type_info = new String[]{'T_CUST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] T_CUST1_type_info = new String[]{'T_CUST1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'RETURN_x','T_CUST','T_CUST1'};
    }
    public class TABLE_OF_BAPIRETURN {
        public SapCustomerDetailsSoap_v2.BAPIRETURN[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZBAPI_CUST_STR1 {
        public String ANRED;
        public String NAME1;
        public String KUNNR;
        public String TELF1;
        public String FAX_NUMBER;
        public String SMTP_ADDR;
        public String STR_SUPPL1;
        public String STR_SUPPL2;
        public String ORT01;
        public String REGIO;
        public String PSTLZ;
        public String LAND1;
        public String GBDAT;
        public String WAERS;
        public String KDGRP;
        public String KATR6;
        public String KATR7;
        public String J_1IPANNO;
        public String J_1ICSTNO;
        public String TOWN;
        public String DIST;
        public String BEZEI;
        private String[] ANRED_type_info = new String[]{'ANRED','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] NAME1_type_info = new String[]{'NAME1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUNNR_type_info = new String[]{'KUNNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TELF1_type_info = new String[]{'TELF1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] FAX_NUMBER_type_info = new String[]{'FAX_NUMBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SMTP_ADDR_type_info = new String[]{'SMTP_ADDR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STR_SUPPL1_type_info = new String[]{'STR_SUPPL1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] STR_SUPPL2_type_info = new String[]{'STR_SUPPL2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ORT01_type_info = new String[]{'ORT01','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] REGIO_type_info = new String[]{'REGIO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PSTLZ_type_info = new String[]{'PSTLZ','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LAND1_type_info = new String[]{'LAND1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] GBDAT_type_info = new String[]{'GBDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WAERS_type_info = new String[]{'WAERS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KDGRP_type_info = new String[]{'KDGRP','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KATR6_type_info = new String[]{'KATR6','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KATR7_type_info = new String[]{'KATR7','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_1IPANNO_type_info = new String[]{'J_1IPANNO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] J_1ICSTNO_type_info = new String[]{'J_1ICSTNO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TOWN_type_info = new String[]{'TOWN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DIST_type_info = new String[]{'DIST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] BEZEI_type_info = new String[]{'BEZEI','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'ANRED','NAME1','KUNNR','TELF1','FAX_NUMBER','SMTP_ADDR','STR_SUPPL1','STR_SUPPL2','ORT01','REGIO','PSTLZ','LAND1','GBDAT','WAERS','KDGRP','KATR6','KATR7','J_1IPANNO','J_1ICSTNO','TOWN','DIST','BEZEI'};
    }
    public class ZWS_BAPI_GET_CUSTOMER_DETAILS {
        public String endpoint_x = 'http://CTECPHAP2.ceat.in:8000/sap/bc/srt/rfc/sap/zws_bapi_get_customer_details/300/zws_bapi_get_customer_details/zws_bapi_get_customer_details';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'SapCustomerDetailsSoap_v2'};
        public SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element ZBAPI_GET_CUSTOMER_DETAILS(SapCustomerDetailsSoap_v2.TABLE_OF_BAPIRETURN RETURN_x,SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR T_CUST,SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR1 T_CUST1) {
            SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILS_element request_x = new SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILS_element();
            request_x.RETURN_x = RETURN_x;
            request_x.T_CUST = T_CUST;
            request_x.T_CUST1 = T_CUST1;
            SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element response_x;
            Map<String, SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element> response_map_x = new Map<String, SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:Zws_BAPI_GET_CUSTOMER_DETAILS:ZBAPI_GET_CUSTOMER_DETAILSRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZBAPI_GET_CUSTOMER_DETAILS',
              'urn:sap-com:document:sap:rfc:functions',
              'ZBAPI_GET_CUSTOMER_DETAILSResponse',
              'SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}