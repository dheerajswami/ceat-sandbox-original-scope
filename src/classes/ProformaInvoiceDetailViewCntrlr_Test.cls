/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class ProformaInvoiceDetailViewCntrlr_Test {
	public static Packing_List__c pc;
	public static Account acc1;
	public static Account acc2;
	public static Contact contact;
	public static Port__c prt1;
	public static Material_Master_Sap__c  materialMaster1;
	public static Material_Master_Sap__c materialMaster2;
	public static Material_Master_Sap__c materialMaster3;
	public static Vehicle_Type__c vt;
	public static Inco_Terms__c it;
	public static Document_Currency__c dc;
	public static Loadability__c loadability;
	public static Loadability__c loadability1;
	public static Proforma_Invoice__c proformaInvoice;
	public static PI_Line_Item__c piLineItem;

	public static void init() {
		acc1 = CEAT_InitializeTestData.createAccForExportsRecType('Acc1', '5000234509','ro','zne','Karnataka');
	    insert acc1;
		contact = CEAT_InitializeTestData.createContact('TestCon', acc1.Id);
		insert contact;
  		loadability =CEAT_InitializeTestData.createLoadability('150', '2010', 'TRUCK', '101164');
  		insert loadability;
  		loadability1 =CEAT_InitializeTestData.createLoadability('150', '2030', 'TRUCK', '101160');
  		insert loadability1;

		pc = CEAT_InitializeTestData.createPacking_List('packingListName','pack');
	    insert pc;
        
        
        acc2 = CEAT_InitializeTestData.createAccForExportsRecType('Acc2', '5000234510','ro1','zn1','TamilNadu');
        acc2.parentId=acc1.id;
        acc2.Active__c=True;
        insert acc2;
	    
	    prt1 = CEAT_InitializeTestData.createPort('01', 'USA', 'Flag','Locode','USAPort' ,'1234');
	    insert prt1;
	    
	    materialMaster1 = CEAT_InitializeTestData.createMaterialMasterSap('101164','CET', 'TRUCK', 'SKU1', 'Ceat','DIEN');
	    materialMaster1.Material_Group__c = '2010';
	    insert materialMaster1;

	    materialMaster2 = CEAT_InitializeTestData.createMaterialMasterSap('101160', 'ALT', 'TRUCK', 'SKU2', 'Altura', 'ZFGS');
	    materialMaster2.Material_Group__c = '2030';
	    insert materialMaster2;

	    materialMaster3 = CEAT_InitializeTestData.createMaterialMasterSap('101165', 'ALT', 'INDUSTRIAL (TUBES)', 'SKU3', 'Altura', 'ZFGS');
	    materialMaster2.Material_Group__c = '2020';
	    insert materialMaster3;

	    vt = CEAT_InitializeTestData.createVehicleType('veh');
	    insert vt;	

	    it = CEAT_InitializeTestData.createIncoTerms('inco');
	    insert it;

	    dc = CEAT_InitializeTestData.createDocumentCurrency('doc');
	    insert dc;

	    proformaInvoice = new Proforma_Invoice__c();
		//proformaInvoice.Customer_Number__c = '5000234509';
		proformaInvoice.Customer__c = acc1.Id ;
		proformaInvoice.Division__c = '07';
		proformaInvoice.Requested_delivery_date__c = System.today();
		proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
		proformaInvoice.Sales_Document_Type__c = '01';
		proformaInvoice.SD_Document_Currency__c = dc.Id;
		//proformaInvoice.Terms_of_Payment__c = tp.Id;
		proformaInvoice.Incoterms__c = it.Id;
		proformaInvoice.Advance_Payment__c = 'No';
		proformaInvoice.Mixing_of_Pro_forma__c = 'No';
		proformaInvoice.Port_of_Destination__c = prt1.Id;
		proformaInvoice.Ship_to_party__c = acc2.Id;
		proformaInvoice.Import_License_Requirement__c = 'No';
		proformaInvoice.Packing_List_Requirement__c = pc.Id;
		proformaInvoice.Vehicle_type__c = vt.Id;
		proformaInvoice.Inspection_Required__c = 'No';
		proformaInvoice.Customized_Container__c = 'No';
		proformaInvoice.Nomination_of_shipping_line__c = 'No';
		proformaInvoice.Clubbing_of_containers__c = 'No';
		proformaInvoice.LC_Required__c = 'No';

		insert proformaInvoice;

		piLineItem = new PI_Line_Item__c();
		piLineItem.Brand__c = 'Ceat';
		piLineItem.Quantity__c = 200;
		piLineItem.System_SetsOrPieces__c = 'DIEN';
		piLineItem.Material_Number__c = '000000000000101164';
		piLineItem.Proforma_Invoice__c = proformaInvoice.Id;
		piLineItem.Category__c = 'TRUCK';

		insert piLineItem;
		
	}
	
	@isTest static void test_method_one() {
		init();
		PageReference pageRef = new PageReference ('apex/ProformaInvoiceDetailView_V2?id='+proformaInvoice.Id);
		Test.setCurrentPageReference(pageRef);
		Test.startTest();
			ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
			ProformaInvoiceDetailViewController piController = new ProformaInvoiceDetailViewController(sc);

		Test.stopTest();
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}


	private class ShipToPartAddress implements WebServiceMock {
		public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

			SapProformaInvoiceSoap ws = new SapProformaInvoiceSoap();    
            
            SapProformaInvoiceSoap.ZWS_SFDC_QUATATION_CREATE zws = new SapProformaInvoiceSoap.ZWS_SFDC_QUATATION_CREATE();

            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H tob = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H();
            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I tom = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I(); 

            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H e = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_H();
            SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I elem = new SapProformaInvoiceSoap.TABLE_OF_ZSFDC_QUATATION_I();      
            
            List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H> b = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_H>();
            List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I> m = new List<SapProformaInvoiceSoap.ZSFDC_QUATATION_I>();  
            
            SapProformaInvoiceSoap.ZSFDC_QUATATION_H zh = new SapProformaInvoiceSoap.ZSFDC_QUATATION_H();
            zh.AUART = 'ZEX0';
            zh.VKORG = '1000';
            zh.VTWEG = '07';
            zh.SPART = '01';
            zh.VKGRP = '';
            zh.VKBUR = '';
            zh.PRSDT = '2015-07-07';
            zh.ANGDT = '2015-07-07';
            zh.BNDDT = '2015-07-07';
            zh.EDATU = '2015-07-15';
            zh.WAERK = 'USD';
            zh.DZTERM = 'ZW17';
            zh.INCO1 = 'CFR';
            zh.INCO2 = 'ALX';//'ALEXANDRA';
            zh.KUNNR = '0056000039';
            zh.ZNUMCNTNR = '02';
            zh.ZIMPRTLCSREQ = 'N';
            zh.ZPCKGREQ = 'WR';
            zh.ZINSPREQ = 'N';
            zh.ZINSPAGNCY = 'NA';
            zh.ZSIZEOFCNTNR = 'HQ';
            zh.ZCUSTCNTNR = 'N';
            zh.ZPRCREF = 'CURR.';
            zh.KUNNR1 = '0056000039';
            zh.ZNOMSHP = 'N';
            zh.ZMIXPI = 'Y';
            zh.ZCLUBCNTNR = 'N';
            zh.ZLICSNUM = '';
            zh.ZADVPAYMNT = '';
            b.add(zh);
            
            SapProformaInvoiceSoap.ZSFDC_QUATATION_I zi = new SapProformaInvoiceSoap.ZSFDC_QUATATION_I();
            zi.MATNR = '000000000000101485';
            zi.WERKS = '1010';
            zi.LGORT = 'FGSL';
            zi.MENGE = '750';
            m.add(zi);
            
            tob.item = b;
            tom.item = m;



		}
	}
	
}