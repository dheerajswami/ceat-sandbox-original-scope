global class Dashboard_Output_Controller { 
    
    public String loggedInUserName    {get;set;}
    public String loggedInUserTerritory  {get;set;}
    public string loggedInUserZone {get;set;}
    public string loggedInUserRole {get;set;}
    public Static String lastDateRun {get;set;}
    String tlLabel = System.Label.TL_Role; 
    String tldLabel = System.Label.TLD_Role;
    public Static String currentDay              = String.valueOf((Date.today()).day());
    public Static String currentMonth            = String.valueOf((Date.today()).month());
    public Static String currentYear             = String.valueOf((Date.today()).year());
    //String loggedInUserTerritory ;
    List<UserTerritory2Association> userTerrCode;
    List<Territory2> userTerritory;
    String loggedInUserId; 
    list<Dashboard_Score__c> dashboardScore1 = new list<Dashboard_Score__c>();
    /*
    global Dashboard_Output_Controller(DashboardInputController controller){
        loggedInUserId=UserInfo.getUserId();
        user loggin = [SELECT id,name,FirstName From User Where id =:loggedInUserId ];
        loggedInUserName = loggin.FirstName;
        //String loggedInUserId=UserInfo.getUserId();
            //loggedInUserName = UserInfo.getName();
             userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId AND (RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) limit 1];  
             if(userTerrCode.size() > 0){
                 userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id];
             }
             if(userTerritory.size() > 0){
                 loggedInUserTerritory=userTerritory[0].Name;
             }
    } */
    global  Dashboard_Output_Controller () { 
        
         loggedInUserId=UserInfo.getUserId();
         
        user loggin = [SELECT id,name,FirstName From User Where id =:loggedInUserId ];
        loggedInUserName = loggin.name;
        //String loggedInUserId=UserInfo.getUserId();
            //loggedInUserName = UserInfo.getName();
             userTerrCode=[select territory2Id,id,RoleInTerritory2 from UserTerritory2Association where UserId=:loggedInUserId AND (RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) limit 1];  
             if(userTerrCode.size() > 0){
                 userTerritory=[select name, id,ParentTerritory2.name from Territory2 where id=:userTerrCode[0].Territory2Id];
             }
             if(userTerritory.size() > 0){
                 loggedInUserTerritory=userTerritory[0].Name;
                 loggedInUserZone = userTerritory[0].ParentTerritory2.name;
                 loggedInUserRole = userTerrCode[0].RoleInTerritory2 ;
             }
             dashboardScore1  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,User__c,LastModifiedDate FROm Dashboard_Score__c where Territory_Code__c=:loggedInUserTerritory AND Month__c =: currentMonth AND Year__c =: currentYear Limit 1];
         	 lastDateRun = dashboardScore1[0].LastModifiedDate.date().adddays(-1).format() ;
    }
    @RemoteAction
    Public Static List<Dashboard_Output_Handler_Controller.dashboardWrapper>  getDashboardData () {
            
             String tlLabel = System.Label.TL_Role; 
            String tldLabel = System.Label.TLD_Role;
            String loggedInUserTerritory ;
            List<UserTerritory2Association> userTerrCode;
            List<Territory2> userTerritory;
             String loggedInUserId; 
             loggedInUserId=UserInfo.getUserId();
            //loggedInUserName = UserInfo.getName();
             userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId AND (RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) limit 1];  
             if(userTerrCode.size() > 0){
                 userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id];
             }
             if(userTerritory.size() > 0){
                 loggedInUserTerritory=userTerritory[0].Name;
             }
             
            Dashboard_Output_Handler_Controller handler = new Dashboard_Output_Handler_Controller();
            system.debug(handler.getDashboardDataRecords(loggedInUserTerritory)+'$$$$$$');
            return handler.getDashboardDataRecords(loggedInUserTerritory);
            //system.debug(dashboardRecord+'dashboardRecord');
    }
}