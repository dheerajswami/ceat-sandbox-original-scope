global class Uploadattchmentbatch implements Database.Batchable<sobject>
{
    global String Query;
    global Attachment attnch;
    global String selec;
    global void alldetails(String selected , Attachment attchmnt)
        {
            system.debug('----------------------------------'+selected );
            attnch = new Attachment ();
            selec = selected ;
                if(selected == 'ALL')
                {
                    Query = 'select id from account';
                }
            else{
                Query = 'select id from account where RecordType.Name= :selec';
                }
                attnch.name =  attchmnt.name;
                attnch.body =  attchmnt.body;
        }
 
        global Database.QueryLocator start(Database.BatchableContext BC)
            {
                return Database.getQueryLocator(query);
            }
 
        global void execute(Database.BatchableContext BC,List<Account> scope)
            {
           
                List<attachment> att = new List<attachment>();
                List<Other_Attachments__c> othrattchlst = new List<Other_Attachments__c>();
                    for(Account acc :Scope)
                    {
                    Other_Attachments__c othrattch = new Other_Attachments__c();
                    othrattch.Account__c = acc.id;
                    othrattch.name = attnch.name;
                    othrattchlst.add(othrattch);
                    }
                 
                insert othrattchlst;
              for(Other_Attachments__c othrattch : othrattchlst)
                {
                    Attachment att1 = new Attachment();
                    att1.name = attnch.name;
                    att1.body = attnch.body;
                    att1 .OwnerId = UserInfo.getUserId();
                    att1 .ParentId = othrattch .id;
                    att1 .IsPrivate = true;
                    att.add(att1);
                }
                insert att;
            
            }
         
        global void finish(Database.BatchableContext BC)
            {
                        
            }
      
        
}