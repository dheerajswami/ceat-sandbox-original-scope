global class Scheduled_SAP_BatchForExCustShipTOCoun implements Schedulable{
   @TestVisible private integer batchsize = 10; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest())
      {
           batchsize = 5;
      } 
      SAP_BatchForExportCustomerShipTOCountry dashBatch= new SAP_BatchForExportCustomerShipTOCountry(); 
      database.executebatch(dashBatch,batchsize);
   }
}