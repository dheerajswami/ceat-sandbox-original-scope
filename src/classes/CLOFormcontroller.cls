public with sharing class CLOFormcontroller {
    public contact dealerdetails{get;set;}
    public string dname{get;set;}
    public Claim__c cl{get;set;}
    //public  Attachment attchmt1{get;set;}
    public Document dttt {get;set;}
    public Material_Master_Sap__c mat{get;set;}
    public boolean showcustomer{get;set;}
    public boolean showclaims{get;set;}
    public boolean showsection{get;set;}
    public string cltype{get;set;}
    public string Materialtype{get;set;}
    public string tyresizevalue{get;set;}
    public string docnum{get;set;}
    public string description{get;set;}
    public list<Claim__c> cllist{get;set;}
    public Attachment attchmt {
  get {
      if (attchmt == null)
        attchmt = new Attachment();
      return attchmt;
    }
  set;
  }
    public CLOFormcontroller(ApexPages.StandardController controller) {
		mat = new Material_Master_Sap__c();
      // attchmt1 = new Attachment();
      dttt = new Document();
        cl = new Claim__c();
        cllist = new list<claim__c>();
         
        showcustomer = false;
        docnum = '';
        showsection=false;
        showclaims=false;
       
        if(apexpages.currentpage().getparameters().get('id')!=null){
    		cl=[select name,Customer_Name__c,Dealer_Name__c,Dealer__r.phone,SAP_Code__c,Customer_Mobile__c,Rim_Size__c,Tyre_Pattern__c,Claim_Type__c 
             ,Ply_Rating__c,Tyre_Type__c,Tyre_Size__c,Serial_Number__c,Spot_Inspect__c,Material_Type__c,Material_Description__c, 
             Material_Group_Description__c,Fitment__c,Part_Replacement_on_Spot__c,Disposition__c,Defect_Type__c,
                Actual_Inspection_Date__c,NSD__c,NBP__c from 
            claim__c where id=:apexpages.currentpage().getparameters().get('id') ];
        }
       
    }
    public void dealerdetailsinfo()
    {
         Id userId = UserInfo.getUserId();
         dealerdetails = [select AccountId,Account.name,Account.phone,Account.KUNNR__c from Contact where Id = :[select contactId from User where Id = :userId].Contactid];
        
    }
    public void populatecustomerdetails()
    {
         
    cltype =cl.claim_type__c;
    system.debug('---'+cltype);
        try{
    if(cltype=='ZC')
    {
        system.debug('in if');
         showcustomer = true;  
        system.debug('in if'+showcustomer);
    }
        else
        {
            showcustomer = false;
            
        }
        }
        catch(exception e)
        {
            
        }
    }
    public void searchclaim()   
    {
        showclaims = true;
        docnum = cl.Docket_Number__c;
       system.debug('----'+docnum);
        system.debug('----'+showclaims);
        
        if(docnum!=null)
        {
            cllist = [select Claim_Type__c,name,from__c,to__c,Docket_Number__c,Customer_Name__c,createddate from claim__c where name =:docnum ];
           
        }
        else if(cl.from__c!=null && cl.to__c!=null)
        {
             Id userId = UserInfo.getUserId();
             Id accid= [select AccountId,Account.name,Account.phone,Account.KUNNR__c from Contact where Id = :[select contactId from User where Id = :userId].Contactid].Accountid;        
            cllist = [select Claim_Type__c,name,from__c,to__c,Docket_Number__c,Customer_Name__c,createddate from claim__c where createddate>=:cl.from__c and createddate<=:cl.to__c and Dealer__r.id=:accid];
        }
    
        if(cl.Docket_Number__c!=null||!string.isBlank(cl.Docket_Number__c))
        {
            cl.from__c=null;
        	cl.to__c=null;  
        }  
         
    }
    public Pagereference NewClaim()
    {
		Pagereference pr = new Pagereference('/apex/CL0Form');     
        return pr;
    }
    public pagereference DocketNumber()
    {
        Pagereference prf = new Pagereference('/'+apexpages.currentPage().getparameters().get('docktnum'));  
        return prf;
    }
    public list<selectoption> getmaterialtypes()
    {
        list<selectoption> matrs = new list<selectoption>();
        matrs.add(new selectoption('none','none'));
        matrs.add(new selectoption('Tyres','Tyres'));
        matrs.add(new selectoption('Tubes','Tubes'));
        matrs.add(new selectoption('Flap','Flap'));
        return matrs;
    }
   
    public list<selectoption> gettyresize() 
    {
        list<selectoption> sizelist = new list<selectoption>();
        sizelist.add(new selectoption('None','None'));
        system.debug('material==>'+Materialtype);
        if(Materialtype!=null)
        {
            for(Material_Master_Sap__c c :[select id ,Tyre_Size__c from Material_Master_Sap__c where Material_Type__c=:Materialtype])
            {
                sizelist.add(new selectoption(c.Tyre_Size__c,c.Tyre_Size__c));
            }
        }
        system.debug('sizelist==>'+sizelist);
        return sizelist;
    }
    public list<selectoption> getmaterialdescription()
    {
         list<selectoption> desclist = new list<selectoption>();
        desclist.add(new selectoption('None','None'));
         if(Materialtype!=null&&tyresizevalue!=null)
         {
             for(Material_Master_Sap__c cm :[select id ,name,Tyre_Size__c from Material_Master_Sap__c where Material_Type__c=:Materialtype and Tyre_Size__c=:tyresizevalue])
            {
                desclist.add(new selectoption(cm.name ,cm.name ));
            }
         }
        return desclist;
    }
   
    public void prepopulateproductdata()
    {
        try
        {
        	if(Materialtype !=null &&  tyresizevalue !=null && description !=null) 
            {
                system.debug('in try if');
               
                mat = [select Name,Rim_Size__c,Tyre_Type__c,Ply_Rating__c,Mat_Grp_Desc__c,Tyre_Size__c,Tyre_Pattern__c,Material_Type__c from Material_Master_Sap__c where Material_Type__c=:Materialtype and Tyre_Size__c=:tyresizevalue and name=:description];
   
            }
        }catch(exception e)
        {
       
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,'Error occurred while Processing Data : '+e.getMessage()));
        }
        }
    
  public PageReference upload()
  		{
	attchmt.name = 'test';
    // attchmt.body = dttt.body; 
    attchmt.OwnerId = UserInfo.getUserId();
    attchmt.ParentId = cl.id; // the record the file is attached to
    attchmt.IsPrivate = false;

    try {
        if(attchmt!=null)
      insert attchmt;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'+e.getMessage()));
      return null;
    } 
      finally
      {
          attchmt=new Attachment();
      }

    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
  }
   
	public pagereference save()
    {
       
        if(cl.Claim_Type__c=='ZC')
        {
            if(cl.Customer_Name__c==null||cl.Customer_Mobile__c==null)
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please enter customer details'));
        }        
    	cl.Material_Type__c = mat.Material_Type__c;
        cl.Rim_Size__c=mat.Rim_Size__c;
        cl.Tyre_Type__c = mat.Tyre_Type__c;
        cl.Material_Description__c=mat.name;
      cl.Tyre_Size__c = mat.Tyre_Size__c;
       cl.Tyre_Pattern__c = mat.Tyre_Pattern__c;
         cl.Material_Group_Description__c=mat.Mat_Grp_Desc__c;
       Id userId = UserInfo.getUserId();
         id accid = [select AccountId,Account.name,MobilePhone,AssoCEAT_Num__c from Contact where Id = :[select contactId from User where Id = :userId].Contactid].Accountid;
        cl.Dealer__c = accid;
      //  cl.Serial_Number__c=mat.Material_Number__c;
          upsert cl;
        system.debug('claiminspect----'+cl.Spot_Inspect__c);
        /*if(cl.Spot_Inspect__c==true)
        {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval automatically');
            req1.setObjectId(cl.id);
            Approval.ProcessResult result = Approval.process(req1);
        }*/
    	upload();
        system.debug('claim----'+cl);
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('===='+baseurl);
         pagereference pr=new pagereference(baseurl+'/'+cl.id);
        return pr;     
    }  
 }