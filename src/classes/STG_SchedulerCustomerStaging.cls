global class STG_SchedulerCustomerStaging implements Schedulable {
    global void execute(SchedulableContext sc) {
        STG_BatchCustomerStaging b = new STG_BatchCustomerStaging(); 
        Database.executebatch(b);
    }
}