public with sharing class OpenClosedActivityLogController {
	public List<Task> openTasks {get; set;}
	public List<Task> openTaskList {get; set;}
	public Date openDueDate {get; set;}
	public Date closedDueDate {get; set;}
	public List<Task> closedTasks {get; set;}
	public List<Task> closedTaskList {get; set;}
	public Id recordId {get; set;}
	public OpenClosedActivityLogController(ApexPages.StandardController stdController) {
		recordId = UserInfo.getUserId();
		System.debug('==#1 '+recordId);
		openTasks = [SELECT ActivityDate,System_Activity_Date__c,Assign_To_Name__c,
					Description,IsClosed,OwnerId,Priority,Status,Subject,WhatId,WhoId 
					FROM Task WHERE IsClosed =: false AND OwnerId =: recordId ORDER BY ActivityDate desc LIMIT 10];
		for(Task tmpTask : openTasks) {
			tmpTask.System_Activity_Date__c = Date.newInstance(tmpTask.ActivityDate.year(), tmpTask.ActivityDate.year(), tmpTask.ActivityDate.day());
			System.debug('==#Date '+tmpTask.System_Activity_Date__c);
			openTaskList.add(tmpTask);
		}

		
		closedTasks = [SELECT ActivityDate,System_Activity_Date__c,Assign_To_Name__c,Conclusion__c,IsClosed,OwnerId,Priority,Status,
					  		  Subject,WhatId,WhoId FROM Task WHERE IsClosed =: true AND OwnerId =: recordId ORDER BY ActivityDate desc LIMIT 10];

		/*for(Task tmpTask : closedTasks) {
			tmpTask.System_Activity_Date__c = Date.newInstance(tmpTask.ActivityDate.year(), tmpTask.ActivityDate.year(), tmpTask.ActivityDate.day());
			System.debug('==#Date '+tmpTask.System_Activity_Date__c);
			closedTaskList.add(tmpTask);
		}*/
		//System.debug('==#2 '+openTasks);
		//System.debug('==#3 '+closedTasks);
	
	}

	 /*public class openActivity {
        public Task openActivityInfo {get; set;}
        public Date dueDate {get; set;}

        public judgingParameterWrapper(Task openActivityInfo) {
        	Date openDate;
            this.openActivityInfo = openActivityInfo;
            for(Task tmpTask : openActivityInfo) {
            	openDate = 
            }
            //this.selected = false;
        }
    }*/

}