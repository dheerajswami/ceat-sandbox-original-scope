global class scheduledBatch_Rep_Spe_Forcast implements Schedulable{
   global void execute(SchedulableContext sc) {
      SP_BatchForSpecialtyBU_Updated b = new SP_BatchForSpecialtyBU_Updated(); 
      database.executebatch(b,200);
   }
}