global class CPORT_CNDNListing_Mapping {
    /*
* Auther  :- Sneha Agrawal
* Purpose :- Fetch Credit Debit Notes from SAP 
*
*
*/

 //-- ATTRIBUTES
    
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static List<CreditDebitMapping> getAllCreditDebitNotes(String cusNum, String fDate, String tDate){
        
        List<CreditDebitMapping> cndn = new List<CreditDebitMapping>();
        String customerId  = '';
        try{
            
            if(cusNum!=null){
               customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
            }
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            
            
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            sap_CNDN_Listing    tst                         = new sap_CNDN_Listing  ();
            sap_CNDN_Listing.ZWS_CNDN_LISTING   zws         = new sap_CNDN_Listing.ZWS_CNDN_LISTING ();
            
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
             sap_CNDN_Listing.TableOfZfi107  tom        = new sap_CNDN_Listing.TableOfZfi107 ();
            
            
            List<sap_CNDN_Listing.Zfi107  > m           = new List<sap_CNDN_Listing.Zfi107  >();        
            
            tom.item = m;
            
            sap_CNDN_Listing.TableOfZfi107   e = new sap_CNDN_Listing.TableOfZfi107 ();
           //System.debug('Swayam123    '+customerId+'   '+fDate+'   '+tDate);
            //fDate = '2014-01-01';
            //tDate = '2014-10-31';
            //customerId = '0050003127';

            e = zws.ZbapiCndnListing(fDate,tom, customerId,tDate);
            
            if(e.item != null){            
                for(sap_CNDN_Listing.Zfi107 z : e.item) {
                    //system.debug(z);
                    cndn.add(new CreditDebitMapping(z.MANDT,z.DocNo, z.SpGlInd, z.PstngDate, z.CUSTOMER, z.DocType, z.Vbeln, z.FKART, z.Sfakn, z.CDNUM, z.ItemText1, z.ItemText, z.AmtDoccur1, z.AmtDoccur));
                }
            }
           // system.debug('Swayam SalesReg********************'+cndn[0]);
            return cndn;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
         
    //-- WRAPPER CLASS
    
    global class CreditDebitMapping{
        public String Mandt     {get;set;}
        public String DocNo     {get;set;}
        public String SpGlInd   {get;set;}
        public String PstngDate {get;set;}
        public String Customer  {get;set;}
        public String DocType   {get;set;}
        public String Vbeln     {get;set;}
        public String Fkart     {get;set;}
        public String Sfakn     {get;set;}
        public String Cdnum     {get;set;}
        public String ItemText1 {get;set;}
        public String ItemText  {get;set;}
        public String AmtDoccur1{get;set;}
        public String AmtDoccur {get;set;}        

        public CreditDebitMapping(String Mandt, String DocNo, String SpGlInd, String PstngDate, String Customer, String DocType, String Vbeln, String Fkart, String Sfakn, String Cdnum, String ItemText1, String ItemText, String AmtDoccur1, String AmtDoccur){
            this.Mandt = Mandt;
            this.DocNo = DocNo;
            this.SpGlInd = SpGlInd;
            this.PstngDate = PstngDate;
            this.Customer = Customer;
            this.DocType = DocType;
            this.Vbeln = Vbeln;
            this.Fkart = Fkart;
            this.Sfakn = Sfakn;
            this.Cdnum = Cdnum;
            this.ItemText1 = ItemText1;
            this.ItemText = ItemText;
            this.AmtDoccur1 = AmtDoccur1;
            this.AmtDoccur = AmtDoccur;
        }
    }
    
    //-- METHODS
}