public with sharing class DseBeatTriggerHandler {
    public static void setDistributor(List<DSE_Beat__c> newDseBeats) {
        Set<String> distributorCodes = new Set<String>();

        for(DSE_Beat__c dseBeat : newDseBeats) {
            distributorCodes.add(dseBeat.Distributor_Code__c);
        }

        List<Account> distributors = [Select Id, Name, KUNNR__c from Account where KUNNR__c in :distributorCodes];

        for(DSE_Beat__c dseBeat : newDseBeats) {
            //Boolean bool = false;
            for(Account acc : distributors) {
                if(acc.KUNNR__c == dseBeat.Distributor_Code__c) {
                    dseBeat.Distributor__c = acc.Id;
                    dseBeat.Distributor_Name__c = acc.Name;
                    //bool = true;
                }
            }
            /*
            if(!bool) {
                dseBeat.Distributor__c = null;
            }*/
        }
    }
}