@isTest 
private class StockVisibility_Controller_TestClass{

      static user userTLRep   = new user();
      static user userTLDRep  = new user();
      static user userRMRep   = new user();
      static user userCSTLRep = new user();
      static user adminUser   = new user();
      static  Server_Url__c server;
      
      //Method to cover stock controller
       static testmethod void createUserTest() {
       
       StockController sc = new StockController();
       
        /* Getting the TL User and its Territory */
       UserTerritory2Association userTL = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='TL_Replacement' limit 1];
       User tl = [select id,UserRole.Name, name from User where id =: userTL.UserId];
       
        /* Getting the TLD User and its Territory */
        UserTerritory2Association userTLD = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='TLD' limit 1];
        User tld = [select id,name,UserRole.Name from User where id =: userTLD.UserId];
        
        
        /* Getting the CSTL User and its Territory */
        UserTerritory2Association userCSTL = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='CSTL' limit 1];
        User cstl = [select id, name from User where id =: userCSTL.UserId];
        
        /* Getting the RM User and its Territory */
        UserTerritory2Association userRM = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='RM' limit 1];
        User rm = [select id, name from User where id =: userRM.UserId];
        
        /* Getting the RM User and its Territory */
        UserTerritory2Association userZGM = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='ZGM' limit 1];
        User zgm = [select id, name from User where id =: userZGM.UserId];
        
        
         /* Running as RM User */
        system.runAs(rm){ 
       /* Stock Header Record Type*/
        Id stockHeaderType = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stock1 = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType ,Zyhcf__c='ASNR',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
             insert stock1;    
             StockController sc1 = new StockController();
             server = new Server_Url__c(url__c='https://ceat--fullcopy.cs6.my.salesforce.com');
             insert server;   
         
              PageReference pg=StockController.getPage('RM','ASNR','Pull','2010');                                       
              Set<SelectOption> plantList = StockController.plantSet('Pull');
              Set<SelectOption> categorySet = StockController.categorySet('2010');
        }
        
        
        /* Running as TL User */
        system.runAs(tl){ 
        /* Stock Header Record Type*/
        Id stockHeaderType1 = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stock2 = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType1 ,Zyhcf__c='ASNR',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
        insert stock2;    
        
        StockController sc2 = new StockController();
          PageReference pg=StockController.getPage('TL','ASNR','Pull','2010');                                       
          Set<SelectOption> plantSet = StockController.plantSet('Pull');
          Set<SelectOption> categorySet = StockController.categorySet('2010');
        }
        
        /* Running as TLD User */
        system.runAs(tld){ 
        
        
        /* Stock Header Record Type*/
        Id stockHeaderType2 = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stock3 = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType2 ,Zyhcf__c='ASNR',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
        insert stock3;    
        
        StockController sc3 = new StockController();
           
         
          PageReference pg=StockController.getPage('TLD','ASNR','Pull','2010');                                       
          Set<SelectOption> plantList = StockController.plantSet('Pull');
          Set<SelectOption> categorySet = StockController.categorySet('2010');
        }
        
        /* Running as CSTL User */
        system.runAs(cstl){ 
        
        
        /* Stock Header Record Type*/
        Id stockHeaderType3 = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stock4 = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType3 ,Zyhcf__c='ASNR',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
        insert stock4;    
        StockController sc4 = new StockController();
        PageReference pg=StockController.getPage('CSTL','ASNR','Pull','2010'); 
          }
        
        /* Running as ZGM User */
        system.runAs(zgm){ 
        /* Stock Header Record Type*/
        Id stockHeaderType3 = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stock4 = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType3 ,Zyhcf__c='ASNR',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
        insert stock4;    
        StockController sc4 = new StockController();
        PageReference pg=StockController.getPage('ZGM','ASNR','Pull','2010');                                       
          
        }
       }
       
       
       
       //Method to cover Stock Visibility
       static testmethod void createTestStockVisibility() {
       
       
       /* Stock Header Record Type*/
        Id stockHeaderType = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Stock Header').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stocks = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderType ,Zyhcf__c='ASNC',ZYHCF_D__c='ASNC',Zyhdc__c='ASNR',Zyhrf__c='ASNR',Rtype__c='NP');
        insert stocks;    
        
        Id stockHeaderTypepull = Schema.SObjectType.Stocks__c.getRecordTypeInfosByName().get('Pull').getRecordTypeId();
        /*Inserting Stock Record*/
        Stocks__c stockspull = new Stocks__c(Category__c='2010',Currents__c='24/12/2015',Intransit1__c='24/12/2015',Intransit__c='24/12/2015',
                                              Maktx__c='sample',Matnr__c='101766',Mustdisp_1__c='24/12/2015',Mustdisp_2__c='24/12/2015',Mustdisp__c='24/12/2015',
                                              RecordTypeId=stockHeaderTypepull ,Zyhcf__c='ASNC',Zyhdc__c='ASNR',Zyhrf__c='ASNR');
        insert stockspull; 
         
        ApexPages.currentPage().getParameters().put('selectedPlant','ASNC');  
        ApexPages.currentPage().getParameters().put('selectedrecordType','PULL');  
        ApexPages.currentPage().getParameters().put('selectedCategory','2010');  
        ApexPages.currentPage().getParameters().put('plantType','ASNC');  
       
        StockVisibilityController svc = new StockVisibilityController();
        
        
        
       UserTerritory2Association userTL = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='TL_Replacement' limit 1];
       User tl = [select id,UserRole.Name, name from User where id =: userTL.UserId];
       
       List<Stocks__c> plantlistDisplay = StockVisibilityController.getPlantListToDisplay(tl.Id,'PULL','ASNC','2010','101766');
       List<Stocks__c> plantlistDisplay1 = StockVisibilityController.getPlantListToDisplay(tl.Id,'PULL','None','2010','101766');
       List<Stocks__c> plantlistDisplay2 = StockVisibilityController.getPlantListToDisplay(tl.Id,'PULL','ASNC','None','101766');
       List<Stocks__c> plantlistDisplay3 = StockVisibilityController.getPlantListToDisplay(tl.Id,'PULL','None','None','101766');
       
       Id loggedInUsrId = UserInfo.getUserId();
       List<Stocks__c> plantlistDisplay0 = StockVisibilityController.getPlantListToDisplay(loggedInUsrId ,'PULL','ASNR','2010','101766');
       List<Stocks__c> plantlistDisp1 = StockVisibilityController.getPlantListToDisplay(loggedInUsrId ,'PULL','None','2010','101766');
       List<Stocks__c> plantlistDisp2 = StockVisibilityController.getPlantListToDisplay(loggedInUsrId ,'PULL','ASNR','None','101766');
       List<Stocks__c> plantlistDisp3 = StockVisibilityController.getPlantListToDisplay(loggedInUsrId ,'PULL','None','None','101766');

       }
       

}