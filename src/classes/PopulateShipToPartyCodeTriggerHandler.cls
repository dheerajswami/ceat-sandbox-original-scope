public with sharing class PopulateShipToPartyCodeTriggerHandler {


	@future(callout=true)

	/*
    * After Insert
    */

    public  static void populateShipToPartyCode(Set<Id> newInvoiceIdSet) {
    	try {
	    	List<Proforma_Invoice__c> invoiceToUpdateList 	= new List<Proforma_Invoice__c>();
	    	SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
	        String username                                 = saplogin.username__c;
	        String password                                 = saplogin.password__c;
	        Blob headerValue = Blob.valueOf(username + ':' + password);
	        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
	    	if(!newInvoiceIdSet.isEmpty()) {
	    		for(Proforma_Invoice__c tmpInvoice : [SELECT System_Port_Name__c, Customer_Number__c, Customer__r.UniqueIdentifier__c FROM Proforma_Invoice__c WHERE Id IN: newInvoiceIdSet]) {
	    			Sap_zws_sfdc_stp_address ws = new Sap_zws_sfdc_stp_address();
					Sap_zws_sfdc_stp_address.zws_SFDC_STP_ADDRESS zws = new Sap_zws_sfdc_stp_address.zws_SFDC_STP_ADDRESS();
					zws.inputHttpHeaders_x = new Map<String,String>();
					zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
					zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
					zws.timeout_x = 60000;

					Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP tom = new Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP();
					List<Sap_zws_sfdc_stp_address.ZSFDC_STP> m 	   = new List<Sap_zws_sfdc_stp_address.ZSFDC_STP>();
					tom.item = m;

					Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP elem = new Sap_zws_sfdc_stp_address.TABLE_OF_ZSFDC_STP();
					System.debug('==#33 '+tmpInvoice.Customer__r.UniqueIdentifier__c);
					if(tmpInvoice.Customer_Number__c != null) {
						String cusNum = UtilityClass.addleadingZeros(tmpInvoice.Customer_Number__c,10);
						elem = zws.ZSFDC_STP_ADDRESS(cusNum, tom, tmpInvoice.System_Port_Name__c);

						System.debug('===#2 '+ elem);
					}
					
					if(elem.item != null) {
						for(Sap_zws_sfdc_stp_address.ZSFDC_STP z : elem.item) {
							tmpInvoice.Ship_to_party__c = z.KUNNR;
							invoiceToUpdateList.add(tmpInvoice);
						}
					}
					
	    		}
	    	}
	    	update invoiceToUpdateList;
	    }catch(Exception e) {}
    }
}