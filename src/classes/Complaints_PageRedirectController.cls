global with sharing class Complaints_PageRedirectController {
    public ApexPages.StandardController standardController { get; set; }
    public static case caseRecordid{get;set;}
    public static PageReference page {get;set;}
    public string selectedNature {get;set;}
    public static  case caseRecord{get;set;}
    public string selectedBusinessCat {get;set;}
    public string selectedCompType {get;set;}
    
    public string selectedState {get;set;}
    public List<SelectOption> naturePickList{get;set;}
    public List<SelectOption> businessCatList{get;set;}
    public List<SelectOption> complaintTypeList{get;set;} 
    public List<SelectOption> stateList{get;set;} 
    public ID Caseid {get;set;}
    
    public Complaints_PageRedirectController(ApexPages.StandardController standardController) {
        this.standardController = standardController;
        caseRecordid = (Case)standardController.getRecord();
        system.debug(caseRecord+'caseRecord');
        caseRecord= [select id , Name_of_complainant__c,Level__c,Complaint_Type__c,Business_Category__c,Nature__c,Mobile_of_complainant__c,Comments__c,CaseNumber from case where id =: caseRecordid.id];
        naturePickList      = new List<SelectOption>();
        businessCatList     = new List<SelectOption>();
        complaintTypeList   = new List<SelectOption>();
        stateList           = new List<SelectOption>();
         
        Schema.DescribeFieldResult fieldResult = Case.Nature__c.getDescribe();
        List<Schema.PicklistEntry> natureList = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry n : natureList){       
            naturePickList.add(new SelectOption(n.getLabel(), n.getValue()));
        } 
        
        Schema.DescribeFieldResult fieldResult1 = Case.Business_Category__c.getDescribe();
        List<Schema.PicklistEntry> businessList = fieldResult1.getPicklistValues();
        for( Schema.PicklistEntry bc : businessList){       
            businessCatList.add(new SelectOption(bc.getLabel(), bc.getValue()));
        } 
        
        Schema.DescribeFieldResult fieldResult2 = Case.Complaint_Type__c.getDescribe();
        List<Schema.PicklistEntry> complaintList = fieldResult2.getPicklistValues();
        for( Schema.PicklistEntry cm : complaintList){       
            complaintTypeList.add(new SelectOption(cm.getLabel(), cm.getValue()));
        } 
        
        List<State_Master__c> states = new List<State_Master__c>();
        
        for(AggregateResult masterRec: [Select State__c from State_Master__c group by State__c]){
            stateList.add(new SelectOption(String.valueOf(masterRec.get('State__c')),String.valueOf(masterRec.get('State__c'))));        
        }
        stateList.sort();
        //page = returnpage(temp);
        
        //caseid = temp.id;
        /*
        Id CibaId = UtilityClass.getRecordTypeId('case','CIBA');
        case record = new case();
        record = [SELECT id , RecordTypeID From Case Where id =: caseid ];
        if(record != null && record.RecordTypeID == CibaId ){
            
        }*/
        

    }
    @RemoteAction
    public Static PageReference getpage(){
        Id CibaId = UtilityClass.getRecordTypeId('case','CIBA');
        system.debug(caseRecord+'SSSSSS');
        if(caseRecord.RecordTypeID == CibaId ){ 
            //page = new Apex.standardController(caseRecord).edit();
            page = new PageReference('/'+caseRecord.id+'/e');
            page.getParameters().put('nooverride','1');
            page.setRedirect(true);    
        }else{
            page = new PageReference('/apex/ComplaintBPOPage'+'?id='+caseRecord.id+'&CF00NO0000001oEwD='+caseRecord.RecordTypeID);
            page.setRedirect(true); 
        }
        return page;
        
    }
    
    //Passing state as param and getting all district under that state
    @RemoteAction
    global static List<SelectOption> getDistricts(String selectedState, String accid){
        List<SelectOption> districtList = new List<SelectOption>();
        system.debug(accid+'DDDD');
        districtList.add(new SelectOption('None','--- None ---')); 
        Set<String> disSet=new Set<String>();
        
        for(State_Master__c s:[Select District__c from State_Master__c where State__c=:selectedState order by District__c asc]){
          if(!disSet.contains(s.District__c)){
            districtList.add(new SelectOption(s.District__c,s.District__c)); 
            disSet.add(s.District__c);
          } 
        }
        return districtList;
    }
     //Passing district as param and getting all towns under that district
    @RemoteAction
    global static List<SelectOption> getTowns(String selectedDistrict){
         List<SelectOption> townList = new List<SelectOption>();
        townList.add(new SelectOption('None','--- None ---')); 
        for(State_Master__c s:[Select Town__c from State_Master__c where District__c=:selectedDistrict order by Town__c asc]){
            townList.add(new SelectOption(s.Town__c,s.Town__c));  
        }
        return townList;
    }

    
}