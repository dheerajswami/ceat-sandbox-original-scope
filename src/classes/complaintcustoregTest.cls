@isTest(SeeAllData=False)

 public class complaintcustoregTest {
     
   public static testMethod void newcustomerreg_testClass(){
  
    State_Master__c s = new State_Master__c();
    s.State__c = 'Madhya Pradesh';
    s.District__c = 'Balaghat';
    insert s;
    ComplaintCustomerRegControllerV1.getDistricts(s.State__c);
   }
  
   public static testMethod void newDistrictCustReg(){
   
    State_Master__c s = new State_Master__c();
    s.State__c = 'Madhya Pradesh';
    s.District__c = 'Balaghat';
    s.Town__c = 'Hatta';
    insert s;
    ComplaintCustomerRegControllerV1.getTowns(s.District__c);
    
  }

  public static testMethod void createcomplaintRegistration() {
            State_Master__c s = new State_Master__c();
            s.State__c = 'Madhya Pradesh';
            s.District__c = 'Barwani';
            s.RO__C = 'IND';
            s.Town__c = 'Dugani';
            insert s;
          
            Account acc = New Account();
            acc.Name = 'test';
            acc.Type_Of_Customer__c = 'Dealer';
            acc.AD_TLNMBR__c = '9663671510';
            acc.Email__c = 'ajeet@gmail.com';
            acc.State__c = 'Madhya Pradesh';
            acc.District__c = 'Barwani';
            acc.Town__c = 'Dugani';
            acc.Street_1__c = 'abcd';
            Insert acc;
          
            case createCase = new case();
            createCase.Name_of_complainant__c='Test';
            createCase.Mobile_of_complainant__c='9663671510';
            createCase.Comment__c='shjakgdjkD';
            createCase.SAP_Reference_Number__c='SP102';
            createCase.Customer_Email__c = 'ajeet@gmail.com';
            createCase.Customer_Phone__c = '9663671510';
            createCase.RecordTypeid = UtilityClass.getRecordTypeId('Case','Pre-complaints');
            insert createCase;
          
            Id [] fixedSearchResults= new Id[1];
            fixedSearchResults[0] = acc.Id;
            Test.setFixedSearchResults(fixedSearchResults);

        ComplaintCustomerRegControllerV1.saveComplaintCustomer(acc.Name,acc.AD_TLNMBR__c,acc.State__c,acc.District__c,acc.Town__c,acc.Email__c,acc.Street_1__c,acc.Type_Of_Customer__c);
        
    }

    public static testMethod void createcomplaintRegistration1() {
            Account acc = New Account();
            acc.Name = 'test';
            acc.Type_Of_Customer__c = 'Dealer';
            acc.AD_TLNMBR__c = '9347482383';
            acc.Email__c = '';
            acc.State__c = 'Madhya Pradesh';
            acc.District__c = 'Barwani';
            acc.Town__c = 'Dugani';
            acc.Street_1__c = 'abcd';
            Insert acc;
          
            case createCase = new case();
            createCase.Name_of_complainant__c='Test';
            createCase.Mobile_of_complainant__c='9663871018';
            createCase.Comment__c='shjakgdjkD';
            createCase.SAP_Reference_Number__c='SP102';
            createCase.Customer_Email__c = '';
            createCase.Customer_Phone__c = '9736627282';
            createCase.RecordTypeid = UtilityClass.getRecordTypeId('Case','Pre-complaints');
            insert createCase;

            ComplaintCustomerRegControllerV1.saveComplaintCustomer(acc.Name,acc.AD_TLNMBR__c,acc.State__c,acc.District__c,acc.Town__c,acc.Email__c,acc.Street_1__c,acc.Type_Of_Customer__c);

  }

     public static testMethod void createcomplaintRegistration2() {
            Account acc = New Account();
            acc.Name = 'test';
            acc.Type_Of_Customer__c = 'Dealer';
            acc.AD_TLNMBR__c = '9663671510';
            acc.Email__c = 'ajeet@gmail.com';
            acc.State__c = 'Madhya Pradesh';
            acc.District__c = 'Barwani';
            acc.Town__c = 'Dugani';
            acc.Street_1__c = 'abcd';
            Insert acc;
          
            case createCase = new case();
            createCase.Name_of_complainant__c='Testing';
            createCase.Mobile_of_complainant__c='9663671510';
            createCase.Comment__c='shjakgdjkD';
            createCase.SAP_Reference_Number__c='SP102';
            createCase.Customer_Email__c = 'ajeet@gmail.com';
            createCase.Customer_Phone__c = '9663671510';
            createCase.RecordTypeid = UtilityClass.getRecordTypeId('Case','Pre-complaints');
            insert createCase;

            case createCase1= new case();
            createCase1.Name_of_complainant__c='Testing';
            createCase1.Mobile_of_complainant__c='9663671510';
            createCase1.Comment__c='shjakgdjkD';
            createCase1.SAP_Reference_Number__c='SP102';
            createCase1.Customer_Email__c = 'ajeet@gmail.com';
            createCase1.Customer_Phone__c = '9663671510';
            createCase1.RecordTypeid = UtilityClass.getRecordTypeId('Case','Pre-complaints');
            insert createCase1;

            ComplaintCustomerRegControllerV1.saveComplaintCustomer(acc.Name,acc.AD_TLNMBR__c,acc.State__c,acc.District__c,acc.Town__c,acc.Email__c,acc.Street_1__c,acc.Type_Of_Customer__c);

  }

    public static testMethod void newDistrictCustReg1(){
    
     account a = new account();
     a.Name = 'sadaa';
     a.AD_TLNMBR__c = '9663671510';
     a.Email__c = 'abc@gmail.com';
     insert a;
    
     ComplaintCustomerRegControllerV1.getphoneNumbersAndEmailList(a.AD_TLNMBR__c,a.Email__c);
    }
     //covering else part
    public static testMethod void newDistrictCustReg2(){
    
     account a = new account();
     a.Name = 'sadaa';
     a.AD_TLNMBR__c = '9663645636';
     a.Email__c = 'pallavi@gmail.com';
     insert a;
    
     ComplaintCustomerRegControllerV1.getphoneNumbersAndEmailList('8888888888',a.Email__c);
    }


    public static testMethod void coverConstructor(){

           State_Master__c s = new State_Master__c();
            s.State__c = 'Madhya Pradesh';
            s.District__c = 'Barwani';
            s.RO__C = 'IND';
            s.Town__c = 'Dugani';
            insert s;
    
     account a = new account();
     a.Name = 'sadaa';
     a.AD_TLNMBR__c = '9663671510';
     a.Email__c = 'abc@gmail.com';
     insert a;
    
     ComplaintCustomerRegControllerV1 ccrc = new ComplaintCustomerRegControllerV1(new ApexPages.StandardController(a));
    }
     
    
    
 }