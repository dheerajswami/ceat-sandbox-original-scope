@isTest
public class ProspectController_TestClass { 
    //Method to create Territory and assigning users to territory
    public static User init(){
            Territory2Model terrModel = new Territory2Model();
            Territory2 terrtypeBURep = new Territory2 ();
            Territory2 terrtypeBUsp = new Territory2 ();
            
            Territory2 terrtypeZoneRepZE01 = new Territory2 ();  
            Territory2 terrtypeZoneRepPAT = new Territory2 ();
            Territory2 terrtypeZoneRepPATC0081 = new Territory2 ();
            
            Territory2 terrtypeZoneRepZE01sp = new Territory2 ();  
            Territory2 terrtypeZoneRepPATsp = new Territory2 ();
            Territory2 terrtypeZoneRepPATC0081sp = new Territory2 ();
             
            UserTerritory2Association TL81user = new UserTerritory2Association ();
            UserTerritory2Association TL81usersp = new UserTerritory2Association ();
            user userTLDataC0081 = new user();
            
            id terrTypeBU ;
            id terrTypeRO ;
            id terrTypeTerr ;
            id terrTypeZone ;       
            
            // Load the test Sales planning Staging from the static resource
            terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
            insert terrModel;
            
            List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
           
            for(sObject temp: listTerritoryType){
                Territory2Type t =(Territory2Type)temp;
                if(t.DeveloperName == 'BUTest'){
                    terrTypeBU = t.id;
                }
                if(t.DeveloperName == 'ROTestTest'){
                    terrTypeRO = t.id;
                }
                if(t.DeveloperName == 'TerritoryTest'){
                    terrTypeTerr = t.id;
                }
                if(t.DeveloperName == 'ZoneTest'){
                    terrTypeZone = t.id;
                }
            }  
            //Replacement      
            //Created BU Territory
            terrtypeBURep =  CEAT_InitializeTestData.createTerritory('ReplacementsTest','ReplacementsTest',terrTypeBU,terrModel.id,null);
            insert terrtypeBURep;        
            
            //Created Zone territory For Replacement
            terrtypeZoneRepZE01  =  CEAT_InitializeTestData.createTerritory('ZE01','ZE01_test',terrTypeZone,terrModel.id,terrtypeBURep.id);
            insert terrtypeZoneRepZE01;
            
            //Created RO Territory Replacement
            terrtypeZoneRepPAT =  CEAT_InitializeTestData.createTerritory('PAT','PAT',terrTypeRO,terrModel.id,terrtypeZoneRepZE01.id);
            insert terrtypeZoneRepPAT;
           
           //Created District Territory Replacement
            terrtypeZoneRepPATC0081 =  CEAT_InitializeTestData.createTerritory('C0081','C0081',terrTypeTerr,terrModel.id,terrtypeZoneRepPAT.id);
            insert terrtypeZoneRepPATC0081;
            
            userTLDataC0081 = CEAT_InitializeTestData.createUser('mm','TestTL','sneha.agrawal@extentor.com','snehaTL81@ceat.com','12345','Replacements',null,'TL_Replacement');
            insert userTLDataC0081;
            
            
            TL81user = CEAT_InitializeTestData.createUserTerrAsso(userTLDataC0081.id, terrtypeZoneRepPATC0081.id,'TL_Replacement');
            insert TL81user;
            
            //**********************************************Specialty Territory********************************
             //Created BU Territory
            terrtypeBUsp =  CEAT_InitializeTestData.createTerritory('SpecialtyTest','SpecialtyTest',terrTypeBU,terrModel.id,null);
            insert terrtypeBUsp;        
            
            //Created Zone territory For Specialty
            terrtypeZoneRepZE01sp  =  CEAT_InitializeTestData.createTerritory('Bihar/Jharkhand','Bihar_Jharkhand',terrTypeZone,terrModel.id,terrtypeBUsp.id);
            insert terrtypeZoneRepZE01sp;
            
            //Created RO Territory Specialty
            terrtypeZoneRepPATsp =  CEAT_InitializeTestData.createTerritory('PAT','SPRP_PAT',terrTypeRO,terrModel.id,terrtypeZoneRepZE01sp.id);
            insert terrtypeZoneRepPATsp;
           
           //Created District Territory Specialty
            terrtypeZoneRepPATC0081sp =  CEAT_InitializeTestData.createTerritory('C0081','SPRP_C0081',terrTypeTerr,terrModel.id,terrtypeZoneRepPATsp.id);
            insert terrtypeZoneRepPATC0081sp;
            
            TL81usersp = CEAT_InitializeTestData.createUserTerrAsso(userTLDataC0081.id, terrtypeZoneRepPATC0081sp.id,'TL_Replacement');
            insert TL81usersp;
            
            return userTLDataC0081;
    }
    //Method to create account and state master data
    public static Account init1(){
         Account accObj=new Account();
             accObj.Name='abc';
             accObj.Sales_District_Text__c='C0081';              
             insert accObj;     
             //system.debug('accObj==========='+accObj);
             
             State_Master__c stm = new State_Master__c();
             stm.State__c = 'UTTAR PRADESH';
             stm.District__c = 'Lucknow';
             stm.Town__c = 'Lucknow';        
             insert stm; 
             
             return accObj; 
    }
     //Test method for ProspectController controller
     static testmethod void testLoadData() {
        
        User userTLDataC0081 =  init();
        
        
         System.runAs(userTLDataC0081){ 
             Account accObj=init1();
             accObj.Convert__c = true;
             accObj.Business_Units__c = 'Replacement';
             update accObj;
             List<SelectOption> dist    = ProspectController.DistrictsNameList('Lucknow');
             List<SelectOption> town    = ProspectController.DepTown('Lucknow');
             
             ApexPages.StandardController con; 
             ProspectController psc= new ProspectController(con);
             List<SelectOption> states  = psc.getStates();
             psc.accObj.id=accObj.Id;
             psc.accObj.Sales_District_Text__c=accObj.Sales_District_Text__c;
             psc.accObj.Name=accObj.name;
             
             PageReference tpageRef = Page.New_ProspectV1;
             Test.setCurrentPage(tpageRef); 
             //ApexPages.currentPage().getParameters().put('Id', po.Id);
             psc.createProspect();       
      }
     }
     //Test method for ProspectControllerSpecailty controller
     static testmethod void testLoadDataSpecialty() {
        
        User userTLDataC0081 =  init();
        
         System.runAs(userTLDataC0081){     
             Account accObj=init1();
             
             List<SelectOption> dist    = ProspectControllerSpecailty.DistrictsNameList('Lucknow');
             List<SelectOption> town    = ProspectControllerSpecailty.DepTown('Lucknow');
             
             ApexPages.StandardController con; 
             ProspectControllerSpecailty psc= new ProspectControllerSpecailty(con);
             List<SelectOption> states  = psc.getStates();
             psc.accObj.id=accObj.Id;
             psc.accObj.Sales_District_Text__c=accObj.Sales_District_Text__c;
             psc.accObj.Name=accObj.name;
             
             PageReference tpageRef = Page.New_ProspectV1;
             Test.setCurrentPage(tpageRef); 
             psc.createProspect();       
        }
     }
     static testmethod void testLoadDataAccount() {
         id prospectRecTypeId = UtilityClass.getRecordTypeId('Account','Prospect'); 
         Account accprospect = new Account(Name='testP',Convert__c = false,recordtypeid = prospectRecTypeId ,Sales_Group_Text__c = 'RAN');
         insert accprospect ;
         //For calling update after insert 
         UtilityClass.setFirstRunTrue();
         RCM_Settings__c emailSetting = new RCM_Settings__c (Name= 'RAN' , Email__c = 'developer@extentor.com');
         insert emailSetting ;
         RCM_Settings__c emailSettingExport = new RCM_Settings__c (Name= 'Export' , Email__c = 'developer@extentor.com');
         insert emailSettingExport;
         accprospect.Convert__c = true;
         accprospect.Business_Units__c = 'Replacement' ;
         update accprospect;
         Prospect_Customer_Counter__c counterValues = new Prospect_Customer_Counter__c();
         counterValues = Prospect_Customer_Counter__c.getOrgDefaults();
         counterValues.customerCounter__c = 8631;
         counterValues.prospectCounter__c = 4515;
         insert counterValues ;
     }
     static testmethod void testLoadDataAccountExport() {
         id prospectRecTypeId = UtilityClass.getRecordTypeId('Account','Prospect'); 
         Account accprospectExport = new Account(Name='testP',Convert__c = false,recordtypeid = prospectRecTypeId );
         insert accprospectExport ;
         RCM_Settings__c emailSettingExport = new RCM_Settings__c (Name= 'Export' , Email__c = 'developer@extentor.com');
         insert emailSettingExport;
         UtilityClass.setFirstRunTrue();
         accprospectExport.Convert__c = true;
         accprospectExport.Business_Units__c = 'Exports' ;
         update accprospectExport;
         //For calling update after insert 
         
         
     }
}