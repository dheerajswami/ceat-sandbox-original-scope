global class SP_Replacement_PrevMonth_Controller { 
    public String loggedInUserId    {get;set;}
    public String loggedInUserName { get; set; }
    public String loggedInTerritory  {get;set;}
    public String month {get;set;}
    public String Year{get;set;}
    UserTerritory2Association userTerrCode;
    Territory2 userTerritory;
    //Wrapper classes
    
    public class tlWrapper{
        public String tlName {get;set;}
        public String tlTerritory {get;set;}
       
        public List<categoryWrapper> catList{get;set;}
        public String tlSpeValue {get;set;}
    }
    public class categoryWrapper implements Comparable{
        public String catName{get;set;}
        public String catTarget{get;set;}
        public String catRunning{get;set;}
        public String catGap{get;set;}        
        public String catASP{get;set;}
        public String budget{get;set;}
        public String Lycm{get;set;}
        public String L3M{get;set;}
        public String planned{get;set;}
        public String tlName1{get;set;}
        public String spTLID{get;set;}
        public String terrCode{get;set;}
        
        public Decimal columnOrder{get;set;}
       
        public Integer compareTo(Object compareTo) {
            categoryWrapper compareToCat = (categoryWrapper)compareTo;
            if (columnOrder == compareToCat.columnOrder) return 0;
            if (columnOrder > compareToCat.columnOrder) return 1;
            return -1;        
        }
    }
     global SP_Replacement_PrevMonth_Controller () {
        loggedInUserId=ApexPages.currentPage().getParameters().get('id');
        if(loggedInUserId=='' || loggedInUserId==null ){
            loggedInUserId=UserInfo.getUserId();
        }
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId limit 1];
        userTerritory=[select name, id from Territory2 where id=:userTerrCode.Territory2Id];
        loggedInTerritory=userTerritory.Name;
     } 
    //Used to get list of TL or Dealer depending on which user logged in
    //List<SP_HandlerForSP_Specialty_Controller.tlWrapper>
    @RemoteAction
    Public Static List<tlWrapper> getSalesPlanningOfRM (String loggedInUserId,String Yearnum,String monthnum,String monthtext,String Yeartext) {
            system.debug(monthnum+'monthnum');
            system.debug(Yearnum+'Yearnum');
            system.debug(monthtext+'monthtext');
            system.debug(Yeartext+'Yeartext');
            
            String rmLabel          		= System.Label.RM_Role;
            String tlLabel        	    	= System.Label.TL_Role;
            String repDealerLabel   		= System.Label.Replacement_Dealer;
            String repROLabel       		= System.Label.Replacement_RO;
            String repTLLabel       		= System.Label.Replacement_TL; 
            String SpecialtyDealerLabel     = System.Label.Specialty;
            
            String tlPageName                                = System.Label.Sales_Planning_RM;
            Integer totalGapAllDL       = 0;
            Integer totalTargetGap      = 0;
            Integer totalRunningGap     = 0;
            
            Integer plan                = 0;
            Integer makeGapzero         = 0;        
            Integer totalPlannedAllTL   = 0;
            Integer totalPlannedAllTL1  = 0; 
            
            Decimal totalValue          = 0.0;
            Decimal totalValue1         = 0.0;
            Decimal totalSpeValueTL     = 0.0;
            Decimal totalSpeValueTL1     = 0.0;
            
            Sales_Planning__c salesPlanningTL ;
            Sales_Planning__c salesPlanningTL1; 
            Sales_Planning__c salesPlanningTLTemp;
            List<UserTerritory2Association> regionForRM         = new List<UserTerritory2Association>();
            List<User> userTL                                   = new List<User>();
            List<Sales_Planning__c> salesPlanningFullPerRO      = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningRO             = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningDealer         = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningDealer   = new List<Sales_Planning__c>();
            
            List<Sales_Planning__c> salesPlanningDealerSpecialty   = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningTLCatwise = new List<Sales_Planning__c>();
            
            List<Sales_Planning__c> existingSalesPlanningTL     = new List<Sales_Planning__c>();
            
            
            List<Account> listOFDealerTOUpdateSpecialtyVal      = new list<Account>();
            List<Territory2> listOfTerr                         = new List<Territory2>();
            List<UserRole> userRMRol                            = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName =: rmLabel]; 
            List<UserRole> userTLRol                            = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName =: tlLabel];
            
            
            List<Sales_Planning__c> salesPlan;
            Map<String,List<Sales_Planning__c>> mapOfTerrSales          = new map<String,List<Sales_Planning__c>>();
            //Map<String,List<Sales_Planning__c>> mapOFTLAndDealer      = new map<String,List<Sales_Planning__c>>();
            Map<String,Decimal> mapOFTlAndSpeValue                      = new Map<String,Decimal>();
            Map<String,List<Sales_Planning__c>> mapOfCatSalesPlanningTL = new map<String,List<Sales_Planning__c>>();
            Map<String,Integer> mapOfROTarget                           = new map<String,Integer>();
            Map<String,Sales_Planning__c> mapOfTLSP;
            Map<String,Integer> mapOfTLL3MTotal                         = new map<String,Integer>();
            Map<String,Integer> mapOfTLPlannedTotal                     = new map<String,Integer>();
            Map<String,String> mapOfTerrAndTL                           = new map<String,String>();
            Map<String,String> mapOfCatCodeAndCatName                   = new map<String,String>();
            Map<String,ID> mapOfCatAndROId                              = new map<String,ID>();
            Map<String,Integer> mapOfTLCatASP                           = new map<String,Integer>();
            Map<String,id> mapOfTerritoryAndUserID                      = new Map<String,ID>();
            Map<String,Integer> mapOfDLPlannedTotal                     = new map<String,Integer>();
            Map<String,Integer> mapOfDLGapTotal                         = new map<String,Integer>();
            Map<String,Decimal> mapofDealerAndSpeValue                  = new map<String,Decimal>();
           
            Set<String> catSet      = new set<String>();
            Set<String> setOfTL     = new set<String>();
            Set<Id> regionId        = new Set<Id>();
            Set<String> regionCode  = new Set<String>();
            Set<String> setOFTerritoryCode  = new Set<String>();
            set<String> catSetSpecialty = new set<String>();
            set<String> dealerCusNumber = new set<String>();
            //getting id of Replacement sales planning record types for dealer , RO, TL and specialty dealer record type
            Id replaceDealerId              = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
            Id replaceROId                  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
            Id replaceTLId                  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
            Id specialtyDealerId            = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: SpecialtyDealerLabel Limit 1].Id;
            
            String ss ='';
            //getting current date for month and year
            Date d = system.today();
            
            
            
            //getting region for logged in user 
            if(userRMRol.size() > 0 && UserInfo.getUserRoleId() == userRMRol[0].Id){
                regionForRM = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: rmLabel ];
                for(UserTerritory2Association reg : regionForRM){
                    regionId.add(reg.Territory2Id);
                    regionCode.add(reg.Territory2.name);
                }
                
            }
            system.debug(regionForRM+'regionForRM');
            system.debug(regionCode+'regionCode');
            //getting territory of region 
            if(regionID.size() > 0){
                listOfTerr = [select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId,(Select Territory2Id,UserID,User.name from UserTerritory2Associations Where RoleInTerritory2=: tlLabel ) from Territory2 where ParentTerritory2Id IN: regionId];
            }
            system.debug(listOfTerr+'listOfTerr');
            if(listOfTerr.size() > 0){
                for(Territory2 temp : listOfTerr){
                            setOFTerritoryCode.add(temp.name);
                            for(UserTerritory2Association s : temp.UserTerritory2Associations){
                                mapOfTerritoryAndUserID.put(temp.Name,s.UserID);
                                setOfTL.add(s.User.name);
                                mapOfTerrAndTL.put(s.User.name,temp.Name);
                            }
                        
                    }
                    
                
            }
            system.debug(setOfTL+'setOfTL');
            //system.debug(mapOfTerritoryAndUserID+'mapOfTerritoryAndUserID');
            //system.debug(mapOfTerrAndTL+'mapOfTerrAndTL');
            //getting Category from custom setting  
            List<Sales_Planning_Categories__c> spc = Sales_Planning_Categories__c.getall().values();
                for(Sales_Planning_Categories__c sp:spc){
                    if(sp.Include_in_Sales_Planning__c == true){
                        catSet.add(sp.Category_Code__c); 
                        mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);  
                    }
                         
            }
            
            //getting Specialty Category from custom setting  
            List<Speciality_Sales_Planning_Categories__c> spcCat = Speciality_Sales_Planning_Categories__c.getall().values();
            for(Speciality_Sales_Planning_Categories__c sp : spcCat) {
                if(sp.Include_in_Sales_Planning__c == true){
                    catSetSpecialty.add(sp.Category_Code__c);
                    //mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);     
                }     
            }
            //getting all current month RO records for logged in user 
            salesPlanningRO = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:yearnum AND Month__c =:monthnum AND RecordTypeId =: replaceROId AND Category__c In :catSet];
            if(salesPlanningRO.size() > 0){
                for(Sales_Planning__c temp :salesPlanningRO){
                    mapOfROTarget.put(temp.Category__c,Integer.valueOf(temp.Target_Quantity__c));
                    mapOfCatAndROId.put(temp.Category__c,temp.id);
                    mapOfTLCatASP.put(temp.Category__c,Integer.valueOf(temp.ASP__c));
                }
            }
            system.debug(salesPlanningRO.size() +'salesPlanningRO');
            system.debug(mapOfROTarget+'mapOfROTarget');
            system.debug(catSet+'catSet');
            //Specialty Value Calculation
                    salesPlanningDealerSpecialty = [SELECT Id,Value__c,Dealer__c,RecordTypeId,SPExternalIDTL__c,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:yearnum AND Month__c =:monthnum AND RecordTypeId =: specialtyDealerId AND Category__c In :catSetSpecialty];
                    if(salesPlanningDealerSpecialty.size() > 0){
                        for(Sales_Planning__c spe : salesPlanningDealerSpecialty){
                                dealerCusNumber.add(spe.Dealer_CustNumber__c);
                                totalValue = 0.0;
                                if(mapofDealerAndSpeValue.containsKey(spe.Dealer_CustNumber__c) ){
                                    totalValue = mapofDealerAndSpeValue.get(spe.Dealer_CustNumber__c);
                                    if(spe.Value__c != null){
                                        totalValue += spe.Value__c;
                                    }
                                    
                                    mapofDealerAndSpeValue.put(spe.Dealer_CustNumber__c,totalValue);
                                }else{
                                    totalValue1 = 0.0;
                                    if(spe.Value__c != null){
                                        totalValue1 = spe.Value__c;
                                    }
                                    mapofDealerAndSpeValue.put(spe.Dealer_CustNumber__c,totalValue1);
                                }
                        }
                    }
                    system.debug(mapofDealerAndSpeValue+'mapofDealerAndSpeValue');
                   listOFDealerTOUpdateSpecialtyVal = [Select id,Name,KUNNR__c,Specialty_Value__c,Sales_District_Text__c From Account Where KUNNR__c IN: dealerCusNumber];
                   system.debug(listOFDealerTOUpdateSpecialtyVal.size()+'JJJJJJ');
                   if(listOFDealerTOUpdateSpecialtyVal.size() > 0){
                        for(Account acc : listOFDealerTOUpdateSpecialtyVal){
                            if(mapofDealerAndSpeValue.containsKey(acc.KUNNR__c)){
                                acc.Specialty_Value__c = mapofDealerAndSpeValue.get(acc.KUNNR__c);
                            }else{
                                acc.Specialty_Value__c = 0.0;
                            }
                            
                        }
                        system.debug(listOFDealerTOUpdateSpecialtyVal+'VVVVVVVVVV');
                        upsert listOFDealerTOUpdateSpecialtyVal KUNNR__c;
                        //calculating Total Specialty Value for TL from each of their Dealers
                        for(Account acc : listOFDealerTOUpdateSpecialtyVal){
                                totalSpeValueTL = 0.0;
                                totalSpeValueTL1 = 0.0;
                                if(mapOFTlAndSpeValue.containsKey(acc.Sales_District_Text__c)){
                                    totalSpeValueTL = mapOFTlAndSpeValue.get(acc.Sales_District_Text__c);
                                    if(acc.Specialty_Value__c != null){
                                        totalSpeValueTL += acc.Specialty_Value__c;
                                    }
                                    mapOFTlAndSpeValue.put(acc.Sales_District_Text__c,totalSpeValueTL);
                                }else{
                                    if(acc.Specialty_Value__c != null){
                                        totalSpeValueTL1 += acc.Specialty_Value__c;
                                    }
                                    mapOFTlAndSpeValue.put(acc.Sales_District_Text__c,totalSpeValueTL1);
                                }
                        }
                   }
            system.debug(mapOFTlAndSpeValue+'mapOFTlAndSpeValue');
            //checking if already existing TL records present in database
            
            existingSalesPlanningTL = [SELECT id,Parent_Sales_Planning__c ,SYS_TL_CAT__c,Category__c,Category_Description__c ,Month__c ,Year__c ,Region_code__c ,Region_Description__c,RecordTypeId  ,Territory_Code__c ,SPExternalIDTL__c,Total_planned__c,Total_LYCM__c,Total_L3M__c,Total_Budget__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:yearnum AND Month__c =:monthnum AND RecordTypeId =: replaceTLId AND Category__c In :catSet];
            if(existingSalesPlanningTL.size() > 0){
                mapOfTLSP = new map<String,Sales_Planning__c>(); 
                for(Sales_Planning__c sp: existingSalesPlanningTL){
                    mapOfTLSP.put(sp.SYS_TL_CAT__c,sp);
                }
                //system.debug(existingSalesPlanningTL.size()+'existingSalesPlanningTL');
            }
            
          //calculating Running total for each category from each DL under that category
    
        if(mapOfTLSP.size() > 0){
            mapOfTLPlannedTotal = new Map<String,Integer>();
            for(String key : mapOfTLSP.keySet()){
                totalPlannedAllTL = 0;
                totalPlannedAllTL1 = 0;
                                
                salesPlanningTL1 = new Sales_Planning__c();
                salesPlanningTL1 = mapOfTLSP.get(key);
                
                if(mapOfTLPlannedTotal.containsKey(salesPlanningTL1.Category__c)){
                    totalPlannedAllTL1 = 0;
                    totalPlannedAllTL1 = mapOfTLPlannedTotal.get(salesPlanningTL1.Category__c);
                    totalPlannedAllTL1 += Integer.ValueOf(salesPlanningTL1.Total_planned__c);
                    mapOfTLPlannedTotal.put(salesPlanningTL1.Category__c,totalPlannedAllTL1);
                    
                }
                else{
                    totalPlannedAllTL = 0;
                    if(salesPlanningTL1.Total_planned__c != null){
                        totalPlannedAllTL = Integer.ValueOf(salesPlanningTL1.Total_planned__c);
                        mapOfTLPlannedTotal.put(salesPlanningTL1.Category__c,totalPlannedAllTL);
                    }
                    
                }                      
            }                                        
        } 
     
        system.debug('---------------*****------------------------------------------mapOfTLPlannedTotal'+mapOfTLPlannedTotal);
        //Calculate Gap for each category from each DL under that category
        if(mapOfTLSP.size() > 0){
            for(String key : mapOfROTarget.keySet()){
                totalGapAllDL = 0;
                totalTargetGap = 0;
                totalRunningGap = 0;
                
                if(mapOfROTarget.get(key) != null && mapOfTLPlannedTotal.get(key) != null){
                    totalTargetGap = mapOfROTarget.get(key);
                    totalRunningGap = mapOfTLPlannedTotal.get(key);
                    totalGapAllDL = totalTargetGap - totalRunningGap ;
                    
                }else if(mapOfROTarget.get(key) != null && mapOfTLPlannedTotal.get(key) == null){
                    
                    totalTargetGap = mapOfROTarget.get(key);
                    totalRunningGap = 0;
                    totalGapAllDL = totalTargetGap - totalRunningGap ;
                }
                mapOfDLGapTotal.put(key,totalGapAllDL);
            }
        }  
        
        
        //filling Wrapper for displaying in vf page  
        
        List<tlWrapper> tlWiseRecord      = new List<tlWrapper>();
        
        for(String tls : setOfTL){
            tlWrapper c         =   new tlWrapper();
            c.tlName            =   tls ;
            c.tlTerritory      =  mapOfTerrAndTL.get(tls);
            c.catList           =   new List<categoryWrapper>(); 
            
            if(mapOFTlAndSpeValue.get(mapOfTerrAndTL.get(tls)) != null ){
                c.tlSpeValue =     String.ValueOF(mapOFTlAndSpeValue.get(mapOfTerrAndTL.get(tls)));
            }else{
                c.tlSpeValue =     String.ValueOF(0);
            }
            
            //system.debug(ss+'ss'); 
            system.debug(mapOfTLSP.size()+'mapOfTLSP');   
            //system.debug(mapOfCatCodeAndCatName+'mapOfCatCodeAndCatName');   
            //system.debug(existingSalesPlanningTL.size()+'existingSalesPlanningTL');  
            
            for(String catname:    catSet ){                                
                categoryWrapper cc      = new categoryWrapper();
                if(mapOfROTarget.containsKey(catname)){
                        
                        cc.catName              = mapOfCatCodeAndCatName.get(catname);
                        cc.columnOrder          = (Sales_Planning_Categories__c.getValues(mapOfCatCodeAndCatName.get(catname))).Sort_Order__c;
                        cc.tlName1              = tls;
                        cc.catTarget            = String.valueOf(mapOfROTarget.get(catname));
                        cc.terrCode             = mapOfTerrAndTL.get(tls);
                        cc.catASP               = String.valueOf(mapOfTLCatASP.get(catname));
                        if(mapOfDLGapTotal.get(catname) == 0 && mapOfTLPlannedTotal.get(catname) != null){
                        system.debug(mapOfTLSP.size()+'mapOfTLSP');  
                        system.debug('---------------*****------------------------------------------mapOfTLPlannedTotal'+mapOfTLPlannedTotal);     
                                cc.catRunning               = String.valueOf(mapOfTLPlannedTotal.get(catname));
                             }else if(mapOfTLPlannedTotal.get(catname) == null){
                                cc.catRunning               = String.valueOf(0); 
                             }
                             if(mapOfDLGapTotal.get(catname) == 0){
                                cc.catGap                   = String.valueOf(mapOfDLGapTotal.get(catname)); 
                             }
                        if(mapOfTLSP.size() > 0 && mapOfTLSP != null && mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname) != null){
                            if(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_Budget__c != null){
                                cc.budget = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_Budget__c); 
                            }else{
                                cc.budget = String.valueOf(0);
                            }
                            
                            if((mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_LYCM__c) != null){
                                cc.Lycm = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_LYCM__c); 
                            }else{
                                cc.Lycm = String.valueOf(0);
                            }
                            
                            if((mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_L3M__c) != null){
                                cc.L3M = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_L3M__c); 
                            }else{
                                cc.L3M = String.valueOf(0);
                            }
                            
                            if((mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c) != null){
                                     //system.debug(mapOfDLGapTotal+'UUUU');
                                     //system.debug(catname+'DDDD');
                                     
                                     plan = 0;
                                     makeGapzero = 0;
                                     /*
                                     if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) > 0 ){
                                            system.debug(Integer.valueOf(cc.L3M));                                          
                                            plan = Integer.ValueOF (mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c + mapOfDLGapTotal.get(catname));                                           
                                            cc.planned = String.valueOf(plan);
                                            
                                            makeGapzero = mapOfDLGapTotal.get(catname);
                                            makeGapzero = 0;
                                            mapOfDLGapTotal.put(catname,makeGapzero);
                                            mapOfTLPlannedTotal.put(catname,mapOfROTarget.get(catname));
                                            cc.catRunning               = String.valueOf(mapOfROTarget.get(catname)); 
                                            cc.catGap                   = String.valueOf(mapOfDLGapTotal.get(catname));  
                                            
                                     }else if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) < 0  ){
                                            plan = Integer.ValueOF (mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c + mapOfDLGapTotal.get(catname));
                                            cc.planned = String.valueOf(plan);
                                            
                                            makeGapzero = mapOfDLGapTotal.get(catname);
                                            makeGapzero = 0;
                                            mapOfDLGapTotal.put(catname,makeGapzero);
                                            mapOfDLPlannedTotal.put(catname,mapOfROTarget.get(catname));
                                            cc.catRunning               = String.valueOf(mapOfROTarget.get(catname)); 
                                            cc.catGap                   = String.valueOf(mapOfDLGapTotal.get(catname));  
                                            
                                     }
                                     else{
                                        
                                        //system.debug('-------------------'+mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c);
                                        cc.planned = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c); 
                                     }*/
                                     cc.planned = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).Total_planned__c); 
                                 }else{
                                            cc.planned = String.valueOf(0);
                                 }
                            
                            if((mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).SPExternalIDTL__c) != null){
                                cc.spTLID = String.valueOf(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname).SPExternalIDTL__c); 
                            }else{
                                cc.spTLID = String.valueOf(0);
                            }                         
                        }
                         else if(mapOfTLSP.get(mapOfTerrAndTL.get(tls)+ catname) == null){                         
                                 cc.budget              = String.valueOf(0);
                                 cc.L3M                 = String.valueOf(0);
                                 cc.Lycm                = String.valueOf(0);
                                 /*
                                 if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) > 0){                                      
                                        cc.planned = String.valueOf(mapOfDLGapTotal.get(catname));
                                        
                                        makeGapzero = mapOfDLGapTotal.get(catname);
                                        makeGapzero = 0;
                                        mapOfDLGapTotal.put(catname,makeGapzero);
                                                                                 
                                        cc.catGap                   = String.valueOf(mapOfDLGapTotal.get(catname));
                                        cc.catRunning               = String.valueOf(mapOfROTarget.get(catname));
                                        
                                        
                                 }else{*/
                                        cc.planned = String.valueOf(0);
                                 //}
                             }
                        c.catList.add(cc);
                        c.catList.sort();
                }
                
        }
        tlWiseRecord.add(c);
      } 
     // system.debug(tlWiseRecord.size()+'---------------------------------------'+tlWiseRecord);
        return tlWiseRecord;
            
    }
     @RemoteAction
    Public Static List<tlWrapper> getSalesPlanningOfTL (String loggedInUserId,String Yearnum,String monthnum,String monthtext,String Yeartext) {
   			return null; 
    }
}