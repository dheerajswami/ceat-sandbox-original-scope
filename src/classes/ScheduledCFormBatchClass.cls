global class ScheduledCFormBatchClass implements Schedulable{   
   global void execute(SchedulableContext sc) {      
      CFormBatchClass cForm = new CFormBatchClass (); 
      database.executebatch(cForm ,1);
   }
}