global class SapDashboardScore_Mapping {

    
    /*
     * Author  :- kishlay Mathur  
     * Purpose :- Dashboard score Creation to SAP 
     * Date    :- 26/08/2015
     *
     */
    
    
    //-- ATTRIBUTES    
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static String createDashboardScore(List<sapDashboardScore.ZSFDC_DASHBOARD> IT_INPUT){
           
        try{
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            sapDashboardScore tst                         = new sapDashboardScore();
            sapDashboardScore.zws_sfdc_dashboard_score zws  = new sapDashboardScore.zws_sfdc_dashboard_score();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
            
            sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD toh      = new sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD();
            
            
            toh.item = IT_INPUT;
            
            
            sapDashboardScore.ZSFDC_DASHBOARD_SCOREResponse_element e = new sapDashboardScore.ZSFDC_DASHBOARD_SCOREResponse_element();
            
            system.debug('HEADER ' + toh);
            
            
            e = zws.ZSFDC_DASHBOARD_SCORE(toh);
            system.debug('swayam----------------'+e);
            
            return e.RETURN_x;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }    
}