@isTest
public class DashboardController_TestClass{  
    
      static user userTLRep   = new user();
      static user userTLDRep  = new user();
      static user userRMRep   = new user();
      static user userCSTLRep = new user();
      static user adminUser   = new user();
      static  Server_Url__c server;
      
      static testmethod void createUserTest() {
        DashboardController db = new DashboardController();        
        
        /* Getting the TL User and its Territory */
        UserTerritory2Association userTL = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='TL_Replacement' limit 1];
        User tl = [select id,UserRole.Name, name from User where id =: userTL.UserId];
        //tl.UserRole.Name = 'TL_Replacement';
        //update tl;
        
        /* Getting the TLD User and its Territory */
        UserTerritory2Association userTLD = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='TLD' limit 1];
        User tld = [select id,name,UserRole.Name from User where id =: userTLD.UserId];
        //tld.UserRole.Name = 'TLD';
        //update tld;
        
        /* Getting the CSTL User and its Territory */
        UserTerritory2Association userCSTL = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='CSTL' limit 1];
        User cstl = [select id, name from User where id =: userCSTL.UserId];
        
        /* Getting the RM User and its Territory */
        UserTerritory2Association userRM = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='RM' limit 1];
        User rm = [select id, name from User where id =: userRM.UserId];
        
        /* Getting the RM User and its Territory */
        UserTerritory2Association userRBM = [select id,UserId,Territory2Id,Territory2.name,RoleInTerritory2 from UserTerritory2Association where RoleInTerritory2='RBM' limit 1];
        User rbm = [select id, name from User where id =: userRBM.UserId];
        
        
        /* Running as RM User */
        system.runAs(rm){ 
            /* Inserting Dashboard Summary Record   */
            string extId = userRM.Territory2.Name + string.valueof(date.today().month()) + string.valueof(date.today().year());   
            Dashboard_Summary_Score__c dashSummaryScore1 = new Dashboard_Summary_Score__c(DSS_External_ID__c=extId,Month__c=string.valueof(date.today().month()),OwnerId=rm.Id,Input_Score__c=0,Year__c=string.valueof(date.today().year()));
            insert dashSummaryScore1;
            
            DashboardController db1= new DashboardController();
            server = new Server_Url__c(url__c='https://ceat--fullcopy.cs6.my.salesforce.com');
            insert server;
            
            PageReference pg=DashboardController.getPage('RM','RM',rm.Id,string.valueof(date.today().month()), string.valueof(date.today().year()));         
        }
        
        /* Running as TL User */
        system.runAs(tl){
            string extId = userTL.Territory2.Name + string.valueof(date.today().month()) + string.valueof(date.today().year());   
            //Inserting Dashboard Summary Record
            Dashboard_Summary_Score__c dashSummaryScore2 = new Dashboard_Summary_Score__c(DSS_External_ID__c=extId,Month__c=string.valueof(date.today().month()),OwnerId=tl.Id,Input_Score__c=0,Year__c=string.valueof(date.today().year()));
            insert dashSummaryScore2;
            DashboardController db1= new DashboardController();
            PageReference pg=DashboardController.getPage('TL','TL',tl.Id,string.valueof(date.today().month()), string.valueof(date.today().year()));             
        }  
        
        /* Running as TLD User */      
        system.runAs(tld){
            //Inserting Dashboard Summary Record
            string extId = userTLD.Territory2.Name + string.valueof(date.today().month()) + string.valueof(date.today().year());   
            Dashboard_Summary_Score__c dashSummaryScore3 = new Dashboard_Summary_Score__c(DSS_External_ID__c=extId,Month__c=string.valueof(date.today().month()),Input_Score__c=0,OwnerId=tld.Id,Year__c=string.valueof(date.today().year()));
            insert dashSummaryScore3;
            DashboardController db1= new DashboardController();
            PageReference pg=DashboardController.getPage('TLD','TLD',tld.Id,string.valueof(date.today().month()), string.valueof(date.today().year())); 
        } 
        
        /* Running as CSTL User */      
        system.runAs(cstl ){ 
            //Inserting Dashboard Summary Record        
            string extId = userCSTL.Territory2.Name + string.valueof(date.today().month()) + string.valueof(date.today().year());   
            Dashboard_Summary_Score__c dashSummaryScore4 = new Dashboard_Summary_Score__c(DSS_External_ID__c=extId,Month__c=string.valueof(date.today().month()),OwnerId=cstl.Id,Input_Score__c=0,Year__c=string.valueof(date.today().year()));
            insert dashSummaryScore4;
            
            DashboardController db2 = new DashboardController();
            PageReference pg1=DashboardController.getPage('CSTL','CSTL',cstl.Id,string.valueof(date.today().month()), string.valueof(date.today().year())); 
        }
         /* Running as RBM User for else part*/
        system.runAs(rbm){ 
            /* Inserting Dashboard Summary Record   */
            string extId = userRBM.Territory2.Name + string.valueof(date.today().month()) + string.valueof(date.today().year());   
            Dashboard_Summary_Score__c dashSummaryScore1 = new Dashboard_Summary_Score__c(DSS_External_ID__c=extId,OwnerId=rm.Id,Input_Score__c=0,Year__c=string.valueof(date.today().year()));
            insert dashSummaryScore1;           
            DashboardController db1= new DashboardController();            
            list<DashboardController.RoleWrapperClass> wrapperList = DashboardController.fetchUsers('CSTL');
            list<DashboardController.RoleWrapperClass> wrapperList1 = DashboardController.fetchUsers('TL');
            list<DashboardController.RoleWrapperClass> wrapperList2 = DashboardController.fetchUsers('All');
            PageReference pg=DashboardController.getPage('RBM','RM',rm.Id,string.valueof(date.today().month()), string.valueof(date.today().year())); 
            
            PageReference pg1=DashboardController.getPage('RBM','RBM',rbm.Id,string.valueof(date.today().month()), string.valueof(date.today().year()));          
        }
      }
}