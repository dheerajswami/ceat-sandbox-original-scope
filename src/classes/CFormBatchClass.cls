global class CFormBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts{
    global String query;
    global String frmDate_cform;
    global String toDate_cform;
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    global CFormBatchClass(String q,String frmDate_cform,String toDate_cform){
        this.query = q;
        this.frmDate_cform = frmDate_cform;
        this.toDate_cform = toDate_cform;
    }
    global CFormBatchClass(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(query == null){
            query = 'select Id,KUNNR__c from Account where RecordType.Name = \'OE\' AND Active__c = true AND KUNNR__c = \'55000607\' ';

        }
        
        String from_date = UtilityClass.getStringDate(Date.today().addDays(-30));
        frmDate_cform = (this.frmDate_cform == null)?from_date:this.frmDate_cform;
        
        String to_date = UtilityClass.getStringDate(Date.today());
        toDate_cform = (this.toDate_cform == null)?to_date:this.toDate_cform;
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<C_Form__c> cFormsToUpsert = new List<C_Form__c>();
        for(sobject s : scope){
            Account acc = (Account)s;
            String customerId = UtilityClass.addleadingZeros(acc.KUNNR__c,Integer.valueOf(CUST_ID_LENGTH));    
            
            List<CFormDetails.CFormMapping> listOfCForms = CFormDetails.getAllCFormDetails(customerId,frmDate_cform,toDate_cform);
            system.debug(listOfCForms);
            for(CFormDetails.CFormMapping cFormDetail : listOfCForms){
                C_Form__c cForm = new C_Form__c();
                cForm.Customer_Part_No__c = cFormDetail.CUST_PART_NUMBER;
                cForm.Invoice__c = cFormDetail.INVOICE;
                cForm.Invoice_date__c = cFormDetail.INVOICE_DATE;
                cForm.Invoice_Qty__c = cFormDetail.INVOICE_QTY;
                cForm.Material_Description__c = cFormDetail.MATERIAL_DESCRIPTION;
                cForm.Material_Number__c = cFormDetail.MATERIAL;
                cForm.Month__c = cFormDetail.MONTH;
                cForm.PO__c = cFormDetail.PO_NO;
                cForm.Post_Tax__c = cFormDetail.POST_TAX;
                cForm.Pre_Tax__c = cFormDetail.PRE_TAX;
                cForm.Sales_Office__c = cFormDetail.SALES_OFFICE;
                cForm.Seller_State__c = cFormDetail.SELLER_STATE;
                cForm.Seller_TIN_No__c = cFormDetail.SELLER_TIN_NO;
                cForm.Tax_value__c = cFormDetail.TAX_VALUE;
                cForm.Customer__c = acc.Id;
                cForm.GRN_Date__c = cFormDetail.BUDAT;
                cForm.GRN_No__c = cFormDetail.MBLNR;
                cForm.External_Id__c = cFormDetail.INVOICE + ';' + cFormDetail.ITEM; 
                cFormsToUpsert.add(cForm);
                if(cFormsToUpsert.size() == 1000){
                    database.upsert(cFormsToUpsert, C_Form__c.External_Id__c,false);
                    cFormsToUpsert = new List<C_Form__c>(); 
                }
            }
            
        }
        if(!cFormsToUpsert.isEmpty()){
            //database.insert(cFormsToUpsert);
            List<Database.UpsertResult> res = database.upsert(cFormsToUpsert, C_Form__c.External_Id__c,false);
            Integer counter=0;
            for(Database.UpsertResult tmpResult : res) {
                counter++;
                System.debug('==#1 '+tmpResult);
            }
            System.debug('==#2 '+counter);
            
        }
    }
    
    global void finish(Database.BatchableContext BC){
    
          
             
    }
    
}