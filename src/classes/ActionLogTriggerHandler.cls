/************************************************************************************************************
    Author  : Sneha Agrawal
    Date    : 12/1/2014
    Purpose : Action Log Trigger Handler to fetch SAP code, Segment, Territory from Dealer/Customer/Account

*************************************************************************************************************/
public class ActionLogTriggerHandler {

        //-- SINGLETON PATTERN
    private static ActionLogTriggerHandler instance;
    public static ActionLogTriggerHandler getInstance() {
          if (instance == null) instance = new ActionLogTriggerHandler();
          return instance;
    }
    
    //-- METHODS 
     
    
    //-- Before Insert
    
      
     public void onBeforeInsert(final List<Task> newObjects){
     
      //  assignDL(newObjects);
     }
     
    
     //-- Before Update
     
      
     public void onBeforeUpdate(final List<Task> newObjects,final List<Task> oldObjects,final Map<Id, Task> newObjectsMap,final Map<Id, Task> oldObjectsMap){
        // assignDL(newObjects);
        
     }
     
    
     //-- Before Delete
     
      
     public void onBeforeDelete(final List<Task> oldObjects,final Map<Id, Task> oldObjectsMap){
     
        
     }
     
     //-- After Insert
     
      
     public void onAfterInsert(final List<Task> newObjects,final Map<Id, Task> newObjectsMap){
     
        
     }  
     
    
    //-- After Update
    
      
     public void onAfterUpdate(final List<Task> newObjects,final List<Task> oldObjects,final Map<Id, Task> newObjectsMap,final Map<Id, Task> oldObjectsMap){
     
        
     }
     
     
     //-- After Delete
          
     public void onAfterDelete(List<Task> oldObjects,final Map<Id, Task> oldObjectsMap){
     
        
     }
    
    public static void assignDL(List<Task> newALs) {    
    List<Task> actionlogList=new List<Task>();  
    Schema.DescribeSObjectResult visitPlanKey = Visits_Plan__c.sObjectType.getDescribe();
    Schema.DescribeSObjectResult accountKey = Account.sObjectType.getDescribe();  
    Account accountObj;
    Visits_Plan__c visitObj;
    String alId;

    Set<Id> visitIds  = new Set<Id>();
    Set<Id> accIds    = new Set<Id>();
    Map<Id,Account> mapOfWhatIdandAccount = new Map<Id,Account>();
        try{
              for(Task task : newALs){       
                       String whatID = String.valueOf(task.WhatId);
                       
                       /*
                        Check whether the what id is Visit Plan or Account objects            
                       */
                        if (whatID.startsWith(visitPlanKey.getKeyPrefix())) {
                            visitIds.add(whatID); 
                                 
                        }else if(whatID.startsWith(accountKey.getKeyPrefix())){
                            accIds.add(whatID);
                            
                        }
               }
                
               
                
               for(Visits_Plan__c vp :[SELECT Id,Dealer__c,Dealer__r.Name,Dealer__r.Customer_Group__c,Dealer__r.KUNNR__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c FROM Visits_Plan__c WHERE Id IN : visitIds]){
                  mapOfWhatIdandAccount.put(vp.Id,new Account(Id = vp.Dealer__c,Name = vp.Dealer__r.Name,Customer_Group__c = vp.Dealer__r.Customer_Group__c,KUNNR__c = vp.Dealer__r.KUNNR__c ,Customer_Segment__c = vp.Dealer__r.Customer_Segment__c, Sales_District_Text__c = vp.Dealer__r.Sales_District_Text__c ));
               }
               for(Account acc : [SELECT Id,Name,Customer_Segment__c,Customer_Group__c,Sales_District_Text__c,KUNNR__c FROM Account WHERE Id IN : accIds]){
                  mapOfWhatIdandAccount.put(acc.Id,acc);
               }

               for(Task task : newALs){
                  if(mapOfWhatIdandAccount.containsKey(task.WhatId)){
                      task.SAP_Code__c        = mapOfWhatIdandAccount.get(task.WhatId).KUNNR__c;
                      task.Customer_Name__c   = mapOfWhatIdandAccount.get(task.WhatId).Name;
                      task.Segment__c         = mapOfWhatIdandAccount.get(task.WhatId).Customer_Segment__c;
                      task.Dealer__c          = mapOfWhatIdandAccount.get(task.WhatId).Id;
                      task.Territory__c       = mapOfWhatIdandAccount.get(task.WhatId).Sales_District_Text__c;
                  }
               }
        }
        catch(Exception e){}

     }
     
     public static void notifyUser(List<Task> newALs,List<Task> oldALs) {  
       
           try{
           Map<Id,Id> oldActionLogMap=new Map<Id,Id>();
           EmailTemplate emailTemp=new EmailTemplate ();
           emailTemp=[SELECT id,Name FROM EmailTemplate where name = 'Action Log escalation notification' limit 1];
           for(Task t: oldALs){
              oldActionLogMap.put(t.id,t.Assign_Escalate__c);
           }              
         
           for(Task t: newALs)
            {        
               
                // Check whether it is re-assigned or not and if it is reassigned then check is it the same user or different user
                //Server_Url__c url=[Select url__c from Server_Url__c limit 1];
                String address = URL.getSalesforceBaseUrl().toExternalForm();
                System.debug('==#Base URL '+address);
                if(t.Assign_Escalate__c != null && t.Assign_Escalate__c != oldActionLogMap.get(t.id))  {         
                        
                        Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage(); 
                        User usr=[select email,id from User where id=:t.Assign_Escalate__c limit 1];
                       
                        String[] mail=new String[]{usr.email};
                        String Body = '<b>**************Action Log Assignment Alert***************</b><br><br>';
                        body+='<b>The following action log requires your attention : </b><br><br>';
						body+='<b>Issue Reported By  :  </b>'+UserInfo.getFirstName()+' '+UserInfo.getLastName()+'<br><br>';
						body+='<b>For Customer  :  </b>'+t.Customer_Name__c+'<br><br>';
                        body+='<b>Issue Reported  :  </b>'+t.Description+'<br><br>';
						body+='<b>Issue Reported Date  :  </b>'+t.Date_of_Reporting__c+'<br><br>';
                        body+='<b>Action to be taken  :  </b>'+t.Action_to_be_taken__c+'<br><br>';
                        body+='<b>Status  :  </b>'+t.status+'<br><br>';
                        body+='<b>Deadline  :  </b>'+t.ActivityDate+'<br><br>';
                        //body+='Click on the link to access the Action log : ';<a href="'+url.url__c+'/'+t.Id'" >Click Here</a>';   
                        body+='<b>Click on the link to access the Action log :  <a href="'+address+'/'+t.Id+'">Click Here </a></b><br><br>';
                        
                        mailHandler.setToAddresses(mail);
                        mailHandler.setHtmlBody(body);
                        mailHandler.setReplyTo(UserInfo.getUserEmail());
                        mailHandler.setSubject('Action log related to '+t.What+' has been re-assigned to you');
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailHandler});
                       
                    }                        
                    
               }
               }catch(Exception e){
                   system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
               }
       }
     public static void notifyUserInsert(List<Task> newALs) { 
          try{
              
             //Server_Url__c url=[Select url__c from Server_Url__c limit 1];
             String address = URL.getSalesforceBaseUrl().toExternalForm();
             System.debug('==#Base URL '+address);
              for(Task t: newALs)
                {        
                    
                    
                    // Check whether it is re-assigned or not 
                    if(t.Assign_Escalate__c != null )  {                                    
                
                        Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage(); 
                        User usr=[select email,id from User where id=:t.Assign_Escalate__c limit 1];
                        
                        String[] mail=new String[]{usr.email};
                         String Body = '<b>**************Action Log Assignment Alert***************</b><br><br>';
                        body+='<b>The following action log requires your attention : </b> <br><br>';
						body+='<b>Issue Reported By  :  </b>'+UserInfo.getFirstName()+' '+UserInfo.getLastName() +'<br><br>';
						body+='<b>For Customer  :  </b>'+t.Customer_Name__c+'<br><br>';
                        body+='<b>Issue Reported  :  </b>'+t.Description+'<br><br>';
						body+='<b>Issue Reported Date   :  </b>'+t.Date_of_Reporting__c+'<br><br>';
                        body+='<b>Action to be taken  :  </b>'+t.Action_to_be_taken__c+'<br><br>';
                        body+='<b>Status  :  </b>'+t.status+'<br><br>';
                        body+='<b>Deadline  :  </b>'+t.ActivityDate+'<br><br>'; 
                        body+='<b>Click on the link to access the Action log :  <a href="'+address+'/'+t.Id+'">Click Here </a></b><br><br>';
                        
                        mailHandler.setToAddresses(mail);
                        mailHandler.setHtmlBody(body);
                        mailHandler.setReplyTo(UserInfo.getUserEmail());
                        mailHandler.setSubject('Action log related to '+t.Customer_Name__c+' has been assigned to you');
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailHandler});
                       
                       }                        
                        
                   }
                }catch(Exception e){
                   system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
               }
       }
}