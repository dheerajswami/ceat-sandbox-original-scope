global class ScheduledBatch_DashboardInputTL implements Schedulable{
   global void execute(SchedulableContext sc) {
      DashboardInput_BatchClass dashboardTLBatch = new DashboardInput_BatchClass(); 
      database.executebatch(dashboardTLBatch ,1);
   }
}