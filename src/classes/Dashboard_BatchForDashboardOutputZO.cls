global class Dashboard_BatchForDashboardOutputZO implements Database.Batchable<sObject> {    
    /* ================================================
    @Name:  Dashboard_BatchForDashboardOutputZO
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor tquila
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will give output score of ZO dashboard                                                                                               
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     10/08/2015      INITIAL DEVELOPMENT                                 
     2.0        Kishlay@extentor     31/12/2015      On Demand run for any month and Added active checkbox as global parameter while running batch for prv month
@=======================================================  */
    Id totalOutputId ;
    Id replaceDealerId ;
    String catDashList = '';
    String tmpMonth;
    String tmpYear;
    String currentMonth;
    String currentDay;
    String currentYear;
    String OutputDashboardRecType                           = System.Label.OutputRecType;
    String OutputtotalDashboardRecType                      = System.Label.TotalOutputRecordType;
    String repDealerLabel                                   = System.Label.Replacement_Dealer;
    list<String> catSet                                              = new list<String>();
    list<Dashboard_Weightage_Master__c>totalValWeight                = new list<Dashboard_Weightage_Master__c>();
    list<Dashboard_Master__c> dashboardMaster                        = new list<Dashboard_Master__c>();
    List<String> catsetParam                                         = new List<String>();
    list<Dealer_Type_Field_Matching__c> dtfm;
    list<String> zoneList                                          = new list<String>();
    list<String> regionNames                                           = new list<String>();
    list<AggregateResult> dashboardScoreRM                        = new list<AggregateResult>();
    //list<Dashboard_Score__c> dashboardScoreRM                   = new List<Dashboard_Score__c>(); 
    list<AggregateResult> dashboardOutputScoreRM                  = new list<AggregateResult>(); 
    list<Dashboard_Summary_Score__c> ZOScore                         = new list<Dashboard_Summary_Score__c>();
    list<UserTerritory2Association> userForZone                    = new list<UserTerritory2Association>();
    list<Dashboard_Score__c> dsnewList                               = new list<Dashboard_Score__c>();
    list<Dashboard_Output_Score__c> dosListInsert                    = new list<Dashboard_Output_Score__c>();
    List<String> catSetIn ;
    list<Dashboard_Score__c> dbScoreRM                               = new list<Dashboard_Score__c>();
    
    set<String> allcatSet                                            = new set<String>();
    set<String> zoneSet                                            = new set<String>();
    set<id> terrTypezoneID                                            = new set<Id>();
    Set<String> catdashSet                                           = new Set<String>();
    Set<String> setOFParametersAllCat                                = new Set<String>();
    set<String> setofCatParam                                        = new set<String>();
    set<id> zoneID                                                    = new set<ID>();
    set<String> setofDSCat                                           = new set<String>();
    Dashboard_Summary_Score__c scoreRM;
    
    Map<String,String> mapOFvalueAndApiName                                     = new Map<String,String>();
    Map<String,List<String>> mapOFDashCatAndCatCode                             = new Map<String,List<String>>();
    Map<String,integer> mapOFCatParamActual                                     = new Map<String,integer>();
    Map<String,integer> mapOFCatParamPlan                                       = new Map<String,integer>();
    Map<String,integer> mapOFCatParamL3M                                        = new Map<String,integer>();
    Map<String,integer> mapOFCatParamLYCM                                       = new Map<String,integer>();
    Map<String,integer> mapOFCatParamMTDTarget                                  = new Map<String,integer>();
    Map<String,integer> mapOFCatParamBilledDealer                               = new Map<String,integer>();
    Map<String,integer> mapOFCatParamPlanDealer                                 = new Map<String,integer>();
    Map<String,integer> mapOFCatParamNoDis                                      = new Map<String,integer>();
    
    Map<String,integer> mapOFCatParamActualAll                                  = new Map<String,integer>();
    Map<String,integer> mapOFCatParamPlanAll                                    = new Map<String,integer>();
    Map<String,integer> mapOFCatParamL3MAll                                     = new Map<String,integer>();
    Map<String,integer> mapOFCatParamLYCMAll                                    = new Map<String,integer>();
    Map<String,integer> mapOFCatParamMTDTargetAll                               = new Map<String,integer>();
    Map<String,integer> mapOFCatParamBilledDealerAll                            = new Map<String,integer>();
    Map<String,integer> mapOFCatParamPlanDealerAll                              = new Map<String,integer>();
    Map<String,integer> mapOFCatParamNoDisAll                                   = new Map<String,integer>();
    Map<String,Dashboard_Weightage_Master__c> mapOFCatAndParam                  = new Map<String,Dashboard_Weightage_Master__c>();
    Map<String,Dashboard_Weightage_Master__c> mapOFCatCodeAndParam              = new Map<String,Dashboard_Weightage_Master__c>();
    global static String month_val;
    global static String year_val;
    global static Boolean active_val;

    global Dashboard_BatchForDashboardOutputZO (){
        currentDay              = String.valueOf((Date.today()).day());
        /*currentMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        currentYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
*/        
        tmpMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        tmpYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
        
        currentMonth = (month_val == null)?tmpMonth:month_val;  
        currentYear  =  (year_val == null)?tmpYear:year_val;
        //terriTypeReg                = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'RO'];
        for(Territory2Type temp : [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Zone']){
            terrTypezoneID.add(temp.id);
        }
        //list<Sales_Planning_Categories__c> dashboardCat = Sales_Planning_Categories__c.getall().values();
            for(Sales_Planning_Categories__c sp:Sales_Planning_Categories__c.getall().values()){
              if(sp.Include_in_Sales_Planning__c == true){
                  allcatSet.add(sp.Category_Code__c);
              }  
              if(sp.Include_in_Dashboard__c == true){
                catdashSet.add(sp.Category_Code__c); 
                if(mapOFDashCatAndCatCode.containsKey(sp.Category_name_as_in_Dashboard__c)) {
                    catSet          = new list<String>();
                    catSet          = mapOFDashCatAndCatCode.get(sp.Category_name_as_in_Dashboard__c); 
                    catset.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                }else{
                    catSet          = new List<String>();
                    catSet.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                }
              }      
        }
        replaceDealerId             = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        totalOutputId               = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: OutputtotalDashboardRecType Limit 1].Id;
        //dwm                         = new List<Dashboard_Weightage_Master__c>();
        totalValWeight              = new list<Dashboard_Weightage_Master__c>();
        if(month_val == null && year_val == null && active_val == null){
            dashboardMaster             = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true AND Role__c = 'ZO'];
            //dwm                         = [Select Id,Category__c,Category_Code__c,Parameters_Output__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c WHERE RecordType.Name =: AND Testing__c = true AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c = 'RM'];
            totalValWeight              = [Select Id,Role__c,Weightage__c,RecordTypeID FROM Dashboard_Weightage_Master__c WHERE RecordTypeID =:totalOutputId AND Testing__c = true AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c = 'ZO'];
            for(Dashboard_Weightage_Master__c param : [Select Id,Category__c,Category_Code__c,Parameters_Output__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c WHERE RecordType.Name =:OutputDashboardRecType AND Testing__c = true AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c = 'ZO']){
                mapOFCatAndParam.put(param.Category__c,param); 
                mapOFCatCodeAndParam.put(param.Category_Code__c,param); 
                catsetParam             = mapOFDashCatAndCatCode.get(param.Category__c);
                
                String[] paramSplit     = (param.Parameters_Output__c).split(';');
                if(paramSplit.size() > 0){
                    for(String st : paramSplit){
                        setOFParametersAllCat.add(st);
                        setofCatParam.add(param.Category_Code__c+'-'+st);
                    }
                }
             }
        }else if(month_val != null && year_val != null && active_val != null){
             dbScoreRM = [SELECT id,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Role__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__r.Category__c,Dashboard_Weightage_Master__r.Parameters_Output__c,Dashboard_Weightage_Master__r.Dashboard_Master__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c where Month__c =: month_val AND Year__c =: year_val AND Zone_Code__c != null AND Dashboard_Weightage_Master__r.Testing__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Active__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Role__c = 'ZO' LIMIT 1];
            
             totalValWeight              = [Select Id,Role__c,Weightage__c,RecordTypeID FROM Dashboard_Weightage_Master__c WHERE RecordTypeID =:totalOutputId AND Dashboard_Master__c =: dbScoreRM[0].Dashboard_Weightage_Master__r.Dashboard_Master__c  AND Role__c = 'ZO'];
                for(Dashboard_Weightage_Master__c param : [Select Id,Category__c,Category_Code__c,Parameters_Output__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c WHERE RecordType.Name =:OutputDashboardRecType AND Dashboard_Master__c =: dbScoreRM[0].Dashboard_Weightage_Master__r.Dashboard_Master__c  AND Weightage__c > 0 AND Role__c = 'ZO']){
                    mapOFCatAndParam.put(param.Category__c,param); 
                    mapOFCatCodeAndParam.put(param.Category_Code__c,param); 
                    catsetParam             = mapOFDashCatAndCatCode.get(param.Category__c);
            
                    String[] paramSplit     = (param.Parameters_Output__c).split(';');
                    if(paramSplit.size() > 0){
                        for(String st : paramSplit){
                            setOFParametersAllCat.add(st);
                            setofCatParam.add(param.Category_Code__c+'-'+st);
                        }
                    }
                }
        }
        dtfm                        =Dealer_Type_Field_Matching__c.getall().values();
        //dashFieldValue              =DashBoardFieldValue__c.getall().values();
        for(DashBoardFieldValue__c temp : DashBoardFieldValue__c.getall().values()){
            if(temp.Field_Values__c != null ){
                
                String[] stList     = (temp.Field_Values__c).split(':');
                system.debug(stList+'stList');
                if(stList.size() > 0){
                    for(String str : stList){
                        mapOFvalueAndApiName.put(str,temp.Field_API_Name__c);
                    }   
                }
            }
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
         String query;
         String Replacements            = 'Replacements';
         String Region                  = 'ZW02';
         //query                    = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE DeveloperName =  \'ZW02\' AND Territory2TypeId IN: terrTypezoneID AND ParentTerritory2.ParentTerritory2.developername =: Replacements'; 
         query                  = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypezoneID AND ParentTerritory2.ParentTerritory2.developername =: Replacements order by Name DESC'; 
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        for(sObject temp : scope){
            Territory2 sps              =(Territory2)temp;
            zoneSet.add(sps.Name);
            zoneID.add(sps.id);
            zoneList.addall(zoneSet);
         }
         userForZone = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where Territory2Id IN:zoneID  and RoleInTerritory2 = 'ZGM'];
          system.debug('------zoneSet'+zoneSet);
          system.debug('------zoneList'+zoneList[0]);
          // Getting list of territories under region or RM      
          list<Territory2>    territoryList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE ParentTerritory2.DeveloperName =: zoneList[0]]; 
          if(territoryList.size() > 0){
                for(Territory2 terr : territoryList){
                     regionNames.add(terr.DeveloperName);
                }  
          }
          //regionNames.sort();
          system.debug(regionNames+'regionNames');
          if(regionNames.size()>0){
             dashboardScoreRM = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Region_Code__c IN: regionNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            //dashboardScoreRM  = [SELECT ID,Actual_MTD__c,Category__c,Dashboard_Weightage_Master__c,DBExternalId__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Number_of_Dealers_Distributors__c,Parameters__c,Territory_Code__c,User__c,Value_L3M__c,Value_LYCM__c,Year__c FROM Dashboard_Score__c where  Territory_Code__c IN: regionNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat];
          }
          if(dashboardScoreRM.size() > 0){
                for(AggregateResult ds : dashboardScoreRM){
                    mapOFCatParamActual.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('actualMTD')));
                    mapOFCatParamPlan.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('Montarget')));
                    mapOFCatParamL3M.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('L3M')));
                    mapOFCatParamLYCM.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('LYCM')));
                    mapOFCatParamMTDTarget.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('targetMTD')));
                    mapOFCatParamBilledDealer.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('BilledDealer')));
                    mapOFCatParamPlanDealer.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('PlanDealer')));
                    mapOFCatParamNoDis.put(String.valueOf(ds.get('Category__c'))+String.valueOf(ds.get('Parameters__c')),Integer.valueOf(ds.get('NumDealer')));
                }
          }
          
          if(regionNames.size()>0){
             dashboardOutputScoreRM = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Region_Code__c IN: regionNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            //dashboardScoreRM  = [SELECT ID,Actual_MTD__c,Category__c,Dashboard_Weightage_Master__c,DBExternalId__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Number_of_Dealers_Distributors__c,Parameters__c,Territory_Code__c,User__c,Value_L3M__c,Value_LYCM__c,Year__c FROM Dashboard_Score__c where  Territory_Code__c IN: regionNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat];
          }
          if(dashboardOutputScoreRM.size() > 0){
                for(AggregateResult dos : dashboardOutputScoreRM){
                    mapOFCatParamActualAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('actualMTD')));
                    mapOFCatParamPlanAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('Montarget')));
                    mapOFCatParamL3MAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('L3M')));
                    mapOFCatParamLYCMAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('LYCM')));
                    mapOFCatParamMTDTargetAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('targetMTD')));
                    mapOFCatParamBilledDealerAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('BilledDealer')));
                    mapOFCatParamPlanDealerAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('PlanDealer')));
                    mapOFCatParamNoDisAll.put(String.valueOf(dos.get('Category__c')),Integer.valueOf(dos.get('NumDealer')));
                }
          }
          
          String keyExt ='';
          keyExt = zoneList[0]+currentMonth+currentYear;
          ZOScore = [SELECT id,Achievement__c,Active__c,Dashboard_Weightage_Master__c,DSS_External_ID__c,Score__c,Total_MTD_Actual__c,Total_MTD_Target__c,Total_Value__c,Total_Value_L3M__c,Total_Value_LYCM__c FROM Dashboard_Summary_Score__c WHERE DSS_External_ID__c =:keyExt AND Month__c =: currentMonth AND Year__C =:currentYear];
          if(!(ZOScore.size() > 0) && userForZone.size()>0 ){
              scoreRM = new Dashboard_Summary_Score__c(OwnerId  = userForZone[0].UserId,DSS_External_ID__c = keyExt,Month__c = currentMonth,Year__c = currentYear);
              database.upsert(scoreRM,Dashboard_Summary_Score__c.Fields.DSS_External_ID__c,false); 
          }
          AggregateResult[] querySPAll = [select sum(Value_L3M__c) L3mSum, sum(Actual_Sales_Value__c) ActualSum, SUM(MTD_target_Val__c) MTDTargetSum, Sum(Value_LYCM__c) LYCMSum, SUM(PlannedAspValue__c) PlanSum From Sales_Planning__c WHERE Dealer__r.Active__c = true AND RecordTypeId =: replaceDealerId AND Month__c =: currentMonth AND Year__c =: currentYear AND Region_code__c IN: regionNames];
          if(ZOScore != null && ZOScore.size() > 0 && querySPAll.size() > 0){
            
                 ZOScore[0].Total_Value_L3M__c = (querySPAll[0].get('L3mSum')) != null ? (Decimal)(querySPAll[0].get('L3mSum'))/100000 : 0;
                 ZOScore[0].Total_Value_LYCM__c = (querySPAll[0].get('LYCMSum')) != null ? (Decimal)(querySPAll[0].get('LYCMSum'))/100000 : 0;
                 ZOScore[0].Total_Value__c = (querySPAll[0].get('PlanSum')) != null ? (Decimal)(querySPAll[0].get('PlanSum'))/100000 : 0;
                 ZOScore[0].Total_MTD_Target__c = (querySPAll[0].get('MTDTargetSum')) != null ? (Decimal)(querySPAll[0].get('MTDTargetSum'))/100000 : 0;
                 ZOScore[0].Total_MTD_Actual__c = (querySPAll[0].get('ActualSum')) != null ? (Decimal)(querySPAll[0].get('ActualSum'))/100000 : 0;
                 if(ZOScore[0].Total_MTD_Target__c > 0 ){
                     Decimal aci = ((ZOScore[0].Total_MTD_Actual__c*100)/ZOScore[0].Total_MTD_Target__c);
                  
                    if(aci <= 100){
                        ZOScore[0].Score__c = (aci*totalValWeight[0].Weightage__c)/100;
                    }else{
                        ZOScore[0].Score__c = (100*totalValWeight[0].Weightage__c)/100;
                    }
                 }  
                 if(userForZone.size()>0){
                    ZOScore[0].OwnerId                   = userForZone[0].UserId;
                    }
                    ZOScore[0].Zone_Code__c = zoneList[0];
                    ZOScore[0].Dashboard_Weightage_Master__c = totalValWeight[0].id;
                    ZOScore[0].Month__c     = currentMonth;
                    ZOScore[0].Year__c      = currentYear;
             
         }
         try{
            database.upsert(ZOScore,Dashboard_Summary_Score__c.Fields.DSS_External_ID__c,false); 
         }catch(Exception e){}
         if(mapOFCatAndParam.size() > 0){
             for(String cat : mapOFCatAndParam.keyset()){
                    String[] paramset = (mapOFCatAndParam.get(cat)).Parameters_Output__c.split(';');
                    if(paramset.size() > 0){
                        for(String str : paramset){
                            Dashboard_Score__c ds=new Dashboard_Score__c();
                            if(userForZone.size()>0){
                            ds.User__c                                  = userForZone[0].UserId;
                            ds.OwnerId                   = userForZone[0].UserId;
                            }
                            if(ZOScore.size() > 0){
                                ds.Dashboard_Summary_Score__c           = ZOScore[0].id;
                            }else{
                                if(scoreRM != null){
                                    ds.Dashboard_Summary_Score__c           = scoreRM.id;
                                }
                                
                            }
                            ds.Dashboard_Weightage_Master__c            = mapOFCatAndParam.get(cat).Id;
                            ds.Zone_Code__c                             = zoneList[0];
                            ds.Month__c                                 = currentMonth;
                            ds.Year__c                                  = currentYear;
                            
                            ds.Parameters__c                            = str;
                            catSetIn = new list<String>();
                            catSetIn = mapOFDashCatAndCatCode.get(cat);
                            catSetIn.sort();
                            if(catSetIn.size() > 0){
                                catDashList = '';
                                for(String s : catSetIn){
                                    if(catDashList == ''){
                                        catDashList = s;
                                    }else{
                                        catDashList = catDashList +':'+ s;
                                    }   
                                }
                            }
                            ds.Category__c                              = catDashList.trim();  
                            setofDSCat.add(ds.Category__c );   
                            if(mapOFCatParamL3M.get(catDashList+str) != null){
                                ds.L3M__c                               = mapOFCatParamL3M.get(catDashList+str);
                            }else{
                                ds.L3M__c                               = 0;
                            }             
                            if(mapOFCatParamLYCM.get(catDashList+str) != null){
                                ds.LYCM__c                              = mapOFCatParamLYCM.get(catDashList+str);
                            }else{
                                ds.LYCM__c                              = 0;
                            }
                             if(mapOFCatParamPlanDealer.get(catDashList+str) != null){
                                ds.No_of_Dealer_Distributor_Plan__c         = mapOFCatParamPlanDealer.get(catDashList+str);
                            }else{
                                ds.No_of_Dealer_Distributor_Plan__c         = 0;
                            }
                            if(mapOFCatParamBilledDealer.get(catDashList+str) != null){
                                ds.No_of_Dealer_Distributor_Billed_Since__c = mapOFCatParamBilledDealer.get(catDashList+str);   
                            }else{
                                ds.No_of_Dealer_Distributor_Billed_Since__c = 0;   
                            }
                            if(mapOFCatParamPlan.get(catDashList+str) != null){
                                ds.Monthly_Target__c                        = mapOFCatParamPlan.get(catDashList+str); 
                            }else{
                                ds.Monthly_Target__c                        = 0; 
                            }                  
                            if(mapOFCatParamMTDTarget.get(catDashList+str) != null){
                                ds.MTD_Target__c                            = mapOFCatParamMTDTarget.get(catDashList+str);
                            }else{
                                ds.MTD_Target__c                            = 0;
                            }                                           
                            if(mapOFCatParamActual.get(catDashList+str) != null){
                                ds.Actual_MTD__c                            = mapOFCatParamActual.get(catDashList+str);
                            }else{
                                ds.Actual_MTD__c                            = 0;
                            }
                            if(mapOFCatParamNoDis.get(catDashList+str) != null){
                                ds.Number_of_Dealers_Distributors__c        = mapOFCatParamNoDis.get(catDashList+str);
                            }else{
                                ds.Number_of_Dealers_Distributors__c        = 0;
                            }
                            ds.DBExternalId__c                              = catDashList.trim() + str + zoneList[0] + currentMonth + currentYear; 
                            dsnewList.add(ds);
                            
                        }
                    }
                    
             }
         }
        try{
              database.upsert (dsnewList,Dashboard_Score__c.Fields.DBExternalId__c,false) ;
        }catch(Exception e){
              System.debug(e.getMessage());
        }
        if(setofDSCat.size() > 0){
             for(String cat : setofDSCat){
                Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c();
                if(userForZone.size()>0){
                dos.User__c                                                 = userForZone[0].UserId;
                }
                dos.Dashboard_Weightage_Master__c                           = mapOFCatCodeAndParam.get(cat).Id;
                if(ZOScore.size() > 0){
                    dos.Dashboard_Summary_Score__c          = ZOScore[0].id;
                }else{
                    if(scoreRM != null){
                        dos.Dashboard_Summary_Score__c          = scoreRM.id;
                    }
                    
                }
               
                dos.Zone_Code__c                                            = zoneList[0];
                dos.Month__c                                                = currentMonth;
                dos.Year__c                                                 = currentYear;
                dos.Category__c                                             = cat;     
                if(mapOFCatParamL3MAll.get(cat) != null){
                    dos.Total_L3M__c                                        = mapOFCatParamL3MAll.get(cat);
                }else{
                    dos.Total_L3M__c                                        = 0;
                }             
                if(mapOFCatParamLYCMAll.get(cat) != null){
                    dos.Total_LYCM__c                                       = mapOFCatParamLYCMAll.get(cat);
                }else{
                    dos.Total_LYCM__c                                       = 0;
                }
                if(mapOFCatParamPlanDealerAll.get(cat) != null){
                    dos.Total_No_of_dealers_distributors_planned__c         = mapOFCatParamPlanDealerAll.get(cat);
                }else{
                    dos.Total_No_of_dealers_distributors_planned__c         = 0;
                }
                if(mapOFCatParamBilledDealerAll.get(cat) != null){
                    dos.Total_dealers_distributors_billed_since__c          = mapOFCatParamBilledDealerAll.get(cat);   
                }else{
                    dos.Total_dealers_distributors_billed_since__c          = 0;   
                }
                
                if(mapOFCatParamPlanAll.get(cat) != null){
                    dos.Total_Monthly_Target__c                             = mapOFCatParamPlanAll.get(cat); 
                }else{
                    dos.Total_Monthly_Target__c                             = 0; 
                }    
                if(mapOFCatParamNoDisAll.get(cat) != null){
                    dos.Total_No_of_Dealers_Distributor__c                 = mapOFCatParamNoDisAll.get(cat); 
                }else{
                    dos.Total_No_of_Dealers_Distributor__c                 = 0; 
                }                
                if(mapOFCatParamMTDTargetAll.get(cat) != null){
                    dos.Total_MTD_Target__c                                 = mapOFCatParamMTDTargetAll.get(cat);
                }else{
                    dos.Total_MTD_Target__c                                 = 0;
                }                                           
                if(mapOFCatParamActualAll.get(cat) != null){
                    dos.Total_Actual_MTD__c                                 = mapOFCatParamActualAll.get(cat);
                }else{
                    dos.Total_Actual_MTD__c                                 = 0;
                }
                dos.DBExternalId__c                                         = cat+ zoneList[0] + currentMonth + currentYear; 
                if(dos.Total_MTD_Target__c > 0){
                     decimal aci = ((dos.Total_Actual_MTD__c)/dos.Total_MTD_Target__c)*100;
                     if(aci < =100){
                        dos.Final_Score__c = ( aci * mapOFCatCodeAndParam.get(cat).Weightage__c)/100;
                     }else{
                        dos.Final_Score__c = (100 * mapOFCatCodeAndParam.get(cat).Weightage__c)/100;
                     } 
                }else{
                     dos.Final_Score__c = 0;
                }
                dosListInsert.add(dos);
             }
             try{
                    database.upsert (dosListInsert,Dashboard_Output_Score__c.Fields.DBExternalId__c,false) ;
              }catch(Exception e){
                  System.debug(e.getMessage());
              }
           }
              
    }
    global void finish(Database.BatchableContext BC)
    {         
     // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: Dashboard_BatchForDashboardOutputZO Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "Dashboard_BatchForDashboardOutputZO processed" ' + a.TotalJobItems + '/n'+' batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n' );  
              if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }
    }
}