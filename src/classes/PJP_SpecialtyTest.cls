@isTest
public class PJP_SpecialtyTest {
    static testmethod void planSpecialty() {
        Test.startTest();
        User rmUser = CEAT_InitializeTestData.createUser('gulati', 'sandeep', 'sandeep.gulati@ceat.dev', 'sandeep.gulati@ceat.dev', 'Emp2', 'OE', null, 'SRM_Specialty');
        insert rmUser;
        User tlUser = CEAT_InitializeTestData.createUser('Ram', 'Ronak', 'ram.ronak@ceat.dev', 'ram.ronak@ceat.dev', 'Emp1', 'OE', null, 'RM_Specialty');
        tlUser.ManagerId = rmUser.Id;
        insert tlUser;
        
        Territory2Model tModel = CEAT_InitializeTestData.createTerritoryModel('Ceat', 'Ceat');
        insert tModel;
        
        //List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');//[Select Id, DeveloperName from Territory2Type];
        /*
        Territory2 terrZone = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'ZC01' Or DeveloperName = 'ZC01')];//CEAT_InitializeTestData.createTerritory('ZC02', 'ZC02', listTerritoryType[0].Id, tModel.Id, null);        
        Territory2 terrRegion = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'BHP' Or DeveloperName = 'BHP') And ParentTerritory2Id = :terrZone.Id];//CEAT_InitializeTestData.createTerritory('Mum', 'Mum', listTerritoryType[0].Id, tModel.Id, terrZone.Id);        
        Territory2 terr = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'B0144' Or DeveloperName = 'B0144') And ParentTerritory2Id = :terrRegion.Id];//CEAT_InitializeTestData.createTerritory('B001', 'B001', listTerritoryType[0].Id, tModel.Id, terrRegion.Id);        
        */
        Territory2Model terrModel = [Select Id from Territory2Model Limit 1];
        Id terrTypeTerr;
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');        
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            
        }
        
        Territory2 terrZone = CEAT_InitializeTestData.createTerritory('ZS05', 'ZS05' , terrTypeTerr, terrModel.Id, null);
        insert terrZone;
        Territory2 terrRegion = CEAT_InitializeTestData.createTerritory('R5', 'R5' , terrTypeTerr, terrModel.Id, terrZone.Id);
        insert terrRegion;
        UserTerritory2Association utAss = CEAT_InitializeTestData.createUserTerrAsso(tlUser.Id, terrZone.Id, 'RM - Specialty');
        insert utAss;
                
        System.runAs(tlUser) {
            RecordType rtOE = CEAT_InitializeTestData.createRecordType('Specialty', 'Account');
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', 'C000000001');
            acc1.Sales_Office_Text__c = 'ZS05';
            acc1.Sales_Group_Text__c = 'R5';
            acc1.Type = 'Specialty Distributor';
            acc1.Active__c = true;
            insert acc1;
            
            Holiday__c h1 = CEAT_InitializeTestData.createHoliday('National', 'National', Date.today(), null, null, null, null, null);
            Holiday__c h2 = CEAT_InitializeTestData.createHoliday('Regional', 'Regional', Date.today(), 'ZC01', 'BHP', null, null, null);
            Holiday__c h3 = CEAT_InitializeTestData.createHoliday('Planned Leaves', 'Planned Leaves', null, null, null, Date.today(), Date.today().addDays(2), 'Emp1');
            List<Holiday__c> hList = new List<Holiday__c>();
            hList.add(h1);
            hList.add(h2);
            hList.add(h3);
            insert hList;
            
            PJP_Norms_Main__c pnm = CEAT_InitializeTestData.createPjpNorm(true, true, true, true, 3, 2, 3, null, 10, 'RM', 'Specialty');
            PJP_Segment_Norm__c psn = CEAT_InitializeTestData.createSegmentNorm(pnm.Id, 'Specialty Distributor', 4);
            //PJP_Daily_Norms__c pdn = CEAT_InitializeTestData.createDailyNorm(pnm.Id, 'Dealer', 6);
            
            Integer mm = Date.today().month();
            Integer yy = Date.today().year();
            String mon = PJP_SpecialtyClass.calculateMonth(mm);
            String quar = null;
            if(mon == 'April' || mon == 'May' || mon == 'June') 
                quar = 'Q1';
            else if(mon == 'July' || mon == 'August' || mon == 'September') 
                quar = 'Q2';
            else if(mon == 'October' || mon == 'November' || mon == 'December') 
                quar = 'Q3';
            else if(mon == 'January' || mon == 'February' || mon == 'March') 
                quar = 'Q4';
            
            ACW__c currentAcw = CEAT_InitializeTestData.createACW(Date.today(), Date.today(), mon, 'PJP Specialty');
            
            PJP_SpecialtyClass mvp = new PJP_SpecialtyClass();
            PJP_SpecialtyClass.getQuarterMonth(null, null, null);
            PJP_SpecialtyClass.getWriteAccess(null, null, null);
            PJP_SpecialtyClass.getQuarterMonth(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            PJP_SpecialtyClass.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            PJP_SpecialtyClass.getRelatedAccounts(yy, mm, mon, quar, String.valueOf(UserInfo.getUserId()));
            
            Visits_Plan__c vp1 = new Visits_Plan__c();
            vp1.Day__c = '1';
            vp1.Type_of_Day__c = 'OE';
            vp1.Id = null;
            Visits_Plan__c vp2 = new Visits_Plan__c();
            vp2.Day__c = '2';
            vp2.Type_of_Day__c = 'MOR';
            vp2.Id = null;
            Visits_Plan__c vp3 = new Visits_Plan__c();
            vp3.Day__c = '3';
            vp3.Type_of_Day__c = 'Company Activity';
            vp3.Id = null;
            Visits_Plan__c vp4 = new Visits_Plan__c();
            vp4.Day__c = '4';
            vp4.Type_of_Day__c = 'Workshop';
            vp4.Id = null;
            Visits_Plan__c vp5 = new Visits_Plan__c();
            vp5.Dealer__c = acc1.Id;
            vp5.Day__c = '5';
            vp5.Type_of_Day__c = 'Visit Day';
            vp5.Id = null;//String.valueOf(acc1.Id);
            
            List<Visits_Plan__c> vpList = new List<Visits_Plan__c>();
            vpList.add(vp1);
            vpList.add(vp2);
            vpList.add(vp3);
            vpList.add(vp4);
            vpList.add(vp5);               
                                    
            PJP_SpecialtyClass.saveVisitPlans(vpList, yy, mm, mon, 'Submit', 'Comments');
            
            PJP_SpecialtyClass.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));            
            
            ApexPages.currentPage().getParameters().put('month', mon);
            ApexPages.currentPage().getParameters().put('year', String.valueOf(yy));
            ApexPages.currentPage().getParameters().put('owner', tlUser.Id);
            mvp = new PJP_SpecialtyClass();
            PJP_SpecialtyClass.getRelatedAccounts(yy, mm, mon, quar, String.valueOf(UserInfo.getUserId()));
            vpList = [Select Id, Dealer__c, Day__c, Type_of_Day__c from Visits_Plan__c];//new List<Visits_Plan__c>();
            PJP_SpecialtyClass.saveVisitPlans(vpList, yy, mm, mon, 'Submit', 'New Comments');
            
            for(Integer i=0; i<=12; i++) {
                String m = PJP_SpecialtyClass.calculateMonth(i);            
            }
            Map<String, String> quarMap = new Map<String, String>();
            quarMap.put('Q1', 'April');
            quarMap.put('Q2', 'July');
            quarMap.put('Q3', 'October');
            quarMap.put('Q4', 'January');
            
            for(String s : quarMap.keySet()) {
                if(s != quar) {
                    Map<Id, Integer> quarVisits = PJP_SpecialtyClass.getQuarterVisits(yy, s, String.valueOf(UserInfo.getUserId()));        
                    //Map<String, Integer> quarAct = PJP_SpecialtyClass.getQuarterActivities(yy, s, String.valueOf(UserInfo.getUserId()));        
                }                
            }            
        }    
        Test.stopTest();
    }
}