global class DB_BatchForDashboardStaging implements Database.Batchable<sObject> {    
    /* ================================================
    @Name:  SP_BatchForAutoPopulateReplacement
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will Auto populate planned for RM and TL if both didn't plan and 
    it will Auto Polulate TL plan if RM already Planned and                                                                                           
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     09/07/2015      INITIAL DEVELOPMENT                                 
   
    @=======================================================  */
      List<Dashboard_Staging__c> updateDashboardSt = new list<Dashboard_Staging__c>();
      List<Dashboard_Staging__c> myList {get;set;} 
      global DB_BatchForDashboardStaging(list<Dashboard_Staging__c> newList){
        this.myList = newList;
        system.debug(myList+'%%%%%%%%%');
      } 
      global List<Dashboard_Staging__c> start(Database.BatchableContext BC){
        String query;
        system.debug(myList+'$$$$$$$');
        return myList;
      }
       
      global void execute(Database.BatchableContext BC,List<sObject> scope){
            for(sObject temp : scope){
                Dashboard_Staging__c dbs                                =(Dashboard_Staging__c)temp;
                system.debug(dbs+'#########');
                dbs.Month__c = '7';
                updateDashboardSt.add(dbs);
            }
            update updateDashboardSt ;
      }
      
      global void finish(Database.BatchableContext BC)
      {    
      
      }   
}