public with sharing class TH_VisitPlanTriggerHandler {

    /*
	 * Auther :- Vivek Deepak	
	 * Purpose :- trigger handler for visit plan object
	 *
	 *
	 *
	 */
	 
	 //-- SINGLETON PATTERN
      private static TH_VisitPlanTriggerHandler instance;
      public static TH_VisitPlanTriggerHandler getInstance() {
          if (instance == null) instance = new TH_VisitPlanTriggerHandler();
          return instance;
      }
      
     // -- ATTRIBUTES    
	
	 //-- CONSTRUCTOR 
	
	 //-- METHODS 
	 
	
	//-- Before Insert
	
	  
	 public void onBeforeInsert(final List<Visits_Plan__c> newObjects){
	 	Schema.DescribeSObjectResult cfrSchema 					= Schema.SObjectType.Visits_Plan__c;
        Map<Id,Schema.RecordTypeInfo> visitPlanRecordType 		= cfrSchema.getRecordTypeInfosById();  
         
        List<Visits_Plan__c> dse_visits = new LIST<Visits_Plan__c>(); 
		
		Decimal distance = Decimal.valueOf(Label.PJP_Deviation);
		
         // Take DSE Beat ids
         for(Visits_Plan__c vp : newObjects){
             if(visitPlanRecordType.get(vp.RecordTypeId).getName() == 'DSE-Beat'){
                 dse_visits.add(vp);
             }else if(visitPlanRecordType.get(vp.RecordTypeId).getName() == 'Visit' && vp.Check_In__c){
             	if(vp.Geolocation__Latitude__s == 0.0 || vp.Geolocation__Latitude__s == null || vp.Geolocation__Longitude__s == 0.0 || vp.Geolocation__Longitude__s == null){
             		vp.Status__c = 'Unsuccessful';
             		vp.Geolocation__Latitude__s = null;
             		vp.Geolocation__Longitude__s = null;
             	}else if(vp.Deviation__c <= distance && vp.Check_In__c){
             		vp.Status__c = 'Successful';
             	}else if(vp.Deviation__c > distance && vp.Check_In__c){
             		vp.Status__c = 'Unsuccessful';
             	}
             	
             }
         } 
         // Add dealer to visit
         updateVisits(dse_visits);
        
	 	
	 }
	 
	
	 //-- Before Update
	 
	  
	 public void onBeforeUpdate(final List<Visits_Plan__c> newObjects,final List<Visits_Plan__c> oldObjects,final Map<Id, Visits_Plan__c> newObjectsMap,final Map<Id, Visits_Plan__c> oldObjectsMap){
	 	Schema.DescribeSObjectResult cfrSchema 					= Schema.SObjectType.Visits_Plan__c;
        Map<Id,Schema.RecordTypeInfo> visitPlanRecordType 		= cfrSchema.getRecordTypeInfosById();  
         
        List<Visits_Plan__c> dse_visits = new LIST<Visits_Plan__c>(); 
		Decimal distance = Decimal.valueOf(Label.PJP_Deviation);
         // Take DSE Beat ids
         for(Visits_Plan__c vp : newObjects){
             if(visitPlanRecordType.get(vp.RecordTypeId).getName() == 'DSE-Beat'){
                 dse_visits.add(vp);
             }else if(visitPlanRecordType.get(vp.RecordTypeId).getName() == 'Visit' && vp.Check_In__c){
             	if(vp.Geolocation__Latitude__s == 0.0 || vp.Geolocation__Latitude__s == null || vp.Geolocation__Longitude__s == 0.0 || vp.Geolocation__Longitude__s == null){
             		vp.Status__c = 'Unsuccessful';
             		vp.Geolocation__Latitude__s = null;
             		vp.Geolocation__Longitude__s = null;
             	}else if(vp.Deviation__c <= distance && vp.Check_In__c){
             		vp.Status__c = 'Successful';
             	}else if(vp.Deviation__c > distance && vp.Check_In__c){
             		vp.Status__c = 'Unsuccessful';
             	}
             	
             }
         }
         // Add dealer to visit
	 	 updateVisits(dse_visits);
	 }
    
    /*Set dealer for DSE Visits*/
    public void updateVisits(final List<Visits_Plan__c> newObjects){
         
         Set<Id> beatIds = new Set<Id>();
         
         Map<Id,Id> accountWithDSE = new Map<Id,Id>();
         
         // Take DSE Beat ids
         for(Visits_Plan__c vp : newObjects){
         	   beatIds.add(vp.DSE_Beat__c);
         }
         
         for(DSE_Beat__c dse : [SELECT Id,Distributor__c FROM DSE_Beat__c WHERE Id IN :beatIds AND Distributor__c != null]){
             accountWithDSE.put(dse.Id, dse.Distributor__c);
         }
         
         // Add Account to Visits
         for(Visits_Plan__c vp : newObjects){
             if(accountWithDSE.containsKey(vp.DSE_Beat__c)){
                 vp.Dealer__c = accountWithDSE.get(vp.DSE_Beat__c);
             }
         }
    }
    
}