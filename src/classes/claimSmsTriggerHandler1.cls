public with sharing class claimSmsTriggerHandler1 {

  public static void sendEmailToTlAndTld(List<Claim__c> newClaimList) {
    String sfdcbaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    System.debug('==#Base URL '+sfdcbaseUrl );


    Map<Id, String> userIdToEmailMap = new Map<Id, String>();
  for(User tmpUser : [SELECT Id, Email FROM User]) {
      if(!userIdToEmailMap.containsKey(tmpUser.Id)) {
          userIdToEmailMap.put(tmpUser.Id, tmpUser.email);
          system.debug('$$$$$'+userIdToEmailMap);
      }
  }
  Map<String, Id> terrCodeToUserIdMap = new Map<String, Id>();
  for(Territory2 tmpTerr : [Select Name, Id, (Select UserId, Territory2Id, IsActive, RoleInTerritory2 From UserTerritory2Associations where RoleInTerritory2 Like '%TL_%' or RoleInTerritory2 Like '%TLD%' ) From Territory2]) {
      System.debug('&&&&###'+tmpTerr);
    for(UserTerritory2Association tmpAssoc : tmpTerr.UserTerritory2Associations) {
        System.debug('%$$$#$'+tmpAssoc);
        if(!terrCodeToUserIdMap.containsKey(tmpTerr.Name)) {
          terrCodeToUserIdMap.put(tmpTerr.Name, tmpAssoc.UserId);
          system.debug('values@@'+terrCodeToUserIdMap);
        }
    }
  }

  Set<ID> caseIds = new Set<ID>();
  for(Claim__c c : newClaimList){
    caseIds.add(c.Complaint__c);    
  }

  List<Case> caseOwnerList = new List<Case>();
  caseOwnerList = [Select id , ownerId, owner.email from Case where ID IN: caseIds];
  Map<ID,String> ownerEmail = new Map<ID, String>();
  for(Case cse :caseOwnerList){
    ownerEmail.put(cse.ID,cse.owner.email);
  }
   for(Claim__c c :newClaimList){
    List<String> toAddList = new List<String>();
     if(c.Complaint__c != null){
        toAddList.add(ownerEmail.get(c.Complaint__c));
     }
    system.debug('DestCodeGa '+c);
    System.debug('===##3'+c.System_district_Code__c);
    System.debug('==##1 '+terrCodeToUserIdMap.get(c.System_district_Code__c));
    System.debug('==##2 '+userIdToEmailMap.get(terrCodeToUserIdMap.get(c.System_district_Code__c)));
    if(toAddList.size() == 0){
        toAddList.add(userIdToEmailMap.get(terrCodeToUserIdMap.get(c.System_district_Code__c)));
    }
    System.debug('sendToValue'+toAddList);
     
           Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage(); 
                   
                   String messageBody = sfdcbaseUrl+'/'+c.id;
                   system.debug('!@!@####'+messageBody);
                   mailHandler.setHtmlBody(messageBody);
                   mailHandler.setToAddresses(toAddList);
                   system.debug('emailvalues'+mailHandler);
                   Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailHandler});

                   //emails.add(mailHandler);
                        
                   //Messaging.sendEmail(new Messaging.SingleEmailMessage{mailHandler});
    
  }  
        
    //if(emails.size()>0){
    // Messaging.sendEmail(emails); 
    //}
    
    

  }
        
    }