global class CPORT_SalesRegisterDetails {
    /*
     * Auther  :- Vivek Deepak  
     * Purpose :- Fetch details of Sales register 
     *            from SAP 
     *
     *
     */
     
     //-- ATTRIBUTES
     
     public static final String CUST_ID_LENGTH  = Label.CPORT_LengthOfCustomerId;
     
     //-- CONTRUCTOR
     
     //-- METHODS
     
     /*
     This method will return list of Sales register values
     */
     WebService static List<SalesRegisterMapping> getAllSalesRegisterDetails(String cusNum, String fDate, String tDate){
        
        List<SalesRegisterMapping> salesReg     = new List<SalesRegisterMapping>();
        System.debug('Swayam123    '+cusNum+'   '+fDate+'   '+tDate);
        try{

        String customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
        
        SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        // SapSalesRegisterSoap tst                     = new SapSalesRegisterSoap();
        // SapSalesRegisterSoap.ZWS_SALES_REGISTER zws  = new SapSalesRegisterSoap.ZWS_SALES_REGISTER();
        SapSalesRegisterSoap scd                    = new SapSalesRegisterSoap();
        SapSalesRegisterSoap.ZWS_SALES_REGISTER_EX zws  = new SapSalesRegisterSoap.ZWS_SALES_REGISTER_EX();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;
        // SapSalesRegisterSoap.TableOfZsd05a tom       = new SapSalesRegisterSoap.TableOfZsd05a();
        // SapSalesRegisterSoap.TableOfZtvfkt tes       = new SapSalesRegisterSoap.TableOfZtvfkt();
  //       SapSalesRegisterSoap.ZbapiSalesRegisterResponse_element elem = new SapSalesRegisterSoap.ZbapiSalesRegisterResponse_element();      
                
  //       List<SapSalesRegisterSoap.Zsd05a> m          = new List<SapSalesRegisterSoap.Zsd05a>();        
        
  //       tom.item = m;
        
  //       SapSalesRegisterSoap.TableOfZsd05a e = new SapSalesRegisterSoap.TableOfZsd05a();
        
  //       elem = zws.ZbapiSalesRegister(fDate,tes,tom, customerId, tDate);
        SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EXResponse_element  tab = new SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EXResponse_element ();
        SapSalesRegisterSoap.TABLE_OF_ZTVFKT tes = new SapSalesRegisterSoap.TABLE_OF_ZTVFKT();
        SapSalesRegisterSoap.TABLE_OF_ZSD05A1 tom = new SapSalesRegisterSoap.TABLE_OF_ZSD05A1();
        tab = zws.ZSFDC_SALES_REGISTER_EX(fDate,tes,tom,customerId,tDate);
        
        Map<String,String> invoiceDes   = new Map<String,String>();
        //Map<String,Decimal> invoiceDesV = new Map<String,Decimal>();
        
        if(tab.IT_OUTPUT != null){ 
            tes = tab.IT_FINAL;
            tom = tab.IT_OUTPUT;  
            //system.debug('INTY ' + tes);
            if(tes.item != null){
                for(SapSalesRegisterSoap.ZTVFKT z1 : tes.item) {
                    invoiceDes.put(z1.Fkart,z1.Vtext);
                    //invoiceDesV.put(z1.Fkart,0.0);
                }
            }
                     
            if(tom.item != null){
                for(SapSalesRegisterSoap.ZSD05A1 z : tom.item) {
                    String invDesc = (invoiceDes.containsKey(z.Fkart) ? invoiceDes.get(z.Fkart) : z.Fkart);
                    //Decimal invDescV = (invoiceDesV.containsKey(z.Fkart) ? invoiceDesV.get(z.Fkart) + (z.Post != '' || z.Post != null ? Decimal.valueOf(z.Post):0.0)  : 0.0);
                    //invoiceDesV.put(z.Fkart,invDescV);
                    
                    salesReg.add(new SalesRegisterMapping(z.Arktx,z.Bzirk,z.Candoc,z.Ceat1,z.Erdat,z.Erzet,invDesc,z.Fkdat,z.Fkimg,z.WAERK,z.Kunag,z.Kvgr1,z.Mandt,z.Matkl,z.Matnr,z.Mwsbp,z.Nbpprice,z.Netwr,z.Ntgew,z.Posnr,z.Post,z.Pre,z.Town,z.Vbeln,z.Vkbur,z.Vkburtext,z.Vkgrp,z.Vkgrptext,z.Vbeln,z.Werks,z.Wgbez,z.Zterm));
                }
            }
            
            
        }
            //system.debug('Swayam SalesReg********************'+salesReg[0]);
            return salesReg;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
     }
     
     

     //-- WRAPPER CLASS
     global class SalesRegisterMapping{
     
        public String material_Description  {get;set;}
        public String bRick                 {get;set;}
        public String candoc                {get;set;}
        public String monthYear             {get;set;}
        public String erDate                {get;set;}
        public String erTime                {get;set;}
        public String invoiceType           {get;set;}
        public String fkDate                {get;set;}
        public String fkImg                 {get;set;}
        public String kdgrpAuft             {get;set;}
        public String knumv                 {get;set;} 
        public String kunag                 {get;set;} // Customer ID
        public String kvgr1                 {get;set;}
        public String mandt                 {get;set;}
        public String matkl                 {get;set;}
        public String material_Num          {get;set;}
        public Decimal taxValue             {get;set;}
        public String customerName          {get;set;} // Customer Name
        public Decimal nbpprice             {get;set;}
        public Decimal netwr                {get;set;}    
        public Decimal ntgew                {get;set;}
        public String posnr                 {get;set;}
        public Decimal invoiceValue         {get;set;}
        public Decimal preTax               {get;set;}
        //public String text1                   {get;set;}
        public String town                  {get;set;}
        public String vbeln                 {get;set;}
        public String invoiceNumber         {get;set;}
        public String vkbur                 {get;set;}
        public String vkBurText             {get;set;}
        public String vkgrp                 {get;set;}
        public String vkgrptext             {get;set;}
        public String vtweg                 {get;set;}
        public String wreks                 {get;set;}
        public String categoryT             {get;set;}
        public String zterm                 {get;set;}  
        
        /*
        Not passing all parameters because
        An Apex method can have on 32 parameters
        FUN FACT
        */
        public SalesRegisterMapping(String material_Description,String bRick,String candoc,String monthYear,String erDate,String erTime,String invoiceType,String fkDate,String fkImg,String kdgrpAuft,String knumv,String kvgr1,String mandt,String matkl,String material_Num,String taxValue,String nbpprice,String netwr,String ntgew,String posnr,String invoiceValue,String preTax,String town,String invoiceNumber,String vkbur,String vkBurText,String vkgrp,String vkgrptext,String vbeln,String wreks,String categoryT,String zterm){

            this.material_Description   = material_Description;
            this.bRick                  = bRick;
            this.candoc                 = candoc;
            this.monthYear              = monthYear;
            this.erDate                 = erDate;
            this.erTime                 = erTime;
            this.invoiceType            = invoiceType;
            this.fkDate                 = fkDate;
            this.fkImg                  = fkImg;
            this.kdgrpAuft              = kdgrpAuft; // Currency 
            this.knumv                  = knumv;
            //this.kunag                    = kunag;
            this.kvgr1                  = kvgr1;
            this.mandt                  = mandt;
            this.matkl                  = matkl;
            this.material_Num           = material_Num;
            this.taxValue               = (taxValue != '' || taxValue != null ? Decimal.valueOf(taxValue):0.0);
            //this.customerName         = customerName;
            this.nbpprice               = (nbpprice != '' || nbpprice != null ? Decimal.valueOf(nbpprice):0.0);
            this.netwr                  = (netwr != '' || netwr != null ? Decimal.valueOf(netwr):0.0);
            this.ntgew                  = (ntgew != '' || ntgew != null ? Decimal.valueOf(ntgew):0.0);
            this.posnr                  = posnr;
            this.invoiceValue           = (invoiceValue != '' || invoiceValue != null ? Decimal.valueOf(invoiceValue):0.0);
            this.preTax                 = (preTax != '' || preTax != null ? Decimal.valueOf(preTax):0.0);
            //this.text1                    = text1;
            this.town                   = town;
            this.vbeln                  = vbeln;
            this.invoiceNumber          = invoiceNumber;
            this.vkbur                  = vkbur;
            this.vkBurText              = vkBurText;
            this.vkgrp                  = vkgrp;
            this.vkgrptext              = vkgrptext;
            //this.vtweg                    = vtweg;
            this.wreks                  = wreks;
            this.categoryT              = categoryT;
            this.zterm                  = zterm;            
        }    
    }
}