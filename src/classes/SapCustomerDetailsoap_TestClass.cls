@isTest
private with sharing class SapCustomerDetailsoap_TestClass {
    
    private static testMethod void coverTypes()
    {
        new SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetails_element();
        new SapCustomerDetailsSoap_v2.Bapireturn();
        new SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetailsResponse_element();
        new SapCustomerDetailsSoap_v2.ZbapiCustStr1();
        new SapCustomerDetailsSoap_v2.ZbapiCustStr();
        new SapCustomerDetailsSoap_v2.TableOfZbapiCustStr();
        new SapCustomerDetailsSoap_v2.TableOfZbapiCustStr1();
        new SapCustomerDetailsSoap_v2.TableOfBapireturn();
    }
    
    
    private class WebServiceMockImpl implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {            
            SapCustomerDetailsSoap_v2.ZbapiCustStr1[] listCustStr1 = new List<SapCustomerDetailsSoap_v2.ZbapiCustStr1>();                
            SapCustomerDetailsSoap_v2.ZbapiCustStr1 custStr1 = new SapCustomerDetailsSoap_v2.ZbapiCustStr1();
            custStr1.Anred = '0056787896'; 
            custStr1.Name1 = '0056787896'; 
            custStr1.Kunnr = '0056787896'; 
            custStr1.Telf1 = '0056787896'; 
            custStr1.FaxNumber = '0056787896'; 
            custStr1.SmtpAddr = 'a@b.com'; 
            custStr1.StrSuppl1 = '0056787896'; 
            custStr1.StrSuppl2 = '0056787896'; 
            custStr1.Ort01 = '0056787896'; 
            custStr1.Regio = '0056787896'; 
            custStr1.Pstlz = '0056787896'; 
            custStr1.Land1 = '0056787896'; 
            custStr1.Gbdat = '0056787896'; 
            custStr1.Waers = '0056787896'; 
            custStr1.Kdgrp = '0056787896'; 
            custStr1.Katr6 = '0056787896'; 
            custStr1.Katr7 = '0056787896'; 
            custStr1.J1ipanno = '0056787896'; 
            custStr1.J1icstno = '0056787896'; 
            custStr1.Town = '0056787896'; 
            custStr1.Dist = '0056787896'; 
            custStr1.Bezei = '0056787896';
            listCustStr1.add(custStr1);
            
            SapCustomerDetailsSoap_v2.ZbapiCustStr[] listCustStr = new List<SapCustomerDetailsSoap_v2.ZbapiCustStr>();                
            SapCustomerDetailsSoap_v2.ZbapiCustStr custStr = new SapCustomerDetailsSoap_v2.ZbapiCustStr();
            custStr.Kunnr = '0056787896'; 
            custStr.Name1 = '0056787896'; 
            custStr.Telf1 = '0056787896'; 
            custStr.Ort01 = '0056787896'; 
            custStr.Regio = '0056787896'; 
            custStr.Pstlz = '0056787896'; 
            custStr.Land1 = '0056787896'; 
            custStr.Parnr = '0056787896'; 
            custStr.SmtpAddr = 'a@b.com'; 
            custStr.FaxNumber = '0056787896'; 
            custStr.TelNumber = '0056787896'; 
            custStr.Parvw = '0056787896'; 
            custStr.StrSuppl1 = '0056787896'; 
            custStr.StrSuppl2 = '0056787896'; 
            custStr.Bzirk = '0056787896'; 
            custStr.Waers = '0056787896'; 
            custStr.Kdgrp = '0056787896'; 
            custStr.Vkbur = '0056787896'; 
            custStr.Vkgrp = '0056787896'; 
            custStr.Vtweg = '0056787896'; 
            custStr.Spart = '0056787896'; 
            custStr.Kvgr1 = '0056787896'; 
            custStr.Kvgr2 = '0056787896'; 
            custStr.Kvgr3 = '0056787896'; 
            custStr.Loevm = '0056787896';
            listCustStr.add(custStr);
            
            SapCustomerDetailsSoap_v2.ZbapiCustStr[] cstr = new List<SapCustomerDetailsSoap_v2.ZbapiCustStr>();
            SapCustomerDetailsSoap_v2.ZbapiCustStr1[] cstr1 = new List<SapCustomerDetailsSoap_v2.ZbapiCustStr1>();
            SapCustomerDetailsSoap_v2.TableOfBapireturn tbp         = new SapCustomerDetailsSoap_v2.TableOfBapireturn();        
            SapCustomerDetailsSoap_v2.TableOfZbapiCustStr tcstr     = new SapCustomerDetailsSoap_v2.TableOfZbapiCustStr();
            tcstr.item = listCustStr;
            SapCustomerDetailsSoap_v2.TableOfZbapiCustStr1 tcstr1   = new SapCustomerDetailsSoap_v2.TableOfZbapiCustStr1();     
            tcstr1.item = listCustStr1;
            SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetailsResponse_element  elem = new SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetailsResponse_element();
            elem.TCust =tcstr;
            elem.TCust1 =tcstr1;
            //SapCustomerDetailsSoap_v2.ZWS_GET_CUSTOMER_DETAILS webServiceClass = new SapCustomerDetailsSoap_v2.ZWS_GET_CUSTOMER_DETAILS();
            //webServiceClass.ZbapiGetCustomerDetails(null,tcstr,tcstr1);
            
            if(request instanceof SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetails_element)
                response.put('response_x', elem);
            return;
        }
    }
    
    @IsTest
    private static void coverGeneratedCodeCRUDOperations()
    {   
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        //SapCustomerDetailsSoap_v2 sapClass = new SapCustomerDetailsSoap_v2();
        
        //SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetailsResponse_element zws = new SapCustomerDetailsSoap_v2.ZbapiGetCustomerDetailsResponse_element();
        // Invoke operations         
        //SapCustomerDetailsSoap_v2.ZWS_GET_CUSTOMER_DETAILS webServiceClass = new SapCustomerDetailsSoap_v2.ZWS_GET_CUSTOMER_DETAILS();
        //webServiceClass.ZbapiGetCustomerDetails(null,null,null);
        SAPLogin__c sapLogin = CEAT_InitializeTestData.sapLoginCustomSetting('SAP Login', 'username', 'password');
        insert sapLogin;
        Test.startTest();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        //STG_CustomerStagingClass.getAllCustomerInformation();
        STG_SchedulerCustomerStaging bth = new STG_SchedulerCustomerStaging();
        String sch = '0 0 18 * * ?'; 
        system.schedule('Test Territory Check', sch, bth);
        Test.stopTest();     
    }
}