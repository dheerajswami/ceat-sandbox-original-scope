//Generated by wsdl2apex

public class Sap_SendClaimDetailsToSap {
    public class TableOfZsdcl0 {
        public Sap_SendClaimDetailsToSap.Zsdcl0[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZsdGetCl0DataResponse_element {
        public Sap_SendClaimDetailsToSap.TableOfZsdcl0 ItInput;
        public String Return_x;
        private String[] ItInput_type_info = new String[]{'ItInput','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Return_x_type_info = new String[]{'Return','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ItInput','Return_x'};
    }
    public class ZsdGetCl0Data_element {
        public Sap_SendClaimDetailsToSap.TableOfZsdcl0 ItInput;
        private String[] ItInput_type_info = new String[]{'ItInput','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ItInput'};
    }
    public class Zsdcl0 {
        public String Mandt;
        public String Docket;
        public String Kunag;
        public String Name;
        public String Name1;
        public String Qmart;
        public String Street1;
        public String Street2;
        public String Telephone;
        public String SmtpAddr;
        public String Matsize;
        public String Tyrep;
        public String Matnr;
        public String Arktx;
        public String Fitmt;
        public String Vmake;
        public String Ymany;
        public String Vkm;
        public String Chassis;
        public String Fegrp;
        public String Wear;
        public String Fecod;
        public String Inspby;
        public String Insdate;
        public String Actnsd;
        public String Kbetr;
        public String RelatedNum;
        public String State;
        public String District;
        public String Town;
        public String Mvgr2;
        public String Rimsize;
        public String PlyRating;
        public String TyreType;
        public String Matkl;
        public String Wgbez;
        public String SerialNum;
        private String[] Mandt_type_info = new String[]{'Mandt','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Docket_type_info = new String[]{'Docket','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Kunag_type_info = new String[]{'Kunag','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Name_type_info = new String[]{'Name','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Name1_type_info = new String[]{'Name1','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Qmart_type_info = new String[]{'Qmart','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Street1_type_info = new String[]{'Street1','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Street2_type_info = new String[]{'Street2','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Telephone_type_info = new String[]{'Telephone','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] SmtpAddr_type_info = new String[]{'SmtpAddr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Matsize_type_info = new String[]{'Matsize','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Tyrep_type_info = new String[]{'Tyrep','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Matnr_type_info = new String[]{'Matnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Arktx_type_info = new String[]{'Arktx','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Fitmt_type_info = new String[]{'Fitmt','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Vmake_type_info = new String[]{'Vmake','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Ymany_type_info = new String[]{'Ymany','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Vkm_type_info = new String[]{'Vkm','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Chassis_type_info = new String[]{'Chassis','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Fegrp_type_info = new String[]{'Fegrp','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Wear_type_info = new String[]{'Wear','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Fecod_type_info = new String[]{'Fecod','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Inspby_type_info = new String[]{'Inspby','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Insdate_type_info = new String[]{'Insdate','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Actnsd_type_info = new String[]{'Actnsd','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Kbetr_type_info = new String[]{'Kbetr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] RelatedNum_type_info = new String[]{'RelatedNum','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] State_type_info = new String[]{'State','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] District_type_info = new String[]{'District','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Town_type_info = new String[]{'Town','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Mvgr2_type_info = new String[]{'Mvgr2','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Rimsize_type_info = new String[]{'Rimsize','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] PlyRating_type_info = new String[]{'PlyRating','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] TyreType_type_info = new String[]{'TyreType','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Matkl_type_info = new String[]{'Matkl','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Wgbez_type_info = new String[]{'Wgbez','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] SerialNum_type_info = new String[]{'SerialNum','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Mandt','Docket','Kunag','Name','Name1','Qmart','Street1','Street2','Telephone','SmtpAddr','Matsize','Tyrep','Matnr','Arktx','Fitmt','Vmake','Ymany','Vkm','Chassis','Fegrp','Wear','Fecod','Inspby','Insdate','Actnsd','Kbetr','RelatedNum','State','District','Town','Mvgr2','Rimsize','PlyRating','TyreType','Matkl','Wgbez','SerialNum'};
    }
    public class ZWS_GET_CL0_DATA {
        public String endpoint_x = 'http://CTECQHAP.ceat.in:8000/sap/bc/srt/rfc/sap/zws_get_cl0_data/300/zws_get_cl0_data/zws_get_cl0_data';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'Sap_SendClaimDetailsToSap', 'urn:sap-com:document:sap:rfc:functions', 'Sap_SendClaimDetailsToSapFunctions'};
        public Sap_SendClaimDetailsToSap.ZsdGetCl0DataResponse_element ZsdGetCl0Data(Sap_SendClaimDetailsToSap.TableOfZsdcl0 ItInput) {
            Sap_SendClaimDetailsToSap.ZsdGetCl0Data_element request_x = new Sap_SendClaimDetailsToSap.ZsdGetCl0Data_element();
            request_x.ItInput = ItInput;
            Sap_SendClaimDetailsToSap.ZsdGetCl0DataResponse_element response_x;
            Map<String, Sap_SendClaimDetailsToSap.ZsdGetCl0DataResponse_element> response_map_x = new Map<String, Sap_SendClaimDetailsToSap.ZsdGetCl0DataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:soap:functions:mc-style:ZWS_GET_CL0_DATA:ZsdGetCl0DataRequest',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsdGetCl0Data',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsdGetCl0DataResponse',
              'Sap_SendClaimDetailsToSap.ZsdGetCl0DataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}