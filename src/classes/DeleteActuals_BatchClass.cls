global with sharing class DeleteActuals_BatchClass implements Database.Batchable<sObject>{

    String buType = System.Label.Actual_Replacement;
    
    global DeleteActuals_BatchClass (){
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query='select id,Processed__c,BU__c from Sales_Planing_Staging__c where Processed__c=true AND BU__c=:buType';
        return Database.getQueryLocator(query);   
    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
        list<Sales_Planing_Staging__c> spIdListToDelete= new list<Sales_Planing_Staging__c>();
        if(scope.size()>0){
            //List<Sales_Planing_Staging__c> spList=(List<Sales_Planing_Staging__c>)scope; 
            for(SObject sp : scope){
                Sales_Planing_Staging__c sp1= (Sales_Planing_Staging__c)sp;
                spIdListToDelete.add(sp1);
            }
        }
        if(spIdListToDelete.size()>0){
            delete spIdListToDelete;
        }
    }
    global void finish(Database.BatchableContext BC) {
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: DeleteActuals_BatchClass Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "DeleteActuals_BatchClass" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
    }   
}