public class DashboardStagingTriggerHandler{
    
    //-- SINGLETON PATTERN
    private static DashboardStagingTriggerHandler instance;
    public static DashboardStagingTriggerHandler getInstance() {
        if (instance == null) instance = new DashboardStagingTriggerHandler();
        return instance;
    }
    list<Territory2Type> terriType                                                 = new list<Territory2Type>();
    list<UserTerritory2Association> userAsstemp ;
    list<UserTerritory2Association> userAssList ;
    list<Dashboard_Input_Score__c> listOFDISInsert                                 = new list<Dashboard_Input_Score__c>();
    list<Dashboard_Input_Score__c> listOfDISQuery                                  = new list<Dashboard_Input_Score__c>();
    list<Dashboard_Summary_Score__c> dashboardSummary                              = new list<Dashboard_Summary_Score__c>();
    set<id> terrTypeID                                                             = new set<Id>();
    set<String> keyfactor                                                          = new set<String>();
    list<String> terrSet                                                           = new list<String>();
    set<String> DSSSet                                                             = new set<String>();
    map<String,list<UserTerritory2Association>>  mapOfuserAndAssociation           = new map<String,list<UserTerritory2Association>>();
    map<String,Dashboard_Weightage_Master__c>  mapOfDashboardMaster                = new map<String,Dashboard_Weightage_Master__c>();
    map<String,Decimal> mapForSummaryScore                                         = new map<String,Decimal>();
    map<String,Dashboard_Input_Score__c> mapForSummaryScore1                       = new map<String,Dashboard_Input_Score__c>();
    
    list<Dashboard_Summary_Score__c> listofDSS                                     = new list<Dashboard_Summary_Score__c>();
    Map<String,Dashboard_Summary_Score__c> mapOfTerrAndDSS                         = new Map<String,Dashboard_Summary_Score__c>();
    
    String tldRole             = system.Label.TLD_Role;
    String cstlRole                       = system.Label.CSTL_Role;
    Id TLDRecTypeId = UtilityClass.getRecordTypeId('Dashboard_Input_Score__c','TLD');
    Id CSTLRecTypeId = UtilityClass.getRecordTypeId('Dashboard_Input_Score__c','CSTL');
    public DashboardStagingTriggerHandler(){
        terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
            

        List<UserTerritory2Association> userAssociation = new list<UserTerritory2Association>();
        userAssociation = [SELECT id,UserId,user.Email,user.Username,user.UserRole.Name,Territory2.DeveloperName,RoleInTerritory2,IsActive,Territory2Id from UserTerritory2Association WHERE (RoleInTerritory2= 'TLD' OR RoleInTerritory2= 'CSTL') AND Territory2.ParentTerritory2.ParentTerritory2.ParentTerritory2.ParentTerritory2.Name= 'Replacements'];
        //system.debug(userAssociation.size()+'DDD');
        if(userAssociation.size( ) > 0){
            for(UserTerritory2Association u : userAssociation){
                if(mapOfuserAndAssociation.containsKey(u.user.Username)){
                    userAsstemp = new list<UserTerritory2Association>();
                    userAsstemp = mapOfuserAndAssociation.get(u.user.Username);
                    userAsstemp.add(u);
                    mapOfuserAndAssociation.put(u.user.Username,userAsstemp);
                }else{
                    userAsstemp = new list<UserTerritory2Association>();
                    userAsstemp.add(u);
                    mapOfuserAndAssociation.put(u.user.Username,userAsstemp);
                }
            }
        }
        list<Dashboard_Weightage_Master__c> dashboardWeMaster =  new list<Dashboard_Weightage_Master__c>();
        dashboardWeMaster = [Select id , Testing__c, Name,SYS_Used_For_TLD__c , Role__c,Parameters_Inout__c,Category__c,Weightage__c From Dashboard_Weightage_Master__c WHERE Testing__c = true AND (Role__c = 'TLD' OR Role__c = 'CSTL')];
        if(dashboardWeMaster.size( ) > 0){
            for(Dashboard_Weightage_Master__c dwm : dashboardWeMaster ){
                    mapOfDashboardMaster.put(dwm.SYS_Used_For_TLD__c,dwm);
            }
        }
    }
    //system.debug(mapOfDashboardMaster);
    //system.debug(mapOfuserAndAssociation);
    public void onBeforeInsert(final List<Dashboard_Staging__c> newRecords){
          system.debug(mapOfuserAndAssociation);
         
          for(Dashboard_Staging__c ds: newRecords){
              terrSet = new list<String>();
              //system.debug(ds.Email_Id__c+'BBBB');
              if(ds.Email_Id__c != null){
                   //system.debug(mapOfuserAndAssociation.get((ds.Email_Id__c).trim())+'#####');
                  if(mapOfuserAndAssociation.containsKey((ds.Email_Id__c).trim())){
                      userAssList = new list<UserTerritory2Association>();
                      userAssList = mapOfuserAndAssociation.get((ds.Email_Id__c).trim());
                      String terr = '' ;
                      if(userAssList.size() > 0){
                          for(UserTerritory2Association us : userAssList){
                            terrSet.add(us.Territory2.DeveloperName); 
                          }
                          terrSet.sort();
                          if(terrSet.size() > 0){
                            for(String str : terrSet){
                                if(terr  == ''){
                                  terr =  str;
                                }else{
                                  terr = terr +','+ str;
                                }  
                            }
                          }
                          //system.debug(terr+'@@@@@');
                          Dashboard_Input_Score__c dis = new Dashboard_Input_Score__c();
                          if(userAssList[0].RoleInTerritory2 == 'TLD'){
                            //system.debug('Inside');
                            dis.RecordTypeID = TLDRecTypeId;
                            if(mapOfDashboardMaster.containsKey((ds.Category__c).trim()+(ds.parameters__c).trim()+userAssList[0].RoleInTerritory2)){
                                dis.Dashboard_Weightage_Master__c = mapOfDashboardMaster.get((ds.Category__c).trim()+(ds.parameters__c).trim()+userAssList[0].RoleInTerritory2).id;
                            }
                            dis.Category__c = ds.Category__c.trim();
                            dis.Parameters__c = ds.Parameters__c.trim();
                            dis.Total_Actual_MTD__c = ds.Total_Actual_MTD__c;
                            dis.Total_Monthly_Target__c = ds.Total_Monthly_Target__c;
                            dis.Total_MTD_Target__c = ds.Total_MTD_Target__c;
                            dis.Territory_Code__c = terr;
                            dis.Month__c = ds.Month__c;
                            dis.Year__c = ds.Year__c;
                            dis.OwnerId = userAssList[0].userId;
                            dis.User__c = userAssList[0].userId;
                            
                            //dis.OwnerID = '';
                            dis.Input_Dashboard_ExID__c = userAssList[0].userId+terr+ds.Month__c+ds.Year__c+ds.Category__c.trim()+ds.Parameters__c.trim()+userAssList[0].RoleInTerritory2;
                            DSSSet.add(terr+UtilityClass.fetchMonthVal(ds.Month__c)+ds.Year__c);
                            keyfactor.add(String.valueOf(userAssList[0].userId).substring(0,15)+ds.Month__c+ds.Year__c+userAssList[0].RoleInTerritory2);
                            listOFDISInsert.add(dis);
                          }
                          if(userAssList[0].RoleInTerritory2 == 'CSTL'){
                            //system.debug('Inside');
                            dis.RecordTypeID = CSTLRecTypeId;
                            if(mapOfDashboardMaster.containsKey((ds.Category__c).trim()+(ds.parameters__c).trim()+userAssList[0].RoleInTerritory2)){
                                dis.Dashboard_Weightage_Master__c = mapOfDashboardMaster.get((ds.Category__c).trim()+(ds.parameters__c).trim()+userAssList[0].RoleInTerritory2).id;
                            }
                            dis.Category__c = ds.Category__c.trim();
                            dis.Parameters__c = ds.Parameters__c.trim();
                            dis.Total_Actual_MTD__c = ds.Total_Actual_MTD__c;
                            dis.Total_Monthly_Target__c = ds.Total_Monthly_Target__c;
                            dis.Total_MTD_Target__c = ds.Total_MTD_Target__c;
                            dis.Territory_Code__c = terr;
                            dis.Month__c = ds.Month__c;
                            dis.Year__c = ds.Year__c;
                            dis.OwnerId = userAssList[0].userId;
                            dis.User__c = userAssList[0].userId;
                            //dis.OwnerID = '';
                            dis.Input_Dashboard_ExID__c = userAssList[0].userId+ds.Month__c+ds.Year__c+ds.Category__c.trim()+ds.Parameters__c.trim()+userAssList[0].RoleInTerritory2;
                            DSSSet.add(String.valueOF(userAssList[0].userId)+UtilityClass.fetchMonthVal(ds.Month__c)+ds.Year__c);
                            keyfactor.add(String.valueOf(userAssList[0].userId).substring(0,15)+ds.Month__c+ds.Year__c+userAssList[0].RoleInTerritory2);
                            listOFDISInsert.add(dis);
                          }
                          
                      }
                  }
              }
          }
          system.debug(listOFDISInsert.size()+'$$$$');
          if(listOFDISInsert.size() > 0){
              upsert listOFDISInsert Input_Dashboard_ExID__c;
          }
          AggregateResult[] listOfDISQuery = [SELECT sum(ScoreMTD__c)sumScore,OwnerID  FROM Dashboard_Input_Score__c WHERE SYS_Used_For_TLD_CSTL__c IN: keyfactor Group By OwnerID];
          system.debug(listOfDISQuery+'listOfDISQuery');
          /*
          if(listOfDISQuery.size() > 0){
              for(String key : keyfactor){
                  Decimal score = 0.0;
                  for(Dashboard_Input_Score__c match : listOfDISQuery){
                      if(key.equals(match.SYS_Used_For_TLD_CSTL__c) && match.ScoreMTD__c != null){
                          score += match.ScoreMTD__c;
                      }
                      mapForSummaryScore.put(match.Territory_Code__c+monthYearMap.get(match.Month__c)+match.Year__c,score);
                      mapForSummaryScore1.put(match.Territory_Code__c+monthYearMap.get(match.Month__c)+match.Year__c,match);
                  }

              }
          }*/
          
          system.debug(DSSSet+'DSSSet');
          listofDSS = [SELECT OwnerID,id,Input_Score__c,Territory_Code__c,DSS_External_ID__c FROM Dashboard_Summary_Score__c WHERE DSS_External_ID__c IN: DSSSet];
          if(listofDSS.size() > 0){
            for(Dashboard_Summary_Score__c d : listofDSS){
                mapOfTerrAndDSS.put(d.OwnerID,d);
            }
          }
          system.debug(mapOfTerrAndDSS+'mapOfTerrAndDSS');
          if(listOfDISQuery.size() > 0 ){
          
            for (AggregateResult ar : listOfDISQuery)  {
                if(mapOfTerrAndDSS.containsKey(String.valueof(ar.get('OwnerID')))){
                    Dashboard_Summary_Score__c dssupdate = new Dashboard_Summary_Score__c ();
                    dssupdate = mapOfTerrAndDSS.get(String.valueof(ar.get('OwnerID')));
                    dssupdate.Input_Score__c = (Decimal)ar.get('sumScore');
                    dashboardSummary.add(dssupdate);
                }
                
            }
          }
          system.debug(dashboardSummary+'dashboardSummary');
          /*
          if(mapForSummaryScore.size() > 0){
              for(String str : mapForSummaryScore.keyset()){
                Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
                //Dashboard_Input_Score__c dis = new Dashboard_Input_Score__c();
                dss.Input_Score__c = mapForSummaryScore.get(str);
                //dis = mapForSummaryScore1.get(str);
                dss.Territory_Code__c = mapForSummaryScore1.get(str).Territory_Code__c;
                dss.Month__c          = monthYearMap.get(mapForSummaryScore1.get(str).month__c);//mapForSummaryScore1.get(str).
                dss.Year__c           = mapForSummaryScore1.get(str).Year__c;
                dss.DSS_External_ID__c = str;
                dashboardSummary.add(dss);
              }
          }*/
          if(dashboardSummary.size() > 0){
              upsert dashboardSummary DSS_External_ID__c;
          }
    }
}