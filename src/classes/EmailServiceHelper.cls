global class EmailServiceHelper implements Messaging.InboundEmailHandler {
	
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
																Messaging.InboundEnvelope env) {
		Id accountId;
		Id contactId;
		String fromEmailAddress;
 
		// Create an InboundEmailResult object for returning the result of the
		// Apex Email Service
		try {
			Id caseRecTypeId = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Pre_complaints'].Id;
			Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
			fromEmailAddress = email.fromAddress;
			List<List<SObject>> searchList = new List<List<SObject>>();
                    
            String query = 'FIND {'+fromEmailAddress+'} IN ALL FIELDS RETURNING Account (Id),Contact(Id)';
            searchList = search.query(query); 
            
            List<Account> accountList = new List<Account>();
            List<Contact> contactList = new List<Contact>();
            System.debug('==#1 '+fromEmailAddress);
            

            if(!searchList.isEmpty()){
                accountList = (List<Account>)searchList[0];
                contactList = (List<Contact>)searchList[1];
                System.debug('==#2 '+accountList);
            	System.debug('==#3 '+contactList);
            }

			Case tmpCase = new Case();
	    	Attachment[] attachments = new Attachment[0];
			if(email.Subject != null && email.Subject != '') {
				tmpCase.Subject = email.Subject;
			}else {
				tmpCase.Subject = 'No Subject was provided';
			}
			if(!accountList.isEmpty()) {
				tmpCase.AccountId = accountList[0].Id;
			}
			if(!contactList.isEmpty()) {
				tmpCase.ContactId = contactList[0].Id;
			}

			tmpCase.RecordTypeId = caseRecTypeId;
			insert tmpCase;

			CaseComment comment = new CaseComment(ParentId=tmpCase.Id, CommentBody=email.plainTextBody);


			if(email.TextAttachments != null) {
	            for(Messaging.InboundEmail.TextAttachment attachment: email.TextAttachments) {
	                attachments.add(new Attachment(ParentId=tmpCase.Id, Body=Blob.valueOf(attachment.body), Name=attachment.fileName, ContentType=attachment.mimeTypeSubType));
	            }
	        }
	        // Process videos, images, etc.
	        if(email.BinaryAttachments != null) {
	            for(Messaging.InboundEmail.BinaryAttachment attachment: email.BinaryAttachments) {
	                attachments.add(new Attachment(ParentId=tmpCase.Id, Body=attachment.body, name=attachment.fileName, ContentType=attachment.mimeTypeSubType));
	            }
	        }
	    
	    	insert comment;
	    	insert attachments;
			result.success = true;
			return result;
		}catch(Exception e) {
			System.debug(e.getMessage());
			return null;
		}
	}

	public class idWrapper {
		id accountId;
		id contactId;

		public idWrapper(){}
	}
}