@isTest 
private class SP_BatchForSpecialty_OE_BU_TestClass{
    static testmethod void testLoadData() {
    	Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
    	List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountSpeOE');
    	List<sObject> listSpecialtyOEDealerData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'SpecialtyOEDealer');
    	for(sObject li : listSpecialtyOEDealerData){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listSpecialtyOEDealerData;
        SP_BatchForSpecialty_OE_BU nn = new SP_BatchForSpecialty_OE_BU();
        database.executebatch(nn,200); 
    }
}