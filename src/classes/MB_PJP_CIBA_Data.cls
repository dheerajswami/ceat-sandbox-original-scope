/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By: Fetch Mobile Data for PJP App
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchCIBAData/*')

global with sharing class MB_PJP_CIBA_Data {
    
    global class MobileData{
        public List<Visits_Plan__c> visitsPlans {get;set;}
        public List<Account> accounts {get;set;}
        public List<Task> tasks {get;set;}
        public String timeStamp {get;set;}
        public String UserType {get;set;}
        public List<DSE_Beat__c> dse_beats {get;set;}
        
        public MobileData(){
            tasks           = new List<Task>();
            accounts        = new List<Account>(); 
            visitsPlans     = new List<Visits_Plan__c>();
            dse_beats       = new List<DSE_Beat__c>();
        }
    }
    
    global class MobileDataRequest{
        
        public List<Visits_Plan__c> visitsPlans {get;set;}
        public List<Task> tasks {get;set;}
        public String userTerritory {get;set;}

        public MobileDataRequest(){
            tasks           = new List<Task>();            
            visitsPlans     = new List<Visits_Plan__c>();
        }
        
    }

    public static final String CUSTOMER_SEGMENT = 'Distributor';
    public static final Integer NUM_DAYS        = 7;
    
    @HttpPost
    global static MobileData getMobileDataPJP(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String dt = req.params.get('TimeStamp');
        DateTime appDateString = UtilityClass.correctDateTime(dt);
       
        
        try{
                Map<Integer, String> monthsString = new Map<Integer, String>{1 => 'January', 2 => 'February',3 => 'March',4 => 'April',5 => 'May',6 => 'June',7 => 'July',8 => 'August',9 => 'September',10 => 'October',11 => 'November',12 => 'December'};
                
                try{               
                    MobileDataRequest mobReq = new MobileDataRequest();
                    
                    mobReq = (MobileDataRequest)JSON.deserialize(req.requestBody.toString(),MobileDataRequest.class);
                    //system.debug('RESP' + mobReq);
                    if(mobReq.visitsPlans != null){
                        Integer mn = date.today().month();
                        String yr = String.valueOf(date.today().year());
                        PJP__c pjp = new PJP__c();
                        Boolean noPJPflag = false;
                        try{
                        pjp = [SELECT Id FROM PJP__c WHERE Visit_Plan_Month__c =:monthsString.get(mn) AND Visit_Plan_Year__c =:yr AND OwnerId =:userInfo.getUserId() LIMIT 1];
                            noPJPflag = true;    
                        }catch(QueryException e){
                            noPJPflag = false; 
                        }    
                        List<Visits_Plan__c> visitToCreate = new List<Visits_Plan__c>();
                        List<Visits_Plan__c> visitToUpdate = new List<Visits_Plan__c>();
                        List<Account> accounts = new List<Account>();
                        Set<Id> newVisitId = new Set<Id>();
                        
                        for(Visits_Plan__c tsk : mobReq.visitsPlans){
                            String tId = String.valueOf(tsk.Id);
                            
                            if(tId.startsWith('LOCAL_') && noPJPflag){
                                tsk.Id = null;
                                visitToCreate.add(tsk);
                                if(tsk.Master_Location__c){
                                    accounts.add(new Account(Id = tsk.Dealer__c,Geolocation__Latitude__s = tsk.Geolocation__Latitude__s,Geolocation__Longitude__s = tsk.Geolocation__Longitude__s));
                                }
                            }else{
                                if(tsk.Master_Location__c){
                                    accounts.add(new Account(Id = tsk.Dealer__c,Geolocation__Latitude__s = tsk.Geolocation__Latitude__s,Geolocation__Longitude__s = tsk.Geolocation__Longitude__s));
                                }
                                visitToUpdate.add(tsk);
                            }
                        }
                        
                        
                        Database.update(visitToUpdate);
                        Database.update(accounts);
                        
                        for(Visits_Plan__c v : visitToCreate){
                            v.Master_Plan__c = pjp.Id;
                        }
                        Database.SaveResult[] srv = Database.insert(visitToCreate);
                        for(Database.SaveResult s : srv){
                            newVisitId.add(s.getId());
                        }
                    }
                    
                    if(mobReq.tasks != null){
                        List<Task> taskToCreate = new List<Task>();
                        List<Task> taskToUpdate = new List<Task>();
                        for(Task tsk : mobReq.tasks){
                            String tId = String.valueOf(tsk.Id);
                            if(tId.startsWith('LOCAL_')){
                                tsk.Id = null;
                                taskToCreate.add(tsk);
                            }else{
                                taskToUpdate.add(tsk);
                            }
                        }
                        
                        Database.update(taskToUpdate);            
                        Database.insert(taskToCreate); 
                    }
                }catch(Exception e){
                    try{
                        Error_Log__c errorLog = new Error_Log__c(Class_Name__c = 'MB_PJP_MobileDataDummy',Line_Number__c = Integer.valueOf(e.getLineNumber()),Error_Message__c = 'TOP - ' + e.getMessage());
                        insert errorLog;
                    }catch(Exception ex){
                        system.debug(ex.getLineNumber() + ' Error -'+ ex.getMessage()+ ' Cause' + ex.getCause());
                    }
                }
            
            List<Visits_Plan__c> vp     = new List<Visits_Plan__c>();
            List<Account> acc           = new List<Account>();
            List<Task> tks              = new List<Task>();
            List<User> urList           = new List<User>();
            
            String userRole     = [ SELECT Id,Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
            
            Date lastSevenDay = Date.today().addDays(-NUM_DAYS);
            Date nextSevenDay = Date.today().addDays(NUM_DAYS);
			List<Visits_Plan__c> otherVisits;
            if(appDateString == null){                
                
                if(userRole.contains('TLD')){
                    otherVisits = new List<Visits_Plan__c>();
                    
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,
                                    (SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c 
                                     FROM Visit_Plans__r 
                                     WHERE Visit_Day__c > :lastSevenDay AND Visit_Day__c < :nextSevenDay AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved')
                           FROM Account 
                           WHERE Customer_Segment__c =:CUSTOMER_SEGMENT AND Active__c = true order by Name];     
                    
                    for(Account a : acc){
                        if(a.Visit_Plans__r != null){
                            vp.addAll(a.Visit_Plans__r);
                        }
                    }
                    
                    otherVisits = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c 
                                     FROM Visits_Plan__c 
                                     WHERE Visit_Day__c > :lastSevenDay AND Visit_Day__c < :nextSevenDay AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Record_Type_Name__c != 'Visit'];
                    vp.addAll(otherVisits);
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                           FROM Task 
                           WHERE (WhatId IN : vp OR WhatId IN : acc OR Status = 'Open') AND OwnerId =:UserInfo.getUserId()];
                    
                        
                }else if(userRole.contains('Specialty')){
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c 
                           FROM Account WHERE Active__c = true order by Name];    

                    vp  = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c 
                           FROM Visits_Plan__c 
                           WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Visit_Day__c > :lastSevenDay AND Visit_Day__c < :nextSevenDay Order by Visit_Day__c];

                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                        FROM Task 
                        WHERE (WhatId IN : vp OR WhatId IN : acc OR Status = 'Open') AND OwnerId =:UserInfo.getUserId()];
                        

                }else{
                    otherVisits = new List<Visits_Plan__c>();
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visit_Plans__r WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Visit_Day__c > :lastSevenDay AND Visit_Day__c < :nextSevenDay Order by Visit_Day__c desc) FROM Account WHERE Active__c = true order by Name];    
                    
                    for(Account a : acc){
                        if(a.Visit_Plans__r != null){
                            vp.addAll(a.Visit_Plans__r);
                        }
                    }
                    
                    otherVisits = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Visit_Day__c > :lastSevenDay AND Visit_Day__c < :nextSevenDay AND Record_Type_Name__c != 'Visit' Order by Visit_Day__c desc];
                    vp.addAll(otherVisits);
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc OR Status = 'Open') AND OwnerId =:UserInfo.getUserId()];
                    
                }                        
            }
            else{
                if(userRole.contains('TLD')){
                    
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account WHERE Customer_Segment__c =:CUSTOMER_SEGMENT AND Active__c = true AND LastModifiedDate >:appDateString order by Name];                                                 
                    vp = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Sub_Dealers_Met__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE LastModifiedDate >:appDateString AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved'];
                    
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate >:appDateString ];
                }else if(userRole.contains('Specialty')){
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account
                        WHERE Active__c = true AND LastModifiedDate >:appDateString order by Name];    

                    vp  = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND LastModifiedDate > :appDateString Order by Visit_Day__c];

                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                        FROM Task 
                        WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                }else{
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account WHERE Active__c = true AND LastModifiedDate > :appDateString order by Name];  
                    
                    
                    vp = [SELECT Id,Offline_Id__c,Visit_Month_Value__c,Visit_Year_Value__c,Visit_Day_Value__c,Sub_Dealers_Met__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE LastModifiedDate > :appDateString AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved'];
                    
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                    
                }                        
                
            }
            
            /*
            Populate the data into the wrapper 
            */
            MobileData mobileData = new MobileData();
            mobileData.accounts     = acc;
            mobileData.visitsPlans  = vp;
            mobileData.tasks 		= tks;
            mobileData.UserType 	= userRole;
            mobileData.timeStamp 	= DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            
            return mobileData;
        }catch(Exception e){
            
            try{
                Error_Log__c errorLog = new Error_Log__c(Class_Name__c = 'MB_PJP_MobileDataDummy',Line_Number__c = Integer.valueOf(e.getLineNumber()),Error_Message__c = e.getMessage());
                insert errorLog;
            }catch(Exception ex){
                system.debug(ex.getLineNumber() + ' Error -'+ ex.getMessage()+ ' Cause' + ex.getCause());
            }
            
            return null;
        }
        
        
    }
    

   
    
}