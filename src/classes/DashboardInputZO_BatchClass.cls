/************************************************************************************************************
    Author  : Sneha Agrawal
    Date    : 9/7/2015
    Purpose : Dashboard Input ZO (ZGM) Batch Class
*************************************************************************************************************/
global with sharing class DashboardInputZO_BatchClass implements Database.Batchable<sObject> {
   
   integer month; 
   
  //*********************STRINGS********************************************************   
    String rmRecType                 = system.Label.RM_RecType;
    string currentMonth;
    string currentYear;
    global static String month_val;
    global static String year_val;
    global static Boolean active_val;
    String DSSString = '';
    //Id zgmRecTypeId      ;
    //Id zgmInputRecTypeId ;
    
    String zgmRole                        = system.Label.ZGM_Role;  
    String Replacements                  = 'Replacements';
    String actionLogRecType              = system.Label.ActionLogInputRecordType;
    String efleetRecType                 = system.Label.Efleet_RecType;
    string TLInputRecType                = system.Label.Input_RecType; 
    
 //*****************************LISTS***************************************************     
    list<Territory2Type> terriType                         = new list<Territory2Type>();    
    list<Dashboard_Master__c> dashboardMaster;
    list<Dashboard_Input_Score__c> inputDashboard;   
    list<Dashboard_Weightage_Master__c> dwm;
    list<Dashboard_Summary_Score__c> listofDSS       = new list<Dashboard_Summary_Score__c>();
    list<Dashboard_Input_Score__c> dashboardInputScore     = new List<Dashboard_Input_Score__c>(); 
 //*********************************SETS***********************************
   
    set<id> terrTypeID                                      = new set<Id>();
    set<id> territoryID                                     = new set<ID>();
    Set<String> zoneSet                                   = new Set<String>();
    
 //*******************************MAPS*******************************************   
      map<Integer,String> monthYearMap = new map<Integer,String>();
      map<String, Integer> yearMonthMap = new map<String, Integer>();
     map<String,Integer> rmInputTargetMap;
     map<String,Integer> rmInputTargetMTDMap;
     map<String,Integer> rmInputActualMap; 
     map<String,Dashboard_Summary_Score__c> mapOfRegionAndDSS = new Map<String,Dashboard_Summary_Score__c>();    
     

      
  /*
        All initialization in Constructor
  */
  global DashboardInputZO_BatchClass(){
    
    
     dashboardMaster            = new list<Dashboard_Master__c>();  
     
     dwm                        = new list<Dashboard_Weightage_Master__c>();
    
     rmInputTargetMap       = new map<String,Integer>();
     rmInputTargetMTDMap    = new map<String,Integer>();
     rmInputActualMap       = new map<String,Integer>();
     
      month                       = (Date.today()).month();                       
                 
         /*
                To convert current month into month name
         */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');

            yearMonthMap.put('January',1);
            yearMonthMap.put('February',2);
            yearMonthMap.put('March',3);
            yearMonthMap.put('April',4);
            yearMonthMap.put('May',5);
            yearMonthMap.put('June',6);
            yearMonthMap.put('July',7);
            yearMonthMap.put('August',8);
            yearMonthMap.put('September',9);
            yearMonthMap.put('October',10);
            yearMonthMap.put('November',11);
            yearMonthMap.put('December',12);
              
      currentMonth = (month_val == null)?monthYearMap.get(month):month_val;  
      currentYear  =  (year_val == null)?String.valueOf((Date.today()).year()):year_val;    
     
     
     terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Zone' limit 1];
     for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
     }   
     // rmRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:rmRecType Limit 1].Id;
     //rmInputRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:rmRecType Limit 1].Id;
                  
     /* Getting all PJP Dashboard Weightage records */
     Id inputRecTypeId      = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
     Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
     Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:efleetRecType Limit 1].Id;
     Id distributorTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.TL_Distributor Limit 1].Id;
     if(month_val == null && year_val == null && active_val == null){
         dashboardMaster       = [SELECT id,Role__c,Active__c From Dashboard_Master__c WHERE Active__c = true AND Role__c='ZO'];
         dwm                   = new List<Dashboard_Weightage_Master__c>();
         dwm                    = [Select Id,Category__c,Parameters_Inout__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c where Testing__c = true AND  (RecordTypeId=:inputRecTypeId OR RecordTypeId=:efleetRecTypeId OR RecordTypeId=:actionLogRecTypeId OR RecordTypeId=:distributorTypeId)  AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c='ZO'];
     }else if(month_val != null && year_val != null && active_val != null){
        dashboardInputScore  = [SELECT ID,Achievement__c,Region_Code__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Testing__c ,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Dashboard_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where  Zone_Code__c != null AND Month__c =:month_val AND Year__c =: year_val AND Dashboard_Weightage_Master__r.Testing__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Active__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Role__c = 'ZO' Limit 1];
        dwm                   = new List<Dashboard_Weightage_Master__c>();
        dwm                    = [Select Id,Category__c,Parameters_Inout__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c where Testing__c =: dashboardInputScore[0].Dashboard_Weightage_Master__r.Testing__c AND  (RecordTypeId=:inputRecTypeId OR RecordTypeId=:efleetRecTypeId OR RecordTypeId=:actionLogRecTypeId OR RecordTypeId=:distributorTypeId)  AND Dashboard_Master__c =: dashboardInputScore[0].Dashboard_Weightage_Master__r.Dashboard_Master__c AND Role__c='ZO' AND Weightage__c > 0];
     }
               
  }
     
  /* Start method that will fetch all territories and pass it to execute method (batch size 1)*/   
  global Database.QueryLocator start(Database.BatchableContext BC){        
          
         String query;
         String[] filters         =  new String[]{'SPRP%'};
         
         /* Querying all territories under Replacement (Extra query to avoid ZGMs Under Specialty)*/
         list<Territory2>    terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND ParentTerritory2.ParentTerritory2.DeveloperName =: Replacements order by Name desc]; 
         for(Territory2 terr :terrList){
            territoryID.add(terr.id);
         }
         query                            = 'SELECT id,UserId,Territory2.DeveloperName,Territory2.name,RoleInTerritory2,IsActive,Territory2Id from UserTerritory2Association WHERE Territory2Id In: territoryID AND RoleInTerritory2=:zgmRole order by Territory2.Name desc'; 
         return Database.getQueryLocator(query);   
               
   }  
  global void execute(Database.BatchableContext BC,list<sObject> scope) {   
      set<string> regionSet = new set<string>();
     
      map<string,list<string>> terrMap = new map<string,list<string>>();
     
      string userId     = '';
      string zone = '';
      
      for(sObject temp : scope){
           UserTerritory2Association sps              = (UserTerritory2Association)temp;
           zoneSet.add(sps.Territory2.DeveloperName);            
           zone = sps.Territory2.DeveloperName;
           userId = sps.UserId;
      }  
           
      // Getting list of territories under region or RM      
      list<Territory2>    regionList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE ParentTerritory2.DeveloperName IN: zoneSet]; 
      
      if(regionList.size()>0){
          for(Territory2 reg : regionList){          
             regionSet.add(reg.DeveloperName);           
          } 
      } 
     
    // ************************************************************RM Roll Up***************************************************************************    
       list<Dashboard_Input_Score__c> dashboardInputScoreRM                = new List<Dashboard_Input_Score__c>(); 
       Id rmRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:rmRecType Limit 1].Id;
       if(regionSet.size()>0){
            dashboardInputScoreRM  = [SELECT ID,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId=:rmRecTypeId AND Region_Code__c IN: regionSet  AND  Month__c =: currentMonth AND Year__c =: currentYear ];
       }
       string rmKey = '';       
       
      if(dashboardInputScoreRM.size()>0){ 
       for(Dashboard_Input_Score__c inpScore : dashboardInputScoreRM){
           rmKey = inpScore.Category__c.trim() + inpScore.Parameters__c.trim() ;
           
           Integer totalTarget    = 0;
           Integer totalTargetMTD = 0;
           Integer totalActual    = 0;
           
           if(rmInputTargetMap.containsKey(rmKey)){
                if(rmInputTargetMap.get(rmKey)!=null){
                   totalTarget = rmInputTargetMap.get(rmKey) + integer.valueof(inpScore.Total_Monthly_Target__c);
                }else{
                    totalTarget = 0 + integer.valueof(inpScore.Total_Monthly_Target__c);
                }
                rmInputTargetMap.put(rmKey,totalTarget);
           }else{
               totalTarget = integer.valueof(inpScore.Total_Monthly_Target__c);
               rmInputTargetMap.put(rmKey,totalTarget);
           }           
           
           if(rmInputTargetMTDMap.containsKey(rmKey)){
               if(rmInputTargetMTDMap.get(rmKey)!=null){
                  totalTargetMTD = rmInputTargetMTDMap.get(rmKey) + integer.valueof(inpScore.Total_MTD_Target__c);
               }else{
                  totalTargetMTD = 0 + integer.valueof(inpScore.Total_MTD_Target__c);
               } 
               rmInputTargetMTDMap.put(rmKey,totalTargetMTD);
           }else{
               totalTargetMTD = integer.valueof(inpScore.Total_MTD_Target__c);
               rmInputTargetMTDMap.put(rmKey,totalTargetMTD);
           }
           
           if(rmInputActualMap.containsKey(rmKey)){
                if(rmInputActualMap.get(rmKey)!=null){
                    totalActual = rmInputActualMap.get(rmKey) + integer.valueof(inpScore.Total_Actual_MTD__c);
                }else{
                    totalActual = 0 + integer.valueof(inpScore.Total_Actual_MTD__c);
                }
                rmInputActualMap.put(rmKey,totalActual);
           }else{
               totalActual = integer.valueof(inpScore.Total_Actual_MTD__c);
               rmInputActualMap.put(rmKey,totalActual);
           }
         }
       }   
    
   
     //*************************************************************RM***************************************************************************
     list<Dashboard_Input_Score__c> dsnewList = new list<Dashboard_Input_Score__c>();
     
     if(dwm.size()>0){
        string key = '';   
        
        for(Dashboard_Weightage_Master__c dw:dwm){
            
            Integer targetTotal    = 0;
            Integer targetMTDTotal = 0;
            Integer actualTotal    = 0;
        
            key = dw.Category__c.trim() + dw.Parameters_Inout__c.trim() ;
             
                                    Dashboard_Input_Score__c ds=new Dashboard_Input_Score__c();
                                    ds.User__c                       = userId;
                                    ds.Zone_Code__c                  = zone;
                                    ds.Dashboard_Weightage_Master__c = dw.Id;
                                    ds.Month__c                      = currentMonth;
                                    ds.Year__c                       = currentYear;    
                                                    
                                    ds.Parameters__c                 = dw.Parameters_Inout__c; 
                                    ds.Category__c                   = dw.Category__c;
                                    ds.Input_Dashboard_ExID__c       = userId + zone + currentMonth + currentYear + dw.Category__c.trim() + (dw.Parameters_Inout__c).trim()+zgmRole;    
                                    ds.OwnerId                       = userId;
                                   
                                    // Total target of RM
                                     if(rmInputTargetMap.get(key)!=null){
                                           targetTotal = targetTotal + rmInputTargetMap.get(key) ;
                                     }
                                    
                                     
                                     // Total target MTD of RM
                                     if(rmInputTargetMTDMap.get(key)!=null){
                                           targetMTDTotal = targetMTDTotal + rmInputTargetMTDMap.get(key) ;
                                     }
                                     
                                     
                                     // Total Actual of RM
                                     if(rmInputActualMap.get(key)!=null){
                                           actualTotal = actualTotal + rmInputActualMap.get(key) ;
                                     }
                                     
                                     
                                    if(targetTotal>0)         {
                                         ds.Total_Monthly_Target__c = targetTotal;  
                                    }else{
                                         ds.Total_Monthly_Target__c = 0;
                                    } 
                                    
                                    if(targetMTDTotal>0)         {
                                         ds.Total_MTD_Target__c = targetMTDTotal;  
                                    }else{
                                         ds.Total_MTD_Target__c = 0;
                                    } 
                                    
                                    if(actualTotal>0)         {
                                         ds.Total_Actual_MTD__c= actualTotal;  
                                    }else{
                                         ds.Total_Actual_MTD__c=0;
                                    }  
                                                                     
                                    if(targetMTDTotal>0 && actualTotal!=null){
                                            decimal achv = actualTotal / targetMTDTotal;
                                            ds.Achievement_MTD__c=  achv;  
                                            if(achv <=100){
                                                ds.Score__c = (achv * dw.Weightage__c/100);
                                            }else{
                                                ds.Score__c = dw.Weightage__c;
                                            }
                                    }else{
                                            ds.Achievement_MTD__c=0;
                                            ds.Score__c = 0;
                                     }
                                    dsnewList.add(ds);
                            
        }
        
     }
        /*
            Upserting RM Input Dashboard Records
        */
        if(dsnewList.size()>0){
            Database.upsert(dsnewList,Dashboard_Input_Score__c.Fields.Input_Dashboard_ExID__c,false);  
        }
      
        /* 
            Calculating total input score for particular region and inserting in  Dashboard_Summary_Score__c object 
        */
        
        list<Dashboard_Input_Score__c> inputDashboard = new list<Dashboard_Input_Score__c>();
        Double totalScore = 0;
        inputDashboard = [select id,Month__c,Year__c,ScoreMTD__c,Score__c,Zone_Code__c from Dashboard_Input_Score__c where Zone_Code__c=:zone AND Month__c =:currentMonth AND Year__c =:currentYear];
        

        if(!inputDashboard.isEmpty()){
        DSSString = inputDashboard [0].Zone_Code__c + yearMonthMap.get(inputDashboard[0].Month__c) + inputDashboard[0].Year__c;
        system.debug('DSSS'+DSSString );
        }
        AggregateResult[] inputDashboardScore =  [SELECT sum(ScoreMTD__c)sumScore,Zone_Code__c  FROM Dashboard_Input_Score__c where Zone_Code__c=:zone AND Month__c =:currentMonth AND Year__c =:currentYear Group By Zone_Code__c];
        system.debug('OPPPP'+inputDashboardScore );
        listofDSS = [SELECT id,Input_Score__c,Zone_Code__c,DSS_External_ID__c FROM Dashboard_Summary_Score__c WHERE DSS_External_ID__c =: DSSString];
        if(listofDSS.size() > 0){
          for(Dashboard_Summary_Score__c d : listofDSS){
              mapOfRegionAndDSS.put(d.Zone_Code__c,d);
          }
        }

        /*if(inputDashboard.size() > 0){
            for(Dashboard_Input_Score__c inp : inputDashboard){
                if(inp.ScoreMTD__c >0){
                    totalScore = totalScore + inp.ScoreMTD__c ; 
                }else{
                    totalScore = totalScore + 0; 
                }                
            }
         }*/
        Dashboard_Summary_Score__c dashboardTotalInputScore = new Dashboard_Summary_Score__c();

        if(inputDashboardScore .size() > 0 ){
          
            for (AggregateResult ar : inputDashboardScore )  {
                if(mapOfRegionAndDSS.containsKey(String.valueof(ar.get('Zone_Code__c')))){
                    
                    dashboardTotalInputScore = mapOfRegionAndDSS.get(String.valueof(ar.get('Zone_Code__c')));
                    dashboardTotalInputScore.Input_Score__c = (Decimal)ar.get('sumScore');
                    
                }
                
            }

          //dashboardTotalInputScore.Input_Score__c         = totalScore;
          dashboardTotalInputScore.DSS_External_ID__c     = DSSString;
          //dashboardTotalInputScore.RecordTypeId         = rmInputRecTypeId;
          dashboardTotalInputScore.Zone_Code__c           = zone;
          dashboardTotalInputScore.Month__c               = String.valueOf(yearMonthMap.get(currentMonth));
          dashboardTotalInputScore.Year__c                = currentYear;
          dashboardTotalInputScore.OwnerId                = userId ;
          database.upsert(dashboardTotalInputScore,Dashboard_Summary_Score__c.Fields.DSS_External_ID__c,false); 
        }
  }
     
  global void finish(Database.BatchableContext BC) {
         // commenting code. This will be handled via "Apex Exception Email" feature
             
  }  
}