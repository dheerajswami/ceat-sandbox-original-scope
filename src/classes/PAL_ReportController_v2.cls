global class PAL_ReportController_v2 {

    /*
     * Auther  :- Vivek Deepak  
     * Purpose :- Generate Excel like reports for PAL 
     *            
     *
     *
     */

    //-- CONTRUCTOR
    
    

    //-- ATTRIBUTES

    //-- WRAPPER CLASS

    global class PAL_Wrapper{

        public Action_Plan__c actionPlan            {get;set;}
        public Fortnightly_Action_Plan__c fNightly1 {get;set;}
        public Fortnightly_Action_Plan__c fNightly2 {get;set;}
        public Fortnightly_Action_Plan__c fNightly3 {get;set;}
        public Fortnightly_Action_Plan__c fNightly4 {get;set;}
        public Fortnightly_Action_Plan__c fNightly5 {get;set;}
        public Fortnightly_Action_Plan__c fNightly6 {get;set;}

        public PAL_Wrapper(Action_Plan__c ap,Fortnightly_Action_Plan__c fn1,Fortnightly_Action_Plan__c fn2,Fortnightly_Action_Plan__c fn3,Fortnightly_Action_Plan__c fn4,Fortnightly_Action_Plan__c fn5,Fortnightly_Action_Plan__c fn6){
            //this.priorityArea = pa;
            this.actionPlan   = ap;
            this.fNightly1    = fn1;
            this.fNightly2    = fn2;
            this.fNightly3    = fn3;
            this.fNightly4    = fn4;
            this.fNightly5    = fn5;
            this.fNightly6    = fn6;

        }

    }


    //-- METHODS

    /*
     fetch zone details 
    */
    @RemoteAction
    global static List<SelectOption> fetchZoneName(){
         List<SelectOption> zoneList = new List<SelectOption>();
          Schema.DescribeFieldResult fieldResult = Priority_Area__c.Zone1__c.getDescribe();
          List<Schema.PicklistEntry> zList = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry n : zList ){       
                zoneList.add(new SelectOption(n.getLabel(), n.getValue()));
            }              
        return zoneList;
    }

    /*
    fetch Custom Setting 
    */
    @RemoteAction
    global static List<PAL_Structure__c> fetchCustomSettingForQuater(){
        return [SELECT Id,Name,First_Month__c,Second_Month__c,Third_Month__c,Month_1_Slab_1__c,Month_1_Slab_2__c,Month_2_Slab_1__c,Month_2_Slab_2__c,Month_3_Slab_1__c,Month_3_Slab_2__c FROM PAL_Structure__c];
    }

    /*
    fetch Area details
    */

    @RemoteAction
    global static List<SelectOption> fetchAreaNames(String zoneId){
        //return [SELECT Id,Name,Region_Code__c FROM Region__c WHERE Zone__r.Name =:zoneId];
        List<SelectOption> regionList = new List<SelectOption>();
        list<Territory2> terrList = [select id,DeveloperName,name from Territory2 where ParentTerritory2.DeveloperName = :zoneId ];
            for( Territory2 n : terrList ){       
                regionList.add(new SelectOption(n.DeveloperName, n.DeveloperName));
            }              
        return regionList;
    }

    /*
    fetch PAL details
    */
   

    @RemoteAction
    global static List<PAL_Wrapper> getAllPalWrapper(String zone,String area,String quat){
        system.debug(zone+''+area+''+quat);
        List<PAL_Wrapper> palWrapper = new List<PAL_Wrapper>();

        for(Action_Plan__c actionP : [SELECT Action_Planning__c,Avg_Accrued_Impact__c,Avg_Proposed_Impact__c,Base__c,CreatedById,CreatedDate,Customer__c,End_Date__c,FY_Quarter__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,M1_Accrued_Impact__c,M1_Proposed_Impact__c,M2_Accrued_Impact__c,M2_Proposed_Impact__c,M3_Accrued_Impact__c,M3_Proposed_Impact__c,Name,Priority_Area__c,Proposed_Month_Impact_over_Base__c,SAP_Code__c,Segment__c,Start_Date__c,Status__c,What_actions_are_to_be_taken__c,Whose_intervention_is_required__c,Customer__r.Name,Priority_Area_T__c,Territory_Code__c,CreatedBy.Name,(SELECT Achievement__c,Action_Plan__c,CreatedById,CreatedDate,End_Date__c,Fortnightly_Action_Plan__c,Fortnight__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,OwnerId,Quarter__c,Start_Date__c FROM Fortnightly_Action_Plans__r ORDER By Start_Date__c asc) FROM Action_Plan__c WHERE Priority_Area__r.Zone1__c=:zone AND Priority_Area__r.Region1__c=:area AND FY_Quarter__c LIKE :(quat+'%')]){
            PAL_Wrapper pal = new PAL_Wrapper(actionP,null,null,null,null,null,null);

            pal.actionPlan = actionP;

            for(Fortnightly_Action_Plan__c fN: actionP.Fortnightly_Action_Plans__r){



                if(pal.fNightly1 == null){
                    pal.fNightly1 = fn;
                }else if(pal.fNightly2 == null){
                    pal.fNightly2 = fn;
                }else if(pal.fNightly3 == null){
                    pal.fNightly3 = fn;
                }else if(pal.fNightly4 == null){
                    pal.fNightly4 = fn;
                }else if(pal.fNightly5 == null){
                    pal.fNightly5 = fn;
                }else if(pal.fNightly6 == null){
                    pal.fNightly6 = fn;
                }
                
            }
            
            palWrapper.add(pal);

        }
        system.debug(palWrapper);
        return palWrapper;
    }

}