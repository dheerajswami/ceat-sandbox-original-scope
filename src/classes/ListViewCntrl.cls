public with sharing class ListViewCntrl {


	public List<List_View_Details__c> customSetting {get;set;}

	public ListViewCntrl(){
		String profileName = [SELECT Id,Name FROM Profile WHERE Id = : UserInfo.getProfileId()].Name;
		customSetting = new List<List_View_Details__c>();
		for(List_View_Details__c listView : [SELECT Id,Name,Object_Label__c,Profile_Name__c,height__c FROM List_View_Details__c]){
			if(listView.Profile_Name__c != null && listView.Profile_Name__c.contains(profileName)){
				customSetting.add(listView);
			}
		} 
		
	}


}