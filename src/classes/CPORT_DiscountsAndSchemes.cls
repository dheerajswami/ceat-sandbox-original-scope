global with sharing class CPORT_DiscountsAndSchemes {
/*
   * Auther      :- Vivek Deepak
   * Purpose     :- Fetch details of Discounts and Schemes 
   *                from SAP 
   * Modified By :- 
   * Date        :- 12/2/2015
   *
   */
   
    //-- ATTRIBUTES

    // -- METHODS

	/*
	Method to fetch discounts by invoking webserivce class
	*/
	 WebService static List<DiscountsAndSchemes> getAllDiscounts(String circleNo,String customerId){
		SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                   	= saplogin.username__c;
        String password                   	= saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        SAP_zws_ad_disc1_McS  ws = new  SAP_zws_ad_disc1_McS ();
        
        SAP_zws_ad_disc1_McS.ZWS_AD_DISC1 zws = new SAP_zws_ad_disc1_McS.ZWS_AD_DISC1();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1  tom = new SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1();   
        SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element e = new SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element();       
        //e = zws.ZsfdcAdDisc1('000000001180',tom,'0050000007');
        e = zws.ZsfdcAdDisc1(circleNo,tom,customerId);
        List<DiscountsAndSchemes> discounts = new List<DiscountsAndSchemes>();

        if(e.ItOutput != null){
            tom = e.ItOutput;
            if(tom.item != null){
                for(SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1 elem : tom.item){
                    DiscountsAndSchemes tempObj = new DiscountsAndSchemes(elem.Crnum,elem.Kunnr,elem.Mwskz,elem.RunDate,elem.Name1,elem.Office,elem.Zgroup,elem.Dwerk,elem.Apdat,elem.Sdsas,elem.Zavg,elem.Sdsgr,elem.Sdsat,elem.Pre,elem.Post,elem.Kwert,elem.Rot,elem.Targt,elem.Reqto,elem.Addin,elem.Sds,elem.Tov,elem.Crval,elem.Disc,elem.Indic);
                    discounts.add(tempObj);
                }
            }
            
        }
	 	return discounts;
	 }

	 /*
		Mapping Class
	 */

    global class DiscountsAndSchemes{
    	public String Crnum;
        public String Kunnr;
        public String Mwskz;
        public String RunDate;
        public String Name1;
        public String Office;
        public String Zgroup;
        public String Dwerk;
        public String Apdat;
        public String Sdsas;
        public String Zavg;
        public String Sdsgr;
        public String Sdsat;
        public String Pre;
        public String Post;
        public String Kwert;
        public String Rot;
        public String Targt;
        public String Reqto;
        public String Addin;
        public String Sds;
        public String Tov;
        public String Crval;
        public String Disc;
        public String Indic;

        public DiscountsAndSchemes(String Crnum,String Kunnr,String Mwskz,String RunDate,String Name1,String Office,String Zgroup,String Dwerk,String Apdat,String Sdsas,String Zavg,String Sdsgr,String Sdsat,String Pre,String Post,String Kwert,String Rot,String Targt,String Reqto,String Addin,String Sds,String Tov,String Crval, String Disc,String Indic){

        	this.Crnum = Crnum;
	        this.Kunnr = Kunnr;
	        this.Mwskz = Kunnr;
	        this.RunDate = RunDate;
	        this.Name1 = Name1;
	        this.Office = Office;
	        this.Zgroup = Zgroup;
	        this.Dwerk = Dwerk;
	        this.Apdat = Apdat;
	        this.Sdsas = Sdsas;
	        this.Zavg = Zavg;
	        this.Sdsgr = Sdsgr;
	        this.Sdsat = Sdsat;
	        this.Pre = Pre;
	        this.Post = Post;
	        this.Kwert = Kwert;
	        this.Rot = Rot;
	        this.Targt = Targt;
	        this.Reqto = Reqto;
	        this.Addin = Addin;
	        this.Sds = Sds;
	        this.Tov = Tov;
	        this.Crval = Crval;
	        this.Disc = Disc;
	        this.Indic = Indic;
        }
    }		


}