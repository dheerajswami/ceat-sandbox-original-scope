global class CPORT_CustomerOutstandingDetails {

 /*
   * Auther      :- Sneha Agrawal
   * Purpose     :- Fetch details of Customer Data
   *                from SAP 
   * Modified By :- 
   * Date        :- 15/12/2014
   *
   */
   
    //-- ATTRIBUTES
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    /*   This method will return list of Sales register values
   */
   
  
  WebService static List<CustomerOutstandingTL> getAllCustomerOutstandingDetails(String territory_code){
    
      //String customerId                   = UtilityClass.addleadingZeros(territory_code,Integer.valueOf(CUST_ID_LENGTH));
    
      SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                   = saplogin.username__c;
        String password                   = saplogin.password__c;
        Blob headerValue                  = Blob.valueOf(username + ':' + password);
        String authorizationHeader        = 'Basic '+ EncodingUtil.base64Encode(headerValue);
           
      sapZWS_CUST_OUTSTANDING_TL tst            = new sapZWS_CUST_OUTSTANDING_TL ();
      sapZWS_CUST_OUTSTANDING_TL.ZWS_CUST_OUTSTANDING_TL zws   = new sapZWS_CUST_OUTSTANDING_TL.ZWS_CUST_OUTSTANDING_TL();
        
        zws.inputHttpHeaders_x                  = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                           = 60000;
        
      sapZWS_CUST_OUTSTANDING_TL.TableOfZbapiCustOs tom     = new sapZWS_CUST_OUTSTANDING_TL.TableOfZbapiCustOs ();
    
      List<sapZWS_CUST_OUTSTANDING_TL.ZbapiCustOs> m        = new List<sapZWS_CUST_OUTSTANDING_TL.ZbapiCustOs>();        
        
      tom.item = m;        
        
       sapZWS_CUST_OUTSTANDING_TL.TableOfZbapiCustOs e      = new  sapZWS_CUST_OUTSTANDING_TL.TableOfZbapiCustOs();
       e = zws.ZbapiCustOutstandingTl(territory_code,tom) ;
       //system.debug(e);

       List<CustomerOutstandingTL> custOutStanding = new List<CustomerOutstandingTL>();

       if (e.item != null) {
          for (sapZWS_CUST_OUTSTANDING_TL.ZbapiCustOs zs : e.item) {
              custOutStanding.add(new CustomerOutstandingTL((zs.OutstandAmt != null ? Decimal.valueOf(zs.OutstandAmt) : 0.0),zs.Fkdat,zs.Vbeln,zs.Fkart,zs.Chequeno,zs.Chequedate,zs.Vtext,zs.Despda,zs.Duedate,zs.OsDays,territory_code));
          }
       }
      
      return custOutStanding;
    } 


    /*
    Customer Outstanding for a single customer
    */

  WebService static CPORT_PortalHomeController.AccountWrapper getAllCustomerOutstandingDetailsCS(String cst){
    
      String customerId                   = UtilityClass.addleadingZeros(cst,Integer.valueOf(CUST_ID_LENGTH));
    
    SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                   = saplogin.username__c;
        String password                   = saplogin.password__c;
        Blob headerValue                  = Blob.valueOf(username + ':' + password);
        String authorizationHeader        = 'Basic '+ EncodingUtil.base64Encode(headerValue);
           
    SapCustomerLimitSoap  tst            = new SapCustomerLimitSoap  ();
    SapCustomerLimitSoap.ZWS_GET_CUST_LIMIT  zws   = new SapCustomerLimitSoap.ZWS_GET_CUST_LIMIT ();
        
        zws.inputHttpHeaders_x                  = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                           = 60000;
        
    SapCustomerLimitSoap.TableOfZbapiCustLimit  cutl     = new SapCustomerLimitSoap.TableOfZbapiCustLimit();
    SapCustomerLimitSoap.TableOfBapireturn   cutBp     = new SapCustomerLimitSoap.TableOfBapireturn ();
    
           
        
    SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element e = new SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element();

    e = zws.ZbapiGetCustLimit(customerId,cutl,cutBp) ;

    CPORT_PortalHomeController.AccountWrapper accountLimitInfo;

    if(e.ItCust != null){
      if(e.ItCust.item != null){
          for(SapCustomerLimitSoap.ZbapiCustLimit lt : e.ItCust.item){

              accountLimitInfo = new CPORT_PortalHomeController.AccountWrapper(null,
                (lt.Deposit != null ? Decimal.valueOf(lt.Deposit):0.0),
                (lt.Advance != null ? Decimal.valueOf(lt.Advance):0.0),
                (lt.Limitavailable != null ? Decimal.valueOf(lt.Limitavailable):0.0),
                (lt.Netoutstand != null ? Decimal.valueOf(lt.Netoutstand):0.0),
                (lt.Permisbleamt != null ? Decimal.valueOf(lt.Permisbleamt):0.0),
                (lt.Totoutstand != null ? Decimal.valueOf(lt.Totoutstand):0.0),
                (lt.Totoverdue != null ? Decimal.valueOf(lt.Totoverdue):0.0));
          }
      }
    }

      
      return accountLimitInfo;
    }


    /* Customer outstanding TL*/
    global class CustomerOutstandingTL{
        public Decimal amount_in_localCurrency {get;set;}
        public String  date_of_billing         {get;set;}
        public String billing_doc              {get;set;}
        public String billing_type             {get;set;}
        public String cheqNo                   {get;set;}
        public String chequeRecivedDate        {get;set;}
        public String discription              {get;set;} 
        public String dispatched_date          {get;set;}
        public String doc_date                 {get;set;}
        public String outstanding_days         {get;set;}
        public String territory_code           {get;set;}   

        public CustomerOutstandingTL(Decimal amt,String dob,String bdc,String btype,String cheqNo,String crd,String dis,String dispd,String doc_date,String out_d,String tcode){
          amount_in_localCurrency = amt;
          date_of_billing         = dob;
          billing_doc             = bdc;
          billing_type            = btype;
          cheqNo                  = cheqNo;
          chequeRecivedDate       = crd;
          discription             = dis;
          dispatched_date         = dispd;
          doc_date                = doc_date;
          outstanding_days        = out_d;
          territory_code          = tcode;
        }       
    }


       
 }