/**
 * This class contains unit tests for validating the behavior of Apex triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_TDS_Bulk_Uploader {
public static TDS__c tdsobj;
public static Account acc;
public static Contact con;
public static Attachment att;
public static User user;
public static smagicinteract__smsMagic__c smsObj;
public static testMethod void testmeth (){
acc = new Account (Name='Test Account',Pan_Number__c='0BI44LZTDF');
insert acc;
con = new Contact (LastName='Test Contact', Email='Test@gmail.com',AccountId=acc.Id);
insert con;
Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
user u = new User(Alias = 'portal', Email='portaluser@test.com',
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='portuser@test.com',contactId=con.Id);

tdsobj = new TDS__c (TDS_Account__c = acc.id);
insert tdsobj;
Attachment attch = new Attachment();
blob b= Blob.valueOf('test body');
attch.parentid=tdsobj.Id;
attch.body=b;
attch.name='0BI44LZTDF_Q1_2015-16.docx';
insert attch;
    /*smsObj = new smagicinteract__smsMagic__c();
    smsObj.smagicinteract__PhoneNumber__c = con.phone;
        smsObj.smagicinteract__SMSText__c = 'Test SMS for Bulk upload ';
        smsObj.smagicinteract__senderId__c = 'smsMagic';
        smsObj.smagicinteract__Name__c = con.name;
        Blob cryptoKey = Crypto.generateAesKey(256);  

    String dataToEncrypt =  smsObj.smagicinteract__PhoneNumber__c;                                  

    Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, Blob.valueOf(dataToEncrypt));
    smsObj.smagicinteract__external_field__c = String.valueof(encryptedData);
    insert smsobj;*/

}
}