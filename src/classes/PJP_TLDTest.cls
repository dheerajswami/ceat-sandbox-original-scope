@isTest
public class PJP_TLDTest {
    static testmethod void planTLD() {
        
        User rmUser = CEAT_InitializeTestData.createUser('gulati', 'sandeep', 'sandeep.gulati@ceat.dev', 'sandeep.gulati@ceat.dev', 'Emp2', 'Replacements', null, 'RM');
        insert rmUser;
        User tlUser = CEAT_InitializeTestData.createUser('Ram', 'Ronak', 'ram.ronak@ceat.dev', 'ram.ronak@ceat.dev', 'Emp1', 'Replacements', null, 'TLD');
        tlUser.ManagerId = rmUser.Id;
        insert tlUser;
        
        Territory2Model terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceat', 'Ceat');
        insert terrModel;
        /*
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');//[Select Id, DeveloperName from Territory2Type];
        
        Territory2 terrZone = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'ZC01' Or DeveloperName = 'ZC01')];//CEAT_InitializeTestData.createTerritory('ZC02', 'ZC02', listTerritoryType[0].Id, tModel.Id, null);        
        Territory2 terrRegion = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'BHP' Or DeveloperName = 'BHP') And ParentTerritory2Id = :terrZone.Id];//CEAT_InitializeTestData.createTerritory('Mum', 'Mum', listTerritoryType[0].Id, tModel.Id, terrZone.Id);        
        Territory2 terr = [Select Id, ParentTerritory2Id from Territory2 where (Name = 'B0144' Or DeveloperName = 'B0144') And ParentTerritory2Id = :terrRegion.Id];//CEAT_InitializeTestData.createTerritory('B001', 'B001', listTerritoryType[0].Id, tModel.Id, terrRegion.Id);        
        */
        
        Id terrTypeTerr;
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');        
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            
        }
        Territory2 terrZone = CEAT_InitializeTestData.createTerritory('ZS05', 'ZS05' , terrTypeTerr, terrModel.Id, null);
        insert terrZone;
        Territory2 terrRegion = CEAT_InitializeTestData.createTerritory('R5', 'R5' , terrTypeTerr, terrModel.Id, terrZone.Id);
        insert terrRegion;
        Territory2 terr = CEAT_InitializeTestData.createTerritory('T5', 'T5' , terrTypeTerr, terrModel.Id, terrRegion.Id);
        insert terr;
        
        UserTerritory2Association utAss = CEAT_InitializeTestData.createUserTerrAsso(tlUser.Id, terr.Id, 'TL');
        insert utAss;
                
        System.runAs(tlUser) {
            RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', 'C000000001');
            acc1.Sales_District_Text__c = 'T5';     
            acc1.Type = 'Analyst';
            acc1.RecordTypeId = rtDealer.Id;
            acc1.Active__c = true;
            insert acc1;
            
            DSE_Beat__c dseBeat = CEAT_InitializeTestData.createDseBeat('Beat-001', acc1.Id, acc1.KUNNR__c, acc1.Name, 'DSE-001', 'Abhishek', 'Emp1');            
            dseBeat.TLD_Territory_Code__c = 'T5';
            update dseBeat;
            Holiday__c h1 = CEAT_InitializeTestData.createHoliday('National', 'National', Date.today(), null, null, null, null, null);
            Holiday__c h2 = CEAT_InitializeTestData.createHoliday('Regional', 'Regional', Date.today(), 'ZC01', 'BHP', null, null, null);
            Holiday__c h3 = CEAT_InitializeTestData.createHoliday('Planned Leaves', 'Planned Leaves', null, null, null, Date.today(), Date.today().addDays(2), 'Emp1');
            List<Holiday__c> hList = new List<Holiday__c>();
            hList.add(h1);
            hList.add(h2);
            hList.add(h3);
            insert hList;
            
            PJP_Norms_Main__c pnm = CEAT_InitializeTestData.createPjpNorm(true, true, true, true, 3, 2, 3, 10, null, 'TLD', 'Replacement');
            PJP_Segment_Norm__c psn = CEAT_InitializeTestData.createSegmentNorm(pnm.Id, 'Analyst', 4);
            PJP_Daily_Norms__c pdn = CEAT_InitializeTestData.createDailyNorm(pnm.Id, 'Dealer', 6);
            
            Integer mm = Date.today().month();
            Integer yy = Date.today().year();
            String mon = PJP_TLDClass.calculateMonth(mm);
            
            ACW__c currentAcw = CEAT_InitializeTestData.createACW(Date.today(), Date.today(), mon, 'PJP Replacement');
            
            PJP_TLDClass mvp = new PJP_TLDClass();
            PJP_TLDClass.getWriteAccess(null, null, null);            
            PJP_TLDClass.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            PJP_TLDClass.getRelatedDseBeats(2014, 12, 'December', String.valueOf(UserInfo.getUserId()));
            
            Visits_Plan__c vp1 = new Visits_Plan__c();
            vp1.Day__c = '1';
            vp1.Type_of_Day__c = 'OE';
            vp1.Id = null;
            Visits_Plan__c vp2 = new Visits_Plan__c();
            vp2.Day__c = '2';
            vp2.Type_of_Day__c = 'MOR';
            vp2.Id = null;
            Visits_Plan__c vp3 = new Visits_Plan__c();
            vp3.Day__c = '3';
            vp3.Type_of_Day__c = 'Company Activity';
            vp3.Id = null;
            Visits_Plan__c vp4 = new Visits_Plan__c();
            vp4.Day__c = '4';
            vp4.Type_of_Day__c = 'Influencer Visit';
            vp4.Id = null;
            Visits_Plan__c vp5 = new Visits_Plan__c();
            vp5.Dealer__c = acc1.Id;
            vp5.Day__c = '5';
            vp5.Type_of_Day__c = 'Visit Day';
            vp5.Id = null;//String.valueOf(acc1.Id);
            
            List<Visits_Plan__c> vpList = new List<Visits_Plan__c>();
            vpList.add(vp1);
            vpList.add(vp2);
            vpList.add(vp3);
            vpList.add(vp4);
            vpList.add(vp5);               
                                    
            PJP_TLDClass.saveVisitPlans(vpList, yy, mm, mon, 'Submit', 'Comments');
            
            PJP_TLDClass.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            
            ApexPages.currentPage().getParameters().put('month', mon);
            ApexPages.currentPage().getParameters().put('year', String.valueOf(yy));
            ApexPages.currentPage().getParameters().put('owner', tlUser.Id);
            mvp = new PJP_TLDClass();
            PJP_TLDClass.getRelatedDseBeats(yy, mm, mon, String.valueOf(UserInfo.getUserId()));
            vpList = [Select Id, Dealer__c, Day__c, Type_of_Day__c from Visits_Plan__c];//new List<Visits_Plan__c>();
            PJP_TLDClass.saveVisitPlans(vpList, yy, mm, mon, 'Submit', 'New Comments');
            
            for(Integer i=0; i<=12; i++) {
                String m = PJP_TLDClass.calculateMonth(i);            
            }
        }           
    }
}