//Author :PALLAVI
@isTest(SeeAllData=False)

 public class preComplaintTest {
  public static testMethod void CreateComplaint_testclass(){
       
    
        case createCase = new case();
        createCase.Name_of_complainant__c='Test';
        createCase.Mobile_of_complainant__c='9044263263';
        createCase.Comment__c='shjakgdjkD';
        createCase.SAP_Reference_Number__c='SP102';
        insert createCase;
      
    PreCompaint pr = new PreCompaint();
    PreCompaint.createPreCompaint(createCase.Name_of_complainant__c,createCase.Mobile_of_complainant__c,createCase.Comment__c,'(SP102)','');
    }
    public static testMethod void testSoslFixedResult(){
        case ca = new case();
        ca.Name_of_complainant__c='Test';
        ca.Mobile_of_complainant__c='9044263263';
        ca.Comment__c='shjakgdjkD';
        ca.SAP_Reference_Number__c='SP103';
        insert ca;
        
        account acc = new account();
        acc.name='test';
        acc.type='Replacement';
        insert acc;
        
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = acc.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        PreCompaint.createPreCompaint(ca.Name_of_complainant__c,ca.Mobile_of_complainant__c,ca.Comment__c,ca.SAP_Reference_Number__c,acc.id);
       
    }
    public static testMethod void checkComps(){
        
        account acc = new account();
        acc.name='test';
        acc.type='OE Plant';
        acc.AD_TLNMBR__c = '9044263263';
        insert acc;
        
        case newcase = new case();
        newcase.Name_of_complainant__c='Test';
        newcase.Mobile_of_complainant__c='9044263263'; 
        newcase.Comment__c='shjakgdjkD';
        insert newcase;
         
         
         PreCompaint.createPreCompaint(newcase.Name_of_complainant__c,newcase.Mobile_of_complainant__c,newcase.Comment__c,'','');
   
    } 
    
     // Covering the else block Line 119
    public static testMethod void checkComps2(){
        
        account acc = new account();
        acc.name='test';
        acc.type='Replacement';
        acc.AD_TLNMBR__c = '9044263213';
        insert acc;
        
        case newcase = new case();
        newcase.Name_of_complainant__c='Test';
        newcase.Mobile_of_complainant__c='9044263263'; 
        newcase.Comment__c='shjakgdjkD';
        insert newcase;
         
         
         PreCompaint.createPreCompaint(newcase.Name_of_complainant__c,newcase.Mobile_of_complainant__c,newcase.Comment__c,'',''); 
   
    } 
    
    // Covering the else block Line 144
    public static testMethod void checkComps1(){
        
        account acc = new account();
        acc.name='test';
        acc.type='Replacement';
        acc.AD_TLNMBR__c = '9044263263';
        insert acc;
        
        case newcase = new case();
        newcase.Name_of_complainant__c='Test';
        newcase.Mobile_of_complainant__c='9044263263'; 
        newcase.Comment__c='shjakgdjkD';
        insert newcase;
         
         
         PreCompaint.createPreCompaint(newcase.Name_of_complainant__c,newcase.Mobile_of_complainant__c,newcase.Comment__c,'',acc.id);
   
    } 
    
    
    
    
    public static testMethod  void CreateCompls(){
    Account acc = new Account();
    acc.Name='testAccount';
    acc.type='Customer';
   
    test.startTest();
    Insert acc;
    PreCompaint.searchAccounts(acc.type);
    test.stopTest();
     
      System.assertequals('Customer',[SELECT Type FROM Account WHERE ID =:acc.ID].type);
    }
 }