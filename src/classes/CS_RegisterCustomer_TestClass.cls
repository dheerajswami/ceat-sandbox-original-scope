@isTest(seeAllData = false)

public class CS_RegisterCustomer_TestClass {

    static Account ceatShoppe;
    static Contact shoppeContact;
    static User csUser;
    static CS_Customer__c cust1,cust2;
    
    public static void init(){
        ceatShoppe = CEAT_InitializeTestData.createAccount('Shoppe1','890');
        ceatShoppe.District__c ='Mumbai';
        database.insert(ceatShoppe);
        
        shoppeContact = CEAT_InitializeTestData.createContact('owner 1',ceatShoppe.Id);
        database.insert(shoppeContact);
        
        csUser = CEAT_InitializeTestData.createPortalUser(shoppeContact.Id);
        database.insert(csUser);
            
    }
    
    public static testMethod void registerNewAndExistingCustomer(){
        
        init();
        State_Master__c stm = new State_Master__c();
        stm.State__c = 'Kerala';
        stm.District__c = 'Amanganj';
        stm.Town__c = 'Amanganj';        
        insert stm;
        Test.startTest();
          System.runAs(csUser) {
             // The following code runs as user 'u' 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId()); 
             PageReference pg = Page.CS_RegisterCustomer;
            Test.setCurrentPage(pg);
            cust1 = new CS_Customer__c();
            ApexPages.StandardController sc = new ApexPages.standardController(cust1);
            CS_RegisterCustomer_Controller ctrl = new CS_RegisterCustomer_Controller(sc);
            
            //enter a mobile number and search for a customer
            ctrl.mobileNo = '9009110018';
            ctrl.search();
            system.assertEquals(ctrl.csCustomer.Name,null);//non existing customer
            //system.assertEquals(ctrl.csCustomer.District__c,'Mumbai');//deafults value to account's district
            
            // enter details about this new customer and try to submit such that validation error happens 
            ctrl.csCustomer.Name = 'cs customer';
            ctrl.csCustomer.Preferred_Communication_Method__c = 'Email';
            ctrl.csCustomer.Email__c = 'developer@extentor.com';
            ctrl.csCustomer.State__c = 'Kerala';
            List<SelectOption> town  = CS_RegisterCustomer_Controller.getTowns(ctrl.csCustomer.State__c);
            CS_RegisterCustomer_Controller.submitCustomer( ctrl.csCustomer);// throws validation error on page
            //System.assertNotequals(null,ApexPages.GetMessages().get(0).getSummary());
            
            //ctrl.csCustomer.Preferred_Communication_Method__c = 'Mobile';
            //CS_RegisterCustomer_Controller.submitCustomer(String.valueOf(System.today()+1), String.valueOf(System.today()+1), ctrl.csCustomer);//creates a CS Customer record
            
            //system.assertNotEquals(ctrl.csCustomer.Id,null);
            
            //----------------------------------------------------------
            //register existing customer
            cust2 = new CS_Customer__c();
            ApexPages.StandardController sc1 = new ApexPages.standardController(cust2);
            CS_RegisterCustomer_Controller ctrl1 = new CS_RegisterCustomer_Controller(sc1);
            ctrl1.mobileNo = '9009110018';
            ctrl1.search();
            system.assertEquals(ctrl1.csCustomer.Name,'cs customer');//existing customer
            ctrl1.submit();//updates existing CS Customer record
            //ctrl1.registeredvehicles();
             ctrl1.newendcustomervehicle();
            ctrl1.editVehiclemobile();
            ctrl1.editVehicle();
            ctrl1.savenewvehicle();
           // ctrl1.deleteVehicle();
           
            
          }
       
        Test.stopTest();
    }
    
    public static testMethod void savevehicle()
    {
        CS_Customer__c cs = new CS_Customer__c();
        cs.Mobile__c = '8125748526';
        cs.name='praveen';
        insert cs;
        CS_Vehicle__c csve = new CS_Vehicle__c();
        csve.CS_Customer__c = cs.id;
        csve.Makes__c= 'Ferrari';
        csve.Models__c = 'FR4';
        csve.Avg_Monthly_Running__c = 350;
        csve.Last_Alignment__c = system.today();
        csve.Last_Balancing_Change__c = system.today();
        insert csve;
      
           
            
        
    }
    
    public static testMethod void validateCancel(){
        
        init();
        Test.startTest();
          System.runAs(csUser) {
             // The following code runs as user 'u' 
             System.debug('Current User: ' + UserInfo.getUserName());
             System.debug('Current Profile: ' + UserInfo.getProfileId()); 
             PageReference pg = Page.CS_RegisterCustomer;
            Test.setCurrentPage(pg);
            cust1 = new CS_Customer__c();
            ApexPages.StandardController sc = new ApexPages.standardController(cust1);
            CS_RegisterCustomer_Controller ctrl = new CS_RegisterCustomer_Controller(sc);
            
            //enter a mobile number and search for a customer
            ctrl.mobileNo = '9009110018';
            ctrl.cancel();
          }
       
        Test.stopTest();
    }
}