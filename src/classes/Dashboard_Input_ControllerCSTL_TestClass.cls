/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Dashboard_Input_ControllerCSTL_TestClass {
    static list<Id> terrTypeID                            = new list<Id>();
    static list<Id> terrId                                = new list<Id>();
    
    static testMethod void myUnitTest() {
        
        string CSTLInputRecType         = system.Label.CSTL_RecType;
        map<String,Id> wightageMap      = new map<String,Id>();
        
        Id CSTLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName ='CSTL' Limit 1].Id;
        Id summaryInputRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName ='CSTL_Input_Score' Limit 1].Id;
            
        //list<sObject> dwmAl      = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'DashboardMasterInputCSTL');
        integer monthNumber = date.today().month();
        String month = (monthNumber == 1)?'January':(monthNumber == 2)?'February':(monthNumber == 3)?'March':(monthNumber == 4)?'April':(monthNumber == 5)?'May':(monthNumber == 6)?'June':(monthNumber == 7)?'July':(monthNumber == 8)?'August':(monthNumber == 9)?'September':(monthNumber == 10)?'October' : (monthNumber == 11)?'November' : 'December';
        
        
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
       
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE RoleInTerritory2='CSTL' limit 1];    
        //list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND id =:];
        
        list<string> terrNames = new list<string>();
        string loggedInUserTerritory ='';
        
         list<UserTerritory2Association> userTerrCode=[select territory2Id,Territory2.name,id from UserTerritory2Association where UserId=:userTerrAssList[0].UserId AND RoleInTerritory2='CSTL'];  
        
	     if(userTerrCode.size() > 0){
	        	   terrNames = new list<string>();
	        	   for(UserTerritory2Association terr : userTerrCode){
	        	   		terrNames.add(terr.Territory2.name);
	        	   }
	        	    terrNames.sort();
	     }
         if(terrNames.size() > 0){
	               for(string terr : terrNames){
	                    if(loggedInUserTerritory =='' || loggedInUserTerritory ==null){
	                        loggedInUserTerritory = terr;
	                    }else{
	                        loggedInUserTerritory = loggedInUserTerritory +','+ terr ;
	                    }
	              }
         }
	    
        User u = [Select id, name from User where id = :userTerrAssList[0].UserId limit 1];
        
        
        //Insert Dashboard Master
         Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true);
         insert dm;
        
        Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();
        dwmTemp.Dashboard_Master__c = dm.id;
        dwmTemp.role__c = 'CSTL';
        dwmTemp.Weightage__c = 0;
        
        insert dwmTemp; 
        
        Dashboard_Summary_Score__c dbSummaryScore = new Dashboard_Summary_Score__c();
        dbSummaryScore.Territory_Code__c = loggedInUserTerritory;
        dbSummaryScore.Month__c = string.valueof(monthNumber);
        dbSummaryScore.Year__c = string.valueof(date.today().year());
        dbSummaryScore.Input_Score__c = 20;
        dbSummaryScore.RecordTypeId = summaryInputRecTypeId;
        dbSummaryScore.ownerId = u.id;
        insert dbSummaryScore;
        
       // list<sObject> InputScore     = Test.loadData(Dashboard_Input_Score__c.sObjectType, 'DashboardInputScoreCSTL');
        //list<sObject> InputSummaryScore     = Test.loadData(Dashboard_Summary_Score__c.sObjectType, 'DashboardSummaryScore');
        
        //for(sObject inps : InputSummaryScore){
            Dashboard_Input_Score__c inpScore = new Dashboard_Input_Score__c();
            inpScore.RecordTypeId = CSTLInputRecTypeId;
            inpScore.Month__c = month;
            inpScore.year__c = string.valueof(date.today().year());
            inpScore.Dashboard_Weightage_Master__c=dwmTemp.id;
            inpScore.Territory_Code__c=loggedInUserTerritory;
            inpScore.Parameters__c='Number of P1 customers met';
            inpScore.PJP__c=true;
            inpScore.Category__c='PJP adherance';
            inpScore.Total_Monthly_Target__c=0;
            inpScore.Total_MTD_Target__c=0;
            inpScore.Total_Actual_MTD__c=0;
            inpScore.Achievement_MTD__c=0;
            inpScore.Score__c=0;
            inpScore.User__c = u.id;
            inpScore.OwnerId = u.id;
            
       // }
        insert inpScore;
        
       
        //Dashboard_Input_ControllerCSTL dbTl = new Dashboard_Input_ControllerCSTL();
        //DashboardInputControllerHandlerCSTL.dashboardWrapper  res = Dashboard_Input_ControllerCSTL.getCstlInputDashboardData();
        
        system.runAs(u){
        	
        
            Dashboard_Summary_Score__c dbSummaryScore1 = new Dashboard_Summary_Score__c();
            dbSummaryScore1.Territory_Code__c = loggedInUserTerritory;
            dbSummaryScore1.Month__c = string.valueof(date.today().month());
            dbSummaryScore1.Year__c = string.valueof(date.today().year());
            dbSummaryScore1.Input_Score__c = 20;
            dbSummaryScore1.RecordTypeId = summaryInputRecTypeId;
            dbSummaryScore1.OwnerId = u.Id;
            insert dbSummaryScore1;
            
            ApexPages.currentPage().getParameters().put('Id',u.Id);
			ApexPages.currentPage().getParameters().put('month',string.valueof(date.today().month()));
			ApexPages.currentPage().getParameters().put('year',string.valueof(date.today().year()));
			
            Dashboard_Input_ControllerCSTL dbTl1 = new Dashboard_Input_ControllerCSTL();
            DashboardInputControllerHandlerCSTL.dashboardWrapper  res1 = Dashboard_Input_ControllerCSTL.getCstlInputDashboardData(u.Id,string.valueof(date.today().month()),string.valueof(date.today().year()));
        
        }
    }
                
    
}