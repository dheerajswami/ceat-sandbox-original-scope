global class Sap_Sfdc_Customer_Parent_Child_Mapping{

  /*
* Author  :- Supriya Chakrapani 
* Purpose :- Mapping class for Sap_Sfdc_Customer_Parent_Child
* Date    :- 27/Oct/2015
*
*/
    
    //-- ATTRIBUTES    
         public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    //-- CONSTRUCTOR
    
    //-- Methods
         WebService static List<Sfdc_Customer_Parent_Child_Mapping> getAllCustomerParentChild(String custID){
     
     
         String customerId                   = UtilityClass.addleadingZeros(custID,Integer.valueOf(CUST_ID_LENGTH));
      
         List<Sfdc_Customer_Parent_Child_Mapping> custParchild = new List<Sfdc_Customer_Parent_Child_Mapping>();        
            try{
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            Sap_Sfdc_Customer_Parent_Child tst = new Sap_Sfdc_Customer_Parent_Child();
            Sap_Sfdc_Customer_Parent_Child.zws_sfdc_customer_parent_child zws = new Sap_Sfdc_Customer_Parent_Child.zws_sfdc_customer_parent_child(); 
             zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 120000;
            
            Sap_Sfdc_Customer_Parent_Child.TABLE_OF_ZSFDC_CUST_PRE_CH  tab = new Sap_Sfdc_Customer_Parent_Child.TABLE_OF_ZSFDC_CUST_PRE_CH();
            Sap_Sfdc_Customer_Parent_Child.TABLE_OF_ZSFDC_CUST_PRE_CH e = new Sap_Sfdc_Customer_Parent_Child.TABLE_OF_ZSFDC_CUST_PRE_CH();
            List<Sap_Sfdc_Customer_Parent_Child.ZSFDC_CUST_PRE_CH > m = new List<Sap_Sfdc_Customer_Parent_Child.ZSFDC_CUST_PRE_CH >();
            
            tab.item = m;
            
            e=zws.ZSFDC_CUSTOMER_PARENT_CHILD(tab,customerId);
            system.debug(e+'dddddd');
             if(e.item != null){
               for(Sap_Sfdc_Customer_Parent_Child.ZSFDC_CUST_PRE_CH z : e.item){
                    custParchild.add(new Sfdc_Customer_Parent_Child_Mapping(z.MARKET,z.PARENT_CODE,z.CHILD_CODE));
                }
            }
             
            system.debug('custParchild----------------'+custParchild.size());        
            return custParchild;
            }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
            }
         }   
            global class Sfdc_Customer_Parent_Child_Mapping{       
        
            public String MARKET;
            public String PARENT_CODE;
            public String CHILD_CODE;
            
            
            
             public Sfdc_Customer_Parent_Child_Mapping(String MARKET, String PARENT_CODE, String CHILD_CODE){
            this.MARKET= MARKET;
            this.PARENT_CODE= PARENT_CODE;             
            this.CHILD_CODE= CHILD_CODE; 
        }
       } 
     }