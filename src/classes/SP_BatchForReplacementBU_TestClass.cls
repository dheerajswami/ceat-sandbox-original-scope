@isTest 
private class SP_BatchForReplacementBU_TestClass{
    static testmethod void testLoadData() {
        String repDealerLabel           = System.Label.Replacement_Dealer;
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
        Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        // Load the test Sales planning Staging from the static resource
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatReplacement');
        List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataR');
        List<sObject> listReplacementDealerData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'Staging_Replacement');
        for(sObject li : listReplacementDealerData){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listReplacementDealerData;
        List<sObject> listSPData = Test.loadData(Sales_Planning__c.sObjectType, 'SP_Replacement_Dealer');
        for(sObject li : listSPData){
            Sales_Planning__c temp = (Sales_Planning__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            temp.RecordTypeId = replaceDealerId;
        }
        update listSPData;
        SP_BatchForReplacementBUUpdated nn = new SP_BatchForReplacementBUUpdated();
        database.executebatch(nn,200); 
    }
}