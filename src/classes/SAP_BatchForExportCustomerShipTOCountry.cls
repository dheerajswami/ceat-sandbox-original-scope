global class SAP_BatchForExportCustomerShipTOCountry implements Database.Batchable<sObject>,Database.AllowsCallouts {    
    /* ================================================
    @Name:  SAP_BatchForExportCustomerShipTOCountry
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will update ship-t0-country code of Export Customer on daily basis                                                                                            
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     30/06/2015      INITIAL DEVELOPMENT                                 
   
    @=======================================================  */
    id exportRecTypeId;
    map<String,Set<String>> mapofCustShipToCountry       = new Map<String,Set<String>>();
    set<String> setOfAccounts                   = new set<String>();
    //map<String, Account> mapOfAccounts               = new map<String, Account>();
    map<String, String> mapOfAccountsAndParent      = new map<String, String>();
    list<Account> updatelistofCustomer              = new list<Account>();
    list<Account> newRecords                        = new list<Account>();
    list<Account> updateChild                       = new list<Account>();
    set<String> setOFCountry;
    set<String> setOFCountrytemp;
    Account dealer;
    global SAP_BatchForExportCustomerShipTOCountry (){
        
        exportRecTypeId = UtilityClass.getRecordTypeId('Account','Exports'); 
   }
   global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        query                                            = 'SELECT id,Name,KUNNR__c,Ship_to_Country__c from Account WHERE (CreatedDate = TODAY  OR LastModifiedDate = TODAY ) AND KUNNR__c != null '; 
        //RecordTypeId =: exportRecTypeId AND
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            for(sObject temp : scope){
                Account acc                                = (Account)temp;
                sap_zws_cust_shtp  ws = new sap_zws_cust_shtp ();
                sap_zws_cust_shtp.ZWS_CUST_SHTP  zws = new sap_zws_cust_shtp.ZWS_CUST_SHTP();
                zws.inputHttpHeaders_x = new Map<String,String>();
                zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
                zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
                zws.timeout_x = 35000;
                  
                sap_zws_cust_shtp.TableOfZsdCustShtp tom2 = new sap_zws_cust_shtp.TableOfZsdCustShtp();     

                sap_zws_cust_shtp.TableOfZsdCustShtp elem = new sap_zws_cust_shtp.TableOfZsdCustShtp(); 
                if(acc.KUNNR__c != null){
                    String cusNum = UtilityClass.addleadingZeros(acc.KUNNR__c,10);
                    system.debug(cusNum+'cusNum'); 
                    elem = zws.ZsdCustShtp(tom2,cusNum);
                    system.debug(elem+'elem'); 
                    setOFCountry = new set<String>();
                    if(elem.item != null){

                        for(sap_zws_cust_shtp.ZsdCustShtp z : elem.item){
                            if(z.Kunn2.startsWith('00'))
                                z.Kunn2                       = z.Kunn2.subString(2);
                                setOFCountry.add(z.Land1);
                                mapofCustShipToCountry.put(acc.KUNNR__c,setOFCountry);
                                if(string.valueOf(integer.valueOf(z.Kunn2)) != acc.KUNNR__c){
                                    setOfAccounts.add(string.valueOf(integer.valueOf(z.Kunn2)));
                                    mapOfAccountsAndParent.put(string.valueOf(integer.valueOf(z.Kunn2)),acc.KUNNR__c);
                                }
                                
                        }
                        
                    }
                }
            }
            List<Account> Acct = new List<Account>();
            Acct = [Select ID,KUNNR__c from Account where  KUNNR__c IN:setOfAccounts ];
            system.debug(Acct.size()+'Acct');
            system.debug(setOfAccounts+'setOfAccounts');
            system.debug(setOfAccounts.size()+'setOfAccounts');
            for( Account a : Acct){
               
                if(mapOfAccountsAndParent.containsKey(a.KUNNR__c)){
                    dealer = new account(KUNNR__c=mapOfAccountsAndParent.get(a.KUNNR__c));
                    a.Parent = dealer;
                    updateChild.add(a);
                }
            }     
            system.debug(updateChild+'updateChild');
            system.debug(updateChild.size()+'updateChild');
            if(updateChild.size() > 0){
                upsert updateChild KUNNR__c;
            }    
              
                    
            
            if(mapofCustShipToCountry.size() > 0){

                for(sObject temp : scope){
                    Account acc                                = (Account)temp;
                    if(mapofCustShipToCountry.containsKey(acc.KUNNR__c)){
                        setOFCountrytemp = new set<String>();
                        setOFCountrytemp                  = mapofCustShipToCountry.get(acc.KUNNR__c);
                        String counCode = '';
                        for(String str : setOFCountrytemp){
                            if(counCode == ''){
                                counCode =  str;
                            }else{
                                counCode = counCode +';'+ str;
                            }  
                        }
                        acc.Ship_to_Country__c            = counCode;
                        
                        updatelistofCustomer.add(acc);
                        
                         
                        
                    }
                }
        }
        
        
        
        system.debug(updatelistofCustomer+'updatelistofCustomer');
        system.debug(mapofCustShipToCountry+'mapofCustShipToCountry');
        if(updatelistofCustomer.size() > 0){
                upsert updatelistofCustomer KUNNR__c;
        }  
    }
    global void finish(Database.BatchableContext BC)
    {       
        // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: Dashboard_BatchForDashboardOutputRO Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SAP_BatchForExportCustomerShipTOCountry" ' + a.TotalJobItems +'/n'+ ' batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n');  
           
            if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
            }        
            
    }
}