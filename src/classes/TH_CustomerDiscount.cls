public with sharing class TH_CustomerDiscount {
    
    /*
     * Auther :- Vivek Deepak   
     * Purpose :- DEcription of class Demo by Vivek
     *
     *
     *
     */
     
     //-- SINGLETON PATTERN
      private static TH_CustomerDiscount instance;
      public static TH_CustomerDiscount getInstance() {
          if (instance == null) instance = new TH_CustomerDiscount();
          return instance;
      }
      
     // -- ATTRIBUTES    
     
     //-- CONSTRUCTOR 

     
    
     //-- METHODS 
     
    
    //-- Before Insert
    
      
     public void onBeforeInsert(final List<Customer_Discounts__c> newObjects){
     
        updateCustomerId(newObjects);
     }
     
    
     //-- Before Update
     
      
     public void onBeforeUpdate(final List<Customer_Discounts__c> newObjects,final List<Customer_Discounts__c> oldObjects,final Map<Id, Customer_Discounts__c> newObjectsMap,final Map<Id, Customer_Discounts__c> oldObjectsMap){
     
        updateCustomerId(newObjects);
     }
     
     
     /*
      Mehthod is add customer id 
     */
     public void updateCustomerId(List<Customer_Discounts__c> newObjects){
        
        Set<String> sapNumbers = new Set<String>();
        
        for(Customer_Discounts__c csd : newObjects){
            sapNumbers.add(csd.Customer_Number__c);
        }
        
        Map<String,String> accountIds = new Map<String,String>();
        
        for(Account acc :[SELECT Id,UniqueIdentifier__c FROM Account WHERE UniqueIdentifier__c IN :sapNumbers]){
            accountIds.put(acc.UniqueIdentifier__c,acc.Id);
        }
        
        
        for(Customer_Discounts__c csd : newObjects){
            if(csd.Account__c == null){
                csd.Account__c = accountIds.get(csd.Customer_Number__c);
            } 
        }
        
        
     }
    

}