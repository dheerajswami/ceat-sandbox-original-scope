/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class MB_CMU_Data_TestClass {
	private static Account account;
	private static List<Account> accounts;
	private static List<Customer_Location__c> custLocations;
	private static List<Contact> contacts;
	
	public static void initializeTestData(){
		        
		        String tempNumber = String.valueOf(Integer.valueOf(Math.random()*1000));
		        
		        Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
		        database.upsert(counterL);
		        
		        account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
		        database.insert(account);

		        accounts = new List<Account>();
		        accounts.add(account);

		        custLocations = new List<Customer_Location__c>();
		        contacts = new List<Contact>();

		        for(Integer i = 0;i < 5;i++){
		        	contacts.add(new Contact(AccountId = account.Id,FirstName = 'TEST'+String.valueOf(i),LastName='Last'+String.valueOf(i)));

		        	custLocations.add(new Customer_Location__c(Customer__c = account.Id,Location__latitude__s = 12.3445, Location__longitude__s = 77.5657));
		        }	
	}
	
	public static void initializeTestDataException(){
		        
		        String tempNumber = String.valueOf(Integer.valueOf(Math.random()*1000));
		        
		        Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
		        database.upsert(counterL);
		        
		        account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
		        database.insert(account);

		        accounts = new List<Account>();
		        accounts.add(account);

		        custLocations = new List<Customer_Location__c>();
		        contacts = new List<Contact>();

		        for(Integer i = 0;i < 5;i++){
		        	contacts.add(new Contact(AccountId = account.Id,FirstName = 'TEST'+String.valueOf(i),LastName='Last'+String.valueOf(i)));

		        	custLocations.add(new Customer_Location__c(Location__latitude__s = 12.3445, Location__longitude__s = 77.5657));
		        }	
	}

	static testMethod void myUnitTest() {
    	Test.startTest();
        initializeTestData();
    	MB_CMU_Data.MobileData mobData = new MB_CMU_Data.MobileData();
    		mobData.accounts.addAll(accounts);
    		mobData.custLocation.addAll(custLocations);
    		mobData.contacts.addAll(contacts);

    		String josn = JSON.serialize(mobData);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',null);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCMUData'; 
		        RestContext.request.httpMethod = 'POST';

		        MB_CMU_Data.getAccountListAndUpdate();
    	Test.stopTest();

    }
    
    static testMethod void myUnitTestWithTimeStamp() {
    	Test.startTest();
        initializeTestData();
    	MB_CMU_Data.MobileData mobData = new MB_CMU_Data.MobileData();
    		mobData.accounts.addAll(accounts);
    		mobData.custLocation.addAll(custLocations);
    		mobData.contacts.addAll(contacts);

    		String josn = JSON.serialize(mobData);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCMUData'; 
		        RestContext.request.httpMethod = 'POST';

		        MB_CMU_Data.getAccountListAndUpdate();
    	Test.stopTest();

    }
    
    	static testMethod void myUnitTest2() {
    	Test.startTest();
        initializeTestDataException();
    	MB_CMU_Data.MobileData mobData = new MB_CMU_Data.MobileData();
    		mobData.accounts.addAll(accounts);
    		mobData.custLocation.addAll(custLocations);
    		mobData.contacts.addAll(contacts);

    		String josn = JSON.serialize(mobData);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        RestContext.request.addParameter('TimeStamp',null);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCMUData'; 
		        RestContext.request.httpMethod = 'POST';

		        MB_CMU_Data.getAccountListAndUpdate();
    	Test.stopTest();

    }
    
    static testMethod void myUnitTestWithTimeStamp2() {
    	Test.startTest();
        initializeTestDataException();
    	MB_CMU_Data.MobileData mobData = new MB_CMU_Data.MobileData();
    		mobData.accounts.addAll(accounts);
    		mobData.custLocation.addAll(custLocations);
    		mobData.contacts.addAll(contacts);

    		String josn = JSON.serialize(mobData);
		    	
		    	system.RestContext.request = new RestRequest();
		        RestContext.request.requestBody = Blob.valueOf(josn);
		        String temp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
		        RestContext.request.addParameter('TimeStamp',temp);
		      
		        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchCMUData'; 
		        RestContext.request.httpMethod = 'POST';

		        MB_CMU_Data.getAccountListAndUpdate();
    	Test.stopTest();

    }
	 
}