global class ScheduledBatch_DashboardInputRM implements Schedulable{
   global void execute(SchedulableContext sc) {
      DashboardInputRM_BatchClass dashboardRMBatch = new DashboardInputRM_BatchClass(); 
      database.executebatch(dashboardRMBatch ,1);
   }
}