public class PjpTriggerHandler {
    public static void postFeedOnChatter(List<PJP__c> newPjps, List<PJP__c> oldPjps) {
        FeedItem fitem;
        List<FeedItem> feedItems = new List<FeedItem>();
        for(PJP__c newPjp : newPjps) {
        	for(PJP__c oldPjp : oldPjps) {
                if(newPjp.Id == oldPjp.Id && ((newPjp.Sys_ApprovalStatus__c == 'Approved' && oldPjp.Sys_ApprovalStatus__c != 'Approved') || (newPjp.Sys_ApprovalStatus__c == 'Rejected' && oldPjp.Sys_ApprovalStatus__c != 'Rejected'))) {
                    fitem=new FeedItem();
                    fItem.parentId=newPjp.OwnerId;
                    fItem.Title='Click here to view the PJP';
                    if(newPjp.Sys_ApprovalStatus__c == 'Approved' && oldPjp.Sys_ApprovalStatus__c != 'Approved') {                        
                        fItem.body = 'PJP has been approved for the month '+newPjp.Visit_Plan_Month__c+' by '+UserInfo.getUserName()+'. ';                        
                    }
                    if(newPjp.Sys_ApprovalStatus__c == 'Rejected' && oldPjp.Sys_ApprovalStatus__c != 'Rejected') {
                        fItem.body = 'PJP has been rejected for the month '+newPjp.Visit_Plan_Month__c+' by '+UserInfo.getUserName()+'. ';
                    }
                    fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newPjp.Id;
                    insert fItem;
                }
            }    
        }
    }
}