global class CPORT_CustomerOutstandingAndLimit {

    /*
     * Author  :- Vivek Deepak  
     * Purpose :- Fetch details of Customer Details and Customer Limits 
     *            
     * Version :- V2
     * Changes :- extra columns added in web service for currency
     * By      :- Supriya
     * Date    :- 1/10/2015
     */
     
     //-- ATTRIBUTES
     
     public static final String CUST_ID_LENGTH  = Label.CPORT_LengthOfCustomerId;
     
     //-- CONTRUCTOR
     
     //-- METHODS
     
     /*
     This method will return list of CN/DN details
     */
     WebService static List<CustomerOutStandingMapping> getAllCustomerOutstandingDetails(String cusNum){

        List<CustomerOutStandingMapping> customerOut = new List<CustomerOutStandingMapping>();

        try{

        String customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
        
        SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        SapCustomerOutstandingSoapV4 tst                             = new SapCustomerOutstandingSoapV4();
        SapCustomerOutstandingSoapV4.ZWS_CUST_OUTSTANDING   zws       = new SapCustomerOutstandingSoapV4.ZWS_CUST_OUTSTANDING  ();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;
        SapCustomerOutstandingSoapV4.TableOfZbapiCustOs   tom             = new SapCustomerOutstandingSoapV4.TableOfZbapiCustOs ();
              
                
        List<SapCustomerOutstandingSoapV4.ZbapiCustOs> m                  = new List<SapCustomerOutstandingSoapV4.ZbapiCustOs>();        
        
        tom.item = m;
        
        SapCustomerOutstandingSoapV4.TableOfZbapiCustOs e = new SapCustomerOutstandingSoapV4.TableOfZbapiCustOs();
        
        e = zws.ZbapiCustOutstanding(tom, customerId);
        
        if(e.item != null){            
            for(SapCustomerOutstandingSoapV4.ZbapiCustOs z : e.item) {
                customerOut.add(new CustomerOutStandingMapping(z.Fkart,z.Vtext,z.Vbeln,z.Fkdat,z.Despda,z.OutstandAmt,z.Duedate,z.OsDays,z.Chequeno,z.Chequedate,z.Curr,z.AmtDoccur,z.LocCurrcy));
            }
        }
            
            return customerOut;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        return null;
     }


    /*
    * Fetch customer Limits
    *   
    */
    WebService static CustomerLimitMapping getAllCustomerLimitsDetails(String cusNum){

        CustomerLimitMapping customerLimitMapping = new CustomerLimitMapping();

        try{
            String customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
              
            SapCustomerLimitSoap  tst                           = new SapCustomerLimitSoap();
            SapCustomerLimitSoap.ZWS_GET_CUST_LIMIT   zws       = new SapCustomerLimitSoap.ZWS_GET_CUST_LIMIT();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
        
            SapCustomerLimitSoap.TableOfBapireturn tbapi        = new SapCustomerLimitSoap.TableOfBapireturn();
            SapCustomerLimitSoap.TableOfZbapiCustLimit tCust    = new SapCustomerLimitSoap.TableOfZbapiCustLimit();

            SapCustomerLimitSoap.ZbapiCustLimit zCl                         = new SapCustomerLimitSoap.ZbapiCustLimit();
            //SapCustomerLimitSoap.ZbapiGetCustLimit_element zEl    = new SapCustomerLimitSoap.ZbapiGetCustLimit_element();
            SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element  zEl     = new SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element ();
        
            zEl = zws.ZbapiGetCustLimit(customerId,tCust,tbapi);

            List<CustomerBapiReturn> custB = new List<CustomerBapiReturn>();
            List<CustomerCustLimit> custL = new List<CustomerCustLimit>();

            if(zEl.ItCust != null){
                tCust = zEl.ItCust; 
                if(tCust.item != null){
                    for(SapCustomerLimitSoap.ZbapiCustLimit zClt : tCust.item){
                        custL.add(new CustomerCustLimit(zClt.Customer,zClt.Name1,zClt.Deposit,zClt.Permisbleamt,zClt.Limitavailable,zClt.Advance,zClt.Totoutstand,zClt.Netoutstand,zClt.Totoverdue));
                    }
                }
            }

            if(zEl.Return_x != null){
                tbapi = zEl.Return_x;
                if(tbapi.item != null){
                    for(SapCustomerLimitSoap.Bapireturn  zBpt : tbapi.item){
                        custB.add(new CustomerBapiReturn(zBpt.Type_x,zBpt.Code,zBpt.Message,zBpt.LogNo,zBpt.LogMsgNo,zBpt.MessageV1,zBpt.MessageV2,zBpt.MessageV3,zBpt.MessageV4));
                    }
                }
            }

            customerLimitMapping.custBapi = custB;
            customerLimitMapping.custLimit = custL; 

            return customerLimitMapping;

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
    }


     //-- WRAPPER CLASS

     global class CustomerOutStandingMapping{
        public String Fkart         {get;set;}
        public String Vtext         {get;set;}
        public String Vbeln         {get;set;}
        public String Fkdat         {get;set;}
        public String Despda        {get;set;}
        public String OutstandAmt   {get;set;}
        public String Duedate       {get;set;}
        public Integer OsDays        {get;set;}
        public String Chequeno      {get;set;}
        public String Chequedate    {get;set;}
        public String Curr          {get;set;}
        public String AmtDoccur     {get;set;}
        public String LocCurrcy     {get;set;}
        
        public CustomerOutStandingMapping(String Fkart,String Vtext,String Vbeln,String Fkdat,String Despda,String OutstandAmt,String Duedate,Integer OsDays,String Chequeno,String Chequedate,String Curr,String AmtDoccur,String LocCurrcy){
            this.Fkart          = Fkart;
            this.Vtext          = Vtext;
            this.Vbeln          = Vbeln;
            this.Fkdat          = Fkdat;
            this.Despda         = Despda;
            this.OutstandAmt    = OutstandAmt;
            this.Duedate        = Duedate;
            this.OsDays         = OsDays;
            this.Chequeno       = Chequeno;
            this.Chequedate     = Chequedate;
            this.Curr           = Curr;
            this.AmtDoccur      = AmtDoccur;
            this.LocCurrcy      = LocCurrcy;
            
        }
     }



    /*
    * Wrapper class for customer Limit 
    */

     global class CustomerLimitMapping{

        public List<CustomerBapiReturn> custBapi {get;set;}
        public List<CustomerCustLimit>  custLimit {get;set;}

        public CustomerLimitMapping(){
            custBapi = new List<CustomerBapiReturn>();
            custLimit = new List<CustomerCustLimit>();
        }

     }

     global class CustomerBapiReturn{
        public String Type_x        {get;set;}
        public String Code          {get;set;}
        public String Message       {get;set;}
        public String LogNo         {get;set;}
        public String LogMsgNo      {get;set;}
        public String MessageV1     {get;set;}
        public String MessageV2     {get;set;}
        public String MessageV3     {get;set;}
        public String MessageV4     {get;set;}

        public CustomerBapiReturn(String Type_x,String Code,String Message,String LogNo,String LogMsgNo,String MessageV1,String MessageV2,String MessageV3,String MessageV4){
            this.Type_x = Type_x;
            this.Code   = Code;
            this.Message = Message;
            this.LogNo = LogNo;
            this.LogMsgNo = LogMsgNo;
            this.MessageV1 = MessageV1;
            this.MessageV2 = MessageV2;
            this.MessageV3 = MessageV3;
            this.MessageV4 = MessageV4;
        }
     }

     global class CustomerCustLimit{
        public String Customer          {get;set;}
        public String Name1             {get;set;}
        public String Deposit           {get;set;}
        public String Permisbleamt      {get;set;}
        public String Limitavailable    {get;set;}
        public String Advance           {get;set;}
        public String Totoutstand       {get;set;}
        public String Netoutstand       {get;set;}
        public String Totoverdue        {get;set;}

        public CustomerCustLimit(String Customer,String Name1,String Deposit,String Permisbleamt,String Limitavailable,String Advance,String Totoutstand,String Netoutstand,String Totoverdue){
            this.Customer = Customer;
            this.Name1 = Name1;
            this.Deposit = Deposit;
            this.Permisbleamt = Permisbleamt;
            this.Limitavailable = Limitavailable;
            this.Advance = Advance;
            this.Totoutstand = Totoutstand;
            this.Netoutstand = Netoutstand;
            this.Totoverdue = Totoverdue;
        }
     }

}