global class EmailScheduledApex implements Schedulable {
list<Tds__c> TdsList;
Map<id,Tds__c> TdsMap = new Map<id,Tds__c>();
list<Contact> conlist = new list<contact>();
Set<id> attlist = new Set<id>();
Set<id> TdsIds= new Set<id>();
Set<id> accids= new Set<id>();
list<Contact> conupdatelist = new List<Contact>();

// This test runs a scheduled job at midnight Sept. 3rd. 2022
   
   global void execute(SchedulableContext ctx) {
    List<Messaging.SingleEmailMessage> mails = 
  new List<Messaging.SingleEmailMessage>();
      conlist = [select id,email,accountId from contact where tds_check__C= true limit 400];
      if(!conlist.isEmpty()){
      for(Contact con: conlist)
      {
          accids.add(con.accountId );
          
      }
      Map<Id,Account> accconmap= new Map<Id,Account>([select id,(select id,email from contacts),(select id,name,TDS_Account__c from TDSs__r limit1 order by createddate ) from account where  id in :accids]);
      for(Account acc: accconmap.values())
      {
       if(!acc.TDSs__r.isempty()){
          TdsIds.add(acc.TDSs__r[0].id);
          }
      }
      TdsList = [select id,name,TDS_Account__c,(select id,parentid,name from Attachments limit1 order by createddate) from Tds__c where id in :TdsIds];
      
      
      for(Tds__c Tds: TdsList ){
          TdsMap.put(tds.Id,tds); 
           if(!Tds.Attachments.isempty()){
          attlist.add(Tds.Attachments[0].id); 
          }    
      }
   List<Attachment>  attlist1 = [select id,name,body,parentid from Attachment where id in :attlist];
   system.debug('--attlist1 --'+attlist1 );
   list<emailTemplate> emailtemplateid = [select id from emailTemplate where name='TDS Bulk Upload' limit 1];
    system.debug('-emailtemplateid'+emailtemplateid[0].Id);
      for(Attachment atchment :attlist1){
          
        List<Contact> conlist3= accconmap.get( TdsMap.get(atchment.parentId).TDS_Account__c).contacts;
        
         for(Contact con: conlist3)
          {
            
            //contactlist.add(con);
      
      // Step 1: Create a new Email
      Messaging.SingleEmailMessage mail = 
      new Messaging.SingleEmailMessage();
      // Step 2: Set list of people who should get the email
Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName(atchment.name);
        efa.setBody(atchment.body);
     // List<String> sendTo = new List<String>();
     // sendTo.add(con.Email);
      //mail.setToAddresses(sendTo); 
     
      mail.setSenderDisplayName('Attachment upload'); 
     // mail.setSubject('Attachment Info');
      mail.setTemplateId(emailtemplateid[0].Id);
       mail.setTargetObjectId(con.Id);
      String body = 'Hi , '+'\n';
      //body += 'Please Find the attachment '+'\n';
     // mail.setHtmlBody(body);
    mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
      // Step 5. Add your email to the master list
      mails.add(mail);
  // Step 6: Send all emails in the master list
  con.Tds_Check__c = false;
  conupdatelist.add(con);
          }
          
        
                }
                if(!mails.isEmpty())
                 Messaging.sendEmail(mails);
        if(!conupdatelist.isEmpty())
      update conupdatelist;
      }
      
   }   
}