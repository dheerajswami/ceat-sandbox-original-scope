global class ScheduledBatch_StockDelete implements Schedulable{
   global void execute(SchedulableContext sc) {
      Stocks_BatchForDelete stockBatch = new Stocks_BatchForDelete(); 
      database.executebatch(stockBatch ,200);
   }
}