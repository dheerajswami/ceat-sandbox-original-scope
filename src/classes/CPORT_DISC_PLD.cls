global class CPORT_DISC_PLD {
    
    /*
* Auther      :- Vivek Deepak
* Purpose     :- Wrapper for AsyncSap_PLD_DISC 
* Modified By :- 
* Date        :- 4/6/2015
*
*/
    
    // ATTRIBUTES --
        public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    // CONTRUCTOR --
    public CPORT_DISC_PLD(CPORT_PortalHomeController cont){
        
    }
    /*public CPORT_DISC_PLD(CPORT_PortalHome_OE_Controller cont){
        
    }*/
    public CPORT_DISC_PLD(){
        
    }
    // METHODS   --
    

    @RemoteAction
    global static PLD_NEW_CLASS getAllDiscountsPLDM(String circleNo,String kunnag){
       return getAllDiscountsPLD(circleNo,kunnag);
    }

    @RemoteAction
    global static List<Debit_Note_slab> getDebitNoteSlabM(String circleNo,String kunnag){
       return getDebitNoteSlab(circleNo,kunnag);
    }

    @RemoteAction
    global static List<Discounts_slab> getDiscountSlabM(String circleNo,String kunnag){
       return getDiscountSlab(circleNo,kunnag);
    }

  @RemoteAction
    global static List<SapDiscount1504New.ZSDSLAB> getDiscountSlab1504M(String circleNo,String kunnag){
      return  getDiscount1504Slab(circleNo,kunnag);
    }
    
    @RemoteAction
    global static List<Collection_sales> getCollectoionSalesM(String circleNo,String kunnag){
       return getCollectoionSales(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CS_Sales_Discounts> getCSSalesDiscountsM(String circleNo,String kunnag){
       return getCSSalesDiscounts(circleNo,kunnag);
    }


    @RemoteAction
    global static List<CPORT_DISC_TLD.Sap_TLD_Disc> getSlabTldM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getSlabTld(circleNo,kunnag);
    }

    @RemoteAction
    global static CPORT_DISC_TLD.SAP_PPD_Discount getPPDDiscountMasterM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getPPDDiscountMaster(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CPORT_DISC_TLD.Sap_ad_Discount> getSapAdDiscountsM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getSapAdDiscounts(circleNo,kunnag);
    }   


    /*
Method to fetch discounts PLD
*/
    WebService static PLD_NEW_CLASS getAllDiscountsPLD(String circleNo,String kunnag){
      
      PLD_NEW_CLASS disc_pld_map = new PLD_NEW_CLASS();
        
      try{

          String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH)); 
          SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');                
          String username                     = saplogin.username__c;
          String password                     = saplogin.password__c;
          Blob headerValue = Blob.valueOf(username + ':' + password);
          String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          SapDiscountsPLDnew.ZWS_ZSFDC_PLD_DISC zws = new SapDiscountsPLDnew.ZWS_ZSFDC_PLD_DISC();

          //sap_tld_disc_Prod.ZWS_SFDC_TLD_DISC zws                     = new sap_tld_disc_Prod.ZWS_SFDC_TLD_DISC();
          SapDiscountsPLDnew.ZSFDC_PLD_DISCResponse_element   reponse_element  = new SapDiscountsPLDnew.ZSFDC_PLD_DISCResponse_element();
          zws.inputHttpHeaders_x = new Map<String,String>();
          zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
          zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
          zws.timeout_x = 12000;
          SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB  tom = new SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_DEBIT_SLAB();
          SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_PLD  jerry = new SapDiscountsPLDnew.TABLE_OF_ZSFDC_WORKDHEET_PLD();
          //Continuation cont = new Continuation(60);
          reponse_element = zws.ZSFDC_PLD_DISC(circleNo,tom,customerId,jerry);

          List<PLD_Discount_New>   pld_new        = new List<PLD_Discount_New>();
          List<PLD_New_Debit_Slab> pld_debit_slab = new List<PLD_New_Debit_Slab>();
          //system.debug(reponse_element);
          if(reponse_element.DEBIT_NOTE_SLAB != null){
              if(reponse_element.DEBIT_NOTE_SLAB.item != null){
                 for(SapDiscountsPLDnew.ZSFDC_WORKDHEET_DEBIT_SLAB loop_var : reponse_element.DEBIT_NOTE_SLAB.item){
                     
                     pld_debit_slab.add(new PLD_New_Debit_Slab(loop_var.SCHEMENAME,loop_var.PRETAX,loop_var.POSTTAX,loop_var.DISCOUNT,loop_var.QUANTITY,loop_var.DISCOUNT_RATE,loop_var.DISCOUNT_TYPE,loop_var.ELIGIBILITY));
                 }
              }
          }
          if(reponse_element.PLD != null){
              if(reponse_element.PLD.item != null){
                 for(SapDiscountsPLDnew.ZSFDC_WORKDHEET_PLD loop_var : reponse_element.PLD.item){
                    pld_new.add(new PLD_Discount_New(loop_var.SCHEMENAME,loop_var.PRETAX,loop_var.POSTTAX,loop_var.AVG_SALES_VALUE,loop_var.SALES_VALUE_DISB,loop_var.SALES_QTY_DISB,loop_var.AVG_QTY,loop_var.DISCOUNT_RATE,loop_var.Discount,loop_var.DISCOUNT_TYPE));
                 }
              }
          }   
          
          disc_pld_map.pld_new = pld_new;
          disc_pld_map.pld_debit_slab = pld_debit_slab;

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        } 
        
        
        return disc_pld_map;
    }
    
    /*
Debit note slab
*/
    WebService static List<Debit_Note_slab> getDebitNoteSlab(String circleNo,String kunnag){
        
      List<Debit_Note_slab> debit_note_map = new List<Debit_Note_slab>();
      try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        Sap_DN_Slab_Disc_Prod   ws = new  Sap_DN_Slab_Disc_Prod ();
        
        Sap_DN_Slab_Disc_Prod.ZWS_SDFC_DEBIT_NOTE_SLAB_DISC  zws                = new Sap_DN_Slab_Disc_Prod.ZWS_SDFC_DEBIT_NOTE_SLAB_DISC ();
        Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDiscResponse_element  e         = new Sap_DN_Slab_Disc_Prod.ZsdfcDebitNoteSlabDiscResponse_element ();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        Sap_DN_Slab_Disc_Prod.TableOfZsd2851key   tom = new Sap_DN_Slab_Disc_Prod.TableOfZsd2851key();   
       // Continuation cont = new Continuation(60);
        e = zws.ZsdfcDebitNoteSlabDisc(circleNo,tom,customerId);
        
        system.debug('OUTPUT' + e);
        
        if(e != null){
            if(e.ItOutput != null){
                Sap_DN_Slab_Disc_Prod.TableOfZsd2851key temp = new Sap_DN_Slab_Disc_Prod.TableOfZsd2851key();
                temp = e.ItOutput;
                for(Sap_DN_Slab_Disc_Prod.Zsd2851key  loop_var : temp.item){
                    debit_note_map.add(new Debit_Note_slab(loop_var.Mandt,loop_var.Kunag,loop_var.Crnum,loop_var.Name1,loop_var.Sds,loop_var.Pre,loop_var.Post,loop_var.Kwert,loop_var.Disc,loop_var.Dwerk,loop_var.Fkimg,loop_var.Fkimg1,loop_var.Discount,loop_var.Type2,loop_var.Value,loop_var.Indcr,loop_var.Multi,loop_var.Vkbur,loop_var.Vkgrp,loop_var.Office,loop_var.Zgroup,loop_var.Superq,loop_var.Kwert1,loop_var.Kwert2,loop_var.Diff,loop_var.Flag,loop_var.Mwskz,loop_var.Nbp));
                }
            }
        } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        } 
        
        
        return debit_note_map;
    }    


    

// WRAPPER CLASS

global class PLD_NEW_CLASS {
   public List<PLD_Discount_New>   pld_new { get;set; }
   public List<PLD_New_Debit_Slab> pld_debit_slab {get;set;}

   public PLD_NEW_CLASS(){
     pld_new        = new List<PLD_Discount_New>();
     pld_debit_slab = new List<PLD_New_Debit_Slab>();
   }
}

// PLD NEW WRAPPER
global class PLD_Discount_New {
  public String SCHEMENAME {get;set;}
  public String PRETAX {get;set;}
  public String POSTTAX {get;set;}
  public String AVG_SALES_VALUE {get;set;}
  public String SALES_VALUE_DISB {get;set;}
  public String SALES_QTY_DISB {get;set;}
  public String AVG_QTY {get;set;}
  public String DISCOUNT_RATE {get;set;}
  public String DISCOUNT {get;set;}
  public String DISCOUNT_TYPE {get;set;}
  
  public PLD_Discount_New(String SCHEMENAME,String PRETAX,String POSTTAX,String AVG_SALES_VALUE,String SALES_VALUE_DISB,String SALES_QTY_DISB,String AVG_QTY,String DISCOUNT_RATE,String Discount,String DISCOUNT_TYPE){
      this.SCHEMENAME       = SCHEMENAME;
      this.PRETAX           = PRETAX;
      this.POSTTAX          = POSTTAX;
      this.AVG_SALES_VALUE  = AVG_SALES_VALUE;
      this.SALES_VALUE_DISB = SALES_VALUE_DISB;
      this.SALES_QTY_DISB   = SALES_QTY_DISB;
      this.AVG_QTY          = AVG_QTY;
      this.DISCOUNT_RATE    = DISCOUNT_RATE;
      this.DISCOUNT         = Discount;
      this.DISCOUNT_TYPE    = DISCOUNT_TYPE;
  }
}  


// PLD NEW DEBIT SLAB WRAPPER
global class PLD_New_Debit_Slab{
      public String SCHEMENAME { get; set; }
      public String PRETAX { get; set; }
      public String POSTTAX { get; set; }
      public String DISCOUNT { get; set; }
      public String QUANTITY { get; set; }
      public String DISCOUNT_RATE { get; set; }
      public String ELIGIBILITY { get; set; }
      public String DISCOUNT_TYPE { get; set; }
      
      public PLD_New_Debit_Slab(String SCHEMENAME,String PRETAX,String POSTTAX,String DISCOUNT,String QUANTITY,String DISCOUNT_RATE,String DISCOUNT_TYPE,String ELIGIBILITY){
          this.SCHEMENAME     = SCHEMENAME;
          this.PRETAX         = PRETAX;
          this.POSTTAX        = POSTTAX;
          this.DISCOUNT       = DISCOUNT;
          this.QUANTITY       = QUANTITY;
          this.DISCOUNT_RATE  = DISCOUNT_RATE;
          this.ELIGIBILITY    = ELIGIBILITY;
          this.DISCOUNT_TYPE  = DISCOUNT_TYPE;
      }
}

    /*
Debit Note Slab Mapping
*/
    
    global class Debit_Note_slab{
        public String client {get;set;} 
        public String sold_to_party {get;set;}
        public String Crnum {get;set;}
        public String Name1 {get;set;}
        public String sds_value {get;set;}
        public String pre_tax_val {get;set;}
        public String post_tax_val {get;set;}
        public String conditional_val {get;set;}
        public String conditional_val2 {get;set;}
        public String delivering_plant {get;set;}
        public String actual_invoiced_quantity {get;set;}
        public String actual_invoiced_quantity2 {get;set;}
        public String discount {get;set;}
        public String Type2 {get;set;}
        public String discount_value {get;set;}
        public String genral_flag {get;set;}
        public String more_val {get;set;}
        public String sale_office {get;set;}
        public String sale_group {get;set;}
        public String office {get;set;}
        public String description {get;set;}
        public String super_group {get;set;}
        public String conditional_val3 {get;set;}
        public String conditional_val4 {get;set;}
        public String conditional_val5 {get;set;}
        public String single_char_ind {get;set;}
        public String tax_on_sale {get;set;}
        public String net_billing_price {get;set;}
        public String customer_group3 {get;set;}
        public String ratio {get;set;}
        
        public Debit_Note_slab(String client, String sold_to_party, String crnum,
                               String name1, String sds_value, String pre_tax_val,
                               String post_tax_val, String conditional_val,
                               String conditional_val2, String delivering_plant,
                               String actual_invoiced_quantity, String actual_invoiced_quantity2,
                               String discount, String type2, String discount_value,
                               String genral_flag, String more_val, String sale_office,
                               String sale_group, String office, String description,
                               String super_group, String conditional_val3,
                               String conditional_val4, String conditional_val5,
                               String single_char_ind, String tax_on_sale,
                               String net_billing_price) {
                                   
                                   this.client = client;
                                   this.sold_to_party = sold_to_party;
                                   this.Crnum = crnum;
                                   this.Name1 = name1;
                                   this.sds_value = sds_value;
                                   this.pre_tax_val = pre_tax_val;
                                   this.post_tax_val = post_tax_val;
                                   this.conditional_val = conditional_val;
                                   this.conditional_val2 = conditional_val2;
                                   this.delivering_plant = delivering_plant;
                                   this.actual_invoiced_quantity = actual_invoiced_quantity;
                                   this.actual_invoiced_quantity2 = actual_invoiced_quantity2;
                                   this.discount = discount;
                                   this.Type2 = type2;
                                   this.discount_value = discount_value;
                                   this.genral_flag = genral_flag;
                                   this.more_val = more_val;
                                   this.sale_office = sale_office;
                                   this.sale_group = sale_group;
                                   this.office = office;
                                   this.description = description;
                                   this.super_group = super_group;
                                   this.conditional_val3 = conditional_val3;
                                   this.conditional_val4 = conditional_val4;
                                   this.conditional_val5 = conditional_val5;
                                   this.single_char_ind = single_char_ind;
                                   this.tax_on_sale = tax_on_sale;
                                   this.net_billing_price = net_billing_price;
                                   //this.customer_group3 = customer_group3;
                                   //this.ratio = ratio;
                               }        
    }

    /*
    Discounts slab
    */
    WebService static List<Discounts_slab> getDiscountSlab(String circleNo,String kunnag){
        
      List<Discounts_slab> dis_slab_map = new List<Discounts_slab>();
      
      try{

          String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
          SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
          
          String username                     = saplogin.username__c;
          String password                     = saplogin.password__c;
          Blob headerValue = Blob.valueOf(username + ':' + password);
          String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          Sap_Slab_Disc_Prod   ws = new  Sap_Slab_Disc_Prod();
          
          Sap_Slab_Disc_Prod.zws_zsfdc_slab_disc zws                   = new Sap_Slab_Disc_Prod.zws_zsfdc_slab_disc();
          Sap_Slab_Disc_Prod.ZsfdcSlabDiscResponse_element  e          = new Sap_Slab_Disc_Prod.ZsfdcSlabDiscResponse_element();
          zws.inputHttpHeaders_x = new Map<String,String>();
          zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
          zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
          zws.timeout_x = 12000;
          Sap_Slab_Disc_Prod.TableOfZsd308Data  tom = new Sap_Slab_Disc_Prod.TableOfZsd308Data();   
          Continuation cont = new Continuation(60);
          e = zws.ZsfdcSlabDisc(circleNo,tom,customerId);
          
          system.debug('OUTPUT' + e);
          
          if(e != null){
              if(e.ItOutput != null){
                  Sap_Slab_Disc_Prod.TableOfZsd308Data temp = new Sap_Slab_Disc_Prod.TableOfZsd308Data();
                  temp = e.ItOutput;
                  
                  for(Sap_Slab_Disc_Prod.Zsd308Data loop_var : temp.item){
                      system.debug('OUTPUT' + loop_var);
                      dis_slab_map.add(new Discounts_slab(loop_var.Mandt,loop_var.Cirno,loop_var.Kunag,loop_var.Name1,loop_var.Vkbur,loop_var.Avgsds,loop_var.Vkgrp,loop_var.Werks,loop_var.Svdisb,loop_var.Pretax,loop_var.Posttax,loop_var.Multi,loop_var.Indcr,loop_var.Ratio,loop_var.Avgqty,loop_var.Dfkimg,loop_var.Clubt,loop_var.Fkimg,loop_var.Bqty,loop_var.WGr,loop_var.Appdate,loop_var.Discount,loop_var.Disc,loop_var.Type2,loop_var.Bod,loop_var.Dcind,loop_var.Value,loop_var.KwertN,loop_var.Mwskz,loop_var.Pcr));
                  }
              }
          } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        system.debug('OUTPUT' + dis_slab_map);
        
        return dis_slab_map;
    }
    /*Discount Slabs 1504 */
    
     WebService static List<SapDiscount1504New.ZSDSLAB> getDiscount1504Slab (String circleNo,String kunnag){
     SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        SapDiscount1504New  scd                           = new SapDiscount1504New();
        SapDiscount1504New.ZWSSFDC_BPLOU_DISCOUNT  zws      = new SapDiscount1504New.ZWSSFDC_BPLOU_DISCOUNT();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;

        SapDiscount1504New.TABLE_OF_ZSDSLAB tom = new SapDiscount1504New.TABLE_OF_ZSDSLAB();
        SapDiscount1504New.ZSFDC_BPLOU_DISCOUNTResponse_element resp = new SapDiscount1504New.ZSFDC_BPLOU_DISCOUNTResponse_element();
        resp = zws.ZSFDC_BPLOU_DISCOUNT('000000001504',tom,'0050002706');
         //system.debug('discountResult:' + resp.IT_OUTPUT.item);
        return resp.IT_OutPUT.item;
       
     }
    
    /*
     Discounts slabs
    */
    global class Discounts_slab{
      public String Mandt {get;set;}
      public String Cirno {get;set;}
      public String Kunag {get;set;}
      public String Name1 {get;set;}
      public String Vkbur {get;set;}
      public String Avgsds {get;set;}
      public String Vkgrp {get;set;}
      public String Werks {get;set;}
      public String Svdisb {get;set;}
      public String Pretax {get;set;}
      public String Posttax {get;set;}
      public String Multi {get;set;}
      public String Indcr {get;set;}
      public String Ratio {get;set;}
      public String Avgqty {get;set;}
      public String Dfkimg {get;set;}
      public String Clubt {get;set;}
      public String Fkimg {get;set;}
      public String Bqty {get;set;}
      public String WGr {get;set;}
      public String Appdate {get;set;}
      public String Discount {get;set;}
      public String Disc {get;set;}
      public String Type2 {get;set;}
      public String Bod {get;set;}
      public String Dcind {get;set;}
      public String Value {get;set;}
      public String KwertN {get;set;}
      public String Mwskz {get;set;}
      public String Pcr {get;set;}

      public Discounts_slab(String mandt, String cirno, String kunag, String name1,
          String vkbur, String avgsds, String vkgrp, String werks,
          String svdisb, String pretax, String posttax, String multi,
          String indcr, String ratio, String avgqty, String dfkimg,
          String clubt, String fkimg, String bqty, String wGr,
          String appdate, String discount, String disc, String type2,
          String bod, String dcind, String value, String kwertN,
          String mwskz, String pcr) {
        
        this.Mandt = mandt;
        this.Cirno = cirno;
        this.Kunag = kunag;
        this.Name1 = name1;
        this.Vkbur = vkbur;
        this.Avgsds = avgsds;
        this.Vkgrp = vkgrp;
        this.Werks = werks;
        this.Svdisb = svdisb;
        this.Pretax = pretax;
        this.Posttax = posttax;
        this.Multi = multi;
        this.Indcr = indcr;
        this.Ratio = ratio;
        this.Avgqty = avgqty;
        this.Dfkimg = dfkimg;
        this.Clubt = clubt;
        this.Fkimg = fkimg;
        this.Bqty = bqty;
        this.WGr = wGr;
        this.Appdate = appdate;
        this.Discount = discount;
        this.Disc = disc;
        this.Type2 = type2;
        this.Bod = bod;
        this.Dcind = dcind;
        this.Value = value;
        this.KwertN = kwertN;
        this.Mwskz = mwskz;
        this.Pcr = pcr;
      }
    }
    
    /*
    Collection Sales
    */ 

    WebService static List<Collection_sales> getCollectoionSales(String circleNo,String kunnag){
        
      List<Collection_sales> collection_sales_map = new List<Collection_sales>();
      try{  

          String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
          SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
          
          String username                     = saplogin.username__c;
          String password                     = saplogin.password__c;
          Blob headerValue = Blob.valueOf(username + ':' + password);
          String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          Sap_Collection_Sales_Disc_Prod   ws = new  Sap_Collection_Sales_Disc_Prod();
          
          Sap_Collection_Sales_Disc_Prod.ZWS_SFDC_COLLECTION_SALES_DIS  zws                   = new Sap_Collection_Sales_Disc_Prod.ZWS_SFDC_COLLECTION_SALES_DIS();
          Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDiscResponse_element  e          = new Sap_Collection_Sales_Disc_Prod.ZsfdcCollectionSalesDiscResponse_element();
          zws.inputHttpHeaders_x = new Map<String,String>();
          zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
          zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
          zws.timeout_x = 12000;
          Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key  tom = new Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key();   
          Continuation cont = new Continuation(60);
          e = zws.ZsfdcCollectionSalesDisc(circleNo,tom,customerId);
          
          system.debug('OUTPUT' + e);
          
          if(e != null){
              if(e.ItOutput != null){
                  Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key temp = new Sap_Collection_Sales_Disc_Prod.TableOfZsd3021key();
                  temp = e.ItOutput;
                  for(Sap_Collection_Sales_Disc_Prod.Zsd3021key loop_var : temp.item){
                      collection_sales_map.add(new Collection_sales(loop_var.Mandt,loop_var.Vbeln,loop_var.Posnr,loop_var.Kunag,loop_var.Crnum,loop_var.Bezei1,loop_var.Bezei2,loop_var.Dwerk,loop_var.Superd,loop_var.Fkdat,loop_var.Postdat,loop_var.Duedat,loop_var.Zterm,loop_var.Paydat,loop_var.Zdays,loop_var.Fkimg,loop_var.Pre,loop_var.Post,loop_var.Disc,loop_var.Type2,loop_var.Discount,loop_var.Indcr,loop_var.Werks,loop_var.PayAmount,loop_var.Augbl));
                  }
              }
          } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        
        return collection_sales_map;
    }

    global class Collection_sales{
      public String Mandt {get;set;}
      public String Vbeln {get;set;}
      public String Posnr {get;set;}
      public String Kunag {get;set;}
      public String Crnum {get;set;}
      public String Bezei1 {get;set;}
      public String Bezei2 {get;set;}
      public String Dwerk {get;set;}
      public String Superd {get;set;}
      public String Fkdat {get;set;}
      public String Postdat {get;set;}
      public String Duedat {get;set;}
      public String Zterm {get;set;}
      public String Paydat {get;set;}
      public Integer Zdays {get;set;}
      public String Fkimg {get;set;}
      public String Pre {get;set;}
      public String Post {get;set;}
      public String Disc {get;set;}
      public String Type2 {get;set;}
      public String Discount {get;set;}
      public String Indcr {get;set;}
      public String Werks {get;set;}
      public String PayAmount {get;set;}
      public String Augbl {get;set;}
      
      public Collection_sales(String mandt, String vbeln, String posnr, String kunag,
            String crnum, String bezei1, String bezei2, String dwerk,
            String superd, String fkdat, String postdat, String duedat,
            String zterm, String paydat, Integer zdays, String fkimg,
            String pre, String post, String disc, String type2,
            String discount, String indcr, String werks, String payAmount,
            String augbl) {
          
          this.Mandt = mandt;
          this.Vbeln = vbeln;
          this.Posnr = posnr;
          this.Kunag = kunag;
          this.Crnum = crnum;
          this.Bezei1 = bezei1;
          this.Bezei2 = bezei2;
          this.Dwerk = dwerk;
          this.Superd = superd;
          this.Fkdat = fkdat;
          this.Postdat = postdat;
          this.Duedat = duedat;
          this.Zterm = zterm;
          this.Paydat = paydat;
          this.Zdays = zdays;
          this.Fkimg = fkimg;
          this.Pre = pre;
          this.Post = post;
          this.Disc = disc;
          this.Type2 = type2;
          this.Discount = discount;
          this.Indcr = indcr;
          this.Werks = werks;
          this.PayAmount = payAmount;
          this.Augbl = augbl;
        }
    }

    /*
    CS Sales Discounts
    */

    WebService static List<CS_Sales_Discounts> getCSSalesDiscounts(String circleNo,String kunnag){
        
      List<CS_Sales_Discounts> cs_sales_map = new List<CS_Sales_Discounts>();
      try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        Sap_CS_Sales_Disc_Prod   ws = new  Sap_CS_Sales_Disc_Prod();
        
        Sap_CS_Sales_Disc_Prod.zws_zsfdc_cs_sales_disc  zws                  = new Sap_CS_Sales_Disc_Prod.zws_zsfdc_cs_sales_disc();
        Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDiscResponse_element   e          = new Sap_CS_Sales_Disc_Prod.ZsfdcCsSalesDiscResponse_element();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        Sap_CS_Sales_Disc_Prod.TableOfZsd47Data tom = new Sap_CS_Sales_Disc_Prod.TableOfZsd47Data();   
        //Continuation cont = new Continuation(60);
        e = zws.ZsfdcCsSalesDisc(circleNo,tom,customerId);
        
        system.debug('OUTPUT' + e);
        
        if(e != null){
            if(e.ItOutput != null){
                Sap_CS_Sales_Disc_Prod.TableOfZsd47Data temp = new Sap_CS_Sales_Disc_Prod.TableOfZsd47Data();
                temp = e.ItOutput;
                for(Sap_CS_Sales_Disc_Prod.Zsd47Data loop_var : temp.item){
                    cs_sales_map.add(new CS_Sales_Discounts(loop_var.Mandt,loop_var.Crnum,loop_var.Kunag,loop_var.Name1,loop_var.Pre,loop_var.Post,loop_var.Kwerg,loop_var.Kwert,loop_var.Kwert1,loop_var.Fkimg,loop_var.Fkimg1,loop_var.Discount,loop_var.Type2,loop_var.Value,loop_var.Type_x,loop_var.Dwerk,loop_var.Superq,loop_var.Superd,loop_var.Kvgr3,loop_var.Grthv,loop_var.Grthq,loop_var.Vkgrp,loop_var.Vkbur,loop_var.Sel));
                }
            }
        } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        
        return cs_sales_map;
    }    

    global class CS_Sales_Discounts{
         public String Mandt {get;set;}
         public String Crnum {get;set;}
         public String Kunag {get;set;}
         public String Name1 {get;set;}
         public String Pre {get;set;}
         public String Post {get;set;}
         public String Kwerg {get;set;}
         public String Kwert {get;set;}
         public String Kwert1 {get;set;}
         public String Fkimg {get;set;}
         public String Fkimg1 {get;set;}
         public String Discount {get;set;}
         public String Type2 {get;set;}
         public String Value {get;set;}
         public String Type_x {get;set;}
         public String Dwerk {get;set;}
         public String Superq {get;set;}
         public String Superd {get;set;}
         public String Kvgr3 {get;set;}
         public String Grthv {get;set;}
         public String Grthq {get;set;}
         public String Vkgrp {get;set;}
         public String Vkbur {get;set;}
         public String Sel {get;set;}
      public CS_Sales_Discounts(String mandt, String crnum, String kunag, String name1,
          String pre, String post, String kwerg, String kwert, String kwert1,
          String fkimg, String fkimg1, String discount, String type2,
          String value, String type_x, String dwerk, String superq,
          String superd, String kvgr3, String grthv, String grthq,
          String vkgrp, String vkbur, String sel) {
        
        this.Mandt = mandt;
        this.Crnum = crnum;
        this.Kunag = kunag;
        this.Name1 = name1;
        this.Pre = pre;
        this.Post = post;
        this.Kwerg = kwerg;
        this.Kwert = kwert;
        this.Kwert1 = kwert1;
        this.Fkimg = fkimg;
        this.Fkimg1 = fkimg1;
        this.Discount = discount;
        this.Type2 = type2;
        this.Value = value;
        this.Type_x = type_x;
        this.Dwerk = dwerk;
        this.Superq = superq;
        this.Superd = superd;
        this.Kvgr3 = kvgr3;
        this.Grthv = grthv;
        this.Grthq = grthq;
        this.Vkgrp = vkgrp;
        this.Vkbur = vkbur;
        this.Sel = sel;
      }
    }
    
}