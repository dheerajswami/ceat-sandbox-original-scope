/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class DashboardInputOutputHandler_TestClass{
    
    static list<Id> terrTypeID                            = new list<Id>();
    static list<Id> terrId                                = new list<Id>();
    // Test method to cover Input section
    
    static testMethod void myUnitTest() {
        String tlRole = System.Label.TL_Role;
        string TLInputRecType           = system.Label.Input_RecType;
        
        map<String,Id> wightageMap = new map<String,Id>();
        
        Id TLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName ='TL' Limit 1].Id;
        Id summaryInputRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:TLInputRecType Limit 1].Id;
        
         
        integer monthNumber = date.today().month();
        String month1 = (monthNumber == 1)?'Jan':(monthNumber == 2)?'Feb':(monthNumber == 3)?'Mar':(monthNumber == 4)?'Apr':(monthNumber == 5)?'May':(monthNumber == 6)?'June':(monthNumber == 7)?'July':(monthNumber == 8)?'Aug':(monthNumber == 9)?'Sep':(monthNumber == 10)?'Oct' : (monthNumber == 11)?'Nov' : 'Dec';
            
        //list<sObject> dwmAl      = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'DashboardMasterInput');
           
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId AND RoleInTerritory2=:tlRole];    
        User u = [Select id, name from User where id = :userTerrAssList[0].UserId limit 1];
        
        //Insert Dashboard Master
         Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true, Role__c = 'TL_Replacement');
         insert dm;                
       
         Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();                    
         dwmTemp.Dashboard_Master__c = dm.id;
         dwmTemp.role__c = tlRole;
         dwmTemp.Weightage__c = 0;
         insert dwmTemp; 
        
        Dashboard_Input_Score__c inpScore = new Dashboard_Input_Score__c();
            inpScore.RecordTypeId = TLInputRecTypeId;           
            inpScore.Month__c = month1;
            inpScore.year__c = string.valueof(date.today().year());
            inpScore.Dashboard_Weightage_Master__c = dwmTemp.id;
            inpScore.Territory_Code__c = userTerrAssList[0].Territory2.name;
            inpScore.Parameters__c='Number of P1 customers met';
            inpScore.PJP__c=true;
            inpScore.Category__c='PJP adherance';
            inpScore.Total_Monthly_Target__c=0;
            inpScore.Total_MTD_Target__c=0;
            inpScore.Total_Actual_MTD__c=0;
            inpScore.Achievement_MTD__c=0;
            inpScore.Score__c=0;
            inpScore.User__c = u.id;    
            inpScore.OwnerId = u.id;         
       
        insert inpScore;
        
        //**********************************************************OUTPUT*******************************************************************
            String repDealerLabel           = System.Label.Replacement_Dealer;
            String repROLabel                                       = System.Label.Replacement_RO;
            String repTLLabel                                       = System.Label.Replacement_TL; 
            String tlLabel = System.Label.TL_Role; 
            
            Date d = system.today();
            String month = String.valueOf(d.month());
            String year = String.valueOf(d.Year());
            user runuser = new user();
            
            
            
            list<Sales_Planning__c> queryAP =  new list<Sales_Planning__c>();
            List<Dashboard_Output_Handler_Controller.dashboardWrapper> listOFds = new List<Dashboard_Output_Handler_Controller.dashboardWrapper>();
            List<UserTerritory2Association> userTerrCode = new list<UserTerritory2Association>();
            
            set<id> setOFSp = new set<Id>();
            Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
            Id replaceROId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
            Id replaceTLId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
            Id dashboardOutputId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName = 'Output' Limit 1].Id;
            Id dshOutputTotalId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName = 'Total_Output_Value' Limit 1].Id;
            // Load the test Sales planning Staging from the static resource
            List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'catrepDashboard1');
            //List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataR');
            
            
            
            terrlist = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE /*Name = 'D0167' AND */ Territory2TypeId =: terriType[0].id limit 1];
            
            userTerrCode = [select territory2Id,Territory2.name,id,UserID from UserTerritory2Association where territory2Id =: terrlist[0].id AND (RoleInTerritory2=: tlLabel )]; 
            if(userTerrCode.size() > 0 ){
                runuser = [Select id from User Where id =: userTerrCode[0].UserID];
            }      
            
            Dashboard_Weightage_Master__c total = new Dashboard_Weightage_Master__c();
            total.Testing__c = true;
            total.Dashboard_Master__c = dm.id;
            total.Role__c = 'TL_Replacement';
            total.Weightage__c = 4;
            total.RecordTypeId  = dshOutputTotalId ;
            insert total;
            
            Dashboard_Weightage_Master__c dwmTBB = new Dashboard_Weightage_Master__c (Category__c='TBB',Parameters_Output__c='High Priority Dealer,Maintain Dealer,Institutions',Role__c='TL_Replacement',Testing__c=true,Weightage__c= 10,
            RecordTypeID = dashboardOutputId,Dashboard_Master__c = dm.id   );
            insert dwmTBB;
            
            Dashboard_Score__c dsTBB = new Dashboard_Score__c (Actual_MTD__c = 123,Number_of_Dealers_Distributors__c = 10,Category__c = '2010',Dashboard_Weightage_Master__c = dwmTBB.id,L3M__c= 123,LYCM__c = 344,Month__c = month ,Monthly_Target__c = 345,MTD_Target__c=322,
            No_of_Dealer_Distributor_Billed_Since__c = 9,No_of_Dealer_Distributor_Plan__c = 10,Parameters__c = 'High Priority Dealer',Score__c = 5,Testing__c = true,Year__c= year,User__c = runuser.id,ownerId=runuser.id,Territory_Code__c ='B0001' );
            insert dsTBB ;
            
        
            Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c (Input_Score__c = 20,Total_Value__c=12312,Total_Value_L3M__c=456,Total_Value_LYCM__c=12343,Dashboard_Weightage_Master__c=total.id,DSS_External_ID__c=(userTerrCode[0].Territory2.name+month+year),Month__c=month,Score__c=5,ownerId=runuser.id,Territory_Code__c=userTerrCode[0].Territory2.name,Year__c = year);
            insert dss;
           
            Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c(Total_Monthly_Target__c = 1234,Total_No_of_Dealers_Distributor__c = 10,Total_LYCM__c = 233,Total_Actual_MTD__c = 343,Total_MTD_Target__c = 465,Total_No_of_dealers_distributors_planned__c = 9,Total_L3M__c =34,Total_dealers_distributors_billed_since__c=9,
            Category__c ='2010',Dashboard_Weightage_Master__c = dwmTBB.id,Dashboard_Summary_Score__c=dss.id,Territory_Code__c ='B0001',Testing__c = true,User__c = runuser.id,Month__c =month,Year__c = year);
            insert dos;
           
            //List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
            List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDashboard');
            List<sObject> listDashField = Test.loadData(DashBoardFieldValue__c.sObjectType, 'DashboardField');
            List<sObject> listDealerField = Test.loadData(Dealer_Type_Field_Matching__c.sObjectType, 'DealerField');
            list<sObject> listDashboardTLData = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'DashboardTLWeightage1');
            for(sObject ds : listDashboardTLData ){
                Dashboard_Weightage_Master__c temp= (Dashboard_Weightage_Master__c)ds;
                temp.RecordTypeId = dashboardOutputId;
                temp.Dashboard_Master__c = dm.id;
            }
            update listDashboardTLData ;
            
        //***********************************************************************************************************************************
         system.runAs(runuser){
            
                       
         /*   
            Dashboard_Summary_Score__c dbSummaryScore1 = new Dashboard_Summary_Score__c();
            dbSummaryScore1.Territory_Code__c = userTerrCode[0].Territory2.name;
            dbSummaryScore1.Month__c = string.valueof(monthNumber) ;
            dbSummaryScore1.Year__c = string.valueof(date.today().year());
            dbSummaryScore1.Input_Score__c = 20;
            dbSummaryScore1.DSS_External_ID__c = userTerrCode[0].Territory2.name+monthNumber+string.valueof(date.today().year());
            dbSummaryScore1.RecordTypeId = summaryInputRecTypeId;
            insert dbSummaryScore1;
        */
            
			ApexPages.currentPage().getParameters().put('Id',runuser.Id);
			ApexPages.currentPage().getParameters().put('month',month);
			ApexPages.currentPage().getParameters().put('year',year);
            DashboardInputOutputHandler dbTl1 = new DashboardInputOutputHandler();
            DashboardInputOutputHandler.dashboardWrapperMain  res1 = DashboardInputOutputHandler.getDashboardDataRecords(runuser.Id,month,year);
           // DashboardInputOutputHandler.dashboardWrapperMain  res2 = DashboardInputOutputHandler.getDashboardDataRecords(null);
        }
    }
}