@istest
public class VehicleServiceHelpTest{

    static testMethod void testParse() {
        String json=        '{"customersDetails":'+
        '[{"Name":"Ashish Verma",'+
        '"Mobile":"9711300094",'+
        '"Email":"ashish.verma@Vfirst.com",'+
        '"VehicleRegistrationNo":"123456789",'+
        '"VehicleMake":"Honda",'+
        '"VehicleModel":"Brio",'+
        '"NextWheelAlignmentDate":"2015-11-15 00:00:00",'+
        '"NextBlancingDate":"2015-11-15 00:00:00",'+
        '"LastWheelAlignmentDate":"2015-09-15 00:00:00",'+
        '"LastBlancingDate":"2015-09-15 00:00:00",'+
        '"Alignment":"TRUE",'+
        '"Balancing":"TRUE",'+
        '"NitrogenInflation":"TRUE",'+
        '"TyresChanged":"TRUE",'+
        '"FLTyreCondition":"fine",'+
        '"FRTyreCondition":"worst",'+
        '"RLTyreCondition":"worst",'+
        '"RRTyreCondition":"worst",'+
        '"RemainingFLTyreLife":"2",'+
        '"RemainingFRTyreLife":"2",'+
        '"RemainingRLTyreLife":"2",'+
        '"RemainingRRTyreLife":"2",'+
        '"NextFLTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextFRTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextRRTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextRLTyreChangeDate":"2015-12-15 00:00:00",'+
        '"FLTyreShoppy":"Dev",'+
        '"FRTyreShoppy":"Shiva",'+
        '"RLTyreShoppy":"Dev",'+
        '"RRTyreShoppy":"Dev"},'+
        '{"Name":"Saurabh Sharma",'+
        '"Mobile":"9560300094",'+
        '"Email":"saurabh.sharma@Vfirst.com",'+
        '"VehicleRegistrationNo":"32123456789",'+
        '"VehicleMake":"Honda",'+
        '"VehicleModel":"Brio",'+
        '"NextWheelAlignmentDate":"2015-11-15 00:00:00",'+
        '"NextBlancingDate":"2015-11-15 00:00:00",'+
        '"LastWheelAlignmentDate":"2015-09-15 00:00:00",'+
        '"LastBlancingDate":"2015-09-15 00:00:00",'+
        '"Alignment":"TRUE",'+
        '"Balancing":"TRUE",'+
        '"NitrogenInflation":"TRUE",'+
        '"TyresChanged":"TRUE",'+
        '"FLTyreCondition":"fine",'+
        '"FRTyreCondition":"worst",'+
        '"RLTyreCondition":"worst",'+
        '"RRTyreCondition":"worst",'+
        '"RemainingFLTyreLife":"2",'+
        '"RemainingFRTyreLife":"2",'+
        '"RemainingRLTyreLife":"2",'+
        '"RemainingRRTyreLife":"2",'+
        '"NextFLTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextFRTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextRRTyreChangeDate":"2015-12-15 00:00:00",'+
        '"NextRLTyreChangeDate":"2015-12-15 00:00:00",'+
        '"CEATShoppeName":"Dev"}]'+
        '}'+
        ' ';
        VehicleServiceHelp.parse(json);
        //System.assert(obj != null);
    }
    }