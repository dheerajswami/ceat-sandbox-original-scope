global class Batch_TownGeolocation implements Database.Batchable<State_Master__c>, Database.AllowsCallouts,  Database.Stateful {    
    global Iterable<State_Master__c> start(Database.BatchableContext BC){        
        return [Select Town__c, Town_Geolocation__c, District__c, State__c from State_Master__c where Town_Geolocation__Latitude__s = null Limit 10];
    }
    
    global void execute(Database.BatchableContext BC, List<State_Master__c> scope) {
        Long start = DateTime.now().getTime();
        while(DateTime.now().getTime()< start+1000){ 
        }
        List<State_Master__c> listToUpdate = new List<State_Master__c>();
        for(State_Master__c sm : scope) {
            String[] x = getCoordinates(sm.Town__c+','+sm.District__c+','+sm.State__c);
            sm.Town_Geolocation__Latitude__s = Decimal.valueOf(x[0]);
            sm.Town_Geolocation__Longitude__s = Decimal.valueOf(x[1]);
            listToUpdate.add(sm);
        }
        update listToUpdate;        
    }
    
    global void finish(Database.BatchableContext BC) {        
        //put your email address to get exception notifications
    }
    
    public static String[] getCoordinates(String commonAddress){ 
        String[] coordinates = new List< String>(); 
        System.debug(commonAddress.contains(' '));
        commonAddress = commonAddress.replaceAll('(\\s+)','+');
        System.debug('commonAddress   '+commonAddress);
        String url = 'https://maps.googleapis.com/maps/api/geocode/json?';
        url += 'address=' + commonAddress;//Bangalore';// 
        url += '&sensor=false'; 
        
        system.debug(Logginglevel.ERROR,'GeoUtilitiesCoordinates url: ' + url);    
        
        Http h = new Http(); 
        HttpRequest req = new HttpRequest();
        
        req.setMethod('GET');
        req.setEndpoint(url); 
        
        String responseBody;
        if (!Test.isRunningTest()){ 
            // Methods defined as TestMethod do not support Web service callouts
            HttpResponse res = h.send(req); 
            system.debug('res // ' + res);
            responseBody = res.getBody();
            System.debug('Swayam response  '+responseBody);
            
            W_Class1 m = (W_Class1) JSON.deserialize(responseBody, W_Class1.class);
            System.debug('Swayam    '+m.results);
            for(W_Class2 w : m.results) {
                coordinates.add(w.geometry.location.lat);
                coordinates.add(w.geometry.location.lng);                
            }            
        }
        else {
            // dummy data
            responseBody = '\"location\" : { \"lat\" : 32.0719776,\"lng\" : 34.7914048}';
        } 
        system.debug('responseBody // ' + responseBody);
        return coordinates; 
    }
    
    global class W_Class1 {
        String exclude_from_slo;
        String status;
        List<W_Class2> results;
    }
    
    global class W_Class2 {        
        List<W_Class3> address_components;
        String formatted_address;
        W_Class4 geometry;
        String place_id;
        List<String> types;
    }
    
    global class W_Class3 {
        String long_name;
        String short_name;
        List<String> types;
    }
    
    global class W_Class4 {        
        W_Class5 bounds;
        W_Class6 location;
        String location_type;
        W_Class5 viewport;
    }
    
    global class W_Class5 {
        W_Class6 northeast;
        W_Class6 southwest;        
    }
    
    global class W_Class6 {
        String lat;
        String lng;        
    }
    
}