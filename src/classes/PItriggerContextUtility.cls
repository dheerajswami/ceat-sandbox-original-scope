public class PItriggerContextUtility {
	private static Boolean beforeInsertFlag = true;
    private static Boolean afterInsertFlag = true;
    private static Boolean beforeUpdateFlag = true;
    private static Boolean afterUpdateFlag = true;
    private static Boolean beforeDeleteFlag = true;
    private static Boolean afterDeleteFlag = true;
    private static Boolean undeleteFlag = true;

    /*public static boolean isBIFirstRun() {
        return beforeInsertFlag;
    }
    
    public static void setBIFirstRunFalse(){
        beforeInsertFlag = false;
    }*/
    
    public static boolean isAIFirstRun() {
        return afterInsertFlag;
    }
    
    public static void setAIFirstRunFalse(){
        afterInsertFlag = false;
    }
    
    /*public static boolean isBUFirstRun() {
        return beforeUpdateFlag;
    }*/
    
    /*public static void setBUFirstRunFalse(){
        beforeUpdateFlag = false;
    }*/
    
    public static boolean isAUFirstRun() {
        return afterUpdateFlag;
    }
    
    
    public static void setAUFirstRunFalse(){
        afterUpdateFlag = false;
    }
    
    /*public static boolean isBDFirstRun() {
        return beforeDeleteFlag;
    }
    */
    /*public static void setBDFirstRunFalse(){
        beforeDeleteFlag = false;
    }
    */
    /*public static boolean isADFirstRun() {
        return afterDeleteFlag;
    }
    */
    /*public static void setADFirstRunFalse(){
        afterDeleteFlag = false;
    }
    */
    /*public static boolean isUdFirstRun() {
        return undeleteFlag;
    }
    */
    /*public static void setUdFirstRunFalse(){
        undeleteFlag = false;
    }
    */
}