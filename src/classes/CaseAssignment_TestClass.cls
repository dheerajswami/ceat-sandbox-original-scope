@isTest
public class CaseAssignment_TestClass {
    static testmethod void caseReplacement() {
        EmailTemplate e = new EmailTemplate (developerName = 'Email_for_Escalation_Matrix_CaseCSD', FolderId = '00l90000000kgZJ', TemplateType= 'Text', Name = 'Email for Escalation Matrix Case(CSD)');
        //insert e;
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Replacements','MIS Officer; RCM','Sales','1','External','RM');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        insert caseReplacement;
        //Case caseReplacement1 = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        //insert caseReplacement1;
        caseReplacement.Nature__c = 'External';
        CaseHelper.run = true;
        update caseReplacement;
        
    }

    static testmethod void caseReplacementCSD() {
        EmailTemplate e = new EmailTemplate (developerName = 'Email_for_Escalation_Matrix_CaseCSD', FolderId = '00l90000000kgZJ', TemplateType= 'Text', Name = 'Email for Escalation Matrix Case(CSD)');
        //insert e;
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Replacements','MIS Officer; RCM','Customer Service','1','External','RM');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Customer Service','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        insert caseReplacement;
        //Case caseReplacement1 = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        //insert caseReplacement1;
        caseReplacement.Nature__c = 'External';
        CaseHelper.run = true;
        update caseReplacement;
        
    }
    static testmethod void caseReplacementTORoleHO() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Replacements','MIS Officer; RCM','Sales','1','External','MIS Officer');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        //caseReplacement.CC_Email__c = 'ciba@extentor.com';
        insert caseReplacement;
        //Case caseReplacement1 = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        //insert caseReplacement1;
        caseReplacement.Nature__c = 'External';
        CaseHelper.run = true;
        update caseReplacement;
        
    }
    static testmethod void caseReplacementTORoleHOCCEmailFilled() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Replacements','MIS Officer; RCM','Sales','1','External','MIS Officer');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseReplacement = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        caseReplacement.CC_Email__c = 'ciba@extentor.com';
        insert caseReplacement;
        //Case caseReplacement1 = CEAT_InitializeTestData.createCaseReplacement('External','Replacements','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Replacement','MADHYA PRADESH','Balaghat','1');
        //insert caseReplacement1;
        caseReplacement.Nature__c = 'External';
        CaseHelper.run = true;
        update caseReplacement;
        
    }

    static testmethod void caseExport() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Exports','QA; QA1','Sales','1','External','Export cluster mgr');
        insert mat;
        Employee_Directory__c empDir = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','Export cluster mgr','Kishlay','Export','Far East 1','FE03');
        insert empDir;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        
        Case caseExport = CEAT_InitializeTestData.createCaseExport('External','Exports','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Export','','','1');
        caseExport.cluster_code__c = 'FE03';
        insert caseExport ;
        caseExport.Nature__c = 'External';
        CaseHelper.run = true;
        update caseExport;
        
    }

    static testmethod void caseExportTORoleHO() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Exports','QA1; Export cluster mgr','Sales','1','External','QA');
        insert mat;
        Employee_Directory__c empDir = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','Export cluster mgr','Kishlay','Export','Far East 1','FE03');
        insert empDir;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        
        Case caseExport = CEAT_InitializeTestData.createCaseExport('External','Exports','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Export','','','1');
        caseExport.cluster_code__c = 'FE03';
        //caseExport.CC_Email__c = 'ciba@extentor.com';
        insert caseExport ;
        caseExport.Nature__c = 'External';
        CaseHelper.run = true;
        update caseExport;
        
    }

    static testmethod void caseExportTORoleHOCCEmailFilled() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Exports','QA1; Export cluster mgr','Sales','1','External','QA');
        insert mat;
        Employee_Directory__c empDir = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','Export cluster mgr','Kishlay','Export','Far East 1','FE03');
        insert empDir;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        
        Case caseExport = CEAT_InitializeTestData.createCaseExport('External','Exports','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Export','','','1');
        caseExport.cluster_code__c = 'FE03';
        caseExport.CC_Email__c = 'ciba@extentor.com';
        insert caseExport ;
        caseExport.Nature__c = 'External';
        CaseHelper.run = true;
        update caseExport;
        
    }
    static testmethod void caseOEMPlant() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Plant','QA; QA1','Sales','1','External','CSOE');
        insert mat;
        Employee_Directory__c empDirCSOE = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','CSOE','Kishlay','OE Plant','','');
        empDirCSOE.OE_Plant__c = accOE.id;
        empDirCSOE.Sales_Office__c = 'N102';
        insert empDirCSOE;
        
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Plant','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Plant','','','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }

    static testmethod void caseOEMPlantTORoleHO() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Plant','QA1; CSOE','Sales','1','External','QA');
        insert mat;
        Employee_Directory__c empDirCSOE = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','CSOE','Kishlay','OE Plant','','');
        empDirCSOE.OE_Plant__c = accOE.id;
        empDirCSOE.Sales_Office__c = 'N102';
        insert empDirCSOE;
        
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Plant','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Plant','','','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        //caseOEMPlant.CC_Email__c = 'ciba@extentor.com';
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }

    static testmethod void caseOEMPlantTORoleHOCCEmailFilled() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Plant','QA1; CSOE','Sales','1','External','QA');
        insert mat;
        Employee_Directory__c empDirCSOE = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','CSOE','Kishlay','OE Plant','','');
        empDirCSOE.OE_Plant__c = accOE.id;
        empDirCSOE.Sales_Office__c = 'N102';
        insert empDirCSOE;
        
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirQA1 = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA1','Kishlay','HO','','');
        insert empDirQA1 ;
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Plant','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Plant','','','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        caseOEMPlant.CC_Email__c = 'ciba@extentor.com';
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }

    static testmethod void caseSpecialty() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accSpecialtyPlant = CEAT_InitializeTestData.createAccount('TestAcc','358338635');
        accSpecialtyPlant.Type = 'Specialty Plant';
        accSpecialtyPlant.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accSpecialtyPlant ;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Specialty','RM - Specialty;QA','Sales','1','External','ZSM');
        mat.Escalation_material_type__c = '1';
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirRMSpe = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','RM - Specialty','Kishlay','Specailty Plant','','');
        empDirRMSpe.Specialty_Plant__c = accSpecialtyPlant.id;
        insert empDirRMSpe;
        Employee_Directory__c empDirZSM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','ZSM','Kishlay','Replacement','','');
        insert empDirZSM ;
        Case caseSpecialty = CEAT_InitializeTestData.createCaseReplacement('External','Specialty','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Specialty','MADHYA PRADESH','Balaghat','1');
        caseSpecialty.Escalation_material_type__c = '1';
        caseSpecialty.Dealer_Plant__c = accSpecialtyPlant.id;
        insert caseSpecialty ;
        caseSpecialty.Nature__c = 'External';
        CaseHelper.run = true;
        update caseSpecialty ;
    }

    static testmethod void caseSpecialtyTORoleHO() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accSpecialtyPlant = CEAT_InitializeTestData.createAccount('TestAcc','358338635');
        accSpecialtyPlant.Type = 'Specialty Plant';
        insert accSpecialtyPlant ;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Specialty','RM - Specialty;ZSM','Sales','1','External','QA');
        mat.Escalation_material_type__c = '1';
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRMSpe = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','RM - Specialty','Kishlay','Specailty Plant','','');
        empDirRMSpe.Specialty_Plant__c = accSpecialtyPlant.id;
        insert empDirRMSpe;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirZSM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','ZSM','Kishlay','Replacement','','');
        insert empDirZSM ;
        Case caseSpecialty = CEAT_InitializeTestData.createCaseReplacement('External','Specialty','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Specialty','MADHYA PRADESH','Balaghat','1');
        caseSpecialty.Escalation_material_type__c = '1';
        caseSpecialty.Dealer_Plant__c = accOE.id;
        //caseSpecialty.CC_Email__c = 'ciba@extentor.com';
        insert caseSpecialty ;
        caseSpecialty.Nature__c = 'External';
        CaseHelper.run = true;
        update caseSpecialty ;
    }
    
    static testmethod void caseSpecialtyTORoleHOCCEmailFilled() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accSpecialtyPlant = CEAT_InitializeTestData.createAccount('TestAcc','358338635');
        accSpecialtyPlant.Type = 'Specialty Plant';
        insert accSpecialtyPlant ;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('Specialty','RM - Specialty;ZSM','Sales','1','External','QA');
        mat.Escalation_material_type__c = '1';
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRMSpe = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','RM - Specialty','Kishlay','Specailty Plant','','');
        empDirRMSpe.Specialty_Plant__c = accSpecialtyPlant.id;
        insert empDirRMSpe;
        Employee_Directory__c empDirQA = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','QA','Kishlay','HO','','');
        insert empDirQA ;
        Employee_Directory__c empDirZSM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','ZSM','Kishlay','Replacement','','');
        insert empDirZSM ;
        Case caseSpecialty = CEAT_InitializeTestData.createCaseReplacement('External','Specialty','Sales','Open','Test Subject',acc.id,con.id,'Email','External - Specialty','MADHYA PRADESH','Balaghat','1');
        caseSpecialty.Escalation_material_type__c = '1';
        caseSpecialty.Dealer_Plant__c = accOE.id;
        caseSpecialty.CC_Email__c = 'ciba@extentor.com';
        insert caseSpecialty ;
        caseSpecialty.Nature__c = 'External';
        CaseHelper.run = true;
        update caseSpecialty ;
    }
    static testmethod void caseOEMDealer() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Dealer','MIS Officer; RCM','Sales','1','External','RM');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Dealer','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Dealer','MADHYA PRADESH','Balaghat','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }
    
    static testmethod void caseOEMDealerTORoleHO() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Dealer','RCM; RM','Sales','1','External','MIS Officer');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Dealer','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Dealer','MADHYA PRADESH','Balaghat','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        caseOEMPlant.CC_Email__c = 'ciba@extentor.com';
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }
    /*
    static testmethod void caseOEMDealerTORoleHOWithCCEmailFilled() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Dealer','RCM','Sales','1','External','RM');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Dealer','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Dealer','MADHYA PRADESH','Balaghat','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        caseOEMPlant.CC_Email__c = 'ciba@extentor.com';
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }

    static testmethod void caseOEMDealerCCRoleNull() {
        Account acc = CEAT_InitializeTestData.createAccount('TestAcc','358338678');
        insert acc;
        Account accOE = CEAT_InitializeTestData.createAccount('TestAcc','358338634');
        accOE.recordTypeId = CEAT_InitializeTestData.createRecordType('OE', 'Account').Id;
        insert accOE;
        Contact con = CEAT_InitializeTestData.createContact('TestCon',acc.id);
        con.Email = 'kishlay.mathur@extentor.com';
        insert con;
        Matrix__c mat = CEAT_InitializeTestData.createEscMatrix('OEM - Dealer','','Sales','1','External','MIS Officer');
        insert mat;
        State_Master__c StateMaster = CEAT_InitializeTestData.createStateMaster('MADHYA PRADESH','Balaghat','Rampayli');
        StateMaster.RO__c =  'JAB';
        insert StateMaster ;
        Employee_Directory__c empDirRM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RM','Kishlay','Replacement','','');
        insert empDirRM;
        Employee_Directory__c empDirRCM = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','RCM','Kishlay','Replacement','','');
        insert empDirRCM;
        Employee_Directory__c empDirMiSOff = CEAT_InitializeTestData.createEmpDir('JAB','kishlay.mathur@extentor.com','MIS Officer','Kishlay','HO','','');
        insert empDirMiSOff; 
        Case caseOEMPlant = CEAT_InitializeTestData.createCaseReplacement('External','OEM - Dealer','Sales','Open','Test Subject',acc.id,con.id,'Email','External - OEM - Dealer','MADHYA PRADESH','Balaghat','1');
        caseOEMPlant.Dealer_Plant__c = accOE.id;
        insert caseOEMPlant ;
        caseOEMPlant.Nature__c = 'External';
        CaseHelper.run = true;
        update caseOEMPlant ;
        
    }*/
}