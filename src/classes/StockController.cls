global class StockController{  
    
    public Boolean isCstl                    {get;set;}
    public Boolean isRm                      {get;set;}
    public Boolean isZgm                     {get;set;}
    
    public Boolean isNotCstl                 {get;set;}
    public Boolean guestUser                 {get;set;}    
    public static PageReference page         {get;set;}
    
    public String loggedInUserId             {get;set;}
    public String loggedInUserRole           {get;set;}
    
    public Set<Stocks__c> selectPlantSet       {get;set;}
    
    public static String rmRole         = system.label.RM_Role;
    public static String tlRole         = system.label.TL_Role;
    public static String tldRole        = system.label.TLD_Role;
    String cstlRole                     = system.Label.CSTL_Role;  
    
    
    public StockController(){        
        
        isCstl                  = false;
        isNotCstl               = false;
        isRm                    = false;
        guestUser               = true;
        isZgm                   = false;
        
        
        
        /* Getting Logged in User Name and Territory */
        loggedInUserId   = UserInfo.getUserId();
        
        system.debug('userid@@@'+loggedInUserId);  
        user loggin      = [SELECT id,name,FirstName,UserRole.Name From User Where id =:loggedInUserId];  
        
        loggedInUserRole = loggin.UserRole.Name;
        if(loggedInUserRole != null && loggedInUserRole.contains('RM')){
            isCstl = false;
            isRm = true;
            isNotCstl = false;
            guestUser = false;
            loggedInUserRole = 'RM';           
        }else if(loggedInUserRole != null && loggedInUserRole.contains('TL') && (!loggedInUserRole.contains('TLD')) && (!loggedInUserRole.contains('CSTL'))){
            loggedInUserRole = 'TL';
            isCstl = false;
            isRm = false;
            isNotCstl = true;
            guestUser = false;         
        }else if(loggedInUserRole != null && loggedInUserRole.contains('TLD')){
            loggedInUserRole = 'TLD';  
            isCstl = false;
            isRm = false;
            isNotCstl = true;
            guestUser = false;     
        }else if(loggedInUserRole != null && loggedInUserRole.contains('CSTL')){
            loggedInUserRole = 'CSTL';
            isCstl    = true;
            isNotCstl = false;
            isRm      = false;
            guestUser = false;         
        }else if(loggedInUserRole != null && loggedInUserRole.contains('ZGM')){
            loggedInUserRole = 'ZGM';
            isZgm     = true; 
            isCstl    = false;
            isNotCstl = false;
            isRm      = false;
            guestUser = false;         
        } 
    }  
    
    @RemoteAction
    global Static PageReference getPage(string loggedInUserRole,String plantSelected,String selectedRecordType,String categorySelected){
        
        String loggedInUserId = UserInfo.getUserId();   
        system.debug('userid@@@'+loggedInUserId);   
        Server_Url__c serverUrl;
        string role  = '';
        
        /* if((selectedRole == 'All' && selectedUserRole != 'none') || (selectedRole != 'All' && selectedUserRole != 'none')){
User u = [select id,name,UserRole.DeveloperName from User where id =:selectedUserRole limit 1];
role = u.UserRole.DeveloperName ;
}
*/
        serverUrl=[select url__c from Server_Url__c limit 1];
        
        
        if(loggedInUserRole != '' ){
            page = new PageReference(serverUrl.url__c+'/apex/'+Constants.StockPage);  
        }
        else{
            //else It should redirect to home page
            page = new PageReference(serverUrl.url__c+'/home/home.jsp');                    
        }         
        
        return page;
    }
    
    
    @RemoteAction
    global static Set<SelectOption> plantSet(String RecordType){
        String selectedRecordType ;
        if(RecordType == 'Pull'){
        selectedRecordType = 'Pull';
        }
        else{
        selectedRecordType = 'Non_Pull';
        }
        
        Set<SelectOption> plantSet = new Set<SelectOption>();
        String loggedInUserId = UserInfo.getUserId();
        User currentUser = [SELECT id,name,FirstName,UserRole.Name, ProfileId, Profile.Name From User where Id =:loggedInUserId];
        String usrRole = currentUser.UserRole.Name;
        Id usrProfileId = currentUser.ProfileId;
        String profileName = currentUser.Profile.Name;
        
        if(usrRole.contains('RM') || usrRole.contains('RBM') || usrRole.contains('TL') || usrRole.contains('CSTL') ){
            //plantSet.add(new SelectOption('None','None'));
            Id stockheaderID = [SELECT id,DeveloperName FROM RecordType where DeveloperName =: selectedRecordType ].Id;
            String DevNameRO ='';
            List<UserTerritory2Association> terList = [SELECT Territory2Id,RoleInTerritory2,UserId FROM UserTerritory2Association where UserId =: loggedInUserId];
            
            if(terList[0].RoleInTerritory2.contains('TL')  || terList[0].RoleInTerritory2.contains('TLD') || terList[0].RoleInTerritory2.contains('CSTL')){
                Id DevNameTerritory = [SELECT DeveloperName,Id,ParentTerritory2Id FROM Territory2 where Id =:terList[0].Territory2Id].ParentTerritory2Id ;
                DevNameRO = [SELECT DeveloperName,Id FROM Territory2 where Id=: DevNameTerritory ].DeveloperName;
                system.debug('Terr..'+DevNameTerritory +'Ro...'+DevNameRO );
            }
            else{
                DevNameRO = [SELECT DeveloperName,Id FROM Territory2 where Id =: terList[0].Territory2Id].DeveloperName;
            }
            
             List<AggregateResult> plantList = [Select Zyhcf__c  from Stocks__c where Zyhrf_Code__c =:DevNameRO And RecordTypeId=:stockheaderID group by Zyhcf__c order by Zyhcf__c   ];
            for(AggregateResult plantOptions: plantList){
                plantSet.add(new SelectOption('None','None' ));
                plantSet.add(new SelectOption((String)plantOptions.get('Zyhcf__c'),(String)plantOptions.get('Zyhcf__c') ));
            }
        }
        else{
            //plantSet.add(new SelectOption('None','None'));
            Id stockheaderID = [SELECT id,DeveloperName FROM RecordType where DeveloperName =:selectedRecordType ].Id;
            
            List<AggregateResult> plantList = [Select Zyhrf__c,Zyhcf__c from Stocks__c where RecordTypeId=:stockheaderID  group by Zyhrf__c,Zyhcf__c order by Zyhcf__c];
            for(AggregateResult plantOptions: plantList){
                plantSet.add(new SelectOption('None','None' ));
                plantSet.add(new SelectOption((String)plantOptions.get('Zyhcf__c'),(String)plantOptions.get('Zyhcf__c') ));
            }
            
            /*Schema.DescribeFieldResult fieldResult =  Stocks__c.Zyhcf__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();            
            for( Schema.PicklistEntry f : ple)
            {
                plantSet.add(new SelectOption(f.getLabel(), f.getValue()));
            } */  
        }
        
        
        return plantSet;
    }
    
    
    @RemoteAction
    global static Set<SelectOption> categorySet(String CFAcode){
        Set<SelectOption> categorySet = new Set<SelectOption>();
        categorySet.add(new SelectOption('All Selected','All Selected'));
        Id stockheaderID = [SELECT id,DeveloperName FROM RecordType where DeveloperName = 'Stock_Header'].Id;
        List<Stocks__c> catList = new List<Stocks__c>();
        catList = [Select Zyhcf__c,Category__c from Stocks__c where Zyhcf__c =:CFAcode and RecordTypeId !=: stockheaderID order by Category__c ASC limit 50000];
        if(catList!= null){
        for(Stocks__c catSet :catList){
            categorySet.add(new SelectOption(catSet.Category__c,catSet.Category__c));
        }
        }
        return categorySet;
    }       
    
    
}