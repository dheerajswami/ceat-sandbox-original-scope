public class GetSelectedFPs {
    /*
     * Author  :- Swayam Arora  
     * Purpose :- Used for submission of multiple FPs to ZM/ZGM 
     *            
     *
     *
     */

    ApexPages.StandardSetController setCon;

    public GetSelectedFPs(ApexPages.StandardController controller) {
        
    }

    public GetSelectedFPs(ApexPages.StandardSetController controller) {
        setCon=controller;
    }
    
    public PageReference runSelectedRules() {
        String[] addresses = new String[]{}; 
        
        Id gotId;
            
        Set<id> fpIds = new Set<Id>();
        if(setCon.getSelected().size() > 0) {
            for(sObject s: setCon.getSelected()){
            fpIds.add(s.Id);
            }
            List<Fortnightly_Action_Plan__c> fpList = [Select OwnerId, Achievement__c from Fortnightly_Action_Plan__c where Id In :fpIds];
           for(Fortnightly_Action_Plan__c fp : fpList) {
               if(fp.Achievement__c == Null) {
                    fp.addError('Please enter Achievements before submitting for reviews.');
                }
            }
           gotId = fpList[0].OwnerId;
            
            addresses.add('swayam.arora@extentor.com');
    
            UtilityClass.sendEmail(gotId, addresses, System.Label.Email_on_Submission_of_Fortnightly_Plans);
        } else {
            Fortnightly_Action_Plan__c fp = new Fortnightly_Action_Plan__c();
            fp.addError('Please select one or more Fortnightly Plans before submitting.');
        }
     
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        return new PageReference('/a04/o');     
    }    
}