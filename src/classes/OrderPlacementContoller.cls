global class OrderPlacementContoller {
    //public Account dealer{get;set;}
    
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
	static Product_Division__c firstElem; 
    
    /*
    Fetch All SAP Customer Basic 
    */
    @RemoteAction
    global static Account getAccountInfo(String accId,Boolean isPasscode){
        if(isPasscode){
            return [SELECT Id,Name,Sales_District_Text__c,Distribution_Channel_Text__c,Division_Text__c,Passcode__c,UniqueIdentifier__c,Street_1__c,Street_2__c,Town__c,District__c,State__c,PARNR__c FROM Account WHERE Passcode__c =:accId];//LIMIT 100       
        }else{
            return [SELECT Id,Name,Sales_District_Text__c,Distribution_Channel_Text__c,Division_Text__c,Passcode__c,UniqueIdentifier__c,Street_1__c,Street_2__c,Town__c,District__c,State__c,PARNR__c FROM Account WHERE Id =:accId];//LIMIT 100    
        }        
    }
    


    
    @RemoteAction
    global static List<Product_Division__c> getProductDivision(){
    	
    	List<Product_Division__c> productsList = new List<Product_Division__c>();
    	for(Product_Division__c pd :[select Id,Name,Code__c,Sort_Order__c from Product_Division__c order by Sort_Order__c asc]){
    		productsList.add(pd);
    	}
    	
    	
        return productsList;
    }

    @RemoteAction
    global static List<Material_Master_Sap__c> getMaterialDetails(String mat,String division_v){
        
        String[] wildcard = mat.split(' ');
        String finalSearchStr = '';
        //if(wildcard.size() > 0){
            for(String str : wildcard){
                finalSearchStr += str+'%';
            }
        //}
        List<Material_Master_Sap__c> matMast = new List<Material_Master_Sap__c>();

        return [SELECT Id,Name,Tyre_Size__c,Material_Group__c,Pattern_Brand__c,Mat_Grp_Desc__c,Ply_Rating__c,Material_Number__c 
                FROM Material_Master_Sap__c 
                WHERE Division__c=:division_v AND (Plain_Desc__c LIKE  :('%'+finalSearchStr+'%')  OR Name LIKE : ('%'+finalSearchStr+'%') OR Tyre_Size__c LIKE :('%'+finalSearchStr+'%') OR Ply_Rating__c LIKE :('%'+finalSearchStr+'%') OR Material_Number__c LIKE :('%'+finalSearchStr+'%'))  LIMIT 50];
        /*return [SELECT Id,Name,Tyre_Size__c,Material_Group__c,Mat_Grp_Desc__c,Ply_Rating__c,Material_Number__c 
                FROM Material_Master_Sap__c 
                WHERE Material_Number__c LIKE  :('%'+mat+'%')  OR Name LIKE  :('%'+mat+'%') OR Tyre_Size__c LIKE :('%'+mat+'%') OR Ply_Rating__c LIKE :('%'+mat+'%') LIMIT 50];*/
    }

    @RemoteAction
    global static List<String> getMaterialGroup(String division_v){
        system.debug(division_v+'division_v');
        List<String> categories = new List<String>();

        for(AggregateResult ar : [SELECT Mat_Grp_Desc__c FROM Material_Master_Sap__c WHERE Division__c=:division_v group by Mat_Grp_Desc__c]){
            categories.add(String.valueOf(ar.get('Mat_Grp_Desc__c')));
        }
        return categories;
    } 

    @RemoteAction
    global static List<Material_Master_Sap__c> fetchProductsCategoryW(String cate, String division_v){
        return [SELECT Id,Tyre_Size__c 
                FROM Material_Master_Sap__c 
                WHERE Mat_Grp_Desc__c = :cate and Division__c=:division_v order by Tyre_Size__c asc];
    }

    @RemoteAction
    global static List<Material_Master_Sap__c> fetchProductsSize(String size,String cate){
        return [SELECT Id,Ply_Rating__c 
                FROM Material_Master_Sap__c 
                WHERE Tyre_Size__c = :size AND Mat_Grp_Desc__c = :cate order by Ply_Rating__c asc];
    }
    
    @RemoteAction
    global static List<Material_Master_Sap__c> fetchProductsPlayRating(String plr,String size,String cate){
        system.debug(plr + '' + size + ' ' + cate);
        return [SELECT Id,Name,Tyre_Size__c,Pattern_Brand__c,Material_Group__c,Mat_Grp_Desc__c,Ply_Rating__c,Material_Number__c 
                FROM Material_Master_Sap__c 
                WHERE Tyre_Size__c = :size AND Mat_Grp_Desc__c = :cate AND Ply_Rating__c = :plr AND (Pattern_Brand__c != null OR Pattern_Brand__c != '') order by Pattern_Brand__c asc];
    }
    
    @RemoteAction
    global static List<SapUnrestrictedStock_Mapping.UnrestrictedStockMapping> fetchUnrestrictedStock(String salesDistrict) {
        //salesDistrict = 'B0021';        
        List<SapUnrestrictedStock_Mapping.UnrestrictedStockMapping> stock = SapUnrestrictedStock_Mapping.getAllUnrestrictedStock(salesDistrict);
        return stock;        
    }

    global class ReponseOrderCreation{
        public String mesg {get;set;}
        public String ulrN {get;set;}
        public Boolean naviga {get;set;}

        public ReponseOrderCreation(String m,String u,Boolean n){
            this.mesg = m;
            this.ulrN = u;
            this.naviga = n;
        }

    }
    
    @RemoteAction
    global static ReponseOrderCreation submitOrder(Account acc,List<Order_Line_Items__c> olis,String shipToAddress,String division_v) { 
        
        List<SapOrderCreationSoap.ZSFDC_SO_H> order_header = new List<SapOrderCreationSoap.ZSFDC_SO_H>();
        List<SapOrderCreationSoap.ZSFDC_SO_I> order_items  = new List<SapOrderCreationSoap.ZSFDC_SO_I>();
        
        Order order = new Order();

        SapOrderCreationSoap.ZSFDC_SO_H h = new SapOrderCreationSoap.ZSFDC_SO_H();
        h.AUART = Label.ORDER_SalesDocNum;
        order.Sales_Document_Type__c = Label.ORDER_SalesDocNum; 
        h.VKORG = Label.ORDER_SalesOrg;
        order.Sales_Organization__c = Label.ORDER_SalesOrg;
        h.VTWEG = (acc.Distribution_Channel_Text__c != null ? UtilityClass.addleadingZeros(acc.Distribution_Channel_Text__c,2) : '') ; 
        order.AccountId = acc.Id;
        order.Division_Text__c = (division_v != null ? UtilityClass.addleadingZeros(division_v,2) : '');
        h.SPART = (division_v != null ? UtilityClass.addleadingZeros(division_v,2) : '');         
        h.AUDAT = Date.today().year()+'-'+(String.valueOf(Date.today().month()).length() == 1 ? '0'+Date.today().month() : String.valueOf(Date.today().month()))+'-'+(String.valueOf(Date.today().day()).length() == 1 ? '0'+Date.today().day() : String.valueOf(Date.today().day()));
        system.debug(division_v);
        h.KUNNR = UtilityClass.addleadingZeros(acc.UniqueIdentifier__c,Integer.valueOf(Label.CPORT_LengthOfCustomerId)); 
        
        order_header.add(h);
        
        system.debug('HEADER ' + h);
        
        Boolean flag = false;

        for(Order_Line_Items__c oli : olis) {
            
            if(oli.Flag__c == 'Send to SAP'){
                SapOrderCreationSoap.ZSFDC_SO_I i = new SapOrderCreationSoap.ZSFDC_SO_I();
                i.MATNR = UtilityClass.addleadingZeros(oli.Material_Number__c,18);
                i.WERKS = oli.Plant__c;
                i.LGORT = oli.Storage_Location__c;
                System.debug(Integer.valueOf(oli.Quantity__c));
                i.MENGE = String.valueOf(Integer.valueOf(oli.Quantity__c));
                i.ABGRU = '';
                order_items.add(i);
                flag = true;  
            }else{
            	SapOrderCreationSoap.ZSFDC_SO_I i = new SapOrderCreationSoap.ZSFDC_SO_I();
                i.MATNR = UtilityClass.addleadingZeros(oli.Material_Number__c,18);
                i.WERKS = oli.Plant__c;
                i.LGORT = oli.Storage_Location__c;
                System.debug(Integer.valueOf(oli.Quantity__c));
                i.MENGE = String.valueOf(Integer.valueOf(oli.Quantity__c));
                i.ABGRU = (oli.Flag__c == 'Original' ?'16':'01'); // PROD
                
                //i.ABGRU = '16'; // SANDBOX
                system.debug(oli.Flag__c);
                order_items.add(i);
            }
            
        }
        
        String msg;

        //if(flag){
            //system.debug('ITEMS' + order_items);
            msg = SapOrderCreation_Mapping.createOrder(order_header, order_items);
        //}    
        
        String bURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        
        if(msg != null && msg.contains('Successfuly')) {
            try {
                String orderNum = msg.split(' ')[2];
                order.OrderReferenceNumber = orderNum;
                order.EffectiveDate = Date.today();
                order.Document_Date_in_Document__c = Date.today();
                order.Status = 'Submitted to SAP';
                order.Error_Message__c = msg;
                order.Description = shipToAddress;
                //insert order;
                Database.SaveResult srList = Database.insert(order);

                

                for(Order_Line_Items__c oli : olis) {
                    oli.Related_Order__c = order.Id;                
                } 
                insert olis;   

                
                String rp =  'Order created successfully in SAP. Please note order number '+orderNum+' for future reference.';
                ReponseOrderCreation resp = new ReponseOrderCreation(rp,bURL+'/'+srList.getId(),true);
                return resp;
            } catch(Exception e) {
                system.debug(e.getMessage());
                //return 'Order not created.';
                ReponseOrderCreation resp = new ReponseOrderCreation('Order not created.','',false);
                return resp;
            }            
        } else {

            try {
                //String orderNum = msg.split(' ')[2];
                //order.OrderReferenceNumber = orderNum;
                order.EffectiveDate = Date.today();
                order.Document_Date_in_Document__c = Date.today();
                order.Status = (flag ?'Failed' : 'Draft') ;
                order.Error_Message__c = msg;
                Database.SaveResult srList = Database.insert(order);
                for(Order_Line_Items__c oli : olis) {
                    oli.Related_Order__c = order.Id;
                                
                } 
                insert olis;   
                String rp =  'Order created successfully in SFDC';
                ReponseOrderCreation resp = new ReponseOrderCreation(rp,bURL+'/'+srList.getId(),true);
                return resp;
            } catch(Exception e) {
                system.debug(e.getMessage());
                ReponseOrderCreation resp = new ReponseOrderCreation('Order not created.','',false);
                return resp;
            }
            
        }        
    }

    @RemoteAction
    global static List<String> fetchShipToAddress(String cusNum){
        return getCustomerShipToAddress(cusNum);
    } 

    /* Call SAP for ship to address */
    WebService static List<String> getCustomerShipToAddress(String cusNum){
        
        List<String> addresses = new List<String>();
        
        String customerId = '';
        try{
        
        if(cusNum!=null){
            customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
        }
        
        SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        sapShipToAddress  scd                           = new sapShipToAddress();
        sapShipToAddress.zws_SFDC_STP_ADDRESS  zws      = new sapShipToAddress.zws_SFDC_STP_ADDRESS  ();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;
        sapShipToAddress.TABLE_OF_ZSFDC_STP   tom       = new sapShipToAddress.TABLE_OF_ZSFDC_STP();
              
                
        List<sapShipToAddress.ZSFDC_STP> m              = new List<sapShipToAddress.ZSFDC_STP>();        
        
        tom.item = m;
        
        system.debug('customerId--'+customerId);
            
            
            
        tom = zws.ZSFDC_STP_ADDRESS(customerId,tom);            
            //system.assertEquals(e,null);
        if(tom.item != null){            
            //system.debug(tom.item);
            for(sapShipToAddress.ZSFDC_STP addr : tom.item){
                addresses.add(addr.ADDRESS);
            }
        }
            
            return addresses;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        return null;
     }

}