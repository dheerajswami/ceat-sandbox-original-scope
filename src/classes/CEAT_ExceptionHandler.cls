public with sharing class CEAT_ExceptionHandler {
   
    public CEAT_ExceptionHandler() {}
    public String errorMessage = '';
    public void validateLineItemList(List<PI_Line_Item__c> lineItemList) {
        if(lineItemList.isEmpty() && lineItemList != null) {
            throw new customException('Please add atleast one PI Line Item to save.');
        }
    }
    public void ValidateCustomerFields(String accountId, String shipToParty) {
        System.debug('==#9 '+accountId);
        System.debug('==#10 '+shipToParty);
        if(accountId == null || accountId == '') {
            throw new customException('Please choose a Customer!!');
        }
        if(shipToParty == '' || shipToParty == null || shipToParty == '--None--') {
            throw new customException('Please choose a Ship to Party!!');
        }
    }

    public void ValidateMandatoryFields(String requestedDelvDate, String customerId, String advancePayment, String advancedPaymentDetails, String nominationOfShippingLine, String shippingLineName, String clubbingOfcontainers, String clubbingDetails, String inspectionRequired, String inspectionAgencyName,String DivPicklist,String DisChannelPicklist,string SDPicklist,string SDDcoc,string incoterms,String Mix,String ShipTocoun,String PostDest,String LicReq,String PLReq,String VehType,String cuscont,string LCReq) {
        
        
        if(requestedDelvDate == '' ||DivPicklist == '' || DisChannelPicklist == '' || SDPicklist == '' ||
            SDDcoc== '' ||  incoterms== '' || Mix== '' || ShipTocoun== '' || PostDest== '' || LicReq== '' || 
            PLReq== '' || VehType== '' || cuscont== '' || LCReq== '' || nominationOfShippingLine=='' || 
            clubbingOfcontainers=='' || inspectionRequired== '' || advancePayment=='') {


            if(DivPicklist == ''){
            errorMessage += 'Division Picklist.' ;
            //throw new customException(errorMessage);
            }
            if(DisChannelPicklist == ''){
                errorMessage += 'Distribution Channel Picklist.';
            //throw new customException(errorMessage);
            }
            if(requestedDelvDate == ''){
                errorMessage += 'Requested Delivery Date.';
            }
            if(SDPicklist == ''){
                errorMessage += 'Sales Document Type Picklist.';
            //throw new customException(errorMessage);
            }
            if(SDDcoc== ''){
                errorMessage += 'SD Document Currency. \r\n';
            //throw new customException(errorMessage);
            }
            /*if(TermsofPay == '' && customerId == null){
            errorMessage += 'Terms Of Payment. \r\n';
            }*/
            if(incoterms== ''){
                errorMessage += 'Inco Terms. \r\n';
            //throw new customException(errorMessage);
            }
            if(Mix== ''){
                errorMessage += 'Mixing of Proforma.';
            //throw new customException(errorMessage);
            }
            if(ShipTocoun== ''){
                errorMessage += 'Ship To Country.';
            //throw new customException(errorMessage);
            }
            if(PostDest== '--None--'){
                errorMessage += 'Port of Destination.';
            //throw new customException(errorMessage);
            }
            if(LicReq== ''){
                errorMessage += 'Import License Requirement.';
            //throw new customException(errorMessage);
            }
            if(PLReq== ''){
                errorMessage += 'PackingList License Requirement.';
            //throw new customException(errorMessage);
            }
            if(VehType== ''){
                errorMessage += 'Vehicle Type.';
            //throw new customException(errorMessage);
            }
            if(cuscont== ''){
                errorMessage += 'Customized Containers.';
            }
            if(advancePayment=='') {
                errorMessage += 'Advance Payment.';
            }
            if(nominationOfShippingLine== ''){
                errorMessage += 'Nomination of shipping line.';
            }
            if(clubbingOfcontainers== ''){
                errorMessage += 'Clubbing of containers.';
            }
            if(inspectionRequired== ''){
                errorMessage += 'Inspection Required.';
            }
            if(LCReq== ''){
                errorMessage += 'LC Required.';
            }
            
            throw new customException(errorMessage);
        }
        if((advancePayment == 'Yes' && advancedPaymentDetails == '') || 
            (nominationOfShippingLine == 'Yes' && shippingLineName == '') || 
            (clubbingOfcontainers == 'Yes' && clubbingDetails == '') || 
            (inspectionRequired == 'Yes' && inspectionAgencyName == '')) {

            if(advancePayment == 'Yes' && advancedPaymentDetails == '') {
                errorMessage += 'Please Provide Advanced Payment Details. ';
            }
            if(nominationOfShippingLine == 'Yes' && shippingLineName == '') {
                System.debug('Inside Exception If');
                errorMessage += 'Please Provide Shipping Line Name. ';
            }
            if(clubbingOfcontainers == 'Yes' && clubbingDetails == '') {
                errorMessage += 'Please Provide Clubbing Details. ';
            }
            if(inspectionRequired == 'Yes' && inspectionAgencyName == '') {
                errorMessage += 'Please Provide Inspection Agency Name. ';
            }
            throw new customException(errorMessage);
        }
        
    }

/*-------------------------ADDED BY SUPRIYA ---------------------------*/
// Following methods used in ComplaintsBpoController to validate Complaints Fields
    
    //added to validate serial number logic
   

    public void ValidateBpoMandateFields(String nature,String businessCat,String complaintType,String stateVal,
                                         String districtVal,String townVal,String clusterVal,ID customerName,Id dealerPlant,String customerphone,
                                         String keyWord,String keywordInternal,String issue,String detailsOfComplaints,
                                         String newComment,String Product){
        system.debug('inside validate method'+dealerPlant);
        if(nature == '' || businessCat == '' || complaintType == '' || issue ==''
            || detailsOfComplaints == '' || newComment =='' ){

            if(nature == ''){
                errorMessage += 'Nature' + '<br/>';
            }
            if(businessCat == ''){
                errorMessage += 'Business Category'+ '<br/>';    
            }
            if(complaintType == ''){
                errorMessage += 'Complaint Type'+ '<br/>';    
            }
            
            if(issue == '' || issue == null || issue == 'undefined'){
                errorMessage += 'Issue'+ '<br/>';    
            }
            if(nature !='' && (detailsOfComplaints == '' || detailsOfComplaints == null || detailsOfComplaints == 'undefined')){
                errorMessage += 'Details Of Complaints'+ '<br/>';    
            }
            if(newComment == '' || newComment == null ||newComment == 'undefined'){
                errorMessage += 'New Comment'+ '<br/>';    
            }
            if(Product == '' && complaintType == 'Customer Service'){
                errorMessage += 'Product'+'<br/>';
            }

            throw new customException(errorMessage);
        }
        if((businessCat == 'Specialty' || businessCat =='OEM - Dealer' || businessCat == 'OEM - Plant') && String.ValueOf(dealerPlant) ==null ){
            system.debug('Enetred specialty' +String.ValueOf(dealerPlant));
            if(String.ValueOf(dealerPlant) ==null){
            errorMessage += 'Dealer Plant' +'<br/>';
        }
            throw new customException(errorMessage);
        }

        if((nature == 'External' && (keyWord == ''|| keyWord == null|| keyWord == 'undefined')) || (nature == 'Internal' && (keywordInternal == ''|| keywordInternal==null||keywordInternal == 'undefined'))){
            
                errorMessage += 'Keyword'+ '<br/>';    
            
            throw new customException(errorMessage);
        }
        /*else{
            if(keywordInternal == ''){
                errorMessage += 'Keyword'+ '<br/>';    
            }
            throw new customException(errorMessage);
        }*/
        
        //if((businessCat != 'Exports' && businessCat != 'OEM - Plant')  && 
        //    (stateVal == '' || districtVal == '' )){
        //    if(stateVal == ''){
        //        errorMessage += 'State'+ '<br/>';    
        //    }
        //    if(districtVal == ''){
        //        errorMessage += 'District'+ '<br/>';    
        //    }
        //    /*if(townVal == '--None--'){
        //        errorMessage += 'Town'+ '<br/>';    
        //    }*/
        //     throw new customException(errorMessage );
        //}
        //if(String.ValueOf(customerName) == null || customerphone == null){
        //    if(String.ValueOf(customerName) == null){
        //        errorMessage += 'Customer Name'+ '<br/>';    
        //    }
        //    if(customerphone == null){
        //        errorMessage += 'Customer Phone'+ '<br/>';    
        //    }
        //    throw new customException(errorMessage );
        //}

        //if((businessCat == 'Specialty' || businessCat =='OEM - Dealer') && String.ValueOf(dealerPlant) ==null ){
        //    system.debug('Enetred specialty' +String.ValueOf(dealerPlant));
        //    if(String.ValueOf(dealerPlant) ==null){
        //    errorMessage += 'Dealer Plant' +'<br/>';
        //}
        //    throw new customException(errorMessage);
        //}

        //if(businessCat == 'Exports' && (clusterVal == '' || String.ValueOf(customerName) == null || customerphone == null)){
        //        if(clusterVal == ''){
        //            errorMessage += 'Cluster'+ '<br/>';
        //        }
        //        if(String.ValueOf(customerName) == null){
        //            errorMessage += 'Customer Name'+ '<br/>';
        //        }
        //        if(customerphone == null){
        //            errorMessage += 'Phone Number'+ '<br/>';
        //        }
        //        throw new customException(errorMessage);
        //    }
            
    }


    public void validateSerialNumberLogic(String complaintType,String product,String inspection,String material,String materialCode,String plantCode,String weekVal,String monthVal,
                                        String sNo,String yearVal){
        system.debug('123');
        if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (plantCode==''||plantCode==null ||yearVal==''||yearVal==null
            ||sNo ==''||sNo==null||weekVal==''||weekVal==null) && (materialCode=='2010'
                    ||materialCode=='2015'||materialCode=='2170'||materialCode=='2150'
                    ||materialCode=='2180'||materialCode=='2030'||materialCode=='2040')){
                    system.debug('@1234'+plantCode);
                    if( plantCode==''||plantCode== null){
                        errorMessage+= 'Plant'+'<br/>';
                    }
                    if(yearVal == ''||yearVal==null){
                        errorMessage+= 'Year'+'<br/>';
                    }
                    if(sNo ==''||sNo==null){
                        errorMessage+='S No'+'<br/>';
                    }
                    if(product=='Tyre'&& weekVal=='' || weekVal == null){
                        errorMessage += 'Week'+ '<br/>';
                    }
                throw new customException(errorMessage);
                    
                }
        if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (plantCode==''||plantCode==null ||yearVal==''||yearVal==null
            ||sNo ==''||sNo==null||weekVal==''||weekVal==null) && (materialCode!='2010'
                    &&materialCode!='2015'&&materialCode!='2170'&&materialCode!='2150'
                    &&materialCode!='2180'&&materialCode!='2030'&&materialCode!='2040')){
                    system.debug('@1234'+plantCode);
                    if( plantCode==''||plantCode== null){
                        errorMessage+= 'Plant'+'<br/>';
                    }
                    if(yearVal == ''||yearVal==null){
                        errorMessage+= 'Year'+'<br/>';
                    }
                    
                    if(product=='Tyre' && weekVal=='' || weekVal == null){
                        errorMessage += 'Week'+ '<br/>';
                    }
                throw new customException(errorMessage);
                    
                }

        if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (plantCode==''||plantCode==null ||yearVal==''||yearVal==null
            ||sNo ==''||sNo==null||weekVal==''||weekVal==null) && (materialCode=='2010'
                    ||materialCode=='2015'||materialCode=='2170'||materialCode=='2150'
                    ||materialCode=='2180'||materialCode=='2030'||materialCode=='2040')){

                    if( plantCode==''||plantCode== null){
                        errorMessage+= 'Plant'+'<br/>';
                    }
                    if(yearVal == ''||yearVal==null){
                        errorMessage+= 'Year'+'<br/>';
                    }
                    if(sNo ==''||sNo==null){
                        errorMessage+='S No'+'<br/>';
                    }
                    if(product!='Tyre' && monthVal == ''){
                       errorMessage += 'Month'+ '<br/>'; 
                    }
                    throw new customException(errorMessage);
                
                }

        if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (plantCode==''||plantCode==null ||yearVal==''||yearVal==null
            ||sNo ==''||sNo==null||weekVal==''||weekVal==null) && (materialCode!='2010'
                    &&materialCode!='2015'&&materialCode!='2170'&&materialCode!='2150'
                    &&materialCode!='2180'&&materialCode!='2030'&&materialCode!='2040')){

                    if( plantCode==''||plantCode== null){
                        errorMessage+= 'Plant'+'<br/>';
                    }
                    if(yearVal == ''||yearVal==null){
                        errorMessage+= 'Year'+'<br/>';
                    }
                    if(sNo ==''||sNo==null){
                        errorMessage+='S No'+'<br/>';
                    }
                    if(monthVal == ''){
                       errorMessage += 'Month'+ '<br/>'; 
                    }
                    throw new customException(errorMessage);
                
                }        


         if((complaintType == 'Customer Service') &&(product == 'Tyre' && weekVal =='')) {
                    system.debug(material+'this is material %%%');
                    if(weekVal==''){
                        errorMessage += 'Week'+ '<br/>';
                    }
                    throw new customException(errorMessage);
                }
                if((complaintType == 'Customer Service') &&(product != 'Tyre' && monthVal == '')) {  
                    if(monthVal == ''){
                       errorMessage += 'Month'+ '<br/>'; 
                    }
                    throw new customException(errorMessage);
           }
    }
    public void ValidateOtherUserFields(String complaintType,String product,String tyre_Size,String tyre_Pattern,
                                        String material,String materialCode,String plantCode,String weekVal,String monthVal,
                                        String sNo,String yearVal,String status,String comment,
                                        String inspection,String disposition,String defect_Type,                                                  
                                        String source,String remark,Decimal nSD,String actualInspectionDate){
            System.debug('==Til HereNSD '+nSD);
            if((complaintType == 'Customer Service') &&(product == '' || tyre_Size == '--None--' ||  material == '' )){ 

                if(product == ''){
                    errorMessage += 'Product'+ '<br/>';    
                }
                if(tyre_Size == ''){
                    errorMessage += 'Tyre Size'+ '<br/>';    
                }
                //if(tyre_Pattern == ''){
                //    errorMessage += 'Tyre Pattern'+ '<br/>';    
                //}
                if(material == ''){
                    errorMessage += 'Material'+ '<br/>';    
                }
                
                throw new customException(errorMessage);
            }

            if(status == '' ||comment == '' ) {
                if(status == ''){
                    errorMessage += 'Status'+'<br/>';    
                }
                if(comment == ''){
                    errorMessage += 'Comment'+ '<br/>';    
                }
                
                throw new customException(errorMessage);
            }
            System.debug('==Till Here');
                if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (product == 'Tyre') && (materialCode=='2010'
                    ||materialCode=='2015'||materialCode=='2170'||materialCode=='2150'
                    ||materialCode=='2180'||materialCode=='2030'||materialCode=='2040')){

                    if(plantCode=='' || plantCode== null){
                        errorMessage+= 'Plant'+'<br/>';
                    }
                    if((complaintType == 'Customer Service') &&(product == 'Tyre' && weekVal =='')) {
                    system.debug(material+'this is material %%%');
                    if(weekVal==''){
                        errorMessage += 'Week'+ '<br/>';
                    }

                }
                    throw new customException(errorMessage);
                }
                if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (product != 'Tyre') && (materialCode=='2010'
                    ||materialCode=='2015'||materialCode=='2170'||materialCode=='2150'
                    ||materialCode=='2180'||materialCode=='2030'||materialCode=='2040')){

                if((complaintType == 'Customer Service') &&(product != 'Tyre' && monthVal == '')) {  
                    if(monthVal == ''){
                       errorMessage += 'Month'+ '<br/>'; 
                    }
                    throw new customException(errorMessage);
                }
                }
                if((complaintType == 'Customer Service') &&(product == 'Tyre' && weekVal =='')) {
                    system.debug(material+'this is material %%%');
                    if(weekVal==''){
                        errorMessage += 'Week'+ '<br/>';
                    }
                    throw new customException(errorMessage);
                }
                if((complaintType == 'Customer Service') &&(product != 'Tyre' && monthVal == '')) {  
                    if(monthVal == ''){
                       errorMessage += 'Month'+ '<br/>'; 
                    }
                    throw new customException(errorMessage);
                }
                if((complaintType == 'Customer Service') && (inspection == 'On Spot') && (disposition == '' ||defect_Type == '' ||
               source == '' ||remark == '' ||nSD == null ||actualInspectionDate == '' )){
                    if(disposition == ''){
                    errorMessage += 'Disposition'+ '<br/>';    
                }
                if(defect_Type == ''){
                    errorMessage += 'Defect Type'+ '<br/>';    
                }
                if(source == ''){
                    errorMessage += 'Source'+ '<br/>';    
                }
                if(remark == ''){
                    errorMessage += 'Remark'+ '<br/>';    
                }
                
                if(nSD == null){
                    errorMessage += 'NSD'+ '<br/>';    
                }
                if(actualInspectionDate == ''){
                    errorMessage += 'Actual Inspection Date'+ '<br/>';
                }
                    throw new customException(errorMessage);
            }

                
    }

    public void validateSecondEditForBpo(String status,String comment){
        if(status == '' || comment == ''){
            if(status ==''){
                errorMessage += 'Status' + '<br/>';
            }

            if(comment == ''){
                errorMessage += 'New Comment' + '<br/>';
            }
            throw new customException(errorMessage);
        }
    }

    public void validateSecondEditForOtherUsers(String status,String comment,String revisit,String reason,String inspection,String disposition,String defect_Type,                                                  
                                        String source,String remark,Decimal nSD,String actualInspectionDate ){
        system.debug('***!');
        if(status == '' || comment == ''){
            if(status ==''){
                system.debug('##1');
                errorMessage += 'Status' + '<br/>';
            }

            if(comment == ''){
                system.debug('##2');
                errorMessage += 'New Comment' + '<br/>';
            }
            throw new customException(errorMessage);
        }
        if(revisit != '' && reason == ''){
            if(reason == ''){
                system.debug('##3');
                errorMessage += 'Reason for Revisit' + '<br/>';
            }
            throw new customException(errorMessage);
        }

        if(inspection == 'On Spot' && (disposition == '' || defect_Type == '' ||
               source == '' ||remark == '' ||nSD == null ||actualInspectionDate == '' )){
            system.debug('##4');
                    if(disposition == ''){
                    errorMessage += 'Disposition'+ '<br/>';    
                }
                if(defect_Type == ''){
                    errorMessage += 'Defect Type'+ '<br/>';    
                }
                if(source == ''){
                    errorMessage += 'Source'+ '<br/>';    
                }
                if(remark == ''){
                    errorMessage += 'Remark'+ '<br/>';    
                }
                
                if(nSD == null){
                    errorMessage += 'NSD'+ '<br/>';    
                }
                if(actualInspectionDate == ''){
                    errorMessage += 'Actual Inspection Date'+ '<br/>';
                }
                    throw new customException(errorMessage);
            }
    }
  


  /************** Used in Claim Controller **************/

    public void validateclaimFields(Claim__c newClaim) {
        if(newClaim.Warranty_Card_Number__c != '' && newClaim.Date_of_Warranty_Card__c == null) {
            errorMessage += 'Date in Warrenty Card.';
        }
    }


  public class customException extends Exception{}
}