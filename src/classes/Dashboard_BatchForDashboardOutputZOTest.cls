@isTest
private class Dashboard_BatchForDashboardOutputZOTest {
    
     static testmethod void testDashboardData() {
        String repDealerLabel                                   = System.Label.Replacement_Dealer;
        String OutputDashboardRecType                           = System.Label.OutputRecType;
       
        String OutputtotalDashboardRecType                      = System.Label.TotalOutputRecordType;
        Date dt = System.today();
        String month = String.valueOf(dt.month());
        String year = String.valueOf(dt.year());
        //set<id> setOFds = new set<Id>();
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
        list<Territory2Type> Terrtyp = new list<Territory2Type>();
        list<Territory2> Terr2 = new list<Territory2>();
        list<Dashboard_Score__c> dashscore                               = new list<Dashboard_Score__c>();
        list<Dashboard_Output_Score__c> dashOpscore                      = new list<Dashboard_Output_Score__c>();
        list<Dashboard_Summary_Score__c> dashSumscore                    = new list<Dashboard_Summary_Score__c>();

        Terrtyp =   [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(Terrtyp + 'Terrtyp');

        Terr2 =      [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: Terrtyp[0].id limit 100];
        system.debug(Terr2 + 'Terr2');

        id OutputRecTypeID              = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.OutputRecType Limit 1].Id;
        
        //id TotalOutputRecordTypeID      = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.TotalOutputRecordType Limit 1].Id;
        id  totalOutputId               = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName ='Total_Output_Value' Limit 1].Id;
        system.debug('-----------------------' + totalOutputId);
        DashBoardFieldValue__c dfv = new DashBoardFieldValue__c();
            dfv.Name = 'Club_Type__c';
            dfv.Field_API_Name__c = 'Club_Type__c';
            dfv.Field_Values__c = 'Galaxy';
    
        insert dfv;
        
        Dashboard_Master__c dm = new Dashboard_Master__c();
            dm.Role__c = 'ZO';
            dm.Active__c = true;
        
        insert dm;
        
        Dashboard_Master__c dm1 = new Dashboard_Master__c();
            dm1.Role__c = 'RM';
            dm1.Active__c = true;
        
        insert dm1;
        String currentMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        String currentYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
        Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
        
            dwm.RecordTypeId = OutputRecTypeID ;
            dwm.Dashboard_Master__c = dm.ID;
            dwm.Testing__c = true;
            dwm.Category__c = 'TBB';
            dwm.Category_Code__c = '2010';
            dwm.Parameters_Output__c ='High Priority Dealer';
            dwm.Role__c = 'ZO';
            dwm.Weightage__c = 10.00;
    
         insert dwm;
        
       Dashboard_Weightage_Master__c dwm1 = new Dashboard_Weightage_Master__c();
        
            dwm1.RecordTypeId = OutputRecTypeID ;
            dwm1.Dashboard_Master__c = dm1.ID;
            dwm1.Testing__c = true;
            dwm1.Category__c = 'TBB';
            dwm1.Category_Code__c = '2010';
            dwm1.Parameters_Output__c ='High Priority Dealer';
            dwm1.Role__c = 'RM';
            dwm1.Weightage__c = 10.00;
    
         insert dwm1;
          
       Dashboard_Weightage_Master__c dwmtot = new Dashboard_Weightage_Master__c();
        
            dwmtot.RecordTypeId = totalOutputId;
            dwmtot.Dashboard_Master__c = dm.ID;
            dwmtot.Testing__c = true;
            dwmtot.Role__c = 'ZO';
            dwmtot.Weightage__c = 10.00;
    
         insert dwmtot;

       Dashboard_Weightage_Master__c dwmtot1 = new Dashboard_Weightage_Master__c();
        
            dwmtot1.RecordTypeId = totalOutputId;
            dwmtot1.Dashboard_Master__c = dm1.ID;
            dwmtot1.Testing__c = true;
            dwmtot1.Role__c = 'RM';
            dwmtot1.Weightage__c = 10.00;
    
         insert dwmtot1; 
system.debug('--------------------------'+dwmtot);
        
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
            dss.Active__c= true;
            dss.Dashboard_Weightage_Master__c = dwmtot.Id;
            dss.DSS_External_ID__c = 'ZW0282015';
            dss.Input_Score__c = 0;
            dss.Month__c = currentMonth ;
            dss.Zone_Code__c = 'ZW02';
            dss.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss.Total_MTD_Actual__c = 0;
            dss.Total_MTD_Target__c = 230;
            dss.Total_Value__c = 245;
            dss.Total_Value_L3M__c = 0;
            dss.Total_Value_LYCM__c=0;
            dss.Year__c = currentYear ;
            
            insert dss;
            
        Dashboard_Summary_Score__c dss1 = new Dashboard_Summary_Score__c();
            dss1.Active__c= true;
            dss1.Dashboard_Weightage_Master__c = dwmtot1.Id;
            dss1.DSS_External_ID__c = 'AHM82015';
            dss1.Input_Score__c = 0;
            dss1.Month__c = currentMonth ;
            dss1.Region_Code__c = 'AHM';
            dss1.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss1.Total_MTD_Actual__c = 0;
            dss1.Total_MTD_Target__c = 230;
            dss1.Total_Value__c = 245;
            dss1.Total_Value_L3M__c = 0;
            dss1.Total_Value_LYCM__c=0;
            dss1.Year__c = currentYear ;
            
            insert dss1;

        Dashboard_Score__c ds1 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds1.Dashboard_Weightage_Master__c = dwm1.ID;
            ds1.Dashboard_Summary_Score__c = dss1.ID;
            ds1.MTD_Target__c = 988;
            ds1.Actual_MTD__c = 605;
            ds1.L3M__c = 1490;
            ds1.LYCM__c = 1000;
            ds1.Month__c = currentMonth ;
            ds1.Monthly_Target__c = 1098;
            ds1.Region_Code__c = 'AHM';
            ds1.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds1.No_of_Dealer_Distributor_Plan__c = 7;
            ds1.Number_of_Dealers_Distributors__c = 13;
            ds1.Category__c = '2010';
            ds1.Year__c = currentYear ;
            ds1.Parameters__c = 'High Priority Dealer';
            
        insert ds1;

        Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos.Dashboard_Weightage_Master__c = dwm1.ID;
            dos.Dashboard_Summary_Score__c = dss1.ID;
            dos.Total_MTD_Target__c = 2470;
            dos.Total_Actual_MTD__c = 1645;
            dos.Total_L3M__c = 0;
            dos.Region_Code__c = 'AHM';
            
            dos.Total_LYCM__c = 1625;
            dos.Total_Monthly_Target__c = 2743;
            dos.Total_dealers_distributors_billed_since__c = 32;
            dos.Total_No_of_dealers_distributors_planned__c = 37;
            dos.Total_No_of_Dealers_Distributor__c = 146;
            dos.Category__c = '2010';
            dos.Year__c = currentYear ; 
            dos.Month__c = currentMonth ;
         insert dos;
            
        
      
    Dashboard_BatchForDashboardOutputZO db = new Dashboard_BatchForDashboardOutputZO();
    database.executebatch(db,2000);     


        }
    /*static testmethod void testDashboardData1() {
        String repDealerLabel                                   = System.Label.Replacement_Dealer;
        String OutputDashboardRecType                           = System.Label.OutputRecType;
       
        String OutputtotalDashboardRecType                      = System.Label.TotalOutputRecordType;
        Date dt = System.today();
        String month = String.valueOf(dt.month());
        String year = String.valueOf(dt.year());
        //set<id> setOFds = new set<Id>();
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
        list<Territory2Type> Terrtyp = new list<Territory2Type>();
        list<Territory2> Terr2 = new list<Territory2>();
        list<Dashboard_Score__c> dashscore                               = new list<Dashboard_Score__c>();
        list<Dashboard_Output_Score__c> dashOpscore                      = new list<Dashboard_Output_Score__c>();
        list<Dashboard_Summary_Score__c> dashSumscore                    = new list<Dashboard_Summary_Score__c>();

        Terrtyp =   [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(Terrtyp + 'Terrtyp');

        Terr2 =      [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: Terrtyp[0].id limit 100];
        system.debug(Terr2 + 'Terr2');

        id OutputRecTypeID              = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.OutputRecType Limit 1].Id;
        
        //id TotalOutputRecordTypeID      = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.TotalOutputRecordType Limit 1].Id;
        id  totalOutputId               = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName ='Total_Output_Value' Limit 1].Id;
        system.debug('-----------------------' + totalOutputId);
        DashBoardFieldValue__c dfv = new DashBoardFieldValue__c();
            dfv.Name = 'Club_Type__c';
            dfv.Field_API_Name__c = 'Club_Type__c';
            dfv.Field_Values__c = 'Galaxy';
    
        insert dfv;
        
        Dashboard_Master__c dm = new Dashboard_Master__c();
            dm.Role__c = 'ZO';
            dm.Active__c = true;
        
        insert dm;
        
        Dashboard_Master__c dm1 = new Dashboard_Master__c();
            dm1.Role__c = 'RM';
            dm1.Active__c = true;
        
        insert dm1;
        String currentMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        String currentYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
        Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
        
            dwm.RecordTypeId = OutputRecTypeID ;
            dwm.Dashboard_Master__c = dm.ID;
            dwm.Testing__c = true;
            dwm.Category__c = 'TBB';
            dwm.Category_Code__c = '2010';
            dwm.Parameters_Output__c ='High Priority Dealer';
            dwm.Role__c = 'ZO';
            dwm.Weightage__c = 10.00;
    
         insert dwm;
        
       Dashboard_Weightage_Master__c dwm1 = new Dashboard_Weightage_Master__c();
        
            dwm1.RecordTypeId = OutputRecTypeID ;
            dwm1.Dashboard_Master__c = dm1.ID;
            dwm1.Testing__c = true;
            dwm1.Category__c = 'TBB';
            dwm1.Category_Code__c = '2010';
            dwm1.Parameters_Output__c ='High Priority Dealer';
            dwm1.Role__c = 'RM';
            dwm1.Weightage__c = 10.00;
    
         insert dwm1;
          
       Dashboard_Weightage_Master__c dwmtot = new Dashboard_Weightage_Master__c();
        
            dwmtot.RecordTypeId = totalOutputId;
            dwmtot.Dashboard_Master__c = dm.ID;
            dwmtot.Testing__c = true;
            dwmtot.Role__c = 'ZO';
            dwmtot.Weightage__c = 10.00;
    
         insert dwmtot;

       Dashboard_Weightage_Master__c dwmtot1 = new Dashboard_Weightage_Master__c();
        
            dwmtot1.RecordTypeId = totalOutputId;
            dwmtot1.Dashboard_Master__c = dm1.ID;
            dwmtot1.Testing__c = true;
            dwmtot1.Role__c = 'RM';
            dwmtot1.Weightage__c = 10.00;
    
         insert dwmtot1; 
system.debug('--------------------------'+dwmtot);
        
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
            dss.Active__c= true;
            dss.Dashboard_Weightage_Master__c = dwmtot.Id;
            dss.DSS_External_ID__c = 'ZW0282015';
            dss.Input_Score__c = 0;
            dss.Month__c = currentMonth ;
            dss.Zone_Code__c = 'ZW02';
            dss.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss.Total_MTD_Actual__c = 0;
            dss.Total_MTD_Target__c = 230;
            dss.Total_Value__c = 245;
            dss.Total_Value_L3M__c = 0;
            dss.Total_Value_LYCM__c=0;
            dss.Year__c = currentYear ;
            
            insert dss;
            
        Dashboard_Summary_Score__c dss1 = new Dashboard_Summary_Score__c();
            dss1.Active__c= true;
            dss1.Dashboard_Weightage_Master__c = dwmtot1.Id;
            dss1.DSS_External_ID__c = 'AHM82015';
            dss1.Input_Score__c = 0;
            dss1.Month__c = currentMonth ;
            dss1.Region_Code__c = 'AHM';
            dss1.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss1.Total_MTD_Actual__c = 0;
            dss1.Total_MTD_Target__c = 230;
            dss1.Total_Value__c = 245;
            dss1.Total_Value_L3M__c = 0;
            dss1.Total_Value_LYCM__c=0;
            dss1.Year__c = currentYear ;
            
            insert dss1;

        Dashboard_Score__c ds1 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds1.Dashboard_Weightage_Master__c = dwm1.ID;
            ds1.Dashboard_Summary_Score__c = dss1.ID;
            ds1.MTD_Target__c = 988;
            ds1.Actual_MTD__c = 605;
            ds1.L3M__c = 1490;
            ds1.LYCM__c = 1000;
            ds1.Month__c = currentMonth ;
            ds1.Monthly_Target__c = 1098;
            ds1.Region_Code__c = 'AHM';
            ds1.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds1.No_of_Dealer_Distributor_Plan__c = 7;
            ds1.Number_of_Dealers_Distributors__c = 13;
            ds1.Category__c = '2010';
            ds1.Year__c = currentYear ;
            ds1.Parameters__c = 'High Priority Dealer';
            
        insert ds1;

        Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos.Dashboard_Weightage_Master__c = dwm1.ID;
            dos.Dashboard_Summary_Score__c = dss1.ID;
            dos.Total_MTD_Target__c = 2470;
            dos.Total_Actual_MTD__c = 1645;
            dos.Total_L3M__c = 0;
            dos.Region_Code__c = 'AHM';
            
            dos.Total_LYCM__c = 1625;
            dos.Total_Monthly_Target__c = 2743;
            dos.Total_dealers_distributors_billed_since__c = 32;
            dos.Total_No_of_dealers_distributors_planned__c = 37;
            dos.Total_No_of_Dealers_Distributor__c = 146;
            dos.Category__c = '2010';
            dos.Year__c = currentYear ; 
            dos.Month__c = currentMonth ;
         insert dos;
            
        
     Test.startTest(); 
    Dashboard_BatchForDashboardOutputZO db = new Dashboard_BatchForDashboardOutputZO('03', '04', '2015');
    database.executebatch(db,2000);     
    Test.stopTest();

        }*/
}