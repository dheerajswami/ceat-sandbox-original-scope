@isTest
private class SP_Staging_BatchForDelete_TestClass {
    static testmethod void test() {
        // The query used by the batch job.
        
        String buType = System.Label.Actual_Replacement;
        String mnth=String.valueOf(Date.Today().Month()-1);
        
        String query='select id,Processed__c,BU__c,Month__c from Sales_Planing_Staging__c where Processed__c = true AND BU__c !=:buType  AND Month__c=: mnth ';
        
        
        // Create some test items to be deleted
        
        //   by the batch job.
        
        Sales_Planing_Staging__c[] splist = new List<Sales_Planing_Staging__c>();
         for (Integer i=0;i<10;i++) {
            
             Sales_Planing_Staging__c sp = new  Sales_Planing_Staging__c(
             Processed__c=true,
             BU__c='Replacement',
             Month__c=String.valueOf(Date.Today().Month()-1)
             );
             splist.add(sp);
         }
         insert splist;
         Test.startTest();
        
        SP_Staging_BatchForDelete b = new SP_Staging_BatchForDelete();
        
        Database.executeBatch(b);
        
        Test.stopTest();
        
        
        
        
        
    }
}