@isTest
private class ETDDetails_Test {
	
	@isTest static void test_method_one() {
		SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;

        Test.startTest();
        	ETDDetails etd = new ETDDetails();
        	//etd.ETDDetailsMapping('xer', '987678', 'det', '10023', 'date', '20', 'matDesc', 'reason');
        	
        	System.Test.setMock(WebServiceMock.class, new getETDdetails());
            ETDDetails.getAllETDDetails('987678');
        Test.stopTest();
	}

    @isTest static void test_method_two() {
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;

        Test.startTest();
            ETDDetails etd = new ETDDetails();           
            System.Test.setMock(WebServiceMock.class, new getETDdetailsWithOutDate());
            ETDDetails.getAllETDDetails('987678');
        Test.stopTest();
    }


    @isTest static void test_method_three() {
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;

        Test.startTest();
            ETDDetails etd = new ETDDetails();           
            System.Test.setMock(WebServiceMock.class, new getETDdetailsExptn());
            ETDDetails.getAllETDDetails('987678');
        Test.stopTest();
    }

	private class getETDdetails implements WebServiceMock {
		public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

			Sap_ETDWebService ws = new Sap_ETDWebService();
            //Sap_ETDWebService.ZWS_BAPI_PO_DATE    etd = new Sap_ETDWebService.ZWS_BAPI_PO_DATE();
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tom = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();
            List<Sap_ETDWebService.ZSD_PO_DATE> poList = new List<Sap_ETDWebService.ZSD_PO_DATE>();
            Sap_ETDWebService.ZSD_PO_DATE po 		= new  Sap_ETDWebService.ZSD_PO_DATE();
            
            po.VBELN 	= 'vblen';
            po.POSNR 	= '9867';
            po.BSTKD 	= 'bstdk';
            po.BSTDK_E 	= 'bstkde';
            po.MATNR 	= '001234';
            po.MAKTX 	= 'maktx';
            po.ETDAT 	= '12-12-2015';
            po.ETDNO 	= 'etdno';
            po.REASON 	= 'reason';
            po.MENGE 	= 'menge';

            poList.add(po);
            tom.item = poList;
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tob = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();  
            String piNum = '987678';

            
            Sap_ETDWebService.ZBAPI_PO_DATEResponse_element e = new Sap_ETDWebService.ZBAPI_PO_DATEResponse_element();
            e.IT_OUTPUT =  tom;
            e.RETURN_x  = 'Response';
            
            Sap_ETDWebService.ZBAPI_PO_DATE_element dateElement = new Sap_ETDWebService.ZBAPI_PO_DATE_element();
            dateElement.BSTKD = '987678';
            dateElement.IT_OUTPUT = tob;
            dateElement.VBELN = '';

            response.put('response_x', e);

            return;
		}
	}
	
    private class getETDdetailsWithOutDate implements WebServiceMock {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

            Sap_ETDWebService ws = new Sap_ETDWebService();
            //Sap_ETDWebService.ZWS_BAPI_PO_DATE    etd = new Sap_ETDWebService.ZWS_BAPI_PO_DATE();
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tom = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();
            List<Sap_ETDWebService.ZSD_PO_DATE> poList = new List<Sap_ETDWebService.ZSD_PO_DATE>();
            Sap_ETDWebService.ZSD_PO_DATE po        = new  Sap_ETDWebService.ZSD_PO_DATE();
            
            po.VBELN    = 'vblen';
            po.POSNR    = '9867';
            po.BSTKD    = 'bstdk';
            po.BSTDK_E  = 'bstkde';
            po.MATNR    = 'matnr';
            po.MAKTX    = 'maktx';
            po.ETDAT    = '';
            po.ETDNO    = 'etdno';
            po.REASON   = 'reason';
            po.MENGE    = 'menge';

            poList.add(po);
            tom.item = poList;
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tob = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();  
            String piNum = '987678';

            
            Sap_ETDWebService.ZBAPI_PO_DATEResponse_element e = new Sap_ETDWebService.ZBAPI_PO_DATEResponse_element();
            e.IT_OUTPUT =  tom;
            e.RETURN_x  = 'Response';
            
            Sap_ETDWebService.ZBAPI_PO_DATE_element dateElement = new Sap_ETDWebService.ZBAPI_PO_DATE_element();
            dateElement.BSTKD = '987678';
            dateElement.IT_OUTPUT = tob;
            dateElement.VBELN = '';

            response.put('response_x', e);

            return;
        }
    }

    private class getETDdetailsExptn implements WebServiceMock {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

            Sap_ETDWebService ws = new Sap_ETDWebService();
            //Sap_ETDWebService.ZWS_BAPI_PO_DATE    etd = new Sap_ETDWebService.ZWS_BAPI_PO_DATE();
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tom = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();
            List<Sap_ETDWebService.ZSD_PO_DATE> poList = new List<Sap_ETDWebService.ZSD_PO_DATE>();
            Sap_ETDWebService.ZSD_PO_DATE po        = new  Sap_ETDWebService.ZSD_PO_DATE();
            
            po.VBELN    = 'vblen';
            po.POSNR    = 'posnr';
            po.BSTKD    = 'bstdk';
            po.BSTDK_E  = 'bstkde';
            po.MATNR    = '001234';
            po.MAKTX    = 'maktx';
            po.ETDAT    = '12-12-2015';
            po.ETDNO    = 'etdno';
            po.REASON   = 'reason';
            po.MENGE    = 'menge';

            poList.add(po);
            tom.item = poList;
            Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE tob = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();  
            String piNum = '987678';

            
            Sap_ETDWebService.ZBAPI_PO_DATEResponse_element e = new Sap_ETDWebService.ZBAPI_PO_DATEResponse_element();
            e.IT_OUTPUT =  tom;
            e.RETURN_x  = 'Response';
            
            Sap_ETDWebService.ZBAPI_PO_DATE_element dateElement = new Sap_ETDWebService.ZBAPI_PO_DATE_element();
            dateElement.BSTKD = '987678';
            dateElement.IT_OUTPUT = tob;
            dateElement.VBELN = '';

            response.put('response_x', e);

            return;
        }
    }
}