global with sharing class ProspectController {
    id recId;
    public  Account accObj{get;set;}
    public string selectedState{get;set;}
    public List<String> statesList {get;set;}
    public String prospectId{get;set;}   
    public List<Territory2> territoryList = new list<Territory2>();
    public Set<String> territorySet {get;set;}
    public Boolean redirectPage{get;set;} 
    static Account accRec;
    set<id> terrTypeID                     = new set<Id>();
    list<Territory2Type> terriType         = new list<Territory2Type>();
    public ProspectController(ApexPages.StandardController con){
        //Quering id of Prospect record type
        recId=[select id,DeveloperName from RecordType where SobjectType='Account' AND DeveloperName='Prospect' limit 1].Id;
        accObj=new Account(RecordTypeId=recId);
        redirectPage = false;
        terriType         = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        territoryList = [Select id,ParentTerritory2Id,Territory2TypeId,name from Territory2 where Territory2TypeId IN : terrTypeID];
        territorySet = new set<String>();
        for(Territory2  tr : territoryList){
            territorySet.add(tr.name);
        }
    }
    
    public List<SelectOption> getStates(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None','--- None ---'));     
        Set<String> stSet=new Set<String>();

        for(AggregateResult masterRec: [Select State__c from State_Master__c group by State__c]){
            options.add(new SelectOption(String.valueOf(masterRec.get('State__c')),String.valueOf(masterRec.get('State__c'))));        
        }
        options.sort();
        return options;
        
    }
    //Passing state as param and getting all district under that state
    @RemoteAction
    global static List<SelectOption> DistrictsNameList(String strstate){
        
        List<SelectOption> options = new List<SelectOption>();
        Set<String> disSet=new Set<String>();
        options.add(new SelectOption('None','--- None ---')); 
        for(State_Master__c s:[Select District__c from State_Master__c where State__c=:strstate order by District__c asc]){
            if(!disSet.contains(s.District__c)){
                options.add(new SelectOption(s.District__c,s.District__c));
                disSet.add(s.District__c);
            }
        }   
        return options;
    }
    //Passing district as param and getting all towns under that district
    @RemoteAction
    global static List<SelectOption> DepTown(String strDistrict){
        // system.debug('get town method........'+strDistrict);
   
        
        List<SelectOption> options = new List<SelectOption>();
        Set<String> townSet=new Set<String>(); 
        options.add(new SelectOption('None','--- None ---')); 
        for(State_Master__c s:[Select Town__c from State_Master__c where District__c=:strDistrict order by Town__c asc]){
            //townSet.add(s.Town__c);
            options.add(new SelectOption(s.Town__c,s.Town__c));  
        }
        return options;
    }
    //Creating Prospect in account having record type prospect
    public void createProspect(){
        List<Territory2> territoryId                      = new List<Territory2>(); 
        List<Territory2> roTerritoryId                    = new List<Territory2>(); 
        List<Territory2> zoTterritoryId                   = new List<Territory2>(); 
        
        system.debug(accObj.Name+'accout');
        if(accObj.Name == ''){
            accObj.Name.addError('enter acc name');
        }
        accObj.State__c = selectedState;
        accObj.District__c=Apexpages.currentPage().getParameters().get('myDistrict');
        accObj.Town__c=Apexpages.currentPage().getParameters().get('myCity');  
        accObj.Type = 'Prospect';
        if(territorySet.contains(accObj.Sales_District_Text__c.toUpperCase())){
            redirectPage = true;
            try{
                territoryId = [Select id,ParentTerritory2Id,name from Territory2 where DeveloperName =:accObj.Sales_District_Text__c.toUpperCase()];
                roTerritoryId = [Select id,DeveloperName,ParentTerritory2Id,name from Territory2 where id =:territoryId[0].ParentTerritory2Id];
                zoTterritoryId = [Select id,DeveloperName,ParentTerritory2Id,name from Territory2 where id =:roTerritoryId[0].ParentTerritory2Id];
               
               
                accObj.Sales_Group_Text__c          = roTerritoryId[0].Name;
                accObj.Sales_Office_Text__c         = zoTterritoryId[0].Name;
                accObj.Sales_District_Text__c       = accObj.Sales_District_Text__c.toUpperCase();
                accObj.Business_Units__c = 'Replacement';
                accObj.Customer_Segment__c = 'New Dealers';
                insert accObj;
                
                //Added by PALLAVI
                List<State_Master__c> stList = new List<State_Master__c>();
                stList = [SELECT State__c,District__c,Zone__c, RO__c FROM State_Master__c WHERE State__c =:accObj.State__c AND District__c =:accObj.District__c limit 1];
                system.debug('Mastervalues'+stList);
                IF(stList.size()>0 && stList[0].State__c==accObj.State__c && stList[0].District__c==accObj.District__c){
                accObj.Sales_Group_Text__c = stList[0].RO__c;
                accObj.Sales_Office_Text__c  = stList[0].Zone__c;
                update accObj;
                system.debug('updatevalues'+accobj);
                }
            }
            catch(Exception e){}
            prospectId=accObj.Id;
            }else{
           redirectPage = false;
           //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Please enter Account name'));
           //system.debug();
        }
        
    }
    

}