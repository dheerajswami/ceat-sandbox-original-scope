global class CPORT_ClaimDateMapping {
    
    /*
* Auther  :- Swayam Arora	
* Purpose :- Fetch Claim Details from SAP 
*
*
*/
    
    //-- ATTRIBUTES
    
    public static final String CUST_ID_LENGTH 	= Label.CPORT_LengthOfCustomerId;
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static List<ClaimDataMapping> getAllClaimData(String cusNum, String fDate, String tDate,String territory){
        
        List<ClaimDataMapping> claimData = new List<ClaimDataMapping>();
        String customerId = '';
        try{
            if(cusNum!=null){
           	  customerId 								= UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
            }
            SAPLogin__c saplogin 							= SAPLogin__c.getValues('SAP Login');
                        
            String username                 				= saplogin.username__c;
            String password 								= saplogin.password__c;
            Blob headerValue 								= Blob.valueOf(username + ':' + password);
            String authorizationHeader 						= 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            SAP_Claim_Data tst 					= new SAP_Claim_Data();
            SAP_Claim_Data.ZWS_CLAIM_DATA zws 	= new SAP_Claim_Data.ZWS_CLAIM_DATA();
            
            zws.inputHttpHeaders_x 							= new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x 									= 60000;
            SAP_Claim_Data.TableOfZsd232 tom 		= new SAP_Claim_Data.TableOfZsd232();
            
            
            List<SAP_Claim_Data.Zsd232> m 			= new List<SAP_Claim_Data.Zsd232>();        
            
            tom.item = m;
            
            SAP_Claim_Data.TableOfZsd232 e = new SAP_Claim_Data.TableOfZsd232();
            
            
            e = zws.ZbapiClaimData(territory,fDate, tom, customerId, tDate);
            
            if(e.item != null){            
                for(SAP_Claim_Data.Zsd232 z : e.item) {
                	system.debug('claim--'+z);     
                    claimData.add(new ClaimDataMapping(z.Mandt, z.Exnum, z.Qmnum, z.Mawerk, z.Erdat, z.Kunum, z.Name1, z.Zname1, z.Addr, z.Matnr, z.Matkl, z.Maktx, z.Vkgrp, z.Zserialnr, z.Insdate, z.Fegrp, z.Fecod, z.Text, z.Actnsd, z.Vper, z.Kbetr, z.Netwr, z.Closs, z.Vbeln, z.Vbeln1, z.Fkdat1, z.Fkdat, z.Inspby, z.Docket, z.Telf2, z.Bzirk));
                }
            }
           
            return claimData;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
	     
    //-- WRAPPER CLASS
    
    global class ClaimDataMapping{
        public String Mandt;
        public String bill_Number; 			//Exnum
        public String claim_Invoice_Number; // Qmnum
        public String Mawerk;
        public String Erdat;
        public String Kunum;
        public String customer_Name; 		// Name1
        public String Zname1;
        public String customer_Address; 	// Addr
        public String Matnr; 				// Material Code
        public String Matkl;
        public String material_Description; // Maktx
        public String Vkgrp;
        public String claim_Number; 		//  Zserialnr
        public String Insdate;
        public String disposition; 			//  Fegrp
        public String Fecod;
        public String defect; // Text
        public String actual_nsd; // Actual NSD
        public String wear; // Vper
        public String Kbetr;
        public String Netwr;
        public String Closs;
        public String Vbeln;
        public String Vbeln1;
        public String Fkdat1;
        public String Fkdat;
        public String Inspby;
        public String Docket;
        public String Telf2;
        public String Bzirk;	 
        
        /*
Not passing all parameters because
An Apex method can have on 32 parameters
FUN FACT
*/
        public ClaimDataMapping(String Mandt, String bill_Number, String claim_Invoice_Number, String Mawerk, String Erdat, String Kunum, String customer_Name, String Zname1, String customer_Address, String Matnr, String Matkl, String material_Description, String Vkgrp, String claim_Number, String Insdate, String disposition, String Fecod, String defect, String actual_nsd, String wear, String Kbetr, String Netwr, String Closs, String Vbeln, String Vbeln1, String Fkdat1, String Fkdat, String Inspby, String Docket, String Telf2, String Bzirk){
            this.Mandt = Mandt;
            this.bill_Number = bill_Number; 			//Exnum
            this.claim_Invoice_Number = claim_Invoice_Number; // Qmnum
            this.Mawerk = Mawerk;
            this.Erdat = Erdat;
            this.Kunum = Kunum;
            this.customer_Name = customer_Name; 		// Name1
            this.Zname1 = Zname1;
            this.customer_Address = customer_Address; 	// Addr
            this.Matnr = Matnr; 				// Material Code
            this.Matkl = Matkl;
            this.material_Description = material_Description; // Maktx
            this.Vkgrp = Vkgrp;
            this.claim_Number = claim_Number; 		//  Zserialnr
            this.Insdate = Insdate;
            this.disposition = disposition; 			//  Fegrp
            this.Fecod = Fecod;
            this.defect = defect; // Text
            this.actual_nsd = actual_nsd; // Actual NSD
            this.wear = wear; // Vper
            this.Kbetr = Kbetr;
            this.Netwr = Netwr;
            this.Closs = Closs;
            this.Vbeln = Vbeln;
            this.Vbeln1 = Vbeln1;
            this.Fkdat1 = Fkdat1;
            this.Fkdat = Fkdat;
            this.Inspby = Inspby;
            this.Docket = Docket;
            this.Telf2 = Telf2;
            this.Bzirk = Bzirk;
        }
    }
    
    //-- METHODS
    
    
}