public class TH_MaterialMaster{

 //-- SINGLETON PATTERN
    private static TH_MaterialMaster instance;
    public static TH_MaterialMaster getInstance() {
        if (instance == null) instance = new TH_MaterialMaster();
        return instance;
    }
    
    private TH_MaterialMaster(){}
    
    public void onBeforeInsert(final List<Material_Master_Sap__c> newRecords){
        for(Material_Master_Sap__c rec : newRecords){
            if(rec.Name != null){
                rec.Plain_Desc__c = rec.Name.replaceAll('[^a-zA-Z0-9]','');
            }
        }
    
    }
    /*
    public void onBeforeUpdate(final List<Material_Master_Sap__c> updatedRecords){
        for(Material_Master_Sap__c rec : updatedRecords){
            rec.Plain_Desc__c = rec.Name.replaceAll('[^a-zA-Z0-9]','');
        }
    }*/

}