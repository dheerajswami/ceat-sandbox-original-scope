global class Sap_BatchForDashboardScoreCreation implements Database.Batchable<sObject>,Database.AllowsCallouts  {    
 /* ================================================
    @Name:  Sap_BatchForDashboardScoreCreation 
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor tquila
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will create dashboard score in SAP                                                                                               
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     26/08/2015      INITIAL DEVELOPMENT                                 
   
@=======================================================  */

    Date d = system.today();
    String month = String.valueOf(d.month());
    String year = String.valueOf(d.year());
    
    global Database.QueryLocator start(Database.BatchableContext BC){
         String query;
         //query                    = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE DeveloperName =  \'ASN\' AND Territory2TypeId IN: terrTypeRegID AND ParentTerritory2.ParentTerritory2.ParentTerritory2.developername =: Replacements'; 
         query                  = 'SELECT Year__c,Month__c,Region_Code__c,Territory_Code__c,Zone_Code__c,Input_Score__c,Total_Output_Score__c,Total_User_Score__c,Owner_Username__c,Owner_Role__c  from Dashboard_Summary_Score__c WHERE Year__c =\''+year+'\' AND Month__c =\''+month+'\''; 
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD res = new sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD();
        List<sapDashboardScore.ZSFDC_DASHBOARD> dashboard_items  = new List<sapDashboardScore.ZSFDC_DASHBOARD>();
        for(sObject temp : scope){
            Dashboard_Summary_Score__c dashboardScore                                =(Dashboard_Summary_Score__c)temp;
            
            sapDashboardScore.ZSFDC_DASHBOARD dashboardRecord = new sapDashboardScore.ZSFDC_DASHBOARD();
            dashboardRecord.YEAR1= dashboardScore.year__c;
            String monthwith2Char = UtilityClass.addleadingZeros(dashboardScore.Month__c,2);
            dashboardRecord.MONTH1= monthwith2Char;
           
            dashboardRecord.OWNER_USERNAME = dashboardScore.Owner_Username__c; 
            
    
            dashboardRecord.INPUT_SCORE = String.valueOF(dashboardScore.Input_Score__c);
            dashboardRecord.TOTAL_OUTPUT = String.valueOF(dashboardScore.Total_Output_Score__c);
            
            dashboardRecord.TOTAL_USER = String.valueOF(dashboardScore.Total_User_Score__c);
            dashboardRecord.OWNER_ROLE  = dashboardScore.Owner_Role__c;  
            dashboard_items.add(dashboardRecord);
            //res = SapDashboardScore_Mapping.createDashboardScore(dashboard_items);
            
        }
        system.debug(dashboard_items+'dashboard_items');
        system.debug(dashboard_items.size()+'dashboard_items');
        String msg;
        msg = SapDashboardScore_Mapping.createDashboardScore(dashboard_items);
        system.debug(msg+'msg');
    }
    
    global void finish(Database.BatchableContext BC)
    {         
     // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: Sap_BatchForDashboardScoreCreation Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "Sap_BatchForDashboardScoreCreation processed" ' + a.TotalJobItems + '/n'+' batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n' );  
             
                 if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             } 
    }

}