@istest
public class VehicleServiceDataPassingCallOut_TEST{



   testmethod static void methodone(){
   
   
   
   test.starttest();
      Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', '5000234509');
            //acc1.Fleet_Type__c = '';
            insert acc1;
            Account acc2 = CEAT_InitializeTestData.createAccount('Acc2', '5000234508');
            
            Contact con = CEAT_InitializeTestData.createContact('Con', acc1.Id);
            insert con;
            
            CS_Customer__c  csCust= CEAT_InitializeTestData.createCSCustomer('test',acc2.id,'123456789');
            insert csCust;
            
            CS_Vehicle__c cVh= CEAT_InitializeTestData.createCSVeh(csCust.id);
            insert cVh;
      
            Vehicle__c veh = CEAT_InitializeTestData.createVehicle('On Road', 'Normal Load', 'Truck', '14', acc1.Id); 
            veh.Wheel_Type__c = '4W';
            insert veh;
            
            Vehicle_Service__c  vSer = CEAT_InitializeTestData.createVehService(cVh.id);
            vSer.Balancing__c=true;
            vSer.Nitrogen_Inflation__c=true;
            vSer.Rotation__c=true;
            vSer.Alignment__c=true;
            vSer.Air_Inflation__c=true;
            vSer.Other_Services__c='Brake Oil Change';
            vSer.FL_Wear_Pattern__c='Uniform';
            vSer.RL_Wear_Pattern__c='Uniform';
            vSer.Fr_Wear_Pattern__c='Uniform';
            vSer.RR_Wear_Pattern__c='Uniform';
            vSer.Tyres_Sold__c=12;
            vSer.Tyre_Category__c='CAR RAD';
            vSer.SKU__c='145/80 R 12 MILAZE TUBELESS';
            vSer.FL_Tyre_NSD__c=2.0;
            vSer.FR_Tyre_NSD__c=2.0;
            //vSer.FL_Remaining_TWI__c=0.40;
            //vSer.FR_Remaining_TWI__c=0.40;
            vSer.RL_Tyre_NSD__c=2.0;
            vSer.RR_Tyre_NSD__c=2.0;
            //vSer.RL_Tyre_TWI__c=0.40;
            //vSer.RR_Remaining_TWI__c=0.40;
            //vSer.Spare_Tyre_NSD__c=1.0;
            //vSer.FL_Condition__c='Needs Change';
            //vSer.FR_Condition__c='Needs Change';
            //vSer.RL_Condition__c='Needs Change';
            //vSer.RR_Condition__c='Needs Change';
            //vSer.Next_Date_Balancing__c=system.today();
            //vSer.Next_Date_Alignment__c=system.today();
            //vSer.FL_Tyre_Health__c=4000.00;
            //vSer.FR_Tyre_Health__c=4000.00;
            //vSer.FL_Tyre_Change_Date__c=system.today();
            //vSer.FR_Tyre_Change_Date__c=system.today();
            //vSer.RL_Tyre_Health__c=4000.00;
            //vSer.RR_Tyre_Health__c=4000.00;
            //vSer.RL_Tyre_Change_Date__c=system.today();
           //vSer.RR_Tyre_Change_Date__c=system.today();
            
            
            insert vSer;
            
            //veh.Vehicle__c=cVh;
            
       test.stoptest();    
            
            
            
   
   
   
   
   }






}