@isTest  (seeAllData = false)
private class SAP_BatchForCustomerSDS_TestClass{  
    static Account acc;
    
    private class WebServiceMockImpl implements WebServiceMock
    {        
        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
             sapCustomer_SDS_PROD_TL zw = new sapCustomer_SDS_PROD_TL(); 
             sapCustomer_SDS_PROD_TL.ZbapiCustSds z = new sapCustomer_SDS_PROD_TL.ZbapiCustSds();
             z.Kunnr = '50003327';
             z.Sds = '23445';
             sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element res = new sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element();
             sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds vv = new sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds();
             list<sapCustomer_SDS_PROD_TL.ZbapiCustSds> item1 = new list<sapCustomer_SDS_PROD_TL.ZbapiCustSds> ();
             item1.add(z);
             
                response.put('response_x', res);
            return;
            
        }     
    }
    static testmethod void testLoadData() {
        list<Territory2Type> terriType = new list<Territory2Type>();
        list<Territory2> terrlist = new list<Territory2>();
        
        terriType                             = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(terriType+'terriType');
        terrlist = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE /*Name = 'B0001' AND */ Territory2TypeId =: terriType[0].id limit 1];
        system.debug(terrlist +'terrlist ');
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        //List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataActual');
        account ac = new account();
        ac.name ='kishlaytest';
        ac.KUNNR__c = '4343444';
        ac.Sales_District_Text__c = 'B0001';
        ac.SDS__c = 23311;
        //insert ac;
        acc = CEAT_InitializeTestData.createAccount('CustomerTest','50003327');
        //database.insert(acc);
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        Test.startTest() ;
        SAP_BatchForCustomerSDS nn = new SAP_BatchForCustomerSDS();
        //nn.query                  = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: terriType[0].id LIMIT 100 '; 
        
        ID batchprocessid = database.executebatch(nn,2000); 
        //System.abortJob(batchprocessid);
        Test.stopTest();
    }
    static testmethod void testLoadData1() {
        list<Territory2Type> terriType = new list<Territory2Type>();
        list<Territory2> terrlist = new list<Territory2>();
        
        terriType                             = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(terriType+'terriType');
        terrlist = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE /*Name = 'B0001' AND */ Territory2TypeId =: terriType[0].id limit 1];
        system.debug(terrlist +'terrlist ');
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        //List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataActual');
        account ac = new account();
        ac.name ='kishlaytest';
        ac.KUNNR__c = '4343444';
        ac.Sales_District_Text__c = 'B0001';
        ac.SDS__c = 23311;
        //insert ac;
        acc = CEAT_InitializeTestData.createAccount('CustomerTest','50003327');
        //database.insert(acc);
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        scheduledBatch_SDS temp = new scheduledBatch_SDS ();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, temp);
        
    }
}