global class scheduledBatch_RM_Output_Dashboard implements Schedulable{
   @TestVisible private integer batchsize = 1; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest()){
           batchsize = 2000;
      } 
      Dashboard_BatchForDashboardOutputRO b = new Dashboard_BatchForDashboardOutputRO();
      database.executeBatch(b,batchsize);
   }
}