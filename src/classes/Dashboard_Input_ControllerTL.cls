global class Dashboard_Input_ControllerTL { 
  
    public String loggedInUserName           {get;set;}
    public String loggedInUserTerritory      {get;set;}
    
    list<UserTerritory2Association> userTerrCode;
    list<Territory2> userTerritory;
    
    String loggedInUserId; 
    String tlRole = System.Label.TL_Role; 
   
    // CONSTRUCTOR
    global Dashboard_Input_ControllerTL (){
    
        loggedInUserId   = UserInfo.getUserId();
        user loggin      = [SELECT id,name,FirstName,UserRole.Name From User Where id =:loggedInUserId];
        loggedInUserName = loggin.FirstName;
        set<string> terrNames ;
        userTerrCode = new list<UserTerritory2Association>();
        
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId AND RoleInTerritory2=: tlRole limit 1];  
	    if(userTerrCode.size() > 0){
	             userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id];
	    		if(userTerritory.size() > 0){
	             loggedInUserTerritory=userTerritory[0].Name;
	    		}
	    }
    }
    @RemoteAction
    Public Static DashboardInputControllerHandlerTL.dashboardWrapper  getTlInputDashboardData () {
       
        String loggedInUserTerritory;
        String tlRole			 = System.Label.TL_Role; 
        String loggedInUserId    = UserInfo.getUserId();
        
        list<Territory2> userTerritory;
        list<UserTerritory2Association> userTerrCode;
        list<string> terrNames ;
        
        userTerrCode = new list<UserTerritory2Association>();
        
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:loggedInUserId AND RoleInTerritory2=: tlRole limit 1];  
	    if(userTerrCode.size() > 0){
	             userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id];
	              if(userTerritory.size() > 0){
	            	 loggedInUserTerritory=userTerritory[0].Name;
	    		  }
	    }
        DashboardInputControllerHandlerTL handler = new DashboardInputControllerHandlerTL();
        return handler.getDashboardTlRecords(loggedInUserTerritory);
    }
}