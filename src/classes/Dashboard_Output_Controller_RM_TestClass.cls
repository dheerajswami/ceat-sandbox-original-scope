@isTest 
private class Dashboard_Output_Controller_RM_TestClass{
    static list<Id> terrTypeID                            = new list<Id>();
    static list<Id> terrId                                = new list<Id>();
    static testmethod void testLoadData() {
        String repDealerLabel           = System.Label.Replacement_Dealer;
        String repROLabel                                       = System.Label.Replacement_RO;
        String repTLLabel                                       = System.Label.Replacement_TL; 
        String tlLabel = System.Label.TL_Role; 
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
        user runuser = new user();
        list<Territory2Type> terriType = new list<Territory2Type>();
        list<Territory2> roList = new list<Territory2>();
        list<Sales_Planning__c> queryAP =  new list<Sales_Planning__c>();
        
        List<UserTerritory2Association> userTerrCode = new list<UserTerritory2Association>();
        set<id> setOFSp = new set<Id>();
        Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        Id replaceROId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        Id replaceTLId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
        Id dashboardOutputId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName = 'Output' Limit 1].Id;
        Id dshOutputTotalId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName = 'Total_Output_Value' Limit 1].Id;
        // Load the test Sales planning Staging from the static resource
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'catrepDashboard1');
        //List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataR');
        
        
        terriType                             = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'RO'];
        
        roList = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE /*Name = 'D0167' AND */ Territory2TypeId =: terriType[0].id limit 1];
        system.debug(roList+'roList');
        userTerrCode = [select territory2Id,id,UserID from UserTerritory2Association where territory2Id =: roList[0].id AND RoleInTerritory2= 'RM' ]; 
        system.debug(userTerrCode+'userTerrCode');
        if(userTerrCode.size() > 0 ){
            runuser = [Select id from User Where id =: userTerrCode[0].UserID];
        }
        Dashboard_Master__c dm = new Dashboard_Master__c (Active__c  = true,Role__c = 'RM');
        insert dm;
        Dashboard_Weightage_Master__c total = new Dashboard_Weightage_Master__c();
        total.Testing__c = true;
        total.Dashboard_Master__c = dm.id;
        total.Role__c = 'RM';
        total.Weightage__c = 4;
        total.RecordTypeId  = dshOutputTotalId ;
        insert total;
        
        Dashboard_Weightage_Master__c dwmTBB = new Dashboard_Weightage_Master__c (Category__c='TBB',Parameters_Output__c='High Priority Dealer,Maintain Dealer,Institutions',Role__c='RM',Testing__c=true,Weightage__c= 10,
        RecordTypeID = dashboardOutputId,Dashboard_Master__c = dm.id   );
        insert dwmTBB;
        Dashboard_Score__c dsTBB = new Dashboard_Score__c (Actual_MTD__c = 123,Number_of_Dealers_Distributors__c = 10,Category__c = '2010',Dashboard_Weightage_Master__c = dwmTBB.id,L3M__c= 123,LYCM__c = 344,Month__c = month ,Monthly_Target__c = 345,MTD_Target__c=322,
        No_of_Dealer_Distributor_Billed_Since__c = 9,No_of_Dealer_Distributor_Plan__c = 10,Parameters__c = 'High Priority Dealer',Score__c = 5,Testing__c = true,Year__c= year,User__c = runuser.id,ownerId= runuser.id,Region_Code__c ='AGR' );
        insert dsTBB ;
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c (Total_Value__c=12312,Total_Value_L3M__c=456,Total_Value_LYCM__c=12343,Dashboard_Weightage_Master__c=total.id,DSS_External_ID__c='AGR52015',Month__c=month,Score__c=5,Region_Code__c ='AGR',Year__c = year,ownerId= runuser.id);
        insert dss;
        Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c(Total_Monthly_Target__c = 1234,Total_No_of_Dealers_Distributor__c = 10,Total_LYCM__c = 233,Total_Actual_MTD__c = 343,Total_MTD_Target__c = 465,Total_No_of_dealers_distributors_planned__c = 9,Total_L3M__c =34,Total_dealers_distributors_billed_since__c=9,
        Category__c ='2010',Dashboard_Weightage_Master__c = dwmTBB.id,Dashboard_Summary_Score__c=dss.id,Region_Code__c ='AGR',Testing__c = true,User__c = runuser.id,Month__c =month,Year__c = year);
        insert dos;
        
        //List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
        List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDashboard');
        List<sObject> listDashField = Test.loadData(DashBoardFieldValue__c.sObjectType, 'DashboardField');
        List<sObject> listDealerField = Test.loadData(Dealer_Type_Field_Matching__c.sObjectType, 'DealerField');
        list<sObject> listDashboardRMData = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'DashboardRMWeightage');
        for(sObject ds : listDashboardRMData ){
            Dashboard_Weightage_Master__c temp= (Dashboard_Weightage_Master__c)ds;
            temp.RecordTypeId = dashboardOutputId;
            temp.Dashboard_Master__c = dm.id;
        }
        update listDashboardRMData ;
       
        //***********************************************************Input Section***************************************************************************
        String rmRole = System.Label.RM_Role;
        string TLInputRecType           = system.Label.Input_RecType;
        
        map<String,Id> wightageMap = new map<String,Id>();
        
        Id TLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:rmRole Limit 1].Id;
        Id summaryInputRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:rmRole Limit 1].Id;
        
         
        integer monthNumber = date.today().month();
        String month1 = (monthNumber == 1)?'Jan':(monthNumber == 2)?'Feb':(monthNumber == 3)?'Mar':(monthNumber == 4)?'Apr':(monthNumber == 5)?'May':(monthNumber == 6)?'June':(monthNumber == 7)?'July':(monthNumber == 8)?'Aug':(monthNumber == 9)?'Sep':(monthNumber == 10)?'Oct' : (monthNumber == 11)?'Nov' : 'Dec';
       
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId AND RoleInTerritory2=:rmRole];    
        User u = [Select id, name from User where id = :userTerrAssList[0].UserId limit 1];
        
         Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();                    
         dwmTemp.Dashboard_Master__c = dm.id;
         dwmTemp.role__c = rmRole;
         dwmTemp.Category__c='PJP adherance';
         dwmTemp.Parameters_Inout__c='Number of P1 customers met';
         dwmTemp.Weightage__c = 5;
         insert dwmTemp; 
        
        Dashboard_Input_Score__c inpScore = new Dashboard_Input_Score__c();
            inpScore.RecordTypeId = TLInputRecTypeId;           
            inpScore.Month__c = month1;
            inpScore.year__c = string.valueof(date.today().year());
            inpScore.Dashboard_Weightage_Master__c = dwmTemp.id;
            inpScore.Territory_Code__c = userTerrAssList[0].Territory2.name;
            inpScore.Parameters__c='Number of P1 customers met';
            inpScore.PJP__c=true;
            inpScore.Category__c='PJP adherance';
            inpScore.Total_Monthly_Target__c=3;
            inpScore.Total_MTD_Target__c=3;
            inpScore.Total_Actual_MTD__c=3;
            inpScore.Achievement_MTD__c=3;
            inpScore.Score__c=5;
            inpScore.User__c = runuser.id;            
       
        insert inpScore;
        Dashboard_Summary_Score__c dbsum = new Dashboard_Summary_Score__c();
        dbsum.Month__c           = string.valueof(date.today().month());
        dbsum.year__c            = string.valueof(date.today().year());
        dbsum.Input_Score__c     = 5;
        dbsum.Region_Code__c     = userTerrAssList[0].Territory2.name;
        dbsum.DSS_External_ID__c = 'AGR' + month + year;
        dbsum.RecordTypeId    = summaryInputRecTypeId; 
        dbsum.ownerId    = runuser.id;
        upsert dbsum;
        
        system.runAs(runuser){
            Dashboard_Output_Handler_Controller_RM.dashboardWrapperMain res = new Dashboard_Output_Handler_Controller_RM.dashboardWrapperMain();
            
            ApexPages.currentPage().getParameters().put('Id',runuser.Id);
			ApexPages.currentPage().getParameters().put('month',string.valueof(date.today().month()));
			ApexPages.currentPage().getParameters().put('year',string.valueof(date.today().year()));
			
			
            Dashboard_Output_Controller_RM dc = new Dashboard_Output_Controller_RM();
            res = Dashboard_Output_Controller_RM.getDashboardData(runuser.id,string.valueof(date.today().month()),string.valueof(date.today().year()));
        }
    }
}