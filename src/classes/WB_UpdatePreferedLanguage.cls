/*+----------------------------------------------------------------------
||         Author:  kishlay Mathur
||
||        Purpose:To Fetch Prefered Language of customer and employee
||
||        Modefied By: Kishlay
|| 
||        Reason:  for employee as well  
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/UpdatePreferedLanguage/*')

global with sharing class WB_UpdatePreferedLanguage {
    global class customerDetails{
            public String message           {get;set;}
            public String reference_Code    {get;set;}
    }
  
    @HttpGet
    global static customerDetails getCustomerDetails(){
        RestRequest req         = RestContext.request;
        RestResponse res        = RestContext.response;
        String reference_Code   = req.params.get('reference_Code');
        String customer_lang    = req.params.get('customerlang');
        String flag             = req.params.get('flag');
        try{
           if(flag != null && flag == 'D'){
               Account account = [SELECT Id,Name,Preferred_Language__c FROM Account WHERE KUNNR__c=:reference_Code];
           
               account.Preferred_Language__c = customer_lang;
           
               update account;
               customerDetails cusdetails =  new customerDetails();
               cusdetails.reference_Code  =  reference_Code;
               cusdetails.message         =  'Y'; 
               return cusdetails; 
           }else if(flag != null && flag == 'E'){
               Employee_Master__c employee = [SELECT id,Preferred_Language__c,Name,Passcode__c,EmployeeId__c FROM Employee_Master__c WHERE EmployeeId__c =: reference_Code]; 
               employee.Preferred_Language__c = customer_lang;
           
               update employee;
               customerDetails cusdetails =  new customerDetails();
               cusdetails.reference_Code  =  reference_Code;
               cusdetails.message         =  'Y'; 
               return cusdetails; 
           }else{
               customerDetails cusdetails =  new customerDetails();
               cusdetails.reference_Code  =  reference_Code;
               cusdetails.message         =  'N'; 
               return cusdetails; 
           }
         
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            customerDetails cusdetails =  new customerDetails();
            cusdetails.reference_Code  =  reference_Code;
            cusdetails.message         =  'N'; 
            return cusdetails; 
        }
        
       
    }
}