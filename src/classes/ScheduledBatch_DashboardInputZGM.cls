global class ScheduledBatch_DashboardInputZGM implements Schedulable{
   global void execute(SchedulableContext sc) {
      DashboardInputZO_BatchClass dashboardZGMBatch = new DashboardInputZO_BatchClass(); 
      database.executebatch(dashboardZGMBatch ,1);
   }
}