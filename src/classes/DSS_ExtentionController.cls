global with sharing class DSS_ExtentionController {


	public DSS_ExtentionController(){
	
	}
	
	public DSS_ExtentionController(CPORT_PortalHomeController controller){
	
	}
	
	// GET Customer for my (User) territory 
	@RemoteAction
	global static List<Account> getAllMyCustomers(String territory){
		return [SELECT Id,Name,KUNNR__c From Account WHERE KUNNR__c != '' AND Sales_District_Text__c =: territory];
	}
    
    // GET Customer Salesplanning details  
	@RemoteAction
	global static List<Sales_Planning__c> getDealerShapshot(String customerCode,String month){
        String query = 'SELECT Id,LYCM__c,L3M__c,Total_planned__c,Actual_Quantity__c,PlannedAspValue__c,Actual_Sales_Value__c,Category__c,Category_Description__c'+
                       ' FROM Sales_Planning__c'+ 
                       ' WHERE Dealer__c=\''+customerCode+'\' AND Reporting_Month__c ='+month;
        system.debug(query);
        return Database.query(query);
	}

	// GET Customers with Circular Number
	@RemoteAction
    global static List<Customer_Discounts__c> getCustomerDiscounts(string territoy){
        return [SELECT Id,Account__c,Customer_Number__c,Account__r.Name,Circular_Number__c 
                FROM Customer_Discounts__c 
                WHERE Account__r.Sales_District_Text__c =:territoy];
    }

    @RemoteAction
    global static List<CPORT_DISC_TLD.SAP_Discount_Master> fetchDiscountMaster(){        
        return CPORT_DISC_TLD.getDiscountMaster('TEST','TEST');
    }

    // AD DSICOUNT
    @RemoteAction
    global static List<CPORT_DISC_TLD.Sap_ad_Discount> getSapAdDiscountsM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getSapAdDiscounts(circleNo,kunnag);
    }
    
    // SLAB TL
    @RemoteAction
    global static List<CPORT_DISC_TLD.Sap_TLD_Disc> getSlabTldM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getSlabTld(circleNo,kunnag);
    }

    // PPD 

    @RemoteAction
    global static CPORT_DISC_TLD.SAP_PPD_Discount getPPDDiscountMasterM(String circleNo,String kunnag){
       return CPORT_DISC_TLD.getPPDDiscountMaster(circleNo,kunnag);
    }

    @RemoteAction
    global static CPORT_DISC_PLD.PLD_NEW_CLASS getAllDiscountsPLDM(String circleNo,String kunnag){
       return CPORT_DISC_PLD.getAllDiscountsPLD(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CPORT_DISC_PLD.Debit_Note_slab> getDebitNoteSlabM(String circleNo,String kunnag){
       return CPORT_DISC_PLD.getDebitNoteSlab(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CPORT_DISC_PLD.Discounts_slab> getDiscountSlabM(String circleNo,String kunnag){
       return CPORT_DISC_PLD.getDiscountSlab(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CPORT_DISC_PLD.Collection_sales> getCollectoionSalesM(String circleNo,String kunnag){
       return CPORT_DISC_PLD.getCollectoionSales(circleNo,kunnag);
    }

    @RemoteAction
    global static List<CPORT_DISC_PLD.CS_Sales_Discounts> getCSSalesDiscountsM(String circleNo,String kunnag){
       return CPORT_DISC_PLD.getCSSalesDiscounts(circleNo,kunnag);
    }
}