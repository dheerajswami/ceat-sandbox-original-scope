public class PJPPlanningController{

    public static PageReference page {get;set;}
    
    public PJPPlanningController(){ }
    @RemoteAction
    Public Static PageReference getpage() { 
         
         //--------------- Profiles ------------     
          String replacmentProfile      = system.label.Replacement_Profile;
          String specialtyProfile       = system.label.Specialty;
          String exportProfile          = system.label.Export_Profile;
          String specialtyOeProfile     = system.label.Specialty_OE; 
          
           //--------------- Roles ------------  
          String tlRole                 = system.label.TL_Role;
          String rmRole                 = system.label.RM_Role;
          String tldRole                = system.label.TLD_Role;
          String cstldRole              = system.label.CSTL_Role;
           
          String loggedInUserId ;          
          
         //Getting logged in user Id
         loggedInUserId=UserInfo.getUserId();
         
         List<UserRole> userTLRol  = [SELECT DeveloperName,Id FROM UserRole WHERE (DeveloperName  like 'TL%') AND (NOT(DeveloperName  like  '%Specialty')) AND (NOT(DeveloperName  like  '%OE')) AND (NOT(DeveloperName  like  'TLD%'))];//DeveloperName =: tlLabel];
         List<UserRole> userCSTLRol  = [SELECT DeveloperName,Id FROM UserRole WHERE (DeveloperName  like 'CSTL%') AND (NOT(DeveloperName  like  '%Specialty')) AND (NOT(DeveloperName  like  '%OE')) AND (NOT(DeveloperName  like  'TLD%'))];//DeveloperName =: tlLabel];
         List<UserRole> userTLDRol  = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName  like  'TLD%'];
        
         String roleTL='';
         for(UserRole rl1 :userTLRol){
            if(UserInfo.getUserRoleId() == rl1.id)
            roleTL = rl1.id;
         }
         
         String roleCSTL='';
         for(UserRole rl1 :userCSTLRol){
            if(UserInfo.getUserRoleId() == rl1.id)
            roleCSTL = rl1.id;
         }
        
         String roleTLD='';
         for(UserRole tld :userTLDRol){
            if(UserInfo.getUserRoleId() == tld.id)
            roleTLD = tld.id;
         }
        
         //Querying LoggedIn user Role
         User userRole=  [select ProfileId,name,id,email,UserRole.DeveloperName from User where id=:loggedInUserId];
         
         //Getting profile for loggedIn User
         String profileName=[Select Id,Name from Profile where Id=:userRole.profileId].Name;
        
         //*************************Replacement***************************************
         if(profileName==replacmentProfile){
           
             //If loggedin user is TL/CSTL, it should redirect to Sales planning TL/CSTL page
            
             if(((roleTL!=null && roleTL!='') || (roleCSTL!=null && roleCSTL!='')) && userRole.UserRole.DeveloperName != tldRole){
                page = new PageReference('/apex/'+Constants.pjpReplacementPage);
                page.setRedirect(true);             
             }
             //If loggedin user is TLD, it should redirect to Sales planning TLD page
             
             else if(roleTLD!=null && roleTLD!=''){
                page = new PageReference('/apex/'+Constants.pjpReplacementTLDPage);
                page.setRedirect(true);             
                    
             }
             //else It should redirect to home page
             else{
                page = new PageReference('/home/home.jsp');
                page.setRedirect(true);  
             }
         }
         //*************************Specialty****************************************
         
         else if(profileName==specialtyProfile){
             
                page = new PageReference('/apex/'+Constants.pjpSpecialtyPage);
                page.setRedirect(true);
         }
         //*************************Exports****************************************
         else if(profileName==exportProfile){           
      
                page = new PageReference('/apex/'+Constants.pjpExportsPage);
                page.setRedirect(true); 
         } 
         //*************************OE****************************************
         
         else if(profileName==Constants.OEProfile){             
      
                page = new PageReference('/apex/'+Constants.pjpOEPage);
                page.setRedirect(true);  
         }          
         //************************Admin**************************************
         else{
            page = new PageReference('/home/home.jsp');
            page.setRedirect(true); 
         }
         return page;
    }
}