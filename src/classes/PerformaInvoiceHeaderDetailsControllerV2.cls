global with sharing class PerformaInvoiceHeaderDetailsControllerV2 {
    public List<SelectOption> packingListOption {get; set;}
    public List<SelectOption> documentCurrencyOption {get; set;}
    //public List<SelectOption> termsOfPaymentOption {get; set;}
    public List<SelectOption> incoTermsOption {get; set;}
    public List<SelectOption> vehicleTypeOption {get; set;}
    public List<SelectOption> countryOption {get; set;}
    public List<SelectOption> materialGroupOption {get; set;}
    public String defaultValue {get;set;}
    public Id invoiceId {get;set;}
    public String customerName {get; set;}
    public Proforma_Invoice__c prf{get;set;}
    public Id customerId {get; set;}
    //public String vehicletype {get; set;}
    //public Integer piLitemIndexToDelete {get;set;}
    public String dateForPricingExchangeRate {get; set;}
    public String quotationDate {get; set;}
    //public Integer quantity {get; set;}
    public String inquiryvlidityDate {get; set;}
    public String requestedDelvDate {get; set;}
    public String selectedPort {get; set;}
    public String selectedPortCode {get; set;}
    public String selectedShipToParty {get; set;}

    public PerformaInvoiceHeaderDetailsControllerV2(ApexPages.StandardController con) {
        
        if(con.getRecord().id!=null){
            this.invoiceId = con.getRecord().id;
            system.debug(invoiceId +'invoiceId ');
            prf = new Proforma_Invoice__c();
            prf=[Select Advance_Payment__c,Adanced_payment_details__c ,Mixing_of_Pro_forma__c ,Nomination_of_shipping_line__c,Shipping_Line_Name__c,Clubbing_of_containers__c,Inspection_Required__c ,Inspection_Agency_Name__c ,Customized_Container__c ,Clubbing_details__c,LC_Required__c,Import_License_Requirement__c ,Incoterms_part_1__c,Incoterms__r.Name,Vehicle_Type_Code__c,Vehicle_type__r.Name,SD_Document_Currency_Code__c,Packing_List_Requirement__r.Name,
                Port_of_Destination__r.Name,Customer__r.Name,Ship_to_party__c,Port_of_Discharge_Name__c,
                Sales_Document_Type__c,SD_Document_Currency__r.Name,System_Ship_to_Country__c,Distribution_Channel_PickList__c,DivisionPickList__c,Sales_Document_Type_PickList__c,Packing_List_Requirement__c,Quotation_Inquiry_is_valid_from__c,Date_for_pricing_and_exchange_rate__c,
                Date_until_which_bid_quotation_binding__c,Packing_List_Code__c,Requested_delivery_date__c,Terms_of_Payment__r.Name,Terms_of_Payment_Key__c 
                FROM Proforma_Invoice__c WHERE Id =: invoiceId  LIMIT 1];
            customerId = prf.Customer__c;
            defaultValue = prf.Sales_Document_Type__c ;
            inquiryvlidityDate = prf.Quotation_Inquiry_is_valid_from__c.format();
            requestedDelvDate = prf.Requested_delivery_date__c.format();
            selectedPort = prf.Port_of_Destination__r.Name;
            selectedPortCode = prf.Port_of_Discharge_Name__c;
            selectedShipToParty = prf.Ship_to_party__c;
        }
        else{
            prf = new Proforma_Invoice__c();
            prf=null;
            defaultValue = '--None--';
            Id logdInUserId = UserInfo.getUserId();
            System.debug('==#1 '+logdInUserId);
            User tempUser;
            tempUser = [SELECT AccountId,Address,ContactId,Username FROM User WHERE Id =: logdInUserId];
            System.debug('==#00 '+ tempUser.AccountId);
            if(tempUser.AccountId != null) {
                customerName = getAccountName(tempUser.AccountId);
                customerId = tempUser.AccountId;
            }
        }
        System.debug('==#2 '+customerName);
        //dateForPricingExchangeRate = String.valueOf(System.today());
        dateForPricingExchangeRate = (System.today()).format();
        //quotationDate = String.valueOf(System.today() + 60);
        quotationDate = (System.today() + 60).format();
        populatePackingList();
        populateDocumentCurrency();
        //populateTermsOfPayment();
        populateIncoTerms();
        populateVehicleType();
        populateCountry();
        populateMaterialGroup();
    }

    public void populatePackingList() {
        Map<Id, Packing_List__c> idToPackingListMap = new Map<Id, Packing_List__c>([SELECT Code__c,Id,Name FROM Packing_List__c]);
        packingListOption = new List<SelectOption>();
        if(prf == null) {
            packingListOption.add(new SelectOption('','--None--'));
        }
        else {
            packingListOption.add(new SelectOption(prf.Packing_List_Code__c,prf.Packing_List_Requirement__r.Name));
        }
        for(Id id : idToPackingListMap.keySet()) {
            packingListOption.add(new SelectOption(idToPackingListMap.get(id).Code__c, idToPackingListMap.get(id).Name));
            
        }
    }

    public void populateDocumentCurrency() {
        Map<Id, Document_Currency__c> idToDocumentCurrencyMap = new Map<Id, Document_Currency__c>([SELECT Code__c,Name FROM Document_Currency__c]);
        documentCurrencyOption = new List<SelectOption>();
        if(prf ==null){
            documentCurrencyOption.add(new SelectOption('USD','United States Dollar'));
        
        }
        else{
            documentCurrencyOption.add(new SelectOption(prf.SD_Document_Currency_Code__c,prf.SD_Document_Currency__r.name));
        }
        for(Id id : idToDocumentCurrencyMap.keySet()) {
            documentCurrencyOption.add(new SelectOption(idToDocumentCurrencyMap.get(id).Code__c, idToDocumentCurrencyMap.get(id).Name));
            //System.debug('==== '+documentCurrencyOption);
        }
    }

    /*public void populateTermsOfPayment() {
        Map<Id, Terms_of_Payment__c> idToTermsOfPaymentMap = new Map<Id, Terms_of_Payment__c>([SELECT Code__c,Name FROM Terms_of_Payment__c]);
        termsOfPaymentOption = new List<SelectOption>();

            if((prf != null && prf.Terms_of_Payment__c == null) || prf == null){
                termsOfPaymentOption.add(new SelectOption('','--None--'));
                
            }else if((prf != null && prf.Terms_of_Payment__c != null)) {
                termsOfPaymentOption.add(new SelectOption(prf.Terms_of_Payment_Key__c,prf.Terms_of_Payment__r.name));
            }
            for(Id id : idToTermsOfPaymentMap.keySet()) {
                termsOfPaymentOption.add(new SelectOption(idToTermsOfPaymentMap.get(id).Code__c, idToTermsOfPaymentMap.get(id).Name));
            }
    }*/

    public void populateIncoTerms() {
        Map<Id, Inco_Terms__c> idToIncoTermsMap = new Map<Id, Inco_Terms__c>([SELECT Code__c,Name FROM Inco_Terms__c]);
        incoTermsOption = new List<SelectOption>();
        if(prf == null) {
            incoTermsOption.add(new SelectOption('','--None--'));
        }
        else {
           incoTermsOption.add(new SelectOption(prf.Incoterms_part_1__c,prf.Incoterms__r.Name)); 
        }
        for(Id id : idToIncoTermsMap.keySet()) {
            incoTermsOption.add(new SelectOption(idToIncoTermsMap.get(id).Code__c, idToIncoTermsMap.get(id).Name + '('+idToIncoTermsMap.get(id).Code__c+')'));
        }
    }

    public void populateVehicleType() {
        Map<Id, Vehicle_Type__c> idToVehicleTypeMap = new Map<Id, Vehicle_Type__c>([SELECT Code__c,Name FROM Vehicle_Type__c]);
        vehicleTypeOption = new List<SelectOption>();
        if(prf == null) {
           vehicleTypeOption.add(new SelectOption('','--None--')); 
        }
        else {
            vehicleTypeOption.add(new SelectOption(prf.Vehicle_Type_Code__c,prf.Vehicle_type__r.Name));
        }
        for(Id id : idToVehicleTypeMap.keySet()) {
            vehicleTypeOption.add(new SelectOption(idToVehicleTypeMap.get(id).Code__c, idToVehicleTypeMap.get(id).Name));
        }
    }

    public void populateCountry() {
        Map<Id, Port__c> idToPortMap = new Map<Id, Port__c>([SELECT Country_Code__c,Country_Name__c,Flag__c,Locode__c,Name,Port_Code__c FROM Port__c]);
        Map<String, String> codeToCountryMap = new Map<String, String>();
        countryOption = new List<SelectOption>();
        
        for(Id id : idToPortMap.keySet()) {
            if(!codeToCountryMap.containsKey(idToPortMap.get(id).Country_Code__c)) {
                codeToCountryMap.put(idToPortMap.get(id).Country_Code__c, idToPortMap.get(id).Country_Name__c);
            }
        }
        if(prf == null) {
           countryOption.add(new SelectOption('','--None--')); 
           }else {
            System.debug('==#test1 '+codeToCountryMap);
              countryOption.add(new SelectOption(prf.System_Ship_to_Country__c, codeToCountryMap.get(prf.System_Ship_to_Country__c)));  
           }
        
        for(String  str : codeToCountryMap.keySet()) {
            countryOption.add(new SelectOption(str, codeToCountryMap.get(str)));

        }
        System.debug('==#1 '+codeToCountryMap);
        System.debug('==#108 '+userInfo.getUserType());
        
        
    }

    public void populateMaterialGroup() {
        List<Brand__c> brandList = Brand__c.getall().values();
        List<String> brandNameList = new List<String>();
        for(Brand__c tmpBrand : brandList) {
            brandNameList.add(tmpBrand.Name);
        }
        System.debug('==# Custom Setting value '+ brandList);
        Map<String, String> matGroupToNameMap = new Map<String, String>();
        materialGroupOption = new List<SelectOption>();
        materialGroupOption.add(new SelectOption('','--None--'));
        for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Mat_Grp_Desc__c,Mat_Grp1_Desc__c FROM Material_Master_Sap__c WHERE Mat_Grp1_Desc__c IN : brandNameList]) {
            if(!matGroupToNameMap.containsKey(tempMmS.Material_Group_1__c) && tempMmS.Material_Group_1__c != null) {
               // matGroupToNameMap.put(tempMmS.Material_Group_1__c, tempMmS.Mat_Grp1_Desc__c);
               matGroupToNameMap.put(tempMmS.Mat_Grp1_Desc__c, tempMmS.Mat_Grp1_Desc__c);
            }
        }
        System.debug('==#2 '+matGroupToNameMap);
        for(String str : matGroupToNameMap.keySet()) {
            materialGroupOption.add(new SelectOption(str, matGroupToNameMap.get(str)));
            
        }
        System.debug('==#3 '+materialGroupOption);
    }


    /**** Search for Accounts ****/
    @RemoteAction
    public static List<Account> searchAccounts(String searchTerm) {
        System.debug('==#1 '+searchTerm );
        Id accRecordTypeId = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE Name =: 'Exports' AND SobjectType =: 'Account'].Id;
        System.debug('==#== '+accRecordTypeId);
        List<Account> accounts  = Database.query('Select Id, Name, UniqueIdentifier__c from Account where ParentId = null and Ship_to_Country__c != null and RecordTypeId =\''+accRecordTypeId +'\' and Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ORDER BY UniqueIdentifier__c LIMIT 10');
        System.debug('==#2 '+accounts);
        return accounts;
    }


    /**** Fetch Port Names related to a Country ****/
    @RemoteAction
    global static Map<String, String> portOption(String countryCode){
        Map<String, String> portCodeToNameMap = new Map<String, String>();
        System.debug('==#1 Inside Remote Method '+countryCode);
        //portCodeToNameMap.put('', '--None--');
        Set<String> portCodeSet = new Set<String>();
        for(Port__c p : [SELECT Country_Code__c,Country_Name__c,Flag__c,Locode__c,Name,Port_Code__c FROM Port__c WHERE Country_Code__c =: countryCode ORDER BY Name asc]) {
            if(!portCodeSet.contains(p.Locode__c)) {
                portCodeToNameMap.put(p.Locode__c, p.Name);
                portCodeSet.add(p.Locode__c);
            }
        }
        return portCodeToNameMap;
    }

    /**** Fetch Account Name and Account Number realted to Parent Account ****/
    @RemoteAction
    global static Map<String, String> shipToPartyOption(String accountId) {
        System.debug('==#002 ' +accountId);
        Map<String, String> accountNumberToNameMap = new Map<String, String>();
        //if(accountId != '' || accountId != null) {
            try{
               for(Account tmpAccount : [SELECT AccountNumber,Active__c,Name, KUNNR__c,ParentId,UniqueIdentifier__c FROM Account WHERE Active__c = true AND ParentId =: accountId]) {
                    if(!accountNumberToNameMap.containsKey(tmpAccount.UniqueIdentifier__c)) {
                        accountNumberToNameMap.put(tmpAccount.UniqueIdentifier__c, tmpAccount.Name+' ('+tmpAccount.UniqueIdentifier__c+')');
                    }
                } 
            //}
            
                if(accountNumberToNameMap.isEmpty()) {
                    System.debug('==#003 '+accountNumberToNameMap);
                    Account tmpAccount = [SELECT AccountNumber,Active__c,Name, KUNNR__c,ParentId,UniqueIdentifier__c FROM Account WHERE Active__c = true and Id =: accountId];
                    accountNumberToNameMap.put(tmpAccount.UniqueIdentifier__c, tmpAccount.Name+' ('+tmpAccount.UniqueIdentifier__c+')');
                }
                System.debug('==#003 '+accountNumberToNameMap);
                return accountNumberToNameMap;

            }catch(Exception exptn) {
                return null;
            }
    }


    /**** Fetch Material Group Description related to a particular Brand ****/
    @RemoteAction
    global static Map<String, String> fetchMaterialGroupDesc(String brand, String setsOrPieces) {
        System.debug('==#2 Inside Remote Method '+brand);
        Map<String, String> brandToMaterialGrpDescMap = new Map<String, String>();
        Set<String> matGrpDescSet = new Set<String>();
        for(Material_Master_Sap__c tempMm : [SELECT Material_Group_1__c,Mat_Grp_Desc__c,Name, Mat_Grp1_Desc__c, Mtart__c FROM Material_Master_Sap__c WHERE Mat_Grp1_Desc__c =: brand AND Mtart__c =: setsOrPieces ORDER BY Mat_Grp_Desc__c]) {
            if(!matGrpDescSet.contains(tempMm.Mat_Grp_Desc__c)) {
                brandToMaterialGrpDescMap.put(tempMm.Mat_Grp_Desc__c, tempMm.Mat_Grp_Desc__c);
                matGrpDescSet.add(tempMm.Mat_Grp_Desc__c);
            }
        }

        return brandToMaterialGrpDescMap;
    }


    /**** Fetch SKU related to a particular Material Group Description ****/
    @RemoteAction
    global static Map<String, String> fetchSKUs(String matGrpDesc, String selectedBrand, String setsOrPieces) {
        System.debug(selectedBrand+'==#4 '+matGrpDesc);
        Map<String, String> idToMatMasterNameMap = new Map<String, String>();
        Set<String> matMasterIdSet = new Set<String>();
        if(setsOrPieces == 'DIEN') {
           for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name,Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: matGrpDesc AND 
                                                        Mat_Grp1_Desc__c =: selectedBrand AND 
                                                        Mtart__c =: 'DIEN' ORDER BY Name]) {
                if(!matMasterIdSet.contains(tempMmS.Name)) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                    matMasterIdSet.add(tempMmS.Name);
                }
            } 
        }else if(setsOrPieces == 'ZFGS') {
            for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name, Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: matGrpDesc AND 
                                                        Mat_Grp1_Desc__c =: selectedBrand AND 
                                                        Mtart__c =: 'ZFGS' ORDER BY Name]) {
                if(!matMasterIdSet.contains(tempMmS.Name)) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                    matMasterIdSet.add(tempMmS.Name);
                }
            }
        }
        
        return idToMatMasterNameMap;
    }


    @RemoteAction
    global static Double calculateLoadibility(String sku, Decimal quantity) {
        Map<String, string> materialNoToCapacityMap = new  Map<String, string>();
        Map<String, String> materialNameToMaterialNoMap = new Map<String, String>();
        Double capacity;
        Decimal noOfContainers;

        for(Material_Master_Sap__c tmpMaster : [SELECT Id, Material_Number__c, Name FROM Material_Master_Sap__c]) {
            if(!materialNameToMaterialNoMap.containsKey(tmpMaster.Name)) {
                materialNameToMaterialNoMap.put(tmpMaster.Name, tmpMaster.Material_Number__c);
            }
        }

        for(Loadability__c tmpLoad : [SELECT Capacity__c,Category__c,Group__c,Material_Number__c,Name FROM Loadability__c]) {
            if(!materialNoToCapacityMap.containsKey(tmpLoad.Material_Number__c)) {
                materialNoToCapacityMap.put(tmpLoad.Material_Number__c, tmpLoad.Capacity__c);
                //materialNoToCapacityMap.put(UtilityClass.addleadingZeros(tmpLoad.Material_Number__c, 18), tmpLoad.Capacity__c);
            }
        }
        try {
            System.debug('==#200 '+ sku);
            System.debug('==#204 '+materialNoToCapacityMap);
            System.debug('==#201 '+materialNameToMaterialNoMap.get(sku));
            System.debug('==#202 '+materialNoToCapacityMap.get(materialNameToMaterialNoMap.get(sku)));
            capacity = Double.valueOf(materialNoToCapacityMap.get(materialNameToMaterialNoMap.get(sku)));
            System.debug('==#203 '+ capacity);
            noOfContainers = quantity / capacity;

            //return Math.ceil(noOfContainers);
            System.debug('==# SetScaled value '+ noOfContainers.setscale(2));
            return (noOfContainers.setscale(2));
        }catch(Exception e) {
            return null;
        }
    }

    /**** Save Proforma Invoice and related Proforma Invoice Line Items ****/
    @RemoteAction
    global static PageReference saveInvoice(String inquiryDate, String requestedDelvDate, Proforma_Invoice__c proformaInvoice, List<PI_Line_Item__c> lineItemList) {
        try {
            String invoiceCurrency = proformaInvoice.SD_Document_Currency__c;
            System.debug('==#101 '+requestedDelvDate);
            String customerId = [SELECT AccountId,Address,ContactId,Username FROM User WHERE Id =:UserInfo.getUserId()].AccountId;
            System.debug('==#1' + proformaInvoice);
            System.debug('==#100' + customerId);
            CEAT_ExceptionHandler exptnHandler = new CEAT_ExceptionHandler();
            exptnHandler.validateLineItemList(lineItemList);
            exptnHandler.ValidateMandatoryFields(requestedDelvDate, customerId, proformaInvoice.Advance_Payment__c, proformaInvoice.Adanced_payment_details__c, 
                                                 proformaInvoice.Nomination_of_shipping_line__c, proformaInvoice.Shipping_Line_Name__c, 
                                                 proformaInvoice.Clubbing_of_containers__c, proformaInvoice.Clubbing_details__c, 
                                                 proformaInvoice.Inspection_Required__c, proformaInvoice.Inspection_Agency_Name__c,
                                                proformaInvoice.DivisionPickList__c,proformaInvoice.Distribution_Channel_PickList__c,
                                                proformaInvoice.Sales_Document_Type_PickList__c,proformaInvoice.SD_Document_Currency__c,
                                                proformaInvoice.Incoterms__c,
                                                proformaInvoice.Mixing_of_Pro_forma__c,proformaInvoice.System_Ship_to_Country__c , 
                                                proformaInvoice.Port_of_Destination__c,proformaInvoice.Import_License_Requirement__c,
                                                proformaInvoice.Packing_List_Requirement__c ,proformaInvoice.Vehicle_type__c ,
                                                proformaInvoice.Customized_Container__c ,proformaInvoice.LC_Required__c);

            Proforma_Invoice__c tmpPinvoice = new Proforma_Invoice__c();
            String accid = '';
            tmpPinvoice = proformaInvoice;
            System.debug('==#12 '+proformaInvoice.Terms_of_Payment__c);
            tmpPinvoice.Date_for_pricing_and_exchange_rate__c =  System.today();
            //tmpPinvoice.Date_until_which_bid_quotation_binding__c = (System.today() + 60);
            if(!Test.isRunningTest()) {
                tmpPinvoice.Requested_delivery_date__c             = Date.parse(requestedDelvDate);
            
                tmpPinvoice.Quotation_Inquiry_is_valid_from__c     = (inquiryDate == '' ? System.today() : Date.parse(inquiryDate));                       
                tmpPinvoice.SD_Document_Currency__c                = getCurrencyId(proformaInvoice.SD_Document_Currency__c);
                //tmpPinvoice.Terms_of_Payment__c                    = getTermsOfPaymentId(proformaInvoice.Terms_of_Payment__c);
                tmpPinvoice.Incoterms__c                           = getincoTermId(proformaInvoice.Incoterms__c);
                tmpPinvoice.Port_of_Destination__c                 = getportId(proformaInvoice.Port_of_Destination__c);
                tmpPinvoice.Packing_List_Requirement__c            = getPackingListId(proformaInvoice.Packing_List_Requirement__c);
                tmpPinvoice.Vehicle_type__c                        = getVehicleId(proformaInvoice.Vehicle_type__c);
                System.debug('==#14 Executed till here');
                System.debug('==#15 '+tmpPinvoice.Customer__c);
                 
                System.debug('===================');
            }
                
                
                /*if(customerId == null) {
                    exptnHandler.ValidateCustomerFields(tmpPinvoice.Customer__c, tmpPinvoice.Ship_to_party__c);
             
                }*/
                accid = (String.valueOf(proformaInvoice.Customer__c) != '' ? proformaInvoice.Customer__c : customerId);
                    //System.debug('===#16 Inside If ');
                    tmpPinvoice.Customer__c = accid;


                 
                //exptnHandler.ValidateCustomerFields(tmpPinvoice.Customer__c, tmpPinvoice.Ship_to_party__c);                                        
            insert tmpPinvoice;

            List<PI_Line_Item__c> lineItemToInsertList = new List<PI_Line_Item__c>();
            Map<String, Material_Master_Sap__c> matNameToMatMasterMap =new Map<String, Material_Master_Sap__c>();
            String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
            for(Material_Master_Sap__c tmpMaster : [SELECT Id, Material_Number__c, Name FROM Material_Master_Sap__c WHERE Name != '.'] ) {
                if(!matNameToMatMasterMap.containsKey(tmpMaster.Name)) {
                    matNameToMatMasterMap.put(tmpMaster.Name, tmpMaster);
                }
                //System.debug('==#5 '+matNameToMatMasterMap);
            }
            

            for(PI_Line_Item__c tmLineItem : lineItemList) {
                tmLineItem.Proforma_Invoice__c                 = tmpPinvoice.Id;
                System.debug('==#106 '+ tmLineItem.Material_Master__c);
                if(!Test.isRunningTest()) {
                    Id matMasterId                                 = matNameToMatMasterMap.get(tmLineItem.Material_Master__c).Id;
                    String matNum                                  = matNameToMatMasterMap.get(tmLineItem.Material_Master__c).Material_Number__c;   
                    //String  matNum                                 = tmLineItem.Material_Number__c;
                    tmLineItem.Material_Number__c                  = matNum;
                    tmLineItem.Material_Master__c                  = matMasterId;
                    //tmLineItem.Price__c                            = invoiceCurrency + ' ' + String.valueOf(tmLineItem.Price__c);
                    tmLineItem.Unit_Price__c                       = tmLineItem.Unit_Price__c;
                    tmLineItem.System_Currency_Code__c             = invoiceCurrency;
                    //System.debug('==#107 '+tmLineItem.Unit_Price__c);
                }
                lineItemToInsertList.add(tmLineItem);

            }
            System.debug('==#107 '+lineItemToInsertList);
            insert lineItemToInsertList;

            if(userInfo.getUserType() == 'CspLitePortal') {
                PageReference pf = new PageReference(baseURL+'/customers/apex/ProformaInvoiceDetailView_V2?id='+tmpPinvoice.Id);
                pf.setRedirect(true);
                return pf;
            }
            else {
                PageReference pf = new PageReference(baseURL+'/'+tmpPinvoice.Id);
                pf.setRedirect(true);
                return pf; 
            }

        }
        catch(Exception exptn) {
            //System.debug(exptn.get());
            if(exptn.getTypeName() == 'CEAT_ExceptionHandler.customException') {
                throw new customException(exptn.getMessage());
            }else {
               throw new customException('Something went wrong. Please fill all the mandatory fields or contact your Administrator.'); 
            }
            //throw new customException(exptn.getTypeName());   
        }
        
    }



    @RemoteAction
    global static PageReference updateInvoice(String pricingDate, String inquiryDate, String requestedDelvDate, Proforma_Invoice__c proformaInvoice) {
        try {
            String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
            String invoiceCurrency = proformaInvoice.SD_Document_Currency__c;
            System.debug('==#101 '+requestedDelvDate);
            String customerId = [SELECT AccountId,Address,ContactId,Username FROM User WHERE Id =:UserInfo.getUserId()].AccountId;
            System.debug('==#1' + proformaInvoice);
            System.debug('==#100' + customerId);
            CEAT_ExceptionHandler exptnHandler = new CEAT_ExceptionHandler();
            System.debug('====## '+proformaInvoice.Advance_Payment__c);
            exptnHandler.ValidateMandatoryFields(requestedDelvDate, customerId, proformaInvoice.Advance_Payment__c, proformaInvoice.Adanced_payment_details__c, 
                                                 proformaInvoice.Nomination_of_shipping_line__c, proformaInvoice.Shipping_Line_Name__c, 
                                                 proformaInvoice.Clubbing_of_containers__c, proformaInvoice.Clubbing_details__c, 
                                                 proformaInvoice.Inspection_Required__c, proformaInvoice.Inspection_Agency_Name__c,
                                                proformaInvoice.DivisionPickList__c,proformaInvoice.Distribution_Channel_PickList__c,
                                                proformaInvoice.Sales_Document_Type_PickList__c,proformaInvoice.SD_Document_Currency__c,
                                                proformaInvoice.Incoterms__c,
                                                proformaInvoice.Mixing_of_Pro_forma__c,proformaInvoice.System_Ship_to_Country__c , 
                                                proformaInvoice.Port_of_Destination__c,proformaInvoice.Import_License_Requirement__c,
                                                proformaInvoice.Packing_List_Requirement__c ,proformaInvoice.Vehicle_type__c ,
                                                proformaInvoice.Customized_Container__c ,proformaInvoice.LC_Required__c);

            
            Proforma_Invoice__c tmpPinvoice = new Proforma_Invoice__c();
            String accid = '';
            tmpPinvoice = proformaInvoice;
            System.debug('==#12 '+proformaInvoice.Terms_of_Payment__c);
            if(!Test.isRunningTest()) {
                tmpPinvoice.Requested_delivery_date__c                  = Date.parse(requestedDelvDate);
                tmpPinvoice.Date_for_pricing_and_exchange_rate__c       = Date.parse(pricingDate);
                tmpPinvoice.Quotation_Inquiry_is_valid_from__c          = (inquiryDate == '' ? System.today() : Date.parse(inquiryDate));                       
                tmpPinvoice.SD_Document_Currency__c                     = getCurrencyId(proformaInvoice.SD_Document_Currency__c);
                //tmpPinvoice.Terms_of_Payment__c                         = getTermsOfPaymentId(proformaInvoice.Terms_of_Payment__c);
                tmpPinvoice.Incoterms__c                                = getincoTermId(proformaInvoice.Incoterms__c);
                tmpPinvoice.Port_of_Destination__c                      = getportId(proformaInvoice.Port_of_Destination__c);
                tmpPinvoice.Packing_List_Requirement__c                 = getPackingListId(proformaInvoice.Packing_List_Requirement__c);
                tmpPinvoice.Vehicle_type__c                             = getVehicleId(proformaInvoice.Vehicle_type__c);
                System.debug('==#14 Executed till here');
                System.debug('==#15 '+tmpPinvoice.Customer__c);
                 
                System.debug('===================');
            }
                
                //if(String.valueOf(proformaInvoice.Customer__c) != '') {
                    /*exptnHandler.ValidateCustomerFields(tmpPinvoice.Customer__c, tmpPinvoice.Ship_to_party__c);
                    System.debug('===#17 '+tmpPinvoice.Customer__c);*/
                //}
                accid = (String.valueOf(proformaInvoice.Customer__c) != '' ? proformaInvoice.Customer__c : customerId);
                    System.debug('===#16 Inside If ');
                    tmpPinvoice.Customer__c = accid;


                 
                //exptnHandler.ValidateCustomerFields(tmpPinvoice.Customer__c, tmpPinvoice.Ship_to_party__c);                                        
            update tmpPinvoice;
            PageReference pf = new PageReference(baseURL+'/'+tmpPinvoice.Id);
            pf.setRedirect(true);
            return pf;
        }catch(Exception  e) {
            throw new customException(e.getMessage());
        }
    }

    public String getAccountName(String userAccId) {
        //Map<String, Id> contactToAccountIdMap = new Map<String, Id>();
        Account tmpAcc = [SELECT Id, Name FROM Account WHERE Id =: userAccId];
        return tmpAcc.Name;
    }

    public static Id getCurrencyId(String currencyCode) {
        Id currencyId = [SELECT Id, Code__c FROM Document_Currency__c WHERE Code__c =: currencyCode].Id;
        System.debug(currencyId);
        return currencyId;
    }

    /*public static Id getTermsOfPaymentId(String paymentCode) {
        try {
           Id paymentId = [SELECT Id, Code__c FROM Terms_of_Payment__c WHERE Code__c =: paymentCode].Id;
            return paymentId; 
        }catch(Exception e) {
            return null;
        }
        

    }*/

    public static Id getincoTermId(String incoCode) {
        Id paymentId = [SELECT Id, Code__c FROM Inco_Terms__c WHERE Code__c =: incoCode].Id;
        return paymentId;
    }

    public static Id getportId(String portcode) {
        System.debug('==#3 '+portcode);
        Id portId = [SELECT Id, Locode__c FROM Port__c WHERE Locode__c =: portcode].Id;
        System.debug('==#4 '+portId);
        return portId;
    }

    public static Id getPackingListId(String packingCode) {
        Id packingId = [SELECT Id, Code__c FROM Packing_List__c WHERE Code__c =: packingCode].Id;
        return packingId;
    }

    public static Id getVehicleId(String vehicleCode) {
        System.debug('==#13 '+vehicleCode);
        Id vehicleId = [SELECT Id, Code__c FROM Vehicle_Type__c WHERE Code__c =: vehicleCode].Id;
        System.debug('==#13 '+vehicleId);
        return vehicleId;
    }


    public class customException extends Exception{}
}