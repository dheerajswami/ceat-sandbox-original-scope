//Generated by wsdl2apex

public class sap_zws_sfdc_sales_order_create {
    public class ZSFDC_SO_I {
        public String MATNR;
        public String WERKS;
        public String LGORT;
        public String MENGE;
        private String[] MATNR_type_info = new String[]{'MATNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] WERKS_type_info = new String[]{'WERKS','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LGORT_type_info = new String[]{'LGORT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MENGE_type_info = new String[]{'MENGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'MATNR','WERKS','LGORT','MENGE'};
    }
    public class ZSFDC_SO_H {
        public String AUART;
        public String VKORG;
        public String VTWEG;
        public String SPART;
        public String AUDAT;
        public String KUNNR;
        private String[] AUART_type_info = new String[]{'AUART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VKORG_type_info = new String[]{'VKORG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] VTWEG_type_info = new String[]{'VTWEG','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SPART_type_info = new String[]{'SPART','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] AUDAT_type_info = new String[]{'AUDAT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUNNR_type_info = new String[]{'KUNNR','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'AUART','VKORG','VTWEG','SPART','AUDAT','KUNNR'};
    }
    public class TABLE_OF_ZSFDC_SO_H {
        public sap_zws_sfdc_sales_order_create.ZSFDC_SO_H[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSFDC_SALES_ORDER_CREATE_element {
        public sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_H IT_OUT_H;
        public sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_I IT_OUT_I;
        private String[] IT_OUT_H_type_info = new String[]{'IT_OUT_H','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IT_OUT_I_type_info = new String[]{'IT_OUT_I','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_OUT_H','IT_OUT_I'};
    }
    public class TABLE_OF_ZSFDC_SO_I {
        public sap_zws_sfdc_sales_order_create.ZSFDC_SO_I[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSFDC_SALES_ORDER_CREATEResponse_element {
        public sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_H IT_OUT_H;
        public sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_I IT_OUT_I;
        public String RETURN_x;
        private String[] IT_OUT_H_type_info = new String[]{'IT_OUT_H','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IT_OUT_I_type_info = new String[]{'IT_OUT_I','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_OUT_H','IT_OUT_I','RETURN_x'};
    }
    public class zws_sfdc_sales_order_create {
        public String endpoint_x = 'http://CTECQHAP.ceat.in:8000/sap/bc/srt/rfc/sap/zws_sfdc_sales_order_create/300/zws_sfdc_sales_order_create/zws_sfdc_sales_order_create';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'sap_zws_sfdc_sales_order_create'};
        public sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATEResponse_element ZSFDC_SALES_ORDER_CREATE(sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_H IT_OUT_H,sap_zws_sfdc_sales_order_create.TABLE_OF_ZSFDC_SO_I IT_OUT_I) {
            sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATE_element request_x = new sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATE_element();
            request_x.IT_OUT_H = IT_OUT_H;
            request_x.IT_OUT_I = IT_OUT_I;
            sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATEResponse_element response_x;
            Map<String, sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATEResponse_element> response_map_x = new Map<String, sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATEResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:zws_sfdc_sales_order_create:ZSFDC_SALES_ORDER_CREATERequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_SALES_ORDER_CREATE',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_SALES_ORDER_CREATEResponse',
              'sap_zws_sfdc_sales_order_create.ZSFDC_SALES_ORDER_CREATEResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}