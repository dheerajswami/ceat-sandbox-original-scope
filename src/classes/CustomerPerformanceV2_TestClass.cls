@isTest(seeAllData = false)
public class CustomerPerformanceV2_TestClass{

	static Account acc;
	static sapSalesRegisterTrend_Prod_Mapping.SalesRegisterTrend_Prod_Mapping salesReg;

	public static void init(){
        acc = CEAT_InitializeTestData.createAccount('CustomerTest','50003327');
        database.insert(acc);
        
        SAPLogin__c saplogin 							= new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
    }
    private class WebServiceMockImpl implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {   
        	sapSalesRegisterTrend_Prod.ZWS_ZSFDC_SALES_REGISTER_GROUP zws  = new sapSalesRegisterTrend_Prod.ZWS_ZSFDC_SALES_REGISTER_GROUP();
            
            sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER tom = new sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER(); 
        	
            sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER e   = new sapSalesRegisterTrend_Prod.TABLE_OF_ZSFDC_SALES_REGISTER();

            
            List<sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER> zsList = new List<sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER>();
            sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER zs = new sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER();

            zs.KUNNR = '50003327';
            //this.NAME1 = NAME1;             
            zs.MATKL = '2220'; 
            zs.WGBEZ = 'TRUCK';
            zs.FKIMG = '2220';
            zs.NETWR = '2220';
            zs.MONTH = '03';     
            zs.YEAR  = '2014';  
            zsList.add(zs);
            
            e.item = zsList;
            
            sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER_GROUPResponse_element zsc = new sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER_GROUPResponse_element();
            zsc.IT_TEMP = e;
            sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER_GROUP_element zsf1 = new sapSalesRegisterTrend_Prod.ZSFDC_SALES_REGISTER_GROUP_element();
            zsf1.FDATE = '2014-12-12';
            zsf1.TDATE = '2015-12-12';
            zsf1.IT_TEMP = tom;
            zsf1.KUNNR = '50003327';
            

            response.put('response_x', zsc);
           // e=zws.ZSFDC_SALES_REGISTER_GROUP('2014-12-12', tom,'50003327','2015-12-12');
            return;
        }

    }
        public static testMethod void showSalesValue(){
      
        init();
        
        PageReference pg = Page.CustomerPerformance_V2;
        Test.setCurrentPage(pg);
        
        ApexPages.StandardController sc = new ApexPages.standardController(acc);
        Test.startTest() ;
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
         
        CustomerPerformanceV2 ctrl = new CustomerPerformanceV2(sc);

        CustomerPerformanceV2.calculateYTDValues(acc.Id) ;
        Map<String,String> catQuantity= new Map<String,string>();
        catQuantity.put('Apr@@CAR RAD','[0, 0, 0, 8.0]');
        catQuantity.put('Apr@@CAR RAD (TUBES)','[0, 0, 0, 6.0]');
        CustomerPerformanceV2.setCategory('CAR RAD', catQuantity);
       
        Test.stopTest();

        sapSalesRegisterTrend_Prod_Mapping.SalesRegisterTrend_Prod_Mapping[] srList = new List<sapSalesRegisterTrend_Prod_Mapping.SalesRegisterTrend_Prod_Mapping>();
    
}  

}