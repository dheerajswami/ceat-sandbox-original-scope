/**************************************************************************
 Developer : Sneha Agrawal
 Date      : 23rd June, 2015
 Purpose   : To merge Input / Output Dashboard 
***************************************************************************/

public with sharing class DashboardInputOutputHandler{        
   
    public string loggedInUserName {get;set;}
    public string loggedInUserTerritory {get;set;}
    public string loggedInUserRO {get;set;}
    public string loggedInUserZone {get;set;}
    public string loggedInUserRole {get;set;}
    
    public id userId      {get;set;}      
    public string selectedMonth {get;set;}
    public string selectedYear {get;set;}
    
    public Static String lastDateRun {get;set;}
    public Static String currentDay              = String.valueOf((Date.today()).day());
    public Static String currentMonth            = String.valueOf((Date.today()).month());
    public Static String currentYear             = String.valueOf((Date.today()).year());
    list<Dashboard_Score__c> dashboardScore1 = new list<Dashboard_Score__c>();
    // Main Wrapper Class
    
    public class dashboardWrapperMain{
        public  List<dashboardWrapper> outputDb {get;set;}
        public  dashboardInputWrapper inputDb {get;set;}
         
        dashboardWrapperMain(){
            
        }
    }    
   
   //Wrapper Classes  For Input
   
    public class dashboardInputWrapper{    
        public set<string> moduleSet     {get;set;}    
        public set<string> catSet     {get;set;}
        public list<Dashboard_Input_Score__c> dbInputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
    }    
    
    //Wrapper classes  For Output  
    
    public class dashboardWrapper implements Comparable{
        public String catName{get;set;}
        public Double score{get;set;}
        public Double catWeight{get;set;}
        public Decimal columnOrder{get;set;}
        public List<Dashboard_Score__c> dashboardScore{get;set;}
        public List<Dashboard_Output_Score__c> dashboardOutputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
        
        public Integer compareTo(Object compareTo) {
            dashboardWrapper compareToCat = (dashboardWrapper)compareTo;
            if (columnOrder == compareToCat.columnOrder) return 0;
            if (columnOrder > compareToCat.columnOrder) return 1;
            return -1;        
        }
    }
     // CONSTRUCTOR
     
    public DashboardInputOutputHandler(){
        
        list<Territory2> userTerritory = new list<Territory2>();
        String tlLabel = System.Label.TL_Role; 
        String tldLabel = System.Label.TLD_Role;
        user loggin = [SELECT id,name,UserRole.name,FirstName From User Where id =:UserInfo.getUserId() ];
        //loggedInUserName = loggin.FirstName;
        
        userId            = ApexPages.currentPage().getParameters().get('Id');
        selectedMonth     = ApexPages.currentPage().getParameters().get('month');
        selectedYear      = ApexPages.currentPage().getParameters().get('year');
        user logged = [Select name from User where id =:userId];
        loggedInUserName = logged.name;
        list<UserTerritory2Association> userTerrCode=[select territory2Id,id,RoleInTerritory2 from UserTerritory2Association where UserId=:userId AND (RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) limit 1];  
             
             if(userTerrCode[0].RoleInTerritory2.contains('Replacement')){
                loggedInUserRole = 'TL';
             }else{
                loggedInUserRole = userTerrCode[0].RoleInTerritory2 ;
             }
            
             if(userTerrCode.size() > 0){
                 userTerritory=[select name, id,ParentTerritory2.ParentTerritory2.name,ParentTerritory2.name from Territory2 where id=:userTerrCode[0].Territory2Id];
             }
             if(userTerritory.size() > 0){
                 loggedInUserTerritory=userTerritory[0].Name;
                 loggedInUserRO = userTerritory[0].ParentTerritory2.name;
                 loggedInUserZone = userTerritory[0].ParentTerritory2.ParentTerritory2.name;
             }
         dashboardScore1  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,User__c,LastModifiedDate FROm Dashboard_Score__c where User__c=:userId AND Month__c =: selectedMonth AND Year__c =: selectedYear Limit 1];
          if(dashboardScore1.size()>0){
             lastDateRun = dashboardScore1[0].LastModifiedDate.date().adddays(-1).format() ;
          }
         if(selectedMonth != string.valueof(date.today().month())){
          Integer numberOfDays = date.daysInMonth(integer.valueof(selectedYear), integer.valueof(selectedMonth));
          Date lastDayOfMonth  = date.newInstance(integer.valueof(selectedYear), integer.valueof(selectedMonth), numberOfDays);       
          lastDateRun          = string.valueof(lastDayOfMonth.format());
        }   
    }
    
     /*
    * Method Name   : getDashboardDataRecords
    * Description   : It will return Dashboard score, dashboard input/output score and dashboard summary score 
    *                 related to loggedin user territory for current month 
    * @param        : Loggedin User territory ex B0001
    * @returns      : dashboardWrapperMain
    */
    @RemoteAction
    public static dashboardWrapperMain getDashboardDataRecords(Id userId, string selectedMonth, string selectedYear){
         
        
        //********************************************** Output Section ***************************************************************************
       
        String catDashList             = '';
        
        list<Dashboard_Weightage_Master__c> dashboardWeight  = new List<Dashboard_Weightage_Master__c>(); 
        list<Dashboard_Score__c> dashboardScore              = new List<Dashboard_Score__c>(); 
        list<Dashboard_Output_Score__c> dashboardOutputScore = new List<Dashboard_Output_Score__c>(); 
        list<Dashboard_Score__c> dashScorList ;
        
        list<Dashboard_Output_Score__c> dashoutScore ;
        list<Dashboard_Master__c> dashboardMaster            = new list<Dashboard_Master__c>();
        list<string> dashcatSet                              = new list<String>();
        list<String> catlist = new List<String>();
        
        Dashboard_Summary_Score__c dashSummaryScore          = new Dashboard_Summary_Score__c();
        Set<String> SetOFCat = new Set<String>();
        List<String> catsetParam                             = new List<String>();
        Set<String> setOFParametersAllCat                    = new Set<String>();
        Map<String,String> MapOFCatAndCode                   = new Map<String,String>();
        List<String> catSet                                  = new List<String>();
        Set<String> catdashSet                               = new Set<String>();
        Map<String,Double> MapOFCatAndWeigth                 = new Map<String,Double>();
        map<id,string> mapofTerritoryType                = new map<id,string>();
        Map<String,String> mapOFDashCatAndCatName            = new Map<String,String>();
        
        List<Dashboard_Score__c> daList                      = new list<Dashboard_Score__c>();
        List<Dashboard_Output_Score__c > daoutList           = new list<Dashboard_Output_Score__c >();
        list<UserTerritory2Association > userASS             = new list<UserTerritory2Association>();
        Dashboard_Score__c dsrecord ;
        
        Map<String,List<Dashboard_Score__c>> MapOFCatAndDashboardScore              = new Map<String,list<Dashboard_Score__c>>();
        Map<String,List<Dashboard_Output_Score__c >> MapOFCatAndDashboardOutScore   = new Map<String,list<Dashboard_Output_Score__c >>();
        Map<String,Dashboard_Weightage_Master__c> mapOFCatAndParam                  = new Map<String,Dashboard_Weightage_Master__c>();
        Map<String,List<String>> mapOFDashCatAndCatCode                             = new Map<String,List<String>>();
        list<Sales_Planning_Categories__c> dashboardCat                             = Sales_Planning_Categories__c.getall().values();
            for(Sales_Planning_Categories__c sp:dashboardCat){
              if(sp.Include_in_Dashboard__c == true){
                catdashSet.add(sp.Category_Code__c); 
                if(mapOFDashCatAndCatCode.containsKey(sp.Category_name_as_in_Dashboard__c)) {
                    catSet          = new List<String>();
                    catSet          = mapOFDashCatAndCatCode.get(sp.Category_name_as_in_Dashboard__c); 
                    catset.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }else{
                    catSet          = new List<String>();
                    catSet.add(sp.Category_Code__c);
                    mapOFDashCatAndCatCode.put(sp.Category_name_as_in_Dashboard__c,catset);
                    mapOFDashCatAndCatName.put(sp.Category_name_as_in_Dashboard__c,sp.Name);
                }
              }      
        }
        userASS = [SELECT IsActive,UserId, RoleInTerritory2, Territory2Id, Territory2.DeveloperName FROM UserTerritory2Association WHERE (RoleInTerritory2 = 'TLD' OR RoleInTerritory2 = 'TL_Replacement')];
        if(userASS.size() > 0){
            for(UserTerritory2Association u : userAss){
                mapofTerritoryType.put(u.UserId,u.RoleInTerritory2);           
            }
        }
        /*dashboardMaster             = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true  AND Role__c =:  mapofTerritoryType.get(userId)];
        dashboardWeight = [SELECT id,Category__c,Parameters_Output__c,Role__c,Testing__c,Weightage__c FROM Dashboard_Weightage_Master__c where RecordType.Name='Output' AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c =:  mapofTerritoryType.get(userId) AND testing__c = true];
       
        
        for(Dashboard_Weightage_Master__c param : dashboardWeight){
            mapOFCatAndParam.put(param.Category__c,param);
            catsetParam             = mapOFDashCatAndCatCode.get(param.Category__c);
            dashcatSet.addAll(catsetParam);
            String[] paramSplit     = (param.Parameters_Output__c).split(';');
            if(paramSplit.size() > 0){
                for(String st : paramSplit){
                    setOFParametersAllCat.add(st);
                }
            }  
        }*/
        if(userId!=null){
             dashboardScore  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Role__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__r.Category__c,Dashboard_Weightage_Master__r.Parameters_Output__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c,LastModifiedDate FROM Dashboard_Score__c where User__c=:userId AND Month__c =: selectedMonth AND Year__c =: selectedYear /*AND Parameters__c IN : setOFParametersAllCat*/];
            dashboardOutputScore = [SELECT ID,Total_Monthly_Target__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Final_Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c where User__c=:userId  AND Month__c =: selectedMonth AND Year__c =: selectedYear];//where DBExternalId__c='2010B008242015'
            dashSummaryScore = [SELECT ID,Achievement__c,All_Category_Score1__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Weightage__c,DSS_External_ID__c,Month__c,Score__c,Territory_Code__c,Total_MTD_Actual__c,Total_MTD_Target__c,Total_Output_Score__c,Total_Value__c,Total_Value_L3M__c,Total_Value_LYCM__c,Year__c FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId AND Month__c =: selectedMonth AND Year__c =: selectedYear];
        }else{
             dashboardScore  = [SELECT id,Actual_MTD__c,Number_of_Dealers_Distributors__c,Category__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Role__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__r.Category__c,Dashboard_Weightage_Master__r.Parameters_Output__c,L3M__c,LYCM__c,Month__c,Monthly_Target__c,MTD_Target__c,
             No_of_Dealer_Distributor_Billed_Since__c,No_of_Dealer_Distributor_Plan__c,Parameters__c,Score__c,Testing__c,User__c,Year__c,Achievement_MTD__c FROM Dashboard_Score__c];
             dashboardOutputScore = [SELECT ID,Total_Monthly_Target__c,Final_Score__c,Total_No_of_Dealers_Distributor__c,Total_LYCM__c,Total_Actual_MTD__c,Total_MTD_Target__c,Total_No_of_dealers_distributors_planned__c,Total_L3M__c,Total_dealers_distributors_billed_since__c,Achievement_MTDTest__c,Category__c,Dashboard_Weightage_Master__c,Score__c,Score1__c,Testing__c,Territory_Code__c,User__c FROM Dashboard_Output_Score__c];
        
        }
        /*
        for(Dashboard_Weightage_Master__c ss : dashboardWeight ){
            SetOFCat.add(ss.Category__c);
            MapOFCatAndWeigth.put(ss.Category__c,ss.Weightage__c);
        }   */    
        
        for(Dashboard_Score__c ds : dashboardScore  ){
               dashScorList = new list<Dashboard_Score__c>();
               if(MapOFCatAndDashboardScore.containsKey(ds.Category__c)){
                   dashScorList = MapOFCatAndDashboardScore.get(ds.Category__c);
                   dashScorList.add(ds);
                   MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
                   SetOFCat.add(ds.Dashboard_Weightage_Master__r.Category__c);
                   MapOFCatAndWeigth.put(ds.Dashboard_Weightage_Master__r.Category__c,ds.Dashboard_Weightage_Master__r.Weightage__c);
               }else{
                   dashScorList = new list<Dashboard_Score__c>();
                   dashScorList.add(ds);
                   MapOFCatAndDashboardScore.put(ds.Category__c,dashScorList);
                   SetOFCat.add(ds.Dashboard_Weightage_Master__r.Category__c);
                   MapOFCatAndWeigth.put(ds.Dashboard_Weightage_Master__r.Category__c,ds.Dashboard_Weightage_Master__r.Weightage__c);
               }
               //daList.add(ds);               
        }
       
       // MapOFCatAndDashboardScore.put('2010',daList);
        for(Dashboard_Output_Score__c temp : dashboardOutputScore ){
            dashoutScore = new list<Dashboard_Output_Score__c>();
            if(MapOFCatAndDashboardOutScore.containsKey(temp.Category__c)){
                   dashoutScore = MapOFCatAndDashboardOutScore.get(temp.Category__c);
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }else{
                   dashoutScore= new list<Dashboard_Output_Score__c>();
                   dashoutScore.add(temp);
                   MapOFCatAndDashboardOutScore.put(temp.Category__c,dashoutScore);
               }
           // daoutList .add(temp);
        }
        
        //MapOFCatAndDashboardOutScore.put('2010',daoutList);
        List<dashboardWrapper> dashboardRecord      = new List<dashboardWrapper>();
        
        for(String str : SetOFCat){
             dashboardWrapper c         =   new dashboardWrapper();
             c.catName = str;
            // c.score= 20;
             c.catWeight = MapOFCatAndWeigth.get(str);
             c.columnOrder = (Sales_Planning_Categories__c.getValues(mapOFDashCatAndCatName.get(str))).Dashboard_Sort_Order__c;
             catset = new list<String>();
             catlist = new List<String>();
             if(mapOFDashCatAndCatCode.ContainsKey(str)){
                  catlist=    mapOFDashCatAndCatCode.get(str);
                  catlist.sort();
             }
             if(catlist.size() > 0){
                catDashList = '';
                for(String s : catlist){
                    if(catDashList == ''){
                        catDashList = s;
                    }else{
                        catDashList = catDashList +':'+ s;
                    }   
                }
            }
        
        
             c.dashboardScore = MapOFCatAndDashboardScore.get(catDashList);
             c.dashboardOutputScore  = MapOFCatAndDashboardOutScore.get(catDashList);
             c.dashboardSummaryScore = dashSummaryScore;
             
             if(MapOFCatAndDashboardOutScore.get(catDashList) !=null && MapOFCatAndDashboardScore.get(catDashList)!=null){
                 dashboardRecord.add(c);
             }
             dashboardRecord.sort();
        }
        
        //********************************************** Input Section ***************************************************
       
         string dealers                  = system.Label.TL_PJP_nput;    
         string distributors             = system.Label.TL_Distributor;
         string truckCustomer            = system.Label.TL_Efleet; 
         string actionLog                = system.Label.TL_Action_Log;   
         
         String tldRole                       = system.Label.TLD_Role; 
          map<Integer,String> monthYearMap = new map<Integer,String>();
         /*
                To convert current month into month name
        */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');
            
        Integer month                    = Date.today().month();
        String currentMonth1             = monthYearMap.get(Integer.valueof(selectedMonth));
        String currentYear1              = String.valueOf((Date.today()).year());
        string TLInputRecType           = system.Label.Input_RecType;
        
        string userRole = mapofTerritoryType.get(userId);
        if(userRole !=null){
            if(userRole.contains('Replacement')){
                userRole = 'TL';
            }
        }
        Set<String> SetOFCatEfleet                                        = new Set<String>();        
       
        list<Dashboard_Input_Score__c> dashboardInputScore                = new List<Dashboard_Input_Score__c>(); 
        
        Dashboard_Summary_Score__c dashSummaryScore1                      = new Dashboard_Summary_Score__c();
        map<String,Double> MapOFCatAndWeigtage                            = new map<String,Double>();
       
        /* Getting  Input score / Summary score for particular territory */
        if(Integer.valueof(selectedMonth) == month){
            if(userId!=null){
            dashboardInputScore  = [SELECT ID,Achievement__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where RecordType.Name=:userRole AND User__c=:userId  AND  Month__c =: currentMonth1 AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
            dashSummaryScore1    = [SELECT ID,DSS_External_ID__c,Input_Score__c,Territory_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId  AND  Month__c =: selectedMonth AND Year__c =: selectedYear limit 1];
            }else{
                dashboardInputScore = [SELECT ID,ScoreMTD__c,Achievement__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c  where RecordType.Name=:userRole AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
            } 
        }else{
            if(userId!=null){
            dashboardInputScore  = [SELECT ID,Achievement__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where RecordType.Name=:userRole AND User__c=:userId  AND  Month__c =: currentMonth1 AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Weightage__c>0 ];
            dashSummaryScore1    = [SELECT ID,DSS_External_ID__c,Input_Score__c,Territory_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE OwnerId=:userId  AND  Month__c =: selectedMonth AND Year__c =: selectedYear limit 1];
            }else{
                dashboardInputScore = [SELECT ID,ScoreMTD__c,Achievement__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c  where RecordType.Name=:userRole AND Dashboard_Weightage_Master__r.Weightage__c>0 ];
            } 
        }
        
        
        /* Creating Input wrapper instance to create input Dashboard records */
        
        dashboardInputWrapper dashboardRecord1           = new dashboardInputWrapper();
        dashboardRecord1.catSet                          = new set<string>();
        dashboardRecord1.moduleSet                       = new set<string>();
        
        for(Dashboard_Input_Score__c ss : dashboardInputScore){
                dashboardRecord1.catSet.add(ss.RecordTypeName__c+'@@'+ss.Category__c);               
        } 
        if(mapofTerritoryType.get(userId) != tldRole){
            dashboardRecord1.moduleSet.add(dealers);        
            dashboardRecord1.moduleSet.add(truckCustomer);          
            dashboardRecord1.moduleSet.add(actionLog);
        }else{          
            dashboardRecord1.moduleSet.add(distributors);
            dashboardRecord1.moduleSet.add(actionLog);
        }
        dashboardRecord1.dbInputScore               = dashboardInputScore;
        dashboardRecord1.dashboardSummaryScore      = dashSummaryScore1;
      
        /* Creating Main Wrapper Record*/
        dashboardWrapperMain wrapper = new dashboardWrapperMain();
        wrapper.inputDb              = dashboardRecord1;
        wrapper.outputDb             = dashboardRecord;        
        return wrapper;    
    }
}