public with sharing class PIlineItemTriggerHandler {
    private static PIlineItemTriggerHandler classInstance;

    static {
        classInstance = new PIlineItemTriggerHandler();
    }

    private PIlineItemTriggerHandler() {}

    public static PIlineItemTriggerHandler getInstance() {
        return classInstance;
    }

    /*public void onBeforeInsert(final List<PI_Line_Item__c> newPIlineItemList) {
        List<PI_Line_Item__c> pIlineItemList = new List<PI_Line_Item__c>();
        for(PI_Line_Item__c tmpPILitem : newPIlineItemList) {
            if(tmpPILitem.Quantity__c != null && tmpPILitem.Material_Number__c != null) {
                pIlineItemList.add(tmpPILitem);
            }
        }
        if(!pIlineItemList.isEmpty()) {
            populateLoadability(newPIlineItemList);
        }
    }*/

    public void onBeforeUpdate(final List<PI_Line_Item__c> newPIlineItemList, final Map<Id, PI_Line_Item__c> oldPIlineItemMap) {
        List<PI_Line_Item__c> piLineItemList = new List<PI_Line_Item__c>();
        List<PI_Line_Item__c> piLineItemToResetList = new List<PI_Line_Item__c>();
        for(PI_Line_Item__c tmpPILitem : newPIlineItemList) {
            PI_Line_Item__c oldPILitem = oldPIlineItemMap.get(tmpPILitem.Id);
            //System.debug('==#1 '+tmpPILitem.Material_Number__c + ' '+oldPILitem.Material_Number__c);
            //System.debug('==#2 '+tmpPILitem.Quantity__c + ' '+oldPILitem.Quantity__c);
            if((tmpPILitem.Material_Master__c != oldPILitem.Material_Master__c && tmpPILitem.Material_Master__c != null) || (tmpPILitem.Quantity__c != oldPILitem.Quantity__c && tmpPILitem.Quantity__c != null)) {
                piLineItemList.add(tmpPILitem);
            }
            if((tmpPILitem.Material_Master__c != oldPILitem.Material_Master__c && tmpPILitem.Material_Master__c == null) || (tmpPILitem.Quantity__c != oldPILitem.Quantity__c && tmpPILitem.Quantity__c == null)) {
                piLineItemToResetList.add(tmpPILitem);
            }
        }
        //System.debug('==#3 '+piLineItemList);
        if(!piLineItemList.isEmpty()) {
            populateLoadability(piLineItemList);
        }
        if(!piLineItemToResetList.isEmpty()) {
            resetNoOfContainersValue(piLineItemToResetList);
        }
    }

    public void populateLoadability(List<PI_Line_Item__c> piLineItemList) {
        //System.debug('==#4 '+piLineItemList);
        Map<Id, Material_Master_Sap__c> skuToMaterialMasterMap = new Map<Id, Material_Master_Sap__c>([SELECT Id, Material_Number__c, Name FROM Material_Master_Sap__c WHERE Name != '.']);
        Map<String, string> materialNoToCapacityMap = new  Map<String, string>();
        Double capacity;
        try{
            for(Loadability__c tmpLoad : [SELECT Capacity__c,Category__c,Group__c,Material_Number__c,Name FROM Loadability__c]) {
                if(!materialNoToCapacityMap.containsKey(tmpLoad.Material_Number__c)) {
                    //System.debug('==#5 '+materialNoToCapacityMap);
                    materialNoToCapacityMap.put(tmpLoad.Material_Number__c, tmpLoad.Capacity__c);
                }
            }
            System.debug('==#6 '+materialNoToCapacityMap);
            for(PI_Line_Item__c tmpPILitem : piLineItemList) {
                //if(!Test.isRunningTest()) {
                    System.debug('==#6.1 '+ skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Material_Number__c);
                    System.debug('====#7 '+materialNoToCapacityMap.get(String.valueOf(Integer.valueOf(skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Material_Number__c))));
                    capacity = Double.valueOf(materialNoToCapacityMap.get(String.valueOf(Integer.valueOf(skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Material_Number__c))));
                    //System.debug('==#7 '+capacity);
                    //System.debug('==#8 '+(tmpPILitem.Quantity__c / capacity));
                    tmpPILitem.No_of_Container__c = tmpPILitem.Quantity__c / capacity;
                    System.debug('==#8 '+tmpPILitem.Material_Number__c);
                    tmpPILitem.Material_Number__c = skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Name;
                    tmpPILitem.Brand__c           = skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Mat_Grp1_Desc__c;
                    tmpPILitem.Category__c        = skuToMaterialMasterMap.get(tmpPILitem.Material_Master__c).Mat_Grp_Desc__c;
                    System.debug('==#9 '+tmpPILitem.Material_Number__c);
                //}
            }
        }
        catch(Exception e) {
            e.getMessage();
        }

    }

    public void resetNoOfContainersValue(List<PI_Line_Item__c> piLineItemToResetList) {
        for(PI_Line_Item__c tmpPILitem : piLineItemToResetList) {
            tmpPILitem.No_of_Container__c = 0;
        }
    }
}