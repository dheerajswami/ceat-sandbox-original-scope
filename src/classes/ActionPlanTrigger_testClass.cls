@isTest(seeAllData = false)
public  class ActionPlanTrigger_testClass {
    
     static testMethod void testActionplanApproval(){
     	Priority_Area__c pa = new Priority_Area__c(Region1__c = 'KAN',Priority_Area__c = 'test data',Start_Date__c = date.today(),End_Date__c = date.today().addDays(30));
    	insert pa;
    	
    	Action_Plan__c ap = new Action_Plan__c(Priority_Area__c = pa.Id,Action_Planning__c = 'test plan',M1_Proposed_Impact__c = 12, M2_Proposed_Impact__c = 12, M3_Proposed_Impact__c = 12);
    	insert ap;
    	Test.startTest();
    	ap.M1_Accrued_Impact__c = 10;
    	 ap.M2_Accrued_Impact__c = 10;
    	 ap.M3_Accrued_Impact__c = 10;
    	 update ap;
    	Test.stopTest();
    }
}