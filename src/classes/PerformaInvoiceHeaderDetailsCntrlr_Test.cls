/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
public class PerformaInvoiceHeaderDetailsCntrlr_Test {
	public static User portalUserExports;
	public static Packing_List__c pc;
	public static Account acc1;
	public static Account acc2;
	public static Contact contact;
	public static Port__c prt1;
	public static Material_Master_Sap__c  materialMaster1;
	public static Material_Master_Sap__c materialMaster2;
	public static Material_Master_Sap__c materialMaster3;
	public static Vehicle_Type__c vt;
	public static Inco_Terms__c it;
	public static Terms_of_Payment__c tp;
	public static Document_Currency__c dc;
	public static Loadability__c loadability;
	public static Loadability__c loadability1;
	public static Loadability__c loadability2;
	public static String accid;
	public static Network network;
	public static NetworkMember networkMember;
	public static String enquiryDte;
	public static String delvDte;

	public static void init() {

		enquiryDte = String.ValueOf(System.today());
	    delvDte    = String.ValueOf(System.today() + 60);
 
		acc1 = CEAT_InitializeTestData.createAccForExportsRecType('Acc1', '5000234509','ro','zne','Karnataka');
	    insert acc1;
		contact = CEAT_InitializeTestData.createContact('TestCon', acc1.Id);
		insert contact;
		portalUserExports = CEAT_InitializeTestData.createPortalUserExports(contact.Id);
        insert portalUserExports;

        /*networkMember = new NetworkMember();
        networkMember.MemberId = portalUserExports.Id;
        networkMember.NetworkId = network.Id;
        insert networkMember;*/

		pc = CEAT_InitializeTestData.createPacking_List('packingListName','pack');
	    insert pc;
        

        
        acc2 = CEAT_InitializeTestData.createAccForExportsRecType('Acc2', '5000234510','ro1','zn1','TamilNadu');
        acc2.parentId=acc1.id;
        acc2.Active__c=True;
        insert acc2;
	    accid= acc2.id;
	    
	    prt1 = CEAT_InitializeTestData.createPort('US', 'United States', 'Flag','Locode','USAPort' ,'1234');
	    insert prt1;
	    
	    materialMaster1 = CEAT_InitializeTestData.createMaterialMasterSap('101164','CET', 'TRUCK', 'SKU1', 'Ceat','DIEN');
	    materialMaster1.Material_Group__c = '2010';
	    insert materialMaster1;

	    materialMaster2 = CEAT_InitializeTestData.createMaterialMasterSap('101160', 'ALT', 'TRUCK', 'SKU2', 'Altura', 'ZFGS');
	    materialMaster2.Material_Group__c = '2030';
	    insert materialMaster2;

	    materialMaster3 = CEAT_InitializeTestData.createMaterialMasterSap('101165', 'ALT', 'INDUSTRIAL (TUBES)', 'SKU3', 'Altura', 'ZFGS');
	    materialMaster2.Material_Group__c = '2020';
	    insert materialMaster3;

	    loadability =CEAT_InitializeTestData.createLoadability('150', '2010', 'TRUCK', '101164');
  		insert loadability;
  		loadability1 =CEAT_InitializeTestData.createLoadability('150', '2030', 'TRUCK', '101160');
  		insert loadability1;
  		loadability2 =CEAT_InitializeTestData.createLoadability('150', '2020', 'INDUSTRIAL (TUBES)', '101165');
  		insert loadability2;

	    vt = CEAT_InitializeTestData.createVehicleType('veh');
	    insert vt;	

	    it = CEAT_InitializeTestData.createIncoTerms('inco');
	    insert it;

	    tp = CEAT_InitializeTestData.createTermsOfPayment('pay');
	    insert tp;

	    dc = CEAT_InitializeTestData.createDocumentCurrency('doc');
	    insert dc; 

	}
	
    static testmethod void unitTest1(){
    	init();
        Brand__c setting = new Brand__c();
		setting.Name = 'Ceat';
		insert setting;
		Brand__c setting1 = new Brand__c();
		setting1.Name = 'Altura';
		insert setting1;

        Test.startTest();
	        
	        Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
			proformaInvoice.Customer__c = acc1.Id ;
			proformaInvoice.Division__c = '07';
			proformaInvoice.Requested_delivery_date__c = System.today();
			proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
			proformaInvoice.Sales_Document_Type__c = '01';
			proformaInvoice.SD_Document_Currency__c = dc.Id;
			proformaInvoice.Terms_of_Payment__c = tp.Id;
			proformaInvoice.Incoterms__c = it.Id;
			proformaInvoice.Advance_Payment__c = 'No';
			proformaInvoice.Mixing_of_Pro_forma__c = 'No';
			proformaInvoice.Port_of_Destination__c = prt1.Id;
			proformaInvoice.Ship_to_party__c = acc2.Id;
			proformaInvoice.Import_License_Requirement__c = 'No';
			proformaInvoice.Packing_List_Requirement__c = pc.Id;
			proformaInvoice.Vehicle_type__c = vt.Id;
			proformaInvoice.Inspection_Required__c = 'No';
			proformaInvoice.Customized_Container__c = 'No';
			proformaInvoice.Nomination_of_shipping_line__c = 'No';
			proformaInvoice.Clubbing_of_containers__c = 'No';
			proformaInvoice.LC_Required__c = 'No';
	    
	        PI_Line_Item__c piLineItem = new PI_Line_Item__c();
			piLineItem.Brand__c = 'CET';
			piLineItem.Quantity__c = 100;
			piLineItem.Material_Number__c = '101164';
			piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

	        List<PI_Line_Item__c> piLine = new  List<PI_Line_Item__c>();
	        piLine.add(piLineItem);
	        PageReference pageRef = new PageReference ('apex/PerformaInvoiceHeaderDetails_V9');
			Test.setCurrentPageReference(pageRef);
	        ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
        	PerformaInvoiceHeaderDetailsControllerV2 pdc= new PerformaInvoiceHeaderDetailsControllerV2(sc);

	        PerformaInvoiceHeaderDetailsControllerV2.searchAccounts(acc1.Id);
	        PerformaInvoiceHeaderDetailsControllerV2.portOption('US');
	        PerformaInvoiceHeaderDetailsControllerV2.shipToPartyOption(acc1.Id);
	        PerformaInvoiceHeaderDetailsControllerV2.shipToPartyOption(acc2.Id);
	        PerformaInvoiceHeaderDetailsControllerV2.fetchMaterialGroupDesc('Ceat','DIEN');
	        PerformaInvoiceHeaderDetailsControllerV2.fetchSKUs('TRUCK', 'Ceat', 'DIEN');
	        PerformaInvoiceHeaderDetailsControllerV2.getportId('Locode');
	        PerformaInvoiceHeaderDetailsControllerV2.getPackingListId('pack');
	        PerformaInvoiceHeaderDetailsControllerV2.getVehicleId('veh');
	        PerformaInvoiceHeaderDetailsControllerV2.getCurrencyId('doc');
	        PerformaInvoiceHeaderDetailsControllerV2.getincoTermId('inco');
	        //PerformaInvoiceHeaderDetailsControllerV2.getTermsOfPaymentId('pay');
	        PerformaInvoiceHeaderDetailsControllerV2.saveInvoice('enquiryDte','delvDte', proformaInvoice, piLine);
	        PerformaInvoiceHeaderDetailsControllerV2.calculateLoadibility('SKU2', 123.0);
	        //PerformaInvoiceHeaderDetailsControllerV2.getAccountName(acc1.Id);
	    Test.stopTest();
	}

	static testmethod void unitTest2() {
		init();
	        
	        Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
			proformaInvoice.Customer__c = acc1.Id ;
			proformaInvoice.Division__c = '07';
			proformaInvoice.Requested_delivery_date__c = System.today();
			proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
			proformaInvoice.Sales_Document_Type__c = '01';
			proformaInvoice.SD_Document_Currency__c = dc.Id;
			//proformaInvoice.Terms_of_Payment__c = tp.Id;
			proformaInvoice.Incoterms__c = it.Id;
			proformaInvoice.Advance_Payment__c = 'No';
			proformaInvoice.Mixing_of_Pro_forma__c = 'No';
			proformaInvoice.Port_of_Destination__c = prt1.Id;
			proformaInvoice.Ship_to_party__c = acc2.Id;
			proformaInvoice.Import_License_Requirement__c = 'No';
			proformaInvoice.Packing_List_Requirement__c = pc.Id;
			proformaInvoice.Vehicle_type__c = vt.Id;
			proformaInvoice.Inspection_Required__c = 'No';
			proformaInvoice.Customized_Container__c = 'No';
			proformaInvoice.Nomination_of_shipping_line__c = 'No';
			proformaInvoice.Clubbing_of_containers__c = 'No';
			proformaInvoice.LC_Required__c = 'No';

			PI_Line_Item__c piLineItem = new PI_Line_Item__c();
			piLineItem.Brand__c = 'CET';
			piLineItem.Quantity__c = 100;
			piLineItem.Material_Number__c = '101164';
			piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

	        List<PI_Line_Item__c> piLine = new  List<PI_Line_Item__c>();
	        piLine.add(piLineItem);

			System.runAs(portalUserExports) {

				PageReference pageRef = new PageReference ('customers/apex/PerformaInvoiceHeaderDetails_V9');
				Test.setCurrentPageReference(pageRef);
				Test.startTest();
					ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
	        		PerformaInvoiceHeaderDetailsControllerV2 pdc= new PerformaInvoiceHeaderDetailsControllerV2(sc);
					PerformaInvoiceHeaderDetailsControllerV2.fetchMaterialGroupDesc('Altura','ZFGS');
					PerformaInvoiceHeaderDetailsControllerV2.fetchSKUs('TRUCK', 'Altura', 'ZFGS');
					PerformaInvoiceHeaderDetailsControllerV2.saveInvoice('enquiryDte','delvDte', proformaInvoice, piLine);
		        Test.stopTest();
    		}
	}

	static testmethod void unitTest3(){
    	init();
        
        	System.runAs(portalUserExports) {
	        
	        Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
			proformaInvoice.Customer__c = acc1.Id ;
			proformaInvoice.Division__c = '07';
			proformaInvoice.Requested_delivery_date__c = System.today();
			proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
			proformaInvoice.Sales_Document_Type__c = '01';
			proformaInvoice.SD_Document_Currency__c = dc.Id;
			proformaInvoice.Terms_of_Payment__c = tp.Id;
			proformaInvoice.Incoterms__c = it.Id;
			proformaInvoice.Advance_Payment__c = 'No';
			proformaInvoice.Mixing_of_Pro_forma__c = 'No';
			proformaInvoice.Port_of_Destination__c = prt1.Id;
			proformaInvoice.Ship_to_party__c = acc2.Id;
			proformaInvoice.Import_License_Requirement__c = 'No';
			proformaInvoice.Packing_List_Requirement__c = pc.Id;
			proformaInvoice.Vehicle_type__c = vt.Id;
			proformaInvoice.Inspection_Required__c = 'No';
			proformaInvoice.Customized_Container__c = 'No';
			proformaInvoice.Nomination_of_shipping_line__c = 'No';
			proformaInvoice.Clubbing_of_containers__c = 'No';
			proformaInvoice.LC_Required__c = 'No';
			proformaInvoice.System_Ship_to_Country__c = 'US';

			insert proformaInvoice;
	    
	        PI_Line_Item__c piLineItem = new PI_Line_Item__c();
			piLineItem.Brand__c = 'CET';
			piLineItem.Quantity__c = 100;
			piLineItem.Material_Number__c = '101164';
			piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

			insert piLineItem;

	        List<PI_Line_Item__c> piLine = new  List<PI_Line_Item__c>();
	        piLine.add(piLineItem);
	        	System.debug('==#InsideTestMethod '+proformaInvoice.Id);
		        PageReference pageRef = new PageReference ('/customers/apex/PerformaInvoiceHeaderDetails_V10?id='+proformaInvoice.Id);
				Test.setCurrentPageReference(pageRef);
				Test.startTest();
		        	ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
	        		PerformaInvoiceHeaderDetailsControllerV2 pdc= new PerformaInvoiceHeaderDetailsControllerV2(sc);
	        		PerformaInvoiceHeaderDetailsControllerV2.updateInvoice(enquiryDte, enquiryDte, delvDte, proformaInvoice);
	        	Test.stopTest();
        	}   
	}

	static testmethod void unitTest4() {
		init();
	        
	        Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
			proformaInvoice.Customer__c = acc1.Id ;
			proformaInvoice.Division__c = '07';
			proformaInvoice.Requested_delivery_date__c = System.today();
			proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
			proformaInvoice.Sales_Document_Type__c = '01';
			proformaInvoice.SD_Document_Currency__c = dc.Id;
			//proformaInvoice.Terms_of_Payment__c = tp.Id;
			proformaInvoice.Incoterms__c = it.Id;
			proformaInvoice.Advance_Payment__c = '';
			proformaInvoice.Mixing_of_Pro_forma__c = 'No';
			proformaInvoice.Port_of_Destination__c = prt1.Id;
			proformaInvoice.Ship_to_party__c = acc2.Id;
			proformaInvoice.Import_License_Requirement__c = 'No';
			proformaInvoice.Packing_List_Requirement__c = pc.Id;
			proformaInvoice.Vehicle_type__c = vt.Id;
			proformaInvoice.Inspection_Required__c = 'No';
			proformaInvoice.Customized_Container__c = 'No';
			proformaInvoice.Nomination_of_shipping_line__c = 'No';
			proformaInvoice.Clubbing_of_containers__c = 'No';
			proformaInvoice.LC_Required__c = 'No';

			PI_Line_Item__c piLineItem = new PI_Line_Item__c();
			piLineItem.Brand__c = 'CET';
			piLineItem.Quantity__c = 100;
			piLineItem.Material_Number__c = '101164';
			piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

	        List<PI_Line_Item__c> piLine = new  List<PI_Line_Item__c>();
	        piLine.add(piLineItem);

			System.runAs(portalUserExports) {

				PageReference pageRef = new PageReference ('customers/apex/PerformaInvoiceHeaderDetails_V9');
				Test.setCurrentPageReference(pageRef);
				Test.startTest();
				ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
        		PerformaInvoiceHeaderDetailsControllerV2 pdc= new PerformaInvoiceHeaderDetailsControllerV2(sc);
				try {
					PerformaInvoiceHeaderDetailsControllerV2.saveInvoice('enquiryDte','delvDte', proformaInvoice, piLine);
				}catch(Exception e) {
					return;
				}
	        	Test.stopTest();
    		}
	}

	static testmethod void unitTest5() {
		init();
	        
	        Proforma_Invoice__c proformaInvoice = new Proforma_Invoice__c();
			proformaInvoice.Customer__c = acc1.Id ;
			proformaInvoice.Division__c = '07';
			proformaInvoice.Requested_delivery_date__c = System.today();
			proformaInvoice.Quotation_Inquiry_is_valid_from__c = System.today();
			proformaInvoice.Sales_Document_Type__c = '01';
			proformaInvoice.SD_Document_Currency__c = dc.Id;
			//proformaInvoice.Terms_of_Payment__c = tp.Id;
			proformaInvoice.Incoterms__c = it.Id;
			proformaInvoice.Advance_Payment__c = '';
			proformaInvoice.Mixing_of_Pro_forma__c = 'No';
			proformaInvoice.Port_of_Destination__c = prt1.Id;
			proformaInvoice.Ship_to_party__c = acc2.Id;
			proformaInvoice.Import_License_Requirement__c = 'No';
			proformaInvoice.Packing_List_Requirement__c = pc.Id;
			proformaInvoice.Vehicle_type__c = vt.Id;
			proformaInvoice.Inspection_Required__c = 'No';
			proformaInvoice.Customized_Container__c = 'No';
			proformaInvoice.Nomination_of_shipping_line__c = 'No';
			proformaInvoice.Clubbing_of_containers__c = 'No';
			proformaInvoice.LC_Required__c = 'No';

			PI_Line_Item__c piLineItem = new PI_Line_Item__c();
			piLineItem.Brand__c = 'CET';
			piLineItem.Quantity__c = 100;
			piLineItem.Material_Number__c = '101164';
			piLineItem.Proforma_Invoice__c = proformaInvoice.Id;

	        List<PI_Line_Item__c> piLine = new  List<PI_Line_Item__c>();
	        //piLine.add(piLineItem);

			System.runAs(portalUserExports) {

				PageReference pageRef = new PageReference ('customers/apex/PerformaInvoiceHeaderDetails_V9');
				Test.setCurrentPageReference(pageRef);
				Test.startTest();
				ApexPages.StandardController sc = new ApexPages.StandardController(proformaInvoice);
        		PerformaInvoiceHeaderDetailsControllerV2 pdc= new PerformaInvoiceHeaderDetailsControllerV2(sc);
				try {
					PerformaInvoiceHeaderDetailsControllerV2.saveInvoice('enquiryDte','delvDte', proformaInvoice, piLine);
				}catch(Exception e) {
					return;
				}
	        	Test.stopTest();
    		}
	}
	
}