global class RsmDashboardClass { 
    
    private RsmDashboardClass(){}
    WebService  static List<RSMClaimMapping> webserviceForClaim(){
        List<RSMClaimMapping> rsmClaim = new List<RSMClaimMapping>();
        try{
              SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
              String username                                 = saplogin.username__c;
              String password                                 = saplogin.password__c;
              Blob headerValue = Blob.valueOf(username + ':' + password);
              String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
              sapRSMDashboardClaimAuthorized ws = new sapRSMDashboardClaimAuthorized();

              sapRSMDashboardClaimAuthorized.ZWS_ZSFDC_ZONE_RO_COUNTMAP zws = new sapRSMDashboardClaimAuthorized.ZWS_ZSFDC_ZONE_RO_COUNTMAP();
              zws.inputHttpHeaders_x = new Map<String,String>();
              zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
              zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
              zws.timeout_x = 120000;
              sapRSMDashboardClaimAuthorized.TABLE_OF_ZSFDC_CURR_ACTL tom = new sapRSMDashboardClaimAuthorized.TABLE_OF_ZSFDC_CURR_ACTL();

              sapRSMDashboardClaimAuthorized.TABLE_OF_ZSFDC_CURR_ACTL e = new sapRSMDashboardClaimAuthorized.TABLE_OF_ZSFDC_CURR_ACTL();        
              e = zws.ZSFDC_ZONE_RO_COUNTMAP('',tom);
              system.debug(e.item);
              if(e.item != null){
                  for(sapRSMDashboardClaimAuthorized.ZSFDC_CURR_ACTL tm : e.item){
                    system.debug(tm.BZIRK+'Territory');
                    system.debug(tm.TARGET_INSBY+'Target');
                    system.debug(tm.ACTUL_INSBY+'Actual');
                    system.debug(tm.MNTH+'Month');
                    system.debug(tm.YR+'Year');
                    rsmClaim.add(new RSMClaimMapping(tm.Mandt,tm.BZIRK,tm.TARGET_INSBY,tm.ACTUL_INSBY,tm.MNTH,tm.YR));
                  }
              }
              return rsmClaim;
            }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
    }
    //-- WRAPPER CLASS
    
    global class RSMClaimMapping{
        public String Mandt     {get;set;}
        public String BZIRK     {get;set;}
        public String TARGET_INSBY   {get;set;}
        public String ACTUL_INSBY {get;set;}
        public String MNTH  {get;set;}
        public String YR   {get;set;}
         

        public RSMClaimMapping(String Mandt, String BZIRK,String TARGET_INSBY,String ACTUL_INSBY,String MNTH,String YR){
            this.Mandt        = Mandt;
            this.BZIRK        = BZIRK;
            this.TARGET_INSBY = TARGET_INSBY;
            this.ACTUL_INSBY  = ACTUL_INSBY;
            this.MNTH         = MNTH;
            this.YR           = YR;
            
        }
    }
    @future(callout=true)
    public static void createClaimAuthorizedRecord (){
            List<RSMClaimMapping> rsmClaim = new List<RSMClaimMapping>();
            list<Dashboard_Staging__c> InsertListForRsm = new list<Dashboard_Staging__c>();
            id rsmClaimRec = UtilityClass.getRecordTypeId('Dashboard_Staging__c','RSMClaim'); 
            //RsmDashboardClass rsm = new RsmDashboardClass();
            rsmClaim = RsmDashboardClass.webserviceForClaim();  

            
            system.debug(rsmClaim +'rsmClaim');
            if(rsmClaim.size() > 0){
                system.debug(rsmClaim.size() +'rsmClaim');
                for(RSMClaimMapping rsm : rsmClaim){
                    Dashboard_Staging__c ds = new Dashboard_Staging__c(Territory_Code__c = rsm.BZIRK,Target__c = rsm.TARGET_INSBY,Actual__c = rsm.ACTUL_INSBY,Month__c=rsm.MNTH,Year__c = rsm.YR,recordtypeId = rsmClaimRec);
                    InsertListForRsm.add(ds);
                }

            }
            if(InsertListForRsm.size() > 0 ){
                insert InsertListForRsm;
            }
    }
}