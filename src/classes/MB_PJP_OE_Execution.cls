/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By: Fetch Mobile Data for PJP App for OE
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchPJPMobileDataOE/*')

global with sharing class MB_PJP_OE_Execution {

    global class MobileDataOE{
    	public List<String> actionToBeTakenR {get;set;}
    	public List<Visits_Plan__c> visitsPlans {get;set;}
        public List<Account> accounts {get;set;}
        public List<Task> tasks {get;set;}
        public List<String> actionToBeTaken {get;set;}
        public List<User> users {get;set;}
        public String timeStamp {get;set;}
        public String UserType {get;set;}
        public List<DSE_Beat__c> dse_beats {get;set;}
        
    	public MobileDataOE(){
    		actionToBeTakenR = new List<String>();
    		tasks           = new List<Task>();
            accounts        = new List<Account>(); 
            visitsPlans     = new List<Visits_Plan__c>();
            actionToBeTaken = new List<String>(); 
            users           = new List<User>();
            dse_beats       = new List<DSE_Beat__c>(); 
    	}
    }

	@HttpPost
    global static MobileDataOE getMobileDataPJP(){
    	RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String dt = req.params.get('TimeStamp');
        DateTime appDateString = UtilityClass.correctDateTime(dt);
        
        try{
            Map<Integer, String> monthsString = new Map<Integer, String>{1 => 'January', 2 => 'February',3 => 'March',4 => 'April',5 => 'May',6 => 'June',7 => 'July',8 => 'August',9 => 'September',10 => 'October',11 => 'November',12 => 'December'};
                try{               
                    MB_PJP_CIBA_Data.MobileDataRequest mobReq = new MB_PJP_CIBA_Data.MobileDataRequest();
                    
                    mobReq = (MB_PJP_CIBA_Data.MobileDataRequest)JSON.deserialize(req.requestBody.toString(),MB_PJP_CIBA_Data.MobileDataRequest.class);
                    system.debug('RESP' + mobReq);
                    if(mobReq.visitsPlans != null){
                        Integer mn = date.today().month();
                        String yr = String.valueOf(date.today().year());
                        PJP__c pjp = new PJP__c();
                        Boolean noPJPflag = false;
                        try{
                        pjp = [SELECT Id FROM PJP__c WHERE Visit_Plan_Month__c =:monthsString.get(mn) AND Visit_Plan_Year__c =:yr AND OwnerId =:userInfo.getUserId() LIMIT 1];
                            noPJPflag = true;    
                        }catch(QueryException e){
                            noPJPflag = false; 
                        }    
                        List<Visits_Plan__c> visitToCreate = new List<Visits_Plan__c>();
                        List<Visits_Plan__c> visitToUpdate = new List<Visits_Plan__c>();
                        Set<Id> newVisitId = new Set<Id>();
                        
                        for(Visits_Plan__c tsk : mobReq.visitsPlans){
                            String tId = String.valueOf(tsk.Id);
                            if(tId.startsWith('LOCAL_') && noPJPflag){
                                tsk.Id = null;
                                visitToCreate.add(tsk);
                            }else{
                                visitToUpdate.add(tsk);
                            }
                        }
                        
                        
                        Database.update(visitToUpdate);
                        for(Visits_Plan__c v : visitToCreate){
                            v.Master_Plan__c = pjp.Id;
                        }
                        Database.SaveResult[] srv = Database.insert(visitToCreate);
                        for(Database.SaveResult s : srv){
                            newVisitId.add(s.getId());
                        }
                    }
                    
                    if(mobReq.tasks != null){
                        List<Task> taskToCreate = new List<Task>();
                        List<Task> taskToUpdate = new List<Task>();
                        for(Task tsk : mobReq.tasks){
                            String tId = String.valueOf(tsk.Id);
                            if(tId.startsWith('LOCAL_')){
                                tsk.Id = null;
                                taskToCreate.add(tsk);
                            }else{
                                taskToUpdate.add(tsk);
                            }
                        }
                        
                        Database.update(taskToUpdate);            
                        Database.insert(taskToCreate); 
                    }
                }catch(Exception e){
                    system.debug('TOP' + e.getLineNumber() + ' Error -'+ e.getMessage()+ ' Cause' + e.getCause());
                }
            
            List<Visits_Plan__c> vp     = new List<Visits_Plan__c>();
            List<Account> acc           = new List<Account>();
            List<Task> tks              = new List<Task>();
            List<User> urList           = new List<User>();
            List<DSE_Beat__c> dseB      = new List<DSE_Beat__c>();
            
            String userRole     = [ SELECT Id,Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
            
            if(appDateString == null){                
                
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,Offline_Id__c,Dealer__c,Visit_Day_Value__c,Visit_Month_Value__c,Visit_Year_Value__c,Dealer__r.Name,Dealer__r.KUNNR__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c,Dealership_Sub_dealers_Visited__c FROM Visit_Plans__r Order by Visit_Day__c desc LIMIT 10) FROM Account order by Name];    
                    
                    Set<Id> visitIds = new Set<Id>();
                    
                    for(Account a : acc){
                        if(a.Visit_Plans__r != null){
                            vp.addAll(a.Visit_Plans__r);
                            for(Visits_Plan__c visit : a.Visit_Plans__r){
                            	visitIds.add(visit.Id);
                            }                                                        
                        }
                    }
                    
                    for(Visits_Plan__c visit : [SELECT Id,Dealership_Sub_dealers_Visited__c,Offline_Id__c,Dealer__c,Dealer__r.Name,Dealer__r.KUNNR__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c,Visit_Day_Value__c,Visit_Month_Value__c,Visit_Year_Value__c FROM Visits_Plan__c WHERE Visit_Day__c >=:date.today()]){
                    	if(!visitIds.contains(visit.Id)){
                    		vp.add(visit);
                    	}
                    }
                    
                    
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Action_Required__c,Action__c,Subject,Customer_Name__c,Assign_To_Name__c,WhatId,Status,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc) AND OwnerId =:UserInfo.getUserId()];
                    
                    urList = MB_PJP_MobileDataDummy.getAllUsers(UserInfo.getUserId()); 
            }else{
                
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account WHERE LastModifiedDate > :appDateString order by Name];  
                    
                    vp = [SELECT Id,Dealership_Sub_dealers_Visited__c,Offline_Id__c,Visit_Day_Value__c,Visit_Month_Value__c,Visit_Year_Value__c,Dealer__c,Dealer__r.Name,Dealer__r.KUNNR__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE LastModifiedDate > :appDateString ];
                    
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Action_Required__c,Action__c,Subject,Customer_Name__c,Assign_To_Name__c,WhatId,Status,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc) AND OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                    
                	urList = MB_PJP_MobileDataDummy.getAllUsers(UserInfo.getUserId()); 
            }
            
            
            
            
            MobileDataOE mobileData = new MobileDataOE();
            mobileData.accounts     = acc;
            mobileData.visitsPlans  = vp;
            mobileData.tasks = tks;
            
            Schema.DescribeFieldResult fieldResult1 = Task.Action__c.getDescribe();
            List<Schema.PicklistEntry> ple1 = fieldResult1.getPicklistValues();
            
            Schema.DescribeFieldResult fieldResult2 = Task.Action_Required__c.getDescribe();
            List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
            
            String[] pickVals = Label.Action_To_Be_Replacement.split(';');
            
            for(Schema.PicklistEntry f : ple1){
            	mobileData.actionToBeTaken.add(f.getLabel());
            }
            
            for(Schema.PicklistEntry f : ple2){
            	mobileData.actionToBeTakenR.add(f.getLabel());
            } 
            
            mobileData.users = urList;
            mobileData.UserType = userRole;
            mobileData.timeStamp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            system.debug(mobileData);
            return mobileData;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage()+ ' Cause' + e.getCause());
            return null;
        }
        
    	return null;
    }
}