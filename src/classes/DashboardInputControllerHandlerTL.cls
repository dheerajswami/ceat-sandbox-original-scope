/*********************************************************************************
Author        : Sneha Agrawal
Created Date  : 26/5/2015
**********************************************************************************/

global class  DashboardInputControllerHandlerTL{
    
    //Wrapper Classes    
    public class dashboardWrapper{    
    	public set<string> moduleSet     {get;set;}    
        public set<string> catSet     {get;set;}
        public list<Dashboard_Input_Score__c> dbInputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
    }
    
    //CONSTRUCTOR
    public DashboardInputControllerHandlerTL(){
    }
    
    public  dashboardWrapper getDashboardTlRecords(String loggedInUserTerritory){
         map<Integer,String> monthYearMap = new map<Integer,String>();
         /*
                To convert current month into month name
        */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');
            
        Integer month                   = Date.today().month();
        String currentMonth             = monthYearMap.get(month);
        String currentYear              = String.valueOf((Date.today()).year());
        String currentDay               = String.valueOf((Date.today()).day());
        
        string TLRecType                = system.Label.TL_RecType;    
        String pjpRecType               = system.Label.Input_RecordType;
        String actionLogRecType         = system.Label.ActionLogInputRecordType;
        String efleetRecType            = system.Label.Efleet_RecType; 
        string TLInputRecType           = system.Label.Input_RecType;
        
        Set<String> SetOFCat        = new Set<String>();
        Set<String> SetOFCatEfleet  = new Set<String>();
        
        list<Dashboard_Weightage_Master__c> dashboardWeightages       = new List<Dashboard_Weightage_Master__c>(); 
        list<Dashboard_Input_Score__c> dashboardInputScore               = new List<Dashboard_Input_Score__c>(); 
        list<Dashboard_Master__c> dashboardMaster                        = new list<Dashboard_Master__c>();
       
        Dashboard_Summary_Score__c dashSummaryScore               = new Dashboard_Summary_Score__c();
        map<String,Double> MapOFCatAndWeigtage                    = new map<String,Double>();
        
        dashboardMaster         = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true];
        
        Id TLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:TLInputRecType Limit 1].Id;
        Id TLRecTypeId        = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:TLRecType Limit 1].Id;
            
        Id dwmActionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
        Id dwmEfleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:efleetRecType Limit 1].Id;
        Id dwmPjpRecTypeId        = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:pjpRecType Limit 1].Id;
           
        dashboardWeightages       = [SELECT id,Category__c,Parameters_Inout__c,Role__c,Testing__c,Weightage__c FROM Dashboard_Weightage_Master__c where (RecordTypeId=:dwmActionLogRecTypeId OR RecordTypeId=:dwmEfleetRecTypeId OR RecordTypeId=:dwmPjpRecTypeId) AND Dashboard_Master__c =: dashboardMaster[0].id];
        string externalId 		  = loggedInUserTerritory +month +currentYear ;
        
        if(loggedInUserTerritory!=null){
            dashboardInputScore = [SELECT ID,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: TLRecTypeId AND Territory_Code__c=:loggedInUserTerritory  AND  Month__c =: currentMonth AND Year__c =: currentYear AND (NOT(Parameters__c ='SDS increase / decrease')) AND (NOT(Parameters__c ='BTL (Counter branding)'))];//where DBExternalId__c='2010B008242015'
            dashSummaryScore    = [SELECT ID,DSS_External_ID__c,Input_Score__c,Territory_Code__c,Month__c,Year__c  FROM Dashboard_Summary_Score__c WHERE DSS_External_ID__c =: externalId limit 1];
        }else{
             dashboardInputScore = [SELECT ID,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: TLRecTypeId AND (NOT(Parameters__c ='SDS increase / decrease')) AND (NOT(Parameters__c ='BTL (Counter branding)'))];
        } 
        dashboardWrapper dashboardRecord         = new dashboardWrapper();
        dashboardRecord.catSet                   = new set<string>();
        dashboardRecord.moduleSet                   = new set<string>();
        
        for(Dashboard_Input_Score__c ss : dashboardInputScore){
                dashboardRecord.catSet.add(ss.RecordTypeName__c+'@@'+ss.Category__c);
                dashboardRecord.moduleSet.add(ss.RecordTypeName__c);
        } 
             
             dashboardRecord.dbInputScore               = dashboardInputScore;
             dashboardRecord.dashboardSummaryScore      = dashSummaryScore;
           
        return dashboardRecord; 
    }
}