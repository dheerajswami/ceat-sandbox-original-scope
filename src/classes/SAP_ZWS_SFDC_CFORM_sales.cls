//Generated by wsdl2apex

public class SAP_ZWS_SFDC_CFORM_sales {
    public class TABLE_OF_ZSFDC_SALES {
        public SAP_ZWS_SFDC_CFORM_sales.ZSFDC_SALES[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZSFDC_CFORM_SALES_element {
        public String FROMDATE;
        public SAP_ZWS_SFDC_CFORM_sales.TABLE_OF_ZSFDC_SALES IT_FINAL;
        public String KUNAG;
        public String TODATE;
        private String[] FROMDATE_type_info = new String[]{'FROMDATE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IT_FINAL_type_info = new String[]{'IT_FINAL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] KUNAG_type_info = new String[]{'KUNAG','urn:sap-com:document:sap:rfc:functions',null,'0','1','false'};
        private String[] TODATE_type_info = new String[]{'TODATE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'FROMDATE','IT_FINAL','KUNAG','TODATE'};
    }
    public class ZSFDC_CFORM_SALESResponse_element {
        public SAP_ZWS_SFDC_CFORM_sales.TABLE_OF_ZSFDC_SALES IT_FINAL;
        public String RETURN_x;
        private String[] IT_FINAL_type_info = new String[]{'IT_FINAL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_FINAL','RETURN_x'};
    }
    public class ZSFDC_SALES {
        public String CUSTOMER;
        public String CUSTOMER_NAME;
        public String INVOICE_DATE;
        public String MONTH;
        public String INVOICE;
        public String MATERIAL;
        public String MATERIAL_DESCRIPTION;
        public String CUST_PART_NUMBER;
        public String PO_NO;
        public String INVOICE_QTY;
        public String PRE_TAX;
        public String POST_TAX;
        public String TAX_VALUE;
        public String SALES_OFFICE;
        public String SELLER_STATE;
        public String SELLER_TIN_NO;
        private String[] CUSTOMER_type_info = new String[]{'CUSTOMER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CUSTOMER_NAME_type_info = new String[]{'CUSTOMER_NAME','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INVOICE_DATE_type_info = new String[]{'INVOICE_DATE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MONTH_type_info = new String[]{'MONTH','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INVOICE_type_info = new String[]{'INVOICE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATERIAL_type_info = new String[]{'MATERIAL','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MATERIAL_DESCRIPTION_type_info = new String[]{'MATERIAL_DESCRIPTION','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CUST_PART_NUMBER_type_info = new String[]{'CUST_PART_NUMBER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PO_NO_type_info = new String[]{'PO_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] INVOICE_QTY_type_info = new String[]{'INVOICE_QTY','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PRE_TAX_type_info = new String[]{'PRE_TAX','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] POST_TAX_type_info = new String[]{'POST_TAX','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TAX_VALUE_type_info = new String[]{'TAX_VALUE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SALES_OFFICE_type_info = new String[]{'SALES_OFFICE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SELLER_STATE_type_info = new String[]{'SELLER_STATE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] SELLER_TIN_NO_type_info = new String[]{'SELLER_TIN_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'CUSTOMER','CUSTOMER_NAME','INVOICE_DATE','MONTH','INVOICE','MATERIAL','MATERIAL_DESCRIPTION','CUST_PART_NUMBER','PO_NO','INVOICE_QTY','PRE_TAX','POST_TAX','TAX_VALUE','SALES_OFFICE','SELLER_STATE','SELLER_TIN_NO'};
    }
    public class zws_sfdc_cform_sales {
        public String endpoint_x = 'http://CTECQHAP.ceat.in:8000/sap/bc/srt/rfc/sap/zws_sfdc_cform_sales/300/zws_sfdc_cform_sales/zws_sfdc_cform_sales';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'SAP_ZWS_SFDC_CFORM_sales'};
        public SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALESResponse_element ZSFDC_CFORM_SALES(String FROMDATE,SAP_ZWS_SFDC_CFORM_sales.TABLE_OF_ZSFDC_SALES IT_FINAL,String KUNAG,String TODATE) {
            SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALES_element request_x = new SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALES_element();
            request_x.FROMDATE = FROMDATE;
            request_x.IT_FINAL = IT_FINAL;
            request_x.KUNAG = KUNAG;
            request_x.TODATE = TODATE;
            SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALESResponse_element response_x;
            Map<String, SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALESResponse_element> response_map_x = new Map<String, SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALESResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:zws_SFDC_CFORM_SALES:ZSFDC_CFORM_SALESRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_CFORM_SALES',
              'urn:sap-com:document:sap:rfc:functions',
              'ZSFDC_CFORM_SALESResponse',
              'SAP_ZWS_SFDC_CFORM_sales.ZSFDC_CFORM_SALESResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}