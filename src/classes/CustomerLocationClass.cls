global class CustomerLocationClass {
    public static String salesforceUrl {get;}
    
    public CustomerLocationClass() {
        salesforceUrl = URL.getSalesforceBaseUrl().toExternalForm();
    }
    /*
@RemoteAction
global static List<String> fetchZones(){
List<String> zones = new List<String>();
Territory2Type tType = [Select Id from Territory2Type where DeveloperName = 'Zone'];
for(Territory2 terr : [SELECT Id, Name FROM Territory2 where Territory2TypeId = :tType.Id]) {
zones.add(terr.Name);
}
zones.add(0, 'All');
return zones;
}

@RemoteAction
global static List<String> fetchRegions(String zName){
List<String> regions = new List<String>();
Territory2Type tType = [Select Id from Territory2Type where DeveloperName = 'RO'];
Territory2 zone = [SELECT Id, Name FROM Territory2 where Name = :zName Limit 1];
for(Territory2 terr : [SELECT Id, Name FROM Territory2 where Territory2TypeId = :tType.Id And ParentTerritory2Id = :zone.Id]) {
regions.add(terr.Name);
}
regions.add(0, 'All');
return regions;
}

@RemoteAction
global static List<String> fetchTerritories(String rName){
List<String> territories = new List<String>();
Territory2Type tType = [Select Id from Territory2Type where DeveloperName = 'Territory'];
Territory2 region = [SELECT Id, Name FROM Territory2 where Name = :rName Limit 1];
for(Territory2 terr : [SELECT Id, Name FROM Territory2 where Territory2TypeId = :tType.Id And ParentTerritory2Id = :region.Id]) {
territories.add(terr.Name);
}
territories.add(0, 'All');
return territories;
}
*/
    @RemoteAction
    global static List<String> fetchStates(){
        Set<String> states = new Set<String>();
        List<String> listStates = new List<String>();
        for(AggregateResult sm : [SELECT State__c FROM State_Master__c Group By State__c Order By State__c]) {
            //states.add(sm.State__c);
            states.add((String)sm.get('State__c'));
        }
        listStates.addAll(states);
        listStates.sort();
        listStates.add(0, 'All');
        return listStates;
    }
    
    @RemoteAction
    global static List<String> fetchDistricts(String state){
        Set<String> districts = new Set<String>();
        List<String> listDistricts = new List<String>();
        for(AggregateResult sm : [SELECT District__c FROM State_Master__c where State__c = :state Group By District__c Order By District__c]) {            
            districts.add((String)sm.get('District__c'));
        }
        listDistricts.addAll(districts);
        listDistricts.sort();
        listDistricts.add(0, 'All');
        return listDistricts;
    }
    
    @RemoteAction
    global static List<String> fetchTowns(String district){
        Set<String> towns = new Set<String>();
        List<String> listTowns = new List<String>();
        for(State_Master__c sm : [SELECT Id, Town__c FROM State_Master__c where District__c = :district Order By Town__c]) {
            towns.add(sm.Town__c);
        }
        listTowns.addAll(towns);
        listTowns.sort();
        listTowns.add(0, 'All');
        return listTowns;
    }
    
    @RemoteAction
    global static List<String> fetchCustGroups(){
        Set<String> custGroups = new Set<String>();
        List<String> listCustGroups = new List<String>();
        for(Account acc : [SELECT Id, Customer_Group__c FROM Account]) {
            custGroups.add(acc.Customer_Group__c);
        }
        listCustGroups.addAll(custGroups);
        listCustGroups.sort();
        listCustGroups.add(0, 'All');
        return listCustGroups;
    }   
    
    @RemoteAction
    global static List<String> fetchCustGroup1s(){
        Set<String> custGroup1s = new Set<String>();
        List<String> listCustGroup1s = new List<String>();
        for(Account acc : [SELECT Id, Customer_Group_Text1__c FROM Account]) {
            custGroup1s.add(acc.Customer_Group_Text1__c);
        }
        for(AggregateResult acc : [SELECT Customer_Group_Text1__c FROM Lead where Customer_Group_Text1__c != Null Group by Customer_Group_Text1__c]) {
            custGroup1s.add((String)acc.get('Customer_Group_Text1__c'));
        }
        listCustGroup1s.addAll(custGroup1s);
        listCustGroup1s.sort();
        listCustGroup1s.add(0, 'All');
        return listCustGroup1s;
    }   
    
    @RemoteAction
    global static List<WrapperZones> fetchWrapperZones(){
        List<WrapperZones> wZones = new List<WrapperZones>();
        
        Territory2Type zoneType = [Select Id from Territory2Type where DeveloperName = 'Zone'];
        Territory2Type regionType = [Select Id from Territory2Type where DeveloperName = 'RO'];
        Territory2Type terrType = [Select Id from Territory2Type where DeveloperName = 'Territory'];
        
        Map<Id, Territory2> zones = new Map<Id, Territory2>([SELECT Id, Name FROM Territory2 where Territory2TypeId = :zoneType.Id]);
        Map<Id, Territory2> regions = new Map<Id, Territory2>([SELECT Id, Name, ParentTerritory2Id FROM Territory2 where Territory2TypeId = :regionType.Id]);
        
        WrapperZones wZone;
        wZone = new WrapperZones();
        wZone.territory = 'All';
        wZone.region = 'All';
        wZone.zone = 'All';
        wZones.add(wZone);
        for(Territory2 terr : [SELECT Id, Name, ParentTerritory2Id FROM Territory2 where Territory2TypeId = :terrType.Id]) {            
            wZone = new WrapperZones();
            wZone.territory = terr.Name;
            wZone.region = regions.get(terr.ParentTerritory2Id).Name;
            wZone.zone = zones.get(regions.get(terr.ParentTerritory2Id).ParentTerritory2Id).Name;
            wZones.add(wZone);
        }
        
        return wZones;
    }
    
    @RemoteAction
    global static List<State_Master__c> fetchStateMaster(){
        List<State_Master__c> sMaster = new List<State_Master__c>();        
        sMaster = [SELECT Id, State__c, District__c, Town__c FROM State_Master__c Order By State__c, District__c, Town__c];                
        sMaster.add(0, new State_Master__c(State__c='All', District__c='All', Town__c='All'));
        return sMaster;
    }
    
    @RemoteAction
    global static Map<String, System.Location> fetchTownGeocodes(String state, String district, String town) {
        String str1 = '';
        if(state != 'All') {            
            str1 += ' where State__c = :state';            
            if(district != 'All') {
                str1 += ' AND District__c = :district';                
                if(town != 'All') {
                    str1 += ' AND Town__c = :town';
                }
            }
        }
        
        str1 = 'Select Town__c, Town_Geolocation__c from State_Master__c' + str1;
        
        Map<String, System.Location> townGeoMap = new Map<String, System.Location>();
        
        for(State_Master__c sm : Database.query(str1)) {
            townGeoMap.put(sm.Town__c, sm.Town_Geolocation__c);
        }
        
        return townGeoMap;
    }
    
    @RemoteAction
    global static List<WrapperAllCustomers> fetchCustomers(String zone, String region, String terr, String state, String district, String town, String cGroup, String cGroup1) {
        String str1 = '';
        String str2 = '';
        if(zone != 'All') {
            if(str1 == '') {
                str1 = ' where';
            } else {
                str1 += ' AND';
            }            
            str1 += ' Sales_Office_Text__c = :zone';            
            if(region != 'All') {
                str1 += ' AND Sales_Group_Text__c = :region';                
                if(terr != 'All') {
                    str1 += ' AND Sales_District_Text__c = :terr';
                }
            }
        }
        
        if(state != 'All') {
            if(str1 == '') {
                str1 = ' where';
            } else {
                str1 += ' AND';
            }
            str1 += ' State__c = :state';            
            if(district != 'All') {
                str1 += ' AND District__c = :district';                
                if(town != 'All') {
                    str1 += ' AND Town__c = :town';
                }
            }
        }
        
        if(cGroup != 'All') {
            if(!cGroup.contains('All')) {
            cGroup = cGroup.replace('[', '');
            cGroup = cGroup.replace(']', '');
            List<String> cGroups = cGroup.split(', ');
            if(str1 == '') {
                str1 = ' where Customer_Group__c in :cGroups';
            } else {
                str1 += ' AND Customer_Group__c in :cGroups';
            } 
            }
        }
        
        if(cGroup1 != 'All') {
            if(!cGroup1.contains('All')) {                                
            cGroup1 = cGroup1.replace('[', '');
            cGroup1 = cGroup1.replace(']', '');
            List<String> cGroup1s = cGroup1.split(', ');
            if(str1 == '') {
                str1 = ' where Customer_Group_Text1__c in :cGroup1s';
            } else {
                str1 += ' AND Customer_Group_Text1__c in :cGroup1s';
            } 
            }
        }
        str2 = 'Select Id, Name, Customer_Code__c, Geolocation__c, Geolocation__latitude__s, Geolocation__longitude__s, Town__c, District__c, State__c, Customer_Group__c, Customer_Group_Text1__c from Lead' + str1;
        str1 = 'Select Id, Name, UniqueIdentifier__c, Geolocation__c, Geolocation__latitude__s, Geolocation__longitude__s, Town__c, District__c, State__c, Customer_Group__c, Customer_Group_Text1__c from Account' + str1;
        System.debug('Swayam str1   '+str1);
        List<Account> gotAccounts = Database.query(str1);                
        System.debug('Swayam str2   '+str2);
        List<Lead> gotLeads = Database.query(str2);
        
        List<WrapperAllCustomers> allCustomers = new List<WrapperAllCustomers>();
        Map<String, System.Location> townGeoCodes = fetchTownGeocodes(state, district, town);
        
        WrapperAllCustomers cust;
        for(Account acc : gotAccounts) { 
            if(acc.Geolocation__Latitude__s == null) {
                if(townGeoCodes.get(acc.Town__c) != null) {
                    acc.Geolocation__Latitude__s = townGeoCodes.get(acc.Town__c).getLatitude();
                    acc.Geolocation__longitude__s = townGeoCodes.get(acc.Town__c).getLongitude();
                    System.Location loc = System.Location.newInstance(townGeoCodes.get(acc.Town__c).getLatitude()+0.0005, townGeoCodes.get(acc.Town__c).getLongitude());
                    townGeoCodes.put(acc.Town__c, loc);                    
                }                
            }
            cust = new WrapperAllCustomers(acc.Name, acc.UniqueIdentifier__c, 'Account', acc.Customer_Group__c, acc.Customer_Group_Text1__c, acc.Town__c, acc.District__c, acc.State__c, acc.Geolocation__Latitude__s, acc.Geolocation__Longitude__s);
            allCustomers.add(cust);
        }
        
        for(Lead acc : gotLeads) { 
            if(acc.Geolocation__Latitude__s == null) {
                if(townGeoCodes.get(acc.Town__c) != null) {
                    acc.Geolocation__Latitude__s = townGeoCodes.get(acc.Town__c).getLatitude();
                    acc.Geolocation__longitude__s = townGeoCodes.get(acc.Town__c).getLongitude(); 
                    System.Location loc = System.Location.newInstance(townGeoCodes.get(acc.Town__c).getLatitude()+0.0005, townGeoCodes.get(acc.Town__c).getLongitude());
                    townGeoCodes.put(acc.Town__c, loc);                    
                }                
            }
            cust = new WrapperAllCustomers(acc.Name, acc.Customer_Code__c, 'Lead', acc.Customer_Group__c, acc.Customer_Group_Text1__c, acc.Town__c, acc.District__c, acc.State__c, acc.Geolocation__Latitude__s, acc.Geolocation__Longitude__s);
            allCustomers.add(cust);
        }
        
        return allCustomers;
    }               
    
    global class WrapperStates {
        public String state {get;set;}
        public String district {get;set;}
        public String town {get;set;}        
    }
    
    global class WrapperZones {
        public String zone {get;set;}        
        public String region {get;set;}
        public String territory {get;set;}
    }
    
    global class WrapperAllCustomers {
        public String cName {get;set;}        
        public String cCode {get;set;}
        public String cObject {get;set;}
        public String cGroup {get;set;}
        public String cGroup1 {get;set;}
        public String cTown {get;set;}
        public String cDistrict {get;set;}
        public String cState {get;set;}
        public Decimal cLat {get;set;}
        public Decimal cLng {get;set;}
        
        public WrapperAllCustomers(String name, String code, String obj, String grp, String grp1, String town, String district, String state, Decimal lat, Decimal lng) {
            cName = name;
            cCode = code;
            cObject = obj;
            cGroup = grp;
            cGroup1 = grp1;
            cTown = town;
            cDistrict = district;
            cState = state;
            cLat = lat;
            cLng = lng;
        }
    }
}