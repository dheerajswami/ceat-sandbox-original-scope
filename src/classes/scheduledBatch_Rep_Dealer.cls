global class scheduledBatch_Rep_Dealer implements Schedulable{
   global void execute(SchedulableContext sc) {
      SP_BatchForReplacementBUUpdated b = new SP_BatchForReplacementBUUpdated(); 
      database.executebatch(b,200);
   }
}