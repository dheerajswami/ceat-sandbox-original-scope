@isTest
private class Stocks_BatchForDelete_TestClass {
    static testmethod void test() {
        // The query used by the batch job.
        
        String query='select id from Stocks__c where  CreatedDate <Today ';
      
      
      // Create some test items to be deleted
        
        //   by the batch job.
        
        Stocks__c[] stlist = new List<Stocks__c>();
         for (Integer i=0;i<10;i++) {
           
            Stocks__c st = new  Stocks__c(
            Category__c='2010'
            
            );
            stlist.add(st);
         }
         insert stlist;
         
         Test.startTest();
          ScheduledBatch_StockDelete temp = new ScheduledBatch_StockDelete ();
                String sch = '0 45 23 * * ?'; 
                system.schedule('Stock Delete', sch, temp);
         Stocks_BatchForDelete b = new Stocks_BatchForDelete();
         Database.executeBatch(b);
         Test.stopTest();
        
         Integer i = [SELECT COUNT() FROM Stocks__c];
         System.assertEquals(i, 0);
    }
    
    }