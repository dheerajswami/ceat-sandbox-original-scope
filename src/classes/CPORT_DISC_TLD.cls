global class CPORT_DISC_TLD {

/*
* Auther      :- Vivek Deepak
* Purpose     :- Discount and Schemes Part 2 
* Modified By :- 
* Date        :- 4/6/2015
*
*/
    
    // ATTRIBUTES --
    	public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    // CONTRUCTOR --
    
    // METHODS   --
    /*
	TLD DISC
    */
    WebService static List<Sap_TLD_Disc> getSlabTld(String circleNo,String kunnag){
      
      List<Sap_TLD_Disc> dis_slab_map = new List<Sap_TLD_Disc>();
      
      try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH)); 

        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        
        sap_tld_disc_Prod.ZWS_SFDC_TLD_DISC zws                   	= new sap_tld_disc_Prod.ZWS_SFDC_TLD_DISC();
        sap_tld_disc_Prod.ZsfdcTldDiscResponse_element   e           = new sap_tld_disc_Prod.ZsfdcTldDiscResponse_element();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        sap_tld_disc_Prod.TableOfZsd62Data  tom = new sap_tld_disc_Prod.TableOfZsd62Data();   
        Continuation cont = new Continuation(60);
        e = zws.ZsfdcTldDisc(circleNo,tom,customerId);
        
        system.debug('OUTPUT' + e);
        
        if(e != null){
            if(e.ItOutput != null){
                sap_tld_disc_Prod.TableOfZsd62Data temp = new sap_tld_disc_Prod.TableOfZsd62Data();
                temp = e.ItOutput;
                for(sap_tld_disc_Prod.Zsd62Data loop_var : temp.item){
                    dis_slab_map.add(new Sap_TLD_Disc(loop_var.Mandt,loop_var.Cirno,loop_var.Kunag,loop_var.Name1,loop_var.Superq,loop_var.Pre,loop_var.Post,loop_var.Kwert,loop_var.Disc,loop_var.Dwerk,loop_var.Fkimg,loop_var.Fkimg1,loop_var.Discount,loop_var.Type2,loop_var.Value,loop_var.Target,loop_var.Avgs,loop_var.Type_x,loop_var.Vkbur,loop_var.Vkgrp,loop_var.Office,loop_var.Groups,loop_var.Boi));
                }
            }
        } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        
        return dis_slab_map;
    }

    global class Sap_TLD_Disc{
    	public String Mandt {get;set;}
	    public String Cirno {get;set;}
	    public String Kunag {get;set;}
	    public String Name1 {get;set;}
	    public String Superq {get;set;}
	    public String Pre {get;set;}
	    public String Post {get;set;}
	    public String Kwert {get;set;}
	    public String Disc {get;set;}
	    public String Dwerk {get;set;}
	    public String Fkimg {get;set;}
	    public String Fkimg1 {get;set;}
	    public String Discount {get;set;}
	    public String Type2 {get;set;}
	    public String Value {get;set;}
	    public String Target {get;set;}
	    public String Avgs {get;set;}
	    public String Type_x {get;set;}
	    public String Vkbur {get;set;}
	    public String Vkgrp {get;set;}
	    public String Office {get;set;}
	    public String Groups {get;set;}
	    public String Boi {get;set;}
		public Sap_TLD_Disc(String mandt, String cirno, String kunag, String name1,
				String superq, String pre, String post, String kwert, String disc,
				String dwerk, String fkimg, String fkimg1, String discount,
				String type2, String value, String target, String avgs,
				String type_x, String vkbur, String vkgrp, String office,
				String groups, String boi) {
			this.Mandt = mandt;
			this.Cirno = cirno;
			this.Kunag = kunag;
			this.Name1 = name1;
			this.Superq = superq;
			this.Pre = pre;
			this.Post = post;
			this.Kwert = kwert;
			this.Disc = disc;
			this.Dwerk = dwerk;
			this.Fkimg = fkimg;
			this.Fkimg1 = fkimg1;
			this.Discount = discount;
			this.Type2 = type2;
			this.Value = value;
			this.Target = target;
			this.Avgs = avgs;
			this.Type_x = type_x;
			this.Vkbur = vkbur;
			this.Vkgrp = vkgrp;
			this.Office = office;
			this.Groups = groups;
			this.Boi = boi;
		}
    }

    /*
		Discount Master
    */
	WebService static List<SAP_Discount_Master> getDiscountMaster(String circleNo,String kunnag){
     
     List<SAP_Discount_Master> dis_slab_map = new List<SAP_Discount_Master>();
     
     try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);

		sap_disc_master_prod.zws_sfdc_disc_master zws             = new sap_disc_master_prod.zws_sfdc_disc_master();
        sap_disc_master_prod.ZsfdcDiscMasterResponse_element   e  = new sap_disc_master_prod.ZsfdcDiscMasterResponse_element();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        sap_disc_master_prod.TableOfZsdDiscMaster  tom = new sap_disc_master_prod.TableOfZsdDiscMaster();   
        
        e = zws.ZsfdcDiscMaster(tom);
        
        if(e != null){
            if(e.ItOutput != null){
                sap_disc_master_prod.TableOfZsdDiscMaster temp = new sap_disc_master_prod.TableOfZsdDiscMaster();
                temp = e.ItOutput;
                for(sap_disc_master_prod.ZsdDiscMaster loop_var : temp.item){
                	//system.debug(loop_var);
                    dis_slab_map.add(new SAP_Discount_Master(loop_var.Srnum,loop_var.Zscheme,loop_var.Sapdisc,loop_var.Subcod,loop_var.Ddtext,loop_var.Begda1,loop_var.Endda));
                }
            }
        } 
        
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        return dis_slab_map;
    }
    

	global class SAP_Discount_Master{
		public String Srnum {get;set;}
        public String Zscheme {get;set;}
        public String Sapdisc {get;set;}
        public String Subcod {get;set;}
        public String Ddtext {get;set;}
        public String Begda1 {get;set;}
        public String Endda {get;set;}

        public SAP_Discount_Master(String Srnum,String Zscheme,String Sapdisc,String Subcod,String Ddtext,String Begda1,String Endda){
        	this.Srnum 	= Srnum;
        	this.Zscheme = Zscheme;
        	this.Sapdisc = Sapdisc;
        	this.Subcod  = Subcod;
        	this.Ddtext  = Ddtext;
        	this.Begda1  = Begda1;
        	this.Endda   = Endda;
        }


	}

	/*
	PPD Disc
	*/
	WebService static SAP_PPD_Discount getPPDDiscountMaster(String circleNo,String kunnag){
      
      SAP_PPD_Discount dis_slab_map = new SAP_PPD_Discount();
      
      try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);

		sap_ppd_disc_prod.zws_ppd_disc zws             	 = new sap_ppd_disc_prod.zws_ppd_disc();
        sap_ppd_disc_prod.ZsfdcPpdDiscResponse_element  e  = new sap_ppd_disc_prod.ZsfdcPpdDiscResponse_element();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        sap_ppd_disc_prod.TableOfZsdDiscountRfc tom 	= new sap_ppd_disc_prod.TableOfZsdDiscountRfc();   
        sap_ppd_disc_prod.TableOfZsdDisbRfc temp 		= new sap_ppd_disc_prod.TableOfZsdDisbRfc();   
        
        e = zws.ZsfdcPpdDisc(circleNo,tom,temp,customerId);
        
        system.debug('OUTPUT' + e);
        
        if(e != null){
            if(e.ItOutput != null){
                sap_ppd_disc_prod.TableOfZsdDiscountRfc tom1 	= new sap_ppd_disc_prod.TableOfZsdDiscountRfc();   
        		sap_ppd_disc_prod.TableOfZsdDisbRfc temp1 	= new sap_ppd_disc_prod.TableOfZsdDisbRfc(); 
                tom1  = e.ItOutput;
                temp1 = e.ItOutput1;

                List<SAP_PPD_DiscountOne> disOne = new List<SAP_PPD_DiscountOne>();
                List<SAP_PPD_DiscountTwo> disTwo = new List<SAP_PPD_DiscountTwo>();

                for(sap_ppd_disc_prod.ZsdDiscountRfc loop_var : tom1.item){
                    disTwo.add(new SAP_PPD_DiscountTwo(loop_var.Cirno,loop_var.Kunag,loop_var.Name1,loop_var.Vbelv,loop_var.Pretax,loop_var.Posttax,loop_var.Discount));
                }
                for(sap_ppd_disc_prod.ZsdDisbRfc loop_var : temp1.item){
                    disOne.add(new SAP_PPD_DiscountOne(loop_var.Cirno,loop_var.Kunag,loop_var.InvoiceNumber,loop_var.Posnr,loop_var.DateOfBilling,loop_var.InvoiceValue,loop_var.DiscRate,loop_var.Discount,loop_var.Payment,loop_var.PaymentDate,loop_var.Duedat,loop_var.PostingDate));
                }

                dis_slab_map.firstSet = disOne;
                dis_slab_map.secondSet = disTwo;

            }
        }

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        
        return dis_slab_map;
    }

    global class SAP_PPD_Discount{
    	public List<SAP_PPD_DiscountOne> firstSet 	{get;set;}
    	public List<SAP_PPD_DiscountTwo> secondSet 	{get;set;}

    	public SAP_PPD_Discount(){
    		firstSet 	= new List<SAP_PPD_DiscountOne>();
    		secondSet 	= new List<SAP_PPD_DiscountTwo>();
    	}
    }


	global class SAP_PPD_DiscountOne{
		public String Cirno {get;set;}
	    public String Kunag {get;set;}
	    public String InvoiceNumber {get;set;}
	    public String Posnr {get;set;}
	    public String DateOfBilling {get;set;}
	    public String InvoiceValue {get;set;}
	    public String DiscRate {get;set;}
	    public String Discount {get;set;}
	    public String Payment {get;set;}
	    public String PaymentDate {get;set;}
	    public String Duedat {get;set;}
	    public String PostingDate {get;set;}
		public SAP_PPD_DiscountOne(String cirno, String kunag, String invoiceNumber,
				String posnr, String dateOfBilling, String invoiceValue,
				String discRate, String discount, String payment,
				String paymentDate, String duedat, String postingDate) {
			this.Cirno = cirno;
			this.Kunag = kunag;
			this.InvoiceNumber = invoiceNumber;
			this.Posnr = posnr;
			this.DateOfBilling = dateOfBilling;
			this.InvoiceValue = invoiceValue;
			this.DiscRate = discRate;
			this.Discount = discount;
			this.Payment = payment;
			this.PaymentDate = paymentDate;
			this.Duedat = duedat;
			this.PostingDate = postingDate;
		}
	}

	global class SAP_PPD_DiscountTwo{
		public String Cirno {get;set;}
        public String Kunag {get;set;}
        public String Name1 {get;set;}
        public String Vbelv {get;set;}
        public String Pretax {get;set;}
        public String Posttax {get;set;}
        public String Discount {get;set;}

        public SAP_PPD_DiscountTwo(String cirno, String kunag, String name1, String vbelv,
			String pretax, String posttax, String discount) {
		
			this.Cirno = cirno;
			this.Kunag = kunag;
			this.Name1 = name1;
			this.Vbelv = vbelv;
			this.Pretax = pretax;
			this.Posttax = posttax;
			this.Discount = discount;
		}
	}

	/*
	SAP AD DISCOUNT
	*/
	WebService static List<Sap_ad_Discount> getSapAdDiscounts(String circleNo,String kunnag){
     
     List<Sap_ad_Discount> dis_slab_map = new List<Sap_ad_Discount>();
     
     try{

        String customerId                   = UtilityClass.addleadingZeros(kunnag,Integer.valueOf(CUST_ID_LENGTH));   
        SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
        String username                     = saplogin.username__c;
        String password                     = saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);

		sap_ad_disc_prod.ZWS_AD_DISC1 zws                  = new sap_ad_disc_prod.ZWS_AD_DISC1();
        sap_ad_disc_prod.ZsfdcAdDisc1Response_element   e  = new sap_ad_disc_prod.ZsfdcAdDisc1Response_element();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        sap_ad_disc_prod.TableOfZsdfcAdDiscount1   tom = new sap_ad_disc_prod.TableOfZsdfcAdDiscount1();   
        
        e = zws.ZsfdcAdDisc1(circleNo,tom,customerId);
        system.debug('OUTPUT' + e);
        if(e != null){
            if(e.ItOutput != null){
                sap_ad_disc_prod.TableOfZsdfcAdDiscount1 temp = new sap_ad_disc_prod.TableOfZsdfcAdDiscount1();
                temp = e.ItOutput;
                for(sap_ad_disc_prod.ZsdfcAdDiscount1 loop_var : temp.item){
                    dis_slab_map.add(new Sap_ad_Discount(loop_var.Crnum,loop_var.Kunnr,loop_var.Mwskz,loop_var.RunDate,loop_var.Name1,loop_var.Office,loop_var.Zgroup,loop_var.Dwerk,loop_var.Apdat,loop_var.Sdsas,loop_var.Zavg,loop_var.Sdsgr,loop_var.Sdsat,loop_var.Pre,loop_var.Post,loop_var.Kwert,loop_var.Rot,loop_var.Targt,loop_var.Reqto,loop_var.Addin,loop_var.Sds,loop_var.Tov,loop_var.Crval,loop_var.Disc,loop_var.Indic));
                }
            }
        } 

        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        
        
        return dis_slab_map;
    }

	global class Sap_ad_Discount{
		public String Crnum {get;set;}
	    public String Kunnr {get;set;}
	    public String Mwskz {get;set;}
	    public String RunDate {get;set;}
	    public String Name1 {get;set;}
	    public String Office {get;set;}
	    public String Zgroup {get;set;}
	    public String Dwerk {get;set;}
	    public String Apdat {get;set;}
	    public String Sdsas {get;set;}
	    public String Zavg {get;set;}
	    public String Sdsgr {get;set;}
	    public String Sdsat {get;set;}
	    public String Pre {get;set;}
	    public String Post {get;set;}
	    public String Kwert {get;set;}
	    public String Rot {get;set;}
	    public String Targt {get;set;}
	    public String Reqto {get;set;}
	    public String Addin {get;set;}
	    public String Sds {get;set;}
	    public String Tov {get;set;}
	    public String Crval {get;set;}
	    public String Disc {get;set;}
	    public String Indic {get;set;}
		public Sap_ad_Discount(String crnum, String kunnr, String mwskz,
				String runDate, String name1, String office, String zgroup,
				String dwerk, String apdat, String sdsas, String zavg,
				String sdsgr, String sdsat, String pre, String post, String kwert,
				String rot, String targt, String reqto, String addin, String sds,
				String tov, String crval, String disc, String indic) {
			this.Crnum = crnum;
			this.Kunnr = kunnr;
			this.Mwskz = mwskz;
			this.RunDate = runDate;
			this.Name1 = name1;
			this.Office = office;
			this.Zgroup = zgroup;
			this.Dwerk = dwerk;
			this.Apdat = apdat;
			this.Sdsas = sdsas;
			this.Zavg = zavg;
			this.Sdsgr = sdsgr;
			this.Sdsat = sdsat;
			this.Pre = pre;
			this.Post = post;
			this.Kwert = kwert;
			this.Rot = rot;
			this.Targt = targt;
			this.Reqto = reqto;
			this.Addin = addin;
			this.Sds = sds;
			this.Tov = tov;
			this.Crval = crval;
			this.Disc = disc;
			this.Indic = indic;
		}
	}
}