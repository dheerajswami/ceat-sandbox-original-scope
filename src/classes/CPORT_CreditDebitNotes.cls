global class CPORT_CreditDebitNotes {
    
    /*
* Auther  :- Swayam Arora
* Purpose :- Fetch Credit Debit Notes from SAP 
*
*
*/
    
    //-- ATTRIBUTES
    
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static List<CreditDebitMapping> getAllCreditDebitNotes(String cusNum, String fDate, String tDate,String territory){
        
        List<CreditDebitMapping> cndn = new List<CreditDebitMapping>();
        String customerId  = '';
        try{
            
            if(cusNum!=null){
               customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
            }
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
            
            
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            SapCNDN_CustomerWise   tst                         = new SapCNDN_CustomerWise ();
            SapCNDN_CustomerWise.ZWS_BAPI_CNDN_LISTING  zws         = new SapCNDN_CustomerWise.ZWS_BAPI_CNDN_LISTING();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
            SapCNDN_CustomerWise.TABLE_OF_ZFI107 tom        = new SapCNDN_CustomerWise.TABLE_OF_ZFI107();
            
            
            List<SapCNDN_CustomerWise.ZFI107> m           = new List<SapCNDN_CustomerWise.ZFI107>();        
            
            tom.item = m;
            
            SapCNDN_CustomerWise.TABLE_OF_ZFI107  e = new SapCNDN_CustomerWise.TABLE_OF_ZFI107();
            
            System.debug('Swayam123    '+customerId+'   '+fDate+'   '+tDate);
            //fDate = '2014-01-01';
            //tDate = '2014-10-31';
            //customerId = '0050003127';

            e = zws.ZBAPI_CNDN_LISTING(territory,fDate,tom, customerId, tDate);
            
            if(e.item != null){            
                for(SapCNDN_CustomerWise.ZFI107 z : e.item) {
                    system.debug(z);
                    cndn.add(new CreditDebitMapping(z.MANDT,z.DOC_NO, z.SP_GL_IND, z.PSTNG_DATE, z.CUSTOMER, z.DOC_TYPE, z.VBELN, z.FKART, z.SFAKN, z.CDNUM, z.ITEM_TEXT1, z.ITEM_TEXT, z.AMT_DOCCUR1, z.AMT_DOCCUR));
                }
            }
           // system.debug('Swayam SalesReg********************'+cndn[0]);
            return cndn;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
         
    //-- WRAPPER CLASS
    
    global class CreditDebitMapping{
        public String Mandt     {get;set;}
        public String DocNo     {get;set;}
        public String SpGlInd   {get;set;}
        public String PstngDate {get;set;}
        public String Customer  {get;set;}
        public String DocType   {get;set;}
        public String Vbeln     {get;set;}
        public String Fkart     {get;set;}
        public String Sfakn     {get;set;}
        public String Cdnum     {get;set;}
        public String ItemText1 {get;set;}
        public String ItemText  {get;set;}
        public String AmtDoccur1{get;set;}
        public String AmtDoccur {get;set;}        

        public CreditDebitMapping(String Mandt, String DocNo, String SpGlInd, String PstngDate, String Customer, String DocType, String Vbeln, String Fkart, String Sfakn, String Cdnum, String ItemText1, String ItemText, String AmtDoccur1, String AmtDoccur){
            this.Mandt = Mandt;
            this.DocNo = DocNo;
            this.SpGlInd = SpGlInd;
            this.PstngDate = PstngDate;
            this.Customer = Customer;
            this.DocType = DocType;
            this.Vbeln = Vbeln;
            this.Fkart = Fkart;
            this.Sfakn = Sfakn;
            this.Cdnum = Cdnum;
            this.ItemText1 = ItemText1;
            this.ItemText = ItemText;
            this.AmtDoccur1 = AmtDoccur1;
            this.AmtDoccur = AmtDoccur;
        }
    }
    
    //-- METHODS
    
    
}