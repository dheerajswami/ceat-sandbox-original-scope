@isTest
public class DashboardInputRM_BatchClass_Testclass{
	static String rmRole                = system.Label.RM_Role;
	  static testMethod void myUnitTest() {
		  	list<UserTerritory2Association> terr = [select id,Territory2.developerName,Territory2Id,UserId from UserTerritory2Association where Territory2.developerName='MUM' AND RoleInTerritory2='RM' limit 1];
	        User u = [Select id, name from User where id = :terr[0].UserId limit 1];
	  	    
	        System.runAs(u) {
	        	 //Insert Dashboard Master
                Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true,Role__c='RM');
                insert dm;
                
                Dashboard_Master__c dmtl = new Dashboard_Master__c(Active__c=true,Role__c='TL_Replacement');
                insert dmtl;
                
                Dashboard_Master__c dmtld = new Dashboard_Master__c(Active__c=true,Role__c='TLD');
                insert dmtld;
                
                Dashboard_Master__c dmcstl = new Dashboard_Master__c(Active__c=true,Role__c='CSTL');
                insert dmcstl;
                
	        	Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.ActionLogInputRecordType Limit 1].Id;
                Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.Efleet_RecType Limit 1].Id;
                /*
	        	list<sObject> dwmAl    = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterData');
                list<sObject> dwmPjp   = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataPJP');
                list<sObject> dwmEfleet  = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataEfleet');
                
                for(sObject dwm1 : dwmAl){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm1;
                    dwmTemp.RecordTypeId = actionLogRecTypeId;
                    dwmTemp.Role__c = rmRole;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    
                }
                update dwmAl;
                
                for(sObject dwm2 : dwmPjp){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm2;
                    dwmTemp.RecordTypeId = inputRecTypeId;
                    dwmTemp.Role__c = rmRole;
                    dwmTemp.Dashboard_Master__c = dm.id;
                }
                update dwmPjp;
                
                for(sObject dwm3 : dwmEfleet){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm3;
                    dwmTemp.RecordTypeId = efleetRecTypeId;
                    dwmTemp.Role__c = rmRole;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    
                }
                update dwmEfleet;
                */
                
                Dashboard_Weightage_Master__c dwmTemprm = new Dashboard_Weightage_Master__c();
		        dwmTemprm.Dashboard_Master__c = dm.id;
		        dwmTemprm.RecordTypeId = inputRecTypeId;
		        dwmTemprm.role__c = 'RM';
		        dwmTemprm.Weightage__c = 4;
		        dwmTemprm.Testing__c = true;
                dwmTemprm.Category__c = 'PJP adherance';
                dwmTemprm.Parameters_Inout__c = 'Maintain Dealer';
		        
		        insert dwmTemprm; 
		        
                Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();
		        dwmTemp.Dashboard_Master__c = dmcstl.id;
		        dwmTemp.role__c = 'CSTL';
		        dwmTemp.Weightage__c = 4;
		        dwmTemp.Testing__c = true;
                dwmTemp.Category__c = 'PJP adherance';
                dwmTemp.Parameters_Inout__c = 'Maintain Dealer';
		        
		        insert dwmTemp; 
		        
		        Dashboard_Weightage_Master__c dwmTemp1 = new Dashboard_Weightage_Master__c();
		        dwmTemp1.Dashboard_Master__c = dmtld.id;
		        dwmTemp1.role__c = 'TLD';
		        dwmTemp1.Weightage__c = 4;
		        dwmTemp1.Testing__c = true;
                dwmTemp1.Category__c = 'PJP adherance';
                dwmTemp1.Parameters_Inout__c = 'Maintain Dealer';
		        
		        insert dwmTemp1; 
		        
		        
		        Dashboard_Weightage_Master__c dwmTemp2 = new Dashboard_Weightage_Master__c();
		        dwmTemp2.Dashboard_Master__c = dmtl.id;
		        dwmTemp2.role__c = 'TL_Replacement';
		        dwmTemp2.Weightage__c = 4;
		        dwmTemp2.Testing__c = true;
                dwmTemp2.Category__c = 'PJP adherance';
                dwmTemp2.Parameters_Inout__c = 'Maintain Dealer';
		        
		        insert dwmTemp2; 
		        Id tlRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:'TL' Limit 1].Id;
		        Id tldRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:'TLD' Limit 1].Id;
		        Id cstlRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName ='CSTL' Limit 1].Id;
		        
                 Dashboard_Input_Score__c dstl=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstl.Territory_Code__c             = 'B0001';
                                    dstl.Dashboard_Weightage_Master__c = dwmTemp2.Id;
                                    dstl.Month__c                      = 'August';
                                    dstl.Year__c                       = '2015';     
                                    dstl.RecordTypeId                  = tlRecTypeId;                     
                                    dstl.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstl.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstl.Total_Monthly_Target__c		= 0;                                    
                                    dstl.Total_MTD_Target__c			= 0;                                   
                                    dstl.Total_Actual_MTD__c			= 0;
                                    dstl.Achievement_MTD__c				= 0;
                                    dstl.Score__c 						= 0;
                                     
                insert dstl;
                Dashboard_Input_Score__c dstl1=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstl1.Territory_Code__c             = 'B0003';
                                    dstl1.Dashboard_Weightage_Master__c = dwmTemp2.Id;
                                    dstl1.Month__c                      = 'August';
                                    dstl1.Year__c                       = '2015';     
                                    dstl1.RecordTypeId                  = tlRecTypeId;                     
                                    dstl1.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstl1.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstl1.Total_Monthly_Target__c		= 0;                                    
                                    dstl1.Total_MTD_Target__c			= 0;                                   
                                    dstl1.Total_Actual_MTD__c			= 0;
                                    dstl1.Achievement_MTD__c				= 0;
                                    dstl1.Score__c 						= 0;
                                     
                insert dstl1;
                Dashboard_Input_Score__c dstld=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstld.Territory_Code__c             = 'DI041';
                                    dstld.Dashboard_Weightage_Master__c = dwmTemp1.Id;
                                    dstld.Month__c                      = 'August';
                                    dstld.Year__c                       = '2015';     
                                    dstld.RecordTypeId                  = tldRecTypeId;                     
                                    dstld.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstld.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c    = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstld.Total_Monthly_Target__c		= 0;                                    
                                    dstld.Total_MTD_Target__c			= 0;                                   
                                    dstld.Total_Actual_MTD__c			= 0;
                                    dstld.Achievement_MTD__c			= 0;
                                    dstld.Score__c 						= 0;
                                     
                insert dstld;
                
                 Dashboard_Input_Score__c dstld1=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstld1.Territory_Code__c             = 'DI041';
                                    dstld1.Dashboard_Weightage_Master__c = dwmTemp1.Id;
                                    dstld1.Month__c                      = 'August';
                                    dstld1.Year__c                       = '2015';     
                                    dstld1.RecordTypeId                  = tldRecTypeId;                     
                                    dstld1.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstld1.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c    = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstld1.Total_Monthly_Target__c		= 0;                                    
                                    dstld1.Total_MTD_Target__c			= 0;                                   
                                    dstld1.Total_Actual_MTD__c			= 0;
                                    dstld1.Achievement_MTD__c			= 0;
                                    dstld1.Score__c 						= 0;
                                     
                insert dstld1;
                Dashboard_Input_Score__c dscstl=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dscstl.Territory_Code__c             = 'B0001,B0003,DI041';
                                    dscstl.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dscstl.Month__c                      = 'August';
                                    dscstl.Year__c                       = '2015';     
                                    dscstl.RecordTypeId                  = cstlRecTypeId;                     
                                    dscstl.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dscstl.Category__c                   = 'PJP adherance';
                                    //dscstl.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dscstl.Total_Monthly_Target__c		 = 0;                                    
                                    dscstl.Total_MTD_Target__c			 = 0;                                   
                                    dscstl.Total_Actual_MTD__c			 = 0;
                                    dscstl.Achievement_MTD__c			 = 0;
                                    dscstl.Score__c						 = 0;
                                     
                insert dscstl;
                ScheduledBatch_DashboardInputRM temp = new ScheduledBatch_DashboardInputRM();
                String sch = '0 45 23 * * ?'; 
                system.schedule('RM Dashboard', sch, temp);
                DashboardInputRM_BatchClass db=new DashboardInputRM_BatchClass();
                database.executebatch(db,2000);
	        }
	  }
}