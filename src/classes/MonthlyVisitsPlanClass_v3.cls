global with sharing class MonthlyVisitsPlanClass_v3 {
    
    public static final String salesforceBaseUrl {get;}
    //public List<String> userTerrList {get; set;}

    static {
        salesforceBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
    }

    public MonthlyVisitsPlanClass_v3() {
        //getTerrritories();
    }

    @RemoteAction
    global static WrapperPjpDetails getWriteAccess(String gotMonth, String gotYear, String gotOwner) {
        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();        
        WrapperPjpDetails wpd = new WrapperPjpDetails();            
        Date todayDate = Date.today();
        String yearString;
        PJP__c newPJP;

        List<ACW__c> window = new List<ACW__c>();
        if(gotMonth == null) {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Replacement' And Active__c = true And From_Date__c <= :todayDate And To_Date__c >= :todayDate];            

            if(window.size() == 0) {
                wpd.monthInt = todayDate.month();
                wpd.monthString = calculateMonth(todayDate.month());
                wpd.yearInt = todayDate.year();
                yearString = String.valueOf(wpd.yearInt);
                wpd.isReadOnly = true;                
            } else {
                wpd.monthString = window[0].Month_Text__c;
                wpd.monthInt = Integer.valueOf(window[0].Month__c);
                wpd.yearInt = Integer.valueOf(window[0].Year__c);
                yearString = String.valueOf(wpd.yearInt);
                wpd.isReadOnly = false;
            }
        } else {
            window = [Select From_Date__c, To_Date__c, Month__c, Month_Text__c, Year__c from ACW__c 
                where Page__c = 'PJP Replacement' And Active__c = true And Month_Text__c = :gotMonth And Year__c = :gotYear]; 

            wpd.monthString = window[0].Month_Text__c;
            wpd.monthInt = Integer.valueOf(window[0].Month__c);
            wpd.yearInt = Integer.valueOf(window[0].Year__c);
            yearString = String.valueOf(wpd.yearInt);

            if(window[0].From_Date__c <= todayDate && window[0].To_Date__c >= todayDate) {
                wpd.isReadOnly = false;
            } else {
                wpd.isReadOnly = true;
            }         
        }
        
        if(gotOwner == null) {
            wpd.ownerId = UserInfo.getUserId();
        } else {
            wpd.ownerId = gotOwner;
        }
        User ownerUser = [Select Id, Name, EmployeeNumber, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :wpd.ownerId];
        String str;
        if(ownerUser.UserRole.Name.startsWith('TL -')) {
            str = 'TL - Replacement';
        } else if(ownerUser.UserRole.Name.startsWith('CSTL')) {
            str = 'CSTL';
        }
        try {            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c
                where Visit_Plan_Month__c = :wpd.monthString And Visit_Plan_Year__c = :yearString And OwnerId = :wpd.ownerId limit 1];
            wpd.pjpId = newPJP.Id;
            wpd.pjpComments = newPJP.Comments__c;
            if(newPJP.OwnerId != UserInfo.getUserId() || newPJP.Sys_ApprovalStatus__c == 'Submitted' || newPJP.Sys_ApprovalStatus__c == 'Approved') {
                wpd.isReadOnly = true;
            }
            if(newPJP.PJP_Norms__c != null) {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,No_Of_Influencers__c,No_Of_Trainings__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r) FROM PJP_Norms_Main__c where Id = :newPJP.PJP_Norms__c];
            } else {
                activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,No_Of_Influencers__c,No_Of_Trainings__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                    (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r) FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = :str Limit 1];//LIKE :('%'+ownerUser.UserRole.DeveloperName+'%')
            }
        } catch(Exception e) {            
            activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,No_Of_Influencers__c,No_Of_Trainings__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                           (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r) FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = :str Limit 1];            
        }

        wpd.dailyTargets = '';
        system.debug('==#1 testclass'+activeNorms);
        for(PJP_Norms_Main__c pnm : activeNorms) {
            system.debug('==#2 testclass'+activeNorms);
            wpd.dailyTarget = Integer.valueOf(pnm.Number_of_Daily_Visits__c);
            wpd.isOE = pnm.Is_OE_Days__c;
            wpd.isMOR = pnm.Is_MOR__c;
            wpd.isCompAct = pnm.Is_Company_Activities__c;
            wpd.oeDays = Integer.valueOf(pnm.OE_Days__c);
            wpd.morDays = Integer.valueOf(pnm.MOR__c);
            wpd.compActDays = Integer.valueOf(pnm.Company_Activities__c);
            wpd.influDays = Integer.valueOf(pnm.No_Of_Influencers__c);

            //

            wpd.trainingDays = Integer.valueOf(pnm.No_Of_Trainings__c);

            //

            for(PJP_Daily_Norms__c pdn : pnm.PJP_Daily_Norms__r) {
                wpd.dailyTargets+= pdn.Type__c+','+pdn.Value__c+';';
            }
        }

        return wpd;
    }


    /**
     * getTerrritories
     * @Autor neha.mishra@extentor.com
     * @description A description of the function 
     * @param NA
     * @return List of UserTerritory2Association 
     */

    @RemoteAction
    global static List<UserTerritory2Association> getTerrritories() {
        String ownerId = UserInfo.getUserId();
        List<UserTerritory2Association> terrList = [Select Territory2Id,Territory2.Name,Id,UserId,User.Name from UserTerritory2Association where UserId =:ownerId AND IsActive = true]; 
        
        /*for(UserTerritory2Association tmpTerr : [Select Territory2Id,Territory2.Name,Id,UserId,User.Name from UserTerritory2Association where UserId =:ownerId AND IsActive = true]) {
            if(tmpTerr !=  null) {
                userTerrList.add(tmpTerr.Territory2.Name);
            } 
        }*/
        if(!terrList.isEmpty()) {
            return terrList;
        }else {
            return null;
        }
        
    }


    /**
     * getPlannedAccounts
     * @Author neha.mishra@extentor.com
     * @description this function when called from a page, return list of Accounts for which PJP has been planned for a certain month
     * @param String mnth : PJP Month
     * @param String year : PJP Year
     * @param String ownerId : PJP Owner
     * @param String accountRecordType 
     * @return List<Account> : returns related Accounts to a PJP for Account type "Customer"
     */
    
    @RemoteAction
    global static List<Account> getPlannedAccounts(Integer mnth, String year, String ownerId) {
        Set<Id> accountIdSet = new Set<Id>();
        String month = UtilityClass.fetchMonthName(mnth);
        List<Visits_Plan__c> visitplanList = [Select Id, RecordTypeId, Check_In__c, Dealer__c, Dealer__r.Name, 
                                                Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                                FROM Visits_Plan__c WHERE Master_Plan__r.Visit_Plan_Month__c =: month 
                                                And Master_Plan__r.Visit_Plan_Year__c =: year And Master_Plan__r.OwnerId =: ownerId];


        if(visitplanList.size() > 0) {
            for(Visits_Plan__c vp : visitplanList) {
                if(!accountIdSet.contains(vp.Dealer__c)) {
                    accountIdSet.add(vp.Dealer__c);       
                } 
            }
        }

        List<Account> relatedAccountList = new List<Account>();

        relatedAccountList = [SELECT Id, Name, Customer_Segment__c, KUNNR__c, UniqueIdentifier__c,RecordType.Name,
                                Town__c,Sales_District_Text__c FROM Account WHERE RecordType.Name = 'Customer' AND Id IN: accountIdSet];
        //system.debug('==#1 '+relatedAccountList[0].RecordType.Name);

        if(relatedAccountList.size() > 0) {
            return relatedAccountList;
        }else {
            return null;
        }
    }


    
    @RemoteAction
    global static List<WrapperVisitPlanDays> getRelatedAccounts(Integer yr,Integer mn, String mnth, String year, String ownerId) {
                  
        List<AggregateResult> allAccounts = new List<AggregateResult>(); 
        List<String> deType = new List<String>();
        
        List<WrapperVisitPlanDays> tableList = new List<WrapperVisitPlanDays>();        
        Map<Integer,Holiday__c>  dlMap = new Map<Integer,Holiday__c>();
        Set<Integer>  hlList = new Set<Integer>(); 
        Integer en =  date.daysInMonth(yr, mn);
        List<DayList> listOfWeekndsAndHoliday;
        Integer day29;
        Integer day30;
        Integer day31;
        Set<Id> accountIdSet = new Set<id>();
        List<AvailableDay> availableDaysList;
        
        List<Visits_Plan__c> visitplanList = [Select Id, RecordTypeId, Check_In__c, Dealer__c, Dealer__r.Name, Master_Plan__c, Status__c, Visit_Day__c, Type_of_Day__c
                                              from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c =: year And Master_Plan__r.OwnerId =: ownerId];
        List<PJP__c> newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c, OwnerId from PJP__c
                where Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :year And OwnerId = :ownerId limit 1];

        if(ownerId == null) {
            ownerId = UserInfo.getUserId();
        }

        User ownerUser = [Select Id, Name, EmployeeNumber, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :ownerId];        
		String str;
        if(ownerUser.UserRole.Name.startsWith('TL -')) {
            str = 'TL - Replacement';
        } else if(ownerUser.UserRole.Name.startsWith('CSTL')) {
            str = 'CSTL';
        }
        List<PJP_Norms_Main__c> activeNorms = new List<PJP_Norms_Main__c>();
        if(newPJP.size() > 0 && newPJP[0].PJP_Norms__c != null) {
            activeNorms = [SELECT Id, Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r),
                (SELECT Id,Segment_Name__c,Value__c FROM PJP_Segment_Norm__r) 
                FROM PJP_Norms_Main__c where Id = :newPJP[0].PJP_Norms__c];
        } else {
            activeNorms = [SELECT Id, Company_Activities__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r),
                (SELECT Id,Segment_Name__c,Value__c FROM PJP_Segment_Norm__r) 
                FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = :str Limit 1];
        }        
                   
        Set<String> userTerrAssociation = new Set<String>();
        for(UserTerritory2Association us : [Select Territory2Id,Territory2.Name,Id from UserTerritory2Association where UserId =:ownerId AND IsActive = true]){
            userTerrAssociation.add(us.Territory2.Name);
        }                       

        UserTerritory2Association userTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:ownerId Limit 1];
        Territory2 userTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerrAss.Territory2Id Limit 1];
        Territory2 userRegion = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userTerritory.ParentTerritory2Id Limit 1];
        Territory2 userZone = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userRegion.ParentTerritory2Id Limit 1];

        if(visitplanList.size() > 0) {
            for(Visits_Plan__c vp : visitplanList) {
                if(!accountIdSet.contains(vp.Dealer__c)) {
                    accountIdSet.add(vp.Dealer__c);       
                } 
            }
        }
        
        for(Holiday__c hol : [Select Name, Active__c, Date__c, Name_of_Holiday__c, Description__c, Employee_No__c, Start_Date__c, End_Date__c, RecordType.Name From Holiday__c where (((RecordType.Name = 'National' Or (RecordType.Name = 'Regional' And Region__c = :userRegion.Name)) And Date__c >= :date.newinstance(yr,mn,1) And Date__c <= :date.newinstance(yr,mn,en)) Or (RecordType.Name = 'Planned Leaves' And Employee_No__c = :ownerUser.EmployeeNumber))]){
            if(hol.RecordType.Name == 'Planned Leaves') {    
                for(Date d = hol.Start_Date__c; d <= hol.End_Date__c; d = d.addDays(1)) {                    
                    if(d.month() == Date.newInstance(yr, mn, 1).month()) {
                        dlMap.put(d.day(), hol);
                    }
                } 
                            
            } else {
                dlMap.put(hol.Date__c.day(), hol);
            }            
        }
        
        List<String> accountRecordTypes = 'Dealer or Prospect,Customer,Influencer,Training'.split(',');//System.Label.PJP_TypeToPlan        
        //allAccounts = [Select Id, Name, Customer_Segment__c, KUNNR__c, UniqueIdentifier__c, RecordType.Name RtName, Town__c,Sales_District_Text__c from Account where Sales_District_Text__c In :userTerrAssociation And Active__c = true And RecordType.Name in :accountRecordTypes group by RecordType.Name, Town__c, Sales_District_Text__c, Customer_Segment__c, Name, Id, KUNNR__c, UniqueIdentifier__c order by Town__c, Name];
        allAccounts = [Select Id, Name, Customer_Segment__c, KUNNR__c, UniqueIdentifier__c, RecordType.Name RtName, Town__c,Sales_District_Text__c from Account where Sales_District_Text__c In :userTerrAssociation And Active__c = true And (RecordType.Name = 'Dealer' Or RecordType.Name = 'Prospect') group by Town__c, Sales_District_Text__c, Customer_Segment__c, Name, RecordType.Name, Id, KUNNR__c, UniqueIdentifier__c order by Town__c, Name];
        
        allAccounts.addAll([Select Id, Name, Customer_Segment__c, KUNNR__c, UniqueIdentifier__c, RecordType.Name RtName, Town__c,Sales_District_Text__c from Account where Sales_District_Text__c In :userTerrAssociation And Active__c = true And RecordType.Name = 'Customer' group by RecordType.Name, Town__c, Sales_District_Text__c, Customer_Segment__c, Name, Id, KUNNR__c, UniqueIdentifier__c order by Town__c, Name]);
        if(allAccounts.size() > 0){
            List<String> weekendList = System.Label.PJP_Weekends.split(',');
            if(weekendList.size() == 0) {
                weekendList.add('Sunday');
            }
            listOfWeekndsAndHoliday = new List<DayList>();
            for(Integer i=1; i<=en; i++) {                        
                DayList dl = new DayList();
                DateTime dt=DateTime.newInstance(date.newInstance(yr,mn,i), Time.newInstance(0, 0, 0, 0));
                String dayOfWeek = dt.format('EEE');

                if(dlMap.containsKey(i)) {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'PublicHoliday';
                        dl.holReason = '';
                        if(dlMap.containsKey(i)) {
                            dl.holReason = dlMap.get(i).Name;
                        }
                    }                        
                } else {
                    Boolean b = false;
                    for(String s : weekendList) {                       
                        if(dayOfWeek == s.subString(0, 3)) {
                            dl.dayNumber = i;
                            dl.typeCol = 'Holiday';
                            dl.holReason = s;
                            b = true;
                            break;
                        }
                    }
                    if(b == false){
                        dl.dayNumber = i;
                        dl.typeCol = 'Normal'; 
                    }  
                }
                
                listOfWeekndsAndHoliday.add(dl);
            }

            if(en == 29) {
                day29 = 29;
            } else if(en == 30) {
                day30 = 30;
            } else if(en == 31) {
                day31 = 31;
            }     
            
            for(AggregateResult  d : allAccounts) {                
                Integer target;
                availableDaysList = new List<AvailableDay>(35);                                                                       
                for(PJP_Norms_Main__c pnm : activeNorms) {
                    for(PJP_Segment_Norm__c psn : pnm.PJP_Segment_Norm__r) {
                        if(psn.Segment_Name__c == d.get('Customer_Segment__c')) {                            
                            target = Integer.valueOf(psn.Value__c);                            
                            break;
                        }
                    }
                }
        
                for(integer i = 0; i < 32; i++) {
                    AvailableDay avail = new AvailableDay(i);
                    avail.value = i;
                    avail.dateAvailable = false;
                    avail.planVisitId = null;
                    availableDaysList.set(i,avail);                    
                }
                
                if(availableDaysList.size() > 0) {
                    for(Visits_Plan__c vp : visitplanList) {                         
                        if((d.id == vp.Dealer__c)) {
                            Date d1 = vp.Visit_Day__c;
                            AvailableDay availInstance = new AvailableDay(d1.day());
                            availInstance.value = d1.day();
                            availInstance.typeOfDay = vp.Type_of_Day__c;
                            availInstance.dateAvailable= true;
                            availInstance.planVisitId = vp.id;
                            availableDaysList.set(availInstance.value-1,availInstance);    
                        }
                    } 
                }
                
                tableList.add(new WrapperVisitPlanDays(String.ValueOf(d.get('Id')),String.ValueOf( d.get('Name')),String.ValueOf(d.get('Customer_Segment__c')), String.ValueOf(d.get('UniqueIdentifier__c')), String.ValueOf((d.get('RtName') == 'Dealer' || d.get('RtName') == 'Prospect' ? 'Dealer' : d.get('RtName'))), String.ValueOf(d.get('Town__c')), String.ValueOf(d.get('Sales_District_Text__c')), 'Visit Day', target, listOfWeekndsAndHoliday, day29, day30, day31, availableDaysList));
            }       
            
            List<AvailableDay> morAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> oeAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> compActAvailableDays = new List<AvailableDay>(35);
            List<AvailableDay> influencerVisits = new List<AvailableDay>(35);
            List<AvailableDay> trainingVisits = new List<AvailableDay>(35);
                        
            for(integer i = 0; i < 32; i++) {
                AvailableDay avail = new AvailableDay(i);
                avail.value = i;
                avail.dateAvailable = false;
                avail.planVisitId = null;
                morAvailableDays.set(i,avail);
                oeAvailableDays.set(i,avail);
                compActAvailableDays.set(i,avail);  
                influencerVisits.set(i,avail);
                trainingVisits.set(i, avail);  
            }

            for(Visits_Plan__c vp : visitplanList) {
                if(vp.Type_of_Day__c != 'Visit Day') {
                    Date d1 = vp.Visit_Day__c;
                    AvailableDay availInstance = new AvailableDay(d1.day());
                    availInstance.value = d1.day();
                    availInstance.typeOfDay = vp.Type_of_Day__c;
                    availInstance.dateAvailable= true;
                    availInstance.planVisitId = vp.id;

                    if(vp.Type_of_Day__c == 'MOR') {                        
                        morAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'OE') {
                        oeAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Company Activity') {
                        compActAvailableDays.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Influencer Visit') {
                        influencerVisits.set(availInstance.value-1, availInstance);
                    } else if(vp.Type_of_Day__c == 'Training Visit') {
                        trainingVisits.set(availInstance.value-1, availInstance);
                    }
                }
            }
            
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, null, 'MOR', null, listOfWeekndsAndHoliday, day29, day30, day31, morAvailableDays));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, null, 'OE', null, listOfWeekndsAndHoliday, day29, day30, day31, oeAvailableDays));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, null, 'Company Activity', null, listOfWeekndsAndHoliday, day29, day30, day31, compActAvailableDays));
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, null, 'Influencer Visit', null, listOfWeekndsAndHoliday, day29, day30, day31, influencerVisits));            
            tableList.add(new WrapperVisitPlanDays(null, null, null, null, null, null, null, 'Training Visit', null, listOfWeekndsAndHoliday, day29, day30, day31, trainingVisits));            
        }        
        return tableList; 
    } 
    
    @RemoteAction
    global static String saveVisitPlans(List<Visits_Plan__c> visitplanList,integer yr,Integer mn, string mnth, string year, String buttonClicked, String comments) {        
        User currentUser = [Select Id, Name, Profile.Name, UserRole.Name, UserRole.DeveloperName, ManagerId, Manager.Email, Manager.Name from User where Id = :UserInfo.getUserId()];        
        RecordType rtype = [Select Id, Name, DeveloperName from RecordType where DeveloperName = 'Replacement' And SobjectType = 'PJP__c'];
        
        List<RecordType> rtList = [Select Id, Name, DeveloperName from RecordType where SobjectType = 'Visits_Plan__c'];
        Id rtCompAct = null;
        Id rtInfluencer = null;
        Id rtTraining = null;
        Id rtVisit = null;
        for(RecordType rt : rtList) {
            if(rt.Name == 'MOR-OE-CompAct') {
                rtCompAct = rt.Id;
            } else if(rt.Name == 'Influencer Visit') {
                rtInfluencer = rt.Id;
            } else if(rt.Name == 'Training Visit') {
                rtTraining = rt.Id;
            }else if(rt.Name == 'Visit') {
                rtVisit = rt.Id;
            }
        }

        PJP__c newPJP;
        Date visitPlanDate;
        List<Visits_Plan__c> visitplantoupsert = new List<Visits_Plan__c>();

        String str;
        if(currentUser.UserRole.Name.startsWith('TL -')) {
            str = 'TL - Replacement';
        } else if(currentUser.UserRole.Name.startsWith('CSTL')) {
            str = 'CSTL';
        }
        
        List<PJP_Norms_Main__c> activeNorms = [SELECT Id,Is_MOR__c,Is_OE_Days__c,Is_Company_Activities__c,Company_Activities__c,No_Of_Influencers__c,No_Of_Trainings__c,RecordType.Name,MOR__c,OE_Days__c,User_Type__c,Visits__c, Active__c, Number_of_Daily_Visits__c,
                    (SELECT Id,Type__c,Value__c FROM PJP_Daily_Norms__r) FROM PJP_Norms_Main__c where Active__c = true And RecordType.Name = :Constants.pjpNormsRecordTypeReplacement And User_Type__c = :str Limit 1];
            
        try{            
            newPJP = [Select Id, PJP_Norms__c, Visit_Plan_Month__c, Visit_Plan_Year__c, Sys_ApprovalStatus__c, Comments__c from PJP__c where RecordTypeId = :rtype.Id And Visit_Plan_Month__c = :mnth And Visit_Plan_Year__c = :year And OwnerId = :UserInfo.getUserId() limit 1];
            
            if(newPJP.PJP_Norms__c == null) {
                if(activeNorms.size() > 0) {
                    newPJP.PJP_Norms__c = activeNorms[0].Id;
                }
            } 
            newPJP.Comments__c = comments;            
            update newPJP;
            PJP_CalculateSummary.calculateSummary(newPJP.Id);
            
        } catch(Exception e){                            
            newPJP = new PJP__c();
            if(activeNorms.size() > 0) {
                newPJP.PJP_Norms__c = activeNorms[0].Id;
            }
            newPJP.Visit_Plan_Month__c = mnth;
            newPJP.Visit_Plan_Year__c = year;                        
            newPJP.Visit_Plan_Date_del__c = Date.newInstance(yr, mn, 1);        
            newPJP.Comments__c = comments;    
            newPJP.Owner_s_Manager__c = currentUser.ManagerId;
            newPJP.RecordTypeId = rtype.Id;
            List<UserTerritory2Association> listUserTerrAss = [Select IsActive,Territory2Id,Territory2.Name,UserId,User.Name,Id from UserTerritory2Association where UserId =:currentUser.Id];
            Set<Id> terrIds = new Set<Id>();
            for(UserTerritory2Association uta : listUserTerrAss) {
                terrIds.add(uta.Territory2Id);
            }
            List<Territory2> listUserTerritory = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id in :terrIds];
            for(Integer i=0; i<listUserTerritory.size(); i++) {
                if(i == 0) {
                    newPJP.Territory__c = listUserTerritory[i].Name;
                } else {
                    newPJP.Territory__c += '; ' + listUserTerritory[i].Name;
                }                
            }        
            if(listUserTerritory.size() > 0) {
                Territory2 userRegion = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :listUserTerritory[0].ParentTerritory2Id Limit 1];
                Territory2 userZone = [select id,Description,Name,ParentTerritory2Id from Territory2 where Id = :userRegion.ParentTerritory2Id Limit 1];   
                newPJP.Region__c = userRegion.Name;
                newPJP.Zone__c = userZone.Name;
            }            
            insert newPJP;         
            PJP_CalculateSummary.calculateSummary(newPJP.Id);
        }

        for(Visits_Plan__c temp1: visitplanList){            
            temp1.Visit_Day__c = Date.newinstance(Integer.ValueOf(yr),Integer.ValueOf(mn),Integer.ValueOf(temp1.Day__c));
            temp1.Master_Plan__c = newPJP.Id;
            temp1.Status__c = 'Scheduled';
            if(temp1.Type_of_Day__c == 'MOR' || temp1.Type_of_Day__c == 'OE' || temp1.Type_of_Day__c == 'Company Activity') {                        
                temp1.RecordTypeId = rtCompAct;
            } else if(temp1.Type_of_Day__c == 'Influencer Visit') {
                temp1.RecordTypeId = rtInfluencer;
            } else if(temp1.Type_of_Day__c == 'Training Visit') {
                temp1.RecordTypeId = rtTraining;
            }else {
                temp1.RecordTypeId = rtVisit;
            }                    
            
            visitplantoupsert.add(temp1);            
        }
        
        boolean flag = false;
        if(visitplantoupsert.size() > 0) {
            try {
                List<Visits_Plan__c> previousVisitPlans = [Select Id from Visits_Plan__c where Master_Plan__r.Visit_Plan_Month__c =: mnth And Master_Plan__r.Visit_Plan_Year__c =: year And Master_Plan__r.OwnerId =: UserInfo.getUserId()];
                List<Visits_Plan__c> deleteVisitPlans = new List<Visits_Plan__c>();

                for(Visits_Plan__c prevVp : previousVisitPlans) {
                    Boolean idFound = false;
                    for(Visits_Plan__c newVp : visitplantoupsert) {
                        if(prevVp.Id == newVp.Id) {
                            idFound = true;
                            break;
                        }
                    }
                    if(!idFound) {
                        deleteVisitPlans.add(prevVp);
                    }
                }

                delete deleteVisitPlans;
                upsert visitplantoupsert;
                         
                if(buttonClicked == 'Submit') {
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setComments('Submitting request for approval.');
                    req1.setObjectId(newPJP.Id);
                    
                    // Submit the approval request for the account
                    Approval.ProcessResult result = Approval.process(req1);
                    FeedItem fitem=new FeedItem();
                    fItem.parentId=currentUser.ManagerId;
                    fItem.Title='Click here to Approve/Reject';
                    fItem.body = 'PJP has been submitted for the month '+newPJP.Visit_Plan_Month__c+' by '+currentUser.Name+'. ';
                    //fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/MonthlyVisitsPlan_v2?owner='+currentUser.Id+'&month='+newPJP.Visit_Plan_Month__c+'&year='+newPJP.Visit_Plan_Year__c;
                    fItem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+newPJP.Id;
                    insert fItem;
                    //String[] toAddresses = new String[] {currentUser.Manager.Email}; 
                    //sendemail(newPJP.Id, toAddresses, currentUser.ManagerId);
                }
                       
                flag = true;
            } catch(Exception e) {
                system.debug('Approval Error: '+e.getMessage());
            }
        }
        return buttonClicked+'; '+newPJP.Id;
    }
       
    public static void sendemail(Id gotId, String[] toAddresses, Id managerId) {        
        //String[] toAddresses = new String[] {'swayam.arora@extentor.com'}; 
        System.debug('Swayam Email '+toAddresses);
        EmailTemplate emailTemplateInstance = [select Id from EmailTemplate where Name='PJP Submission Template'];
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(emailTemplateInstance.Id);
        email.setToAddresses(toAddresses);
        email.setTargetObjectId(managerId);
        //email.setWhatId(gotId);
        email.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.Email[] { email } );            
        System.debug('Swayam Email2 '+toAddresses);
    }  
    
    public static String calculateMonth(Integer mm) {
        if(mm == 1) 
            return 'January';
        if(mm == 2) 
            return 'February';
        if(mm == 3) 
            return 'March';
        if(mm == 4) 
            return 'April';
        if(mm == 5) 
            return 'May';
        if(mm == 6) 
            return 'June';
        if(mm == 7) 
            return 'July';
        if(mm == 8) 
            return 'August';
        if(mm == 9) 
            return 'September';
        if(mm == 10) 
            return 'October';
        if(mm == 11) 
            return 'November';
        if(mm == 12) 
            return 'December';

        return '';
    }

    //Wrapper Class

    public class DayList{
        public Integer dayNumber {get;set;}
        public String typeCol {get;set;}
        public String holReason {get;set;}
    }

    public class AvailableDay {
        public integer value {get;set;}
        public boolean dateAvailable {get;set;}
        public id planVisitId {get;set;}
        public String typeOfDay{get;set;}
        
        public AvailableDay(integer i) {
            value = i;
            dateAvailable = false;
            planVisitId = null;
            typeOfDay = null;
        }
    }

    global class WrapperPjpDetails {
        public Integer dailyTarget {get;set;}
        public Boolean isOE {get;set;}
        public Boolean isMOR {get;set;}
        public Boolean isCompAct {get;set;}
        public Integer oeDays {get;set;}
        public Integer morDays {get;set;}
        public Integer compActDays {get;set;}
        public Integer influDays {get;set;}
        public Integer trainingDays {get; set;}
        public String dailyTargets {get;set;}
        public String monthString {get;set;}
        public Integer yearInt {get;set;}
        public Integer monthInt {get;set;}
        public String pjpComments {get;set;}
        public String pjpId {get;set;}
        public String ownerId {get;set;}
        public Boolean isReadOnly {get;set;}
    }
    
    global class WrapperVisitPlanDays {
        public String accountId{get;set;}
        public String accountName{get;set;}
        public String accountType{get;set;}
        public String accountSapCode{get;set;}
        public String accountRecordType{get;set;}
        public String accountTown{get;set;}

        // Added by Neha

        public String accountSalesDistrict{get;set;}

        // End

        public String typeOfDay{get;set;}          
        public List<DayList> dayMon{get;set;}
        public Integer day1{get;set;}
        public Integer day2{get;set;}
        public Integer day3{get;set;}
        public Integer day4{get;set;}
        public Integer day5{get;set;}
        public Integer day6{get;set;}
        public Integer day7{get;set;}
        public Integer day8{get;set;}
        public Integer day9{get;set;}
        public Integer day10{get;set;}
        public Integer day11{get;set;}
        public Integer day12{get;set;}
        public Integer day13{get;set;}
        public Integer day14{get;set;}
        public Integer day15{get;set;}
        public Integer day16{get;set;}
        public Integer day17{get;set;}
        public Integer day18{get;set;}
        public Integer day19{get;set;}
        public Integer day20{get;set;}
        public Integer day21{get;set;}
        public Integer day22{get;set;}
        public Integer day23{get;set;}
        public Integer day24{get;set;}
        public Integer day25{get;set;}
        public Integer day26{get;set;}
        public Integer day27{get;set;}
        public Integer day28{get;set;}
        public Integer day29{get;set;}
        public Integer day30{get;set;}
        public Integer day31{get;set;}
        public List<AvailableDay> availableDayList{get;set;}        
        public Integer targetDays{get;set;}
        public Integer plannedDays{get;set;}
        
        public WrapperVisitPlanDays(String dId, String dName, String dType, String dSapCode, String recType, String accTown, String accDistrict, String tod, Integer tDays, List<DayList> dt, Integer day29, Integer day30, Integer day31, List<AvailableDay> availableDaysVar)
        {
            dayMon = new List<DayList>();
            accountId = dId;
            accountName = dName;
            accountType = dType;
            accountSapCode = dSapCode;
            accountRecordType = recType; 
            accountTown = accTown;

            // Added by Neha

            accountSalesDistrict = accDistrict;

            // End

            typeOfDay = tod;         
            targetDays = tDays;
            dayMon = dt;
            day1 = 1;
            day2 = 2;
            day3 = 3;
            day4 = 4;
            day5 = 5;
            day6 = 6;
            day7 = 7;
            day8 = 8;
            day9 = 9;
            day10 = 10;
            day11 = 11;
            day12 = 12;
            day13 = 13;
            day14 = 14;
            day15 = 15;
            day16 = 16;
            day17 = 17;
            day18 = 18;
            day19 = 19;
            day20 = 20;
            day21 = 21;
            day22 = 22;
            day23 = 23;
            day24 = 24;
            day25 = 25;
            day26 = 26;
            day27 = 27;
            day28 = 28;
            this.day29 = day29;
            if(day30 == 30) {
                this.day29 = 29;
                this.day30 = day30;
            } if(day31 == 31) {
                this.day29 = 29;
                this.day30 = 30;
                this.day31 = day31;
            }
            availableDayList = new List<AvailableDay>();
            availableDayList = availableDaysVar;
        }
    }


}