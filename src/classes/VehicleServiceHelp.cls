public class VehicleServiceHelp{
    public cls_customersDetails[] customersDetails;
   public class cls_customersDetails {
        public String Name; //Ashish Verma
        public String Mobile;   //9711300094
        public String Email;    //ashish.verma@Vfirst.com
        public String VehicleRegistrationNo;    //123456789
        public String VehicleMake;  //Honda
        public String VehicleModel; //Brio
        public String NextWheelAlignmentDate;   //2015-11-15 00:00:00
        public String NextBlancingDate; //2015-11-15 00:00:00
        public String LastWheelAlignmentDate;   //2015-09-15 00:00:00
        public String LastBlancingDate; //2015-09-15 00:00:00
        public String Alignment;    //TRUE
        public String Balancing;    //TRUE
        public String NitrogenInflation;    //TRUE
        public String TyresChanged; //TRUE
        public String FLTyreCondition;  //fine
        public String FRTyreCondition;  //worst
        public String RLTyreCondition;  //worst
        public String RRTyreCondition;  //worst
        public String RemainingFLTyreLife;  //2
        public String RemainingFRTyreLife;  //2
        public String RemainingRLTyreLife;  //2
        public String RemainingRRTyreLife;  //2
        public String NextFLTyreChangeDate; //2015-12-15 00:00:00
        public String NextFRTyreChangeDate; //2015-12-15 00:00:00
        public String NextRRTyreChangeDate; //2015-12-15 00:00:00
        public String NextRLTyreChangeDate; //2015-12-15 00:00:00
        public String CEATShoppeName;   //Dev

    }
    
    
    // help to map records fields into json apex object
    public static string  RecordToJsonObjectMapper(List<Vehicle_Service__c> vList,map<id,CS_Vehicle__c> vsV,map<id,CS_Customer__c> vsc){
    
    list<cls_customersDetails> cls_customersDetailsList=new list<cls_customersDetails>();
    
    for(Vehicle_Service__c v : vList){
    
    
    
      cls_customersDetails c=new cls_customersDetails();
       
       try{       
       c.Name=v.Name;
        c.Mobile=vsc.get(vsV.get(v.Vehicle__c).CS_Customer__c).Mobile__c;// endcustomers
        c.Email=vsc.get(vsV.get(v.Vehicle__c).CS_Customer__c).Email__c;// endcustomers.
        c.VehicleRegistrationNo=vsV.get(v.Vehicle__c).Vehicle_Registration_Number_Old__c; // end customer vehicle. Vehicle_Registration_Number_Old__c
        c.VehicleMake=vsV.get(v.Vehicle__c).Make__c ;//end customer vehicle. Make__c
        c.VehicleModel=vsV.get(v.Vehicle__c).Model__c;//end customer vehicle. Model__c
        c.NextWheelAlignmentDate=string.valueof(vsc.get(vsV.get(v.Vehicle__c).CS_Customer__c).Next_Wheel_Alignment__c);//endcustomers. 
        c.NextBlancingDate=string.valueof(vsc.get(vsV.get(v.Vehicle__c).CS_Customer__c).Next_Balancing_Date__c);// endcustomers.Next_Balancing_Date__c;
        c.LastWheelAlignmentDate=string.valueof(vsV.get(v.Vehicle__c).Last_Alignment__c);//end customer vehicle. Last_Alignment__c
        c.LastBlancingDate=string.valueof(vsV.get(v.Vehicle__c).Last_Balancing_Change__c);// end customer vehicle.Last_Balancing_Change__c
        c.Alignment=String.valueof(v.Alignment__c);
        c.Balancing=String.valueof(v.Balancing__c);
        c.NitrogenInflation=String.valueof(v.Nitrogen_Inflation__c);
        c.TyresChanged=v.Number_of_Tyres_Changed__c;
        c.FLTyreCondition=v.FL_Tyre_Condition__c;
        c.FRTyreCondition=v.FR_Tyre_Condition__c;
        c.RLTyreCondition=v.RL_Tyre_Condition__c;
        c.RRTyreCondition=v.RR_Tyre_Condition__c;
        c.RemainingFLTyreLife=String.valueof(v.FL_Tyre_Health__c);
        c.RemainingFRTyreLife=String.valueof(v.FR_Tyre_Health__c);
        c.RemainingRLTyreLife=String.valueof(v.RL_Tyre_Health__c);
        c.RemainingRRTyreLife=String.valueof(v.RR_Tyre_Health__c);
        c.NextFLTyreChangeDate=String.valueof(v.FL_Tyre_Change_Date__c);
        c.NextFRTyreChangeDate=String.valueof(v.FR_Tyre_Change_Date__c);
        c.NextRRTyreChangeDate=String.valueof(v.RR_Tyre_Change_Date__c);
        c.NextRLTyreChangeDate=String.valueof(v.RL_Tyre_Change_Date__c);
        c.CEATShoppeName=vsc.get(vsV.get(v.Vehicle__c).CS_Customer__c).Ceat_Shoppe__c;
       
       }catch(exception e){
         system.debug('hHHHHHHHHHHH'+e.getmessage());
       } 
      
       if(c.name!=null)
        cls_customersDetailsList.add(c);
     }// end of for 
      
      
      
      system.debug(System.JSON.serialize(cls_customersDetailsList)+'UUUUUUUUUUUUUUUUU');
      
    return  System.JSON.serialize(cls_customersDetailsList);
    }
    
     
    
    public static VehicleServiceHelp parse(String json){
        return (VehicleServiceHelp) System.JSON.deserialize(json, VehicleServiceHelp.class);
    }

}