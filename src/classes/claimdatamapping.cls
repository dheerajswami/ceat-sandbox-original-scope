Public class claimdatamapping{
        
     public void ClaimtoSap()
     {
       
        
        Claimdatatosapmcs.TableOfZsdcl0 toZsdct = new Claimdatatosapmcs.TableOfZsdcl0();
        Claimdatatosapmcs.Zsdcl0 toZsdc = new Claimdatatosapmcs.Zsdcl0();
       toZsdc.Mandt = '300';
        toZsdc.Docket = 'C-00000175';
        toZsdc.DealerSapCode = '0050002439';
        toZsdc.DealerName ='AMIT TYRES';
            toZsdc.CustomerName ='RAJENDHAR';
            toZsdc.ClaimType = 'ZN';
            
            toZsdc.Street1 = 'ADINAD';
            toZsdc.Street2 = 'KOLLAM';
            toZsdc.Telephone = '9004997001';
            toZsdc.EMailAddress ='AMIT.TYRE@GMAIL.COM';
            toZsdc.MaterialSize ='100/9018';
            toZsdc.TyrePattern = 'LOOSE';
            toZsdc.MaterialNumber = '000000000000100400';
            toZsdc.MaterialDescription = 'BR 1000-20';
            toZsdc.Fitment = 'R';
            toZsdc.VehicleModel = 'HONDA';
            toZsdc.YearOfManufacture = '2015';
            toZsdc.VehicleKilometer = '1234';
            toZsdc.Chassis = '64647477';
            toZsdc.Disposition = 'ACCEPTTY';
            toZsdc.WearPercentage = '56.03';
            toZsdc.DefectType = '031';
            toZsdc.InspectedBy = 'RAKESH';
            toZsdc.InspectionDate = '2016-04-21';
            toZsdc.ActualNsd = '2.03';
            toZsdc.NbpPrice = '5512.0';
            //toZsdc.Related_number =  '123456';
            toZsdc.State = 'MAHARASTRA';
            toZsdc.District = 'THANE';
            toZsdc.Town = 'MUMBAI';
           // toZsdc.Material_group2 = '2500';
            toZsdc.Rimsize = '17';
            toZsdc.PlyRating ='40';
            toZsdc.TyreType = 'tl';
            toZsdc.MateialGroup = '2500';
            toZsdc.MaterialGroupDescription ='TRUCK TUBES';
            toZsdc.SlNo='12341212';
            toZsdc.Date1='0561';
        
        toZsdct.item.add(toZsdc);
        
        SAPLogin__c saplogin = SAPLogin__c.getValues('SAP Login');
        String username= saplogin.username__c;
        String password= saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        Claimdatatosapmcs sapclaim = new Claimdatatosapmcs();
        
        Claimdatatosapmcs.ZWS_GET_CL0_DATA  zws = new Claimdatatosapmcs.ZWS_GET_CL0_DATA();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 60000;
        //Claimdatatosapmcs.TableOfZsdcl0 ItInput = toZsdc;//new sapComDocumentSapSoapFunctionsMcS.TableOfZsdcl0();        
        Claimdatatosapmcs.ZsdGetCl0DataResponse_element  sapClaimResponseElement = new  Claimdatatosapmcs.ZsdGetCl0DataResponse_element();
        sapClaimResponseElement = zws.ZsdGetCl0Data(toZsdct);
        system.debug('Final Webservice Execution Result======'+sapClaimResponseElement.RETURN_x);    
    }    
}