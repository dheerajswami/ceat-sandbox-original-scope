global class StockVisibilityController{
    
    public String currents {get;set;}
    public String intransit {get;set;}   
    public String intransit1 {get;set;}
    public String mustdisp{get;set;}
    public String mustdisp1 {get;set;}
    public String mustdisp2 {get;set;}
    
    public Static id userId      {get;set;}  
    public Static string selectedPlant{get;set;}    
    public Static string selectedrecordType{get;set;}
    public Static string selectedCategory{get;set;}
    
    global StockVisibilityController(){
        
        userId            = ApexPages.currentPage().getParameters().get('Id');
        selectedPlant     = ApexPages.currentPage().getParameters().get('plantType');
        selectedrecordType= ApexPages.currentPage().getParameters().get('recordtype');
        selectedCategory  = ApexPages.currentPage().getParameters().get('category');
        List<Stocks__c> stockList = new List<Stocks__c>();
        Id stockheaderID = [SELECT id,DeveloperName FROM RecordType where DeveloperName = 'Stock_Header'].Id;
        if(selectedrecordType == 'PULL'){
            stockList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                         RecordTypeId,RType__c,ZYHCF_D__c FROM Stocks__c Where RecordTypeId =: stockheaderID and RType__c = 'P' AND ZYHCF_D__c =:selectedPlant];
        }
        else{
            stockList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                         RecordTypeId,RType__c,ZYHCF_D__c FROM Stocks__c Where RecordTypeId =: stockheaderID and RType__c = 'NP' AND ZYHCF_D__c =:selectedPlant];
        } 
        system.debug('Selectedpll '+selectedPlant);
        system.debug('headerID'+stockheaderID );
        system.debug('stocklist'+stockList);            
        
        
        if(stockList[0].Currents__c != null){
        currents = stockList[0].Currents__c;
        }else{
        currents = 'Not Available';
        }
        if(stockList[0].Intransit__c != null){
        intransit = stockList[0].Intransit__c;
        }
        else{
        intransit = 'Not Avaiable';
        }
        if(stockList[0].Intransit1__c !=null){
        intransit1 = stockList[0].Intransit1__c;
        }else{
        intransit1 = 'Not Available';
        }
        if(stockList[0].Mustdisp__c!=null){
        mustdisp = stockList[0].Mustdisp__c;
        }else{
        mustdisp ='Not Available';
        }
        if(stockList[0].Mustdisp_1__c!=null){
        mustdisp1 = stockList[0].Mustdisp_1__c;
        }else{
        mustdisp1 = 'Not Available';
        }
        if(stockList[0].Mustdisp_2__c!=null){
        mustdisp2 = stockList[0].Mustdisp_2__c;
        }else{
        mustdisp2 = 'Not Available';
        }   
        
        
    }
    
    
    @RemoteAction
    global static List<Stocks__c> getPlantListToDisplay(Id uID,String recType,String plantMultiselect,String categoryMultiselect, String skuValue){
		skuValue = '%'+skuValue+'%';
    	system.debug('thisissku '+skuValue);
        system.debug('id...'+uID+'record...'+recType+'plant....'+plantMultiselect+'category...'+categoryMultiselect);
        List<String> plant = new List<String>();
        List<String> category = new List<String>();
        if(plantMultiselect !='None' || plantMultiselect !='' || plantMultiselect !='null'){ 
            plant = plantMultiselect.split(',');
        }
        if(categoryMultiselect !='None' || categoryMultiselect !='' || categoryMultiselect != 'null'){
            category = categoryMultiselect.split(',');
        }
        system.debug('plant size'+plant.size()+'...'+plant+'..category size'+category.size()+'...'+category[0]);
        String recTypeLabel = '';
        if(recType == 'PULL'){
            recTypeLabel = 'Pull';
        }
        else{
            recTypeLabel = 'Non_Pull';
        }
        Id usrProfileId = [SELECT Id,ProfileId FROM User where Id =:uID].ProfileId;
        String usrRole = [SELECT id,name,FirstName,UserRole.Name From User where Id =:uID].UserRole.Name;
        String profileName = [SELECT Id,Name FROM Profile where Id =: usrProfileId].Name;
        List<Stocks__c> plantList = new List<Stocks__c>();
        
        if(usrRole.contains('RM') || usrRole.contains('RBM') || usrRole.contains('TL') || usrRole.contains('CSTL') ){
            Id selectedRecordTypeID = [SELECT id,DeveloperName FROM RecordType where DeveloperName =:recTypeLabel ].id;
            if(plant.size() >0 && category[0] == 'null'){
                system.debug('inside 1');
                plantList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,
                             Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                             RecordTypeId,Zyhcf__c,Zyhdc__c,Zyhrf__c,MaterialNumber__c FROM Stocks__c 
                             where RecordTypeId =: selectedRecordTypeID AND Zyhcf__c =: plant  
                             AND MaterialNumber__c LIKE:skuValue  order by Category__c,MaterialNumber__c asc];
            }
            else if(plant.size() > 0 && category.size()>0 && category[0]!= 'null'){
                system.debug('inside 2');
                plantList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,
                             Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                             RecordTypeId,Zyhcf__c,Zyhdc__c,Zyhrf__c,MaterialNumber__c FROM Stocks__c 
                             where RecordTypeId =: selectedRecordTypeID AND Category__c =: category 
                             AND Zyhcf__c =: plant  
                             AND MaterialNumber__c LIKE:skuValue order by Category__c,MaterialNumber__c asc];
            }
            
        }
        else{
            Id selectedRecordTypeID = [SELECT Id,DeveloperName FROM RecordType where DeveloperName =:recTypeLabel ].Id;
            if(plant.size() > 0 && category[0] == 'null'){
                plantList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,
                             Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                             RecordTypeId,Zyhcf__c,Zyhdc__c,Zyhrf__c,MaterialNumber__c FROM Stocks__c 
                             where RecordTypeId =: selectedRecordTypeID AND Zyhcf__c =: plant  
                             AND MaterialNumber__c LIKE:skuValue order by Category__c,MaterialNumber__c asc];
                
            }
            
            else if(plant.size() >0  && category.size()>0 && category[0]!= 'null'){
                plantList = [SELECT Category__c,Currents__c,Intransit1__c,Intransit__c,
                             Maktx__c,Matnr__c,Mustdisp_1__c,Mustdisp_2__c,Mustdisp__c,
                             RecordTypeId,Zyhcf__c,Zyhdc__c,Zyhrf__c,MaterialNumber__c FROM Stocks__c 
                             where RecordTypeId =: selectedRecordTypeID AND Category__c =: category 
                             AND Zyhcf__c =: plant  
                             AND MaterialNumber__c LIKE:skuValue order by Category__c,MaterialNumber__c asc];
            }
        }
        
       
        return plantList ;
    }
    
}