public class SalesPlanningController{	
	
    public static PageReference page {get;set;}
    
    public SalesPlanningController(){ }
    @RemoteAction
    Public Static PageReference getpage() { 
    	 
    	 //--------------- Profiles ------------   	 
          String replacmentProfile 		= system.label.Replacement_Profile;
          String specialtyProfile 		= system.label.Specialty;
          String exportProfile 			= system.label.Export_Profile;
          String specialtyOeProfile 	= system.label.Specialty_OE; 
          
           //--------------- Pages ------------  
          String rmPageRep				= system.label.Sales_Planning_RM;
          String tlPageRep 				= system.label.Sales_Planning_TL;
          String rmPagespe 				= system.label.Sales_Planning_Sp_RM_Page;
          String rmPageSpOe 			= system.label.Specialty_OEPage;
          String rmPageexports  	    = system.label.ExportPage; 
          
           //--------------- Roles ------------  
          String tlRole					= system.label.TL_Role;
          String rmRole 				= system.label.RM_Role;
          String tldRole				= system.label.TLD_Role;
          String rmRoleSpecilaty		= system.label.Specialty_RM_Role;
          String amRoleSpecilaty		= system.label.AM_SpecialtyURole;
          String managerExports			= system.label.ExportSrManager;
          String managerSpOe			= system.label.ExportSrManager;          
           
          String loggedInUserId ;          
          
         //Getting logged in user Id
         loggedInUserId=UserInfo.getUserId();
         
         //Querying LoggedIn user Role
         User userRole=  [select ProfileId,name,id,email,UserRole.DeveloperName from User where id=:loggedInUserId];
         
         List<UserRole> userRMRol  = [SELECT DeveloperName,Id FROM UserRole WHERE (DeveloperName  like 'RM%') AND (NOT(DeveloperName  like  '%Specialty')) AND (NOT(DeveloperName  like  '%OE'))];//DeveloperName =: rmLabel]; 
         List<UserRole> userTLRol  = [SELECT DeveloperName,Id FROM UserRole WHERE (DeveloperName  like 'TL%') AND (NOT(DeveloperName  like  '%Specialty')) AND (NOT(DeveloperName  like  '%OE'))];//DeveloperName =: tlLabel];
        
         String role='';
         for(UserRole rl :userRMRol){
        	if(UserInfo.getUserRoleId() == rl.id)
        	role = rl.id;
         }
         String roleTL='';
         for(UserRole rl1 :userTLRol){
        	if(UserInfo.getUserRoleId() == rl1.id)
        	roleTL = rl1.id;
         }
         
         //Getting profile for loggedIn User
         String profileName=[Select Id,Name from Profile where Id=:userRole.profileId].Name;
        
         //*************************Replacement****************************************
         
         if(profileName==replacmentProfile){
	       
	         //If loggedin user is RM, it should redirect to Sales planning RM page
	        
	         if(role!=null && role!=''){
	         	page = new PageReference('/apex/'+rmPageRep);
				page.setRedirect(true);         	
	         }
	         
	         //If loggedin user is TL/TLD, it should redirect to Sales planning TL page
	         
	         else if((roleTL!=null && roleTL!='') || userRole.UserRole.DeveloperName == tldRole){
	         	page = new PageReference('/apex/'+tlPageRep);
				page.setRedirect(true);         	
	            	
	         }
	         //else It should redirect to home page
	         
	         else{
	         	page = new PageReference('/home/home.jsp');
				page.setRedirect(true);  
	         }
         }
         
         //*************************Specialty****************************************
         
         else if(profileName==specialtyProfile){
         	
         	 //If loggedin user is RM/SRM/AM, it should redirect to Sales planning RM page
	        
	         if(userRole.UserRole.DeveloperName == rmRoleSpecilaty || userRole.UserRole.DeveloperName == amRoleSpecilaty){
	         	page = new PageReference('/apex/'+rmPagespe);
				page.setRedirect(true);         	
	         }
	         //else It should redirect to home page
	         
	         else{
	         	page = new PageReference('/home/home.jsp');
				page.setRedirect(true);  
	         }
         }
         
         //*************************Exports****************************************
         
         else if(profileName==exportProfile){
         	
         	//If loggedin user is RM, it should redirect to Sales planning RM page
	         
	         if(userRole.UserRole.DeveloperName == managerExports){
	         	page = new PageReference('/apex/'+rmPageexports);
				page.setRedirect(true);         	
	         }
	         
	         //else It should redirect to home page
	         else{
	         	page = new PageReference('/home/home.jsp');
				page.setRedirect(true);  
	         }
         } 
         /*
         //*************************Specialty_OE****************************************
         
         else if(profileName==specialtyOeProfile){
         	
         	
         	 //If loggedin user is RM, it should redirect to Sales planning RM page
	        
	         if(userRole.UserRole.DeveloperName == rmRoleSpecilaty || userRole.UserRole.DeveloperName == amRoleSpecilaty){
	         	page = new PageReference('/apex/'+rmPagespe);
				page.setRedirect(true);         	
	         }
	         //else It should redirect to home page
	         
	         else{
	         	page = new PageReference('/home/home.jsp');
				page.setRedirect(true);  
	         }
         }*/
         
         //************************Admin**************************************
         
         else{
         	page = new PageReference('/home/home.jsp');
		    page.setRedirect(true); 
         }
        
         return page;
    }
}