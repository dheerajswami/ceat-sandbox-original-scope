global with sharing class Claim_Cl0DataClassV2 {
    //public Id tmpId{get; set;}
    public String noOfParam{get; set;}
    public Claim__c newClaim {get;set;}
    public Case caseRecord {get;set;}
    public List<Material_Master_Sap__c> materialMaster {get;set;}
    public Set<String> materialTypeSet {get;set;}
    public Set<String> tyrePatternSet {get;set;}
    public Set<String> materialSizeSet {get;set;} 
    public String customerName {get; set;}
    public Id customerId {get; set;} 
    public List<SelectOption> yearOption {get; set;}
    public List<SelectOption> weekOption {get; set;}
    public  List<SelectOption> monthOption {get; set;}
    public List<SelectOption> defectOptions {get; set;}
    public List<SelectOption> dispositionOption {get; set;}
    public Set<SelectOption> productOption {get;set;}
    public List<SelectOption> plantCodeOption {get;set;}  
    public Material_Master_Sap__c materialMasterRecord {get;set;}
    public boolean hasComplaint = false;   
    public String actualInspectionDate {get; set;}
    public String outdoorInspectionDate {get; set;}

    public Claim_Cl0DataClassV2() {
        /*Id accountId;
        Id complaintId;
        String noOfParam = ApexPages.currentPage().getParameters().get('noOfParam');
        System.debug('==##11 '+Id.valueOf(ApexPages.currentPage().getParameters().get('Id')).getSobjectType());
        System.debug('==##3 '+noOfParam);
        if(noOfParam == '"5"') {
            complaintId = ApexPages.currentPage().getParameters().get('Id');
            accountId   = ApexPages.currentPage().getParameters().get('accountId');
            caseRecord = [SELECT Percentage_Wear__c,Actual_Inspection_Date__c,
                        Customer_Email__c,Customer_Phone__c,Disposition__c,
                        Material__c,NSD__c,Product__c,Serial_Number__c,Inspection__c,
                        Tyre_Pattern__c,Tyre_Size__c,Tyre_Type__c,Defect_Type__c,
                        Replacement_Amount__c FROM Case WHERE Id =: complaintId LIMIT 1];


            String matNumwithZeros = UtilityClass.addleadingZeros(caseRecord.Material__c, 18);
            System.debug('==##5 '+matNumwithZeros);
            materialMasterRecord = [SELECT Id,Name,Material_Number__c,Original_NSD__c,NBP__c,Mat_Grp_Desc__c FROM Material_Master_Sap__c WHERE Material_Number__c =: matNumwithZeros LIMIT 1];
        }else if(noOfParam == '"3"') {
            accountId   = ApexPages.currentPage().getParameters().get('Id');
        }
        System.debug('==##1 '+complaintId);
        caseRecord = [SELECT Percentage_Wear__c,Actual_Inspection_Date__c,
                        Customer_Email__c,Customer_Phone__c,Disposition__c,
                        Material__c,NSD__c,Product__c,Serial_Number__c,Inspection__c,
                        Tyre_Pattern__c,Tyre_Size__c,Tyre_Type__c,Defect_Type__c,
                        Replacement_Amount__c FROM Case WHERE Id =: complaintId LIMIT 1];

        //System.debug('==##12'+caseRecord);
        if(caseRecord != null) {
            hasComplaint = true;
            if(caseRecord.Actual_Inspection_Date__c != null) {
                actualInspectionDate = (caseRecord.Actual_Inspection_Date__c).format();
                System.debug('==##2 '+actualInspectionDate);
            }
        }*/
        Id recordId = ApexPages.currentPage().getParameters().get('Id');
        if(UtilityClass.getSObjType(recordId) == 'Claim__c') {
            newClaim = [SELECT Percentage_Wear__c,Id,Chassis_No__c,Claim_Type__c,Complaint__c,
                        Customer_Name__c,Defect_Type__c,Disposition__c,District__c,Customer_Email__c,
                        Fitment__c,Material_Code__c,Material_Pattern__c,Material_Size__c,Material_Type__c,
                        Customer_Mobile__c,NSD__c,Part_Replacement_on_Spot__c,Postal_Code__c,Dealer__c,
                        Dealer__r.Name,Dealer__r.Id,Plant__c,S_NO__c,State__c,Street_1__c,Street_2__c,Town__c,Vehicle_Kms__c,Make_Model__c,
                        Out_Door_Inspection_Slip_Date__c , Out_Door_Inspection_Slip_Number__c, Year_of_Mfg__c,Week__c,Month__c,Inspection__c,Actual_Inspection_Date__c FROM Claim__c WHERE Id =: recordId];
            System.debug('===##1 '+newClaim.Dealer__r.Name);
            if(newClaim.Out_Door_Inspection_Slip_Date__c == null){
            	outdoorInspectionDate = 'dd/mm/yy';
            }else{
				outdoorInspectionDate = (newClaim.Out_Door_Inspection_Slip_Date__c).format();
            }
         	if(newClaim.Out_Door_Inspection_Slip_Date__c == null){
            	actualInspectionDate = 'dd/mm/yy';
         	}else{
         		actualInspectionDate =(newClaim.Actual_Inspection_Date__c).format();
         	}

        }

        monthOption = UtilityClass.populateMonthOptions();
        weekOption = UtilityClass.populateWeek();
        populatePlantCode();
        populateProduct();
        populateYear();
        populateDefect();
        populateDisposition();
    }

    public void populatePlantCode() {
        plantCodeOption = new List<SelectOption>();
        plantCodeOption.add(new SelectOption('','--None--'));
         for(Plant__c tmpPlant: [Select Name from Plant__c where Name!=null order by Name]) {
          plantCodeOption.add(new SelectOption(tmpPlant.Name,tmpPlant.Name));
         }
    }

    public void populateProduct() {
        productOption = new Set<SelectOption>();
        productOption.add(new SelectOption('','--None--'));
        for(Material_Master_Sap__c s:[Select Mat_Grp2_Desc__c,Material_Type__c from Material_Master_Sap__c where Material_Type__c!=null order by Material_Type__c asc  ]){
            productOption.add(new SelectOption(s.Material_Type__c,s.Material_Type__c)); 
        }
    }

    public void populateYear() {
        yearOption = new List<SelectOption>();
        yearOption.add(new SelectOption('','--None--'));
        Integer curYear = System.today().year();
        for(Integer i=2000;i<=curYear;i++){
            yearOption.add(new SelectOption((String.ValueOf(i)).substring(2,4),String.ValueOf(i)));

        }
    }

    public void populateDefect() {
        defectOptions = new List<SelectOption>();
        Map<Id, Defect_Type__c> idToDefectMap = new Map<Id, Defect_Type__c>([SELECT Id, Name FROM Defect_Type__c]);
        /*if(hasComplaint && caseRecord.Inspection__c == 'On Spot') {
            defectOptions.add(new SelectOption(caseRecord.Defect_Type__c, caseRecord.Defect_Type__c));
        }else {*/
           defectOptions.add(new SelectOption('','--None--')); 
        //}
        for(Id tmpId : idToDefectMap.keySet()) {
            defectOptions.add(new SelectOption(idToDefectMap.get(tmpId).name,idToDefectMap.get(tmpId).name));
        }
    }
 
    public void populateDisposition() {
        dispositionOption = new List<SelectOption>();
        Map<Id, Disposition__c> idToDispositionMap = new Map<Id, Disposition__c>([SELECT Id, Name FROM Disposition__c]);
        /*if(hasComplaint && caseRecord.Disposition__c != null) {
            dispositionOption.add(new SelectOption(caseRecord.Disposition__c, caseRecord.Disposition__c));
        }else {*/
            dispositionOption.add(new SelectOption('','--None--'));
        //}
        for(Id tmpId : idToDispositionMap.keySet()) {
            dispositionOption.add(new SelectOption(idToDispositionMap.get(tmpId).name,idToDispositionMap.get(tmpId).name));
        }
    }

    public List<selectOption> selectOptions(String sObjAPIName, String fieldName) {
        List<Schema.PicklistEntry> pick_list_values = UtilityClass.fetchRelatedPicklistOptions(sObjAPIName, fieldName);
        List<selectOption> options = new List<selectOption>();
        options.add(new SelectOption('','--None--'));
        for(Schema.PicklistEntry str : pick_list_values)
        {
            options.add(new selectOption(str.getValue(), str.getValue()));
        }
        return options;
    }

    public List<SelectOption> getStatesList(){
     
        List<SelectOption> options = new List<SelectOption>();        
        Schema.DescribeFieldResult fieldResult = Case.State_Val__c.getDescribe();
        options.add(new SelectOption('', '--None--'));
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }

    /**
     * pageRedirectUrl
     * @description A description of the function 
     * @param String paramName : param explanation
     * @return String retVal : return value explanation
     */

    @RemoteAction
    public static PageReference pageRedirectUrl(String recordId) {
        try {
            String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
            String sobjectType;
            String pageName;
            //System.debug('==#1 '+recordId);
            sobjectType = UtilityClass.getSObjType(recordId);
            System.debug('==#4 '+sobjectType);
            if(sobjectType == 'Account') {
                pageName = 'Claim_Cl0Data_Accounts';
                system.debug('==#5 Inside Accounts '+pageName);
            }else if(sobjectType == 'Case'){
                pageName = 'Claim_Cl0Data_Complaints';
            }
            PageReference pf = new PageReference(baseURL+'/apex/'+pageName+'?Id='+recordId);
            pf.setRedirect(true);
            return pf;
        }catch(Exception e) {
            //System.debug('==#1 Exception caused '+e.getMessage());
            return null;
        }

    }

    @RemoteAction
    public static string objectType(String recordId) {
        try {
            String sobjectType;
            sobjectType = UtilityClass.getSObjType(recordId);
            system.debug('showVal '+sobjectType);
            return sobjectType;
        }catch(Exception e) {
            //System.debug('==#1 Exception caused '+e.getMessage());
            return null;
        }
    }

    @RemoteAction
    global static List<SelectOption> linkedDistricts(String strState){    
        List<SelectOption> options = new List<SelectOption>();
        Set<String> disSet=new Set<String>();
        //options.add(new SelectOption('','--None--')); 
        for(State_Master__c s:[Select District__c,State__c,Town__c from State_Master__c where State__c=:strState order by District__c asc]){
            if(!disSet.contains(s.District__c)){
              options.add(new SelectOption(s.District__c,s.District__c));
                disSet.add(s.District__c);
            }
        }       
        System.debug('==#1 '+options);
        return options;
    }

    @RemoteAction
    global static List<Account> searchAccountsOEorSpec(String searchTerm) {
        System.debug('==#100 '+searchTerm );
        //System.debug('===#300 '+Acctype);
        Id oePlantRecordTypeId = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'OE'].Id;
        //String accType = 'OE Plant';
        List<Account> accounts  = Database.query('Select Id, Name,KUNNR__c,Type,Sales_Office_Text__c from Account where (RecordTypeId =:oePlantRecordTypeId)  AND KUNNR__C != null AND  (Name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'OR KUNNR__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\') ORDER BY KUNNR__c LIMIT 10');
        return accounts;
    } 

    @RemoteAction
    global static List<SelectOption> linkedTowns(String strDistrict){    
        List<SelectOption> options = new List<SelectOption>();
        //Set<String> townSet=new Set<String>(); 
        for(State_Master__c s:[Select District__c,State__c,Town__c from State_Master__c where District__c=:strDistrict order by Town__c asc]){                
            options.add(new SelectOption(s.Town__c,s.Town__c));  
        }        
        return options;
    }

    @RemoteAction
    global static List<SelectOption> linkedMaterialSizes(String matType){    
        List<SelectOption> options = new List<SelectOption>();
        //Set<String> townSet=new Set<String>(); 
        options.add(new SelectOption('--None--','')); 
        for(Material_Master_Sap__c s:[Select Id, Name, Material_Number__c, Mat_Grp2_Desc__c, Pattern_Brand__c, Tyre_Size__c from Material_Master_Sap__c where Mat_Grp2_Desc__c=:matType AND Tyre_Size__c != null]){                
            options.add(new SelectOption(s.Tyre_Size__c,s.Tyre_Size__c));  
        }        
        return options;
    }

    @RemoteAction
    global static List<SelectOption> linkedTyrePatterns(String matType){    
        List<SelectOption> options = new List<SelectOption>();
        //Set<String> townSet=new Set<String>(); 
        options.add(new SelectOption('--None--','--None--')); 
        for(Material_Master_Sap__c s:[Select Id, Name, Material_Number__c, Mat_Grp2_Desc__c, Pattern_Brand__c, Tyre_Size__c from Material_Master_Sap__c where Mat_Grp2_Desc__c=:matType AND Pattern_Brand__c != null]){                
            options.add(new SelectOption(s.Pattern_Brand__c,s.Pattern_Brand__c));  
        }        
        return options;
    }

    @RemoteAction
     global static Set<SelectOption> tyreSizeList(String tyreType){
      Set<SelectOption> tyrSizeList = UtilityClass.fetchTyreSize(tyreType);
        /*tyrSizeList.add(new SelectOption('--None--','---None---'));*/
     /*    for(Material_Master_Sap__c tyrSize: [Select Tyre_Size__c,Mat_Grp2_Desc__c from Material_Master_Sap__c where Mat_Grp2_Desc__c =: category and Tyre_Size__c !=null order by Tyre_Size__c asc]){
           tyrSizeList.add(new SelectOption(tyrSize.Tyre_Size__c,tyrSize.Tyre_Size__c));
     }*/
     return tyrSizeList;
    } 
   
    @RemoteAction
     global static Set<SelectOption> tyrPatternList(String tySize){
      Set<SelectOption> tyrPatternList = UtilityClass.fetchSKUName(tySize);
         /*for(Material_Master_Sap__c tyrSize: [Select Tyre_Size__c,Pattern_Brand__c from Material_Master_Sap__c where Tyre_Size__c =: tySize and Tyre_Size__c !=null order by Tyre_Size__c asc]){
           tyrPatternList.add(new SelectOption(tyrSize.Pattern_Brand__c,tyrSize.Pattern_Brand__c));
     }*/

     return tyrPatternList;
    }

    @RemoteAction
    global static List<Material_Master_Sap__c> searchmaterialNum(String pattern,String searchTerm){
      List<Material_Master_Sap__c> matReturnList = new List<Material_Master_Sap__c>();
       List<Material_Master_Sap__c> materialMasterList = Database.query('Select id,Name,Material_Number__c,Original_NSD__c,NBP__c,Mat_Grp_Desc__c From Material_Master_Sap__c where Pattern_Brand__c=: pattern AND (Material_Number__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\') ORDER BY Material_Number__c LIMIT 10');
       system.debug('==#400'+materialMasterList);
       for(Material_Master_Sap__c tmpMaster:materialMasterList){
          tmpMaster.Material_Number__c = (tmpMaster.Material_Number__c.startsWith('0')?String.valueOf(Integer.valueOf(tmpMaster.Material_Number__c)):tmpMaster.Material_Number__c);
          matReturnList.add(tmpMaster);
       }
          System.debug('==== '+matReturnList);
          return matReturnList;     
    }

    public List<selectOption> getClaimTypes() {
        return selectOptions('Claim__c', 'Claim_Type__c');  
    }

    public List<selectOption> getFitments() {
        return selectOptions('Claim__c', 'Fitment__c'); 
    }



    @RemoteAction
    global static Account getDealerDetails(Id dealerId) {
        Account gotDealer = [Select Id, Name, State__c, City__c, Town__c, District__c, PIN_code__c, Kunnr__c from Account where Id = :dealerId];
        return gotDealer;
    }

    /**
     * fetchProductDetails
     * @description A description of the function 
     * @param String paramName : param explanation
     * @return String retVal : return value explanation
     */

     @RemoteAction
    global static Material_Master_Sap__c fetchProductDetails(String matDesc) {
        try {

            Material_Master_Sap__c materialMaster = [SELECT Material_Number__c,NBP__c,Original_NSD__c,Mat_Grp_Desc__c,Pattern_Brand__c,Ply_Rating__c,Rim_Size__c,Tyre_Type__c FROM Material_Master_Sap__c WHERE name =: matDesc AND Flag__c = true];
            return materialMaster;
        }catch(Exception e) {
            return null;
        }
    }
    

    /**
     * generateSerialNumber
     * @description A description of the function 
     * @param String paramName : param explanation
     * @return String retVal : return value explanation
     */
    public String generateSerialNumber(String plantCode, String slNo, String year, String month, String week) {
        String retVal = plantCode + slNo + week + month +year;
        
        return retVal;
    }
    
    @RemoteAction
    public static PageReference saveClaim(Claim__c newClaim,String actual_inspectionDate, string outdoorInspectionDate) { 
        System.debug('==#2insdate' +actual_inspectionDate);
        Savepoint sp = Database.setSavepoint();
        try{ 
            String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
            //System.debug('==#1 '+newClaim);
            Claim__c tmpclaim = new Claim__c();
            Material_Master_Sap__c materialRecord = new Material_Master_Sap__c();
            materialRecord = fetchProductDetails(newClaim.Material_Pattern__c);
            /*CEAT_ExceptionHandler exptnHandler = new CEAT_ExceptionHandler();
            exptnHandler.validateclaimFields(newClaim);*/
            tmpclaim = newClaim;

            if(actual_inspectionDate !=''){
            	tmpclaim.Actual_Inspection_Date__c = Date.parse(actual_inspectionDate); 
            }else{
            	tmpclaim.Actual_Inspection_Date__c = null;
            }
            
            if(outdoorInspectionDate !=''){
            	tmpclaim.Out_Door_Inspection_Slip_Date__c = Date.parse(outdoorInspectionDate); 
            }else{
            	tmpclaim.Out_Door_Inspection_Slip_Date__c = null;
            }
            tmpclaim.Serial_Number__c = tmpclaim.Plant__c + tmpclaim.S_NO__c + tmpclaim.Week__c + tmpclaim.Month__c +tmpclaim.Year_of_Mfg__c;
            
            // Adding material details to the record
            if(materialRecord != null) {
                tmpclaim.Rim_Size__c = materialRecord.Rim_Size__c;
                tmpclaim.Ply_Rating__c = materialRecord.Ply_Rating__c;
                tmpclaim.Tyre_Type__c = materialRecord.Tyre_Type__c; 
            }
            

            /*tmpCase.Plant__c        = tmpCase.plantCode;
            tmpCase.S_NO__c         = tmpCase.slNo;
            tmpCase.Week__c         = tmpCase.week;
            tmpCase.Month__c        = tmpCase.month;
            tmpCase.Year__c         = tmpCase.year;*/
           /* if(warrantyCardDate !=''){
            tmpclaim.Date_of_Warranty_Card__c = Date.parse(warrantyCardDate);
            }*/ 
            system.debug('showTempClaim'+tmpclaim);
            upsert tmpclaim;
            /*Case caseToUpdate = [SELECT Id, Claim_Raised__c FROM Case WHERE Id =: tmpclaim.Complaint__c];
            if(caseToUpdate !=  null) {
                caseToUpdate.Claim_Raised__c = true;
                update caseToUpdate;
            }*/
            
            PageReference pf = new PageReference(baseURL+'/'+tmpclaim.Id);
            pf.setRedirect(true);
            return pf;
            
            
        }catch(Exception exptn) {
            Database.rollback(sp);
            throw new customException(exptn.getMessage());
        }
    }




    public class customException extends Exception{}
}