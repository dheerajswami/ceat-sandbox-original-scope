global class scheduledBatch_DeleteActuals implements Schedulable{
   global void execute(SchedulableContext sc) {
      DeleteActuals_BatchClass actualBatch = new DeleteActuals_BatchClass(); 
      database.executebatch(actualBatch ,200);
   }
}