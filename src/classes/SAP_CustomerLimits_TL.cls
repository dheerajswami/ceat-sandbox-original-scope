//Generated by wsdl2apex

public class SAP_CustomerLimits_TL {
    public class TABLE_OF_ZBAPI_CUST_LIMIT_TERR {
        public SAP_CustomerLimits_TL.ZBAPI_CUST_LIMIT_TERR[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZBAPI_GET_CUST_LIMIT_TERResponse_element {
        public SAP_CustomerLimits_TL.TABLE_OF_ZBAPI_CUST_LIMIT_TERR IT_CUST;
        public SAP_CustomerLimits_TL.TABLE_OF_BAPIRETURN RETURN_x;
        private String[] IT_CUST_type_info = new String[]{'IT_CUST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'IT_CUST','RETURN_x'};
    }
    public class ZBAPI_GET_CUST_LIMIT_TER_element {
        public String BZIRK;
        public SAP_CustomerLimits_TL.TABLE_OF_ZBAPI_CUST_LIMIT_TERR IT_CUST;
        public SAP_CustomerLimits_TL.TABLE_OF_BAPIRETURN RETURN_x;
        private String[] BZIRK_type_info = new String[]{'BZIRK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] IT_CUST_type_info = new String[]{'IT_CUST','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] RETURN_x_type_info = new String[]{'RETURN','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'BZIRK','IT_CUST','RETURN_x'};
    }
    public class TABLE_OF_BAPIRETURN {
        public SAP_CustomerLimits_TL.BAPIRETURN[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:rfc:functions',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class BAPIRETURN {
        public String TYPE_x;
        public String CODE;
        public String MESSAGE;
        public String LOG_NO;
        public String LOG_MSG_NO;
        public String MESSAGE_V1;
        public String MESSAGE_V2;
        public String MESSAGE_V3;
        public String MESSAGE_V4;
        private String[] TYPE_x_type_info = new String[]{'TYPE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CODE_type_info = new String[]{'CODE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_type_info = new String[]{'MESSAGE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_NO_type_info = new String[]{'LOG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LOG_MSG_NO_type_info = new String[]{'LOG_MSG_NO','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V1_type_info = new String[]{'MESSAGE_V1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V2_type_info = new String[]{'MESSAGE_V2','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V3_type_info = new String[]{'MESSAGE_V3','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] MESSAGE_V4_type_info = new String[]{'MESSAGE_V4','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'TYPE_x','CODE','MESSAGE','LOG_NO','LOG_MSG_NO','MESSAGE_V1','MESSAGE_V2','MESSAGE_V3','MESSAGE_V4'};
    }
    public class ZBAPI_CUST_LIMIT_TERR {
        public String BZIRK;
        public String CUSTOMER;
        public String NAME1;
        public String DEPOSIT;
        public String PERMISBLEAMT;
        public String LIMITAVAILABLE;
        public String ADVANCE;
        public String TOTOUTSTAND;
        public String NETOUTSTAND;
        public String TOTOVERDUE;
        private String[] BZIRK_type_info = new String[]{'BZIRK','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] CUSTOMER_type_info = new String[]{'CUSTOMER','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] NAME1_type_info = new String[]{'NAME1','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] DEPOSIT_type_info = new String[]{'DEPOSIT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] PERMISBLEAMT_type_info = new String[]{'PERMISBLEAMT','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] LIMITAVAILABLE_type_info = new String[]{'LIMITAVAILABLE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] ADVANCE_type_info = new String[]{'ADVANCE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TOTOUTSTAND_type_info = new String[]{'TOTOUTSTAND','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] NETOUTSTAND_type_info = new String[]{'NETOUTSTAND','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] TOTOVERDUE_type_info = new String[]{'TOTOVERDUE','urn:sap-com:document:sap:rfc:functions',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions','false','true'};
        private String[] field_order_type_info = new String[]{'BZIRK','CUSTOMER','NAME1','DEPOSIT','PERMISBLEAMT','LIMITAVAILABLE','ADVANCE','TOTOUTSTAND','NETOUTSTAND','TOTOVERDUE'};
    }
    public class zws_get_cust_limit_ter {
        public String endpoint_x = 'http://ctecphap3.ceat.in:8000/sap/bc/srt/rfc/sap/zws_get_cust_limit_ter/300/zws_get_cust_limit_ter/zws_get_cust_limit_ter';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:rfc:functions', 'SAP_CustomerLimits_TL'};
        public SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TERResponse_element ZBAPI_GET_CUST_LIMIT_TER(String BZIRK,SAP_CustomerLimits_TL.TABLE_OF_ZBAPI_CUST_LIMIT_TERR IT_CUST,SAP_CustomerLimits_TL.TABLE_OF_BAPIRETURN RETURN_x) {
            SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TER_element request_x = new SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TER_element();
            request_x.BZIRK = BZIRK;
            request_x.IT_CUST = IT_CUST;
            request_x.RETURN_x = RETURN_x;
            SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TERResponse_element response_x;
            Map<String, SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TERResponse_element> response_map_x = new Map<String, SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TERResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:rfc:functions:zws_get_cust_limit_ter:ZBAPI_GET_CUST_LIMIT_TERRequest',
              'urn:sap-com:document:sap:rfc:functions',
              'ZBAPI_GET_CUST_LIMIT_TER',
              'urn:sap-com:document:sap:rfc:functions',
              'ZBAPI_GET_CUST_LIMIT_TERResponse',
              'SAP_CustomerLimits_TL.ZBAPI_GET_CUST_LIMIT_TERResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}