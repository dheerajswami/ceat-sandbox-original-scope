public class PjpNormsTriggerHandler {	
	//Activates newly created norm
	public static void activateNorm(List<PJP_Norms_Main__c> newNorms) {
		List<PJP_Norms_Main__c> updatedNorms = new List<PJP_Norms_Main__c>();
		for(PJP_Norms_Main__c newNorm : newNorms) {
			newNorm.Active__c = true;
			List<PJP_Norms_Main__c> relatedNorms = [Select Id, Name, Active__c from PJP_Norms_Main__c where RecordTypeId = :newNorm.RecordTypeId And Active__c = true And User_Type__c = :newNorm.User_Type__c];
			for(PJP_Norms_Main__c relNorm : relatedNorms) {
				relNorm.Active__c = false;				
				updatedNorms.add(relNorm);
			}			
		}	
		update updatedNorms;
	}
	//Validates updation of inactive norms
	public static void inActiveNorms(List<PJP_Norms_Main__c> newNorms, List<PJP_Norms_Main__c> oldNorms) {
		for(PJP_Norms_Main__c newNorm : newNorms) {
			for(PJP_Norms_Main__c oldNorm : oldNorms) {
				if(newNorm.Id == oldNorm.Id && newNorm.Active__c == false && oldNorm.Active__c == false) {
					newNorm.addError('Inactive Norms can not be edited.');
				}
			}
		}
	}
	//Activate last inactive norm on deletion of active norm
	public static void activateInactiveNorms(List<PJP_Norms_Main__c> oldNorms) {
		List<PJP_Norms_Main__c> updatedNorms = new List<PJP_Norms_Main__c>();		
		for(PJP_Norms_Main__c oldNorm : oldNorms) {
			if(oldNorm.Active__c == true) {				
				Integer max = 0;
				PJP_Norms_Main__c lastActiveNorm = new PJP_Norms_Main__c();
				List<PJP_Norms_Main__c> relatedNorms = [Select Id, Name, Active__c from PJP_Norms_Main__c where RecordTypeId = :oldNorm.RecordTypeId And User_Type__c = :oldNorm.User_Type__c And Id != :oldNorm.Id];
				for(PJP_Norms_Main__c relNorm : relatedNorms) {
					If(Integer.valueOf(relNorm.Name.split('-')[relNorm.Name.split('-').size()-1]) >= max) {
						max = Integer.valueOf(relNorm.Name.split('-')[relNorm.Name.split('-').size()-1]);
						lastActiveNorm = relNorm;                        
                        lastActiveNorm.Id = relNorm.Id;                        
					}
				}
                if(relatedNorms.size() > 0) {
                	lastActiveNorm.Active__c = true;                    
                    updatedNorms.add(lastActiveNorm);    
                }				
			}
		}        
		update updatedNorms;	
	}
}