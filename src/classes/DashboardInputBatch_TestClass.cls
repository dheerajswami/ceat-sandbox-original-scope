/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DashboardInputBatch_TestClass {
    
    static list<Id> terrTypeID                            = new list<Id>();
    static list<Id> terrId                                = new list<Id>();
    static list<Dashboard_Weightage_Master__c> dwm        = new List<Dashboard_Weightage_Master__c>();
    
    static String tlRole                = system.Label.TL_Role;
    static String tldRole               = system.Label.TLD_Role; 
    static String rec                   = system.Label.Input_RecordType;
    static String actionLogRecType      = system.Label.ActionLogInputRecordType;
    static String efleetRecType         = system.Label.Efleet_RecType;
    static string TLRecType             = system.Label.TL_RecType;
    static string TLInputRecType        = system.Label.Input_RecType;
    static string influencerRecType     = system.Label.Influencer_Visit_RecType;
    
    static void init(){
        
    }
    
    static testMethod void myUnitTest() {
         Date firstDayOfMonth = System.today().toStartOfMonth();
         Integer numberOfDays = Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month());
         Date lastDayOfMonth  = Date.newInstance(firstDayOfMonth.year(), firstDayOfMonth.month(), numberOfDays);
    
        User tlUser = CEAT_InitializeTestData.createUser('Ram','Ronak','ram.ronak@ceat.dev','ram.ronak@ceat.dev','Emp1','Replacements', null,'TL_Replacement');
        insert tlUser;
        
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory' limit 1];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId ];    
        list<UserTerritory2Association> terr = [select id,Territory2.developerName,Territory2Id,UserId from UserTerritory2Association where Territory2.developerName='B0001' AND RoleInTerritory2='TL_Replacement' limit 1];
        User u = [Select id, name from User where id = :terr[0].UserId limit 1];
  	    
        System.runAs(u) {
                RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer','Account');
            
                Account acc1 = CEAT_InitializeTestData.createAccount('Account1', 'C000000001', 'BHP', 'ZC01', 'B0001');
                //acc1.Sales_District_Text__c = 'B0001';    
                acc1.Type = 'Analyst';
                acc1.Customer_Segment__c='High Priority Dealer';
                acc1.RecordTypeId = rtDealer.Id;
                acc1.Business_Units__c='Replacement';
                acc1.Active__c = true;
                insert acc1;
                
                Account acc6 = CEAT_InitializeTestData.createAccount('Account6', 'C000000006', 'BHP', 'ZC01', 'B0001');
                //acc6.Sales_District_Text__c = 'B0001';    
                acc6.Type = 'Analyst';
                acc6.Customer_Segment__c='High Priority Dealer';
                acc6.RecordTypeId = rtDealer.Id;
                acc6.Business_Units__c='Replacement';
                acc6.Active__c = true;
                insert acc6;

                Account acc2 = CEAT_InitializeTestData.createAccount('Account2', 'C000000002', 'BHP', 'ZC03', 'B0003');
                acc2.Sales_District_Text__c = 'B0001';    
                acc2.Type = 'Analyst';
                acc2.Customer_Segment__c='Maintain Dealer';
                acc2.RecordTypeId = rtDealer.Id;
                acc2.Business_Units__c='Replacement';
                acc2.Active__c = true;
                insert acc2;
                
                //For Efleet
                Account acc3 = CEAT_InitializeTestData.createAccount('Account3', 'C000000033', 'BHP', 'ZC01', 'B0004');
                acc3.Sales_District_Text__c = 'B0001';    
                acc3.Type = 'Analyst';
                acc3.Customer_Segment__c='P1';
                acc3.RecordTypeId = rtDealer.Id;
                acc3.Business_Units__c='Replacement';
                acc3.Active__c = true;
                insert acc3;
               
                Account acc4 = CEAT_InitializeTestData.createAccount('Account4', 'C000000043', 'BHP', 'ZC01', 'B0005');
                acc4.Sales_District_Text__c = 'B0001';    
                acc4.Type = 'Analyst';
                acc4.Customer_Segment__c='P2';
                acc4.RecordTypeId = rtDealer.Id;
                acc4.Business_Units__c='Replacement';
                acc4.Active__c = true;
                insert acc4;
                
                Account acc5 = CEAT_InitializeTestData.createAccount('Account5', 'C000000044', 'BHP', 'ZC01', 'B0006');
                acc5.Sales_District_Text__c = 'B0001';    
                acc5.Type = 'Analyst';
                acc5.Customer_Segment__c='P3';
                acc5.RecordTypeId = rtDealer.Id;
                acc5.Active__c = true;
                acc5.Business_Units__c='Replacement';
                insert acc5;
                
                RecordType repRecTypeAl = [select name,id from RecordType where SObjectType='Task' AND DeveloperName='Replacement' limit 1];
                EmailTemplate validEmailTemplate = new EmailTemplate();
                validEmailTemplate.isActive = true;
                validEmailTemplate.Name = 'Action Log escalation notification';
                validEmailTemplate.DeveloperName = 'sneha1234';
                validEmailTemplate.TemplateType = 'text';
                validEmailTemplate.FolderId = UserInfo.getUserId();
            
                System.debug('==Id1 '+acc2.Id+'  Id2  '+acc6.Id);
                System.debug('==Id3 '+u.Id);
            	System.debug('==UserId '+tlUser.Id);

                insert validEmailTemplate;
                Server_url__c url=new Server_url__c(url__c='https://cs5.salesforce.com');
                insert url;
                Task actionLog1 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,tlUser.Id);
                actionLog1.Action_to_be_taken__c = 'MED/Distributor/Fleet appointment or revival of closed dealer';
                actionLog1.RecordTypeId=repRecTypeAl.Id;    
                actionLog1.ActivityDate  = date.today(); 
            	actionLog1.Description = '';
                
                Task actionLog2 =  CEAT_InitializeTestData.createSignleTasks(acc2.Id,tlUser.Id);
                actionLog2.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog2.RecordTypeId=repRecTypeAl.Id;
                actionLog2.ActivityDate  = date.today();    
                actionLog2.Description = '';
                Task actionLog3 =  CEAT_InitializeTestData.createSignleTasks(acc6.Id,tlUser.Id);
                actionLog3.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog3.RecordTypeId=repRecTypeAl.Id;
                actionLog3.status='Open';
                actionLog3.ActivityDate  = date.today();      
                actionLog3.Description = '';
                Task actionLog4 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,tlUser.Id);
                actionLog4.Action_to_be_taken__c = 'MED/Distributor/Fleet appointment or revival of closed dealer';
                actionLog4.RecordTypeId=repRecTypeAl.Id;
                actionLog4.status='Open';
                actionLog4.ActivityDate  = date.today();
            	actionLog4.Description = '';

                insert actionLog1;
                insert actionLog2;
                insert actionLog3;
                insert actionLog4;
				List<Task> actionLogList = [SELECT Id, Dealer__c, ownerId FROM Task];
            	System.debug('==## '+actionLogList);
                //Insert Dashboard Master
                Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true,Role__c='TL_Replacement');
                insert dm;
                
                
                Id repRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='PJP__c' and DeveloperName =: 'Replacement' Limit 1].Id;
                Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                
                Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
                // Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:efleetRecType Limit 1].Id;
                
                Dashboard_Master__c dmTemp       = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true];
                system.debug('dmTemp=='+dmTemp);
                
                list<sObject> dwmAl    = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterData');
                list<sObject> dwmPjp   = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataPJP');
                list<sObject> dwmEfleet  = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataEfleet');
                
                for(sObject dwm1 : dwmAl){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm1;
                    dwmTemp.RecordTypeId = actionLogRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    
                }
                update dwmAl;
                
                for(sObject dwm2 : dwmPjp){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm2;
                    dwmTemp.RecordTypeId = inputRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                }
                update dwmPjp;
                
                for(sObject dwm3 : dwmEfleet){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm3;
                    dwmTemp.RecordTypeId = efleetRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    //if(dwmTemp.category__c.trim()=='Customer - Data capture and  reporting'){
                      //system.debug('--------------'+dwmTemp.category__c);
                    //}
                }
                update dwmEfleet;
                
                //Insert Vehicle
                Vehicle__c vehicle = CEAT_InitializeTestData.createVehicle('appType','payload','truckType','12',acc1.id);
                insert vehicle;
                
                //Inserting TYRE Object Record
                Tyre__c tyreRecCeat = CEAT_InitializeTestData.createTyre('CEAT',1.1,'11','1','2','233',vehicle.Id);
                Tyre__c tyreRecNonCeat = CEAT_InitializeTestData.createTyre('MRF',1.2,'11','1','2','233',vehicle.Id);
                
                insert tyreRecCeat;
                insert tyreRecNonCeat;
                
                //Inserting Tyre_Inspection__c Object Record
                Tyre_Inspection__c tyreInsp    = CEAT_InitializeTestData.creatTyreInspection(tyreRecCeat.id,u.id);
                Tyre_Inspection__c tyreInspRec = CEAT_InitializeTestData.creatTyreInspection(tyreRecNonCeat.Id,u.id);
                
                insert tyreInsp;
                insert tyreInspRec;
                
                //PJP Record
                PJP__c PjpRec = CEAT_InitializeTestData.createPjpRecord(u.Id);
                PjpRec.RecordTypeId = repRecTypeId;
                insert pjpRec;
                
                
                //Influencer Visit
                String visit= 'Visit Day';
                Id influencerVisitRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Visits_Plan__c' and DeveloperName =:influencerRecType Limit 1].Id;
           
                //Inserting Visit Plans
                list<Visits_Plan__c> vpinfluencerList = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc1.Id,null,false);
                for(Visits_Plan__c vp:vpinfluencerList){
                   vp.RecordTypeId = influencerVisitRecTypeId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c=visit;
                }
                insert vpinfluencerList;
                
                //Inserting Visit Plans
                list<Visits_Plan__c> vpinfluencerList11 = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc2.Id,null,false);
                for(Visits_Plan__c vp:vpinfluencerList11){
                   vp.RecordTypeId = influencerVisitRecTypeId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c=visit;
                }
                insert vpinfluencerList11;
                
                //Inserting Visit Plans for P1 Customer
                list<Visits_Plan__c> vpinfluencerList1 = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc3.Id,null,false);
                for(Visits_Plan__c vp:vpinfluencerList1){
                  // vp.RecordTypeId = influencerVisitRecTypeId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c=visit;
                }
                insert vpinfluencerList1;
                
                //Inserting Visit Plans for P2 Customer
                list<Visits_Plan__c> vpinfluencerList2 = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc4.Id,null,false);
                for(Visits_Plan__c vp:vpinfluencerList2){
                   //vp.RecordTypeId = influencerVisitRecTypeId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c=visit;
                }
                insert vpinfluencerList2;
                
                //Inserting Visit Plans for P3 Customer
                list<Visits_Plan__c> vpinfluencerList3 = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc5.Id,null,false);
                for(Visits_Plan__c vp:vpinfluencerList3){
                  // vp.RecordTypeId = influencerVisitRecTypeId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c=visit;
                }
                insert vpinfluencerList3;
               
           //Inserting Visit Plans
                list<Visits_Plan__c> vpList = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc1.Id,null,false);
                insert vpList;
                /*RecordType repRecTypeAl = [select name,id from RecordType where SObjectType='Task' AND DeveloperName='Replacement' limit 1];
                EmailTemplate validEmailTemplate = new EmailTemplate();
                validEmailTemplate.isActive = true;
                validEmailTemplate.Name = 'Action Log escalation notification';
                validEmailTemplate.DeveloperName = 'sneha1234';
                validEmailTemplate.TemplateType = 'text';
                validEmailTemplate.FolderId = UserInfo.getUserId();
            
            insert validEmailTemplate;
                Server_url__c url=new Server_url__c(url__c='https://cs5.salesforce.com');
                insert url;
                Task actionLog1 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLog1.Action_to_be_taken__c = 'MED/Distributor/Fleet appointment or revival of closed dealer';
                actionLog1.RecordTypeId=repRecTypeAl.Id;    
                actionLog1.ActivityDate  = date.today(); 
                
                Task actionLog2 =  CEAT_InitializeTestData.createSignleTasks(acc2.Id,u.Id);
                actionLog2.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog2.RecordTypeId=repRecTypeAl.Id;
                actionLog2.ActivityDate  = date.today();    
                
                Task actionLog3 =  CEAT_InitializeTestData.createSignleTasks(acc6.Id,u.Id);
                actionLog3.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog3.RecordTypeId=repRecTypeAl.Id;
                actionLog3.status='Open';
                actionLog3.ActivityDate  = date.today();      
                
                Task actionLog4 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLog4.Action_to_be_taken__c = 'MED/Distributor/Fleet appointment or revival of closed dealer';
                actionLog4.RecordTypeId=repRecTypeAl.Id;
                actionLog4.status='Open';
                actionLog4.ActivityDate  = date.today();

                insert actionLog1;
                insert actionLog2;
                insert actionLog3;
                insert actionLog4;*/
            
                list<Task> taskList = CEAT_InitializeTestData.createMultipleTasks(acc1.Id,vpList[0].Id,u.Id,true);
                for(Task tk: taskList){
                    tk.Action_to_be_taken__c = 'Customer workings';
                    tk.Status = 'Closed';
                    tk.Conclusion__c = 'Testing';                    
                    tk.RecordTypeId = repRecTypeAl.Id;
                }
                insert taskList;          
         
                //Task Creation for Efleet
                Task actionLogEf1 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLogEf1.Action_to_be_taken__c = 'Performance report shared with fleets';
                actionLogEf1.Status = 'Open';
                actionLogEf1.Conclusion__c = 'Testing';  
                actionLogEf1.RecordTypeId=repRecTypeAl.Id; 
                insert actionLogEf1;
                
                Task actionLogEf2 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLogEf2.Action_to_be_taken__c = 'Performance report shared with dealers';
                actionLogEf2.Status = 'Closed';
                actionLogEf2.Conclusion__c = 'Testing';  
                actionLogEf2.RecordTypeId=repRecTypeAl.Id; 
                insert actionLogEf2;
                
                Task actionLogEf3 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLogEf3.Action_to_be_taken__c = 'Scrap inspection report shared with fleets';
                actionLogEf3.Status = 'Closed';
                actionLogEf3.Conclusion__c = 'Testing';  
                actionLogEf3.RecordTypeId=repRecTypeAl.Id; 
                insert actionLogEf3;
                
            DashboardInput_BatchClass db=new DashboardInput_BatchClass();
                database.executebatch(db,2000);
        }
    }
  
    static testMethod void myUnitTest1() {
         Date firstDayOfMonth = System.today().toStartOfMonth();
         Integer numberOfDays = Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month());
         Date lastDayOfMonth  = Date.newInstance(firstDayOfMonth.year(), firstDayOfMonth.month(), numberOfDays);
    
        User tlUser = CEAT_InitializeTestData.createUser('Ram','Ronak','ram.ronak@ceat.dev','ram.ronak@ceat.dev','Emp1','Replacements', null,'TL_Replacement');
        insert tlUser;
        
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId ];    
        User u = [Select id, name from User where id = :userTerrAssList[1].UserId limit 1];
        
        System.runAs(u) {
                RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
           
                Id repRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='PJP__c' and DeveloperName =: 'Replacement' Limit 1].Id;
                Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                
                Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
               // Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
              Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:efleetRecType Limit 1].Id;
                
                //Insert Dashboard Master
                Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true,Role__c='TL_Replacement');
                insert dm;
                Dashboard_Master__c dmTemp       = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true];
                system.debug('dmTemp=='+dmTemp);
                
                list<sObject> dwmAl    = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterData');
                list<sObject> dwmPjp   = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataPJP');
                list<sObject> dwmEfleet  = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterDataEfleet');
                
                for(sObject dwm1 : dwmAl){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm1;
                    dwmTemp.RecordTypeId = actionLogRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                }
                update dwmAl;
                
                for(sObject dwm2 : dwmPjp){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm2;
                    dwmTemp.RecordTypeId = inputRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                }
                update dwmPjp;
                
                for(sObject dwm3 : dwmEfleet){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm3;
                    dwmTemp.RecordTypeId = efleetRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    //if(dwmTemp.category__c.trim()=='Customer - Data capture and  reporting'){
                      //system.debug('--------------'+dwmTemp.category__c);
                    //}
                }
                update dwmEfleet;
                
                //PJP Record
                PJP__c PjpRec = CEAT_InitializeTestData.createPjpRecord(u.Id);
                PjpRec.RecordTypeId = repRecTypeId;
                insert pjpRec;
                
                //String visit= 'Visit Day';
                ScheduledBatch_DashboardInputTL temp = new ScheduledBatch_DashboardInputTL();
                String sch = '0 0 23 * * ?'; 
                system.schedule('Test Territory Check', sch, temp);
                DashboardInput_BatchClass db=new DashboardInput_BatchClass();
                database.executebatch(db,2000);
        }
    }
        
      static testMethod void myUnitTestTLD() {             
         Date firstDayOfMonth = System.today().toStartOfMonth();
         Integer numberOfDays = Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month());
         Date lastDayOfMonth  = Date.newInstance(firstDayOfMonth.year(), firstDayOfMonth.month(), numberOfDays);
    
         User tlUser = CEAT_InitializeTestData.createUser('Ram','Ronak','ram.ronak@ceat.dev','ram.ronak@ceat.dev','Emp1','Replacements', null,'TLD');
        insert tlUser;
        
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId ];    
        list<UserTerritory2Association> terr = [select id,Territory2.developerName,Territory2Id,UserId from UserTerritory2Association where Territory2.developerName='DI001' AND RoleInTerritory2='TLD' limit 1];
        User u = [Select id, name from User where id = :terr[0].UserId limit 1];
  	    system.debug('In Test ---'+u.Id);   
        System.runAs(u) {
                RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
                Account acc1 = CEAT_InitializeTestData.createAccount('Account1', 'C000000001', 'NAG', 'ZW01', 'DI001');
                acc1.Sales_District_Text__c = 'DI001';    
                acc1.Type = 'Analyst';
                acc1.Customer_Segment__c='High Priority Dealer';
                acc1.RecordTypeId = rtDealer.Id;
                acc1.Business_Units__c='Replacement';
                acc1.Active__c = true;
                insert acc1;
                
                Account acc2 = CEAT_InitializeTestData.createAccount('Account2', 'C000000002', 'NAG', 'ZW01', 'DI001');
                acc2.Sales_District_Text__c = 'DI001';    
                acc2.Type = 'Analyst';
                acc2.Customer_Segment__c='High Priority Dealer';
                acc2.RecordTypeId = rtDealer.Id;
                acc2.Business_Units__c='Replacement';
                acc2.Active__c = true;
                insert acc2;
                
                RecordType repRecTypeAl = [select name,id from RecordType where SObjectType='Task' AND DeveloperName='Replacement' limit 1];
            
                Task actionLog1 =  CEAT_InitializeTestData.createSignleTasks(acc1.Id,u.Id);
                actionLog1.Action_to_be_taken__c = 'MED/Distributor/Fleet appointment or revival of closed dealer';
                actionLog1.RecordTypeId=repRecTypeAl.Id;    
                actionLog1.ActivityDate  = date.today(); 
                
                Task actionLog2 =  CEAT_InitializeTestData.createSignleTasks(acc2.Id,u.Id);
                actionLog2.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog2.RecordTypeId=repRecTypeAl.Id;
                actionLog2.ActivityDate  = date.today();    
                
                Task actionLog3 =  CEAT_InitializeTestData.createSignleTasks(acc2.Id,u.Id);
                actionLog3.Action_to_be_taken__c = 'BTL (Customer Meets/ Influencer / Sub-dealer meets)';
                actionLog3.RecordTypeId=repRecTypeAl.Id;
                actionLog3.status='Open';
                actionLog3.ActivityDate  = date.today();      
                
                insert actionLog1;
                insert actionLog2;
                insert actionLog3;
            
                Id repRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='PJP__c' and DeveloperName =: 'Replacement' Limit 1].Id;
                Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
                
                //Insert Dashboard Master
                Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true,Role__c='TLD');
                insert dm;
                Dashboard_Master__c dmTemp       = [SELECT id,Active__c From Dashboard_Master__c WHERE Active__c = true];                
                list<sObject> dwmAl              = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'WeightageMasterData');
                
                for(sObject dwm1 : dwmAl){
                    Dashboard_Weightage_Master__c dwmTemp = (Dashboard_Weightage_Master__c)dwm1;
                    dwmTemp.RecordTypeId = actionLogRecTypeId;
                    dwmTemp.Dashboard_Master__c = dm.id;
                    dwmTemp.Role__c = 'TLD';
                }
                update dwmAl;
               
  				Dashboard_Weightage_Master__c dw = new Dashboard_Weightage_Master__c();
                dw.Testing__c = true;
                dw.Category__c = 'PJP adherance';
                dw.Parameters_Inout__c = 'No. of days of beats workings for distributors';
                dw.Role__c = 'TLD';
                dw.Weightage__c = 5;
                dw.Dashboard_Master__c = dm.id;
                dw.RecordTypeId = inputRecTypeId;
                
                insert dw;
                
                //PJP Record
                PJP__c PjpRec = CEAT_InitializeTestData.createPjpRecord(u.Id);
                PjpRec.RecordTypeId = repRecTypeId;
                insert pjpRec;                
                
                Id dseBeatId =[select id,name from RecordType where SobjectType='Visits_Plan__c' AND Name=:system.Label.DSE_Beat_RecType limit 1].Id;
                //Inserting Visit Plans
                
                list<Visits_Plan__c> vpinfluencerList = CEAT_InitializeTestData.createMultipleVisits(pjpRec,acc1.Id,null,true);
                for(Visits_Plan__c vp:vpinfluencerList){
                   //vp.RecordTypeId = dseBeatId;
                   vp.Unplanned_visit__c=false;
                   vp.Type_of_Day__c='Visit Day';
                   vp.status__c= 'Successsful';  
                }
                insert vpinfluencerList;
                ScheduledBatch_DashboardInputTLD temp = new ScheduledBatch_DashboardInputTLD();
                String sch = '0 0 23 * * ?'; 
                system.schedule('Test Territory Check', sch, temp);
        	    DashboardInput_BatchClass_TLD temp1 = new DashboardInput_BatchClass_TLD(); 
                database.executebatch(temp1,200);
        }
    }  
}