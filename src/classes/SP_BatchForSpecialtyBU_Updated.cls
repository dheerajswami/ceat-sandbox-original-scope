global class SP_BatchForSpecialtyBU_Updated implements Database.Batchable<sObject> {    
    String recTypeStagingLabel      = System.Label.Staging;
    String special                  = System.Label.Specialty;
    String replacement              = System.Label.Replacement;
    String Forcast                  = System.Label.Forcast; 
    String recTypeSpeZoneLabel    = System.Label.Specialty_Forcast_Zone;
    String rmRole = system.Label.RM_Role;
    String amRole = system.Label.AM_SpecialtyURole;
    
    String rmRoleSpecilaty      = system.label.Specialty_RM_Role;
    String srmRoleSpecilaty     = system.label.SRM_SpecialtyUrole;
    
    Static String SPECIALTY='Specialty';
    Static String REPLECMENT='Replacement';
    String catZoneSpe = '';
    
    List<Sales_Planing_Staging__c> specialtyDealer = new List<Sales_Planing_Staging__c>();
    List<Sales_Planing_Staging__c> allForcast = new List<Sales_Planing_Staging__c>();
    List<Sales_Planing_Staging__c> stgProccessed = new List<Sales_Planing_Staging__c>();
    List<Sales_Planning__c> repForcastList = new List<Sales_Planning__c>();
    List<Sales_Planning__c> speForcastList = new List<Sales_Planning__c>();
    List<Sales_Planning__c> speForcastListZone = new List<Sales_Planning__c>();
    List<Sales_Planning__c> speDealerListReg = new List<Sales_Planning__c>();
    map<id,Sales_Planing_Staging__c> allForcastMap = new map<id,Sales_Planing_Staging__c>();
    List<Sales_Planning__c> specialtyForcast = new List<Sales_Planning__c>();
    List<Account> ListOfAccount = new list<account>();
    set<String> territoryCode = new set<String>();
    Set<String> catSpeSet = new Set<String>();
    Set<String> catRepSet = new Set<String>();
    Set<String> setkeyCatRegSpe = new Set<String>();
    Set<String> setkeyCatRegRep = new Set<String>();
    Set<String> setkeyCatZone = new Set<String>();
    set<String> catsetKey = new set<String>();
    Map<String,Sales_Planning__c> mapOFForcastCatWiseSpe = new Map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOFForcastCatWiseRep = new Map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOfDealerAndSales = new Map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOfRegionAndSales = new Map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOfZoneAndSales = new Map<String,Sales_Planning__c>();
    Map<String,Sales_Planning__c> mapOfZoneForcast = new Map<String,Sales_Planning__c>();
    List<Territory2> allTerr = new List<Territory2>();
    list<UserTerritory2Association> terrUserList;
    list<UserTerritory2Association> terrUserListSpe;
    Map<id,id> mapOfUserAndTerr;
    Map<id,id> mapOfUserAndTerrSpe;
    Map<String,id> mapOfUserAndTerrCode;
    Map<String,id> mapOfUserAndTerrCodeSpe;
    Set<String> speTerri = new Set<String>();
    Map<String,Territory2> mapOfNameAndTerritory = new Map<String,Territory2>();
    Map<Id,Territory2> mapOFIdAndterritory = new Map<Id,Territory2>();
    Map<String,id> mapOFcusNumAndID = new Map<String,ID>();
    Integer totalForcast  ;
    global SP_BatchForSpecialtyBU_Updated (){
        repForcastList                  = new list<Sales_Planning__c>();
        speForcastList                  = new list<Sales_Planning__c>();
        speForcastListZone              = new list<Sales_Planning__c>();
        specialtyDealer                 = new List<Sales_Planing_Staging__c>();
        speDealerListReg                = new list<Sales_Planning__c>();
        allForcast                      = new List<Sales_Planing_Staging__c>();
        allForcastMap                   = new map<id,Sales_Planing_Staging__c>();
        mapOfDealerAndSales             = new map<String,Sales_Planning__c>();
        mapOfRegionAndSales             = new map<String,Sales_Planning__c>();
        mapOfZoneAndSales               = new map<String,Sales_Planning__c>();
        mapOfZoneForcast                = new map<String,Sales_Planning__c>();
       
        specialtyForcast                = new List<Sales_Planning__c>();
        catSpeSet                       = new Set<String>();
        catRepSet                       = new Set<String>();
        mapOFForcastCatWiseSpe          = new Map<String,Sales_Planning__c>();
        mapOFForcastCatWiseRep          = new Map<String,Sales_Planning__c>();
        allTerr                         = new List<Territory2>();
        totalForcast         = 0;
        mapOFIdAndterritory = new Map<Id,Territory2>([select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId from Territory2]);
        mapOfNameAndTerritory = new Map<String,Territory2>();
        
        for(Territory2 trr : mapOFIdAndterritory.values()){
            mapOfNameAndTerritory.put(trr.DeveloperName,trr);
            territoryCode.add(trr.name);
        }
    } 
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query;
        String month;
        String year;
        Date d = system.today();
        month = String.valueOf(d.month());
        year = String.valueOf(d.Year());
        
        //getting all staging records with BU specialty and Forcast
       
        //Id recordTypeStagingId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeStagingLabel Limit 1].Id;
        query = 'SELECT Id,OwnerId,ASP__c,Budget__c,Reg_code__c,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,'
        +'Territory_Code__c,NBP__c,NBP_Value__c,Year__c,SPExternalIDTL__c,BU__c,SKU__c,MAKTX__c,Zone__c,Zone_Description__c,Value__c,Value_L3M__c,Value_LYCM__c '+
        'FROM Sales_Planing_Staging__c WHERE (BU__c =\''+Forcast +'\' OR BU__c = \''+special+'\' ) AND Reg_code__c IN: territoryCode AND Processed__c = false AND Year__c =\''+year+'\' AND Month__c =\''+month+'\'' ;
        System.debug(Database.getQueryLocator(query)+'@@@query@@@@');
        
         /*
        //Id recordTypeStagingId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeStagingLabel Limit 1].Id;
        
        query = 'SELECT Id,OwnerId,ASP__c,Budget__c,Reg_code__c,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,'
        +'Territory_Code__c,NBP__c,Year__c,SPExternalIDTL__c,BU__c,SKU__c,MAKTX__c,Zone__c,Zone_Description__c,Value__c,Value_L3M__c,Value_LYCM__c '+
        'FROM Sales_Planing_Staging__c WHERE ( BU__c = \''+special+'\' )AND Year__c =\''+year+'\' AND Month__c =\''+month+'\' and Reg_code__c =\'PAT\'' ;
        */
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        String month;
        String year;
        Date d = system.today();
        month = String.valueOf(d.month());
        year = String.valueOf(d.Year());
        String repROLabel = System.Label.Replacement_RO;
        String speForcastLabel = System.Label.Specialty_Forcast;
        allForcastMap=new Map<id,Sales_Planing_Staging__c>();
        allForcast=new List<Sales_Planing_Staging__c>();
        specialtyDealer = new list<Sales_Planing_Staging__c>();
        mapOfUserAndTerr                = new map<Id,id>();
        mapOfUserAndTerrCode            = new map<String,id>();
        mapOfUserAndTerrCodeSpe         = new map<String,id>();
        mapOfUserAndTerrSpe             = new map<Id,id>();
         
        terrUserList                    = new list<UserTerritory2Association>();
        terrUserListSpe                 = new list<UserTerritory2Association>();
        Sales_Planning__c spSpecialtyForcastCatWise;
        Account dealer;
        Sales_Planning__c spReplacementForcastCatWise;
        Sales_Planning__c spReplacementCatWise;
        Sales_Planning__c spSpecialtyForcastCatWiseMap = new Sales_Planning__c();
        Sales_Planning__c spSpecialtyForcastCatZoneWiseMap = new Sales_Planning__c();
        Sales_Planning__c spReplacementForcastCatWiseMap = new Sales_Planning__c();
        Sales_Planning__c spSpecialtyCatWiseMap = new Sales_Planning__c();
        Sales_Planning__c spSpecialtyRegionCatWiseMap = new Sales_Planning__c();
        Sales_Planning__c spSpecialtyRegionCatWise;
        Sales_Planning__c spSpecialtyZoneCatWise;
        String catZoneSpeDealer = '';
        String keyReg = '';
        String CZR = '';
        Id replaceROId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
        Id specailtyForcastId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: speForcastLabel Limit 1].Id;
        Id specailtyForcastZoneId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeSpeZoneLabel Limit 1].Id;
        Id specailtyDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: special Limit 1].Id;
        //getting Speciality Category from custom setting 
        list<Speciality_Sales_Planning_Categories__c> spc = Speciality_Sales_Planning_Categories__c.getall().values();
        for(Speciality_Sales_Planning_Categories__c sp : spc) {
          if(sp.Include_in_Sales_Planning__c == true){    
              catSpeSet.add(sp.Category_Code__c);  
          }          
        }
        
        system.debug('catSpeSet'+catSpeSet);
        
        //**********************Getting territory code of each user**********************
            terrUserList = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 =:rmRole];
            if(terrUserList.size()>0){
                for(UserTerritory2Association ut: terrUserList){
                    mapOfUserAndTerr.put(ut.Territory2Id,ut.UserId);                    
                }
            }
            List<Territory2> userTerritory=[select name,id,DeveloperName from Territory2 where id IN :mapOfUserAndTerr.keyset()];
            for(Territory2 tt: userTerritory){
                mapOfUserAndTerrCode.put(tt.name,mapOfUserAndTerr.get(tt.id));
            }
            
            
            terrUserListSpe = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where RoleInTerritory2 =:amRole OR RoleInTerritory2 =:rmRoleSpecilaty OR RoleInTerritory2 =:srmRoleSpecilaty];
            if(terrUserListSpe.size()>0){
                for(UserTerritory2Association ut1: terrUserListSpe){
                    mapOfUserAndTerrSpe.put(ut1.Territory2Id,ut1.UserId);                   
                }
            }
            List<Territory2> userTerritory1=[select name,id,DeveloperName from Territory2 where id IN :mapOfUserAndTerrSpe.keyset()];
            for(Territory2 tt1: userTerritory1){
                mapOfUserAndTerrCodeSpe.put(tt1.name,mapOfUserAndTerrSpe.get(tt1.id));
            }
            system.debug('mapOfUserAndTerr--'+mapOfUserAndTerr);
        
        
        //getting Replacement Category from Custom setting
        list<Sales_Planning_Categories__c> spcCat = Sales_Planning_Categories__c.getall().values();
            for(Sales_Planning_Categories__c sp:spcCat){
              if(sp.Include_in_Sales_Planning__c == true){
                catRepSet.add(sp.Category_Code__c);  
              }      
        }
        system.debug('catRepSet'+catRepSet);
        //preparing set for each roll up 
        string keyCatRegSpe = '';
        string keyCatZone = '';
        string keyCatRegRep = '';
        catZoneSpe = '';
        string keyCatRegDeaSpe ='';
        ListOfAccount = [SELECT id,name,KUNNR__c From Account];
        for(Account acc : ListOfAccount){
            mapOFcusNumAndID.put(acc.KUNNR__c,acc.id);
        }
        for(sObject temp : scope){
            Sales_Planing_Staging__c sps =(Sales_Planing_Staging__c)temp;
            if(catSpeSet.contains(sps.Category__c)){
                keyCatRegSpe = sps.Category__c+sps.Reg_code__c;
                setkeyCatRegSpe.add(keyCatRegSpe);
                
                if(sps.Dealer_CustNumber__c != null){
                    keyCatRegDeaSpe = sps.Category__c+sps.Reg_code__c+sps.Dealer_CustNumber__c;
                    catsetKey.add(keyCatRegDeaSpe);
                }
            }else if(catRepSet.contains(sps.Category__c)){
                keyCatRegRep = sps.Category__c+sps.Reg_code__c;
                setkeyCatRegRep.add(keyCatRegRep);
            }
            if(sps.Reg_code__c!=null && (mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)) != null)){
            system.debug('@@@'+sps.Reg_code__c+'@@@'+mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id+'@@@'+mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name);
            catZoneSpe = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(sps.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
            keyCatZone = sps.Category__c+catZoneSpe;                                    
            setkeyCatZone.add(keyCatZone);
            }            
        }
        system.debug(catRepSet.size()+'catRepSet');
        system.debug(catSpeSet.size()+'catSpeSet');
        system.debug(scope.size()+'@@@scope@@@');
        //Sperated staging into specialty dealer and forcast for both BU's
        for(sObject temp : scope) {
            Sales_Planing_Staging__c sp =(Sales_Planing_Staging__c)temp;
            if(sp.BU__c == special && sp.BU__c != null){
              
                  specialtyDealer.add(sp);
      
            }  
            else if(sp.BU__c == Forcast && sp.BU__c != null){
                    if(sp.Target_Quantity__c > 0){
                        allForcast.add(sp);
                        allForcastMap.put(sp.id,sp);
                    }
            }
        }
        system.debug(specialtyDealer.size()+'############');
        system.debug(allForcastMap.size()+'@@@@@');
        system.debug(scope.size()+'@@@scope@@@');
        
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch1__c IN : setkeyCatRegSpe AND RecordTypeId =: specailtyForcastId AND Month__c =: month AND Year__c =: year] ){
        mapOFForcastCatWiseSpe.put(salesPlan.SYS_Used_IN_Batch1__c,salesPlan ); 
        }
        
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch1__c IN : setkeyCatRegRep AND RecordTypeId =: replaceROId AND Month__c =: month AND Year__c =: year] ){
        mapOFForcastCatWiseRep.put(salesPlan.SYS_Used_IN_Batch1__c,salesPlan ); 
        }
        
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,SYS_Used_IN_Batch2__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch2__c IN : setkeyCatZone AND RecordTypeId =: specailtyForcastZoneId AND Month__c =: month AND Year__c =: year] ){
        mapOfZoneForcast.put(salesPlan.SYS_Used_IN_Batch2__c,salesPlan ); 
        }
        
        for(Sales_Planning__c salesPlan : [SELECT Id,Dealer_CustNumber__c,Target_Quantity__c,NBP__c,Region_code__c ,Region_Description__c ,Category__c ,Category_Description__c ,
        Month__c ,Year__c ,Zone__c ,Territory_Code__c ,SYS_Used_IN_Batch__c,SYS_Used_IN_Batch2__c,SYS_Used_IN_Batch1__c,Dealer_Name__c ,Dealer__c, BU__c ,Value_L3M__c ,L3M__c ,LYCM__c ,Value_LYCM__c ,SPExternalIDTL__c ,RecordTypeId FROM Sales_Planning__c WHERE SYS_Used_IN_Batch__c IN : catsetKey AND RecordTypeId =: specailtyDealerId AND Month__c =: month AND Year__c =: year] ){
        mapOfDealerAndSales.put(salesPlan.SYS_Used_IN_Batch__c,salesPlan ); 
        }
       // system.debug();
        
        // Divideing forcast to respective BU's
        if(allForcastMap.size() > 0){
            String keySpeRep = '';
            String speZone= '';
            Id userId ;
            Id userIdSpe ;
            String keyspe = '';
            
            for(Sales_Planing_Staging__c temp : allForcastMap.values()){
              //getting all Specialty forcast records
                if(catSpeSet.contains(temp.Category__c) && temp.Category__c<>null && temp.Reg_code__c<>null ){
                      system.debug(temp.Reg_code__c+'temp.Reg_code__c');
                      speZone = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(temp.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
                    //key = temp.Category__c+temp.Region_code__c;
                        keySpeRep = temp.Category__c+temp.Reg_code__c;
                        if(mapOFForcastCatWiseSpe.containsKey(keySpeRep)){
                             System.debug('====keySpeRep:'+keySpeRep);
                                spSpecialtyForcastCatWiseMap = new Sales_Planning__c();
                                spSpecialtyForcastCatWiseMap = mapOFForcastCatWiseSpe.get(keySpeRep);
                                spSpecialtyForcastCatWiseMap.Target_Quantity__c += ((temp.Target_Quantity__c != null && temp.Target_Quantity__c>0)?temp.Target_Quantity__c:0);
                                //spSpecialtyForcastCatWiseMap.NBP__c += ((temp.NBP__c != null && temp.NBP__c>0)?temp.NBP__c:0);
                                spSpecialtyForcastCatWiseMap.NBP__c += ((temp.NBP_Value__c  != null && temp.NBP_Value__c >0)?temp.NBP_Value__c :0);
                                mapOFForcastCatWiseSpe.put(keySpeRep,spSpecialtyForcastCatWiseMap);
                        }else{
                                spSpecialtyForcastCatWise = new Sales_Planning__c(
                                Region_code__c = temp.Reg_code__c,
                                Region_Description__c = temp.Region_Description__c,
                                Category__c = temp.Category__c,
                                Category_Description__c = temp.Category_Description__c,
                                Month__c = temp.Month__c,Year__c = temp.Year__c,Zone__c = speZone,
                                Target_Quantity__c = ((temp.Target_Quantity__c != null && temp.Target_Quantity__c>0)?temp.Target_Quantity__c:0),
                                BU__c = special,
                                RecordTypeId = specailtyForcastId,
                                //NBP__c = ((temp.NBP__c != null && temp.NBP__c>0)?temp.NBP__c:0),
                                NBP__c = ((temp.NBP_Value__c != null && temp.NBP_Value__c>0)?temp.NBP_Value__c:0),
                                SPExternalIDTL__c = temp.Reg_code__c+temp.Category__c+temp.Month__c+temp.Year__c
                            );
                            
                            mapOFForcastCatWiseSpe.put(keySpeRep,spSpecialtyForcastCatWise );
                            
                        }
                    }
                    //getting all Replacement forcast records
                    else if(catRepSet.contains(temp.Category__c) && temp.Category__c<>null && temp.Region_code__c<>null ){
                        keySpeRep = temp.Category__c+temp.Reg_code__c;
                        if(mapOFForcastCatWiseRep.containsKey(keySpeRep)){
                             System.debug('====keySpeRep:'+keySpeRep);
                                spReplacementForcastCatWiseMap = new Sales_Planning__c();
                                spReplacementForcastCatWiseMap = mapOFForcastCatWiseRep.get(keySpeRep);
                                spReplacementForcastCatWiseMap.Target_Quantity__c += (temp.Target_Quantity__c>0?temp.Target_Quantity__c:0);
                                mapOFForcastCatWiseRep.put(keySpeRep,spReplacementForcastCatWiseMap);
                        }else{
                                if(mapOfUserAndTerrCode.get(temp.Reg_code__c)!=null){
                                    userId = mapOfUserAndTerrCode.get(temp.Reg_code__c);
                                }else{
                                    userId = UserInfo.getUserId();
                                    
                                    
                                }
                                spReplacementForcastCatWise = new Sales_Planning__c(
                                Region_code__c = temp.Reg_code__c,
                                Region_Description__c = temp.Region_Description__c,
                                Category__c = temp.Category__c,
                                Category_Description__c = temp.Category_Description__c,
                                Month__c = temp.Month__c,Year__c = temp.Year__c,Zone__c = temp.Zone__c,
                                Target_Quantity__c = temp.Target_Quantity__c,
                                BU__c = replacement,
                                OwnerId = userId,
                                RecordTypeId = replaceROId,
                                SPExternalIDTL__c = temp.Reg_code__c+temp.Category__c+temp.Month__c+temp.Year__c
                            );
                            IF(temp.Target_Quantity__c>0){
                              System.debug('===='+temp.ID+'    :   TEMP.Target_Quantity__c: '+TEMP.Target_Quantity__c);
                            }
                            mapOFForcastCatWiseRep.put(keySpeRep,spReplacementForcastCatWise );
                        }
                    }
                    //getting all Specialty forcast zone wise 
                    if(catSpeSet.contains(temp.Category__c)){
                        catZoneSpe = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(temp.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
                        keyspe = temp.Category__c+catZoneSpe;
                        system.debug(keyspe+'keyspe');
                        if(mapOfZoneForcast.containsKey(keyspe)){
                                spSpecialtyForcastCatZoneWiseMap = new Sales_Planning__c();
                                spSpecialtyForcastCatZoneWiseMap = mapOfZoneForcast.get(keyspe);
                                spSpecialtyForcastCatZoneWiseMap.Target_Quantity__c += ((temp.Target_Quantity__c != null && temp.Target_Quantity__c>0)?temp.Target_Quantity__c:0);
                                //spSpecialtyForcastCatZoneWiseMap.NBP__c += ((temp.NBP__c != null && temp.NBP__c>0)?temp.NBP__c:0);
                                spSpecialtyForcastCatZoneWiseMap.NBP__c += ((temp.NBP_Value__c != null && temp.NBP_Value__c>0)?temp.NBP_Value__c:0);
                                mapOfZoneForcast.put(keyspe,spSpecialtyForcastCatZoneWiseMap);
                        }else{
                                if(mapOfUserAndTerrCodeSpe.get(catZoneSpe)!=null){
                                    userIdSpe = mapOfUserAndTerrCodeSpe.get(catZoneSpe);
                                }else{
                                    userIdSpe = UserInfo.getUserId();
                                }
                                spSpecialtyZoneCatWise = new Sales_Planning__c(
                                Category__c = temp.Category__c,
                                Category_Description__c = temp.Category_Description__c,
                                Month__c = temp.Month__c,Year__c = temp.Year__c,Zone__c = catZoneSpe,
                                Target_Quantity__c = temp.Target_Quantity__c,
                                BU__c = special,
                                OwnerId = userIdSpe,
                                RecordTypeId = specailtyForcastZoneId,
                                NBP__c = ((temp.NBP_Value__c != null && temp.NBP_Value__c >0)?temp.NBP_Value__c :0),
                                //NBP__c = ((temp.NBP__c != null && temp.NBP__c>0)?temp.NBP__c:0),
                                
                                
                                
                                SPExternalIDTL__c = catZoneSpe+temp.Category__c+temp.Month__c+temp.Year__c
                                );
                                mapOfZoneForcast.put(keyspe,spSpecialtyZoneCatWise);
                        }
                    }
                    
            }
        }
        
        //upsert replacement forcast
        if(mapOFForcastCatWiseRep.size() > 0){
            repForcastList = new list<Sales_Planning__c>();
            for(Sales_Planning__c temp : mapOFForcastCatWiseRep.values()){
                repForcastList.add(temp);
            }
            try{
               database.upsert (repForcastList,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
            catch(Exception e){
                  System.debug(e.getMessage());
            }
           // upsert repForcastList SPExternalIDTL__c;
        }
        //upsert speciality Region wise forcast
        if(mapOFForcastCatWiseSpe.size() > 0){
            speForcastList = new list<Sales_Planning__c>();
            for(Sales_Planning__c temp : mapOFForcastCatWiseSpe.values()){
                speForcastList.add(temp);
            }
            try{
                database.upsert (speForcastList,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
            catch(Exception e){
                  System.debug(e.getMessage());
            }
            //upsert speForcastList SPExternalIDTL__c;
        }
        //upsert speciality Zone wise forcast
    if(mapOfZoneForcast.size() > 0){
      speForcastListZone = new list<Sales_Planning__c>();
      for(Sales_Planning__c temp : mapOfZoneForcast.values()){
        speForcastListZone.add(temp);
      }
      try{
                database.upsert (speForcastListZone,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
            catch(Exception e){
                  System.debug(e.getMessage());
            }
     // upsert speForcastListZone SPExternalIDTL__c;
    }
        system.debug(specialtyDealer.size()+'specialtyDealer');
        if(specialtyDealer.size() > 0){
            Id userIdSpeDea ;
            keyReg = '';
            for(Sales_Planing_Staging__c temp : specialtyDealer){
              //if(temp.Reg_code__c == 'PAT' && temp.Dealer_CustNumber__c == '50003726' && temp.Category__c == '2680'){
                System.debug('====temp:'+temp); 
                  if(catSpeSet.contains(temp.Category__c)){
                    catZoneSpeDealer = mapOFIdAndterritory.get(mapOfNameAndTerritory.get(getTerritoryDevName(temp.Reg_code__c,  SPECIALTY)).ParentTerritory2Id).Name;
                    system.debug('++++'+catZoneSpeDealer );
                    keyReg = temp.Category__c+temp.Reg_code__c+temp.Dealer_CustNumber__c;
                    System.debug('====keyReg:'+keyReg); 
                    if(mapOfDealerAndSales.containsKey(keyReg)){
                            System.debug('====keyReg:'+keyReg);
                            spSpecialtyCatWiseMap = new Sales_Planning__c();
                            spSpecialtyCatWiseMap = mapOfDealerAndSales.get(keyReg);
                            if(spSpecialtyCatWiseMap.L3M__c ==null)spSpecialtyCatWiseMap.L3M__c=0;
                            if(spSpecialtyCatWiseMap.LYCM__c ==null)spSpecialtyCatWiseMap.LYCM__c=0;
                            if(spSpecialtyCatWiseMap.Value_LYCM__c ==null)spSpecialtyCatWiseMap.Value_LYCM__c=0;
                            if(spSpecialtyCatWiseMap.Value_L3M__c ==null)spSpecialtyCatWiseMap.Value_L3M__c=0;
                            spSpecialtyCatWiseMap.L3M__c += (temp.L3M__c>0 && temp.L3M__c != null ?temp.L3M__c:0);
                            spSpecialtyCatWiseMap.LYCM__c += ((temp.LYCM__c != null && temp.LYCM__c>0) ?temp.LYCM__c:0);
                            spSpecialtyCatWiseMap.Value_LYCM__c += (temp.Value_LYCM__c>0 && temp.Value_LYCM__c != null ?temp.Value_LYCM__c:0);
                            spSpecialtyCatWiseMap.Value_L3M__c += (temp.Value_L3M__c>0 && temp.Value_L3M__c != null ?temp.Value_L3M__c:0);
                            system.debug('==========<<<<<<<< '+temp.id+'----'+temp.Dealer_CustNumber__c+'========== '+temp.Category__c+'-'+temp.Region_code__c +'<=========>' +temp.L3M__c+'<----->'+temp.LYCM__c);
                            system.debug('==========>>>>>>> '+spSpecialtyCatWiseMap.id+'------'+spSpecialtyCatWiseMap.Dealer_CustNumber__c+'========== '+spSpecialtyCatWiseMap.Category__c+'-'+spSpecialtyCatWiseMap.Region_code__c +'<=========>' +spSpecialtyCatWiseMap.L3M__c+'<----->'+spSpecialtyCatWiseMap.LYCM__c);
                            mapOfDealerAndSales.put(keyReg,spSpecialtyCatWiseMap);
                    }else{
                            if(mapOfUserAndTerrCodeSpe.get(catZoneSpeDealer)!=null){
                                    userIdSpeDea = mapOfUserAndTerrCodeSpe.get(catZoneSpeDealer);
                            }else{
                                    userIdSpeDea = UserInfo.getUserId();
                            }
                            dealer = new Account (KUNNR__c = temp.Dealer_CustNumber__c);
                            spReplacementCatWise = new Sales_Planning__c(
                            Region_code__c = temp.Reg_code__c,
                            Region_Description__c = temp.Region_Description__c,
                            Category__c = temp.Category__c,
                            Category_Description__c = temp.Category_Description__c,
                            Month__c = temp.Month__c,Year__c = temp.Year__c,Zone__c = catZoneSpeDealer,
                            Territory_Code__c = temp.Territory_Code__c,
                            Dealer_CustNumber__c = temp.Dealer_CustNumber__c,
                            Dealer_Name__c =  temp.Dealer_Name__c,Dealer__r = dealer,
                            BU__c = special,Value_L3M__c = temp.Value_L3M__c,
                            OwnerId = userIdSpeDea,
                            Zone_Description__c = catZoneSpeDealer,
                            L3M__c = temp.L3M__c,LYCM__c = temp.LYCM__c,
                            Value_LYCM__c = temp.Value_LYCM__c,
                            RecordTypeId = specailtyDealerId,
                            SPExternalIDTL__c = mapOFcusNumAndID.get(temp.Dealer_CustNumber__c)+temp.Reg_code__c+temp.Category__c+temp.Month__c+temp.Year__c
                            );
                            mapOfDealerAndSales.put(keyReg,spReplacementCatWise);     
                    }
                  }   
              //}
             
            }
        }
        if(mapOfDealerAndSales.size() > 0){
          speDealerListReg = new list<Sales_Planning__c>();
          for(Sales_Planning__c temp: mapOfDealerAndSales.values()){
            speDealerListReg.add(temp);
          }
          try{
                database.upsert (speDealerListReg ,Sales_Planning__c.Fields.SPExternalIDTL__c,false) ;
            }
            catch(Exception e){
                  System.debug(e.getMessage());
            }
          //upsert speDealerListReg SPExternalIDTL__c;
        } 
        for(sObject temp : scope){
            Sales_Planing_Staging__c sp =(Sales_Planing_Staging__c)temp;
            sp.Processed__c = true;
            stgProccessed.add(sp);
        }
        update stgProccessed;
    }
    global void finish(Database.BatchableContext BC)
    {       
            system.debug('catSpeSet'+catSpeSet);
            system.debug('catRepSet'+catRepSet);
            
             // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SP_BatchForSpecialtyBU_Updated Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SP_BatchForSpecialtyBU_Updated" processed ' + a.TotalJobItems + ' </br>   batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '</br>');  
             if(a.TotalJobItems == 0 || a.NumberOfErrors > 0){
                // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
             
            
    }
    GLOBAL static string  getTerritoryDevName( String name,String OperationType){
        String fullName;

        String REPL_PREF='SPRP_';
        if(OperationType==SPECIALTY){
            fullName=REPL_PREF+name;
        }
        if(OperationType==REPLECMENT){
            fullName=name;
        }
        
        
        return fullname;
    }
}