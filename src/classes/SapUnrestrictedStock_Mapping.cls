global class SapUnrestrictedStock_Mapping{

  /*
* Auther  :- Sneha Agrawal  
* Purpose :- Fetch Unrestricted Stock from SAP 
* Date    :- 7/July/2015
*
*/
    
    //-- ATTRIBUTES    
   
    //-- CONSTRUCTOR
    
    //-- Methods
    
    WebService static List<UnrestrictedStockMapping> getAllUnrestrictedStock(String territory_code){
        
        List<UnrestrictedStockMapping> unrestrictedStock = new List<UnrestrictedStockMapping>();        
        try{
            
            SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
                        
            String username                                 = saplogin.username__c;
            String password                                 = saplogin.password__c;
            Blob headerValue                                = Blob.valueOf(username + ':' + password);
            String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            SapUnrestrictedStockSoap tst                    = new SapUnrestrictedStockSoap();
            SapUnrestrictedStockSoap.ZWS_GET_UNRST_STOCK zws  = new SapUnrestrictedStockSoap.ZWS_GET_UNRST_STOCK();
            
            zws.inputHttpHeaders_x                          = new Map<String,String>();
            zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            zws.timeout_x                                   = 60000;
            
            SapUnrestrictedStockSoap.TableOfBapireturn tom      = new SapUnrestrictedStockSoap.TableOfBapireturn();
            SapUnrestrictedStockSoap.TableOfZbapiMatStock itab  = new SapUnrestrictedStockSoap.TableOfZbapiMatStock();
            
            List<SapUnrestrictedStockSoap.Bapireturn> m             = new List<SapUnrestrictedStockSoap.Bapireturn>();        
            
            tom.item = m;
            
            SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element e = new SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element();
            
            e = zws.ZbapiGetUnrstStock(territory_code, itab,'','',tom);
            //String Bzirk,SapUnrestrictedStockSoap.TableOfZbapiMatStock Itab,String Matkl,String Matnr,SapUnrestrictedStockSoap.TableOfBapireturn Return_x
            
            if(e.Itab.item != null){            
                for(SapUnrestrictedStockSoap.ZbapiMatStock z : e.Itab.item) {
                	system.debug('Mat Num -'+ z.Matnr + ' Mat Grp -'+z.Maktx+' Stock - '+ z.Labst  );
                    unrestrictedStock.add(new UnrestrictedStockMapping(z.Matnr, z.Werks,z.Lgort, z.Labst, z.Maktx, z.Matkl, z.Wgbez, z.Lgobe, z.Name1, z.Bismt));
                }
            }    
            system.debug('unrestrictedStock----------------'+unrestrictedStock.size());        
            return unrestrictedStock;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
    }
    
     global class UnrestrictedStockMapping{       
        
        public String Matnr;
        public String Werks;
        public String Lgort;
        public String Labst;
        public String Maktx;
        public String Matkl;
        public String Wgbez;
        public String Lgobe;
        public String Name1;
        public String Bismt;
       
        public UnrestrictedStockMapping(String Matnr, String Werks, String Lgort, String Labst, String Maktx, String Matkl, String Wgbez, String Lgobe, String Name1, String Bismt){
            this.Matnr = Matnr;
            this.Werks = Werks;             
            this.Lgort = Lgort; 
            this.Labst = Labst;
            this.Maktx = Maktx;
            this.Matkl = Matkl;
            this.Wgbez = Wgbez;     
            this.Lgobe = Lgobe;
            this.Name1 = Name1;     
            this.Bismt = Bismt; 
            
        }
    }
    
 
}