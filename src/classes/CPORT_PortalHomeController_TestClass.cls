/**
* cMapping class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates cMapping class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(SeeAllData=false)
global class CPORT_PortalHomeController_TestClass{
    static Account customer ;
    static Contact contact_one ;
    static Contact contact_two ;
    static User portalUser ;
    static User portalUserExports ; 
    static Material_Master_Sap__c material ;            
    
    public static void init(){
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        
        // Export Categories
        List<sObject> listCatExport = Test.loadData(Export_Sales_Planning_Category__c.sObjectType, 'Export_Category');
        
        // Create Account
        customer = CEAT_InitializeTestData.createAccount('Test_Account','50003327');
        insert customer;
        
        // Create Contact
        contact_one = CEAT_InitializeTestData.createContact('Test_Contact',customer.Id);
        insert contact_one;
        
        // Create Contact
        contact_two = CEAT_InitializeTestData.createContact('Test_Contact1',customer.Id);
        insert contact_two; 

        material = new Material_Master_Sap__c(Name = 'MILE XL',Material_Number__c = '10002323',Material_Group__c = '2010');
        insert material;      
        
        Item_Master__c matMaster_one = CEAT_InitializeTestData.createMaterialMasterRecord('2010','1000372');
        insert matMaster_one;  
        Item_Master__c matMaster_two = CEAT_InitializeTestData.createMaterialMasterRecord('2010','1000373');
        insert matMaster_two;  
        
    }
    
    public static void init_users(){
        //Create Exports Portal User
        portalUserExports = CEAT_InitializeTestData.createPortalUserExports(contact_one.Id);
        insert portalUserExports;
        
        //Create  Portal User
        portalUser = CEAT_InitializeTestData.createPortalUserReplacement(contact_two.Id);
        insert portalUser;
    }
    public static testMethod void testStandardUser(){ 
        init();
        init_users();
        
        Test.startTest();                
       System.Test.setMock(WebServiceMock.class, new WebServiceMockImplCustInfo());  
        CPORT_PortalHomeController.getAccountInfo(customer.id);
        CPORT_CustomerOutstandingAndLimit.getAllCustomerLimitsDetails('50003327');
        CPORT_PortalHomeController cport = new CPORT_PortalHomeController();        
       
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplSDSAccounts ());
        CPORT_PortalHomeController.fetchSDSaccounts('50003327','2015-01-01','2015-07-01','new');
        
        // PART 2
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplNBP()); 
        CPORT_PortalHomeController.getNbpPriceListDetails('2010','1000372');
        
        CPORT_PortalHomeController.getAllAccounts();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplClaim()); 
        
        customer = [select Id from Account where KUNNR__c!= '' limit 1];
        CPORT_PortalHomeController.fetchClaimData('50003327','2015-05-1','2015-07-1','B0001');
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplCNDNListing());
        CPORT_PortalHomeController.fetchCreditDebitNotes(customer.id,'2015-05-1','2015-07-1','10000');
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplCustomerDue());         
        CPORT_PortalHomeController.getInvoiceDue('50003327','2015-05-1','2015-07-1','B0001');
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplCNDNListing());
        CPORT_PortalHomeController.fetchCreditDebitNotesCNDN('50003327','2015-05-1','2015-07-1','10000');
        
         System.Test.setMock(WebServiceMock.class, new WebServiceMockImplOutStanding()); 
        CPORT_PortalHomeController.fetchCustomerInvoiceDue('50003327', '2015-10-10', '2015-11-11', '100');
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpDiscounts()); 
        
        CPORT_PortalHomeController cp = new CPORT_PortalHomeController();
        PageReference pf = cp.redirectToPortalPage();
        
        // Running as Export User
        System.runAs(portalUserExports) {
           String pageName;
           pageName = CPORT_PortalHomeController.returnTransferURL();
        }

        // Running as Replacement User
        System.runAs(portalUser) {
           String pageName;
           pageName = CPORT_PortalHomeController.returnTransferURL();
        }

        
        
        CPORT_PortalHomeController.fetchDiscounts('50003327','2220');
        CPORT_PortalHomeController.getAllMaterialCodes();
        System.Test.setMock(WebServiceMock.class, new WebServiceMockDiscMaster_Prod());
        CPORT_PortalHomeController.fetchDiscountMaster('50003327','50003327');
        
         CPORT_PortalHomeController.fetchSalesRegisterValues('50003327','2015-05-1','2015-07-1','10000');
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl1());
      
       
        CPORT_PortalHomeController.getSalesRegisterValues('B0001','2015-07-10','2015-07-10');
        CPORT_PortalHomeController.fetchSalesRegisterValuesExports('50003327','2015-07-10','2015-07-10');
        CPORT_PortalHomeController.getSummerizedSalesRegisterValues('50003327','2015-07-10','2015-07-10','2220');
        CPORT_PortalHomeController.getSummerizedSalesRegisterValues('50003327','2015-07-10','2015-07-10','All');
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImplStock()); 
        CPORT_PortalHomeController.getUnrestrictedStock('B0001');
        
        CPORT_PortalHomeController.getMaterialCodes('2010');
        Test.stopTest();
    }
    
    private class WebServiceMockImplSDSAccounts implements WebServiceMock{
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            
            SapAccountSDS   scd  = new SapAccountSDS ();
            List<SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE > elist = new List<SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE >();
            SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE elem = new SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE();
            SapAccountSDS.ZWS_CUST_SDS_ACC_STATE    zws  = new SapAccountSDS.ZWS_CUST_SDS_ACC_STATE ();
            
            
            elem.PSTNG_DATE ='2200';
            elem.ITEM_TEXT ='2200';
            elem.AC_DOC_NO ='2200';
            elem.DOC_NO ='2200';
            elem.ORG_DOC_NO ='2200';
            elem.CHEC_NO ='2200';
            elem.ITEM_TEXT1 ='2200';
            elem.SP_GL_IND ='2200';
            elem.AMT_DOCCUR1 ='2200';
            elem.AMT_DOCCUR ='2200';
            elem.BALANCE ='2200';
            
            elist.add(elem);
            SapAccountSDS.TABLE_OF_ZSFDC_CUST_SDS_ACC_STATE tab = new SapAccountSDS.TABLE_OF_ZSFDC_CUST_SDS_ACC_STATE();
            tab.item=elist;
            
            SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATEResponse_element e1 = new SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATEResponse_element();
            e1.IT_FINAL =tab;
            
            SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE_element m = new SapAccountSDS.ZSFDC_CUST_SDS_ACC_STATE_element();
            m.FDATE ='2015-01-10';
            m.IT_FINAL = tab;
            m.KUNNR = '50003327';
            m.TDATE = '2015-10-10';
            
            
            response.put('response_x', e1);
            return;
        }
        
        
    }
    
         // Creating response for sap_zws_sfdc_nbp
    
     private class WebServiceMockImplNBP implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<sap_zws_sfdc_nbp.ZSFDC_NBP > zbapilList = new list<sap_zws_sfdc_nbp.ZSFDC_NBP>();
            
            sap_zws_sfdc_nbp.ZSFDC_NBP  cutBp     = new sap_zws_sfdc_nbp.ZSFDC_NBP();    
      
              cutBp.MATNR   = '1000372';
              cutBp.MATNR_DESC     = '2220';
              cutBp.TYRE_PRICE  ='2220';
              cutBp.TUBE ='2220';
              cutBp.TUBE_DESC ='2220';
              cutBp.TUBE_PRICE ='2220';
              cutBp.FLAP = '2015-2-20';
              cutBp.FLAP_DESC ='2220';
              cutBp.FLAP_PRICE ='2010';
              cutBp.SET_PRICE ='2220';            
              cutBp.PAIR_PRICE ='2220';
              cutBp.MATKL ='2010';
              cutBp.MATKL_DESC ='2220';
             
              zbapilList.add(cutBp);
            
            sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP tab = new sap_zws_sfdc_nbp.TABLE_OF_ZSFDC_NBP();
            tab.item = zbapilList ;
            
            sap_zws_sfdc_nbp.ZSFDC_NBPResponse_element e = new sap_zws_sfdc_nbp.ZSFDC_NBPResponse_element();
            e.IT_OUTPUT = tab;
            
            sap_zws_sfdc_nbp.ZSFDC_NBP_element elm = new sap_zws_sfdc_nbp.ZSFDC_NBP_element();
            
            elm.IT_OUTPUT = tab;
            elm.MATKL = '2010';
            elm.MATNR = '1000372';            
            
            response.put('response_x', e);           
            
            return;
        }     
    }
    
          // Creating response for Claim Mapping
    
     private class WebServiceMockImplClaim implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<SAP_Claim_Data.Zsd232 > zbapilList = new list<SAP_Claim_Data.Zsd232>();
            
            SAP_Claim_Data.Zsd232  cutBp     = new SAP_Claim_Data.Zsd232();    
      
              cutBp.Mandt   = '50003327';
              cutBp.Exnum     = '2220';
              cutBp.Qmnum  ='2220';
              cutBp.Mawerk ='2015-2-20';
              cutBp.Erdat ='2015-2-20';
              cutBp.Kunum ='2220';
              cutBp.Name1 = '2015-2-20';
              cutBp.Zname1 ='2220';
              cutBp.Addr ='2015-2-20';
              
              cutBp.Matnr   = '50003327';
              cutBp.Matkl     = '2220';
              cutBp.Maktx  ='2220';
              cutBp.Vkgrp ='2015-2-20';
              cutBp.Zserialnr ='2015-2-20';
              cutBp.Insdate ='2220';
              cutBp.Fegrp = '2015-2-20';
              cutBp.Fecod ='2220';
              cutBp.Text ='2015-2-20';
              
              cutBp.Actnsd ='2220';
              cutBp.Vper = '2015-2-20';
              cutBp.Kbetr ='2220';
              cutBp.Netwr ='2015-2-20';
              
              cutBp.Closs ='2220';
              cutBp.Vbeln = '2015-2-20';
              cutBp.Vbeln1 ='2220';
              cutBp.Fkdat1 ='2015-2-20';
              
              cutBp.Fkdat ='2220';
              cutBp.Inspby = '2015-2-20';
              cutBp.Docket ='2220';
              cutBp.Telf2 ='2015-2-20';
              cutBp.Bzirk ='2015-2-20';
        
              zbapilList.add(cutBp);
            
            SAP_Claim_Data.TableOfZsd232 tab = new SAP_Claim_Data.TableOfZsd232();
            tab.item = zbapilList ;
            
            SAP_Claim_Data.ZbapiClaimDataResponse_element e = new SAP_Claim_Data.ZbapiClaimDataResponse_element();
            e.ItOutput = tab;
            
            SAP_Claim_Data.ZbapiClaimData_element elm = new SAP_Claim_Data.ZbapiClaimData_element();
            elm.Bzirk = 'B0001';
            elm.ItOutput = tab;
            elm.Fdate = '2015-05-1';
            elm.Tdate = '2015-07-1';
            elm.Kunnr = '50003327';            
            
            response.put('response_x', e);           
            
            return;
        }     
    }
    
    // Creating response for Sap_Customer_Due
    
     private class WebServiceMockImplCustomerDue implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<Sap_Customer_Due.ZBAPI_CUST_DUES > zbapilList = new list<Sap_Customer_Due.ZBAPI_CUST_DUES>();
            
            Sap_Customer_Due.ZBAPI_CUST_DUES  cutBp     = new Sap_Customer_Due.ZBAPI_CUST_DUES();    
      
              cutBp.KUNNR   = '50003327';
              cutBp.NAME1   = '2220';
              cutBp.VKBUR   ='2220';
              cutBp.VKGRP ='2220';
              cutBp.VWERK ='2220';
              cutBp.BZIRK ='B0001';
              cutBp.DUEDATE = '2015-2-20';
              cutBp.BELNR ='2220';
              cutBp.BLDAT ='2015-2-20';
              cutBp.DMBTR ='2220';            
        
              zbapilList.add(cutBp);
            
            Sap_Customer_Due.TABLE_OF_ZBAPI_CUST_DUES tab = new Sap_Customer_Due.TABLE_OF_ZBAPI_CUST_DUES();
            tab.item = zbapilList ;
            
            Sap_Customer_Due.ZSFDC_GET_CUST_DUESResponse_element e = new Sap_Customer_Due.ZSFDC_GET_CUST_DUESResponse_element();
            e.IT_OUTPUT = tab;
            
            Sap_Customer_Due.ZSFDC_GET_CUST_DUES_element elm = new Sap_Customer_Due.ZSFDC_GET_CUST_DUES_element();
            elm.Bzirk = 'B0001';
            elm.IT_OUTPUT = tab;
            elm.Fdate = '2015-05-1';
            elm.Tdate = '2015-07-1';
            elm.Kunnr = '0050003327';            
            
            response.put('response_x', e);           
            
            return;
        }     
    }
    
       private class WebServiceMockImplCNDNListing implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<sap_CNDN_Listing.Zfi107  > elList = new list<sap_CNDN_Listing.Zfi107  >();
            sap_CNDN_Listing.Zfi107  elem= new sap_CNDN_Listing.ZFI107();
       
       
              elem.MANDT = '2220';
              elem.DocNo = '2220';
              elem.SpGlInd ='2220';
              elem.PstngDate ='2220';
              elem.CUSTOMER ='2220';
              elem.DocType ='KR';
              elem.VBELN ='2220';
              elem.FKART ='2220';
              elem.SFAKN ='2220';
              elem.CDNUM ='2220';
              elem.ItemText1 ='2220';
              elem.ItemText ='2220';
              elem.AmtDoccur1 ='2220';
              elem.AmtDoccur ='2220';
             
             elList.add(elem);
            
            sap_CNDN_Listing.TableOfZfi107 tom     = new sap_CNDN_Listing.TableOfZfi107();
            tom.item = elList;
            sap_CNDN_Listing.ZbapiCndnListingResponse_element elm = new sap_CNDN_Listing.ZbapiCndnListingResponse_element();
            elm.ItOutput = tom;
             
            sap_CNDN_Listing.ZbapiCndnListing_element e     = new sap_CNDN_Listing.ZbapiCndnListing_element();
            e.ItOutput = tom;
            e.KUNNR='50003327';
            e.FDATE = '2015-05-1';
            e.TDATE = '2015-07-1';       
       
            response.put('response_x', elm);            
            
            return;
        }     
    }
    
      // Creating response for Customer Outstanding
    
     private class WebServiceMockImplOutStanding implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
        
        //SapCustomerOutstandingSoapV4 tst                             = new SapCustomerOutstandingSoapV4();
        SapCustomerOutstandingSoapV4.ZWS_CUST_OUTSTANDING   zws       = new SapCustomerOutstandingSoapV4.ZWS_CUST_OUTSTANDING  ();
        SapCustomerOutstandingSoapV4.TableOfZbapiCustOs   tom             = new SapCustomerOutstandingSoapV4.TableOfZbapiCustOs ();
              
                
        List<SapCustomerOutstandingSoapV4.ZbapiCustOs> elList                  = new List<SapCustomerOutstandingSoapV4.ZbapiCustOs>();        
        SapCustomerOutstandingSoapV4.ZbapiCustOs elem = new SapCustomerOutstandingSoapV4.ZbapiCustOs();
          elem.Fkart = '2220';
          elem.Vtext = '2220';
          elem.Vbeln ='2220';
          elem.Fkdat ='2220';
          elem.Despda ='2220';
          elem.OutstandAmt ='2220';
          elem.Duedate = '2020-01-01';
          elem.OsDays = 10;
          elem.Chequeno ='2220';
          elem.Chequedate ='2220';
          elem.Curr = 'INR';
          elem.AmtDoccur = 'USD';
          elem.LocCurrcy = '2220';
            
           
            elList.add(elem);
            tom.item = elList;
            SapCustomerOutstandingSoapV4.ZbapiCustOutstandingResponse_element e = new SapCustomerOutstandingSoapV4.ZbapiCustOutstandingResponse_element();
            e.ItOutput = tom;
        //SapCustomerOutstandingSoapV4.ZbapiCustOutstanding_element e = new SapCustomerOutstandingSoapV4.ZbapiCustOutstanding_element();
        //SapCustomerOutstandingSoapV4.TableOfZbapiCustOs e = new SapCustomerOutstandingSoapV4.TableOfZbapiCustOs();
        //e.ItOutput = tom;
       // e.Kunnr = '50003327';
         //e = zws.ZbapiCustOutstanding(tom, '50003327');
         
         SapCustomerOutstandingSoapV4.ZbapiCustOutstanding_element elm = new SapCustomerOutstandingSoapV4.ZbapiCustOutstanding_element();
            elm.ItOutput = tom;
            elm.Kunnr = '50003327';
            
            response.put('response_x', e);           
            
            return;
        //response.put('response_x', e);
        }     
    }
         // Creating response for Customer Outstanding
    
     private class WebServiceMockImpDiscounts implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1 > elList = new list<SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1>();
            SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1 elem= new SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1();       
               
               elem.Crnum = '50003327';
               elem.Kunnr = '2220';
               elem.Mwskz = '2220';
               elem.RunDate ='2020-01-01' ;
               elem.Name1 = 'Test';
               elem.Office ='ZC01';
               elem.Zgroup = 'MUM';
               elem.Dwerk = 'B0001';
               elem.Apdat = '2020-01-01';
               elem.Sdsas= '2220';
               elem.Zavg= '2220';
               elem.Sdsgr= '2220';
               elem.Sdsat= '2220';
               elem.Pre= '2220';
               elem.Post= '2220';
               elem.Kwert= '2220';
               elem.Rot= '2220';
               elem.Targt= '2220';
               elem.Reqto= '2220';
               elem.Addin= '2220';
               elem.Sds= '2220';
               elem.Tov= '2220';
               elem.Crval= '2220';
               elem.Disc= '2220';
               elem.Indic= '2220';
            
            elList.add(elem);
            
            SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1 tab = new SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1();
            tab.item = elList ;
            
            SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element elm = new SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element();
            elm.ItOutput = tab;
            elm.Return_x = '';
            
            SAP_zws_ad_disc1_McS.ZsfdcAdDisc1_element e = new SAP_zws_ad_disc1_McS.ZsfdcAdDisc1_element();
            e.ItOutput = tab;
            e.Kunnr = '50003327';            
            e.Crnum = '2220';
                  
            response.put('response_x', elm);            
            
            return;
        }     
    }
        // Creating response for sap_disc_master_prod class

    private class WebServiceMockDiscMaster_Prod implements WebServiceMock {
      public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) {

        List<sap_disc_master_prod.ZsdDiscMaster> discMasterElmentList = new List<sap_disc_master_prod.ZsdDiscMaster>();
        sap_disc_master_prod.ZsdDiscMaster discMastElem = new sap_disc_master_prod.ZsdDiscMaster();
        discMastElem.Srnum = '2011';
        discMastElem.Zscheme = '2010';
        discMastElem.Sapdisc = '2010';
        discMastElem.Subcod = '2100';
        discMastElem.Ddtext = '2011';
        discMastElem.Begda1 = '2001';
        discMastElem.Endda = '2011';

        discMasterElmentList.add(discMastElem);
        sap_disc_master_prod.TableOfZsdDiscMaster tab = new sap_disc_master_prod.TableOfZsdDiscMaster();
        tab.item = discMasterElmentList;


        sap_disc_master_prod.ZsfdcDiscMasterResponse_element e = new sap_disc_master_prod.ZsfdcDiscMasterResponse_element();
        e.ItOutput = tab;
        e.Return_x = 'testString';

        sap_disc_master_prod.ZsfdcDiscMaster_element elem = new sap_disc_master_prod.ZsfdcDiscMaster_element();
        elem.ItOutput = tab;

        
        
        response.put('response_x', e);
         return;

      }
    }
  
  
      // Creating Response for Sales Register web service mapping class  
   private class WebServiceMockImpl1 implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
                
            
           list<SapSalesRegisterSoap.ZSD05A1> elList = new list<SapSalesRegisterSoap.ZSD05A1>();
           SapSalesRegisterSoap.ZSD05A1 elem         = new SapSalesRegisterSoap.ZSD05A1();
           SapSalesRegisterSoap.ZSD05A1 elem1        = new SapSalesRegisterSoap.ZSD05A1();
           SapSalesRegisterSoap.ZSD05A1 elem2        = new SapSalesRegisterSoap.ZSD05A1();
           SapSalesRegisterSoap.ZSD05A1 elem4        = new SapSalesRegisterSoap.ZSD05A1();
            //SapSalesRegisterSoap.Zsd05a elem2= new SapSalesRegisterSoap.Zsd05a();
        elem.MANDT= '2220';
        elem.VBELN= '2220';
        elem.POSNR= '2220';
        elem.VTWEG= '2220';
        elem.FKDAT= '2220';
        elem.ERDAT= '2220';
        elem.ERZET= '2220';
        elem.KUNAG= '0050003327';
        elem.NAME1= '2220';
        elem.FKART= '2220';
        elem.CANDOC= '2220';
        elem.MATNR= '2220';
        elem.ARKTX= '2220';
        elem.MATKL= '2220';
        elem.FKIMG= '2220';
        elem.NETWR= '2220';
        elem.PRE= '2220';
        elem.POST= '2220';
        elem.MWSBP= '2220';
        elem.VKBUR= '2220';
        elem.VKBURTEXT= '2220';
        elem.VKGRP= '2220';
        elem.VKGRPTEXT= '2220';
        elem.KNUMV= '2220';
        elem.KVGR1= '2220';
        elem.TEXT1= '2220';
        elem.ZTERM= '2220';
        elem.TOWN= '2220';
        elem.WERKS= '2220';
        elem.BZIRK= '2220';
        elem.KDGRP_AUFT= '2220';
        elem.WGBEZ = '2220';
        elem.NTGEW= '2220';
        elem.NBPPRICE= '2220';
        elem.CEAT1= '2220';
        elem.WAERK= '2220';
        
        elem1.MANDT= '2220';
        elem1.VBELN= '2220';
        elem1.POSNR= '2220';
        elem1.VTWEG= '2220';
        elem1.FKDAT= '2220';
        elem1.ERDAT= '2220';
        elem1.ERZET= '2220';
        elem1.KUNAG= '0050003327';
        elem1.NAME1= '2220';
        elem1.FKART= '2220';
        elem1.CANDOC= '2220';
        elem1.MATNR= '2220';
        elem1.ARKTX= '2220';
        elem1.MATKL= '2220';
        elem1.FKIMG= '2220';
        elem1.NETWR= '2220';
        elem1.PRE= '2220';
        elem1.POST= '2220';
        elem1.MWSBP= '2220';
        elem1.VKBUR= '2220';
        elem1.VKBURTEXT= '2220';
        elem1.VKGRP= '2220';
        elem1.VKGRPTEXT= '2220';
        elem1.KNUMV= '2220';
        elem1.KVGR1= '2220';
        elem1.TEXT1= '2220';
        elem1.ZTERM= '2220';
        elem1.TOWN= '2220';
        elem1.WERKS= '2220';
        elem1.BZIRK= '2220';
        elem1.KDGRP_AUFT= '2220';
        elem1.WGBEZ = '2220';
        elem1.NTGEW= '2220';
        elem1.NBPPRICE= '2220';
        elem1.CEAT1= '2220';
        elem1.WAERK= '2220';
        
        elem2.MANDT= '2220';
        elem2.VBELN= '2220';
        elem2.POSNR= '2220';
        elem2.VTWEG= '2220';
        elem2.FKDAT= '2220';
        elem2.ERDAT= '2220';
        elem2.ERZET= '2220';
        elem2.KUNAG= '0050003327';
        elem2.NAME1= '2220';
        elem2.FKART= '2220';
        elem2.CANDOC= '2220';
        elem2.MATNR= '2220';
        elem2.ARKTX= '2220';
        elem2.MATKL= '2220';
        elem2.FKIMG= '2220';
        elem2.NETWR= '2220';
        elem2.PRE= '2220';
        elem2.POST= '2220';
        elem2.MWSBP= '2220';
        elem2.VKBUR= '2220';
        elem2.VKBURTEXT= '2220';
        elem2.VKGRP= '2220';
        elem2.VKGRPTEXT= '2220';
        elem2.KNUMV= '2220';
        elem2.KVGR1= '2220';
        elem2.TEXT1= '2220';
        elem2.ZTERM= '2220';
        elem2.TOWN= '2220';
        elem2.WERKS= '2220';
        elem2.BZIRK= '2220';
        elem2.KDGRP_AUFT= '2220';
        elem2.WGBEZ = '2010';
        elem2.NTGEW= '2220';
        elem2.NBPPRICE= '2220';
        elem2.CEAT1= '2220';
        elem2.WAERK= '2220'; 

        elem4.MANDT= '2220';
        elem4.VBELN= '2220';
        elem4.POSNR= '2220';
        elem4.VTWEG= '2220';
        elem4.FKDAT= '2220';
        elem4.ERDAT= '2220';
        elem4.ERZET= '2220';
        elem4.KUNAG= '0050003327';
        elem4.NAME1= '2220';
        elem4.FKART= '2220';
        elem4.CANDOC= '2220';
        elem4.MATNR= '2220';
        elem4.ARKTX= '2220';
        elem4.MATKL= '2220';
        elem4.FKIMG= null;
        elem4.NETWR= '2220';
        elem4.PRE= '2220';
        elem4.POST= '2220';
        elem4.MWSBP= '2220';
        elem4.VKBUR= '2220';
        elem4.VKBURTEXT= '2220';
        elem4.VKGRP= '2220';
        elem4.VKGRPTEXT= '2220';
        elem4.KNUMV= '2220';
        elem4.KVGR1= '2220';
        elem4.TEXT1= '2220';
        elem4.ZTERM= '2220';
        elem4.TOWN= '2220';
        elem4.WERKS= '2220';
        elem4.BZIRK= '2220';
        elem4.KDGRP_AUFT= '2220';
        elem4.WGBEZ = '2220';
        elem4.NTGEW= '2220';
        elem4.NBPPRICE= '2220';
        elem4.CEAT1= '2220';
        elem4.WAERK= '2220';
            
            elList.add(elem);
            elList.add(elem1);
            elList.add(elem2);
            elList.add(elem4);
            
            SapSalesRegisterSoap.TABLE_OF_ZSD05A1 ee = new SapSalesRegisterSoap.TABLE_OF_ZSD05A1 ();
            ee.item = elList;
             
            SapSalesRegisterSoap.ZTVFKT zt= new SapSalesRegisterSoap.ZTVFKT ();
            zt.FKART= '2200';
            zt.VTEXT = '2200';
            
            
           /* SapSalesRegisterSoap.ZTVFKT zt1= new SapSalesRegisterSoap.ZTVFKT ();
            zt1.FKART= '2200';
            zt1.VTEXT = '2220';*/
            
            list<SapSalesRegisterSoap.ZTVFKT > ztList= new list<SapSalesRegisterSoap.ZTVFKT > ();
            ztList.add(zt);
            //ztList.add(zt1);
            
            SapSalesRegisterSoap.TABLE_OF_ZTVFKT tes = new SapSalesRegisterSoap.TABLE_OF_ZTVFKT ();
            tes.item =ztList;
            
            SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EXResponse_element  zws = new SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EXResponse_element  ();
            zws.IT_OUTPUT = ee;
            zws.IT_FINAL= tes;
            
            SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EX_element zws1= new  SapSalesRegisterSoap.ZSFDC_SALES_REGISTER_EX_element ();
            zws1.IT_OUTPUT = ee;
            zws1.IT_FINAL= tes; 
            zws1.KUNNR=  '50003327';
            zws1.FDATE= '2015-05-1';
            zws1.TDATE= '2015-07-1';
            
            response.put('response_x', zws);
                  
            return;
        }     
    }
    
    	 // Creating response for SapUnrestrictedStockSoap
    
     private class WebServiceMockImplStock implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<SapUnrestrictedStockSoap.Bapireturn > zbapilList = new list<SapUnrestrictedStockSoap.Bapireturn>();
            
            SapUnrestrictedStockSoap.Bapireturn  cutBp     = new SapUnrestrictedStockSoap.Bapireturn();    
      
              cutBp.Type_x   = '1000372';
              cutBp.Code     = '2220';
              cutBp.Message  ='2220';
              cutBp.LogNo ='2220';
              cutBp.LogMsgNo ='2220';
              cutBp.MessageV1 ='2220';
              cutBp.MessageV2 = '2220';
              cutBp.MessageV3 ='2220';
              cutBp.MessageV4 ='2010';            
             
             zbapilList.add(cutBp);
            
            SapUnrestrictedStockSoap.TableOfBapireturn tab = new SapUnrestrictedStockSoap.TableOfBapireturn();
            tab.item = zbapilList ;
            
            list<SapUnrestrictedStockSoap.ZbapiMatStock > zbapilList1 = new list<SapUnrestrictedStockSoap.ZbapiMatStock>();
            
            SapUnrestrictedStockSoap.ZbapiMatStock  cutBp1     = new SapUnrestrictedStockSoap.ZbapiMatStock();    
      
              cutBp1.Matnr   = '1000372';
              cutBp1.Werks     = '2220';
              cutBp1.Lgort  ='2220';
              cutBp1.Labst ='2220';
              cutBp1.Maktx ='2220';
              cutBp1.Matkl ='2010';
              cutBp1.Wgbez = '2220';
              cutBp1.Lgobe ='2220';
              cutBp1.Name1 ='2010'; 
              cutBp1.Bismt ='2010';           
            
             zbapilList1.add(cutBp1);
             
            SapUnrestrictedStockSoap.TableOfZbapiMatStock tab1 = new SapUnrestrictedStockSoap.TableOfZbapiMatStock();
            tab1.item = zbapilList1 ;
            
            SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element e = new SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element();
            e.Itab = tab1;
            e.Return_x = tab;
            
            SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element elm = new SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element();
            elm.Bzirk = 'B0001';
            elm.Itab = tab1;
            elm.MATKL = '2010';
            elm.MATNR = '1000372';            
          
            response.put('response_x', e);           
            
            return;
        }     
    }
    
    	 // Creating response for Customer Info
    
     private class WebServiceMockImplCustInfo implements WebServiceMock
    {        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
         
            list<SapCustomerLimitSoap.ZbapiCustLimit > cutlList = new list<SapCustomerLimitSoap.ZbapiCustLimit>();
            SapCustomerLimitSoap.ZbapiCustLimit  cutl     = new SapCustomerLimitSoap.ZbapiCustLimit();    
      
              cutl.Customer = '50003327';
              cutl.Name1 = '2220';
              cutl.Deposit ='2220';
              cutl.Permisbleamt ='2220';
              cutl.Limitavailable ='2220';
              cutl.Advance ='2220';
              cutl.Totoutstand = '2020';
              cutl.Netoutstand ='2220';
              cutl.Totoverdue ='2220';
                
                cutlList.add(cutl);
            
            SapCustomerLimitSoap.TableOfZbapiCustLimit tab = new SapCustomerLimitSoap.TableOfZbapiCustLimit();
            tab.item = cutlList ;
           
            list<SapCustomerLimitSoap.Bapireturn> cutBplList = new list<SapCustomerLimitSoap.Bapireturn  >();
            SapCustomerLimitSoap.Bapireturn   cutBp     = new SapCustomerLimitSoap.Bapireturn ();
           
              cutBp.Type_x = '50003327';
              cutBp.Code = '2220';
              cutBp.Message ='2220';
              cutBp.LogNo ='2220';
              cutBp.MessageV1 ='2220';
              cutBp.LogMsgNo ='2220';
              cutBp.MessageV2 = '2020.7';
              cutBp.MessageV3 ='2220';
              cutBp.MessageV4 ='2220';
                
            cutlList.add(cutl);
            
            cutBplList.add(cutBp);
            
            SapCustomerLimitSoap.TableOfBapireturn tab2 = new SapCustomerLimitSoap.TableOfBapireturn();
            tab2.item = cutBplList;
            
            SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element elm = new SapCustomerLimitSoap.ZbapiGetCustLimitResponse_element();
            elm.ItCust = tab;
            elm.Return_x = tab2;
            
            SapCustomerLimitSoap.ZbapiGetCustLimit_element e = new SapCustomerLimitSoap.ZbapiGetCustLimit_element();
            e.ItCust = tab;
            e.Return_x = tab2;
            e.Customer = '50003327';            
            
            response.put('response_x', elm);           
            
            return;
        }     
    }
}