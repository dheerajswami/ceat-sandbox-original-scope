global class CPORT_CustomerDue {
        /*
* Auther  :- Neha Mishra
* Purpose :- Fetch Custumer Due Details from SAP 
*
*
*/
    
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;

    public CPORT_CustomerDue() {}

    /*
     This method will fetch Customer Dues
     */
     WebService static List<CustomerDueMapping> getAllCustomerDueDetails(String cusNum, String fDate, String tDate,String territory){

        List<CustomerDueMapping> customerDue = new List<CustomerDueMapping>();
		String customerId = '';
        try{
		
		if(cusNum != null && cusNum != ''){
       	    customerId                               = UtilityClass.addleadingZeros(cusNum,Integer.valueOf(CUST_ID_LENGTH));
        }else{
            customerId = '';
        }
		
        SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        sap_Customer_Due_v2  scd                           = new sap_Customer_Due_v2();
        sap_Customer_Due_v2.zws_sfdc_get_cust_dues   zws       = new sap_Customer_Due_v2.zws_sfdc_get_cust_dues  ();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;
        sap_Customer_Due_v2.TABLE_OF_ZBAPI_CUST_DUES   tom             = new sap_Customer_Due_v2.TABLE_OF_ZBAPI_CUST_DUES ();
              
                
        List<sap_Customer_Due_v2.ZBAPI_CUST_DUES> m                    = new List<sap_Customer_Due_v2.ZBAPI_CUST_DUES>();        
        
        tom.item = m;
        
        sap_Customer_Due_v2.TABLE_OF_ZBAPI_CUST_DUES e = new sap_Customer_Due_v2.TABLE_OF_ZBAPI_CUST_DUES();
        system.debug('fdate--'+fdate);
            system.debug('customerId--'+customerId);
            system.debug('territory--'+territory);
        
            
            
        e = zws.ZSFDC_GET_CUST_DUES(territory, fdate, tom, customerId, tDate);
            system.debug('e--'+e);
            //system.assertEquals(e,null);
        if(e.item != null){            
            for(sap_Customer_Due_v2.ZBAPI_CUST_DUES z : e.item) {
                customerDue.add(new CustomerDueMapping(z.KUNNR,z.NAME1,z.VKBUR,z.VKGRP,z.VWERK,z.BZIRK,z.BELNR,z.BLDAT,z.DUEDATE,z.DMBTR));
            }
        }
            
            return customerDue;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Exception -'+ e.getMessage());
            return null;
        }
        return null;
     }

    global class CustomerDueMapping{
        public String Kunnr {get; set;}
        public String Name1 {get; set;}
        public String Vkbur {get; set;}
        public String Vkgrp {get; set;}
        public String Vwerk {get; set;}
        public String Bzirk {get; set;}
        public String Belnr {get; set;}
        public String Bldat {get; set;}
        public String DueDate {get; set;}
        public String Dmbtr {get; set;}
        public CustomerDueMapping(String Kunnr,String Name1,String Vkbur,String Vkgrp, String Vwerk, 
                                  String Bzirk, String Belnr, String Bldat, String DueDate, String Dmbtr){
            this.Kunnr = Kunnr;
            this.Name1 = Name1;
            this.Vkbur = Vkbur;
            this.Vkgrp = Vkgrp;
            this.Vwerk = Vwerk;
            this.Bzirk = Bzirk;
            this.Belnr = Belnr;
            this.Bldat = Bldat;
            this.DueDate = DueDate;
            this.Dmbtr = Dmbtr;
        }
    }

}