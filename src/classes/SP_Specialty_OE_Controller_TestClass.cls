@isTest 
private class SP_Specialty_OE_Controller_TestClass{
    static testmethod void testLoadData() {
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBURep = new Territory2 ();
        Territory2 terrtypeBUSpe = new Territory2 ();
        Territory2 terrtypeBUSpeCountry = new Territory2();
        Territory2 terrtypeZoneSpeDel = new Territory2 ();
        Territory2 terrtypeZoneSpeW1 = new Territory2 ();
        Territory2 terrtypeZoneSpeAUR = new Territory2 ();
        Territory2 terrtypeZoneSpeRAN = new Territory2 ();
        List<Sales_Planning__c> listOFSPSpeOEForZone = new list<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeForZonequery = new list<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeDealer = new list<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPSpeDealerquery = new list<Sales_Planning__c>();
        List<SP_HandlerFor_SP_Specialty_OE_Controller.DealerWrapper> Sprecords = new List<SP_HandlerFor_SP_Specialty_OE_Controller.DealerWrapper>();
        Set<ID> setOFID = new set<ID>();
        set<ID> setOFIDDeaker =  new set<ID>();
        Map<String,String> mapOfSalesPlanningValue = new Map<String,String>();
        Map<String,String> mapOfSalesPlanning = new Map<String,String>();
        String teststring ='';
        Account accrecord = new Account();
        Account accrecord1 = new Account();
        Account accrecord2 = new Account();
        Account accrecord3 = new Account();
        user userRMData = new user();
        user userGMData = new user();
        id terrTypeBU ;
        id terrTypeRO ;
        id terrTypeTerr ;
        id terrTypeZone ;
        id terrTypeCountry ;
        UserTerritory2Association spRMuser = new UserTerritory2Association ();
        UserTerritory2Association spGMuser   = new UserTerritory2Association();
         // Load the test Sales planning Staging from the static resource
        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        insert terrModel;
        
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
        system.debug(listTerritoryType+'listTerritoryType');
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            if(t.DeveloperName == 'BUTest'){
                terrTypeBU = t.id;
            }
            if(t.DeveloperName == 'ROTestTest'){
                terrTypeRO = t.id;
            }
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            if(t.DeveloperName == 'ZoneTest'){
                terrTypeZone = t.id;
            }
            if(t.DeveloperName == 'CountryTest'){
                terrTypeCountry = t.id;
            }
        }
        system.debug(terrTypeBU+'terrTypeBU');
        terrtypeBUSpe =  CEAT_InitializeTestData.createTerritory('Specialty - Replacement','Specialty_Replacement',terrTypeBU,terrModel.id,null);
        insert terrtypeBUSpe;
        terrtypeBUSpeCountry =  CEAT_InitializeTestData.createTerritory('HO','HO_test',terrTypeCountry,terrModel.id,terrtypeBUSpe.id);
        insert terrtypeBUSpeCountry;
        terrtypeZoneSpeDel =  CEAT_InitializeTestData.createTerritory('Del','Del_test',terrTypeZone,terrModel.id,terrtypeBUSpeCountry.id);
        insert terrtypeZoneSpeDel;
        terrtypeZoneSpeW1 =  CEAT_InitializeTestData.createTerritory('W1','W1_test',terrTypeZone,terrModel.id,terrtypeBUSpeCountry.id);
        insert terrtypeZoneSpeW1;
        terrtypeZoneSpeRAN =  CEAT_InitializeTestData.createTerritory('Del','DelZone_test',terrTypeRO,terrModel.id,terrtypeZoneSpeDel.id);
        insert terrtypeZoneSpeRAN;
        terrtypeZoneSpeAUR =  CEAT_InitializeTestData.createTerritory('AUR','AUR_test',terrTypeRO,terrModel.id,terrtypeZoneSpeW1.id);
        insert terrtypeZoneSpeAUR;
        userGMData = CEAT_InitializeTestData.createUser('mm','TestGM','kishlay.mathur@extentor.com','kishlayGM@ceat.com','1234','Specialty',null,'GM_Specialty');
        insert userGMData;
        userRMData = CEAT_InitializeTestData.createUser('mm','CCCCC','kishlay.mathur@extentor.com','kishlayRM@ceat.com','12235','Specialty',null,'RM_Specialty');
        insert userRMData;
        spGMuser = CEAT_InitializeTestData.createUserTerrAsso(userGMData.id, terrtypeBUSpeCountry.id, 'GM_Specialty');
        insert spGMuser;
        spRMuser = CEAT_InitializeTestData.createUserTerrAsso(userRMData.id, terrtypeZoneSpeW1.id, 'RM_Specialty');
        insert spRMuser;
        System.runAs(userRMData) {
              accrecord = CEAT_InitializeTestData.createAccountSpecialty('Specialty','AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081','5');
              accrecord1 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','PATNA TYRE HOUSE', '50014542', 'PAT', 'ZE01', 'C0081','5');
              accrecord2 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081','5');
              accrecord3 = CEAT_InitializeTestData.createAccountSpecialty('Specialty','TYRE AVENUE', '50003565', 'PAT', 'ZE01', 'C0081','5');
              insert accrecord;
              insert accrecord1;
              insert accrecord2;
              insert accrecord3;
              system.debug(accrecord+'accrecord');
             ACW__c acw=new ACW__C();
             Id lockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName ='Locking' Limit 1].Id;
         
             acw.Page__c='SP_Specialty_OE_v1';
             acw.Sales_Planning__c=true;
             acw.From_Date__c= system.today(); 
             acw.To_Date__c = system.today()+1;  
             acw.RecordTypeId =lockingRecType;//locking Recored type 012O00000000bkYIAQ
             insert acw;  
              Server_Url__c url= new Server_Url__c(Url__c = 'https://cs5.salesforce.com');
              insert url;
              listOFSPSpeOEForZone = CEAT_InitializeTestData.createSalesPlanningsSpeOEForcastZone('Specialty_OE_Forcast');
              insert listOFSPSpeOEForZone;
              for(Sales_Planning__c sp : listOFSPSpeOEForZone){
                    setOFID.add(sp.id);
                 }
             listOFSPSpeForZonequery = [Select Discount_On_NBP__c,id,Target_Quantity__c,Category__c from Sales_Planning__c Where id IN:setOFID];
             listOFSPSpeDealer = CEAT_InitializeTestData.createSalesPlanningsSpeOEDealer('Specialty_OE');
             insert listOFSPSpeDealer;
             system.debug(listOFSPSpeDealer+'listOFSPSpeDealer');
             Sprecords = SP_Specialty_OE_Controller.getDealersOfExportManager('W1',userRMData.id);
             teststring = '';
             for(Sales_Planning__c sp : listOFSPSpeDealer){
                setOFIDDeaker.add(sp.id);
             }
             listOFSPSpeDealerquery = [Select id,Value__c,Target_Quantity__c,Dealer_CustNumber__c,Dealer_Name__c,SPExternalIDTL__c,SKU__c,Zone__c from Sales_Planning__c Where id IN:setOFIDDeaker];
             system.debug(listOFSPSpeDealerquery+'listOFSPSpeDealerquery');
             for(Sales_Planning__c sp : listOFSPSpeDealerquery){
                if(sp.SPExternalIDTL__c == '50014542100128W132015'){
                    teststring = sp.SPExternalIDTL__c+'@@'+sp.Dealer_CustNumber__c+'@@'+sp.Zone__c+'@@'+sp.SKU__c+'@@'+sp.Dealer_Name__c;
                    sp.Target_Quantity__c= 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(sp.Target_Quantity__c));
                    sp.Value__c = 12345;
                    mapOfSalesPlanningValue.put(teststring,String.ValueOf(sp.Value__c));
                }
             }
             SP_Specialty_OE_Controller.saveDealerExportRecord (mapOfSalesPlanningValue,mapOfSalesPlanning,'W1',userRMData.id);
             SP_Specialty_OE_Controller.saveAsDraftDealerExportRecord (mapOfSalesPlanningValue, mapOfSalesPlanning,'W1',userRMData.id);
        }
    }
}