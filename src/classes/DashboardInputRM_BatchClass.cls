/************************************************************************************************************
    Author  : Sneha Agrawal
    Date    : 9/7/2015
    Purpose : Dashboard Input RM Batch Class
*************************************************************************************************************/
global with sharing class DashboardInputRM_BatchClass implements Database.Batchable<sObject> {
   
   integer month; 
   
  //*********************STRINGS********************************************************    
    
    String rmRecType                 = system.Label.RM_RecType;
    string currentMonth;
    string currentYear;
    Id rmRecTypeId      ;
    Id rmInputRecTypeId ;
    String rmRole                        = system.Label.RM_Role;  
    String Replacements                  = 'Replacements';
    String actionLogRecType              = system.Label.ActionLogInputRecordType;
    String efleetRecType                 = system.Label.Efleet_RecType;
    string TLInputRecType                = system.Label.Input_RecType; 
    global static String month_val;
    global static String year_val;
    global static Boolean active_val;
    String DSSString = '';
    
 //*****************************LISTS***************************************************     
    list<Territory2Type> terriType                         = new list<Territory2Type>();
    
    list<Dashboard_Master__c> dashboardMaster = new list<Dashboard_Master__c>();
    list<Dashboard_Input_Score__c> inputDashboard;
    list<Account> listAccountTerr = new list<Account>();
    list<Dashboard_Weightage_Master__c> dwm = new list<Dashboard_Weightage_Master__c>();
    list<Dashboard_Summary_Score__c> listofDSS       = new list<Dashboard_Summary_Score__c>(); 
    list<Dashboard_Input_Score__c> dashboardInputScore     = new List<Dashboard_Input_Score__c>();
    list<AggregateResult> inputDashboardScore =  new list<AggregateResult>();
    //list<Dashboard_Weightage_Master__c> dwmActionLog;
    //list<Dashboard_Weightage_Master__c> dwmEfleet;
    
 //*********************************SETS***********************************
    Set<Id> accountIds                                      = new Set<Id>();
    set<id> terrTypeID                                      = new set<Id>();
    set<id> territoryID                                     = new set<ID>();
    Set<String> regionSet                                   = new Set<String>();
    set<Id> CASLUserID                                      = new set<ID>();
 //*******************************MAPS*******************************************   
    
    
     map<String,Integer> tlinputTargetMap = new map<String,Integer>();
     map<String,Integer> tlinputTargetMTDMap = new map<String,Integer>();
     map<String,Integer> tlinputActualMap = new map<String,Integer>();
     
     map<String,Integer> tldinputTargetMap = new map<String,Integer>();
     map<String,Integer> tldinputTargetMTDMap = new map<String,Integer>();
     map<String,Integer> tldinputActualMap = new map<String,Integer>();
       
     map<String,Integer> cstlinputTargetMap = new map<String,Integer>();
     map<String,Integer> cstlinputTargetMTDMap = new map<String,Integer>();
     map<String,Integer> cstlinputActualMap = new map<String,Integer>();
     map<String,Dashboard_Summary_Score__c> mapOfRegionAndDSS = new map<String,Dashboard_Summary_Score__c>();
      
  /*
        All initialization in Constructor
  */
  global DashboardInputRM_BatchClass(){
    
     //dashboardMaster            = new list<Dashboard_Master__c>();  
     //listAccountTerr            = new list<Account>(); 
     
     //dwm                        = new list<Dashboard_Weightage_Master__c>();
    
     //tlinputTargetMap       = new map<String,Integer>();
     //tlinputTargetMTDMap    = new map<String,Integer>();
     //tlinputActualMap       = new map<String,Integer>();
     
     /*tldinputTargetMap      = new map<String,Integer>();
     tldinputTargetMTDMap   = new map<String,Integer>();
     tldinputActualMap      = new map<String,Integer>();*/
       
     /*cstlinputTargetMap     = new map<String,Integer>();
     cstlinputTargetMTDMap  = new map<String,Integer>();
     cstlinputActualMap     = new map<String,Integer>();*/
     
      month                       = (Date.today()).month();                       
                 
         /*
                To convert current month into month name
         */
            

            
              
      currentMonth = (month_val == null)?UtilityClass.fetchMonthName(month):month_val;  
      currentYear  =  (year_val == null)?String.valueOf((Date.today()).year()):year_val;     
     
     
     terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'RO' limit 1];
     for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
     }     
     dashboardMaster       = [SELECT id,Role__c,Active__c From Dashboard_Master__c WHERE Active__c = true AND Role__c=:rmRole];
     rmRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:rmRecType Limit 1].Id;
     rmInputRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:rmRecType Limit 1].Id;
                  
     /* Getting all PJP Dashboard Weightage records */
     Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
     
     Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:actionLogRecType Limit 1].Id;
            
     Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:efleetRecType Limit 1].Id;
     Id distributorTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.TL_Distributor Limit 1].Id;
     if(month_val == null && year_val == null && active_val == null){
        dwm                    = [Select Id,Category__c,Parameters_Inout__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c where Testing__c = true AND  (RecordTypeId=:inputRecTypeId OR RecordTypeId=:efleetRecTypeId OR RecordTypeId=:actionLogRecTypeId OR RecordTypeId=:distributorTypeId)  AND Dashboard_Master__c =: dashboardMaster[0].id AND Role__c=:rmRole];
     }else if(month_val != null && year_val != null && active_val != null){
        dashboardInputScore  = [SELECT ID,Achievement__c,Region_Code__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Dashboard_Weightage_Master__r.Dashboard_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c,ScoreMTD__c FROM Dashboard_Input_Score__c where Region_Code__c != null AND Month__c =:month_val AND Year__c =: year_val AND Dashboard_Weightage_Master__r.Testing__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Active__c =: active_val AND Dashboard_Weightage_Master__r.Dashboard_Master__r.Role__c =: rmRole Limit 1];
        dwm                    = [Select Id,Category__c,Parameters_Inout__c,Role__c,Weightage__c from Dashboard_Weightage_Master__c where (RecordTypeId=:inputRecTypeId OR RecordTypeId=:efleetRecTypeId OR RecordTypeId=:actionLogRecTypeId OR RecordTypeId=:distributorTypeId)  AND Dashboard_Master__c =: dashboardInputScore[0].Dashboard_Weightage_Master__r.Dashboard_Master__c AND Role__c=:rmRole AND Weightage__c  > 0];
     }
               
  }
     
  /* Start method that will fetch all territories and pass it to execute method (batch size 1)*/   
  global Database.QueryLocator start(Database.BatchableContext BC){        
          
         String query;
         String[] filters         =  new String[]{'SPRP%'};
         
         /* Querying all territories under Replacement (Extra query to avoid RMs Under Specialty)*/
         list<Territory2>    terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND ParentTerritory2.ParentTerritory2.ParentTerritory2.DeveloperName =: Replacements]; 
         for(Territory2 terr :terrList){
            territoryID.add(terr.id);
         }
         query                            = 'SELECT id,UserId,Territory2.DeveloperName,Territory2.name,RoleInTerritory2,IsActive,Territory2Id from UserTerritory2Association WHERE Territory2Id In: territoryID AND RoleInTerritory2=:rmRole AND (NOT Territory2.DeveloperName LIKE :  filters'+')'; 
         return Database.getQueryLocator(query);   
               
   }  
  global void execute(Database.BatchableContext BC,list<sObject> scope) {   
      set<string> territorySet = new set<string>();
      list<string> terrNames = new list<string>();
      map<string,list<string>> terrMap = new map<string,list<string>>();
      list<string> cstlTerrList ;
      set<string> cstlTerrSet = new  set<string>();
      
      string userId     = '';
      string regionCode = '';
      string cstlTerr   = '' ;

      for(sObject temp : scope){
          if(temp != null) {
           UserTerritory2Association sps              = (UserTerritory2Association)temp;
           regionSet.add(sps.Territory2.DeveloperName);            
           regionCode = sps.Territory2.DeveloperName;
           userId = sps.UserId;
          }
      }  
      system.debug('------regionSet'+regionSet);
      // Getting list of territories under region or RM      
      list<Territory2>    territoryList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE ParentTerritory2.DeveloperName IN: regionSet]; 
      
      if(territoryList.size()>0){
        for(Territory2 terr : territoryList){           
           territorySet.add(terr.DeveloperName);           
        } 
      } 
      list<UserTerritory2Association> userAssocList = [SELECT id,UserId,Territory2.Name,RoleInTerritory2 from UserTerritory2Association where Territory2.Name In: territorySet AND RoleInTerritory2 = 'CSTL'] ;
      string mapKey ='';
      if(userAssocList.size()>0){
        for(UserTerritory2Association assoc : userAssocList){            
              mapKey = assoc.roleInTerritory2+'@@'+assoc.UserId; //CSTL + userId
              CASLUserID.add(assoc.UserId);           
             if(terrMap.containsKey(mapKey)){
               cstlTerrList = terrMap.get(mapKey);
               cstlTerrList.add(assoc.Territory2.Name);             
               terrMap.put(mapKey,cstlTerrList);
             }else{
               cstlTerrList = new list<string>();
               cstlTerrList.add(assoc.Territory2.Name);             
               terrMap.put(mapKey,cstlTerrList);
             }
        }
      }
      if(terrMap.size() > 0){
          for(string key : terrMap.keySet()){
            cstlTerr = '';
            terrNames = terrMap.get(key);
            terrNames.sort();
                   for(string terr : terrNames){
                        if(cstlTerr =='' || cstlTerr ==null){
                            cstlTerr = terr;
                        }else{
                            cstlTerr = cstlTerr +','+ terr ;
                        }
                  }
                  system.debug('-----------'+cstlTerr);
                  cstlTerrSet.add(cstlTerr);
          }
      }    
     
    // ************************************************************ TL Roll Up***************************************************************************    
       list<Dashboard_Input_Score__c> dashboardInputScoreTL                = new List<Dashboard_Input_Score__c>(); 
       if(territorySet.size()>0){
            dashboardInputScoreTL  = [SELECT ID,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordType.Name='TL' AND Territory_Code__c IN: territorySet  AND  Month__c =: currentMonth AND Year__c =: currentYear ];
       }
       string tlKey = '';
       
       
      if(dashboardInputScoreTL.size()>0){ 
       for(Dashboard_Input_Score__c inpScore : dashboardInputScoreTL){
           tlKey = inpScore.Category__c.trim() + inpScore.Parameters__c.trim() ;
           Integer totalTarget    = 0;
           Integer totalTargetMTD = 0;
           Integer totalActual    = 0;
           
           if(tlinputTargetMap.containsKey(tlKey)){
                if(tlinputTargetMap.get(tlKey)!=null){
                   totalTarget = tlinputTargetMap.get(tlKey) + integer.valueof(inpScore.Total_Monthly_Target__c);
                }else{
                    totalTarget = 0 + integer.valueof(inpScore.Total_Monthly_Target__c);
                }
                tlinputTargetMap.put(tlKey,totalTarget);
           }else{
               totalTarget = integer.valueof(inpScore.Total_Monthly_Target__c);
               tlinputTargetMap.put(tlKey,totalTarget);
           }           
           
           if(tlinputTargetMTDMap.containsKey(tlKey)){
               if(tlinputTargetMTDMap.get(tlKey)!=null){
                  totalTargetMTD = tlinputTargetMTDMap.get(tlKey) + integer.valueof(inpScore.Total_MTD_Target__c);
               }else{
                  totalTargetMTD = 0 + integer.valueof(inpScore.Total_MTD_Target__c);
               } 
               tlinputTargetMTDMap.put(tlKey,totalTargetMTD);
           }else{
               totalTargetMTD = integer.valueof(inpScore.Total_MTD_Target__c);
               tlinputTargetMTDMap.put(tlKey,totalTargetMTD);
           }
           
           if(tlinputActualMap.containsKey(tlKey)){
                if(tlinputActualMap.get(tlKey)!=null){
                    totalActual = tlinputActualMap.get(tlKey) + integer.valueof(inpScore.Total_Actual_MTD__c);
                }else{
                    totalActual = 0 + integer.valueof(inpScore.Total_Actual_MTD__c);
                }
                tlinputActualMap.put(tlKey,totalActual);
           }else{
               totalActual = integer.valueof(inpScore.Total_Actual_MTD__c);
               tlinputActualMap.put(tlKey,totalActual);
           }
         }
       }   
    
    // ************************************************************ TLD ************************************************************************** 
       list<Dashboard_Input_Score__c> dashboardInputScoreTLD                = new List<Dashboard_Input_Score__c>(); 
       if(territorySet.size()>0){
            dashboardInputScoreTLD  = [SELECT ID,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordType.Name='TLD' AND Territory_Code__c IN: territorySet  AND  Month__c =: currentMonth AND Year__c =: currentYear ];
       }
       string tldKey = '';
       
      if(dashboardInputScoreTLD.size()>0){
       for(Dashboard_Input_Score__c inpScore : dashboardInputScoreTLD){
           tldKey = inpScore.Category__c.trim() + inpScore.Parameters__c.trim();
           
           Integer totalTargetTLD    = 0;
           Integer totalTargetMTDTLD = 0;
           Integer totalActualTLD    = 0;
           
           if(tldinputTargetMap.containsKey(tldKey)){
              if(tldinputTargetMap.get(tldKey)!=null){
                   totalTargetTLD = tldinputTargetMap.get(tldKey) + integer.valueof(inpScore.Total_Monthly_Target__c);
              }else{
                   totalTargetTLD = 0 + integer.valueof(inpScore.Total_Monthly_Target__c);
              }
              tldinputTargetMap.put(tldKey,totalTargetTLD);
           }else{
               totalTargetTLD = integer.valueof(inpScore.Total_Monthly_Target__c);
               tldinputTargetMap.put(tldKey,totalTargetTLD);
           }           
           
           if(tldinputTargetMTDMap.containsKey(tldKey)){
               if(tldinputTargetMTDMap.get(tldKey)!=null){
                    totalTargetMTDTLD = tldinputTargetMTDMap.get(tldKey) + integer.valueof(inpScore.Total_MTD_Target__c);
               }else{
                    totalTargetMTDTLD = 0 + integer.valueof(inpScore.Total_MTD_Target__c);
               }
               tldinputTargetMTDMap.put(tldKey,totalTargetMTDTLD);
           }else{
               totalTargetMTDTLD = integer.valueof(inpScore.Total_MTD_Target__c);
               tldinputTargetMTDMap.put(tldKey,totalTargetMTDTLD);
           }
           
           if(tldinputActualMap.containsKey(tldKey)){
               if(tldinputActualMap.get(tldKey)!=null){
                    totalActualTLD = tldinputActualMap.get(tldKey) + integer.valueof(inpScore.Total_Actual_MTD__c);
               }else{
                    totalActualTLD = 0 + integer.valueof(inpScore.Total_Actual_MTD__c);
               }
               tldinputActualMap.put(tldKey,totalActualTLD);
           }else{
               totalActualTLD = integer.valueof(inpScore.Total_Actual_MTD__c);
               tldinputActualMap.put(tldKey,totalActualTLD);
           }
       } 
     }  
       
    // ************************************************************ CSTL *************************************************************************    
 
     list<Dashboard_Input_Score__c> dashboardInputScoreCSTL               = new List<Dashboard_Input_Score__c>(); 
     if(CASLUserID.size()>0){
         dashboardInputScoreCSTL  = [SELECT ID,OwnerID,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordType.Name='CSTL' AND OwnerID IN:CASLUserID  AND  Month__c =: currentMonth AND Year__c =: currentYear ];
     }
       string cstlKey = '';
      
     if(dashboardInputScoreCSTL.size()>0){ 
       for(Dashboard_Input_Score__c inpScore : dashboardInputScoreCSTL){
           cstlKey = inpScore.Category__c.trim() + inpScore.Parameters__c.trim() ;
           
           Integer totalTargetCSTL    = 0;
           Integer totalTargetMTDCSTL = 0;
           Integer totalActualCSTL    = 0;
           
           if(cstlinputTargetMap.containsKey(cstlKey)){
               if(cstlinputTargetMap.get(cstlKey)!=null){
                    totalTargetCSTL = cstlinputTargetMap.get(cstlKey) + integer.valueof(inpScore.Total_Monthly_Target__c);
               }else{
                    totalTargetCSTL = 0 + integer.valueof(inpScore.Total_Monthly_Target__c);
               }
               cstlinputTargetMap.put(cstlKey,totalTargetCSTL);
           }else{
               totalTargetCSTL = integer.valueof(inpScore.Total_Monthly_Target__c);
               cstlinputTargetMap.put(cstlKey,totalTargetCSTL);
           } 
                       
           if(cstlinputTargetMTDMap.containsKey(cstlKey)){
               if(cstlinputTargetMTDMap.get(cstlKey)!=null){
                    totalTargetMTDCSTL = cstlinputTargetMTDMap.get(cstlKey) + integer.valueof(inpScore.Total_MTD_Target__c);
               }else{
                    totalTargetMTDCSTL = 0 + integer.valueof(inpScore.Total_MTD_Target__c);
               }             
               cstlinputTargetMTDMap.put(cstlKey,totalTargetMTDCSTL);
           }else{
               totalTargetMTDCSTL = integer.valueof(inpScore.Total_MTD_Target__c);
               cstlinputTargetMTDMap.put(cstlKey,totalTargetMTDCSTL);
           }
           
           if(cstlinputActualMap.containsKey(cstlKey)){
                if(cstlinputActualMap.get(cstlKey)!=null){
                    totalActualCSTL = cstlinputActualMap.get(cstlKey) + integer.valueof(inpScore.Total_Actual_MTD__c);
                }else{
                    totalActualCSTL = 0 + integer.valueof(inpScore.Total_Actual_MTD__c);
                }
                cstlinputActualMap.put(cstlKey,totalActualCSTL);
           }else{
               totalActualCSTL = integer.valueof(inpScore.Total_Actual_MTD__c);
               cstlinputActualMap.put(cstlKey,totalActualCSTL);
           }
       }   
     }
     
     //*************************************************************RM***************************************************************************
     list<Dashboard_Input_Score__c> dsnewList = new list<Dashboard_Input_Score__c>();
     
     if(dwm.size()>0){
        string key = '';   
        
        for(Dashboard_Weightage_Master__c dw:dwm){
            Integer targetTotal    = 0;
            Integer targetMTDTotal = 0;
            Integer actualTotal    = 0;
        
            key = dw.Category__c.trim() + dw.Parameters_Inout__c.trim() ;
             
                                    Dashboard_Input_Score__c ds=new Dashboard_Input_Score__c();
                                    ds.User__c                       = userId;
                                    ds.Region_Code__c                = regionCode;
                                    ds.Dashboard_Weightage_Master__c = dw.Id;
                                    ds.Month__c                      = currentMonth;
                                    ds.Year__c                       = currentYear;     
                                    ds.RecordTypeId                  = rmRecTypeId;                     
                                    ds.Parameters__c                 = dw.Parameters_Inout__c; 
                                    ds.Category__c                   = dw.Category__c;
                                    ds.Input_Dashboard_ExID__c       = userId + regionCode + currentMonth + currentYear + dw.Category__c.trim() + (dw.Parameters_Inout__c).trim()+rmRole;    
                                    ds.OwnerId                       = userId;
                                    // Total target of TL/TLS/CSTL
                                     if(tlinputTargetMap.get(key)!=null){
                                           targetTotal = targetTotal + tlinputTargetMap.get(key) ;
                                     }
                                     if(tldinputTargetMap.get(key)!=null){
                                           targetTotal = targetTotal + tldinputTargetMap.get(key) ;
                                     }
                                     if(cstlinputTargetMap.get(key)!=null){
                                           targetTotal = targetTotal + cstlinputTargetMap.get(key) ;
                                     }
                                     
                                     // Total target MTD of TL/TLS/CSTL
                                     if(tlinputTargetMTDMap.get(key)!=null){
                                           targetMTDTotal = targetMTDTotal + tlinputTargetMTDMap.get(key) ;
                                     }
                                     if(tldinputTargetMTDMap.get(key)!=null){
                                           targetMTDTotal = targetMTDTotal + tldinputTargetMTDMap.get(key) ;
                                     }
                                     if(cstlinputTargetMTDMap.get(key)!=null){
                                           targetMTDTotal = targetMTDTotal + cstlinputTargetMTDMap.get(key) ;
                                     }
                                     
                                     // Total Actual of TL/TLS/CSTL
                                     if(tlinputActualMap.get(key)!=null){
                                           actualTotal = actualTotal + tlinputActualMap.get(key) ;
                                     }
                                     if(tldinputActualMap.get(key)!=null){
                                           actualTotal = actualTotal + tldinputActualMap.get(key) ;
                                     }
                                     if(cstlinputActualMap.get(key)!=null){
                                           actualTotal = actualTotal + cstlinputActualMap.get(key) ;
                                     }
                                     
                                    if(targetTotal>0)         {
                                         ds.Total_Monthly_Target__c = targetTotal;  
                                    }else{
                                         ds.Total_Monthly_Target__c = 0;
                                    } 
                                    
                                    if(targetMTDTotal>0)         {
                                         ds.Total_MTD_Target__c = targetMTDTotal;  
                                    }else{
                                         ds.Total_MTD_Target__c = 0;
                                    } 
                                    
                                    if(actualTotal>0)         {
                                         ds.Total_Actual_MTD__c= actualTotal;  
                                    }else{
                                         ds.Total_Actual_MTD__c=0;
                                    }  
                                                                     
                                    if(targetMTDTotal>0 && actualTotal!=null){
                                            decimal achv = actualTotal / targetMTDTotal;
                                            ds.Achievement_MTD__c=  achv;  
                                            if(achv <=100){
                                                ds.Score__c = (achv * dw.Weightage__c/100);
                                            }else{
                                                ds.Score__c = dw.Weightage__c;
                                            }
                                    }else{
                                            ds.Achievement_MTD__c=0;
                                            ds.Score__c = 0;
                                     }
                                    dsnewList.add(ds);
                            
        }
        
     }
        /*
            Upserting RM Input Dashboard Records
        */
        if(dsnewList.size()>0){
            Database.upsert(dsnewList,Dashboard_Input_Score__c.Fields.Input_Dashboard_ExID__c,false);  
        }
      
        /* 
            Calculating total input score for particular region and inserting in  Dashboard_Summary_Score__c object 
        */
        
        list<Dashboard_Input_Score__c> inputDashboard = new list<Dashboard_Input_Score__c>();
        Double totalScore = 0;
        inputDashboard = [select id,Month__c,Year__c,ScoreMTD__c,Score__c,Region_Code__c  from Dashboard_Input_Score__c where Region_Code__c=:regionCode AND Month__c =:currentMonth AND Year__c =:currentYear AND RecordTypeId =: rmRecTypeId];
        
        if(!inputDashboard.isEmpty()){
        DSSString = inputDashboard[0].Region_Code__c + UtilityClass.fetchMonthVal(inputDashboard[0].Month__c) + inputDashboard[0].Year__c;
        system.debug('DSSS'+DSSString );
        }
        inputDashboardScore =  [SELECT sum(ScoreMTD__c)sumScore,Region_Code__c  FROM Dashboard_Input_Score__c where Region_Code__c=:regionCode AND Month__c =:currentMonth AND Year__c =:currentYear AND RecordTypeId =: rmRecTypeId Group By Region_Code__c];
        system.debug('OPPPP'+inputDashboardScore );
        listofDSS = [SELECT id,Input_Score__c,Region_Code__c,DSS_External_ID__c FROM Dashboard_Summary_Score__c WHERE DSS_External_ID__c =: DSSString];
          if(listofDSS.size() > 0){
            for(Dashboard_Summary_Score__c d : listofDSS){
                mapOfRegionAndDSS.put(d.Region_Code__c,d);
            }
          }

        /*if(inputDashboard.size() > 0){
            for(Dashboard_Input_Score__c inp : inputDashboard){
                if(inp.ScoreMTD__c >0){
                    totalScore = totalScore + inp.ScoreMTD__c ; 
                }else{
                    totalScore = totalScore + 0; 
                }                
            }
         }*/

        Dashboard_Summary_Score__c dashboardTotalInputScore = new Dashboard_Summary_Score__c();
        if(inputDashboardScore .size() > 0 ){
          
            for (AggregateResult ar : inputDashboardScore)  {
                if(mapOfRegionAndDSS.containsKey(String.valueof(ar.get('Region_Code__c')))){
                    
                    dashboardTotalInputScore = mapOfRegionAndDSS.get(String.valueof(ar.get('Region_Code__c')));
                    dashboardTotalInputScore.Input_Score__c = (Decimal)ar.get('sumScore');
                    
                }
                
            }
            dashboardTotalInputScore.DSS_External_ID__c     = DSSString;
            dashboardTotalInputScore.RecordTypeId           = rmInputRecTypeId;
            dashboardTotalInputScore.Region_Code__c         = regionCode;
            dashboardTotalInputScore.Month__c               = String.valueOf(UtilityClass.fetchMonthVal(currentMonth));
            dashboardTotalInputScore.Year__c                = currentYear;
            dashboardTotalInputScore.OwnerId                = userId ;
            database.upsert(dashboardTotalInputScore,Dashboard_Summary_Score__c.Fields.DSS_External_ID__c,false);
        }

  }
     
  global void finish(Database.BatchableContext BC) {
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: DashboardInputRM_BatchClass Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "DashboardInputRM_BatchClass" processed ' + a.TotalJobItems + '/n'+'batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus + '/n' );  
               if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
                 }
            
  }  
}