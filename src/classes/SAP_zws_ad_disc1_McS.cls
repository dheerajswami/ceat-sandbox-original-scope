//Generated by wsdl2apex

public class SAP_zws_ad_disc1_McS {
    public class ZsdfcAdDiscount1 {
        public String Crnum;
        public String Kunnr;
        public String Mwskz;
        public String RunDate;
        public String Name1;
        public String Office;
        public String Zgroup;
        public String Dwerk;
        public String Apdat;
        public String Sdsas;
        public String Zavg;
        public String Sdsgr;
        public String Sdsat;
        public String Pre;
        public String Post;
        public String Kwert;
        public String Rot;
        public String Targt;
        public String Reqto;
        public String Addin;
        public String Sds;
        public String Tov;
        public String Crval;
        public String Disc;
        public String Indic;
        private String[] Crnum_type_info = new String[]{'Crnum','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Kunnr_type_info = new String[]{'Kunnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Mwskz_type_info = new String[]{'Mwskz','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] RunDate_type_info = new String[]{'RunDate','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Name1_type_info = new String[]{'Name1','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Office_type_info = new String[]{'Office','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Zgroup_type_info = new String[]{'Zgroup','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Dwerk_type_info = new String[]{'Dwerk','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Apdat_type_info = new String[]{'Apdat','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Sdsas_type_info = new String[]{'Sdsas','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Zavg_type_info = new String[]{'Zavg','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Sdsgr_type_info = new String[]{'Sdsgr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Sdsat_type_info = new String[]{'Sdsat','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Pre_type_info = new String[]{'Pre','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Post_type_info = new String[]{'Post','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Kwert_type_info = new String[]{'Kwert','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Rot_type_info = new String[]{'Rot','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Targt_type_info = new String[]{'Targt','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Reqto_type_info = new String[]{'Reqto','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Addin_type_info = new String[]{'Addin','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Sds_type_info = new String[]{'Sds','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Tov_type_info = new String[]{'Tov','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Crval_type_info = new String[]{'Crval','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Disc_type_info = new String[]{'Disc','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Indic_type_info = new String[]{'Indic','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Crnum','Kunnr','Mwskz','RunDate','Name1','Office','Zgroup','Dwerk','Apdat','Sdsas','Zavg','Sdsgr','Sdsat','Pre','Post','Kwert','Rot','Targt','Reqto','Addin','Sds','Tov','Crval','Disc','Indic'};
    }
    public class ZsfdcAdDisc1Response_element {
        public SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1 ItOutput;
        public String Return_x;
        private String[] ItOutput_type_info = new String[]{'ItOutput','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Return_x_type_info = new String[]{'Return','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ItOutput','Return_x'};
    }
    public class TableOfZsdfcAdDiscount1 {
        public SAP_zws_ad_disc1_McS.ZsdfcAdDiscount1[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZsfdcAdDisc1_element {
        public String Crnum;
        public SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1 ItOutput;
        public String Kunnr;
        private String[] Crnum_type_info = new String[]{'Crnum','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] ItOutput_type_info = new String[]{'ItOutput','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Kunnr_type_info = new String[]{'Kunnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Crnum','ItOutput','Kunnr'};
    }
    public class ZWS_AD_DISC1 {
        public String endpoint_x = 'http://CTECPHAP1.ceat.in:8000/sap/bc/srt/rfc/sap/zws_ad_disc1/300/zws_ad_disc1/zws_ad_disc1';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'SAP_zws_ad_disc1_McS', 'urn:sap-com:document:sap:rfc:functions', 'SAP_zws_ad_disc1'};
        public SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element ZsfdcAdDisc1(String Crnum,SAP_zws_ad_disc1_McS.TableOfZsdfcAdDiscount1 ItOutput,String Kunnr) {
            SAP_zws_ad_disc1_McS.ZsfdcAdDisc1_element request_x = new SAP_zws_ad_disc1_McS.ZsfdcAdDisc1_element();
            request_x.Crnum = Crnum;
            request_x.ItOutput = ItOutput;
            request_x.Kunnr = Kunnr;
            SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element response_x;
            Map<String, SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element> response_map_x = new Map<String, SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:soap:functions:mc-style:ZWS_AD_DISC1:ZsfdcAdDisc1Request',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsfdcAdDisc1',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZsfdcAdDisc1Response',
              'SAP_zws_ad_disc1_McS.ZsfdcAdDisc1Response_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}