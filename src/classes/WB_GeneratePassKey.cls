/*+----------------------------------------------------------------------
||         Author:  Kishlay Mathur
||
||        Purpose:To Generate PassKey of the customer and return 
|| 
||        Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/GeneratePassKey/*')

global with sharing class WB_GeneratePassKey {
    global class customerDetails{
            public String reference_Code;
            public String passKey;
    }
    @HttpGet
    global static customerDetails getCustomerDetails(){
        RestRequest req           = RestContext.request;
        RestResponse res          = RestContext.response;
        String reference_Code     = req.params.get('reference_Code');
        String flag           = req.params.get('flag');
        try{
           if(flag != null && flag == 'D'){
               Account account = [SELECT Id,Name,Preferred_Language__c,Passcode__c,KUNNR__c FROM Account WHERE KUNNR__c =:reference_Code];
               //if(account.size() > 0){
                   if(account.Passcode__c != null){
                        customerDetails cusdetails = new customerDetails();
                        cusdetails.passKey= account.Passcode__c;
                        cusdetails.reference_Code = account.KUNNR__c;
                        return cusdetails ; 
                   }else{
                       customerDetails cusdetails = new customerDetails();
                       cusdetails.reference_Code = '';
                       cusdetails.passKey= '';
                       return cusdetails ; 
                   }
               /*}else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.reference_Code = '';
                   cusdetails.passKey= '';
                   return cusdetails ; 
               }*/
           }else if(flag != null && flag == 'E'){
               Employee_Master__c employee = [SELECT id,Name,Passcode__c,EmployeeId__c FROM Employee_Master__c WHERE EmployeeId__c =: reference_Code];
               //if(employee.size() > 0){
                   if(employee.Passcode__c != null){
                        customerDetails cusdetails = new customerDetails();
                        cusdetails.passKey= employee.Passcode__c;
                        cusdetails.reference_Code = employee.EmployeeId__c;
                        return cusdetails ; 
                   }else{
                       customerDetails cusdetails = new customerDetails();
                       cusdetails.reference_Code = '';
                       cusdetails.passKey= '';
                       return cusdetails ; 
                   }
               /*}else{
                   customerDetails cusdetails = new customerDetails();
                   cusdetails.reference_Code = '';
                   cusdetails.passKey= '';
                   return cusdetails ; 
               }*/
           } else{
               customerDetails cusdetails = new customerDetails();
               cusdetails.reference_Code = '';
               cusdetails.passKey= '';
               return cusdetails ; 
           }  
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
            customerDetails cusdetails = new customerDetails();
            cusdetails.reference_Code = '';
            cusdetails.passKey= '';
            return cusdetails ; 
        }
    }
}