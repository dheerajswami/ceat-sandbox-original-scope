//Generated by wsdl2apex

public class sapCustomer_SDS_PROD_TL {
    public class ZbapiCustomerSdsTlResponse_element {
        public sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds ItFinal;
        private String[] ItFinal_type_info = new String[]{'ItFinal','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ItFinal'};
    }
    public class TableOfZbapiCustSds {
        public sapCustomer_SDS_PROD_TL.ZbapiCustSds[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZbapiCustSds {
        public String Kunnr;
        public String Sds;
        private String[] Kunnr_type_info = new String[]{'Kunnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Sds_type_info = new String[]{'Sds','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Kunnr','Sds'};
    }
    public class ZbapiCustomerSdsTl_element {
        public String Bzirk;
        public sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds ItFinal;
        private String[] Bzirk_type_info = new String[]{'Bzirk','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] ItFinal_type_info = new String[]{'ItFinal','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Bzirk','ItFinal'};
    }
    public class ZWS_CUSTOMER_SDS_TL {
        public String endpoint_x = 'http://ctecphap3.ceat.in:8000/sap/bc/srt/rfc/sap/zws_customer_sds_tl/300/zws_customer_sds_tl/zws_customer_sds_tl';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'sapCustomer_SDS_PROD_TL', 'urn:sap-com:document:sap:rfc:functions', 'sapCustomer_SDS_PROD_TL_Functions'};
        public sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds ZbapiCustomerSdsTl(String Bzirk,sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds ItFinal) {
            sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTl_element request_x = new sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTl_element();
            request_x.Bzirk = Bzirk;
            request_x.ItFinal = ItFinal;
            sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element response_x;
            Map<String, sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element> response_map_x = new Map<String, sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:soap:functions:mc-style:ZWS_CUSTOMER_SDS_TL:ZbapiCustomerSdsTlRequest',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZbapiCustomerSdsTl',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZbapiCustomerSdsTlResponse',
              'sapCustomer_SDS_PROD_TL.ZbapiCustomerSdsTlResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.ItFinal;
        }
    }
}