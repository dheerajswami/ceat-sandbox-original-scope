/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Dashboard_Input_ControllerTL_TestClass {
    static list<Id> terrTypeID                            = new list<Id>();
    static list<Id> terrId                                = new list<Id>();
    
    static testMethod void myUnitTest() {
        String tlRole = System.Label.TL_Role;
        string TLInputRecType           = system.Label.Input_RecType;
        
        map<String,Id> wightageMap = new map<String,Id>();
        
        Id TLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName ='TL' Limit 1].Id;
        Id summaryInputRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:TLInputRecType Limit 1].Id;
        
         
        integer monthNumber = date.today().month();
        String month = (monthNumber == 1)?'Jan':(monthNumber == 2)?'Feb':(monthNumber == 3)?'Mar':(monthNumber == 4)?'Apr':(monthNumber == 5)?'May':(monthNumber == 6)?'June':(monthNumber == 7)?'July':(monthNumber == 8)?'Aug':(monthNumber == 9)?'Sep':(monthNumber == 10)?'Oct' : (monthNumber == 11)?'Nov' : 'Dec';
            
        list<sObject> dwmAl      = Test.loadData(Dashboard_Weightage_Master__c.sObjectType, 'DashboardMasterInput');
           
        list<Territory2Type> terriType               = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
                terrTypeID.add(temp.id);
        }  
        String[] filters = new String[]{'SPRP%'};
        list<Territory2> terrList     = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID ];       
        list<Territory2> terrListtemp = [SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters)];
        
        for(Territory2 temp : terrList){
                terrId.add(temp.id);
        } 
        list<UserTerritory2Association> userTerrAssList = [SELECT id,Territory2Id,Territory2.Name,UserId,RoleInTerritory2 from UserTerritory2Association WHERE Territory2Id IN: terrId AND RoleInTerritory2=:tlRole];    
        User u = [Select id, name from User where id = :userTerrAssList[0].UserId limit 1];

        User u1 = [Select id, name from User where id = :userTerrAssList[1].UserId limit 1];

        User u2 = [Select id, name from User where id = :userTerrAssList[2].UserId limit 1];
        
        //Insert Dashboard Master
         Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true);
         insert dm;                
       
         Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();                    
         dwmTemp.Dashboard_Master__c = dm.id;
         dwmTemp.role__c = tlRole;
         dwmTemp.Weightage__c = 0;
         insert dwmTemp; 
        system.runAs(u2){
            Dashboard_Summary_Score__c dbSummaryScore = new Dashboard_Summary_Score__c();
            dbSummaryScore.Territory_Code__c = userTerrAssList[0].Territory2.name;
            dbSummaryScore.Month__c = string.valueof(monthNumber) ;
            dbSummaryScore.Year__c = string.valueof(date.today().year());
            dbSummaryScore.Input_Score__c = 20;
            dbSummaryScore.RecordTypeId = summaryInputRecTypeId;
            dbSummaryScore.DSS_External_ID__c = userTerrAssList[2].Territory2.name+monthNumber+string.valueof(date.today().year());
            insert dbSummaryScore;
        }
        system.runAs(u1){
            Dashboard_Summary_Score__c inpScoreTl = new Dashboard_Summary_Score__c();
            inpScoreTl.RecordTypeId = summaryInputRecTypeId;
            inpScoreTl.Month__c = month;
            inpScoreTl.year__c = string.valueof(date.today().year());
            inpScoreTl.DSS_External_ID__c = userTerrAssList[1].Territory2.name+monthNumber+string.valueof(date.today().year());
            insert inpScoreTl;
        }
        Dashboard_Input_Score__c inpScore = new Dashboard_Input_Score__c();
            inpScore.RecordTypeId = TLInputRecTypeId;           
            inpScore.Month__c = month;
            inpScore.year__c = string.valueof(date.today().year());
            inpScore.Dashboard_Weightage_Master__c = dwmTemp.id;
            inpScore.Territory_Code__c = userTerrAssList[0].Territory2.name;
            inpScore.Parameters__c='Number of P1 customers met';
            inpScore.PJP__c=true;
            inpScore.Category__c='PJP adherance';
            inpScore.Total_Monthly_Target__c=0;
            inpScore.Total_MTD_Target__c=0;
            inpScore.Total_Actual_MTD__c=0;
            inpScore.Achievement_MTD__c=0;
            inpScore.Score__c=0;
            inpScore.User__c = u.id;            
       
        insert inpScore;
        
        Dashboard_Input_ControllerTL dbTl = new Dashboard_Input_ControllerTL();
        DashboardInputControllerHandlerTL.dashboardWrapper  res = Dashboard_Input_ControllerTL.getTlInputDashboardData();
    
        system.runAs(u){
            
            Dashboard_Summary_Score__c dbSummaryScore1 = new Dashboard_Summary_Score__c();
            dbSummaryScore1.Territory_Code__c = userTerrAssList[0].Territory2.name;
            dbSummaryScore1.Month__c = string.valueof(monthNumber) ;
            dbSummaryScore1.Year__c = string.valueof(date.today().year());
            dbSummaryScore1.Input_Score__c = 20;
            dbSummaryScore1.DSS_External_ID__c = userTerrAssList[0].Territory2.name+monthNumber+string.valueof(date.today().year());
            dbSummaryScore1.RecordTypeId = summaryInputRecTypeId;
            insert dbSummaryScore1;
            
            Dashboard_Input_ControllerTL dbTl1 = new Dashboard_Input_ControllerTL();
            DashboardInputControllerHandlerTL.dashboardWrapper  res1 = Dashboard_Input_ControllerTL.getTlInputDashboardData();
        
        }
    }        
}