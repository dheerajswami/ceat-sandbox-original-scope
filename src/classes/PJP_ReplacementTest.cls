@isTest(seeAllData = false)
public class PJP_ReplacementTest {
    public static User rmUser;
    public static User tlUser;
    public static User cstlUser;
    public static Territory2Model terrModel;
    public static Territory2 terrZone;
    public static Territory2 terrRegion;
    public static Territory2 terr;
    public static UserTerritory2Association utAss;
    public static UserTerritory2Association utAss1;
    public static void init() {
        rmUser = CEAT_InitializeTestData.createUser('gulati', 'sandeep', 'sandeep.gulati@ceat.dev', 'sandeep.gulati@ceat.dev', 'Emp2', 'Replacements', null, 'RM');
        insert rmUser;
        tlUser = CEAT_InitializeTestData.createUser('Ram', 'Ronak', 'ram.ronak@ceat.dev', 'ram.ronak@ceat.dev', 'Emp1', 'Replacements', null, 'TL_Replacement');
        tlUser.ManagerId = rmUser.Id;
        insert tlUser;
        
        cstlUser = CEAT_InitializeTestData.createUser('Vijay', 'Sabla', 'vijay.sabla@ceat.dev', 'vijay.sabla@ceat.dev', 'Emp3', 'Replacements', null, 'CSTL_AGR');
        cstlUser.ManagerId = rmUser.Id;
        insert cstlUser;


        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceat', 'Ceat');
        insert terrModel; 
        
        Id terrTypeTerr;
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');        
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            
        }
        terrZone = CEAT_InitializeTestData.createTerritory('ZS05', 'ZS05' , terrTypeTerr, terrModel.Id, null);
        insert terrZone;
        terrRegion = CEAT_InitializeTestData.createTerritory('R5', 'R5' , terrTypeTerr, terrModel.Id, terrZone.Id);
        insert terrRegion;
        terr = CEAT_InitializeTestData.createTerritory('T5', 'T5' , terrTypeTerr, terrModel.Id, terrRegion.Id);
        insert terr;
        
        utAss = CEAT_InitializeTestData.createUserTerrAsso(tlUser.Id, terr.Id, 'TL');
        insert utAss;



        utAss1 = CEAT_InitializeTestData.createUserTerrAsso(cstlUser.Id, terr.Id, 'CSTL');
        insert utAss1;
    }
    static testmethod void planReplacement() {
        init();
                
        System.runAs(tlUser) {
            RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', 'C000000001', 'BHP', 'ZC01', 'T5');
            acc1.Sales_District_Text__c = 'T5';     
            acc1.Type = 'Analyst';
            acc1.RecordTypeId = rtDealer.Id;
            acc1.Active__c = true;
            insert acc1;
            
            Holiday__c h1 = CEAT_InitializeTestData.createHoliday('National', 'National', Date.today(), null, null, null, null, null);
            Holiday__c h2 = CEAT_InitializeTestData.createHoliday('Regional', 'Regional', Date.today(), 'ZC01', 'BHP', null, null, null);
            Holiday__c h3 = CEAT_InitializeTestData.createHoliday('Planned Leaves', 'Planned Leaves', null, null, null, Date.today(), Date.today().addDays(2), 'Emp1');
            List<Holiday__c> hList = new List<Holiday__c>();
            hList.add(h1);
            hList.add(h2);
            hList.add(h3);
            insert hList;
            
            PJP_Norms_Main__c pnm = CEAT_InitializeTestData.createPjpNorm(true, true, true, true, 3, 2, 3, 10, null, 'TL - Replacement', 'Replacement');
            PJP_Segment_Norm__c psn = CEAT_InitializeTestData.createSegmentNorm(pnm.Id, 'Analyst', 4);
            PJP_Daily_Norms__c pdn = CEAT_InitializeTestData.createDailyNorm(pnm.Id, 'Dealer', 6);
            
            Integer mm = Date.today().month();
            Integer yy = Date.today().year();
            //String mon = MonthlyVisitsPlanClass_v2.calculateMonth(mm);
            String mon = MonthlyVisitsPlanClass_v3.calculateMonth(mm);
            ACW__c currentAcw = CEAT_InitializeTestData.createACW(Date.today(), Date.today(), mon, 'PJP Replacement');
            
            /*MonthlyVisitsPlanClass_v2 mvp = new MonthlyVisitsPlanClass_v2();
            MonthlyVisitsPlanClass_v2.getWriteAccess(null, null, null);            
            MonthlyVisitsPlanClass_v2.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            MonthlyVisitsPlanClass_v2.getRelatedAccounts(2014, 12, 'December', '2014', String.valueOf(UserInfo.getUserId()));
            */

            MonthlyVisitsPlanClass_v3 mvp = new MonthlyVisitsPlanClass_v3();
            MonthlyVisitsPlanClass_v3.getWriteAccess(null, null, null);            
            MonthlyVisitsPlanClass_v3.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            
            MonthlyVisitsPlanClass_v3.getRelatedAccounts(2014, 12, 'December', '2014', String.valueOf(UserInfo.getUserId()));
            

            Visits_Plan__c vp1 = new Visits_Plan__c();
            vp1.Day__c = '1';
            vp1.Type_of_Day__c = 'OE';
            vp1.Id = null;
            Visits_Plan__c vp2 = new Visits_Plan__c();
            vp2.Day__c = '2';
            vp2.Type_of_Day__c = 'MOR';
            vp2.Id = null;
            Visits_Plan__c vp3 = new Visits_Plan__c();
            vp3.Day__c = '3';
            vp3.Type_of_Day__c = 'Company Activity';
            vp3.Id = null;
            Visits_Plan__c vp4 = new Visits_Plan__c();
            vp4.Day__c = '4';
            vp4.Type_of_Day__c = 'Influencer Visit';
            vp4.Id = null;
            Visits_Plan__c vp5 = new Visits_Plan__c();
            vp5.Dealer__c = acc1.Id;
            vp5.Day__c = '5';
            vp5.Type_of_Day__c = 'Visit Day';
            vp5.Id = null;//String.valueOf(acc1.Id);
            
            List<Visits_Plan__c> vpList = new List<Visits_Plan__c>();
            vpList.add(vp1);
            vpList.add(vp2);
            vpList.add(vp3);
            vpList.add(vp4);
            vpList.add(vp5);               
                                    
            /*MonthlyVisitsPlanClass_v2.saveVisitPlans(vpList, yy, mm, mon, String.valueOf(yy), 'Submit', 'Comments');
                        
            MonthlyVisitsPlanClass_v2.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));*/
            

            MonthlyVisitsPlanClass_v3.saveVisitPlans(vpList, yy, mm, mon, String.valueOf(yy), 'Submit', 'Comments');                
            MonthlyVisitsPlanClass_v3.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));

            ApexPages.currentPage().getParameters().put('month', mon);
            ApexPages.currentPage().getParameters().put('year', String.valueOf(yy));
            ApexPages.currentPage().getParameters().put('owner', tlUser.Id);
            /*mvp = new MonthlyVisitsPlanClass_v2();
            MonthlyVisitsPlanClass_v2.getRelatedAccounts(yy, mm, mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
*/            
            mvp = new MonthlyVisitsPlanClass_v3();
            MonthlyVisitsPlanClass_v3.getTerrritories();
            //MonthlyVisitsPlanClass_v3.getPlannedAccounts(String.valueOf(mon), String.valueOf(yy), tlUser.Id);
            MonthlyVisitsPlanClass_v3.getRelatedAccounts(yy, mm, mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));

            vpList = [Select Id, Dealer__c, Day__c, Type_of_Day__c from Visits_Plan__c];//new List<Visits_Plan__c>();
            //MonthlyVisitsPlanClass_v2.saveVisitPlans(vpList, yy, mm, mon, String.valueOf(yy), 'Submit', 'New Comments');
            
            MonthlyVisitsPlanClass_v3.saveVisitPlans(vpList, yy, mm, mon, String.valueOf(yy), 'Submit', 'New Comments');

            for(Integer i=0; i<=12; i++) {
                //String m = MonthlyVisitsPlanClass_v2.calculateMonth(i);   
                String m = MonthlyVisitsPlanClass_v3.calculateMonth(i);         
            }

            // Added by Neha

            MonthlyVisitsPlanClass_v3.getPlannedAccounts(mm, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));


        }           
    }

    static testmethod void testACW() {
        init();
                
        System.runAs(tlUser) {
            /*RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', 'C000000001', 'BHP', 'ZC01', 'T5');
            acc1.Sales_District_Text__c = 'T5';     
            acc1.Type = 'Analyst';
            acc1.RecordTypeId = rtDealer.Id;
            acc1.Active__c = true;
            insert acc1;
            
            Holiday__c h1 = CEAT_InitializeTestData.createHoliday('National', 'National', Date.today(), null, null, null, null, null);
            Holiday__c h2 = CEAT_InitializeTestData.createHoliday('Regional', 'Regional', Date.today(), 'ZC01', 'BHP', null, null, null);
            Holiday__c h3 = CEAT_InitializeTestData.createHoliday('Planned Leaves', 'Planned Leaves', null, null, null, Date.today(), Date.today().addDays(2), 'Emp1');
            List<Holiday__c> hList = new List<Holiday__c>();
            hList.add(h1);
            hList.add(h2);
            hList.add(h3);
            insert hList;
            
            PJP_Norms_Main__c pnm = CEAT_InitializeTestData.createPjpNorm(true, true, true, true, 3, 2, 3, 10, null, 'TL', 'Replacement');
            PJP_Segment_Norm__c psn = CEAT_InitializeTestData.createSegmentNorm(pnm.Id, 'Analyst', 4);
            PJP_Daily_Norms__c pdn = CEAT_InitializeTestData.createDailyNorm(pnm.Id, 'Dealer', 6);*/
            
            Integer mm = Date.today().month();
            Integer yy = Date.today().year();
            //String mon = MonthlyVisitsPlanClass_v2.calculateMonth(mm);
            String mon = MonthlyVisitsPlanClass_v3.calculateMonth(mm);
            ACW__c currentAcw = CEAT_InitializeTestData.createACW(Date.today()+1, Date.today()+5, mon, 'PJP Specialty');

            MonthlyVisitsPlanClass_v3 mvp = new MonthlyVisitsPlanClass_v3();
            //MonthlyVisitsPlanClass_v3.getWriteAccess(null, null, null);            
            MonthlyVisitsPlanClass_v3.getWriteAccess(null, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            
        }           
    }

    static testmethod void testACWforReadOnly() {
        init();
                
        System.runAs(cstlUser) {
            RecordType rtDealer = CEAT_InitializeTestData.createRecordType('Dealer', 'Account');
            Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', 'C000000001', 'R5', 'ZS05', 'T5');
            acc1.Sales_District_Text__c = 'T5';     
            acc1.Type = 'Analyst';
            acc1.RecordTypeId = rtDealer.Id;
            acc1.Active__c = true;
            insert acc1;
            Integer mm = Date.today().month();
            Integer yy = Date.today().year();
            //String mon = MonthlyVisitsPlanClass_v2.calculateMonth(mm);
            String mon = MonthlyVisitsPlanClass_v3.calculateMonth(mm);
            ACW__c currentAcw = CEAT_InitializeTestData.createACW(Date.today()+1, Date.today()+5, mon, 'PJP Replacement');

            MonthlyVisitsPlanClass_v3 mvp = new MonthlyVisitsPlanClass_v3();
            //MonthlyVisitsPlanClass_v3.getWriteAccess(null, null, null);            
            MonthlyVisitsPlanClass_v3.getWriteAccess(mon, String.valueOf(yy), String.valueOf(UserInfo.getUserId()));
            MonthlyVisitsPlanClass_v3.getRelatedAccounts(yy, mm, mon, String.valueOf(yy), null);
            
        }           
    }

    static testmethod void testGetPlannedAccountsMethod() {
        init();
        System.runAs(cstlUser) {
            MonthlyVisitsPlanClass_v3.getTerrritories();
        }

    }

    static testmethod void testSendEmailMethod() {
        init();
        String[] toAddresses = new String[] {'neha.mishra@extentor.com'};
       // MonthlyVisitsPlanClass_v3 mv = new MonthlyVisitsPlanClass_v3();
        EmailTemplate e = new EmailTemplate (developerName = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'Text', Name = 'PJP Submission Template1', isActive = true); // plus any other fields that you want to set
        insert e;
        MonthlyVisitsPlanClass_v3.sendemail(tlUser.Id, toAddresses, rmUser.Id);
    }
}