@isTest
private class WB_GeneratePassKey_TestClass {
    
    private static List<Account> acc;
    private static List<Employee_Master__c> empMaster;
    
    
    private  static void init(){
        acc = new List<Account>();
        empMaster = new List<Employee_Master__c>();
        
        
        //for(Integer i = 0;i<LOOP_VARIABLE;i++){
            Account a = new Account(Name = 'Test Acc',KUNNR__c = '55000038',Preferred_Language__c = 'Hindi',Passcode__c  = '1234');
            
            Account a1 = new Account(Name = 'Test Acc1',KUNNR__c = '55000034',Preferred_Language__c = 'Hindi');
            acc.add(a);
            acc.add(a1);
        //}
        
        insert acc;
        
        
            Employee_Master__c e = new Employee_Master__c(EmployeeId__c = '10946527',Name= 'Test emp',Preferred_Language__c = 'English', Passcode__c  = '1235');
            Employee_Master__c e1 = new Employee_Master__c(EmployeeId__c = '10946528',Name= 'Test emp',Preferred_Language__c = 'English');
            empMaster.add(e);
            empMaster.add(e1);
        
        insert empMaster;
        
     
    }
    
    static testMethod void myUnitTest() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '55000038');
        req.addParameter('flag', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }
    static testMethod void myUnitTest1() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '55000034');
        req.addParameter('flag', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }
   
    static testMethod void myUnitTest2() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '10946527');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }

    static testMethod void myUnitTest3() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '10946528');
        req.addParameter('flag', '');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }
    
    static testMethod void myUnitTest4() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '10946528');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    } 
    
    static testMethod void myUnitTest5() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }
    
     static testMethod void myUnitTest6() {
        
        init();
        WB_GeneratePassKey.customerDetails cusdetails = new WB_GeneratePassKey.customerDetails();
        cusdetails.passKey = 'N';
        cusdetails.reference_Code = '10946527';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/GeneratePassKey';  
        req.addParameter('reference_Code', '');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_GeneratePassKey.getCustomerDetails();
        
    }
}