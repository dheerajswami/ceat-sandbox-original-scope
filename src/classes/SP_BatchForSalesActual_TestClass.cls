@isTest 
private class SP_BatchForSalesActual_TestClass{
    static testmethod void testLoadData() {
        String repDealerLabel           = System.Label.Replacement_Dealer;
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
        Map<String,id> mapOFcusNumAndID = new Map<String,ID>();
        Sales_Planning__c spReplacementCatWise = new Sales_Planning__c();
        Id replaceDealerId  = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repDealerLabel Limit 1].Id;
        // Load the test Sales planning Staging from the static resource
        //List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatReplacement');
        List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataActual');
        List<sObject> listReplacementActualData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'ReplacementActualData');
        for(sObject ac : listAccoutData ){
            Account acc = (Account)ac;
            mapOFcusNumAndID.put(acc.KUNNR__c,acc.id);
        }
        for(sObject li : listReplacementActualData){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            //keyCatRegDea = mapOFcusNumAndID.get(spr.Dealer_CustNumber__c)+spr.Reg_code__c+spr.Territory_Code__c+spr.Category__c+spr.Month__c+spr.Year__c ;
            //mapOFkeyStagingrec.put(keyCatRegDea,spr);
        }
        update listReplacementActualData;
        
        spReplacementCatWise = new Sales_Planning__c(
                            Region_code__c = 'KLP',
                            
                            Category__c = '2590',
                            Category_Description__c = 'JEEP RAD (TUBES)',
                            Month__c = month,Year__c = year,
                            Territory_Code__c = 'D0046',
                            Dealer_CustNumber__c = '50014465',
                            Dealer_Name__c =  'GOBIND TYRE RETREADERS',
                            SPExternalIDTL__c = mapOFcusNumAndID.get('50014465')+'KLP'+'D0046'+'2590'+month+year,
                            RecordTypeId = replaceDealerId
                            );
        insert spReplacementCatWise;
        SP_BatchForSalesActual nn = new SP_BatchForSalesActual();
        database.executebatch(nn,200); 
    }
}