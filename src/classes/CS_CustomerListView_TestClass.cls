@isTest(seeAllData = false)
public class CS_CustomerListView_TestClass {
    
    static Account ceatShoppe;
    static Contact shoppeContact;
    static User csUser;
    static CS_Customer__c cust1,cust2;

public static void init(){
    ceatShoppe = CEAT_InitializeTestData.createAccount('Shoppe1','456');
    ceatShoppe.District__c ='Mumbai';
    database.insert(ceatShoppe);
    
    shoppeContact = CEAT_InitializeTestData.createContact('owner 1',ceatShoppe.Id);
    database.insert(shoppeContact);
    
    csUser = CEAT_InitializeTestData.createPortalUser(shoppeContact.Id);
    database.insert(csUser);
    
    cust1 = CEAT_InitializeTestData.createCSCustomer('csCustomer1',ceatShoppe.Id,'1234567891');
    cust2 = CEAT_InitializeTestData.createCSCustomer('csCustomer2',ceatShoppe.Id,'1234567892');
    
}
    public static testMethod void viewCustomers(){
    
    init();
    Test.startTest();
        System.runAs(csUser) {
            insert(cust1);
            insert(cust2);
            
            PageReference pg = Page.CS_CustomerListView;
            Test.setCurrentPage(pg);
            CS_CustomerListViewController ctrl = new CS_CustomerListViewController();
            
            List<CS_Customer__c> customersList = ctrl.getListOfCustomers();
            system.assertEquals(customersList.size(),2);
            
            ctrl.sortByColumn1();//sort customers by Name , it is default sorted ascending; now it will become descending
            system.assertEquals(ctrl.getListOfCustomers()[0].Name,'csCustomer2');
            
            //sort again, so that it is ascending now
            ctrl.sortByColumn1();
            system.assertEquals(ctrl.getListOfCustomers()[0].Name,'csCustomer1');
            
            //navigate to next/first/last/previous
            ctrl.next();
            ctrl.first();
            ctrl.last();
            ctrl.previous();
            system.assertEquals(ctrl.pageNumber,1);
            
            ctrl.cancel();
        }
}

}