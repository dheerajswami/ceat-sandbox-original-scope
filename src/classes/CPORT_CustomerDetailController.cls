public class CPORT_CustomerDetailController {
   
    /*
     
     Naviagte page with customer Id for customer community tab
     
     */
     @RemoteAction
     public static String redirectToPortalPage(){       
            Id accountId = [SELECT Id,AccountId FROM User WHERE Id = : UserInfo.getUserId()].AccountId;            
            return (System.URL.getSalesforceBaseUrl().toExternalForm() + '/customers/'+accountId);             
        
     }

}