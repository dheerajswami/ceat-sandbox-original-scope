global class STG_CustomerStagingClass {

    /*
     * Auther  :- Vivek Deepak  
     * Purpose :- Store Customer Details in the Stageing object  
     *            
     *
     *
     */
     
     //-- ATTRIBUTES

    /*
     Fetch Customer Information from SAP
     */
     WebService static List<Customer_Staging__c> getAllCustomerInformation(){

        Map<String,CustomerDetailWrapper> mapOfCustomerDetail = new Map<String,CustomerDetailWrapper>();

        SAPLogin__c saplogin                            = SAPLogin__c.getValues('SAP Login');
        
        
        String username                                 = saplogin.username__c;
        String password                                 = saplogin.password__c;
        Blob headerValue                                = Blob.valueOf(username + ':' + password);
        String authorizationHeader                      = 'Basic '+ EncodingUtil.base64Encode(headerValue);
          
        SapCustomerDetailsSoap_v2   tst                         = new SapCustomerDetailsSoap_v2();
        SapCustomerDetailsSoap_v2.ZWS_BAPI_GET_CUSTOMER_DETAILS  zws = new SapCustomerDetailsSoap_v2.ZWS_BAPI_GET_CUSTOMER_DETAILS();
        
        zws.inputHttpHeaders_x                          = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                   = 60000;
    
        SapCustomerDetailsSoap_v2.TABLE_OF_BAPIRETURN tbp         = new SapCustomerDetailsSoap_v2.TABLE_OF_BAPIRETURN();        
        SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR tcstr     = new SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR();
        SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR1 tcstr1   = new SapCustomerDetailsSoap_v2.TABLE_OF_ZBAPI_CUST_STR1();

        SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element  elem = new SapCustomerDetailsSoap_v2.ZBAPI_GET_CUSTOMER_DETAILSResponse_element();

        elem = zws.ZBAPI_GET_CUSTOMER_DETAILS(tbp,tcstr,tcstr1);

        if(elem != null){

            if(elem.T_CUST != null){
                if(elem.T_CUST.item != null){
                    for(SapCustomerDetailsSoap_v2.ZBAPI_CUST_STR z : elem.T_CUST.item){
                        
                        

                        CustomerDetailWrapper custD = new CustomerDetailWrapper();
                                    if(z.kunnr.startsWith('00'))
                                        z.kunnr = z.kunnr.subString(2);
                                    //z.Kunnr         = String.valueOf(Integer.valueOf(z.Kunnr));
                                    custD.Kunnr     = z.Kunnr;  
                                    custD.Name1     = z.Name1;  
                                    custD.Telf1     = z.Telf1;  
                                    custD.Ort01     = z.Ort01;  
                                    custD.Regio     = z.Regio;
                                    custD.Pstlz     = z.Pstlz;
                                    custD.Land1     = z.Land1;
                                    custD.Parnr     = z.Parnr;
                                    custD.SmtpAddr  = z.Smtp_Addr;
                                    custD.FaxNumber = z.Fax_Number;
                                    custD.TelNumber = z.Tel_Number;
                                    custD.Parvw     = z.Parvw;
                                    custD.StrSuppl1 = z.Str_Suppl1;
                                    custD.StrSuppl2 = z.Str_Suppl2;
                                    custD.Bzirk     = z.Bzirk;
                                    custD.Waers     = z.Waers;
                                    custD.Kdgrp     = z.Kdgrp;
                                    custD.Vkbur     = z.Vkbur;
                                    custD.Vkgrp     = z.Vkgrp;
                                    custD.Vtweg     = z.Vtweg;
                                    custD.Spart     = z.Spart;
                                    custD.Kvgr1     = z.Kvgr1;
                                    custD.Kvgr2     = z.Kvgr2;
                                    custD.Kvgr3     = z.Kvgr3;
                                    custD.Loevm     = z.Loevm;
                                    custD.CLUBT     = z.CLUBT;
                                    custD.PROS_CODE     = z.PROS_CODE;
                                    custD.CLUBT_D     = z.CLUBT_D;

                                    
                        if(mapOfCustomerDetail.containsKey(z.Kunnr)){
                            //mapOfCustomerDetail.put(z.Kunnr,custD);
                            CustomerDetailWrapper custD1 = new CustomerDetailWrapper();
                            
                            custD1 = mapOfCustomerDetail.get(z.Kunnr);

                            if(custD1.Vtweg != null){
                                custD1.Vtweg = custD1.Vtweg+(custD.Vtweg != null ? ';'+custD.Vtweg : '');
                            }

                            if(custD1.Spart != null){
                                custD1.Spart = custD1.Spart+(custD.Spart != null ? ';'+custD.Spart : '');
                            }

                            mapOfCustomerDetail.put(z.Kunnr,custD1);

                        }else{
                            mapOfCustomerDetail.put(z.Kunnr,custD);
                        }
                    }
                }
            }

            if(elem.T_CUST1 != null){
                if(elem.T_CUST1.item != null){
                    for(SapCustomerDetailsSoap_v2.ZBAPI_CUST_STR1 z : elem.T_CUST1.item){
                        List<CustomerDetailWrapper> cstD = new List<CustomerDetailWrapper>();

                        CustomerDetailWrapper custD = new CustomerDetailWrapper();
                            if(z.kunnr.startsWith('00'))
                                z.kunnr = z.kunnr.subString(2);
                            //z.Kunnr         = String.valueOf(Integer.valueOf(z.Kunnr));
                            custD.Anred = z.Anred;
                            custD.Name1 = z.Name1;
                            custD.Kunnr = z.Kunnr;
                            custD.Telf1 = z.Telf1;
                            custD.FaxNumber = z.Fax_Number;
                            custD.SmtpAddr = z.Smtp_Addr;
                            custD.StrSuppl1 = z.Str_Suppl1;
                            custD.StrSuppl2 = z.Str_Suppl2;
                            custD.Ort01 = z.Ort01;
                            custD.Regio = z.Regio;
                            custD.Pstlz = z.Pstlz;
                            custD.Land1 = z.Land1;
                            custD.Gbdat = z.Gbdat;
                            custD.Waers = z.Waers;
                            custD.Kdgrp = z.Kdgrp;
                            custD.Katr6 = z.Katr6;
                            custD.Katr7 = z.Katr7;
                            custD.J1ipanno = z.J_1ipanno;
                            custD.J1icstno = z.J_1icstno;
                            custD.Town = z.Town;
                            custD.Dist = z.Dist;
                            custD.Bezei = z.Bezei;

                            if(mapOfCustomerDetail.containsKey(z.Kunnr)){
                                CustomerDetailWrapper custD1 = new CustomerDetailWrapper();
                            
                                custD1 = mapOfCustomerDetail.get(z.Kunnr);

                                if(custD1.Vtweg != null){
                                    custD1.Vtweg = custD1.Vtweg+(custD.Vtweg != null ? ';'+custD.Vtweg : '');
                                }

                                if(custD1.Spart != null){
                                    custD1.Spart = custD1.Spart+(custD.Spart != null ? ';'+custD.Spart : '');
                                }

                                mapOfCustomerDetail.put(z.Kunnr,custD1);

                            }else{
                                //cstD.add(custD);
                                mapOfCustomerDetail.put(z.Kunnr,custD);
                            }
                    }
                }
            }
        } 
        
        List<Customer_Staging__c> cStage = new List<Customer_Staging__c>();

        for(String str : mapOfCustomerDetail.keySet()){
            CustomerDetailWrapper  cst = mapOfCustomerDetail.get(str);

            Customer_Staging__c ct = new Customer_Staging__c();

            ct.Central_Sales_Tax_Number__c  = cst.J1icstno;
            ct.City__c                      = cst.Ort01;
            ct.Contact_Person_Number__c     = cst.Parnr;
            ct.Country_Key__c               = cst.Land1;
            ct.Currency__c                  = cst.Waers;
            ct.Customer_Group__c            = cst.Kdgrp;
            ct.Customer_Group1__c           = cst.Kvgr1;
            ct.Customer_Group2__c           = cst.Kvgr2;
            ct.Customer_Group3__c           = cst.Kvgr3;
            ct.Customer_Name__c             = cst.Name1;
            ct.Customer_Number__c           = cst.Kunnr;
            ct.Date_Of_Birth__c             = cst.Gbdat;
            ct.Deletion_Flag__c             = cst.Loevm;
            ct.Distribution_Channel__c      = cst.Vtweg;
            ct.District__c                  = cst.Katr6;
            //ct.District2__c               = cst.
            ct.Division__c                  = cst.Spart;
            ct.Email_Address__c             = cst.SmtpAddr;
            ct.First_Fax__c                 = cst.FaxNumber;
            ct.From_Add_Con_Person__c       = cst.Anred;
            ct.PAN__c                       = cst.J1ipanno;
            ct.Partner_Function__c          = cst.Parvw;
            ct.Postal_Code__c               = cst.Pstlz;
            ct.Region__c                    = cst.Regio;
            ct.Region_L__c                  = cst.Bezei;
            ct.Sales_District__c            = cst.Bzirk;
            ct.Sales_Group__c               = cst.Vkgrp;
            ct.Sales_Office__c              = cst.Vkbur;
            ct.Street_2__c                  = cst.StrSuppl1;
            ct.Street_3__c                  = cst.StrSuppl2;
            ct.Telephone__c                 = cst.Telf1;
            ct.Telephone_Dialling_Code_Number__c = cst.TelNumber;
            ct.Town__c                      = cst.Katr7;
            ct.Club_Type__c                 = cst.CLUBT;
            ct.Club_Type_D__c               = cst.CLUBT_D;
            ct.ProspectCode__c              = cst.PROS_CODE;

            cStage.add(ct);

        }

        //upsert cStage Customer_Number__c;
        return cStage;
     }

     //-- WRAPPER CLASS

     global class CustomerDetailWrapper{

        public String Kunnr     {get;set;}
        public String Name1     {get;set;}
        public String Telf1     {get;set;}
        public String Ort01     {get;set;}
        public String Regio     {get;set;}
        public String Pstlz     {get;set;}
        public String Land1     {get;set;}
        public String Parnr     {get;set;}
        public String SmtpAddr  {get;set;}
        public String FaxNumber {get;set;}
        public String TelNumber {get;set;}
        public String Parvw     {get;set;}
        public String StrSuppl1 {get;set;}
        public String StrSuppl2 {get;set;}
        public String Bzirk     {get;set;}
        public String Waers     {get;set;}
        public String Kdgrp     {get;set;}
        public String Vkbur     {get;set;}
        public String Vkgrp     {get;set;}
        public String Vtweg     {get;set;}
        public String Spart     {get;set;}
        public String Kvgr1     {get;set;}
        public String Kvgr2     {get;set;}
        public String Kvgr3     {get;set;}
        public String Loevm     {get;set;}
        public String Anred     {get;set;}
        public String Gbdat     {get;set;}
        public String Katr6     {get;set;}
        public String Katr7     {get;set;}
        public String J1ipanno  {get;set;}
        public String J1icstno  {get;set;}
        public String Town      {get;set;}
        public String Dist      {get;set;}
        public String Bezei     {get;set;}
        //additional changes - club type, description and prospect code - June 12 ,2015
        public String CLUBT {get;set;}
        public String PROS_CODE {get;set;}
        public String CLUBT_D {get;set;}
     }
}