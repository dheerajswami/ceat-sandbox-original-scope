public class Customer_CategoryPerformance {
    
    public String categoryValuesJSON {get;set;}
    public String monthlyCatValues{get;set;}
    public Set<String> categories {get;set;}
    public List<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesData {get;set;}
    
    private Account acct;
    public static final String CUST_ID_LENGTH   = Label.CPORT_LengthOfCustomerId;
    
    public Customer_CategoryPerformance(ApexPages.StandardController stdController){
        this.acct = (Account)stdController.getRecord();
        String sapCustomerNumber = [select KUNNR__c from Account where Id = :acct.Id].KUNNR__c;
        Map<String,Decimal> categoryValues = new Map<String,Decimal>();         
        
        String customerId = UtilityClass.addleadingZeros(sapCustomerNumber,Integer.valueOf(CUST_ID_LENGTH));
        String fromDate,toDate;
  
        
        fromDate = UtilityClass.getStringDate(Date.today().adddays(-30));//;Date.today().year() + '-' + monthVal + '-' + '01';
        toDate =  UtilityClass.getStringDate(Date.today());
        
        for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesRecord: CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fromDate,toDate)){
            system.debug('hhhh' + salesRecord.fkDate);
            //Date d = Date.parse(salesRecord.fkDate);
            if(categoryValues.containsKey(salesRecord.categoryT)){
                Decimal existingVal = categoryValues.get(salesRecord.categoryT);
                categoryValues.put(salesRecord.categoryT,existingVal + salesRecord.invoiceValue);
            }
            else{
                categoryValues.put(salesRecord.categoryT,salesRecord.invoiceValue);
            }
        }
        system.debug(categoryValues);
        categoryValuesJSON = '[[\'Category\',\'Value\'],';
        for(String cat : categoryValues.keySet()){
            categoryValuesJSON += '[\''+ cat + '\', '+ categoryValues.get(cat) + '],';
        }
        categoryValuesJSON = categoryValuesJSON.subString(0,categoryValuesJSON.length() - 1) + ']';
        
        fromDate = UtilityClass.getStringDate(Date.today().toStartOfMonth().addMonths(-5));
        toDate =  UtilityClass.getStringDate(Date.today());
        calculateMonthlyValues(customerId,fromDate,toDate);
        
        if(Date.today().month() < 4){
            fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-2,4,1));
        }
        else{
            fromDate = UtilityClass.getStringDate(Date.newInstance(Date.today().year()-1,4,1));
        }        
        toDate =  UtilityClass.getStringDate(Date.today());
        calculateYTDValues(customerId,fromDate,toDate); 
    }
    
    void calculateMonthlyValues(String customerId, String fromDate, String toDate){
        Map<String,Decimal> monthlyCategoryValues = new Map<String,Decimal>(); 
        categories = new Set<String>();
        Set<String> uniqueMonths = new Set<String>();
        Set<Date> uniqueMonthValues = new Set<Date>();
        List<Date> sortedMonths = new List<Date>();
        
        monthlyCatValues = '[';
        for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesRecord: CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fromDate,toDate)){
            categories.add(salesRecord.categoryT);
            Date monthOfSale = UtilityClass.getDateFromString(salesRecord.fkDate).toStartOfMonth();
            uniqueMonthValues.add(monthOfSale);
            String mapKeyVal = salesRecord.categoryT + ';' + monthOfSale;//month_val;
            if(monthlyCategoryValues.containsKey(mapKeyVal)){
                Decimal existingVal = monthlyCategoryValues.get(mapKeyVal);
                monthlyCategoryValues.put(mapKeyVal,existingVal + salesRecord.invoiceValue);
            }
            else{
                monthlyCategoryValues.put(mapKeyVal,salesRecord.invoiceValue);
            }
        }
        sortedMonths.addAll(uniqueMonthValues);
        sortedMonths.sort();
        for(Date month: sortedMonths){
            monthlyCatValues += '[\''+ UtilityClass.getMonthName(month.month()) + ' ' + month.year() + '\', ';
            for(String cat : categories){
                Decimal val = 0;
                if(monthlyCategoryValues.get(cat + ';' + month) != null){
                    val = monthlyCategoryValues.get(cat + ';' + month);
                }
                monthlyCatValues += val + ',';
            }
            monthlyCatValues = monthlyCatValues.subString(0,monthlyCatValues.length() - 1);//remove last ,
            monthlyCatValues += '],';
                 
        }
        
        monthlyCatValues = monthlyCatValues.subString(0,monthlyCatValues.length() - 1) + ']';
        //system.assertEquals(monthlyCatValues,'12');
    }
    
    void calculateYTDValues(String customerId, String fromDate, String toDate){
        Map<String,Decimal> monthlyCategoryValues = new Map<String,Decimal>(); 
        categories = new Set<String>();
        Set<String> uniqueMonths = new Set<String>();
        Set<Date> uniqueMonthValues = new Set<Date>();
        List<Date> sortedMonths = new List<Date>();
        
        monthlyCatValues = '[';
        for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesRecord: CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fromDate,toDate)){
            categories.add(salesRecord.categoryT);
            Date monthOfSale = UtilityClass.getDateFromString(salesRecord.fkDate).toStartOfMonth();
            uniqueMonthValues.add(monthOfSale);
            String mapKeyVal = salesRecord.categoryT + ';' + monthOfSale;//month_val;
            if(monthlyCategoryValues.containsKey(mapKeyVal)){
                Decimal existingVal = monthlyCategoryValues.get(mapKeyVal);
                monthlyCategoryValues.put(mapKeyVal,existingVal + salesRecord.invoiceValue);
            }
            else{
                monthlyCategoryValues.put(mapKeyVal,salesRecord.invoiceValue);
            }
        }
        sortedMonths.addAll(uniqueMonthValues);
        sortedMonths.sort();
        for(Date monthVal: sortedMonths){
            monthlyCatValues += '[\''+ UtilityClass.getMonthName(monthVal.month()) + ' ' + monthVal.year() + '\', ';
            for(String cat : categories){
                Decimal val = 0;
                if(monthlyCategoryValues.get(cat + ';' + monthVal) != null){
                    val = monthlyCategoryValues.get(cat + ';' + monthVal);
                }
                monthlyCatValues += val + ',';
            }
            monthlyCatValues = monthlyCatValues.subString(0,monthlyCatValues.length() - 1);//remove last ,
            monthlyCatValues += '],';
                 
        }
        if(monthlyCatValues.length() >2){
        	monthlyCatValues = monthlyCatValues.subString(0,monthlyCatValues.length() - 1) + ']';
            }
        else{
            monthlyCatValues = '[]';
        }
        //system.assertEquals(monthlyCatValues,'12');
    }

}