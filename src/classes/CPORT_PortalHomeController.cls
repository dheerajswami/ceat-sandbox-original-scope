global with sharing class CPORT_PortalHomeController {

    /*
     * Auther  :- Vivek Deepak  
     * Purpose :- Show customer related
     *            information 
     *
     *
     */
    
     // -- ATTRIBUTES    
    
    public static User currentUser                          {get;set;}
  //  public static Contact currentContact                    {get;set;}
    public static Account acc                               {get;set;}
    public string loggedInUserTerritory                     {get;set;}
    public static boolean hideValueColumnInPortal           {get;set;}    
    public static string isNotPortal{get;set;}
    
     //-- CONSTRUCTOR 
     
    
    public CPORT_PortalHomeController(){
        
        currentUser         = [Select ContactId from User where Id = :UserInfo.getUserId()];
        /*currentContact      = [Select Account.KUNNR__c, MailingStreet, MailingCity, MailingState, MailingPostalCode,
        MailingCountry from Contact where Id = :currentUser.ContactId];  */
           
         isNotPortal = '/customers';
        /* Get Logged In User Id and Territory*/  
        if(![Select IsPortalEnabled FROM User WHERE Id=:UserInfo.getUserId()].IsPortalEnabled){
            isNotPortal = '/apex';
            try{ 
                UserTerritory2Association  userTerrCode =[select territory2Id,id,territory2.Name from UserTerritory2Association where UserId=:UserInfo.getUserId() limit 1];  
                
                if(userTerrCode != null){
                    loggedInUserTerritory = userTerrCode.territory2.Name ;
                } 
            }catch(Exception e){}   
            
            hideValueColumnInPortal = true; 
        }
    }

   
     //-- METHODS 
     
     /*
     
     Naviagte page with customer Id for customer community tab
     
     */
     public PageReference redirectToPortalPage(){
        // Fetch account id from the portal user
        string pageName = '';
        try{
            Id accountId = [SELECT Id,AccountId FROM User WHERE Id = : UserInfo.getUserId()].AccountId;
            
            User user=  [select ProfileId,name,id from User where id=:UserInfo.getUserId()];
            String profileName=[Select Id,Name from Profile where Id=:user.profileId].Name;
            
            if(profileName.contains('Exports')){
                hideValueColumnInPortal = false;
                pageName = Label.Customer360ExportsPage;
            }else{
                pageName = Label.Customer360Page;
            }
            return (new Pagereference(pageName +'?Id='+accountId));             
        }catch(Exception e){
            system.debug(e.getMessage());
            return null;
        }
     }

     @RemoteAction
     global static String returnTransferURL(){
        string pageName = '';
        try{
            Id accountId = [SELECT Id,AccountId FROM User WHERE Id = : UserInfo.getUserId()].AccountId;
            
            User user=  [select ProfileId,name,id from User where id=:UserInfo.getUserId()];
            String profileName=[Select Id,Name from Profile where Id=:user.profileId].Name;
            
            if(profileName.contains('Exports')){
                hideValueColumnInPortal = false;
                pageName = Label.CustomerPortalExports;
            }else if(profileName.contains('Replacements')){
                pageName = Label.CsutomerPortalReplacement;
            }else if(profileName.contains('Customer Community-OE')) {
                pageName = Label.CustomerPortalOE;
            }else if(profileName.contains('Customer Community-Speciality')) {
                pageName = Label.CustomerPortalSpeciality;
            }
            return (System.URL.getSalesforceBaseUrl().toExternalForm() + pageName +'?Id='+accountId);             
        }catch(Exception e){
            system.debug(e.getMessage());
            return null;
        }
     }
     
     //--Added By Sneha 
     @RemoteAction
     global static List<SapUnrestrictedStock_Mapping.UnrestrictedStockMapping> getUnrestrictedStock(string territoy){
        return SapUnrestrictedStock_Mapping.getAllUnrestrictedStock(territoy);
     }
     /*
       Invoice Due
    */
    @RemoteAction
    global static List<CPORT_CustomerDue.CustomerDueMapping> getInvoiceDue(String cusNum, String fDate, String tDate,String territory){
        return CPORT_CustomerDue.getAllCustomerDueDetails(cusNum,fDate,tDate,territory);
    }    
    /*
    Get all material codes and category
    */
    @RemoteAction
    global static List<Material_Master_Sap__c> getAllMaterialCodes(){
        return [SELECT Name,Material_Group__c,Material_Number__c,Mat_Grp_Desc__c 
                FROM Material_Master_Sap__c 
                WHERE (Material_Group__c!=null OR Material_Number__c!=null)];
    }    
     /*
        Fetch All Material Codes 
    */
    @RemoteAction
    global static List<string> getMaterialCodes(string selectedCategory){
       list<Material_Master_Sap__c> materialMaster = new list<Material_Master_Sap__c>();
       list<string> matList = new  list<string>();
       materialMaster = [select Material_Group__c,Material_Number__c,Mat_Grp_Desc__c,Name from Material_Master_Sap__c where Material_Group__c=:selectedCategory AND (Material_Group__c!=null OR Material_Number__c!=null)];
       if(materialMaster.size()>0){    
         for(Material_Master_Sap__c mat : materialMaster){
            matList.add(mat.Material_Number__c);
         } 
       }
        return matList; 
    }  
     
     /*
        Fetch All SalesRegister Values of loggedIn User (Territory Wise) 
    */
    @RemoteAction
    global static List<CPORT_SalesRegisterDetails.SalesRegisterMapping> getSalesRegisterValues(string territory,string fDate, string tDate){
       return CPORT_SalesRegisterTLDetails.getAllSalesRegisterDetails(territory,fDate,tDate);
    }    
    
     /*
    Fetch Customer Basic Details and Outstanding
    */
   /* @RemoteAction
    global static List<CPORT_CustomerOutstandingDetails.CustomerOutstandingTL>  getCustomerOutstandingDetails(string territory){
       return CPORT_CustomerOutstandingDetails.getAllCustomerOutstandingDetails(territory);
    }*/
    
    /*
        Fetch Customer Basic Details and Outstanding
    */
    @RemoteAction
    global static List<CPORT_NBPPriceListMapping.NbpPriceMapping>  getNbpPriceListDetails(String matkl, String matnr){
        return CPORT_NBPPriceListMapping.getAllNBPPriceDetails(matkl,matnr);//('2010','101933');       
    }  
    //-- Sneha Code End
   
    /*
    Fetch All SAP Customer Basic 
    */
    @RemoteAction
    global static List<Account> getAllAccounts(){
        return [SELECT Id,Name,Sales_District_Text__c,KUNNR__c FROM Account WHERE KUNNR__c != null ];//LIMIT 100
    }

    /*
    Fetch Customer Basic Details and Outstanding
    */
    @RemoteAction
    global static AccountWrapper getAccountInfo(String accId){
        
        acc = [Select Id,Name,State__c,KUNNR__c,Street_1__c,Street_2__c,Town__c,PIN_code__c,Sales_District_Text__c,District__c,(SELECT Id,Circular_Number__c FROM Customer_Discounts__r) FROM Account WHERE Id =:accId];
        AccountWrapper awrap = CPORT_CustomerOutstandingDetails.getAllCustomerOutstandingDetailsCS(acc.KUNNR__c);
        if(awrap == null){
            awrap = new AccountWrapper(acc,0.0,0.0,0.0,0.0,0.0,0.0,0.0);
        }else{
            awrap.account = acc;
        }
        return awrap;
    }
    /*
     fetch Sales Register Detailed Records Exports
    */
    
     @RemoteAction
    global static list<SalesWrapper> fetchSalesRegisterValuesExports(String customerId, String fDate, String tDate){    
        map<string,Double> catWithInvoiceQtyMap   = new map<string,Double>();
        map<string,Double> catWithInvoiceValueMap = new map<string,Double>();
        map<string,Double> catWithExportsValueMap = new map<string,Double>();
        map<string,string> catWithCurrencyMap = new map<string,string>();
        set<string> catSet = new set<string>();
       
        //list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
        //List<String> eportCategoryList = new List<String>();
        list<SalesWrapper> SalesWrapperList = new list<SalesWrapper>();
        List<SalesWrapper> salesWrapperToDisplayList = new List<SalesWrapper>();
        
        Double qtyTotal   = 0;
        Double valueTotal = 0;
        Double exportValueTotal = 0;
        String currencyVal = '';
        List<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesRegisterList = new List<CPORT_SalesRegisterDetails.SalesRegisterMapping>();
        salesRegisterList = CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fDate,tDate);
        if(salesRegisterList.size()>0){
            for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesR: salesRegisterList){
                System.debug('==# Sales '+salesR);
                catSet.add(salesR.categoryT);
                // Category wise Invoice Quantity Total             
                if(catWithInvoiceQtyMap.containsKey(salesR.categoryT)){
                    qtyTotal = catWithInvoiceQtyMap.get(salesR.categoryT);
                    if(salesR.fkImg != null){
                        qtyTotal = qtyTotal + decimal.valueof(salesR.fkImg);                        
                    }else{
                        qtyTotal = qtyTotal + 0;
                    }                   
                    catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                }else{
                    if(salesR.fkImg != null){
                        qtyTotal   = decimal.valueof(salesR.fkImg);
                    }else{
                        qtyTotal   = 0;
                    }                   
                    catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                }
                
                // Category wise Invoice Value Total     
                if(catWithInvoiceValueMap.containsKey(salesR.categoryT)){
                    valueTotal = catWithInvoiceValueMap.get(salesR.categoryT);
                    if(salesR.invoiceValue != null){
                        valueTotal = valueTotal + salesR.invoiceValue;
                    }else{
                        valueTotal = valueTotal + 0;
                    }                   
                    catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                }else{
                    if(salesR.invoiceValue != null){
                        valueTotal   = salesR.invoiceValue;
                    }else{
                        valueTotal   = 0;
                    }                   
                    catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                }
                //currency value
                if(catWithCurrencyMap.containsKey(salesR.categoryT)){
                currencyVal  =salesR.kdgrpAuft;
                 catWithCurrencyMap.put(salesR.categoryT,currencyVal);
                }
                else{
                currencyVal  =salesR.kdgrpAuft;
                catWithCurrencyMap.put(salesR.categoryT,currencyVal);
                }
                // Exports Value total
                if(catWithExportsValueMap.containsKey(salesR.categoryT)){
                    
                    exportValueTotal = catWithExportsValueMap.get(salesR.categoryT);
                    if(salesR.netwr != null){
                        exportValueTotal = exportValueTotal + salesR.netwr;                     
                    }else{
                        exportValueTotal = exportValueTotal + 0;
                    }                   
                    catWithExportsValueMap.put(salesR.categoryT,exportValueTotal);
                }else{
                    
                    if(salesR.netwr != null){
                        exportValueTotal   = salesR.netwr;
                    }else{
                        exportValueTotal   = 0;
                    }                   
                    catWithExportsValueMap.put(salesR.categoryT,exportValueTotal);
                }
            }
            // Added by Supriya.
            for(String cat : catSet){
                if(catWithInvoiceQtyMap.get(cat) >=0 || catWithInvoiceValueMap.get(cat) >=0){
                    SalesWrapper wrap = new SalesWrapper();
                    wrap.catName = cat;                    
                    wrap.qty     = catWithInvoiceQtyMap.get(cat);                  
                    wrap.value   = catWithInvoiceValueMap.get(cat);    
                    wrap.exports_value  = catWithExportsValueMap.get(cat);
                    wrap.curVal = catWithCurrencyMap.get(cat);
                    SalesWrapperList.add(wrap);
                    system.debug('&&&&&&&&===='+SalesWrapperList);
                }
           }
           // Added by Neha. It is added to avoid displaying tubes and flaps on the page //
             /*for(string cat : catSet){  
                    if(!(cat.contains('TUBES')) && !(cat.contains('FLAPS'))) {
                        System.debug('==# Cat '+cat);
                        SalesWrapper wrap = new SalesWrapper();
                        wrap.catName    = cat;                    
                        wrap.qty        = catWithInvoiceQtyMap.get(cat);
                        wrap.value      = catWithInvoiceValueMap.get(cat);
                        if(catWithInvoiceQtyMap.get(cat) >=0 || catWithInvoiceValueMap.get(cat) >=0) {
                            wrap.exports_value  = catWithExportsValueMap.get(cat);
                        }
                        salesWrapperToDisplayList.add(wrap);
                    }
                    
             }*/
        }
        //system.debug('==# SalesWrapperDisplayList '+salesWrapperToDisplayList);
        return SalesWrapperList;
    }
       
    
    
    /*
     fetch Sales Register Detailed Records
    */
    @RemoteAction
    global static List<CPORT_SalesRegisterDetails.SalesRegisterMapping> fetchSalesRegisterValues(String customerId, String fDate, String tDate, String sec){   
    //System.debug('==# SalesRegisterValues '+ CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fDate,tDate));
       return CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fDate,tDate);
    }
    /*
     fetch Summerized Sales Register Records
    */
    @RemoteAction
    global static List<SalesWrapper> getSummerizedSalesRegisterValues(String customerId, String fDate, String tDate,string invoice){
        map<string,Double> catWithInvoiceQtyMap   = new map<string,Double>();
        map<string,Double> catWithInvoiceValueMap = new map<string,Double>();
        set<string> catSet = new set<string>();
        
        list<SalesWrapper> SalesWrapperList = new list<SalesWrapper>();
        List<SalesWrapper> salesWrapperToDisplayList = new List<SalesWrapper>();
       
        Double qtyTotal   = 0;
        Double valueTotal = 0;
        
        List<CPORT_SalesRegisterDetails.SalesRegisterMapping> salesRegisterList =  CPORT_SalesRegisterDetails.getAllSalesRegisterDetails(customerId,fDate,tDate);
        system.debug(salesRegisterList);
        if(salesRegisterList.size()>0){
            for(CPORT_SalesRegisterDetails.SalesRegisterMapping salesR: salesRegisterList){
                if(invoice==salesR.invoiceType){
                    catSet.add(salesR.categoryT);
                    // Category wise Invoice Quantity Total   
                    if(catWithInvoiceQtyMap.containsKey(salesR.categoryT)){
                        qtyTotal = catWithInvoiceQtyMap.get(salesR.categoryT);
                        if(salesR.fkImg != null){
                            qtyTotal = qtyTotal + decimal.valueof(salesR.fkImg);                        
                        }else{
                            qtyTotal = qtyTotal + 0;
                        }                   
                        catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                    }else{
                        if(salesR.fkImg != null){
                            qtyTotal   = decimal.valueof(salesR.fkImg);
                        }else{
                            qtyTotal   = 0;
                        }                   
                        catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                     }
                     
                      // Category wise Invoice Value Total     
                    if(catWithInvoiceValueMap.containsKey(salesR.categoryT)){
                        valueTotal = catWithInvoiceValueMap.get(salesR.categoryT);
                        if(salesR.invoiceValue != null){
                            valueTotal = valueTotal + salesR.invoiceValue;
                        }else{
                            valueTotal = valueTotal + 0;
                        }                   
                        catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                    }else{
                        if(salesR.invoiceValue != null){
                            valueTotal   = salesR.invoiceValue;
                        }else{
                            valueTotal   = 0;
                        }                   
                        catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                    }
                
                }else if(invoice == 'All'){
                    // FOR ALL Invoices add by Vivek
                    catSet.add(salesR.categoryT);
                    // Category wise Invoice Quantity Total   
                    if(catWithInvoiceQtyMap.containsKey(salesR.categoryT)){
                        qtyTotal = catWithInvoiceQtyMap.get(salesR.categoryT);
                        if(salesR.fkImg != null){
                            qtyTotal = qtyTotal + decimal.valueof(salesR.fkImg);                        
                        }else{
                            qtyTotal = qtyTotal + 0;
                        }                   
                        catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                    }else{
                        if(salesR.fkImg != null){
                            qtyTotal   = decimal.valueof(salesR.fkImg);
                        }else{
                            qtyTotal   = 0;
                        }                   
                        catWithInvoiceQtyMap.put(salesR.categoryT,qtyTotal);
                     }
                     
                      // Category wise Invoice Value Total     
                    if(catWithInvoiceValueMap.containsKey(salesR.categoryT)){
                        valueTotal = catWithInvoiceValueMap.get(salesR.categoryT);
                        if(salesR.invoiceValue != null){
                            valueTotal = valueTotal + salesR.invoiceValue;
                        }else{
                            valueTotal = valueTotal + 0;
                        }                   
                        catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                    }else{
                        if(salesR.invoiceValue != null){
                            valueTotal   = salesR.invoiceValue;
                        }else{
                            valueTotal   = 0;
                        }                   
                        catWithInvoiceValueMap.put(salesR.categoryT,valueTotal);
                    }
                }               
            }
            
             for(string cat : catSet){  
                    
                    SalesWrapper wrap = new SalesWrapper();
                    wrap.catName    = cat;                    
                    wrap.qty        = catWithInvoiceQtyMap.get(cat);
                    wrap.value      = catWithInvoiceValueMap.get(cat);
                    SalesWrapperList.add(wrap);
             }
             
              for(string cat : catSet){  
                    if(!(cat.contains('TUBES')) && !(cat.contains('FLAPS'))) {
                    SalesWrapper wrap = new SalesWrapper();
                    wrap.catName    = cat;                    
                    wrap.qty        = catWithInvoiceQtyMap.get(cat);
                    wrap.value      = catWithInvoiceValueMap.get(cat);
                    salesWrapperToDisplayList .add(wrap);
             }
             
            } 
        }
        return SalesWrapperList;
    } 
    
    /*
        Fetch All ClaimData of Customer Wise / Territory Wise 
    */      
    @RemoteAction
    global static List<CPORT_ClaimDateMapping.ClaimDataMapping> fetchClaimData(String customerId, String fDate, String tDate, String territory){        
        return CPORT_ClaimDateMapping.getAllClaimData(customerId,fDate,tDate,territory);
    }
    
    /*
        Account Statement
    */

    @RemoteAction
    global static List<CPORT_CreditDebitNotes.CreditDebitMapping> fetchCreditDebitNotes(String customerId, String fDate, String tDate, String territory){        
        //System.debug('==# CDNotes being called.'+CPORT_CreditDebitNotes.getAllCreditDebitNotes(customerId,fDate,tDate,territory));
         System.debug('CustId '+customerId); 
         System.debug('fDate '+fDate);
         System.debug('Territory '+territory);  
        return CPORT_CreditDebitNotes.getAllCreditDebitNotes(customerId,fDate,tDate,territory);
    }

    /*
        Credit Debit Notes
    */

    // Added by Neha

    @RemoteAction
    global static List<SapAccountSDSMapping.sdsAccountMapping> fetchSDSaccounts(String customerId, String fDate, String tDate,String temp){        
       // System.debug('==# SapAccountSDSMapping being called.'+SapAccountSDSMapping.getSDSaccounts(customerId,fDate,tDate));
        return SapAccountSDSMapping.getSDSaccounts(customerId,fDate,tDate);
    }

    @RemoteAction
    global static List<CPORT_CNDNListing_Mapping.CreditDebitMapping> fetchCreditDebitNotesCNDN(String customerId, String fDate, String tDate, String sec){        
        List<CPORT_CNDNListing_Mapping.CreditDebitMapping> cndnList = new List<CPORT_CNDNListing_Mapping.CreditDebitMapping>();
        system.debug(acc);
        for(CPORT_CNDNListing_Mapping.CreditDebitMapping cn : CPORT_CNDNListing_Mapping.getAllCreditDebitNotes(customerId,fDate,tDate)){
            if((cn.DocType=='ZC' || cn.DocType=='ZD' || cn.DocType=='DR' || cn.DocType=='KR' ) || (cn.DocType=='RV' && cn.Fkart=='S2') ){
                //system.debug(cn.Fkart);
                cndnList.add(cn);
            }
            system.debug(cn.Fkart);
        }
        return cndnList;
    }

    /*
        For Invoice due from customer outstanding where date is greater than today
    */
    @RemoteAction
    global static List<CPORT_CustomerOutstandingAndLimit.CustomerOutStandingMapping> fetchCustomerInvoiceDue(String customerId,String fDate, String tDate, String sec){
        // Only customer Id parameter is used
        System.debug('Inside Customer Due Method' + customerId);
        List<CPORT_CustomerOutstandingAndLimit.CustomerOutStandingMapping> listToReturn = new List<CPORT_CustomerOutstandingAndLimit.CustomerOutStandingMapping>();
        for(CPORT_CustomerOutstandingAndLimit.CustomerOutStandingMapping temp : CPORT_CustomerOutstandingAndLimit.getAllCustomerOutstandingDetails(customerId)){
            //if(UtilityClass.getDateFromString(temp.Duedate) >= date.today()){
                System.debug('==## '+temp);
                listToReturn.add(temp);
            //}
        }
        System.debug('==# Account List '+ listToReturn);
        return listToReturn;
    }

    /*Get all discounts and schemes for a customer*/
    @RemoteAction
    global static List<CPORT_DiscountsAndSchemes.DiscountsAndSchemes> fetchDiscounts(String customerId, String circleNum){        
        return CPORT_DiscountsAndSchemes.getAllDiscounts(circleNum,customerId);
    }

    /*Get Discount Master */
    @RemoteAction
    global static List<CPORT_DISC_TLD.SAP_Discount_Master> fetchDiscountMaster(String customerId, String circleNum){        
        return CPORT_DISC_TLD.getDiscountMaster(circleNum,customerId);
    }

    // WRAPPER
    global class AccountWrapper{
        public Account account {get;set;}
        public Decimal deposit {get;set;}
        public Decimal advance {get;set;}
        public Decimal limitavailable {get;set;}
        public Decimal netoutstand {get;set;}
        public Decimal permisbleamt {get;set;}
        public Decimal totoutstand {get;set;}
        public Decimal totoverdue {get;set;}
        
        
        public AccountWrapper(Account account,Decimal deposit,Decimal advance,Decimal limitavailable,Decimal netoutstand,Decimal permisbleamt,Decimal totoutstand,Decimal totoverdue){
            this.account = account;
            this.deposit = deposit;
            this.advance = advance;
            this.limitavailable = limitavailable;
            this.netoutstand = netoutstand;
            this.permisbleamt = permisbleamt;
            this.totoutstand = totoutstand;
            this.totoverdue = totoverdue;
            
        }
    }
     
     // Added by Sneha Used in Sales Register
     global class SalesWrapper{        
        public string catName {get;set;}
        public Decimal qty {get;set;}
        public Decimal value {get;set;}
        public Decimal exports_value {get;set;}
        public String curVal {get;set;}
        //public Decimal total {get;set;}
     }

}