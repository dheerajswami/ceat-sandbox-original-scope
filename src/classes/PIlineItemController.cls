global with sharing class PIlineItemController {
    public List<SelectOption> materialGroupOption {get; set;}
    public List<SelectOption> categoryOption {get; set;}
    public List<SelectOption> setOption {get; set;}
    public List<SelectOption> skuOption {get; set;}
    public String selectedBrand {get; set;}
    public String selectedSetsOrPieces {get; set;}
    public String selectedCategory {get; set;}
    public String selectedSku {get; set;}
    public Decimal quantity {get; set;}
    public Double unitPrice {get; set;}
    public PI_Line_Item__c piLineItemRecord {get; set;}
    public Id piLineItemId {get; set;}
    public Id piId {get; set;}
    public PIlineItemController(ApexPages.StandardController stdController) {
        piLineItemId                = ApexPages.currentPage().getParameters().get('Id');
        piLineItemRecord            = [SELECT Brand__c, Category__c, Material_Master__r.Name, Proforma_Invoice__c,
                                    Material_Description__c, Quantity__c, Unit_Price__c, System_SetsOrPieces__c
                                    FROM PI_Line_Item__c WHERE Id =: piLineItemId];
        System.debug('==#1 '+ piLineItemRecord);
        piId                        = piLineItemRecord.Proforma_Invoice__c;
        selectedBrand               = piLineItemRecord.Brand__c;
        selectedCategory            = piLineItemRecord.Category__c;
        selectedSku                 = piLineItemRecord.Material_Master__r.Name;
        selectedSetsOrPieces        = piLineItemRecord.System_SetsOrPieces__c;
        quantity                    = piLineItemRecord.Quantity__c;
        unitPrice                   = piLineItemRecord.Unit_Price__c;
        populateSetsOrPieces(selectedSetsOrPieces);
        populateMaterialGroup();
        populateCategory(selectedBrand, selectedSetsOrPieces);
        populateSKUs(selectedBrand, selectedSetsOrPieces, selectedCategory);
        
    }

    public void populateSetsOrPieces(String selectedSetsOrPieces) {
        setOption = new List<SelectOption>();
        System.debug('==#2 '+selectedSetsOrPieces);
        Map<String, String> setsOrPiecesMap = new Map<String, String>();
        setsOrPiecesMap.put('DIEN', 'Sets');
        setsOrPiecesMap.put('ZFGS', 'Pieces');
        setOption.add(new SelectOption(selectedSetsOrPieces, setsOrPiecesMap.get(selectedSetsOrPieces)));
        if(selectedSetsOrPieces == 'DIEN') {
            setOption.add(new SelectOption('ZFGS', 'Pieces'));
        }else if(selectedSetsOrPieces == 'ZFGS') {
            setOption.add(new SelectOption('DIEN', 'Sets'));
        }
    }

    public void populateMaterialGroup() {
        List<Brand__c> brandList = Brand__c.getall().values();
        List<String> brandNameList = new List<String>();
        for(Brand__c tmpBrand : brandList) {
            brandNameList.add(tmpBrand.Name);
        }
        System.debug('==# Custom Setting value '+ brandNameList);
        Map<String, String> matGroupToNameMap = new Map<String, String>();
        materialGroupOption = new List<SelectOption>();
        materialGroupOption.add(new SelectOption(selectedBrand,selectedBrand));
        for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Mat_Grp_Desc__c,Mat_Grp1_Desc__c FROM Material_Master_Sap__c WHERE Mat_Grp1_Desc__c IN : brandNameList]) {
            if(!matGroupToNameMap.containsKey(tempMmS.Material_Group_1__c) && tempMmS.Material_Group_1__c != null && tempMmS.Mat_Grp1_Desc__c != selectedBrand) {
               // matGroupToNameMap.put(tempMmS.Material_Group_1__c, tempMmS.Mat_Grp1_Desc__c);
               matGroupToNameMap.put(tempMmS.Mat_Grp1_Desc__c, tempMmS.Mat_Grp1_Desc__c);
            }
        }
        System.debug('==#3 '+matGroupToNameMap);
        for(String str : matGroupToNameMap.keySet()) {
            materialGroupOption.add(new SelectOption(str, matGroupToNameMap.get(str)));
            
        }
        System.debug('==#4 '+materialGroupOption);
    }

    public void populateCategory(String brand, String setsOrPieces) {
        System.debug('==#5 Inside Method '+brand);
        Map<String, String> brandToMaterialGrpDescMap = new Map<String, String>();
        categoryOption = new List<SelectOption>();
        categoryOption.add(new SelectOption(selectedCategory,selectedCategory));
        for(Material_Master_Sap__c tempMm : [SELECT Material_Group_1__c,Mat_Grp_Desc__c,Name, Mat_Grp1_Desc__c, Mtart__c FROM Material_Master_Sap__c WHERE Mat_Grp1_Desc__c =: brand AND Mtart__c =: setsOrPieces ORDER BY Mat_Grp_Desc__c]) {
            if(!brandToMaterialGrpDescMap.containsKey(tempMm.Mat_Grp_Desc__c) && tempMm.Mat_Grp_Desc__c != selectedCategory) {
                brandToMaterialGrpDescMap.put(tempMm.Mat_Grp_Desc__c, tempMm.Mat_Grp_Desc__c);
            }
        }
        for(String str : brandToMaterialGrpDescMap.keySet()) {
            categoryOption.add(new SelectOption(str, brandToMaterialGrpDescMap.get(str)));

        }
    }

    public void populateSKUs(String brand, String setsOrPieces, String category) {

        Map<String, String> idToMatMasterNameMap = new Map<String, String>();
        skuOption = new List<SelectOption>();
        if(!Test.isRunningTest()) {
            skuOption.add(new SelectOption(selectedSku, selectedSku)); 
       }

        if(setsOrPieces == 'DIEN') {
           for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name,Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: category AND 
                                                        Mat_Grp1_Desc__c =: brand AND 
                                                        Mtart__c =: 'DIEN' ORDER BY Name]) {
                if(!idToMatMasterNameMap.containsKey(tempMmS.Name) && tempMmS.Name != selectedSku) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                }
            } 
        }else if(setsOrPieces == 'ZFGS') {
            for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name, Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: category AND 
                                                        Mat_Grp1_Desc__c =: brand AND 
                                                        Mtart__c =: 'ZFGS' ORDER BY Name]) {
                if(!idToMatMasterNameMap.containsKey(tempMmS.Name) && tempMmS.Name != selectedSku) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                }
            }
        }
        for(String str : idToMatMasterNameMap.keySet()) {
            skuOption.add(new SelectOption(str, str));

        }

    }


    /**** Fetch Material Group Description related to a particular Brand ****/
    @RemoteAction
    global static Map<String, String> fetchMaterialGroupDesc(String brand, String setsOrPieces) {
        Map<String, String> brandToMaterialGrpDescMap = new Map<String, String>();
        Set<String> matGrpDescSet = new Set<String>();
        for(Material_Master_Sap__c tempMm : [SELECT Material_Group_1__c,Mat_Grp_Desc__c,Name, Mat_Grp1_Desc__c, Mtart__c FROM Material_Master_Sap__c WHERE Mat_Grp1_Desc__c =: brand AND Mtart__c =: setsOrPieces ORDER BY Mat_Grp_Desc__c]) {
            if(!matGrpDescSet.contains(tempMm.Mat_Grp_Desc__c)) {
                brandToMaterialGrpDescMap.put(tempMm.Mat_Grp_Desc__c, tempMm.Mat_Grp_Desc__c);
                matGrpDescSet.add(tempMm.Mat_Grp_Desc__c);
            }
        }

        return brandToMaterialGrpDescMap;
    }


    /**** Fetch SKU related to a particular Material Group Description ****/
    @RemoteAction
    global static Map<String, String> fetchSKUs(String matGrpDesc, String selectedBrand, String setsOrPieces) {
        System.debug(selectedBrand+'==#10 '+matGrpDesc);
        Map<String, String> idToMatMasterNameMap = new Map<String, String>();
        Set<String> matMasterIdSet = new Set<String>();

        if(setsOrPieces == 'DIEN') {
           for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name,Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: matGrpDesc AND 
                                                        Mat_Grp1_Desc__c =: selectedBrand AND 
                                                        Mtart__c =: 'DIEN' ORDER BY Name]) {
                if(!matMasterIdSet.contains(tempMmS.Name)) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                    matMasterIdSet.add(tempMmS.Name);
                }
            } 
        }else if(setsOrPieces == 'ZFGS') {
            for(Material_Master_Sap__c tempMmS : [SELECT Material_Group_1__c,Material_Number__c,Mat_Grp_Desc__c,
                                                        Name, Mtart__c, Mat_Grp1_Desc__c FROM Material_Master_Sap__c 
                                                        WHERE Mat_Grp_Desc__c =: matGrpDesc AND 
                                                        Mat_Grp1_Desc__c =: selectedBrand AND 
                                                        Mtart__c =: 'ZFGS' ORDER BY Name]) {
                if(!matMasterIdSet.contains(tempMmS.Name)) {
                    idToMatMasterNameMap.put(tempMmS.Name, tempMmS.Name);
                    matMasterIdSet.add(tempMmS.Name);
                }
            }
        }
        
        return idToMatMasterNameMap;
    }


    @RemoteAction
    global static String save(Id parentId, Id recordId, PI_Line_Item__c piLineItem) {
        System.debug('==# LineItems '+ piLineItem);
        Map<String, Material_Master_Sap__c> matNameToMatMasterMap =new Map<String, Material_Master_Sap__c>();
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        for(Material_Master_Sap__c tmpMaster : [SELECT Id, Material_Number__c, Name FROM Material_Master_Sap__c WHERE Name != '.'] ) {
            if(!matNameToMatMasterMap.containsKey(tmpMaster.Name)) {
                matNameToMatMasterMap.put(tmpMaster.Name, tmpMaster);
            }
        }
        Double containers;
        containers = calculateLoadibility(piLineItem.Material_Master__c, piLineItem.Quantity__c);
        if(!Test.isRunningTest()) {
            Id matMasterId                                 = matNameToMatMasterMap.get(piLineItem.Material_Master__c).Id;
            String matNum                                  = matNameToMatMasterMap.get(piLineItem.Material_Master__c).Material_Number__c;   
            //String  matNum                                 = tmLineItem.Material_Number__c;
            piLineItem.Material_Number__c                  = matNum;
            piLineItem.Material_Master__c                  = matMasterId;
            //tmLineItem.Price__c                            = invoiceCurrency + ' ' + String.valueOf(tmLineItem.Price__c);
            //piLineItem.Unit_Price__c                       = piLineItem.Unit_Price__c;
            //System.debug('==#107 '+tmLineItem.Unit_Price__c);
            piLineItem.Id                                  = recordId;
            piLineItem.No_of_Container__c                  = containers;
            //piLineItem.Proforma_Invoice__c                 = parentId;
        }
        update piLineItem;
        String url = baseURL+'/'+parentId;
        return url;
    }


    public static Double calculateLoadibility(String sku, Decimal quantity) {
        Map<String, string> materialNoToCapacityMap = new  Map<String, string>();
        Map<String, String> materialNameToMaterialNoMap = new Map<String, String>();
        Double capacity;
        Decimal noOfContainers;

        for(Material_Master_Sap__c tmpMaster : [SELECT Id, Material_Number__c, Name FROM Material_Master_Sap__c]) {
            if(!materialNameToMaterialNoMap.containsKey(tmpMaster.Name)) {
                materialNameToMaterialNoMap.put(tmpMaster.Name, tmpMaster.Material_Number__c);
            }
        }

        for(Loadability__c tmpLoad : [SELECT Capacity__c,Category__c,Group__c,Material_Number__c,Name FROM Loadability__c]) {
            if(!materialNoToCapacityMap.containsKey(tmpLoad.Material_Number__c)) {
                materialNoToCapacityMap.put(tmpLoad.Material_Number__c, tmpLoad.Capacity__c);
                //materialNoToCapacityMap.put(UtilityClass.addleadingZeros(tmpLoad.Material_Number__c, 18), tmpLoad.Capacity__c);
                
            }
        }
        System.debug('==#12 '+materialNoToCapacityMap);

        try {
            System.debug('==#13 '+ sku);
            System.debug('==#14 '+materialNameToMaterialNoMap.get(sku));
            System.debug('==#15 '+materialNoToCapacityMap.get(materialNameToMaterialNoMap.get(sku)));
            if(materialNoToCapacityMap.get(materialNameToMaterialNoMap.get(sku)) != null) {
                capacity = Double.valueOf(materialNoToCapacityMap.get(materialNameToMaterialNoMap.get(sku)));
                System.debug('==#16 '+ capacity);
                noOfContainers = quantity / capacity;
                return (noOfContainers.setscale(2));
            }else {
                return null;
            }
            
        }catch(Exception e) {
            return Double.valueOf('');
        }
    }

    public class customException extends Exception{}

}