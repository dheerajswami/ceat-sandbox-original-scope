@isTest (seeAllData = false)
private class SP_BatchForExportBUUpdated_TestClass{
    static testmethod void testLoadData() {
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBUExport = new Territory2 ();
        Territory2 terrtypeClusterExportAN03 = new Territory2 ();
        Territory2 terrtypeCountryAN03AU = new Territory2 ();
        Territory2 terrtypeCountryAN03BMA = new Territory2 ();
        Territory2 terrtypeCountryAN03CN = new Territory2 ();
        Territory2 terrtypeCountryAN03FJ = new Territory2 ();
        Territory2 terrtypeCountryAN03HK = new Territory2 ();
        Territory2 terrtypeCountryAN03JP = new Territory2 ();
        Territory2 terrtypeCountryAN03KH = new Territory2 ();
        Territory2 terrtypeCountryAN03KR = new Territory2 ();
        Territory2 terrtypeCountryAN03LK = new Territory2 ();
        Territory2 terrtypeCountryAN03MM = new Territory2 ();
        Territory2 terrtypeCountryAN03MY = new Territory2 ();
        Territory2 terrtypeCountryAN03NZ = new Territory2 ();
        Territory2 terrtypeCountryAN03PH = new Territory2 ();
        Territory2 terrtypeCountryAN03TH = new Territory2 ();
        Territory2 terrtypeCountryAN03VN = new Territory2 ();
        id terrTypeBU ;
        id terrTypeCluster ;
        id terrTypeCountry ;
        user userSrManagerExportData = new user();
        user userSrManagerExportDataBU = new user();
        String exportApproverLabel  = System.Label.Export_Approver;
        String tlPageName   = System.Label.ExportPage;
        UserTerritory2Association srManagerExportuser = new UserTerritory2Association ();
        UserTerritory2Association srManagerExportuserBU = new UserTerritory2Association();
        String exportDealer             = System.Label.Export_Dealer; 
        String exportForcast            = System.Label.Export_Forcast_BU;
        String recTypeExportForecast    = System.Label.Export_Forcast;
        List<Territory2Type > terriType        = new list<Territory2Type>();  
        List<Territory2Type > terriTypeCountry = new list<Territory2Type>(); 
        List<Territory2Type > terriTypeBU = new list<Territory2Type>();     
        List<Territory2> Clusters    = new list<Territory2>(); 
        id exportAccountID = UtilityClass.getRecordTypeId('Account','Exports');
        //terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        //insert terrModel;
        terriType               = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'Cluster'];
        terrTypeCluster = terriType[0].id;
        /*
        for(Territory2Type temp : terriType){
            terrTypeCluster.add(temp.id);
        }
        */
        terriTypeCountry = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'Country'];
        terrTypeCountry = terriTypeCountry[0].id;
        terriTypeBU = [SELECT id, DeveloperName,MASTERLABEL from Territory2Type where  MASTERLABEL = 'BU'];
        terrTypeBU = terriTypeBU [0].id;
        Clusters    = [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: terrTypeCluster ];
        
        system.debug(terrTypeBU+'terrTypeBU');
        
        Date d = system.today();
        String month = String.valueOf(d.month());
        String year = String.valueOf(d.Year());
     
        Id exportForcastClusterId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeExportForecast Limit 1].Id;
        Id exportDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: exportDealer Limit 1].Id;
        // Load the test Sales planning Staging from the static resource
        //System.runAs(userSrManagerExportData) {
        
        List<sObject> listCatExport = Test.loadData(Export_Sales_Planning_Category__c.sObjectType, 'Export_Category');
        List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'Account_Export');
        for(sObject temp : listAccoutData){
            Account acc =(Account)temp;
            acc.Ship_to_Country__c = 'AO;BF;GN';
            acc.RecordTypeID = exportAccountID;
        }
        List<sObject> listExportDealerData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'Export_Dealer');
        List<sObject> listExportForcastData = Test.loadData(Sales_Planing_Staging__c.sObjectType, 'Export_Forcast');
        
        for(sObject li : listExportDealerData ){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listExportDealerData ;
        for(sObject li : listExportForcastData ){
            Sales_Planing_Staging__c temp = (Sales_Planing_Staging__c)li;
            temp.Month__c = month;
            temp.Year__c = year;
            
        }
        update listExportForcastData ;
        //List<sObject> listSPData = Test.loadData(Sales_Planning__c.sObjectType, 'SP_Forcast_Data');
     
        
    //}
        test.StartTest();
        SP_BatchForExportBU_Updated nn = new SP_BatchForExportBU_Updated();
        nn.query                  = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID LIMIT 100 '; 
        ID batchprocessid = database.executebatch(nn); 
        //System.abortJob(batchprocessid);
        test.stopTest() ;
        
    }
}