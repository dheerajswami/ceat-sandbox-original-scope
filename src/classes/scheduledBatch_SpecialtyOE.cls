global class scheduledBatch_SpecialtyOE implements Schedulable{
   global void execute(SchedulableContext sc) {
      SP_BatchForSpecialty_OE_BU b = new SP_BatchForSpecialty_OE_BU(); 
      database.executebatch(b,200);
   }
}