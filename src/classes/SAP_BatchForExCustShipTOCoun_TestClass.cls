@isTest  (seeAllData = false)
private class SAP_BatchForExCustShipTOCoun_TestClass{  
    static Account acc;
    
    private class WebServiceMockImpl implements WebServiceMock
    {        
        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
             sap_zws_cust_shtp zw = new sap_zws_cust_shtp(); 
             sap_zws_cust_shtp.ZsdCustShtp z = new sap_zws_cust_shtp.ZsdCustShtp();
             z.Kunnr = '56000320';
             z.Land1 = 'PH';
             z.Kunn2 = '56000320';
             sap_zws_cust_shtp.ZsdCustShtpResponse_element res = new sap_zws_cust_shtp.ZsdCustShtpResponse_element();
             Sap_zws_cust_shtp.TableOfZsdCustShtp vv = new Sap_zws_cust_shtp.TableOfZsdCustShtp();
             list<sap_zws_cust_shtp.ZsdCustShtp> item1 = new list<sap_zws_cust_shtp.ZsdCustShtp> ();
             item1.add(z);
             vv.item = item1;
             res.ItOutput = vv;
                response.put('response_x', res);
            return;
            
        }     
    }
    static testmethod void testLoadData() {
        
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        //List<sObject> listAccoutData = Test.loadData(Account.sObjectType, 'AccountDataActual');
        id exportRecTypeId = UtilityClass.getRecordTypeId('Account','Exports');
        account ac = new account();
        ac.name ='kishlaytest';
        ac.KUNNR__c = '56000320';
        ac.Last_Month_SDS__c = 234244;
        ac.RecordtypeID = exportRecTypeId;
        insert ac;
        //acc = CEAT_InitializeTestData.createAccount('CustomerTest','50003327');
        //database.insert(acc);
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        Test.startTest() ;
        SAP_BatchForExportCustomerShipTOCountry nn = new SAP_BatchForExportCustomerShipTOCountry();
        //nn.query                  = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: terriType[0].id LIMIT 100 '; 
        
        ID batchprocessid = database.executebatch(nn,2000); 
        //System.abortJob(batchprocessid);
        Test.stopTest();
    }
   
}