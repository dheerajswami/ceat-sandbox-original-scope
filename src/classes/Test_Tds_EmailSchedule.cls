/*
 * This class contains unit tests for validating the behavior of Apex triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_Tds_EmailSchedule{
static TestMethod void meth(){
test.startTest();
Account acc = new Account(name='TestAccount');
insert acc;
Contact con = new Contact(lastname='TestLastName',email='Test@123.com',Tds_Check__c=true,AccountId=acc.Id);
insert con;
Tds__C Tds_Obj = new Tds__C(TDS_Account__c=acc.Id);
insert Tds_obj;
Attachment attch = new Attachment();
blob b= Blob.valueOf('test body');
attch.parentid=Tds_Obj.Id;
attch.body=b;
attch.name='0BI44LZTDF_Q1_2015-16.docx';
insert attch;
SchedulableContext sc = null;
EmailScheduledApex  emailschedule = new EmailScheduledApex ();
emailschedule.execute(sc);
test.stopTest();


}
}