/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class BT_PastVisitUnsuccesfull_TestClass {

    static testMethod void myUnitTest() {
    	Integer tempNumber = Integer.valueOf(Math.random()*100000000);
    	
        Prospect_Customer_Counter__c counterL = ActionLog_Initialize_TestData.createCustomerSetP();
        database.insert(counterL);
        
        Account account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
        database.insert(account);

        PJP__c pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
        database.insert(pjp);
        // The query used by the batch job.
       String query = 'SELECT Id,Visit_Day__c,Status__c FROM Visits_Plan__c WHERE Visit_Day__c < TODAY AND (Status__c = \'Scheduled\' OR Status__c = null)';

       // Create some test merchandise items to be deleted
       //   by the batch job.
       Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Visits_Plan__c; 
       Map<String,Schema.RecordTypeInfo> visitRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
       String DEFAULT_VISIT_RT     = 'Visit';
       List<Visits_Plan__c> visits = new List<Visits_Plan__c>();
       for (Integer i=0;i<10;i++) {
           Visits_Plan__c visitRecord = new Visits_Plan__c(RecordTypeId = visitRecordTypeInfo.get(DEFAULT_VISIT_RT).getRecordTypeId(),Dealer__c = account.Id,Visit_Day__c = date.today().addDays(-1),Type_of_Day__c = 'Visit Day',Status__c = '',Master_Plan__c = pjp.Id);
           visits.add(visitRecord);
       }
       database.insert(visits);

       Test.startTest();
       BT_PastVisitUnsuccesfull bth = new BT_PastVisitUnsuccesfull();
       Database.executeBatch(bth);
       Test.stopTest();

       
    }
}