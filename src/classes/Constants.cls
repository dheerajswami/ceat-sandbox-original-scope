public with sharing class Constants {

    public static final String buReplacement                        = 'Replacement';
    public static final String buSpecialty                          = 'Specialty';
    public static final String buExports                            = 'Exports';
    public static final String buOE                                 = 'OE';

    public static final String replacementRoleTL                    = 'TL - Replacement';
    public static final String replacementRoleCSTL                  = 'CSTL';
    public static final String replacementRoleTLD                   = 'TLD';

    public static final String pjpNormsRecordTypeReplacement        = 'Replacement';
    public static final String pjpNormsRecordTypeOE                 = 'OE';
    
    public static final String specialtyRoleGM                      = 'GM Specialty';  
    public static final String specialtyRoleAM                      = 'AM Specialty';
    public static final String specialtyRoleRM                      = 'RM Specialty';
    public static final String specialtyRoleSRM                     = 'SRM Specialty';
    public static final String specialtyRoleVP                      = 'VP Specialty';

    //Added by Sneha
    // public static final String pjpReplacementPage                  = 'MonthlyVisitsPlan_v2';

        /* Added by Neha. PJP module went through enhancement for allowing filter for Customers. Hence the page's version has been updated
        *   CR No - 985(RSM PJP planning)
        */
     public static final String pjpReplacementPage                  = 'MonthlyVisitsPlan_v3';
     public static final String pjpReplacementTLDPage               = 'PJP_TLD';
     public static final String pjpSpecialtyPage                    = 'PJP_Specialty';
     public static final String pjpOEPage                           = 'PJP_OE';
     public static final String pjpExportsPage                      = 'PJP_Exports';
     
     public static final String RMOERole                            = 'RM_OE';
     public static final String TLOERole                            = 'TL_OE';
     
     public static final String OEProfile                           = 'OE';
     public static final Integer convertTolakhs                     = 100000;
     
     // Used in Dashboard
     
     public static final String rmDashboardPage                     = 'DashboardOutputRMPage';
     public static final String tlDashboardPage                     = 'DashboardInputOutputTLPage';
     public static final String cstlDashboardPage                   = 'DashboardInputCSTLPage';
     public static final String zgmDashboardPage                   = 'DashboardZGMPage';
     //public static final String tlInputDashboardPage                = 'DashboardInputTLPage';
     //public static final String tldInputDashboardPage               = 'DashboardInputTLPage';
     
     public static final String StockPage                         = 'StockVisibilityPage';
    
        
     
}