public with sharing class SP_HandlerFor_SP_Export_Controller{

//Wrapper classes
    public class DealerWrapper{
        public String dlName {get;set;}
        public String link {get;set;}
        public Double plannedTargetRunning {get;set;}
        public Double plannedTargetValue {get;set;}
        public String dlNumber {get;set;}
        public String countryCode{get;set;}
        //public String dlCountryCode {get;set;}
        public boolean lockedTL{get;set;}
        public List<categoryWrapper> catList{get;set;}
    }    
     public class categoryWrapper implements Comparable{
        public String catName{get;set;}
        public String catCode{get;set;}        
        public String catTarget{get;set;}
        public String catRunning{get;set;}
        
        public String catGap{get;set;}
        public String catASP{get;set;}
        public String budget{get;set;}
        public String Lycm{get;set;}
        public String L3M{get;set;}
        public String planned{get;set;}
        public String dlName{get;set;}
        public String spDLID{get;set;}
        public String clusterCode{get;set;}
        public Decimal columnOrder{get;set;}
        
        public Boolean saveit{get;set;}     
        public Integer compareTo(Object compareTo) {
            categoryWrapper compareToCat = (categoryWrapper)compareTo;
            if (columnOrder == compareToCat.columnOrder) return 0;
            if (columnOrder > compareToCat.columnOrder) return 1;
            return -1;        
        }  
    }
     public List<DealerWrapper> getSrManagerWiseSalesPlanningRecords(String loggedInUserCluster,String loggedInUserId){
        
        String managerLabel = System.Label.ExportSrManager; 
         
        String month;
        String year;
        Integer day;    
        Integer fromDay;
        Integer toDay; 
        Integer unlFromDay;
        Integer unlToDay;    
        
        Integer totalPlannedAllDL   = 0;
        Integer totalPlannedAllDL1  = 0;
        Integer totalGapAllDL       = 0;
        Integer totalTargetGap      = 0;
        Integer totalRunningGap     = 0;
        Integer plan                = 0;
        Integer makeGapzero         = 0;
        
        List<UserTerritory2Association> territoryForExportManager   = new List<UserTerritory2Association>();
        String exportDealer             = System.Label.Export_Dealer; 
        String exportForcast            = System.Label.Export_Forcast_BU;
        String recTypeExportForecast    = System.Label.Export_Forcast;
        String AcwLockedRecTypeLabel                     = System.Label.Locking;
        String AcwUnlockedRecTypeLabel                   = System.Label.Unlocking;
        String tlPageName                                = System.Label.ExportPage;
        String clustermaninterr                          = system.Label.terrSrManagerExport;
        
        Double runningTotal                              = 0;
        Decimal totalVal                                 = 0;
        String dlsTemp;
        Server_Url__c serverUrl;
        
        Map<String,List<Sales_Planning__c>> mapOfDLSales  = new map<String,List<Sales_Planning__c>>();       
        Map<String,Sales_Planning__c> mapOfDLSP           = new Map<String,Sales_Planning__c>();
        Map<String,Integer> mapCatWithTotalL3M            = new map<String,Integer>();
        Map<String,Id> mapDLNameId                        =new Map<String,Id>();
        Map<String,Integer> mapOfDLPlannedTotal           = new map<String,Integer>();
        Map<String,Double> mapOfPlannedTargetRunning      = new map<String,Double>();
        Map<String,Double> mapOfPlannedValue             = new map<String,Double>();
         
        ACW__c AcwLockedRecord                            = new ACW__c();  
        ACW__c AcwUnlockedRecord                          = new ACW__c();  
        List<Locking_Screen__c> alreadyLocked             = new list<Locking_Screen__c>(); 
        List<Locking_Screen__c> alreadyLocked1             = new list<Locking_Screen__c>();
        List<Locking_Screen__c> alreadyLocked2             = new list<Locking_Screen__c>();  
        
        list<Account> accountRestDealers = new list<Account>();       
        List<Territory2> countryUnderCluster  =new List<Territory2>();
        list<Account> listOFAccount = new list<Account>();
        List<String> parentRegIdList                      =new List<String>();
        List<String> parentRegCodeList                    =new List<String>();         
        List<Territory2> tlTerritory                      = new List<Territory2>();
        List<Territory2> rmTerritory                      = new List<Territory2>(); 
        List<UserTerritory2Association> clusterForExportManager = new List<UserTerritory2Association>();
        List<UserTerritory2Association> regionForRM       = new List<UserTerritory2Association>();
        list<Sales_Planning__c > salesPlanningExportForcase = new list<Sales_Planning__c>();
        Sales_Planning__c salesPlanTemp ;
        Sales_Planning__c salesPlanningDL ;
        Sales_Planning__c salesPlanningDL1 ;
        Sales_Planning__c salesPlanningDLRec;
        Sales_Planning__c salesPlanningDLTemp;
        List<Sales_Planning__c> salesPlan;
        List<Sales_Planning__c> existingSalesPlanningDLExport = new list<Sales_Planning__c>();  
        list<Account> accountProspect                      = new list<Account>();   
        Map<String,Integer> mapOfSDS ;
        Map<String,Integer> mapOfSpecialtyVal ;
        Map<String,Integer> mapOfExportCatTarget             = new map<String,Integer>();
        Map<String,Integer> mapOfExportForCatASP                = new map<String,Integer>();
        Map<String,String> mapOfDLAndNum ;
        Map<String,String> mapOfCatCodeAndCatName        = new map<String,String>();
        Map<String,String> mapOfDLAndSysTL               = new Map<String,String>();
        Map<String,String> mapOfDLNameAndRegCode         = new Map<String,String>();
        Map<String,Integer> mapOfTLPlannedTotal          = new map<String,Integer>();
        Map<String,Integer> mapOfDLGapTotal              = new map<String,Integer>();
        Map<String,Sales_Planning__c> mapOfCatAdDL       = new Map<String,Sales_Planning__c> ();
        Map<String,String> mapOfDLAndCluster             = new Map<String,String>();
        Map<String,String> mapOfDLAndCountry             = new Map<String,String>();
        Map<String,String> mapOfDLAndCountrydes          = new Map<String,String>();
        Map<String,String> mapOfProspectNumber =new Map<String,String>(); 
        Set<String> catExpSet                            = new set<String>();
        Set<Id> clusterId                                = new Set<Id>();
        Set<String> clusterCodeset                           = new Set<String>();
        Set<String> setOfDL                              =new Set<String>();        
        Set<String> countryCode                            = new set<String>();
        Id expoertdealerId           = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: exportDealer Limit 1].Id;
        Id exportForecastId       = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: recTypeExportForecast Limit 1].Id;
        //Id replaceTLId           = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
        Id acwLockedRecTypeId    = [select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =: AcwLockedRecTypeLabel Limit 1].Id;
        Id acwUnlockedRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =: AcwUnlockedRecTypeLabel Limit 1].Id;
        Id accountExportid       = [select id,DeveloperName from RecordType where SobjectType='Account' and DeveloperName = 'Exports' Limit 1].Id;
        Id accountProspectId     = [select id,DeveloperName from RecordType where SobjectType='Account' and DeveloperName =: system.label.Prospect_label Limit 1].Id;
        Date d                  = system.today();              
        month                   = String.valueOf(d.month());
        year                    = String.valueOf(d.Year());
        day                     = d.day();
        
        try{
            AcwLockedRecord=[Select Page__c,Sales_Planning__c,From_Date__c,To_Date__c,RecordTypeId from ACW__c where RecordTypeId=:acwLockedRecTypeId  AND Page__c=:tlPageName AND Sales_Planning__c=true AND Month__c =:Integer.valueof(d.month()) AND Year__c=:String.valueof(d.year()) limit 1];
            if(AcwLockedRecord!=null){
                date fromDate = AcwLockedRecord.From_Date__c;
                fromDay = fromDate.day();
                date toDate = AcwLockedRecord.To_Date__c;
                toDay = toDate.day();
            }
           AcwUnlockedRecord=[Select Page__c,User__c,Sales_Planning__c,Month__c,Year__c,From_Date__c,To_Date__c,RecordTypeId from ACW__c where RecordTypeId=:acwUnlockedRecTypeId AND Page__c=:tlPageName AND Sales_Planning__c=true AND user__c=:loggedInUserId AND (From_Date__c <= TODAY OR To_Date__c >= TODAY) AND Month__c=:d.month() AND Year__c=:String.valueOf(d.year()) limit 1];
           system.debug('---AcwUnlockedRecord-------------'+AcwUnlockedRecord);
           if(AcwUnlockedRecord!=null){
            date unFromDate = AcwUnlockedRecord.From_Date__c;
            unlFromDay = unFromDate.day();
            date unToDate = AcwUnlockedRecord.To_Date__c;
            unlToDay = unToDate.day();
           }
        }catch(Exception e){}     
        
        
       
        List<UserRole> userExportManagerRol  = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName  =: managerLabel];
        system.debug(userExportManagerRol+'userExportManagerRol');
        //List<UserRole> userTLRol1 = [SELECT DeveloperName,Id FROM UserRole WHERE  DeveloperName =: rmLabel ];        
        //if(userExportManagerRol.size() > 0 && UserInfo.getUserRoleId() == userExportManagerRol[0].Id ){ 
          
         //Get territory ids and territory codes of logged in sr manager
         system.debug(loggedInUserId+'loggedInUserId');
        clusterForExportManager = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:loggedInUserId and RoleInTerritory2=: clustermaninterr ];
         for(UserTerritory2Association reg : clusterForExportManager){
                clusterId.add(reg.Territory2Id);
                clusterCodeset.add(reg.Territory2.name);                
         }           
        system.debug(clusterCodeset+'clusterCodeset');
        countryUnderCluster = [SELECT id,Name,DeveloperName,ParentTerritory2Id from Territory2 WHERE ParentTerritory2Id IN: clusterId]; 
        for(Territory2 country : countryUnderCluster){
            countryCode.add(country.DeveloperName);
        }
       // }
        system.debug(clusterId+'clusterId');
        system.debug(clusterForExportManager +'clusterForExportManager ');
        system.debug(clusterCodeset+'clusterCodeset');
        system.debug(countryCode.size()+'countryCode');
        system.debug(countryCode+'countryCode');
        system.debug(countryUnderCluster+'countryUnderCluster');
         //Getting all categories;
        list<Export_Sales_Planning_Category__c> exportCat = Export_Sales_Planning_Category__c.getall().values();
            for(Export_Sales_Planning_Category__c sp:exportCat){
              if(sp.Include_in_Sales_Planning__c == true){
                catExpSet.add(sp.Category_Code__c);
                mapOfCatCodeAndCatName.put(sp.Category_Code__c,sp.Name);  
              }      
        }
        
        salesPlanningExportForcase = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Target_Quantity__c,Year__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c FROM Sales_Planning__c WHERE  Cluster_Code__c IN : clusterCodeset AND Year__c =:year AND Month__c =:month AND RecordTypeId =: exportForecastId AND Category__c In :catExpSet];
    
        if(salesPlanningExportForcase.size() > 0){
                for(Sales_Planning__c temp :salesPlanningExportForcase ){                
                    mapOfExportForCatASP.put(temp.Category__c,Integer.valueOf(temp.ASP__c));
                    mapOfExportCatTarget.put(temp.Category__c,Integer.valueOf(temp.Target_Quantity__c));
                    if(Integer.valueOf(temp.ASP__c) == null){
                        temp.ASP__c = 0;
                    }
                    if(Integer.valueOf(temp.Target_Quantity__c) == null){
                        temp.Target_Quantity__c = 0;
                    }
                    mapOfPlannedValue.put(temp.Category__c,decimal.ValueOf((Integer.valueOf(temp.ASP__c)*Integer.valueOf(temp.Target_Quantity__c))));
                }
        }
        system.debug(salesPlanningExportForcase.size()+'salesPlanningExportForcase');
        
        alreadyLocked = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Approved'];
        alreadyLocked1 = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Submitted'];
        alreadyLocked2 = [SELECT id,name,status__c,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:loggedInUserId AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND status__c='Rejected'];
        //system.debug('---------alreadyLocked'+alreadyLocked); 
        
        existingSalesPlanningDLExport = [SELECT id,Dealer_CustNumber__c,Dealer_Name__c,Parent_Sales_Planning__c ,Category__c,Category_Description__c ,Month__c ,Year__c  ,RecordTypeId  ,SPExternalIDTL__c,Total_planned__c,LYCM__c,L3M__c,Total_LYCM__c,Total_L3M__c,Budget__c,Cluster_Code__c,Cluster_Desc__c,Country_Code__c,Country_Desc__c FROM Sales_Planning__c WHERE Country_Code__c IN: countryCode AND Year__c =:year AND Month__c =:month AND  RecordTypeId =: expoertdealerId AND Category__c In :catExpSet AND Total_planned__c >=0];
        
        system.debug(existingSalesPlanningDLExport.size()+'existingSalesPlanningDLExport');
         if(existingSalesPlanningDLExport .size() > 0){
            mapOfDLSP = new map<String,Sales_Planning__c>(); 
            mapOfDLAndNum = new map<String,String>();
            setOfDL = new Set<String>();
            String key;
            for(Sales_Planning__c temp: existingSalesPlanningDLExport ){ 
                   
                key=temp.Dealer_CustNumber__c +temp.Country_Code__c+ temp.Category__c;                  
                mapOfDLSP.put(key,temp);
                setOfDL.add(temp.Dealer_Name__c+temp.Country_Code__c);
                mapOfDLAndNum.put(temp.Dealer_Name__c+temp.Country_Code__c,temp.Dealer_CustNumber__c);
                mapOfDLAndCluster.put(temp.Dealer_Name__c,temp.Cluster_Code__c);
                mapOfDLAndCountry.put(temp.Dealer_Name__c+temp.Country_Code__c,temp.Country_Code__c );
                mapOfDLAndCountrydes.put(temp.Dealer_Name__c+temp.Country_Code__c,temp.Country_Desc__c);
                //system.debug('------------------------------temp.Dealer__r.Specialty_Value__c'+temp.Dealer__r.Specialty_Value__c);
                //mapOfDLAndSysTL.put(temp.Dealer__r.Name,temp.Territory_Code__c); 
                //mapOfDLNameAndRegCode.put(temp.Dealer__r.Name,temp.Region_Code__c);            
            }
        }
        System.debug(mapOfDLSP+'mapOfDLSP');
        listOFAccount = [SELECT ID,Name,KUNNR__c,Prospect_No__c,Type,LAND1_GP__c,Country__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,SDS__c,Last_Month_SDS__c,Specialty_Value__c,Ship_to_Country__c FROM Account WHERE Ship_to_Country__c != '' AND  RecordTypeId =: accountExportid AND KUNNR__c NOT IN : mapOfDLAndNum.values()];
        if(listOFAccount.size() > 0){
            for(Account acc : listOFAccount){
               String[] country = acc.Ship_to_Country__c.split(';');
                if(country.size() > 0){
                    for(String str : country){
                        if(countryCode.contains(str)){
                            accountRestDealers.add(acc);  
                        }
                    }
                }
            }    
        }
        accountProspect = [SELECT ID,Name,LAND1_GP__c,Prospect_No__c,Type,Country__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,SDS__c,Last_Month_SDS__c,Specialty_Value__c From account Where RecordTypeId =: accountProspectId AND Business_Units__c = 'Exports' AND Type = 'Prospect' And Active__c=true AND  LAND1_GP__c IN: countryCode];
        if(accountProspect.size() > 0){
              
            for(Account acc : accountProspect){
                setOfDL.add((acc.name).trim()+':'+(acc.Country__c).trim());
                
                
                if(acc.Last_Month_SDS__c== null) acc.Last_Month_SDS__c= 0;
                if(acc.Prospect_No__c == null) acc.Prospect_No__c = '0';
                mapOfProspectNumber.put(acc.name+':'+acc.id,string.valueOf(acc.Prospect_No__c));
                
                mapOfDLAndCountrydes.put(acc.Name+':'+acc.Country__c,acc.Country__c);
            
            }
        }
        if(accountRestDealers.size() > 0){
                 
            for(Account acc : accountRestDealers){
                String[] setCountry = acc.Ship_to_Country__c.split(';');
                String countryname = '';
                if(setCountry.size() > 1){
                    
                    for(String str : setCountry){
                        if(countryCode.contains(str)){
                            if(countryname == ''){
                                countryname = str;
                            }else{
                                countryname = countryname +':'+ str;
                            }
                        }
                    }
                }else{
                    countryname = acc.Ship_to_Country__c;
                }
                setOfDL.add((acc.name).trim()+':'+(countryname).trim());
                //mapOFAccAndRelatedCountry.put(acc.KUNNR__c,countryname);
                if(acc.KUNNR__c != null) {
                    mapOfDLAndNum.put(acc.Name+':'+countryname,string.valueOf(acc.KUNNR__c));
                }
                
                if(acc.Last_Month_SDS__c== null) acc.Last_Month_SDS__c= 0;
                if(acc.Prospect_No__c == null) acc.Prospect_No__c = '0';
                mapOfDLAndCountry.put(acc.Name+':'+countryname,countryname);
                mapOfDLAndCountrydes.put(acc.Name+':'+countryname,countryname);
               
            }
        }
        //Calculating planned target running values
       if(mapOfDLSP.size() > 0){
                 for (String key : mapOfDLSP.keySet()) {
                        salesPlanTemp = new Sales_Planning__c();
                        salesPlanTemp = mapOfDLSP.get(key);
                        dlsTemp= salesPlanTemp.Dealer_Name__c+salesPlanTemp.Country_Code__c;        
                        
                        if(mapOfPlannedTargetRunning.containsKey(dlsTemp)){
                            runningTotal=mapOfPlannedTargetRunning.get(dlsTemp);  
                            if(salesPlanTemp.Total_planned__c == null) salesPlanTemp.Total_planned__c = 0;
                            //runningTotal += salesPlanTemp.Total_planned__c;
                            if(mapOfExportForCatASP.get(salesPlanTemp.category__c) != null){
                                runningTotal += salesPlanTemp.Total_planned__c * mapOfExportForCatASP.get(salesPlanTemp.category__c);
                            }
                            
                           // system.debug('---salesPlanTemp.Total_planned__c'+salesPlanTemp.Total_planned__c);
                            //system.debug('---mapOfTLCatASP.get(salesPlanTemp.category__c)'+mapOfTLCatASP.get(salesPlanTemp.category__c));
                            mapOfPlannedTargetRunning.put(dlsTemp,runningTotal);
                            runningTotal=0;
                       }
                       else{      
                            if(salesPlanTemp.Total_planned__c == null) salesPlanTemp.Total_planned__c = 0;
                            if(mapOfExportForCatASP.get(salesPlanTemp.category__c) != null){                       
                                runningTotal += salesPlanTemp.Total_planned__c* mapOfExportForCatASP.get(salesPlanTemp.category__c);
                            }
                            mapOfPlannedTargetRunning.put(dlsTemp,runningTotal);                    
                       }                    
                  }  
       }       
       
        //calculating Running total for each category for each DL under that category
      if(mapOfDLSP.size() > 0){
            for(String key : mapOfDLSP.keySet()){
              
              totalPlannedAllDL = 0;
                totalPlannedAllDL1 = 0;
                
                salesPlanningDL1 = new Sales_Planning__c();
                salesPlanningDL1 = mapOfDLSP.get(key);
                
                if(mapOfDLPlannedTotal.containsKey(salesPlanningDL1.Category__c)){
                    totalPlannedAllDL1 = mapOfDLPlannedTotal.get(salesPlanningDL1.Category__c);
                    totalPlannedAllDL1 += Integer.ValueOf(salesPlanningDL1.Total_planned__c);
                    mapOfDLPlannedTotal.put(salesPlanningDL1.Category__c,totalPlannedAllDL1);
                    totalPlannedAllDL1 = 0;
                }
                else{
                    if(salesPlanningDL1.Total_planned__c != null){
                        totalPlannedAllDL += Integer.ValueOf(salesPlanningDL1.Total_planned__c);
                        mapOfDLPlannedTotal.put(salesPlanningDL1.Category__c,totalPlannedAllDL);
                    }
                }                      
            }  
            //system.debug('------------------mapOfDLPlannedTotal'+mapOfDLPlannedTotal);                                    
        } 
        system.debug('------------------mapOfDLPlannedTotal'+mapOfDLPlannedTotal);
         
        //Calculate Gap for each category from each DL under that category
        if(mapOfDLSP.size() > 0){
          for(String key : mapOfExportCatTarget.keySet()){
            totalGapAllDL = 0;
                totalTargetGap = 0;
                totalRunningGap = 0;               
                
                if(mapOfExportCatTarget.get(key) != null && mapOfDLPlannedTotal.get(key) != null){
                  totalTargetGap =mapOfExportCatTarget.get(key) ;
                  totalRunningGap =mapOfDLPlannedTotal.get(key) ;
                  totalGapAllDL = totalTargetGap - totalRunningGap ;
                  
                }else if(mapOfExportCatTarget.get(key)  != null && mapOfDLPlannedTotal.get(key)  == null){                
                  totalTargetGap = mapOfExportCatTarget.get(key) ;
                  totalRunningGap = 0;
                  totalGapAllDL = totalTargetGap - totalRunningGap ;
                }
                mapOfDLGapTotal.put(key,totalGapAllDL);                
          }               
        } 
        system.debug('------------------mapOfDLGapTotal'+mapOfDLGapTotal); 
        system.debug('------------------mapOfDLAndNum'+mapOfDLAndNum); 
        system.debug(setOfDL+'setOfDL');
        List<String> dlList=new List<String>();
        dlList.addAll(setOfDL);
        dlList.sort();        
       //system.debug('dlList---'+dlList);       
        List<DealerWrapper> dlWiseRecord      = new List<DealerWrapper>();  
        String dd=''; 
        for(String key : mapOfPlannedValue.keySet()){
           
            totalVal += mapOfPlannedValue.get(key);
        }
        serverUrl=[select url__c from Server_Url__c limit 1]; 
        for(String dls : dlList){
            DealerWrapper c       =   new DealerWrapper();            
            c.dlName              =   dls ;            
            c.catList             =   new List<categoryWrapper>();
            c.dlNumber            =   mapOfDLAndNum.get(dls);  
            c.countryCode          = mapOfDLAndCountrydes.get(dls);
            if(mapOfPlannedTargetRunning.get(dls)!=null){
               
            c.plannedTargetRunning=mapOfPlannedTargetRunning.get(dls);
            }else{
              c.plannedTargetRunning        =   0;
            }
            c.plannedTargetValue  = totalVal ;
            
             if(UserInfo.getUserId() != loggedInUserId){
                if(alreadyLocked1.size() > 0){
                    c.link =serverUrl.url__c+'/'+alreadyLocked1[0].id;
                }else if(alreadyLocked.size() > 0){
                    c.link = serverUrl.url__c+'/'+alreadyLocked[0].id;
                }else if(alreadyLocked2.size() > 0){
                    c.link = serverUrl.url__c+'/'+alreadyLocked2[0].id;
                }
            }
            else{
                    c.link = '';
                }
            
            
             if(UserInfo.getUserId()== UserInfo.getUserId())  {
                
                if(alreadyLocked1.size() > 0){ 
                     //c.lockedTL          =    true;
                    
                    if(unlFromDay!=null && unlToDay!=null && day >= unlFromDay && day <= unlToDay){
                         c.lockedTL          =    false;
                    }else{
                        c.lockedTL          =    alreadyLocked1[0].Submitted__c;
                    }
                }
                
                else{
                    if(day >= fromDay && day <= toDay){  
                        //system.debug('------Inside-----else if----------day'+day);
                        c.lockedTL          =    false;
                    }else if(unlFromDay!=null && unlToDay!=null && day >= unlFromDay && day <= unlToDay){
                            c.lockedTL          =    false;
                    }else{
                              c.lockedTL          =    true;
                    }               
                }
                
                if(alreadyLocked2.size() > 0 && day >= fromDay && day <= toDay){
                    c.lockedTL          =    false;
                }
                if(alreadyLocked.size() > 0 ){
                    c.lockedTL          =    true;
                }
            }
            else{
                c.lockedTL          =    true;
            }
            //system.debug('------------------mapOfDLAndNum'+mapOfDLAndNum); 
            for(String catname:    catExpSet ){              
                      
                     categoryWrapper cc = new categoryWrapper();
                     
            if(mapOfExportCatTarget.containsKey(catname) && mapOfExportCatTarget.get(catname) > 0){
                     
                     cc.saveit    = false;
                     cc.catName   = mapOfCatCodeAndCatName.get(catname);
                     cc.columnOrder          = (Export_Sales_Planning_Category__c.getValues(mapOfCatCodeAndCatName.get(catname))).Sort_Order__c;
                     cc.catCode   = catname; 
                     cc.dlName    = dls;
                     cc.catTarget = String.valueOf(mapOfExportCatTarget.get(catname));
                     //cc.clusterCode  = mapOfDLAndCluster.get(dls);
                     cc.clusterCode  = loggedInUserCluster;
                     cc.catASP    = String.valueOf(mapOfExportForCatASP.get(catname));
                     if(cc.catTarget==null){
                        cc.catTarget=String.valueOf(0);
                     }
                     if(mapOfDLPlannedTotal.get(catname) != null && mapOfDLGapTotal.get(catname) == 0){
                       cc.catRunning         = String.valueOf(mapOfDLPlannedTotal.get(catname)); 
                       cc.catGap           = String.valueOf(mapOfDLGapTotal.get(catname));                       
                 }else if(mapOfDLPlannedTotal.get(catname) == null){
                      cc.catRunning         = String.valueOf(0);
                      cc.catGap             = String.valueOf(mapOfDLGapTotal.get(catname));
                 }
                 /*
                  if(mapOfDLGapTotal.get(catname) == 0){
                       cc.catGap           = String.valueOf(mapOfDLGapTotal.get(catname)); 
                 } 
                 */  
                 system.debug(mapOfDLSP+'mapOfDLSP');
                 system.debug(dls+'!!!!!');   
                 system.debug(mapOfDLAndNum.get(dls)+'@@@@');   
                 system.debug(mapOfDLAndCountry.get(dls)+'%%%%'+catname+'*****'); 
                 
                 system.debug('------------------mapOfDLAndNum'+mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname)); 
                 system.debug('------------------Cat'+mapOfDLAndNum.get(dls) + catname+dls);   
                 system.debug('#####'+mapOfDLAndCountry.get(dls) + catname+dls);      
                 if(mapOfDLSP.size() > 0 && mapOfDLSP != null && mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname) != null){
                         
                         if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Budget__c != null){                            
                             cc.budget = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Budget__c);                             
                         }else{
                             cc.budget = String.valueOf(0);                             
                         }
                          if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).LYCM__c) != null){                           
                             cc.Lycm = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).LYCM__c);                              
                         }else{
                             cc.Lycm = String.valueOf(0);
                         }
                         if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).L3M__c) != null){
                             cc.L3M = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).L3M__c); 
                         }else{
                             cc.L3M = String.valueOf(0);
                         }                        
                         if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c) != null){
                                system.debug(catname+'XXXX'+mapOfDLGapTotal.get(catname)+'vvvvvv');
                                system.debug('------------------mapOfDLAndNum'+mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c); 
                                system.debug('------------------Cat'+mapOfDLAndNum.get(dls) + catname);
                                 plan = 0;
                                makeGapzero = 0;
                                 
                                 if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) > 0){
                                     system.debug('---------------------------------------------------ifplanned > 0');
                                     plan = Integer.ValueOF (mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c + mapOfDLGapTotal.get(catname));
                                     cc.planned = String.valueOf(plan);
                                     
                                     makeGapzero = mapOfDLGapTotal.get(catname);
                                     makeGapzero = 0;
                                     mapOfDLGapTotal.put(catname,makeGapzero);
                                     mapOfDLPlannedTotal.put(catname,mapOfExportCatTarget.get(catname));
                                     cc.catRunning         = String.valueOf(mapOfExportCatTarget.get(catname)); 
                                   cc.catGap           = String.valueOf(mapOfDLGapTotal.get(catname));  
                                   cc.saveit = true;
                                   
                                 }else if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) < 0){
                                       system.debug('--------------------------------------------------mapOfDLGapTotal < 0'+mapOfDLGapTotal.get(catname));
                                       
                                     if(Integer.ValueOF (mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c)>=(0 - mapOfDLGapTotal.get(catname)) && Integer.ValueOF (mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c)!=0){
                                        plan = Integer.ValueOF (mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c )+ mapOfDLGapTotal.get(catname);
                                        //+ mapOfDLGapTotal.get(catname));
                                        cc.planned = String.valueOf(plan);
                                        makeGapzero = mapOfDLGapTotal.get(catname);
                                         makeGapzero = 0;
                                         mapOfDLGapTotal.put(catname,makeGapzero);
                                         mapOfDLPlannedTotal.put(catname,mapOfExportCatTarget.get(catname));
                                         cc.catRunning         = String.valueOf((mapOfDLPlannedTotal.get(catname)+mapOfDLGapTotal.get(catname))); 
                                         dd+=(mapOfDLGapTotal.get(catname)+'%%%%%'+catname+'----'+mapOfExportCatTarget.get(catname)+'######');
                                        cc.catGap           = String.valueOf(mapOfDLGapTotal.get(catname));  
                                        cc.saveit = true;
                                        
                                    }else{
                                         cc.planned = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c); 
                                    }
                                 }
                                 else{
                                   
                                   cc.planned = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).Total_planned__c); 
                                 }
                         }else{
                                     cc.planned = String.valueOf(0);
                         }
                         if((mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).SPExternalIDTL__c) != null){
                             cc.spDLID = String.valueOf(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname).SPExternalIDTL__c);                              
                         }else{
                             cc.spDLID = String.valueOf(0);
                         } 
                     }
                     else if(mapOfDLSP.get(mapOfDLAndNum.get(dls)+mapOfDLAndCountry.get(dls)+catname) == null){                         
                         cc.budget = String.valueOf(0);
                         cc.L3M = String.valueOf(0);
                         cc.Lycm = String.valueOf(0);
                         system.debug(catname+'XXXX'+mapOfDLGapTotal.get(catname)+'vvvvvv');
                         if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) > 0){
                                  system.debug('ifffffffffff');
                                   cc.planned = String.valueOf(mapOfDLGapTotal.get(catname));   
                                   system.debug(cc.planned+'cc.planned');   
                                   system.debug(dls+'dls');                                
                                   makeGapzero = mapOfDLGapTotal.get(catname);
                                   makeGapzero = 0;
                                   mapOfDLGapTotal.put(catname,makeGapzero);
                                    
                                   cc.catGap           = String.valueOf(0);
                                   cc.catRunning         = String.valueOf(mapOfExportCatTarget.get(catname));
                                   system.debug(cc.catGap +'cc.catGap'); 
                                   system.debug(cc.catRunning +'cc.catRunning'); 
                                   cc.saveit = true;
                        } else if(mapOfDLGapTotal.get(catname) != null && mapOfDLGapTotal.get(catname) < 0){
                            
                                   cc.planned = String.valueOf(0);
                                   cc.catGap           = String.valueOf(0);
                                   cc.catRunning         = String.valueOf(mapOfExportCatTarget.get(catname));
                                   
                                   cc.saveit = true;
                        }
                        else{
                                   //system.debug('-------------------- In side elseeeeee');
                                   cc.planned = String.valueOf(0);
                        }
                     }
                     c.catList.add(cc);
                     c.catList.sort();
                }
        }
        dlWiseRecord.add(c);
       }
        system.debug('-------------dd'+dd);
        return dlWiseRecord;
        }
        }