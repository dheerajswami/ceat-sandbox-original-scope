/*******************************************************************************
Created By : Sneha Agrawal
Date       : 14th July, 2015
Purpose    : Test class for Efleet_HomeController_Specialty
********************************************************************************/
@isTest
public class EfleetController_Specialty_TestClass{
    
    static testmethod void testEfleetSpecialty(){
        Account acc1 = CEAT_InitializeTestData.createAccount('Acc1', '5000234509');
        //acc1.Fleet_Type__c = '';
        insert acc1;
        Account acc2 = CEAT_InitializeTestData.createAccount('Acc2', '5000234508');
        
        Contact con = CEAT_InitializeTestData.createContact('Con', acc1.Id);
        insert con;
        
        Vehicle__c veh = CEAT_InitializeTestData.createVehicle('On Road', 'Normal Load', 'Truck', '14', acc1.Id); 
        veh.Wheel_Type__c = '4W';
        insert veh;
        Tyre__c newTyre = CEAT_InitializeTestData.createTyre('CEAT', 0, 'HCL', 'Front', '10.00-20', '1', veh.Id);            
        
        Efleet_HomeController_Specialty.fetchAllFleets();
        Efleet_HomeController_Specialty.fetchAllDealers();
        Efleet_HomeController_Specialty.fetchCustomerTypes();
        
        
        Efleet_HomeController_Specialty.sortListAndOthers(new List<String>{'Test'});
        Efleet_HomeController_Specialty.fetchVehicleMaker_Type();
        Efleet_HomeController_Specialty.fetchVehicleMaker_Maker('Other');
        Efleet_HomeController_Specialty.fetchVehicleMaker_TyreSize('Other', 'Other');
        Efleet_HomeController_Specialty.fetchSelectedVehicleType(acc1.Id);        
        Efleet_HomeController_Specialty.saveVehicleType(acc1.Id, new Fleet_Vehicle_Type_Specialty__c(Transporter_Name__c = acc1.Id, Size_of_Wallet_Per_Month__c = 10));
        Efleet_HomeController_Specialty.fetchVehicleTypeSummary(acc1.Id);
        
        Efleet_HomeController_Specialty.fetchVehicleTypes('OTR');
        Efleet_HomeController_Specialty.fetchVehicleTypes('Farm');
        
        
        Efleet_HomeController_Specialty.fetchTyreBrands('OTR');  
        Efleet_HomeController_Specialty.fetchTyreBrands('Farm');  
        Efleet_HomeController_Specialty.fetchCeatSubBrands('OTR');  
        Efleet_HomeController_Specialty.fetchCeatSubBrands('Farm');  
        
        EFleet_Specialty_Business_Info__c sf = new EFleet_Specialty_Business_Info__c();
        sf.Business_Application__c = 'Test'; 
        sf.OTR_Farm__c = 'Test'; 
        sf.Size_of_Business_Parameter__c = 'Test'; 
        sf.Size_of_Business_Range__c = 'Test'; 
        sf.Tyre_Life__c = '2' ;
        sf.Name = 'Test' ;
        insert sf;  
        Efleet_HomeController_Specialty.fetchSizeOfBusiness('Test');
        
        State_Master__c stateMaster = CEAT_InitializeTestData.createStateMaster('Karnataka', 'Bangalore', 'Koramangala');
        insert stateMaster;
        
        Efleet_HomeController_Specialty.fetchStates();          
        Efleet_HomeController_Specialty.fetchDistricts('Karnataka');
        Efleet_HomeController_Specialty.fetchTowns('Bangalore');
        
        Efleet_HomeController_Specialty.fetchSelectedFleet(acc1.Id);
        Fleet_Crop_Details__c fp = new Fleet_Crop_Details__c();
        fp.Transporter_Name__c = acc1.id;            
        Efleet_HomeController_Specialty.fetchSelectedCrops(acc1.id);
        Efleet_HomeController_Specialty.fetchSelectedContact(acc1.id);
        
        Fleet_Vehicle_Information_Specialty__c fv = new Fleet_Vehicle_Information_Specialty__c();
        fv.Transporter_Name__c = acc1.id ;
        fv.Current_Tyre_Brand__c = 'CEAT' ;
        insert fv;
        
        list<Fleet_Vehicle_Information_Specialty__c> fvList = new list<Fleet_Vehicle_Information_Specialty__c>();
        fvList.add(fv);
        
        Fleet_Ceat_Vehicle_Specialty__c fc = new Fleet_Ceat_Vehicle_Specialty__c();
        fc.Vehicle__c = veh.id ;
        
        
        Efleet_HomeController_Specialty.fetchSelectedVehicleInfo(acc1.id);
        Efleet_HomeController_Specialty.fetchCeatRelationships(acc1.id);
        
        list<Fleet_Crop_Details__c> fpList = new list<Fleet_Crop_Details__c>();
        fpList.add(fp);
        Efleet_HomeController_Specialty.saveFleetDetails(acc1,fpList);
        Efleet_HomeController_Specialty.saveContactDetails(con,'30/12/1991','30/12/2014');
        
        Fleet_Vehicle_Information__c vehInfo = CEAT_InitializeTestData.createVehicleInfo(acc1.Id);
        insert vehInfo;
        List<Fleet_Vehicle_Information__c> vehInfoList = new List<Fleet_Vehicle_Information__c>();
        vehInfoList.add(vehInfo);
        Efleet_HomeController_Specialty.saveVehicleInfo(acc1.id,fvList,new list<string> {'1/1/2015'});
        
        Fleet_Ceat_Vehicle_Specialty__c fc1 = new Fleet_Ceat_Vehicle_Specialty__c();
        fc1.Vehicle__c = fv.id ;
        
        list<Fleet_Ceat_Vehicle_Specialty__c> fcList = new list<Fleet_Ceat_Vehicle_Specialty__c>();
        fcList.add(fc1);
        Efleet_HomeController_Specialty.saveCeatRelationships(fcList);   
        
        list<string> stList = new List<String>();
        stList.add('Others'); 
        stList.add('Other');
        //Efleet_HomeController_Specialty.sortListAndOthers(stList);
    }
    
}