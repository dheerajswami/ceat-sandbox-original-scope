@isTest
private class TH_CustomerStagingTriggerHandler_Test {

    //private static list<Customer_Staging__c> cusStag;
    

    static testMethod void toUpdateAccountwithCustomerNum() {
       
       Account acc          =   new Account();
        acc.Name            =   'Sample Account';
        acc.KUNNR__c        =   '1234568';
        //acc.Prospect_No__c    =   '345678';
        
        insert acc;
       
       
       list<Account> Acct = new list<Account>();
       
       Customer_Staging__c cusStag  =   new Customer_Staging__c();
       cusStag.Customer_Name__c     =   acc.Name;
       cusStag.Customer_Number__c   =   '1234568';
       cusStag.ProspectCode__c      =   '345678';
       cusStag.Division__c         = '05';
       cusStag.Customer_Group__c   = 'DS';
       insert cusStag;
      
    }
    
    static testMethod void toUpdateAccountwithProspectCode() {
       
       id prospectid = UtilityClass.getRecordTypeId('Account','Prospect');
       Account acc1                 =   new Account();
        acc1.Name                   =   'Sample Account one';
        //acc1.KUNNR__c               =   '12345689';
        acc1.Prospect_No__c         =   '3456789';
        acc1.UniqueIdentifier__c    =   '3456789';
        acc1.RecordTypeID           = prospectid;
        insert acc1;
       
       
       list<Account> Acct = new list<Account>();
       
       Customer_Staging__c cusStag1     =   new Customer_Staging__c();
       cusStag1.Customer_Name__c        =   acc1.Name;
       cusStag1.Customer_Number__c  =   '12345689';
       cusStag1.ProspectCode__c     =   '3456789';
       cusStag1.Division__c         = '05';
       cusStag1.Customer_Group__c   = 'SP';
       
       
       insert cusStag1;
    }
    
    
    static testMethod void toCreateAccountwithCustomerNum() {
       
       Account acc2                 =   new Account();
        acc2.Name                   =   'Sample Account one';
        acc2.KUNNR__c               =   '';
        acc2.Prospect_No__c         =   '';
        acc2.UniqueIdentifier__c    =   '';
        
        insert acc2;
       
       
       
       list<Account> Acct = new list<Account>();
       
       Customer_Staging__c cusStag2     =   new Customer_Staging__c();
       cusStag2.Customer_Name__c        =   acc2.Name;
       cusStag2.Customer_Number__c  =   '54646';
       //cusStag2.ProspectCode__c     =   '345789';
       cusStag2.Division__c         = '05';
       cusStag2.Customer_Group__c   = 'SP';
       
       insert cusStag2;
       cusStag2.Customer_Name__c = 'gfgfhj';
       update cusStag2;
    }
    
    static testMethod void toUpdateAccountwithProspectCod() {
       
       id prospectid = UtilityClass.getRecordTypeId('Account','Prospect');
       Account acc3                 =   new Account();
        acc3.Name                   =   'Sample Account two';
        //acc1.KUNNR__c               =   '12345689';
        acc3.Prospect_No__c         =   '345789';
        acc3.UniqueIdentifier__c    =   '345789';
        acc3.RecordTypeID           = prospectid;
        insert acc3;
       
       Account acc12                 =   new Account();
        acc12.Name                   =   'Sample Account';
        //acc1.KUNNR__c               =   '12345689';
        acc12.Prospect_No__c         =   '345785';
        acc12.UniqueIdentifier__c    =   '345785';
        acc12.RecordTypeID           = prospectid;
        insert acc12;
        
       list<Account> Acct = new list<Account>();
       list<Customer_Staging__c> AcctStagingList = new list<Customer_Staging__c>();
       
       Customer_Staging__c cusStag3     =   new Customer_Staging__c();
       cusStag3.Customer_Name__c        =   acc3.Name;
       cusStag3.Customer_Number__c  =   '12345689';
       cusStag3.ProspectCode__c     =   '345789';
       cusStag3.Division__c         =   '05';
       cusStag3.Customer_Group__c   =   'SP';
       
       Customer_Staging__c cusStag12    =   new Customer_Staging__c();
       cusStag12.Customer_Name__c        =   acc3.Name;
       cusStag12.Customer_Number__c  =   '345785';
       cusStag12.ProspectCode__c     =   '345785';
       cusStag12.Division__c         =  '05';
       cusStag12.Customer_Group__c   =  'SP';
       
       AcctStagingList.add(cusStag3);
       AcctStagingList.add(cusStag12);
       insert AcctStagingList;
       cusStag3.Customer_Number__c  =   '3457856';
       cusStag3.Division__c = '01';
       update cusStag3;
       
    }
    
    static testMethod void toCreateAccountwithCustomerNo() {
       
       Account acc4                 =   new Account();
        acc4.Name                   =   'Sample Account one';
        acc4.KUNNR__c               =   '';
        acc4.Prospect_No__c         =   '';
        acc4.UniqueIdentifier__c    =   '';
        
        insert acc4;
       
       
       
       list<Account> Acctt = new list<Account>();
       
       Customer_Staging__c cusStag4     =   new Customer_Staging__c();
       cusStag4.Customer_Name__c        =   acc4.Name;
       cusStag4.Customer_Number__c  =   '5446';
       //cusStag2.ProspectCode__c     =   '345789';
       cusStag4.Division__c         = '04';
       cusStag4.Customer_Group__c   = 'SP';
       
       insert cusStag4;
       cusStag4.Customer_Name__c = 'abcdf';
       update cusStag4;
    }
    
    
    static testMethod void deleteCustomerStaging() {
       
       Account acc5                 =   new Account();
        acc5.Name                   =   'Sample Account one';
        acc5.KUNNR__c               =   '';
        acc5.Prospect_No__c         =   '';
        acc5.UniqueIdentifier__c    =   '';
        
        insert acc5;
        
        acc5.Name                   =   'Same Account one';
        
        update acc5;
        
        
       
       
       list<Account> Acct = new list<Account>();
       
       Customer_Staging__c cusStag5     =   new Customer_Staging__c();
       cusStag5.Customer_Name__c        =   acc5.Name;
       cusStag5.Customer_Number__c  =   '54646';
       //cusStag2.ProspectCode__c     =   '345789';
       cusStag5.Division__c         = '05';
       cusStag5.Customer_Group__c   = 'SP';
       
       insert cusStag5;
       cusStag5.Customer_Name__c = 'ghdfhj';
       update cusStag5;
       delete cusStag5;
    }
    
}