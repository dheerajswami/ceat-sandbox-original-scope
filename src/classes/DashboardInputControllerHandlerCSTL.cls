/************************************************************************************************************
    Author  : Sneha Agrawal
    Date    : 22/5/2015
    Purpose : Dashboard Input Batch Class For CSTL
*************************************************************************************************************/

global class  DashboardInputControllerHandlerCSTL{
     
    //Wrapper Classes    
    public class dashboardWrapper{
        public set<string> moduleSet     {get;set;}    
        public set<string> catSet     {get;set;}
        public list<Dashboard_Input_Score__c> dbInputScore {get;set;}
        public Dashboard_Summary_Score__c dashboardSummaryScore {get;set;}
    }
    
    //CONSTRUCTOR
    public DashboardInputControllerHandlerCSTL(){ 
        
    }
    
    public  dashboardWrapper getDashboardCstlRecords(Id userId, string selectedMonth, string selectedYear){
         map<Integer,String> monthYearMap = new map<Integer,String>();
         /*
                To convert current month into month name
        */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');
            
        Integer month                   = Date.today().month();
        String currentMonth             = monthYearMap.get(month);
        String currentYear              = String.valueOf((Date.today()).year());
        
        string dealers                  = system.Label.CSTL_Dealers;    
        String distributors             = system.Label.CSTL_Distributors;
        string truckCustomer            = system.Label.Truck_Customer_Module; 
        string actionLog                = system.Label.CSTL_Action_Log;   
        
        string CSTLRecType              = system.Label.CSTL;    
        String cstlEfleetRecType        = system.Label.CSTL_Truck_Customer_RecType;
        string cstldealerRecType        = system.Label.CSTL_Dealer_RecType;   
        String currentDay               = String.valueOf((Date.today()).day());
        string CSTLInputRecType         = system.Label.CSTL_RecType;
        
        Set<String> SetOFCat        = new Set<String>();
        Set<String> SetOFCatEfleet  = new Set<String>();
        
        list<Dashboard_Input_Score__c> dashboardInputScore        = new List<Dashboard_Input_Score__c>(); 
        
        Dashboard_Summary_Score__c dashSummaryScore               = new Dashboard_Summary_Score__c();
        map<String,Double> MapOFCatAndWeigtage                    = new map<String,Double>();
      
        
        Id CSTLInputRecTypeId   = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Summary_Score__c' and DeveloperName =:CSTLInputRecType Limit 1].Id;
        Id CSTLRecTypeId        = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:CSTLRecType Limit 1].Id;
        if(Integer.valueof(selectedMonth) == month){    
            if(userId!=null){
                dashboardInputScore = [SELECT ID,Achievement__c,ScoreMTD__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: CSTLRecTypeId AND User__c=:userId  AND  Month__c =: monthYearMap.get(Integer.valueof(selectedMonth)) AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
                
                dashSummaryScore    = [SELECT ID,Input_Score__c,Territory_Code__c,Month__c,Year__c FROM Dashboard_Summary_Score__c WHERE RecordTypeId =: CSTLInputRecTypeId AND OwnerId=:userId  AND Month__c =: string.valueof(selectedMonth) AND Year__c =: selectedYear limit 1];
            }else{
                 dashboardInputScore = [SELECT ID,Achievement__c,ScoreMTD__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: CSTLRecTypeId  AND  Month__c =: selectedMonth AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Testing__c=true AND Dashboard_Weightage_Master__r.Weightage__c>0];
            }
        }else{
            if(userId!=null){
                dashboardInputScore = [SELECT ID,Achievement__c,ScoreMTD__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__r.Weightage__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: CSTLRecTypeId AND User__c=:userId  AND  Month__c =: monthYearMap.get(Integer.valueof(selectedMonth)) AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Weightage__c>0];
                
                dashSummaryScore    = [SELECT ID,Input_Score__c,Territory_Code__c,Month__c,Year__c FROM Dashboard_Summary_Score__c WHERE RecordTypeId =: CSTLInputRecTypeId AND OwnerId=:userId  AND Month__c =: string.valueof(selectedMonth) AND Year__c =: selectedYear limit 1];
            }else{
                 dashboardInputScore = [SELECT ID,Achievement__c,ScoreMTD__c,Dashboard_Weightage_Master__r.Weightage__c,RecordTypeName__c,User__c,Territory_Code__c,Dashboard_Weightage_Master__c,Month__c,Year__c,Parameters__c,RecordTypeId,PJP__c,Category__c,Input_Dashboard_ExID__c,Total_Monthly_Target__c,Total_MTD_Target__c,Total_Actual_MTD__c,Achievement_MTD__c,Score__c FROM Dashboard_Input_Score__c where RecordTypeId =: CSTLRecTypeId  AND  Month__c =: selectedMonth AND Year__c =: selectedYear AND Dashboard_Weightage_Master__r.Weightage__c>0];
            }
        }
        dashboardWrapper dashboardRecord         = new dashboardWrapper();
        dashboardRecord.catSet                   = new set<string>();
        dashboardRecord.moduleSet                = new set<string>();
        
        for(Dashboard_Input_Score__c ss : dashboardInputScore){
            dashboardRecord.catSet.add(ss.RecordTypeName__c+'@@'+ss.Category__c); 
        } 
        dashboardRecord.moduleSet.add(dealers);
        dashboardRecord.moduleSet.add(truckCustomer);     
        dashboardRecord.moduleSet.add(actionLog);
        
        dashboardRecord.dbInputScore               = dashboardInputScore;
        dashboardRecord.dashboardSummaryScore      = dashSummaryScore;
        return dashboardRecord; 
    }
}