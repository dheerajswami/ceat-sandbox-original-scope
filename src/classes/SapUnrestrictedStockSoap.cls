//Generated by wsdl2apex

public class SapUnrestrictedStockSoap {
    public class TableOfZbapiMatStock {
        public SapUnrestrictedStockSoap.ZbapiMatStock[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class Bapireturn {
        public String Type_x;
        public String Code;
        public String Message;
        public String LogNo;
        public String LogMsgNo;
        public String MessageV1;
        public String MessageV2;
        public String MessageV3;
        public String MessageV4;
        private String[] Type_x_type_info = new String[]{'Type','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Code_type_info = new String[]{'Code','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Message_type_info = new String[]{'Message','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] LogNo_type_info = new String[]{'LogNo','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] LogMsgNo_type_info = new String[]{'LogMsgNo','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] MessageV1_type_info = new String[]{'MessageV1','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] MessageV2_type_info = new String[]{'MessageV2','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] MessageV3_type_info = new String[]{'MessageV3','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] MessageV4_type_info = new String[]{'MessageV4','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Type_x','Code','Message','LogNo','LogMsgNo','MessageV1','MessageV2','MessageV3','MessageV4'};
    }
    public class ZbapiGetUnrstStock_element {
        public String Bzirk;
        public SapUnrestrictedStockSoap.TableOfZbapiMatStock Itab;
        public String Matkl;
        public String Matnr;
        public SapUnrestrictedStockSoap.TableOfBapireturn Return_x;
        private String[] Bzirk_type_info = new String[]{'Bzirk','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Itab_type_info = new String[]{'Itab','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Matkl_type_info = new String[]{'Matkl','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] Matnr_type_info = new String[]{'Matnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','1','false'};
        private String[] Return_x_type_info = new String[]{'Return','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Bzirk','Itab','Matkl','Matnr','Return_x'};
    }
    public class ZbapiMatStock {
        public String Matnr;
        public String Werks;
        public String Lgort;
        public String Labst;
        public String Maktx;
        public String Matkl;
        public String Wgbez;
        public String Lgobe;
        public String Name1;
        public String Bismt;
        private String[] Matnr_type_info = new String[]{'Matnr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Werks_type_info = new String[]{'Werks','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Lgort_type_info = new String[]{'Lgort','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Labst_type_info = new String[]{'Labst','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Maktx_type_info = new String[]{'Maktx','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Matkl_type_info = new String[]{'Matkl','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Wgbez_type_info = new String[]{'Wgbez','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Lgobe_type_info = new String[]{'Lgobe','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Name1_type_info = new String[]{'Name1','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Bismt_type_info = new String[]{'Bismt','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Matnr','Werks','Lgort','Labst','Maktx','Matkl','Wgbez','Lgobe','Name1','Bismt'};
    }
    public class TableOfBapireturn {
        public SapUnrestrictedStockSoap.Bapireturn[] item;
        private String[] item_type_info = new String[]{'item','urn:sap-com:document:sap:soap:functions:mc-style',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ZbapiGetUnrstStockResponse_element {
        public SapUnrestrictedStockSoap.TableOfZbapiMatStock Itab;
        public SapUnrestrictedStockSoap.TableOfBapireturn Return_x;
        private String[] Itab_type_info = new String[]{'Itab','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Return_x_type_info = new String[]{'Return','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Itab','Return_x'};
    }
    public class ZWS_GET_UNRST_STOCK {
        public String endpoint_x = 'http://CTECPHAP1.ceat.in:8000/sap/bc/srt/rfc/sap/zws_get_unrst_stock/300/zws_get_unrst_stock/zws_get_unrst_stock';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'SapUnrestrictedStockSoap', 'urn:sap-com:document:sap:rfc:functions', 'SapUnrestrictedStockRfc'};
        public SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element ZbapiGetUnrstStock(String Bzirk,SapUnrestrictedStockSoap.TableOfZbapiMatStock Itab,String Matkl,String Matnr,SapUnrestrictedStockSoap.TableOfBapireturn Return_x) {
            SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element request_x = new SapUnrestrictedStockSoap.ZbapiGetUnrstStock_element();
            request_x.Bzirk = Bzirk;
            request_x.Itab = Itab;
            request_x.Matkl = Matkl;
            request_x.Matnr = Matnr;
            request_x.Return_x = Return_x;
            SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element response_x;
            Map<String, SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element> response_map_x = new Map<String, SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:soap:functions:mc-style:ZWS_GET_UNRST_STOCK:ZbapiGetUnrstStockRequest',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZbapiGetUnrstStock',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZbapiGetUnrstStockResponse',
              'SapUnrestrictedStockSoap.ZbapiGetUnrstStockResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}