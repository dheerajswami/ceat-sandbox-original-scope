/*+----------------------------------------------------------------------
||         Author:  Kishlay Mathur
||
||        Purpose: To Update Pass code of Customer 
|| 
||        version: v1
||        purpose: To update pass code of customer and employee  
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/UpdatePassKey/*')

global with sharing class WB_UpdatePassKey {
    global class customerDetails{
            public String message           {get;set;}
            public String reference_Code    {get;set;}
    }
  
    @HttpGet
    global static customerDetails getCustomerDetails(){
      RestRequest req                     = RestContext.request;
        RestResponse res                  = RestContext.response;
        String reference_Code             = req.params.get('reference_Code');
        String customer_passCode_New      = req.params.get('customer_passCode_New');
        String customer_passCode_old      = req.params.get('customer_passCode_old');
        String flag                       = req.params.get('flag');
        system.debug(reference_Code+'-----customernumber-----'+'@@@@@@@@'+customer_passCode_New+'-----------newPassword-------'+customer_passCode_old+'--------oldpassword----------'+flag+'--------flag---------');
        try{
               if(flag != null && flag == 'D'){
                   Account account = [SELECT Id,Name,Passcode__c FROM Account WHERE KUNNR__c=:reference_Code];
                   if(account.Passcode__c == customer_passCode_old){
                      account.Passcode__c = customer_passCode_New;
                      update account;
                      customerDetails cusdetails =  new customerDetails();
                      cusdetails.reference_Code  =  reference_Code;
                      cusdetails.message         =  'Y'; 
                      return cusdetails; 
                       
                   }else{
                      customerDetails cusdetails =  new customerDetails();
                      cusdetails.reference_Code  =  reference_Code;
                      cusdetails.message         =  'incorrect old passkey'; 
                      return cusdetails;  
                   }
               }else if(flag != null && flag == 'E'){
                    Employee_Master__c employee = [SELECT id,Preferred_Language__c,Name,Passcode__c,EmployeeId__c FROM Employee_Master__c WHERE EmployeeId__c =: reference_Code];   
                    if(employee.Passcode__c == customer_passCode_old){
                      employee.Passcode__c = customer_passCode_New;
                      update employee;
                      customerDetails cusdetails =  new customerDetails();
                      cusdetails.reference_Code  =  reference_Code ;
                      cusdetails.message         =  'Y'; 
                      return cusdetails;
                    }else{
                      customerDetails cusdetails =  new customerDetails();
                      cusdetails.reference_Code  =  reference_Code;
                      cusdetails.message         =  'incorrect old passkey'; 
                      return cusdetails;
                    }
               }else{
                      customerDetails cusdetails =  new customerDetails();
                      cusdetails.reference_Code  =  reference_Code;
                      cusdetails.message         =  'Please make sure all input parameter are valid'; 
                      return cusdetails; 
               }
               
        }catch(Exception e){
          system.debug(e.getLineNumber() + ' Error -'+ e.getMessage());
          customerDetails cusdetails =  new customerDetails();
          cusdetails.reference_Code  =  reference_Code;
          cusdetails.message         =  'Please make sure all input parameter are valid'; 
          return cusdetails;
          
        }
        
       
    }
}