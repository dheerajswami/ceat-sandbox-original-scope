global class ScheduledBatch_ForSalesActual implements Schedulable{
   global void execute(SchedulableContext sc) {
      SP_BatchForSalesActual actualBatch = new SP_BatchForSalesActual (); 
      database.executebatch(actualBatch ,200);
   }
}