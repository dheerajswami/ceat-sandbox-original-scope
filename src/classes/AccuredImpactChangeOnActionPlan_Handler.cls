public class AccuredImpactChangeOnActionPlan_Handler 
{
   public static void notifyUserOnFieldChangeMethod(List<Action_Plan__c>newAPs, List<Action_Plan__c>oldAPs){

    Map<Id,Action_Plan__c> oldActionPlanlog = new Map<Id,Action_Plan__c> ();

    For(Action_Plan__c ap : oldAPs){

        oldActionPlanlog.put(ap.Id,ap);

     }

    
    for(Action_Plan__c ap : newAPs){
        string fieldNames = '';
        string values = '';
        
        if(ap.M1_Accrued_Impact__c!=null && ap.M1_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M1_Accrued_Impact__c ){
            fieldNames= 'M1 Accrued Impact';
            values = string.valueof(ap.M1_Accrued_Impact__c);
        
        }
        
          if(ap.M2_Accrued_Impact__c!=null && ap.M2_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M2_Accrued_Impact__c ){
            fieldNames= fieldNames +' , '+'M2 Accrued Impact';
            values = values + ' , '+string.valueof(ap.M2_Accrued_Impact__c);
        
        }
        
        
          if(ap.M3_Accrued_Impact__c!=null && ap.M3_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M3_Accrued_Impact__c ){
            fieldNames= fieldNames +' , '+'M3 Accrued Impact';
            values = values + ' , '+string.valueof(ap.M3_Accrued_Impact__c);
        
        }
        
        if((ap.M1_Accrued_Impact__c!=null && ap.M1_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M1_Accrued_Impact__c ) || (ap.M2_Accrued_Impact__c!=null && ap.M2_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M2_Accrued_Impact__c ) || (ap.M3_Accrued_Impact__c!=null && ap.M3_Accrued_Impact__c != oldActionPlanlog.get(ap.Id).M3_Accrued_Impact__c   ))
        {
         

            try{
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('The value of '+fieldNames + 'has been altered to ' + values);
                req1.setObjectId(ap.Id);
               
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setAction('Approve');
                Approval.ProcessResult result = Approval.process(req1);
                //Approval.ProcessResult result = Approval.process(req1);
            }catch(Exception e){
                    system.debug(e);
            }  
 
       }

    }

    
}

}