public with sharing class TH_CustomerStagingTriggerHandler {

    //-- SINGLETON PATTERN
      private static TH_CustomerStagingTriggerHandler instance;
      public static TH_CustomerStagingTriggerHandler getInstance() {
          if (instance == null) instance = new TH_CustomerStagingTriggerHandler();
          return instance;
      }
    
    
    //-- Before Insert
    
      
     public void onBeforeInsert(final List<Customer_Staging__c> newObjects){
     
        
     }
    
    //-- Before Update
     
      
     public void onBeforeUpdate(final List<Customer_Staging__c> newObjects,final List<Customer_Staging__c> oldObjects,final Map<Id, Customer_Staging__c> newObjectsMap,final Map<Id, Customer_Staging__c> oldObjectsMap){
     
        
     }
     
    
    //-- Before Delete
     
      
     public void onBeforeDelete(final List<Customer_Staging__c> oldObjects,final Map<Id, Customer_Staging__c> oldObjectsMap){
     
        
     }
     
    //-- After Insert
     
      
     public void onAfterInsert(final List<Customer_Staging__c> newObjects,final Map<Id, Customer_Staging__c> newObjectsMap){
     
     Set<String> customerNumb = new Set<String>();
     Set<String> prospectNumb = new Set<String>();
     list<Account> prospectConvert = new list<Account>();
     Map<String,Account> prospectsMap = new Map<String,Account>();
        for(Customer_Staging__c cst : newObjects){
            customerNumb.add(cst.Customer_Number__c);
            if(cst.ProspectCode__c != null){
                prospectNumb.add(cst.ProspectCode__c);
            }
        }

        if(prospectNumb.size() > 0){
            prospectConvert = [SELECT id,UniqueIdentifier__c,Prospect_No__c FROM Account WHERE UniqueIdentifier__c IN:prospectNumb OR Prospect_No__c IN:prospectNumb ];
        }
        if(prospectConvert.size() > 0){
            for(Account acc: prospectConvert){
                prospectsMap.put(acc.UniqueIdentifier__c,acc);
            }
        }
        Map<String,Account> accountsMap = new Map<String,Account>();

        for(Account account : [SELECT Id,KUNNR__c FROM Account WHERE KUNNR__c IN :customerNumb]){

            accountsMap.put(account.KUNNR__c,account);

        }

        List<Account> newAccounts = new List<Account>();
        
        String[] replacementCodes   = Label.Replacement_DC.split(',');
        String[] oeCodes            = Label.OE_DC.split(',');
        String[] exportsCode        = Label.Exports_DC.split(',');


        for(Customer_Staging__c ct : newObjects){
            
            if(accountsMap.containsKey(ct.Customer_Number__c)){
                    Account newAcc = accountsMap.get(ct.Customer_Number__c);

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 = ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    newAcc.Club_Type__c             = ct.Club_Type__c;
                    newAcc.Club_Type_Description__c           = ct.Club_Type_D__c;
					/*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;
                    //newAcc.Territory__c           =   ct.Sales_District__c;
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    newAcc.Address__c               =   (ct.Street_2__c != null?'':ct.Street_2__c) + ' ' +(ct.Street_3__c != null?'':ct.Street_3__c );//                    = cst.StrSuppl1;
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;
          			newAcc.Shipping_Plant__c        =   ct.Plant__c;
                    newAcc.Passcode__c              =   UtilityClass.generatePassCode(4);
          
                  if(ct.Division__c == '05') {
                        if(ct.Customer_Group__c == 'SP') {
                            newAcc.Type = 'Specialty Dealer';
                        } else if(ct.Customer_Group__c == 'DS') {
                            newAcc.Type = 'Specialty Distributor';
                        }
                    }
                
                    newAccounts.add(newAcc);
            }else if(prospectsMap.containsKey(ct.ProspectCode__c)){
                    Account newAcc = prospectsMap.get(ct.ProspectCode__c);

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 = ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    newAcc.Club_Type__c             = ct.Club_Type__c;
                    newAcc.Club_Type_Description__c           = ct.Club_Type_D__c;
                    /*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;
                    //newAcc.Territory__c           =   ct.Sales_District__c;
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    newAcc.Address__c               =   (ct.Street_2__c != null?'':ct.Street_2__c) + ' ' +(ct.Street_3__c != null?'':ct.Street_3__c );//                    = cst.StrSuppl1;
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;
                    newAcc.Shipping_Plant__c        =   ct.Plant__c;
                    newAcc.Passcode__c              =   UtilityClass.generatePassCode(4);
          
                  if(ct.Division__c == '05') {
                        if(ct.Customer_Group__c == 'SP') {
                            newAcc.Type = 'Specialty Dealer';
                        } else if(ct.Customer_Group__c == 'DS') {
                            newAcc.Type = 'Specialty Distributor';
                        }
                    }
                
                    newAccounts.add(newAcc);
            }else{
                    Account newAcc = new Account();

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 =     ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    newAcc.Club_Type__c             = ct.Club_Type__c;
                    newAcc.Club_Type_Description__c           = ct.Club_Type_D__c;
                    /*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;
                    //newAcc.Territory__c           =   ct.Sales_District__c;
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    newAcc.Address__c               =   (ct.Street_2__c != null?'':ct.Street_2__c) + ' ' +(ct.Street_3__c != null?'':ct.Street_3__c );//                    = cst.StrSuppl1;
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;
                    newAcc.Shipping_Plant__c        =   ct.Plant__c;
                    newAcc.Passcode__c              =   UtilityClass.generatePassCode(4);
                  if(ct.Division__c == '05') {
                        if(ct.Customer_Group__c == 'SP') {
                            newAcc.Type = 'Specialty Dealer';
                        } else if(ct.Customer_Group__c == 'DS') {
                            newAcc.Type = 'Specialty Distributor';
                        }
                    }
                    newAccounts.add(newAcc);
            }
            
            
        }

        upsert newAccounts;




        
     }  
     
    
    //-- After Update
    
      
     public void onAfterUpdate(final List<Customer_Staging__c> newObjects,final List<Customer_Staging__c> oldObjects,final Map<Id, Customer_Staging__c> newObjectsMap,final Map<Id, Customer_Staging__c> oldObjectsMap){
        
         Set<String> customerNumb = new Set<String>();
         Set<String> cgP  = new Set<String>();
         Set<String> cgp1 = new Set<String>();
         Set<String> cgp2 = new Set<String>();
         Set<String> cgp3 = new Set<String>();
		 Set<String> prospctNumb = new Set<String>();
		 list<Account> prospectConvrt = new list<Account>();
     	 Map<String,Account> prospectMap = new Map<String,Account>();


            for(Customer_Staging__c cst : newObjects){
                customerNumb.add(cst.Customer_Number__c);
                cgp.add(cst.Customer_Group__c);
                cgp1.add(cst.Customer_Group1__c);
                cgp2.add(cst.Customer_Group2__c);
                cgp3.add(cst.Customer_Group3__c);
				if(cst.ProspectCode__c != null){
                prospctNumb.add(cst.ProspectCode__c);
            }
            }

			
        	if(prospctNumb.size() > 0){
            prospectConvrt = [SELECT id,UniqueIdentifier__c,Prospect_No__c FROM Account WHERE UniqueIdentifier__c IN:prospctNumb OR Prospect_No__c IN:prospctNumb ];
        	}
        	if(prospectConvrt.size() > 0){
            for(Account acc1: prospectConvrt){
                prospectMap.put(acc1.UniqueIdentifier__c,acc1);
            }
        	}
			
			
			
			
			
			
            Map<String,Account> accountsMap = new Map<String,Account>();

            for(Account account : [SELECT Id,KUNNR__c FROM Account WHERE KUNNR__c IN :customerNumb]){

                accountsMap.put(account.KUNNR__c,account);

            }

            List<Account> newAccounts = new List<Account>();
            
            String[] replacementCodes   = Label.Replacement_DC.split(',');
            String[] oeCodes            = Label.OE_DC.split(',');
            String[] exportsCode        = Label.Exports_DC.split(',');

            for(Customer_Staging__c ct : newObjects){
            
            if(accountsMap.containsKey(ct.Customer_Number__c)){
                    Account newAcc = accountsMap.get(ct.Customer_Number__c);

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 = ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    /*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;
                    //newAcc.Territory__c           =   ct.Sales_District__c;
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    newAcc.Address__c               =   (ct.Street_2__c != null?'':ct.Street_2__c) + ' ' +(ct.Street_3__c != null?'':ct.Street_3__c );//                    = cst.StrSuppl1;
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;

                    newAccounts.add(newAcc);
            }
			else if(prospectMap.containsKey(ct.ProspectCode__c)){
                    Account newAcc = prospectMap.get(ct.ProspectCode__c);

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 = ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    newAcc.Club_Type__c             = ct.Club_Type__c;
                    newAcc.Club_Type_Description__c           = ct.Club_Type_D__c;
                    /*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;
                    //newAcc.Territory__c           =   ct.Sales_District__c;
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    newAcc.Address__c               =   (ct.Street_2__c != null?'':ct.Street_2__c) + ' ' +(ct.Street_3__c != null?'':ct.Street_3__c );//                    = cst.StrSuppl1;
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;
                    newAcc.Shipping_Plant__c        =   ct.Plant__c;
          
                  if(ct.Division__c == '05') {
                        if(ct.Customer_Group__c == 'SP') {
                            newAcc.Type = 'Specialty Dealer';
                        } else if(ct.Customer_Group__c == 'DS') {
                            newAcc.Type = 'Specialty Distributor';
                        }
                    }
                
                    newAccounts.add(newAcc);
            }
			else{
                    Account newAcc = new Account();

                    newAcc.LST_Number__c            =   ct.Central_Sales_Tax_Number__c;//   = cst.J1icstno;
                    newAcc.City__c                  =   ct.City__c;//                       = cst.Ort01;
                    newAcc.PARNR__c                 =   ct.Contact_Person_Number__c;//      = cst.Parnr;
                    newAcc.LAND1_GP__c              =   ct.Country_Key__c;//                = cst.Land1;
                    newAcc.WAERS_V02D__c            =   ct.Currency__c;//                   = cst.Waers;
                    newAcc.Customer_Group__c                 = ct.Customer_Group__c;
                    newAcc.Customer_Group_Text1__c           =    ct.Customer_Group1__c;
                    newAcc.Customer_Group_Text2__c           =    ct.Customer_Group2__c;
                    newAcc.Customer_Group_Text3__c           =    ct.Customer_Group3__c;
                    newAcc.Name                     =   ct.Customer_Name__c;//          = cst.Name1;
                    newAcc.KUNNR__c                 =   ct.Customer_Number__c;//            = cst.Kunnr;
                    newAcc.Date_Of_Birth__c         =   ct.Date_Of_Birth__c;//          = cst.Gbdat;
                    newAcc.Deletion_Flag__c         =   ct.Deletion_Flag__c;//          = cst.Loevm;
                    newAcc.Distribution_Channel_Text__c     =   ct.Distribution_Channel__c;//       = cst.Vtweg;
                    /*
                    for(String str  : replacementCodes){
                        if(str.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Replacement';
                        }
                    }
                    for(String str1 : oeCodes){
                        if(str1.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'OE';
                        }
                    }
                    for(String str2 : exportsCode){
                        if(str2.equals(ct.Distribution_Channel__c)){
                            newAcc.Business_Units__c = 'Exports';
                        }
                    }*/
                    newAcc.District__c              =   ct.District__c;//                   = cst.Katr6;
                    newAcc.Division_Text__c         =   ct.Division__c;//                   = cst.Spart;
                    newAcc.AD_SMTPADR__c            =   ct.Email_Address__c;//          = cst.SmtpAddr;
                    newAcc.Fax                      =   ct.First_Fax__c;//                  = cst.FaxNumber;
                    newAcc.PAN_Number__c            =   ct.PAN__c;//                        = cst.J1ipanno;
                    newAcc.PARVW__c                 =   ct.Partner_Function__c;//           = cst.Parvw;
                    newAcc.PIN_code__c              =   ct.Postal_Code__c;//                = cst.Pstlz;
                    newAcc.Region_Code__c           =   ct.Region__c;//                     = cst.Regio;
                    newAcc.REGIO__c                 =   ct.Region_L__c;//                   = cst.Bezei;
                    newAcc.Sales_District_Text__c   =   ct.Sales_District__c;//         = cst.Bzirk;                    
                    newAcc.Sales_Group_Text__c      =   ct.Sales_Group__c;//                = cst.Vkgrp;
                    newAcc.Sales_Office_Text__c     =   ct.Sales_Office__c;//               = cst.Vkbur;
                    
                    newAcc.Street_1__c              =   ct.Street_2__c;
                    newAcc.Street_2__c              =   ct.Street_3__c;
                    newAcc.AD_TLNMBR__c             =   ct.Telephone__c;//                  = cst.Telf1;
                    newAcc.Town_Code__c             =   ct.Telephone_Dialling_Code_Number__c;// = cst.TelNumber;
                    newAcc.Town__c                  =   ct.Town__c;//                       = cst.Katr7;

                    newAccounts.add(newAcc);
            }
        }

        upsert newAccounts;
        
     }
     
     
     //-- After Delete
          
     public void onAfterDelete(List<Customer_Staging__c> oldObjects,final Map<Id, Customer_Staging__c> oldObjectsMap){
     
        
     }
}