public with sharing class AttachmentUploadController {
public String SelectedAlphabet {get; set;}
public string errordsply;
public  List<Account> accnt;
public List<SelectOption> Alphabets {get; set;}
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }
  public AttachmentUploadController ()
  {
       Alphabets = new List<SelectOption>();
        SelectOption option = new SelectOption('--None--', '--None--');
        Alphabets.add(option);
       option = new SelectOption('All', 'All');
       Alphabets.add(option);
       option = new SelectOption('Customer', 'Customer');
        Alphabets.add(option);
        option = new SelectOption('Dealer', 'Dealer');
       Alphabets.add(option);
       option = new SelectOption('Exports', 'Exports');
        Alphabets.add(option);
        option = new SelectOption('OE', 'OE');
       Alphabets.add(option);
       option = new SelectOption('Prospect', 'Prospect');
        Alphabets.add(option);
        option = new SelectOption('Specialty', 'Specialty');
        Alphabets.add(option);
  }

  public PageReference upload() {
          system.debug('-------------------------test'+attachment);
          if(SelectedAlphabet == '--None--' || SelectedAlphabet == null)
          {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Please Select Customer Type'));
         
          }
          else
          {
          if(attachment.body == null)
          {
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Please Select File'));
         
          }
          else
          {
          if(attachment.name == null)
          {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Please Provide The File Name'));
          }
          else
          {
        Uploadattchmentbatch upbatch = new Uploadattchmentbatch();
        upbatch.alldetails(SelectedAlphabet,attachment);
        ID batchprocessid = Database.executeBatch(upbatch);
       // errordsply = upbatch.errorpassing();

      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment Uploaded Sucessfully'));
    attachment.name = '';
    attachment.description = '';
   // return (new pagereference('/001/o')) ; 
   }
   }
  
    }
      return null;
  }

}