global with sharing class Dashboard_Input_ControllerCSTL { 
  
    public String loggedInUserName           {get;set;}
    public String loggedInUserTerritory      {get;set;}  
    public string loggedInUserRO             {get;set;}
    public string loggedInUserZone           {get;set;}  
    public Static String lastDateRun         {get;set;}
    
    public id userId                         {get;set;}     
    public string selectedMonth              {get;set;}
    public string selectedYear               {get;set;}
    
    List<UserTerritory2Association> userTerrCode;
    List<Territory2> userTerritory;
    
    String loggedInUserId; 
    String cstlRole = System.Label.CSTL_Role; 
   
    // CONSTRUCTOR
    global Dashboard_Input_ControllerCSTL (){
        
        userId            = ApexPages.currentPage().getParameters().get('Id');
        selectedMonth     = ApexPages.currentPage().getParameters().get('month');
        selectedYear      = ApexPages.currentPage().getParameters().get('year');
            
        loggedInUserId   = UserInfo.getUserId();
        user loggin      = [SELECT id,name,FirstName From User Where id =: userId ];
        loggedInUserName = loggin.FirstName;        
        
         map<Integer,String> monthYearMap = new map<Integer,String>();
         /*
                To convert current month into month name
        */
            monthYearMap.put(1,'January');
            monthYearMap.put(2,'February');
            monthYearMap.put(3,'March');
            monthYearMap.put(4,'April');
            monthYearMap.put(5,'May');
            monthYearMap.put(6,'June');
            monthYearMap.put(7,'July');
            monthYearMap.put(8,'August');
            monthYearMap.put(9,'September');
            monthYearMap.put(10,'October');
            monthYearMap.put(11,'November');
            monthYearMap.put(12,'December');
            
        list<string> terrNames = new list<string>();
        
        userTerrCode=[select territory2Id,Territory2.ParentTerritory2.ParentTerritory2.name,Territory2.ParentTerritory2.name,Territory2.name,id from UserTerritory2Association where UserId=:userId AND RoleInTerritory2=: cstlRole];  
        
            if(userTerrCode.size() > 0){ 
                   loggedInUserRO = userTerrCode[0].Territory2.ParentTerritory2.name;
                   loggedInUserZone = userTerrCode[0].Territory2.ParentTerritory2.ParentTerritory2.name;                  
                   
                   for(UserTerritory2Association terr : userTerrCode){
                        terrNames.add(terr.Territory2.name);
                        
                   }
                    terrNames.sort();
            }
            if(terrNames.size() > 0){
                   for(string terr : terrNames){
                        if(loggedInUserTerritory =='' || loggedInUserTerritory ==null){
                            loggedInUserTerritory = terr;
                        }else{
                            loggedInUserTerritory = loggedInUserTerritory +','+ terr ;
                        }
                  }
            }
        Id CSTLRecTypeId        = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName =:system.Label.CSTL Limit 1].Id;
        
       list<Dashboard_Input_Score__c>  dashboardInputScore = [SELECT ID,LastModifiedDate  FROM Dashboard_Input_Score__c where RecordTypeId =: CSTLRecTypeId AND User__c=:userId AND Month__c =: monthYearMap.get(Integer.valueof(selectedMonth)) AND Year__c =: selectedYear limit 1];
         if(dashboardInputScore.size()>0){
            lastDateRun = dashboardInputScore[0].LastModifiedDate.date().format() ;
         }
        if(selectedMonth != string.valueof(date.today().month())){
            Integer numberOfDays = date.daysInMonth(integer.valueof(selectedYear), integer.valueof(selectedMonth));
            Date lastDayOfMonth  = date.newInstance(integer.valueof(selectedYear), integer.valueof(selectedMonth), numberOfDays);      
            lastDateRun          = string.valueof(lastDayOfMonth.format());
        }
    }
   
    @RemoteAction
    Public Static DashboardInputControllerHandlerCSTL.dashboardWrapper  getCstlInputDashboardData (Id userId, string month, string year) {
       
        DashboardInputControllerHandlerCSTL handler = new DashboardInputControllerHandlerCSTL();
        
        return handler.getDashboardCstlRecords(userId,month,year);
          
    }
}