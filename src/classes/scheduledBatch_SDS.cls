global class scheduledBatch_SDS implements Schedulable{
   @TestVisible private integer batchsize = 1; 
   global void execute(SchedulableContext sc) {
      if (Test.isRunningTest()){
           batchsize = 2000;
      } 
      SAP_BatchForCustomerSDS b = new SAP_BatchForCustomerSDS(); 
      database.executebatch(b,batchsize);
   }
}