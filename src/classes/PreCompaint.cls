public class PreCompaint {

 Public String accountId {
  get;
  set;
 }
 Public Boolean hasAccountId {
  get;
  set;
 }
 public PreCompaint() {

  accountId = ApexPages.CurrentPage().getparameters().get('id');
  if (accountId != '' && accountId != NULL) {
   hasAccountId = true;
  } else {
    hasAccountId = false;
   }
 }
 @RemoteAction
 public static PageReference createPreCompaint(String complainantName, String complainantMobile, String comment, String complainantSapNum, String accId) {
  String sfdcbaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
  boolean success = false;
  String requiredString;
  String s;
  Id preCompaintId = UtilityClass.getRecordTypeId('case', 'Pre-complaints');
  
  try {
   List < List < SObject >> searchList = new List < List < SObject >> ();
   String query;
   if (complainantSapNum != '') {
    s = complainantSapNum;
    if (s.contains('(')) {
     requiredString = s.substring(s.indexOf('(') + 1, s.indexOf(')'));
    } else {
     requiredString = s;
    }
    query = 'FIND \'' + requiredString + '\' IN ALL FIELDS RETURNING  Account (Id,Name)';
    searchList = search.query(query);
    List < Account > accounts = new List < Account > ();

    if (!(searchlist[0].IsEmpty())) {
     case newcase = new
     case ();
     accounts = (List < Account > ) searchList[0];
     String CheckOEorSpec;
     String CheckBussinessUnit;

     List<Account> accList = [Select type,State__c,District__c,Town__c,Email__c,Sales_Office_Text__c,Sales_Group_Text__c,Sales_District_Text__c,Business_Units__c,Phone,AD_TLNMBR__c,UniqueIdentifier__c from Account where id=: accounts[0].id Limit 1];
               CheckOEorSpec =accList[0].type;
               CheckBussinessUnit= accList[0].Business_Units__c;
               if(CheckOEorSpec =='OE Plant' || CheckOEorSpec == 'Speciality Plant'){
             
               newcase = new case(Name_of_complainant__c = complainantName,Mobile_of_complainant__c= complainantMobile,Description =comment, Comments__c =comment,RecordtypeID = preCompaintId, Accountid=accounts[0].id,Dealer_Plant__c=accounts[0].id,SAP_Reference_Number__c = requiredString,State__c = accList[0].State__c,District__c = accList[0].District__c,Town__c = accList[0].Town__c,Customer_Email__c = accList[0].Email__c,Sales_Office__c = accList[0].Sales_Office_Text__c,RO__c = AccList[0].Sales_Group_Text__c,Sales_District__c = accList[0].Sales_District_Text__c,Cluster_Code__c = accList[0].Sales_Office_Text__c);  
               }else
               { 
               newcase = new case(Name_of_complainant__c = complainantName,Mobile_of_complainant__c= complainantMobile,Description =comment, Comments__c =comment,RecordtypeID = preCompaintId, Accountid=accounts[0].id,SAP_Reference_Number__c = requiredString,State__c = accList[0].State__c,District__c = accList[0].District__c,Town__c = accList[0].Town__c,Customer_Email__c = accList[0].Email__c,Sales_Office__c = accList[0].Sales_Office_Text__c,RO__c = AccList[0].Sales_Group_Text__c,Sales_District__c = accList[0].Sales_District_Text__c,Cluster_Code__c = accList[0].Sales_Office_Text__c);
               }
     
      If(accList[0].Phone != null) {
       newcase.Customer_Phone__c = accList[0].Phone;
      } else {
       newcase.Customer_Phone__c = accList[0].AD_TLNMBR__c;
      }
           
     insert newcase;
     casecomment csecomment = new casecomment(CommentBody = comment, IsPublished = TRUE, ParentId = newcase.id);
     insert csecomment;
     if (newcase.id != null) {
      success = true;
      PageReference newocp = new PageReference(sfdcbaseUrl+'/'+newcase.id);
      newocp.setRedirect(true);
      return newocp;
     } else {
      return null;
     }
    } else {

     case newcase = new
     case (Name_of_complainant__c = complainantName, Mobile_of_complainant__c = complainantMobile, Description = comment, Comments__c = comment, RecordtypeID = preCompaintId, SAP_Reference_Number__c = complainantSapNum);
     insert newcase;
     casecomment csecomment = new casecomment(CommentBody = comment, IsPublished = TRUE, ParentId = newcase.id);
     insert csecomment;
     if (newcase.id != null) {
      success = true;
      PageReference newocp = new PageReference(sfdcbaseUrl+'/'+newcase.id);
      newocp.setRedirect(true);
      return newocp;
     } else {
      return null;
     }
    }
   } else {
    // Added by Ajeet
     if(accId == '' && complainantSapNum == ''){
        case newcase = new case();
    List<Account> mobileAccountList = [SELECT Id,Name,AD_TLNMBR__c,State__c,District__c,Town__c,Email__c FROM Account WHERE AD_TLNMBR__c = :complainantMobile ];
      System.debug('#@@@@2'+mobileAccountList);
    if( accid == '' && mobileAccountList.size()>0){
      newcase.Name_of_complainant__c = complainantName;
      newcase.Mobile_of_complainant__c = complainantMobile;
      newcase.Description = comment;
      newcase.Comments__c = comment;
      newcase.RecordtypeID = preCompaintId;
      newcase.Accountid = mobileAccountList[0].id;
      newcase.State__c = mobileAccountList[0].State__c;
      newcase.district__c = mobileAccountList[0].district__c;
      newcase.Town__c = mobileAccountList[0].Town__c;
      newcase.Customer_Phone__c = mobileAccountList[0].AD_TLNMBR__c;
          
            if(mobileAccountList[0].Email__c !=''){
            newcase.Customer_Email__c = mobileAccountList[0].Email__c;
            }
      system.debug('@#@###3'+newcase);
    
      insert newcase; 
     
    
    }else{
      system.debug('insideelse'+newcase);
      newcase.Name_of_complainant__c = complainantName;
      newcase.Mobile_of_complainant__c = complainantMobile;
      newcase.Description = comment;
      newcase.Comments__c = comment;
      newcase.RecordtypeID = preCompaintId;
      insert newcase;
      
    }
      
    casecomment csecomment = new casecomment(CommentBody = comment, IsPublished = TRUE, ParentId = newcase.id);
    insert csecomment;
    if (newcase.id != null) {
      success = true;
      PageReference newocp = new PageReference(sfdcbaseUrl+'/'+newcase.id);
      newocp.setRedirect(true);
      return newocp;
    } else {
 
      return null;
    }
    
    }
  
      else{ 
          Account ac=[SELECT Id,Name,KUNNR__c,State__c,District__c,Town__c,Email__c,AD_TLNMBR__c FROM Account where Id=:accId];

       case newcase = new case ();
       newcase.Name_of_complainant__c = complainantName;
       newcase.Mobile_of_complainant__c = complainantMobile;
       newcase.Description = comment;
       newcase.Comments__c = comment;
       newcase.RecordtypeID = preCompaintId;
       newcase.Accountid = accId;
       newcase.State__c = ac.State__c;
       newcase.district__c = ac.district__c;
       newcase.Town__c = ac.Town__c;
       newcase.Customer_Phone__c = ac.AD_TLNMBR__c;
          
            if(ac.Email__c !=''){
            newcase.Customer_Email__c = ac.Email__c;
            }
            if(ac.KUNNR__c == '')
            newcase.SAP_Reference_Number__c='';
            else
            newcase.SAP_Reference_Number__c=ac.KUNNR__c;
            system.debug('insideelse3'+newcase);
            insert newcase;
            if (newcase.id != null) {
             success = true;
             PageReference newocp = new PageReference(sfdcbaseUrl+'/'+newcase.id);
             newocp.setRedirect(true);
             return newocp;
            } else {
              system.debug('insideelse4'+newcase);
             return null;
            }

      }
    

    
   }


  } catch (Exception e) {
   system.debug(e.getLineNumber() + ' Error -' + e.getMessage());
   return null;
  }

 }

  @RemoteAction
 public static List < Account > searchAccounts(String searchTerm) {
  List < Account > accounts = Database.query('Select Id, Name,KUNNR__c from Account where Active__c=true AND (KUNNR__C != null AND KUNNR__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\') ORDER BY KUNNR__c LIMIT 10');
  return accounts;
 }

}