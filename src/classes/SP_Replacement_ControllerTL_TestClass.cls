/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SP_Replacement_ControllerTL_TestClass {
   
    
    static User init() {
        //String tlLabel = System.Label.TL_Role;   
        //String tlPageName   = System.Label.Sales_Planning_TL;
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBURep = new Territory2 ();
       
        Territory2 terrtypeZoneRepZE01 = new Territory2 ();
        Territory2 terrtypeZoneRepZC01 = new Territory2 ();   
        Territory2 terrtypeZoneRepPAT = new Territory2 ();
        
        Set<Id> regionId        = new Set<Id>();
        Set<String> regionCode  = new Set<String>();
       
        
        Territory2 terrtypeZoneRepPATC0081 = new Territory2 ();
        Territory2 terrtypeZoneRepPATC0082 = new Territory2 ();
        
        UserTerritory2Association RMuser = new UserTerritory2Association ();
        UserTerritory2Association TL81user = new UserTerritory2Association ();
      
        List<Territory2> parentRegCode=new List<Territory2>();
        List<Territory2> parentRegId=new List<Territory2>();
        List<String> parentRegIdList=new List<String>();
        List<String> parentRegCodeList=new List<String>();
       
        List<User> tlrm=new List<User>();
        List<Sales_Planning__c> listOFSPRepTL = new List<Sales_Planning__c>();
        List<Sales_Planning__c> listOFSPRepTLquery = new List<Sales_Planning__c>();
        
          
        user userRMData = new user();
        user userTLDataC0081 = new user();
        
        id terrTypeBU ;
        id terrTypeRO ;
        id terrTypeTerr ;
        id terrTypeZone ;
        
        // Load the test Sales planning Staging from the static resource
        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        insert terrModel;
        
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
        system.debug(listTerritoryType+'listTerritoryType');
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            if(t.DeveloperName == 'BUTest'){
                terrTypeBU = t.id;
            }
            if(t.DeveloperName == 'ROTestTest'){
                terrTypeRO = t.id;
            }
            if(t.DeveloperName == 'TerritoryTest'){
                terrTypeTerr = t.id;
            }
            if(t.DeveloperName == 'ZoneTest'){
                terrTypeZone = t.id;
            }
        }        
        //Created BU Territory
        terrtypeBURep =  CEAT_InitializeTestData.createTerritory('ReplacementsTest','ReplacementsTest',terrTypeBU,terrModel.id,null);
        insert terrtypeBURep;        
        
        //Created Zone territory For Replacement
        terrtypeZoneRepZE01  =  CEAT_InitializeTestData.createTerritory('ZE01','ZE01_test',terrTypeZone,terrModel.id,terrtypeBURep.id);
        insert terrtypeZoneRepZE01;
        
        //Created RO Territory Replacement
        terrtypeZoneRepPAT =  CEAT_InitializeTestData.createTerritory('PAT','PAT',terrTypeRO,terrModel.id,terrtypeZoneRepZE01.id);
        insert terrtypeZoneRepPAT;
      
        //Created District Territory Replacement
        terrtypeZoneRepPATC0081 =  CEAT_InitializeTestData.createTerritory('C0081','C0081',terrTypeTerr,terrModel.id,terrtypeZoneRepPAT.id);
        //terrtypeZoneRepPATC0081.ParentTerritory2Id=terrtypeZoneRepPAT.id;
        insert terrtypeZoneRepPATC0081;
         
        userTLDataC0081 = CEAT_InitializeTestData.createUser('mm','TestTL','sneha.agrawal@extentor.com','snehaTL81@ceat.com','12345','Replacements',null,'TL_Replacement');
        insert userTLDataC0081;
       
        TL81user = CEAT_InitializeTestData.createUserTerrAsso(userTLDataC0081.id, terrtypeZoneRepPATC0081.id,'TL_Replacement');
        insert TL81user;
        system.debug('TL81user.Territory2.name'+TL81user);
        system.debug('user'+userTLDataC0081.id);
        
         userRMData = CEAT_InitializeTestData.createUser('mm','TestRM','sneha.agrawal@extentor.com','snehaRM@ceat.com','456','Replacements',null,'RM');
         insert userRMData;
          
         // RMuser = new UserTerritory2Association ();
         RMuser = CEAT_InitializeTestData.createUserTerrAsso(userRMData.id, terrtypeZoneRepPAT.id,'RM');
         insert RMuser; 
        
        tlrm.add(userRMData);
        tlrm.add(userTLDataC0081);
        return userTLDataC0081;
      }
      static testmethod void testLoadData1() {
          String tlPageName   = System.Label.Sales_Planning_TL;
          String teststring = '';
          
          List<SPHandlerFor_SP_Replacement_ControllerTL.DealerWrapper> SpDlrecords= new List<SPHandlerFor_SP_Replacement_ControllerTL.DealerWrapper>(); 
          List<UserTerritory2Association> territoryForTL = new List<UserTerritory2Association>();
          List<Sales_Planning__c> listOFSPRepForRO = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForROquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForTL = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForTLquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepDealerquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepDealer = new List<Sales_Planning__c>();
          
          set<ID> setOFId =  new set<ID>();
          set<ID> setOFIdDealer =  new set<ID>();
          //set<ID> setOFIDTL =  new set<ID>();
          Map<String,String> mapOfSalesPlanning = new Map<String,String>();     
          ACW__c acw=new ACW__C();
          ACW__c unlAcw=new ACW__C();
          
          
          Account accrecord = new Account();
          Account accrecord1 = new Account();
          Account accrecord2 = new Account();
          Account accrecord3 = new Account();
          Account accrecord4 = new Account();           
          //Account accrecord4 = 
          User userTLDataC0081=SP_Replacement_ControllerTL_TestClass.init();
          //User userTLDataC0081=tlrm[0];
          //User userRMData=tlrm[1];
          
        Test.startTest();
        System.runAs(userTLDataC0081) {   
            List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType,'CatReplacement');
          String loggedInUserId =userTLDataC0081.id;  
          Prospect_Customer_Counter__c prospect=new Prospect_Customer_Counter__c(CustomerCounter__c=6,ProspectCounter__c=4);
          insert prospect;
          
         // Prospect_Customer_Counter__c orgValue = new Prospect_Customer_Counter__c.getOrgDefaultValues();
          //orgValue.Customercounter__c = 1;
         // orgValue.prospectCounter__c = 1;
         // insert orgValue;
          Id recId=[Select id,DeveloperName from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1].Id;
          
          accrecord = CEAT_InitializeTestData.createAccount('AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081');        
          
          insert accrecord;
          accrecord.ownerID = userTLDataC0081.id;
          accrecord.sds__c=20000;
          accrecord.Specialty_Value__c=200000;
          accrecord.Active__c = true;
          accrecord.Last_Month_SDS__c = 424244;
          update accrecord;
             
          accrecord1 = CEAT_InitializeTestData.createAccount('PATNA TYRE HOUSE', '50014542', 'PAT', 'ZE01', 'C0081');
          accrecord1.recordTypeId=recId;
          accrecord1.ownerID = userTLDataC0081.id;
          accrecord1.sds__c=20000;
          accrecord1.Specialty_Value__c=200000;
          accrecord1.Active__c = true;
          accrecord1.Last_Month_SDS__c = 424244;
          insert accrecord1;
            
          accrecord2 = CEAT_InitializeTestData.createAccount('Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081');
          accrecord2.recordTypeId=recId;
          accrecord2.ownerID = userTLDataC0081.id;
          accrecord2.sds__c=20000;
          accrecord2.Specialty_Value__c=200000;
          accrecord2.Active__c = true;
          accrecord2.Last_Month_SDS__c = 424244;
          insert accrecord2;
            
          accrecord3 = CEAT_InitializeTestData.createAccount('TYRE AVENUE', '50003565', 'PAT', 'ZE01', 'C0081');
          accrecord3.recordTypeId=recId;
          accrecord3.sds__c=20000;
          accrecord3.ownerID = userTLDataC0081.id;
          accrecord3.Active__c = true;
          accrecord3.Specialty_Value__c=200000;
          accrecord3.Last_Month_SDS__c = 424244;
          insert accrecord3;
          
          accrecord4 = CEAT_InitializeTestData.createAccount('kishlay', '2354535', 'PAT', 'ZE01', 'C0081');
          accrecord4.recordTypeId=recId;
          accrecord4.sds__c=20000;
          accrecord4.ownerID = userTLDataC0081.id;
          accrecord4.Active__c = true;
          accrecord4.Specialty_Value__c=200000;
          accrecord4.Last_Month_SDS__c = 424244;
          insert accrecord4;
          
          Id lockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName ='Locking' Limit 1].Id;
          Id unlockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =:'Unlocking' Limit 1].Id;
            
            ServerUrl__c url= new ServerUrl__c(Name='Ceat',Url__c = 'https://cs5.salesforce.com');
            insert url;
            Server_Url__c url1= new Server_Url__c(Name='Ceat',Url__c = 'https://cs5.salesforce.com');
            insert url1;
            acw.Page__c=tlPageName;
            acw.Sales_Planning__c=true;
            acw.From_Date__c= system.today(); 
            acw.To_Date__c = system.today()+1;  
            acw.RecordTypeId =lockingRecType;//locking Recored type 012O00000000bkYIAQ
            insert acw;                 
            
             unlAcw.Page__c=tlPageName;
             unlAcw.Sales_Planning__c=true;
             unlAcw.From_Date__c= system.today()+3; 
             unlAcw.To_Date__c = system.today()+5;  
             unlAcw.RecordTypeId =unlockingRecType;//Unlocking Recored type 
             insert unlAcw;   
             SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);  
            
            territoryForTL = [Select IsActive,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId()  ];
                
            
            //List<sObject> serverUrl = Test.loadData(Server_Url__c.sObjectType, 'ServerUrl');
            
            
             //insert dis;
             listOFSPRepForRO = CEAT_InitializeTestData.createSalesPlanningsRepForcastRO('Replacement_RO');
             insert listOFSPRepForRO;
             for(Sales_Planning__c sp : listOFSPRepForRO){
                    setOFID.add(sp.id);
             }
             listOFSPRepForROquery = [Select Id,ASP__c,Dealer__r.Name,Dealer__r.id,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE id IN:setOFID];
             
             listOFSPRepForTL = CEAT_InitializeTestData.createSalesPlanningsRepTLForTL('Replacement_TL');              
             insert listOFSPRepForTL;  
             
             listOFSPRepDealer=CEAT_InitializeTestData.createSalesPlanningsRepDealerC0081('Replacement_Dealer');     
             insert listOFSPRepDealer;
             for(Sales_Planning__c sp : listOFSPRepDealer){
                    setOFIDDealer.add(sp.id);
             }
             listOFSPRepDealerquery = [Select Id,Dealer__r.id,Name,Dealer__r.Prospect_No__c,Dealer__r.Sales_District_Text__c,Dealer__r.SDS__c,Total_planned__c,Total_LYCM__c,Total_L3M__c,Dealer__r.Name,Dealer__r.KUNNR__c,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,SPExternalIDTL__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE id IN:setOFIDDealer];
            
             SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);               
             system.debug('listOFSPRepDealerquery'+listOFSPRepDealerquery);
             teststring = '';
             for(Sales_Planning__c sp : listOFSPRepDealerquery){
               if(sp.SPExternalIDTL__c == '50003565PATC00812080052015'){
                    //ExtId /custnum /regcode /terrcode/ catCode /dlname /accId /catAsp*planned
                   teststring = sp.SPExternalIDTL__c+'@@'+sp.Dealer__r.KUNNR__c+'@@'+sp.Region_code__c+'@@'+sp.Territory_Code__c+'@@'+sp.Category__c+'@@'+sp.Dealer__r.Name+'@@'+sp.Dealer__r.Id+'@@'+'11111';
                    sp.Total_planned__c = 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(sp.Total_planned__c));               
                }
             }
             system.debug('teststring1'+teststring);
             //teststring = 'undefined'+'@@'+'51000047'+'@@'+'PAT'+'@@'+'C0081'+'@@'+'2120'+'@@'+'AJIT TYRES PVT. LIMITED'+'@@'+listOFSPRepDealerquery[3].Dealer__r.Id+'@@'+'20000';
            // mapOfSalesPlanning.put(teststring,'55');
             SP_Replacement_Controller_TL.saveAsDraftDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
             SP_Replacement_Controller_TL.updateDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
            // SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);*/
                  
            
       } 
       Test.stopTest();
       //System.runAs(userRMData) {   
            // SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id); 
           //} 
      } 
       static testmethod void testLoadData4() {
          String tlPageName   = System.Label.Sales_Planning_TL;
          String teststring = '';
          
          List<SPHandlerFor_SP_Replacement_ControllerTL.DealerWrapper> SpDlrecords= new List<SPHandlerFor_SP_Replacement_ControllerTL.DealerWrapper>(); 
          List<UserTerritory2Association> territoryForTL = new List<UserTerritory2Association>();
          List<Sales_Planning__c> listOFSPRepForRO = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForROquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForTL = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepForTLquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepDealerquery = new List<Sales_Planning__c>();
          List<Sales_Planning__c> listOFSPRepDealer = new List<Sales_Planning__c>();
          
          set<ID> setOFId =  new set<ID>();
          set<ID> setOFIdDealer =  new set<ID>();
          //set<ID> setOFIDTL =  new set<ID>();
          Map<String,String> mapOfSalesPlanning = new Map<String,String>();     
          ACW__c acw=new ACW__C();
          ACW__c unlAcw=new ACW__C();
          
          Account accrecord = new Account();
          Account accrecord1 = new Account();
          Account accrecord2 = new Account();
          Account accrecord3 = new Account();
          Account accrecord4 = new Account();           
          Id lockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName ='Locking' Limit 1].Id; 
          Id accprosrecid=[select id,DeveloperName from RecordType where SobjectType='Account' and DeveloperName ='Prospect' Limit 1].Id;
          Id unlockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =:'Unlocking' Limit 1].Id;
            
          
          User userTLDataC0081=SP_Replacement_ControllerTL_TestClass.init();
          //User userTLDataC0081=tlrm[0];
          //User userRMData=tlrm[1];
          Test.startTest();
          System.runAs(userTLDataC0081) {  
            accrecord = CEAT_InitializeTestData.createAccount('AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081');        
          insert accrecord;
          accrecord.type='prospect';
          accrecord.Active__c=true;
          accrecord.RecordTypeId=accprosrecid;
          accrecord.sds__c=20000;
          accrecord.Specialty_Value__c=200000;
          accrecord.Last_Month_SDS__c = 424244;
          accrecord.Business_Units__c = 'Replacement';
          update accrecord;
             
          accrecord1 = CEAT_InitializeTestData.createAccount('PATNA TYRE HOUSE', '50014542', 'PAT', 'ZE01', 'C0081');
          insert accrecord1;
          accrecord1.RecordTypeId=accprosrecid;
          accrecord1.sds__c=20000;
          accrecord1.Active__c=true;
          accrecord1.type='prospect';
          accrecord1.Specialty_Value__c=200000;
          accrecord1.Last_Month_SDS__c = 424244;
          accrecord1.Business_Units__c = 'Replacement';
          update accrecord1;
            
          accrecord2 = CEAT_InitializeTestData.createAccount('Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081');
          insert accrecord2;
          //accrecord2.RecordTypeId=accprosrecid;
          accrecord2.sds__c=20000;
          accrecord2.Active__c=true;
         // accrecord2.type='prospect';
          accrecord2.Specialty_Value__c=200000;
          accrecord2.Last_Month_SDS__c = 424244;
          accrecord2.Business_Units__c = 'Replacement';
          update accrecord2;
            
          accrecord3 = CEAT_InitializeTestData.createAccount('TYRE AVENUE', '50003565', 'PAT', 'ZE01', 'C0081');
          insert accrecord3;
          //accrecord3.RecordTypeId=accprosrecid;
          //accrecord3.type='prospect';
          accrecord3.Active__c=true;
          accrecord3.sds__c=20000;
          accrecord3.Specialty_Value__c=200000;
          accrecord3.Last_Month_SDS__c = 424244;
          accrecord3.Business_Units__c = 'Replacement';
          update accrecord3;
          
            system.debug('accprosrecid'+accprosrecid);
            ServerUrl__c url= new ServerUrl__c(Name='Ceat',Url__c = 'https://cs5.salesforce.com');
            insert url;
            Server_Url__c url1= new Server_Url__c(Name='Ceat',Url__c = 'https://cs5.salesforce.com');
            insert url1;
             Locking_Screen__c lsforTL= new Locking_Screen__c(User__c =userTLDataC0081.Id,Month__c = '4' ,Year__c = '2015',Submitted__c = true, status__c = 'Submitted',Territory_Code__c='C0081',View_Sales_Planning__c='/apex/SP_Replacement_TL?id='+userTLDataC0081.Id);
             insert lsforTl;
             
            acw.Page__c=tlPageName;
            acw.Sales_Planning__c=true;
            acw.From_Date__c= date.parse('1/2/2015'); 
            acw.To_Date__c = date.parse('1/2/2015');  
            acw.RecordTypeId =lockingRecType;//locking Recored type 012O00000000bkYIAQ
            acw.Month_Text__c= 'April';
            insert acw;      
             
             unlAcw.Page__c=tlPageName;
             unlAcw.Sales_Planning__c=true;
             unlAcw.From_Date__c= system.today(); 
             unlAcw.To_Date__c = system.today();  
             unlAcw.RecordTypeId =unlockingRecType;//Unlocking Recored type 
             acw.Month_Text__c= 'April';
             insert unlAcw;   
             //SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);  
            
            territoryForTL = [Select IsActive,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId()];
                
            List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatReplacement');
            //List<sObject> serverUrl = Test.loadData(Server_Url__c.sObjectType, 'ServerUrl');
            
            
             //insert dis;
             listOFSPRepForRO = CEAT_InitializeTestData.createSalesPlanningsRepForcastRO('Replacement_RO');
             insert listOFSPRepForRO;
             for(Sales_Planning__c sp : listOFSPRepForRO){
                    setOFID.add(sp.id);
             }
             listOFSPRepForROquery = [Select Id,ASP__c,Dealer__r.Name,Dealer__r.id,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE id IN:setOFID];
             
             listOFSPRepForTL = CEAT_InitializeTestData.createSalesPlanningsRepTLForTL('Replacement_TL');              
             insert listOFSPRepForTL;  
             
             //listOFSPRepDealer=CEAT_InitializeTestData.createSalesPlanningsRepDealerC0081('Replacement_Dealer');     
             //insert listOFSPRepDealer;
             //for(Sales_Planning__c sp : listOFSPRepDealer){
                   //setOFIDDealer.add(sp.id);
            // }
            
             Id replaceDealerId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName ='Replacement_Dealer' Limit 1].Id;
        
             Sales_Planning__c spd = new Sales_Planning__c(Dealer_CustNumber__c = '50015640',Dealer_Name__c = 'Chawla tyres',
                                                      RecordTypeId = replaceDealerId,
                                                      Category__c = '2080',
                                                      Category_Description__c = 'JEEP CON',
                                                      Month__c = '4',
                                                      Zone__c = 'ZE01',
                                                      Region_code__c = 'PAT',
                                                      Region_Description__c = 'Patna',
                                                      SPExternalIDTL__c = '50015640PATC0081208022015',
                                                      Territory_Code__c = 'C0081',
                                                      L3M__c = 400,                                                   
                                                      Value_L3M__c = 4526,
                                                      Value_LYCM__c = 6733,
                                                      Year__c = '2015',
                                                      UploadExternalID__c = '50015640208022015'
                                              );
             insert spd;
             Sales_Planning__c spd1 = new Sales_Planning__c(Dealer_CustNumber__c = '50003565',Dealer_Name__c = 'TYRE AVENUE', 
                                                      RecordTypeId = replaceDealerId,
                                                      Category__c = '2080',
                                                      Category_Description__c = 'JEEP CON',
                                                      Month__c = '4',
                                                      Zone__c = 'ZE01',
                                                      Region_code__c = 'PAT',
                                                      Region_Description__c = 'Patna',
                                                      SPExternalIDTL__c = '50003565PATC0081208022015',
                                                      Territory_Code__c = 'C0081',
                                                      L3M__c = null,                                                      
                                                      Value_L3M__c = 4526,
                                                      Value_LYCM__c = 6733,
                                                      Year__c = '2015',
                                                      UploadExternalID__c = '50003565208022015'
                                              );
             insert spd1;
             setOFIDDealer.add(spd1.id);
             setOFIDDealer.add(spd.id);
             listOFSPRepDealerquery = [Select Id,Dealer__r.id,Name,Dealer__r.Prospect_No__c,Dealer__r.Sales_District_Text__c,Dealer__r.SDS__c,Total_planned__c,Total_LYCM__c,Total_L3M__c,Dealer__r.Name,Dealer__r.KUNNR__c,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,SPExternalIDTL__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE id IN:setOFIDDealer];
            
             SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);               
         
             teststring = '';
             for(Sales_Planning__c sp : listOFSPRepDealerquery){
                if(sp.SPExternalIDTL__c == '50014542PATC0081212022015'){
                    //ExtId /custnum /regcode /terrcode/ catCode /dlname /accId /catAsp*planned
                    teststring = sp.SPExternalIDTL__c+'@@'+sp.Dealer__r.KUNNR__c+'@@'+sp.Region_code__c+'@@'+sp.Territory_Code__c+'@@'+sp.Category__c+'@@'+sp.Dealer__r.Name+'@@'+sp.Dealer__r.Id+'@@'+'11111';
                    sp.Total_planned__c = 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(sp.Total_planned__c));                    
                }
             }
             system.debug('teststring'+teststring);
             //SP_Replacement_Controller_TL.saveAsDraftDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
             //SP_Replacement_Controller_TL.updateDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
             
             teststring = 'undefined'+'@@'+'51000047'+'@@'+'PAT'+'@@'+'C0081'+'@@'+'2120'+'@@'+accrecord.name+'@@'+accrecord.Id+'@@'+'60';
             system.debug('teststring'+teststring);
             
             mapOfSalesPlanning.put(teststring,'55');
             SP_Replacement_Controller_TL.saveAsDraftDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
             SP_Replacement_Controller_TL.updateDlSpRecord(mapOfSalesPlanning,userTLDataC0081.id);
             SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id);
          
       } 
       Test.stopTest();
      // System.runAs(userRMData) {   
            // SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id); 
          // } 
       
      } 
     /* static testmethod void testLoadData2() {
          User userRMData = CEAT_InitializeTestData.createUser('mm','TestRM','sneha.agrawal@extentor.com','snehaRM@ceat.com','1234','System Administrator',null,'RM');
          insert userRMData;
          
          UserTerritory2Association RMuser = new UserTerritory2Association ();
          RMuser = CEAT_InitializeTestData.createUserTerrAsso(userRMData.id, terrtypeZoneRepPAT.id,'RM');
          insert RMuser; 
          
          User userTLDataC0081=SP_Replacement_ControllerTL_TestClass.init();
          System.runAs(userRMData) {   
            SpDlrecords=SP_Replacement_Controller_TL.getDealersOfTL(userTLDataC0081.id); 
          }     
       } */
     
     
}