/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class ActionLogTriggerHandler_TestClass {
   
     static Task task;
     static Account acc;
     static User user;
     static User user1; 
     static User user2; 
    static Prospect_Customer_Counter__c counterL;
     
     static List<Task> tasks;
     static List<Task> taskList;
     static List<Task> updatedTaskList;
    
    //private static  final String OWNER        ='005O0000001yUd8IAE';
    //private static  final String REASSIGNUSER ='00590000003S1HAAA0';
     static  final String SUBJECT      = 'Resolve claims';
     static  final String SOURCE       ='Complaints';
     static  final String DESCRIPTION  ='TestClass';
     static  final String CONCLUSION   ='TestClass Conclusion';
    
     static List<Task> actionlogList=new List<Task>();
    
    public static List<Task> init(){
        
        taskList            =new List<Task>();
        updatedTaskList     =new List<Task>();
        Map<Id,Task> newObjTask= new  Map<Id,Task>();
        Map<Id,Task> oldObjTask= new  Map<Id,Task>();
         
        counterL = ActionLog_Initialize_TestData.createCustomerSetP();
        insert counterL;
        
        user=ActionLog_Initialize_TestData.createUser('Replacements','TLuser@testorg.com');
        database.insert(user);
        
        user1=ActionLog_Initialize_TestData.createUser('Replacements','RM@demo.test');
        user1.Alias='stdusr';
        database.insert(user1);
        
        user2=ActionLog_Initialize_TestData.createUser('Replacements','RMuser2@testorg.com');
        user2.Alias='usrstd';
        database.insert(user2);
        
        Integer tempNumber = Integer.valueOf(Math.random()*100000000);
        acc=ActionLog_Initialize_TestData.createAccount('Test Account','B0102','Upcountry','T'+String.valueOf(tempNumber));       
         
           
        System.runAs(user){
            
            ActionLogTriggerHandler act =new ActionLogTriggerHandler();
            
           /* EmailTemplate validEmailTemplate = new EmailTemplate();
            validEmailTemplate.isActive = true;
            validEmailTemplate.Name = 'Action Log escalation notification';
            validEmailTemplate.DeveloperName = 'sneha1234';
            validEmailTemplate.TemplateType = 'text';
            validEmailTemplate.FolderId = UserInfo.getUserId();
            
            insert validEmailTemplate; */
            Server_url__c url=new Server_url__c(url__c='https://cs5.salesforce.com');
            insert url;
            insert(acc);
            
            task=ActionLog_Initialize_TestData.createActionLog('Open',system.today());
            
            task.Source__c          =SOURCE;        
            task.WhatId             =acc.Id;
            task.Subject            =SUBJECT;
            task.OwnerId            =user.Id;
            task.Assign_Escalate__c =user1.Id;  
            //task.Description          =DESCRIPTION;
            task.OwnerId            =user.Id;   
            
            taskList.add(task);
            insert(taskList);   
                
            for(Task t:taskList){
                oldObjTask.put(t.Id,t);
            }
            
            task.status             ='Done';
            task.Assign_Escalate__c =user2.Id;
            task.Conclusion__c      =CONCLUSION;
            updatedTaskList.add(task);
            update updatedTaskList; 
            
            for(Task t:updatedTaskList){
                newObjTask.put(t.Id,t);
            }
            ActionLogTriggerHandler.notifyUser(updatedTaskList,taskList);  
            
            act .onBeforeUpdate(updatedTaskList,taskList,newObjTask,oldObjTask);
            act .onAfterUpdate(updatedTaskList,taskList,newObjTask,oldObjTask);
            act .onBeforeDelete(taskList,oldObjTask);
            act .onAfterDelete(taskList,oldObjTask);
            act .onAfterInsert(updatedTaskList,newObjTask);
         }  
         return updatedTaskList;    
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
       ActionLogTriggerHandler act =ActionLogTriggerHandler.getInstance();
       tasks= init();
       ActionLogTriggerHandler.assignDL(tasks);
       act.onBeforeInsert(tasks);   
       ActionLogTriggerHandler.notifyUserInsert(tasks);
    }
}