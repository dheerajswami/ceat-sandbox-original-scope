@isTest
private class MB_PJP_OE_Execution_TestClass {
    
    private static Account account;
    private static Task  task;  
    private static Prospect_Customer_Counter__c counterL;
    private static PJP__c pjp;
    private static Visits_Plan__c visit;
    private static List<Task> tasks;
    private static List<Visits_Plan__c> visits;
    
    
    public static void initializeTestData(){
        Integer tempNumber = Integer.valueOf(Math.random()*100000000);
        /*
counterL = ActionLog_Initialize_TestData.createCustomerSetP();
database.insert(counterL);

account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));

database.insert(account);       

pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
database.insert(pjp);

visit = CEAT_InitializeTestData.createSingleVisits(pjp,account.Id);
//database.insert(visit);
//
tasks = new List<Task>();
tasks = CEAT_InitializeTestData.createMultipleTasks(account.Id,null,UserInfo.getUserId(),false);
database.insert(tasks);

*/
        Map<String,Id> roleId 		= new Map<String,Id>();
        Map<String,Id>  profileIds 	= new Map<String,Id>();
        
        for(UserRole role : [SELECT Id,Name FROM UserRole]){
            roleId.put(role.Name,role.Id);
        }
        
        for(Profile prof : [SELECT Id,Name FROM Profile]){
            profileIds.put(prof.Name,prof.Id);
        }
        
        User rmUser = CEAT_InitializeTestData.createUserPJP('CSOE'+tempNumber,'TEST','testOE.'+tempNumber+'@test.com','testOE.'+tempNumber+'@test.com',String.valueOf(tempNumber),profileIds.get('OE'),null,roleId.get('RM - OE'));
        database.insert(rmUser);
        
        system.runAs(rmUser){
            
            counterL = ActionLog_Initialize_TestData.createCustomerSetP();
            database.upsert(counterL);
            
            account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
            account.RecordTypeId = [select Id from RecordType where SobjectType = 'Account' and Name = 'OE' limit 1].Id;
            database.insert(account);     
            
            pjp = CEAT_InitializeTestData.createPjpRecord(UserInfo.getUserId());
            database.insert(pjp);
            
            visits = new List<Visits_Plan__c>();
            visits = CEAT_InitializeTestData.createMultipleVisits(pjp,account.Id,null,false);
            database.insert(visits);
            
            tasks = new List<Task>();
            tasks = CEAT_InitializeTestData.createMultipleTasks(account.Id,null,rmUser.Id,false);
            database.insert(tasks);
            
        }
        
        
    }
    
    public static void initializeTestData2(){
        Integer tempNumber = Integer.valueOf(Math.random()*100000000);
        
        counterL = ActionLog_Initialize_TestData.createCustomerSetP();
        database.insert(counterL);
        
        account = CEAT_InitializeTestData.createAccount('Test Account','T'+String.valueOf(tempNumber));
        account.RecordTypeId = [select Id from RecordType where SobjectType = 'Account' and Name = 'OE' limit 1].Id;
        database.insert(account);       
        
        visit = CEAT_InitializeTestData.createSingleVisits(pjp,account.Id);
        //database.insert(visit);
        
    }
    
    /*
* Test case when the visit is new 
*/
    
    static testMethod void positiveTestCase() {
        initializeTestData();
        Test.startTest();
        
        MB_PJP_MobileDataDummy.MobileDataRequest mobReq = new MB_PJP_MobileDataDummy.MobileDataRequest();
        mobReq.visitsPlans.addAll(visits);
        //mobReq.visitsPlans.addAll(visitsWithoutId);
        mobReq.tasks.addAll(tasks);
        //mobReq.tasks.addAll(tasksWithouId);
        
        String josn = JSON.serialize(mobReq);      
        
        //String josn = JSON.serialize(visit);
        
        system.RestContext.request = new RestRequest();
        RestContext.request.requestBody = Blob.valueOf(josn);
        
        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchPJPMobileDataOE'; 
        RestContext.request.httpMethod = 'POST';
        
        MB_PJP_OE_Execution.getMobileDataPJP();
        
        Test.stopTest();
        
    }
    
    /*
* Test case when the visit is has to be updated 
*/
    /*
    static testMethod void positiveTestCaseUpdate() {
        
        Test.startTest();
        
        initializeTestData();
        database.insert(visit);
        
        String josn = JSON.serialize(visit);
        
        system.RestContext.request = new RestRequest();
        RestContext.request.requestBody = Blob.valueOf(josn);
        
        RestContext.request.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FetchPJPMobileDataOE'; 
        RestContext.request.httpMethod = 'POST';
        
        MB_PJP_OE_Execution.getMobileDataPJP();
        
        Test.stopTest();
        
    }*/
}