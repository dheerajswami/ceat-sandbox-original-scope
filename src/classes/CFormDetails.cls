global class CFormDetails {
    
    global class CFormMapping{
        public String CUSTOMER {get;set;}
        public String CUSTOMER_NAME{get;set;}
        public Date INVOICE_DATE{get;set;}
        public String MONTH{get;set;}
        public String INVOICE{get;set;}
        public String ITEM{get;set;}
        public String MATERIAL{get;set;}
        public String MATERIAL_DESCRIPTION{get;set;}
        public String CUST_PART_NUMBER{get;set;}
        public String PO_NO{get;set;}
        public Decimal INVOICE_QTY{get;set;}
        public Decimal PRE_TAX{get;set;}
        public Decimal POST_TAX{get;set;}
        public Decimal TAX_VALUE{get;set;}
        public String SALES_OFFICE{get;set;}
        public String SELLER_STATE{get;set;}
        public String SELLER_TIN_NO{get;set;}
        public String MBLNR{get;set;}
        public Date BUDAT{get;set;}
        
        
        public CFormMapping(String CUSTOMER, String CUSTOMER_NAME, String INVOICE_DATE, String MONTH, String INVOICE, String ITEM,String MATERIAL, String MATERIAL_DESCRIPTION, String CUST_PART_NUMBER, String PO_NO, String INVOICE_QTY, String PRE_TAX, String POST_TAX, String TAX_VALUE, String SALES_OFFICE, String SELLER_STATE, String SELLER_TIN_NO,String MBLNR,String BUDAT){
            this.CUSTOMER = CUSTOMER;
            this.CUSTOMER_NAME = CUSTOMER_NAME;
            this.INVOICE_DATE = UtilityClass.getDateFromString(INVOICE_DATE);
            this.MONTH = MONTH;
            this.INVOICE = INVOICE;
            this.ITEM = ITEM;
            this.MATERIAL = MATERIAL;
            this.MATERIAL_DESCRIPTION = MATERIAL_DESCRIPTION;
            this.CUST_PART_NUMBER = CUST_PART_NUMBER;
            this.PO_NO = PO_NO;
            this.INVOICE_QTY =  (INVOICE_QTY != '' || INVOICE_QTY != null ? Decimal.valueOf(INVOICE_QTY):0.0);
            this.PRE_TAX = (PRE_TAX != '' || PRE_TAX != null ? Decimal.valueOf(PRE_TAX):0.0);
            this.POST_TAX = (POST_TAX != '' || POST_TAX != null ? Decimal.valueOf(POST_TAX):0.0);
            this.TAX_VALUE = (TAX_VALUE != '' || TAX_VALUE != null ? Decimal.valueOf(TAX_VALUE):0.0);
            this.SALES_OFFICE = SALES_OFFICE;
            this.SELLER_STATE = SELLER_STATE;
            this.SELLER_TIN_NO = SELLER_TIN_NO;
            this.MBLNR = MBLNR; 
            this.BUDAT = (BUDAT == '0000-00-00')?null:UtilityClass.getDateFromString(BUDAT);
        }
    }
    
    WebService static List<CFormMapping> getAllCFormDetails(String custNum, String fDate, String tDate){
        
        List<CFormMapping> allCForms = new List<CFormMapping>();
        try{
            SAP_ZWS_CFORM.ZSFDC_CFORM_SALESResponse_element cFormResponseElement = new SAP_ZWS_CFORM.ZSFDC_CFORM_SALESResponse_element();
            SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES  clsTable = new SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES();
            SAP_ZWS_CFORM.zws_sfdc_cform_sales  clsInstance = new SAP_ZWS_CFORM.zws_sfdc_cform_sales ();
            
            Blob headerValue 		= Blob.valueOf('sfdc'+ ':' +'ceat@1234');
            // Blob headerValue 		= Blob.valueOf('ho_abap'+ ':' +'ceat2@2015');
            String authorizationHeader 		= 'Basic '+ EncodingUtil.base64Encode(headerValue);
            
            clsInstance.inputHttpHeaders_x = new Map<String,String>();
            clsInstance.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            clsInstance.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
            clsInstance.timeout_x 									= 60000;
            
            SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES cFormResponses = new SAP_ZWS_CFORM.TABLE_OF_ZSFDC_SALES();
            //system.debug('$12'+fDate + clsTable + custNum + tDate);
            cFormResponseElement = clsInstance.ZSFDC_CFORM_SALES(fDate,clsTable,custNum,tDate);
            //system.debug('$1122'+cFormResponseElement);
            if(cFormResponseElement != null){   
                cFormResponses = cFormResponseElement.IT_FINAL;
                if(cFormResponses.item != null){
                    for(SAP_ZWS_CFORM.ZSFDC_SALES z : cFormResponses.item) {
                        if(z != null){
                            allCForms.add(new CFormMapping(z.CUSTOMER, z.CUSTOMER_NAME, z.INVOICE_DATE, z.MONTH, z.INVOICE,z.ITEM, z.MATERIAL, z.MATERIAL_DESCRIPTION, z.CUST_PART_NUMBER, z.PO_NO, z.INVOICE_QTY, z.PRE_TAX, z.POST_TAX, z.TAX_VALUE, z.SALES_OFFICE, z.SELLER_STATE, z.SELLER_TIN_NO,z.MBLNR,z.BUDAT));
                        }
                    }
                }
            }
            
        }
        catch(Exception e){
            system.debug( e.getLineNumber() + e.getMessage());
        }
        return allCForms;
    }
}