global class  DashboardStaging_BatchClass implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query;
        //Date s = system.today();
        query='SELECT ID,Name FROM Dashboard_Staging__c WHERE CreatedDate = today' ;
        system.debug('query:'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<Dashboard_Staging__c> scope){
       delete scope;
        
    }
    global void finish(Database.BatchableContext BC){
         // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: DashboardStaging_BatchClass Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "DashboardStaging_BatchClass" processed ' + a.TotalJobItems + '/n'+' batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus+ '/n' );  
             
               if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }
            
             
    }
    
}