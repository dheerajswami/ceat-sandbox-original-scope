@isTest
private class WB_VerifyPassKey_TestClass {
    
    private static List<Account> acc;
    private static List<Employee_Master__c> empMaster;
    private static final Integer LOOP_VARIABLE = 10;
    
    private  static void init(){
        acc = new List<Account>();
        empMaster = new List<Employee_Master__c>();
        
        
        //for(Integer i = 0;i<LOOP_VARIABLE;i++){
            Account a = new Account(Name = 'Test Acc',KUNNR__c = '55000038',Preferred_Language__c = 'Hindi',Passcode__c  = '1234');
            acc.add(a);
        //}
        
        insert acc;
        
        
            Employee_Master__c e = new Employee_Master__c(EmployeeId__c = '10946527',Name= 'Test emp',Preferred_Language__c = 'English', Passcode__c  = '1235');
            empMaster.add(e);
        
        
        insert empMaster;
        
     
    }
    
    static testMethod void myUnitTest() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '55000038');
        req.addParameter('passKey', '4555');
        req.addParameter('flag', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    }
    static testMethod void myUnitTest1() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '55000038');
        req.addParameter('passKey', '1234');
        req.addParameter('flag', 'D');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    }
   
    static testMethod void myUnitTest2() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '10946527');
        req.addParameter('passKey', '1234');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    }

    static testMethod void myUnitTest3() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '10946527');
        req.addParameter('passKey', '1235');
        req.addParameter('flag', '');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    }
    
    static testMethod void myUnitTest4() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '10946527');
        req.addParameter('passKey', '1235');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    } 
    
    static testMethod void myUnitTest5() {
        
        init();
        WB_VerifyPassKey.customerDetails cusdetails = new WB_VerifyPassKey.customerDetails();
        cusdetails.yORn = 'N';
        cusdetails.preferred_lang = '';
        cusdetails.sfdcCustomer_Code = '';
        cusdetails.passKey= '';
        
        //do request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/VerifyPassKey';  
        req.addParameter('reference_Code', '');
        req.addParameter('passKey', '1235');
        req.addParameter('flag', 'E');

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        cusdetails  = WB_VerifyPassKey.getCustomerDetails();
        
    }
}