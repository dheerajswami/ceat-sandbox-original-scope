@isTest  (seeAllData = false)
private class Sap_BatchForDashbScoreCreation_TestClass{  
    static Account acc;
    
    private class WebServiceMockImpl implements WebServiceMock
    {        
        
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {    
             sapDashboardScore zw = new sapDashboardScore (); 
             sapDashboardScore.ZSFDC_DASHBOARD z = new sapDashboardScore.ZSFDC_DASHBOARD();
             z.YEAR1  = '2015';
             z.MONTH1 = '08';
             z.OWNER_USERNAME = 'test@ceat.in';
             z.INPUT_SCORE   = '10.2';
             z.TOTAL_OUTPUT  = '11.2';
             z.TOTAL_USER    = '21.4';
             z.OWNER_ROLE    = 'CSTL';
             
             sapDashboardScore.ZSFDC_DASHBOARD_SCOREResponse_element res = new sapDashboardScore.ZSFDC_DASHBOARD_SCOREResponse_element();
             sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD vv = new sapDashboardScore.TABLE_OF_ZSFDC_DASHBOARD();
             list<sapDashboardScore.ZSFDC_DASHBOARD> item1 = new list<sapDashboardScore.ZSFDC_DASHBOARD> ();
             item1.add(z);
             vv.item = item1;
             res.IT_INPUT = vv;
                response.put('response_x', res);
            return;
            
        }     
    }
    static testmethod void testLoadData() {
        date d = system.today();
        String month = String.ValueOf(d.month());
        String year = String.ValueOf(d.year());
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        
        
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
        dss.Month__c = month;
        dss.Year__c = year;
        //dss.OwnerID = ;
        dss.Input_Score__c = 12.2;
        dss.Score__c = 1.3;
        insert dss;
        //acc = CEAT_InitializeTestData.createAccount('CustomerTest','50003327');
        //database.insert(acc);
        
        SAPLogin__c saplogin               = new SAPLogin__c();
        saplogin.name='SAP Login';
        saplogin.username__c='sfdc';
        saplogin.password__c='ceat@1234';
        insert saplogin;
        Test.startTest() ;
        Sap_BatchForDashboardScoreCreation nn = new Sap_BatchForDashboardScoreCreation();
        //nn.query                  = 'SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: terriType[0].id LIMIT 100 '; 
        
        ID batchprocessid = database.executebatch(nn,2000); 
        //System.abortJob(batchprocessid);
        Test.stopTest();
    }
   
}