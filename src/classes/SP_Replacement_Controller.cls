global class SP_Replacement_Controller {    
    
    public String loggedInUserName  {get;set;}
    public String loggedInTerritory  {get;set;}
    public Datetime myDatetime ;
    public String myDatetimeStr {get;set;}
    list<UserTerritory2Association> userTerrCode;
    list<Territory2> userTerritory;
    
    global  SP_Replacement_Controller() { 
        myDatetime = Datetime.now();
        userTerrCode = new list<UserTerritory2Association>();
        userTerritory = new list<Territory2>();
        myDatetimeStr = myDatetime.format('MMMM, yyyy');
        
        userTerrCode=[select territory2Id,id from UserTerritory2Association where UserId=:UserInfo.getUserId() limit 1];
        if(userTerrCode.size() > 0){
            userTerritory=[select name, id from Territory2 where id=:userTerrCode[0].Territory2Id];
        }
        system.debug(userTerritory+'userTerritory');
        if(userTerritory.size() > 0){
            loggedInTerritory=userTerritory[0].Name;
        }
        system.debug(loggedInTerritory+'loggedInTerritory');
        
    } 
    
    //Used to get list of TL or Dealer depending on which user logged in    
    @RemoteAction
    Public Static List<SP_HandlerForSP_Replacement_Controller.tlWrapper> getTLsOfRM () {
    //Public Static void getTLsOfRM () {
            SP_HandlerForSP_Replacement_Controller handler = new SP_HandlerForSP_Replacement_Controller();
            return handler.getRMWiseSalesPlanningRecords();
    }
    @RemoteAction
    Public Static void saveTlSpRecord (Map<String,String> mapOfSalesPlanning) {
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String repTLLabel       = System.Label.Replacement_TL;
            String repROLabel       = System.Label.Replacement_RO;
            String rmLabel          = System.Label.RM_Role;
            String tlLabel          = System.Label.TL_Role;
            String tldLabel = System.Label.TLD_Role; 
            String rbmLabel          = System.Label.RBM_Role;
            String loggedInUserTerritory;
            
            Map<String,String> mapOfTLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfCatAndROId                  = new map<String,ID>();
            
            Map<String,String> mapOfRegAndRegCode           = new map<String,String>();
            Map<String,String> mapOfTerritoryAndUserID          = new Map<String,String>();
            Map<String,String> mapOfCatCodeAndCatName        = new map<String,String>();
            
            List<Sales_Planning__c> listOfSalesPlanningTL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningTLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> regionForRM             = new List<UserTerritory2Association>();
            List<Territory2> allTlsTerritoryUnderRM                 = new List<Territory2>();
            List<UserTerritory2Association> allTLsUsers             = new List<UserTerritory2Association>();
            List<UserTerritory2Association> rbmUsers                = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningTLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningRO                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> listOfTerr                             = new List<Territory2>();
            List<User>  allTLsUseremails                            = new List<User>();
            List<String>  allTLsUseremailIds                            = new List<String>();
            
            Sales_Planning__c spTLRecord;
            Id replaceTLId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
            Id replaceROId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
            
            Set<String> salesPlanningTLId           = new Set<String>();
            Set<String> regionCode                  = new Set<String>();
            Set<Id> regionId                        = new Set<Id>();
            Set<Id> allTLsRegionIds                 = new Set<Id>();
            List<String>allTLUserIds                    = new List<String>();
            
            regionForRM = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: rmLabel ];
            for(UserTerritory2Association reg : regionForRM){
                regionId.add(reg.Territory2Id);
                regionCode.add(reg.Territory2.name);
            }
                 loggedInUserTerritory=regionForRM[0].Territory2.name;
            allTlsTerritoryUnderRM =[select id,DeveloperName,ParentTerritory2Id from Territory2 where ParentTerritory2Id IN: regionId];
            
            for(Sales_Planning_Categories__c spc :[Select Name, Category_Code__c, Include_in_Sales_Planning__c, Sort_Order__c from Sales_Planning_Categories__c where Include_in_Sales_Planning__c = true]) {
              
               mapOfCatCodeAndCatName.put((spc.Name).trim(),spc.Category_Code__c);                            
            } 
            for(Territory2 terr:allTlsTerritoryUnderRM){
                allTLsRegionIds.add(terr.id);
            }
          
            rbmUsers=[Select Territory2Id,UserID from UserTerritory2Association Where RoleInTerritory2=: rbmLabel AND Territory2Id IN:regionId]; 
             system.debug('rbmUsers'+rbmUsers);
            if(allTLsRegionIds.size()>0){
             allTLsUsers=[Select Territory2Id,UserID from UserTerritory2Association Where RoleInTerritory2=: tlLabel AND Territory2Id IN:allTLsRegionIds]; 
            }
            if(allTLsUsers.size()>0){
                for(UserTerritory2Association user:allTLsUsers){
                    allTLUserIds.add(user.UserID);
                }
            }
            if(rbmUsers.size()>0){
                for(UserTerritory2Association user:rbmUsers){
                    allTLUserIds.add(user.UserID);
                }
            }
            if(allTLUserIds.size()>0){
                allTLsUseremails=[Select id,email from User where id IN:allTLUserIds];
            }
            if(allTLsUseremails.size()>0){
                for(User u:allTLsUseremails){
                    allTLsUseremailIds.add(u.email);
                }
            }
            system.debug('allTLsUseremailIds'+allTLsUseremailIds);
            if(regionId.size() > 0){
                    listOfTerr = [select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId,(Select Territory2Id,UserID from UserTerritory2Associations Where RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) from Territory2 where ParentTerritory2Id IN: regionId];
            }
            if(listOfTerr.size() > 0){
                for(Territory2 temp : listOfTerr){
                        for(UserTerritory2Association s : temp.UserTerritory2Associations){
                            mapOfTerritoryAndUserID.put(temp.Name,s.UserID);
                        }
                }
            }
            salesPlanningRO = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: replaceROId];
            
            for(Sales_Planning__c temp :salesPlanningRO){
                    //String capCat = temp.Category_Description__c.toUpperCase();
                    mapOfCatAndROId.put(temp.Category__c,temp.id);
                    //mapOfCatAndCatCode.put((temp.Category_Description__c).trim(),temp.Category__c);
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    String ownerIDTL;
                    system.debug(listKey+'^^^');
                    if(listKey[0] != 'undefined'){
                            salesPlanningTLId.add(listKey[0]);
                            mapOfTLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                    }
                    else{
                            st = listKey[1] + listKey[3] + month +year ;
                           
                            if(mapOfTerritoryAndUserID.get(listKey[1]) == null ){
                                ownerIDTL = userInfo.getUserID();
                            }else{
                                ownerIDTL = mapOfTerritoryAndUserID.get(listKey[1]) ;
                            }
                            spTLRecord = new Sales_Planning__c(OwnerId = mapOfTerritoryAndUserID.get(listKey[1]),Parent_Sales_Planning__c = mapOfCatAndROId.get(listKey[3]),Category__c = listKey[3],Category_Description__c = listKey[2],
                            Month__c = month ,Year__c = year ,Region_code__c = regionForRM[0].Territory2.name,RecordTypeId = replaceTLId, Territory_Code__c = listKey[1],Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st);
                            listOfSalesPlanningTLForInsert.add(spTLRecord);
                            system.debug(st+'$$$$$');
                            system.debug(spTLRecord.OwnerId+'$$$$$');
                            system.debug(spTLRecord.Parent_Sales_Planning__c+'$$$$$');
                    }
            }
            
            listOfSalesPlanningTL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningTLId];
            if(listOfSalesPlanningTL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningTL){
                            String val = mapOfTLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Total_planned__c = Decimal.valueOf(val);
                            upsertsalesPlanningTLList.add(sp);
                    }
            }
            if(upsertsalesPlanningTLList.size() > 0){
                    upsert upsertsalesPlanningTLList SPExternalIDTL__c;
            }
            if(listOfSalesPlanningTLForInsert.size() > 0){
                    upsert listOfSalesPlanningTLForInsert SPExternalIDTL__c;
            }
            
            alreadyLocked = [SELECT id,name,Submitted__c,Month__c,User__c,Year__c from Locking_Screen__c WHERE User__c =:UserInfo.getUserId() AND Month__c =: month AND Year__c =: year AND Submitted__c = true AND Status__c='Submitted'];
            if(!(alreadyLocked.size() > 0)){
                Locking_Screen__c lsforRM= new Locking_Screen__c(User__c = UserInfo.getUserId(),Month__c = month ,Year__c = year,Submitted__c = true,Status__c='Submitted',Territory_Code__c=loggedInUserTerritory);
                insert lsforRM;
            }
            
             //By submitting Sales planning, email will go to RBM and to all TLs under that RM
            if(allTLUserIds.size()>0){
                Messaging.SingleEmailMessage mailHandler =new Messaging.SingleEmailMessage();
                String name=UserInfo.getName(); 
               /* EmailTemplate emailTemp=new EmailTemplate ();
                emailTemp=[SELECT id,Name FROM EmailTemplate where name = 'Sales planning Submitted by RM' limit 1];
                //Sales planning Submitted by RM
                mailHandler.setTargetObjectIds(allTLUserIds);
                mailHandler.saveAsActivity = false;
                mailHandler.setTemplateId(emailTemp.id);
                //mailHandler.setWhatIds(allTLUserIds);
                system.debug('mailHandler'+mailHandler);*/
                        String body = 'Hi, <br><br>';
                        body+='Sales planning has been submitted for the month '+month+', '+year+' by Regional Manager of '+loggedInUserTerritory+'<br><br>';
                        body+='Regards,<br><br>'+name;
                        body+='<br>';
                        body+='Regional Manager | '+loggedInUserTerritory+'<br><br>';
                        mailHandler.setToAddresses(allTLsUseremailIds);
                        mailHandler.setHtmlBody(body);
                        mailHandler.setReplyTo(UserInfo.getUserEmail());
                        mailHandler.setSubject('Sales planning has been submitted by RM for month '+month+', '+year);
                
                
                try{
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mailHandler});
                }
                catch(Exception e){
                }
            }
            
    }
    @RemoteAction
    Public Static void saveAsDraftTlSpRecord (Map<String,String> mapOfSalesPlanning) {
            Date d = system.today();
            String month            = String.valueOf(d.month());
            String year             = String.valueOf(d.Year());
            String repTLLabel       = System.Label.Replacement_TL;
            String repROLabel       = System.Label.Replacement_RO;
            String rmLabel          = System.Label.RM_Role;
            String tlLabel          = System.Label.TL_Role;
             String tldLabel = System.Label.TLD_Role; 
            Map<String,String> mapOfTLIDAndPlanned          = new Map<String,String>();
            Map<String,ID> mapOfCatAndROId                  = new map<String,ID>();
            
            Map<String,String> mapOfRegAndRegCode           = new map<String,String>();
            Map<String,String> mapOfTerritoryAndUserID          = new Map<String,String>();
            Map<String,String> mapOfCatCodeAndCatName        = new map<String,String>();
            
            
            List<Sales_Planning__c> listOfSalesPlanningTL           = new List<Sales_Planning__c>();
            List<Sales_Planning__c> listOfSalesPlanningTLForInsert  = new List<Sales_Planning__c>();
            List<UserTerritory2Association> regionForRM             = new List<UserTerritory2Association>();
            List<Sales_Planning__c> upsertsalesPlanningTLList       = new List<Sales_Planning__c>();
            List<Sales_Planning__c> salesPlanningRO                 = new List<Sales_Planning__c>();
            List<Locking_Screen__c> alreadyLocked                   = new list<Locking_Screen__c>(); 
            List<Territory2> listOfTerr                             = new List<Territory2>();
            
            Sales_Planning__c spTLRecord;
            Id replaceTLId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repTLLabel Limit 1].Id;
            Id replaceROId = [select id,DeveloperName from RecordType where SobjectType='Sales_Planning__c' and DeveloperName =: repROLabel Limit 1].Id;
            
            Set<String> salesPlanningTLId           = new Set<String>();
            Set<String> regionCode                  = new Set<String>();
            Set<Id> regionId                        = new Set<Id>();
            
            regionForRM = [Select IsActive,RoleInTerritory2,Territory2Id,Territory2.name,UserId,User.name,ID from UserTerritory2Association where UserId =:UserInfo.getUserId() and RoleInTerritory2=: rmLabel ];
            for(UserTerritory2Association reg : regionForRM){
                regionId.add(reg.Territory2Id);
                regionCode.add(reg.Territory2.name);
            }
            if(regionId.size() > 0){
                    listOfTerr = [select id,AccountAccessLevel,ContactAccessLevel,Description,DeveloperName,Name,ParentTerritory2Id,Territory2ModelId,Territory2TypeId,(Select Territory2Id,UserID from UserTerritory2Associations Where RoleInTerritory2=: tlLabel OR RoleInTerritory2=: tldLabel) from Territory2 where ParentTerritory2Id IN: regionId];
            }
            if(listOfTerr.size() > 0){
                for(Territory2 temp : listOfTerr){
                        for(UserTerritory2Association s : temp.UserTerritory2Associations){
                            mapOfTerritoryAndUserID.put(temp.Name,s.UserID);
                        }
                }
            }
            for(Sales_Planning_Categories__c spc :[Select Name, Category_Code__c, Include_in_Sales_Planning__c, Sort_Order__c from Sales_Planning_Categories__c where Include_in_Sales_Planning__c = true]) {
              
               mapOfCatCodeAndCatName.put((spc.Name).trim(),spc.Category_Code__c);                            
            } 
            salesPlanningRO = [SELECT Id,ASP__c,Budget__c,Dealer__c,RecordTypeId,Category__c,SYS_TL_CAT__c,Category_Description__c,Dealer_CustNumber__c,Dealer_Name__c,L3M__c,LYCM__c,Month__c,Region_code__c,Region_Description__c,Target_Quantity__c,Territory_Code__c,Year__c FROM Sales_Planning__c WHERE Region_code__c IN:regionCode AND Year__c =:year AND Month__c =:month AND RecordTypeId =: replaceROId];
            
            for(Sales_Planning__c temp :salesPlanningRO){
                    //String capCat = temp.Category_Description__c.toUpperCase();
                    mapOfCatAndROId.put(temp.Category__c,temp.id);
                    //mapOfCatAndCatCode.put(capCat,temp.Category__c);
            }
            for(String temp : mapOfSalesPlanning.keySet()){
                    String key = temp;
                    String st = '';
                    system.debug(key+'&&&');
                    String[] listKey = key.split('@@');
                    system.debug(listKey+'^^^');
                    String ownerIDTL;
                    if(listKey[0] != 'undefined'){
                            salesPlanningTLId.add(listKey[0]);
                            mapOfTLIDAndPlanned.put(listKey[0],mapOfSalesPlanning.get(temp));
                    }
                    else{
                            st = listKey[1] + listKey[3] + month +year ;
                            
                            //system.debug('listKey[2]---------'+listKey[2]);
                            //system.debug('mapOfCatCodeAndCatName.get(listKey[2].trim())-------'+mapOfCatCodeAndCatName.get(listKey[2].trim()));
                            
                            if(mapOfTerritoryAndUserID.get(listKey[1]) == null ){
                                ownerIDTL = userInfo.getUserID();
                            }else{
                                ownerIDTL = mapOfTerritoryAndUserID.get(listKey[1]) ;
                            }
                            
                            spTLRecord = new Sales_Planning__c(OwnerId = ownerIDTL ,Parent_Sales_Planning__c = mapOfCatAndROId.get(listKey[3]),Category__c = listKey[3],Category_Description__c = listKey[2],
                            Month__c = month ,Year__c = year ,Region_code__c = regionForRM[0].Territory2.name,RecordTypeId = replaceTLId, Territory_Code__c = listKey[1],Total_planned__c = Decimal.valueOf(mapOfSalesPlanning.get(temp)),SPExternalIDTL__c = st);
                            listOfSalesPlanningTLForInsert.add(spTLRecord);
                            system.debug(st+'$$$$$');
                            system.debug(spTLRecord.OwnerId+'$$$$$');
                            system.debug(spTLRecord.Parent_Sales_Planning__c+'$$$$$');
                    }
            }
            
            listOfSalesPlanningTL = [SELECT id,SPExternalIDTL__c,Total_planned__c from Sales_Planning__c WHERE SPExternalIDTL__c IN: salesPlanningTLId];
            if(listOfSalesPlanningTL.size() > 0){
                    for(Sales_Planning__c sp : listOfSalesPlanningTL){
                            String val = mapOfTLIDAndPlanned.get(sp.SPExternalIDTL__c);
                            sp.Total_planned__c = Decimal.valueOf(val);
                            upsertsalesPlanningTLList.add(sp);
                    }
            }
            if(upsertsalesPlanningTLList.size() > 0){
                    upsert upsertsalesPlanningTLList SPExternalIDTL__c;
            }
            if(listOfSalesPlanningTLForInsert.size() > 0){
                    upsert listOfSalesPlanningTLForInsert SPExternalIDTL__c;
            }
    }
}