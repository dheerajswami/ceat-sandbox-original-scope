/*+----------------------------------------------------------------------
||         Author:  Vivek Deepak
||
||        Purpose:To Update Account Data for Mobile 
||
||    Modefied By: Fetch Mobile Data for PJP App
|| 
||          Reason:    
||
++-----------------------------------------------------------------------*/

@RestResource(urlMapping='/FetchPJPMobileDataDummy/*')

global with sharing class MB_PJP_MobileDataDummy {
    
    global class MobileData{
        public List<Visits_Plan__c> visitsPlans {get;set;}
        public List<Account> accounts {get;set;}
        public List<Task> tasks {get;set;}
        public List<String> actionToBeTaken {get;set;}
        public List<User> users {get;set;}
        public String timeStamp {get;set;}
        public String UserType {get;set;}
        public List<DSE_Beat__c> dse_beats {get;set;}
        
        public MobileData(){
            tasks           = new List<Task>();
            accounts        = new List<Account>(); 
            visitsPlans     = new List<Visits_Plan__c>();
            actionToBeTaken = new List<String>(); 
            users           = new List<User>();
            dse_beats       = new List<DSE_Beat__c>();
        }
    }
    
    global class MobileDataRequest{
        public List<Visits_Plan__c> visitsPlans {get;set;}
        public List<Task> tasks {get;set;}
        
        public MobileDataRequest(){
            tasks           = new List<Task>();            
            visitsPlans     = new List<Visits_Plan__c>();
        }
        
    }
    
    @HttpPost
    global static MobileData getMobileDataPJP(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String dt = req.params.get('TimeStamp');
        DateTime appDateString = UtilityClass.correctDateTime(dt);
       
        
        try{
            Map<Integer, String> monthsString = new Map<Integer, String>{1 => 'January', 2 => 'February',3 => 'March',4 => 'April',5 => 'May',6 => 'June',7 => 'July',8 => 'August',9 => 'September',10 => 'October',11 => 'November',12 => 'December'};
                try{               
                    MobileDataRequest mobReq = new MobileDataRequest();
                    
                    mobReq = (MobileDataRequest)JSON.deserialize(req.requestBody.toString(),MobileDataRequest.class);
                    //system.debug('RESP' + mobReq);
                    if(mobReq.visitsPlans != null){
                        Integer mn = date.today().month();
                        String yr = String.valueOf(date.today().year());
                        PJP__c pjp = new PJP__c();
                        Boolean noPJPflag = false;
                        try{
                        pjp = [SELECT Id FROM PJP__c WHERE Visit_Plan_Month__c =:monthsString.get(mn) AND Visit_Plan_Year__c =:yr AND OwnerId =:userInfo.getUserId() LIMIT 1];
                            noPJPflag = true;    
                        }catch(QueryException e){
                            noPJPflag = false; 
                        }    
                        List<Visits_Plan__c> visitToCreate = new List<Visits_Plan__c>();
                        List<Visits_Plan__c> visitToUpdate = new List<Visits_Plan__c>();
                        List<Account> accounts = new List<Account>();
                        Set<Id> newVisitId = new Set<Id>();
                        
                        for(Visits_Plan__c tsk : mobReq.visitsPlans){
                            String tId = String.valueOf(tsk.Id);
                            
                            if(tId.startsWith('LOCAL_') && noPJPflag){
                                tsk.Id = null;
                                visitToCreate.add(tsk);
                                if(tsk.Master_Location__c){
                                    accounts.add(new Account(Id = tsk.Dealer__c,Geolocation__Latitude__s = tsk.Geolocation__Latitude__s,Geolocation__Longitude__s = tsk.Geolocation__Longitude__s));
                                }
                            }else{
                                if(tsk.Master_Location__c){
                                    accounts.add(new Account(Id = tsk.Dealer__c,Geolocation__Latitude__s = tsk.Geolocation__Latitude__s,Geolocation__Longitude__s = tsk.Geolocation__Longitude__s));
                                }
                                visitToUpdate.add(tsk);
                            }
                        }
                        
                        
                        Database.update(visitToUpdate);
                        Database.update(accounts);
                        
                        for(Visits_Plan__c v : visitToCreate){
                            v.Master_Plan__c = pjp.Id;
                        }
                        Database.SaveResult[] srv = Database.insert(visitToCreate);
                        for(Database.SaveResult s : srv){
                            newVisitId.add(s.getId());
                        }
                    }
                    
                    if(mobReq.tasks != null){
                        List<Task> taskToCreate = new List<Task>();
                        List<Task> taskToUpdate = new List<Task>();
                        for(Task tsk : mobReq.tasks){
                            String tId = String.valueOf(tsk.Id);
                            if(tId.startsWith('LOCAL_')){
                                tsk.Id = null;
                                taskToCreate.add(tsk);
                            }else{
                                taskToUpdate.add(tsk);
                            }
                        }
                        
                        Database.update(taskToUpdate);            
                        Database.insert(taskToCreate); 
                    }
                }catch(Exception e){
                    system.debug('TOP' + e.getLineNumber() + ' Error -'+ e.getMessage()+ ' Cause' + e.getCause());
                }
            
            List<Visits_Plan__c> vp     = new List<Visits_Plan__c>();
            List<Account> acc           = new List<Account>();
            List<Task> tks              = new List<Task>();
            List<User> urList           = new List<User>();
            List<DSE_Beat__c> dseB      = new List<DSE_Beat__c>();
            
            String userRole     = [ SELECT Id,Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
            String[] pickVals = new List<String>();
            
            if(appDateString == null){                
                if(userRole.contains('TLD')){
                    String territoryCode = [select Territory2.Name from UserTerritory2Association where RoleInTerritory2 = 'TLD' AND UserId = :UserInfo.getUserId()].Territory2.Name;
                    //String userTLCode   = [SELECT Id,EmployeeNumber FROM User WHERE Id =: UserInfo.getUserId()].EmployeeNumber;
                    Set<Id> accountIds = new Set<Id>();
                    for( DSE_Beat__c ds : [SELECT Id,Beat_Code__c,Distributor__c,Distributor_Code__c,Distributor_Name__c,DSE_Code__c,DSE_Name__c FROM DSE_Beat__c WHERE TLD_Territory_Code__c =:territoryCode AND Distributor__c != null]){
                        dseB.add(ds);
                        accountIds.add(ds.Distributor__c);
                    }
                    
                    //system.debug(dseB);
                    Date todayD = Date.today();
                    Date nextSevenDay = todayD.addDays(-7);
                    
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,
                        (SELECT Id,Offline_Id__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c 
                            FROM Visit_Plans__r WHERE Visit_Day__c > :nextSevenDay AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved')
                            FROM Account WHERE Id IN :accountIds order by Name];     
                    
                    for(Account a : acc){
                        if(a.Visit_Plans__r != null){
                            vp.addAll(a.Visit_Plans__r);
                        }
                    }
                    
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                        FROM Task 
                        WHERE (WhatId IN : vp OR WhatId IN : acc OR Status = 'Open') AND OwnerId =:UserInfo.getUserId()];
                    
                    pickVals = Label.Action_To_Be_Replacement.split(';'); 
                        
                }else if(userRole.contains('Specialty')){
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account order by Name];    


                    vp  = [SELECT Id,Offline_Id__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Visit_Day__c >= :Date.today() AND Visit_Day__c <= :Date.today().addDays(7) Order by Visit_Day__c];

                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                        FROM Task 
                        WHERE OwnerId =:UserInfo.getUserId()];
                        
                    pickVals = Label.Action_To_Be_Taken_Specialty.split(';');    

                }else{
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c,(SELECT Id,Offline_Id__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visit_Plans__r WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' Order by Visit_Day__c desc LIMIT 10) FROM Account order by Name];    
                    
                    for(Account a : acc){
                        if(a.Visit_Plans__r != null){
                            vp.addAll(a.Visit_Plans__r);
                        }
                    }
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc OR Status = 'Open') AND OwnerId =:UserInfo.getUserId()];
                    
                    pickVals = Label.Action_To_Be_Replacement.split(';'); 
                }                        
                urList = getAllUsers(UserInfo.getUserId()); 
            }
            else{
                if(userRole.contains('TLD')){
                    String territoryCode = [select Territory2.Name from UserTerritory2Association where RoleInTerritory2 = 'TLD' AND UserId = :UserInfo.getUserId()].Territory2.Name;
                    //String userTLCode   = [SELECT Id,EmployeeNumber FROM User WHERE Id =: UserInfo.getUserId()].EmployeeNumber;
                    Set<Id> accountIds = new Set<Id>();
                    for( DSE_Beat__c ds : [SELECT Id,Beat_Code__c,Distributor__c,Distributor_Code__c,Distributor_Name__c,DSE_Code__c,DSE_Name__c FROM DSE_Beat__c WHERE TLD_Territory_Code__c =:territoryCode AND Distributor__c != null]){
                        dseB.add(ds);
                        accountIds.add(ds.Distributor__c);
                    }
                    
                    
                    
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account WHERE Id IN :accountIds OR LastModifiedDate >:appDateString order by Name];                                                 
                    
                    vp = [SELECT Id,Offline_Id__c,Sub_Dealers_Met__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE LastModifiedDate >:appDateString AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved'];
                    
                    //tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc) AND OwnerId =:UserInfo.getUserId() AND LastModifiedDate >:appDateString ];
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate >:appDateString ];
                }else if(userRole.contains('Specialty')){
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account
                        WHERE LastModifiedDate >:appDateString order by Name];    

                        Date todayD = Date.today();
                        Date nextSevenDay = todayD.addDays(7);

                    vp  = [SELECT Id,Offline_Id__c,Dealer__c,Dealer__r.Name,Dealer__r.Town__c,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Sub_Dealers_Met__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE Master_Plan__r.Sys_ApprovalStatus__c = 'Approved' AND Visit_Day__c >= :Date.today() AND Visit_Day__c <= :Date.today().addDays(7) AND LastModifiedDate > :appDateString Order by Visit_Day__c];

                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c 
                        FROM Task 
                        WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                }else{
                    acc = [SELECT Id,UniqueIdentifier__c,Customer_Segment__c,Geolocation__latitude__s,Geolocation__longitude__s,Customer_Group__c,TIN_Number__c,Active__c,Fax,Name,Business_Units__c,KUNNR__c,Distribution_Channel_Text__c,Division_Text__c,Sales_District_Text__c,Sales_Group_Text__c,Sales_Office_Text__c,PARNR__c,LAND1_GP__c,District__c,AD_SMTPADR__c,PAN_Number__c,AD_TLNMBR__c,Tax_Code__c,Town__c,PIN_code__c,State__c,Address__c FROM Account WHERE LastModifiedDate > :appDateString order by Name];  
                    
                    
                    
                    vp = [SELECT Id,Offline_Id__c,Sub_Dealers_Met__c,Dealer__r.Town__c,Dealer__c,Dealer__r.Name,Dealer__r.UniqueIdentifier__c,Dealer__r.Customer_Group__c,Dealer__r.Customer_Segment__c,Dealer__r.Sales_District_Text__c,Visit_Day__c,Check_In__c,Status__c,Geolocation__c,Record_Type_Name__c,Type_of_Influencer__c,Type_of_Day__c FROM Visits_Plan__c WHERE LastModifiedDate > :appDateString AND Master_Plan__r.Sys_ApprovalStatus__c = 'Approved'];
                    
                    //tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE (WhatId IN : vp OR WhatId IN : acc) AND OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                    tks = [SELECT Id,Offline_Id__c,OwnerId,Subject,Customer_Name__c,Assign_To_Name__c,Action_to_be_taken__c,WhatId,Status,WhoId,ActivityDate,Description,Source__c,Date_of_Reporting__c,SAP_Code__c,Territory__c,Segment__c,Commitment_sheet_signed__c,Assign_Escalate__c,Conclusion__c,Actual_End_Date__c FROM Task WHERE OwnerId =:UserInfo.getUserId() AND LastModifiedDate > :appDateString];
                    
                }                        
                urList = getAllUsers(UserInfo.getUserId()); 
            }
            
            /*
            Populate the data into the wrapper 
            */
            MobileData mobileData = new MobileData();
            mobileData.accounts     = acc;
            mobileData.visitsPlans  = vp;
            mobileData.tasks = tks;
            
            for(String str : pickVals){
                mobileData.actionToBeTaken.add(str);
            }
            
            mobileData.users = urList;
            mobileData.UserType = userRole;
            mobileData.timeStamp = DateTime.now().format('dd/MM/yyyy hh:mm aaa');
            
            return mobileData;
        }catch(Exception e){
            system.debug(e.getLineNumber() + ' Error -'+ e.getMessage()+ ' Cause' + e.getCause());
            try{
                Error_Log__c errorLog = new Error_Log__c(Class_Name__c = 'MB_PJP_MobileDataDummy',Line_Number__c = Integer.valueOf(e.getLineNumber()),Error_Message__c = e.getMessage());
                insert errorLog;
            }catch(Exception ex){
                system.debug(ex.getLineNumber() + ' Error -'+ ex.getMessage()+ ' Cause' + ex.getCause());
            }
            
            return null;
        }
        
        
    }
    
    /*
* Fetch all users who are above in hierachy
*/
    global static List<User> getAllUsers(Id userId){
        
        Set<Id> userIds = new Set<Id>();
        Set<Id> territoriesId = new Set<Id>();
        List<UserTerritory2Association> territoryId = new List<UserTerritory2Association>();
        territoryId = [SELECT Territory2Id FROM UserTerritory2Association WHERE UserId = :userId];
        for(UserTerritory2Association uA : territoryId){
            territoriesId.add(uA.Territory2Id);
        }
        
        List<Territory2> territoryList = new List<Territory2>();        
        territoryList = [SELECT Id, DeveloperName, ParentTerritory2Id, ParentTerritory2.ParentTerritory2Id FROM Territory2 WHERE Id IN :territoriesId];
        
        for(Territory2 tr : territoryList){
            territoriesId.add(tr.ParentTerritory2Id);
            territoriesId.add(tr.ParentTerritory2.ParentTerritory2Id);
        }
        
        for(UserTerritory2Association uMap : [SELECT Id,Territory2Id,UserId FROM UserTerritory2Association WHERE Territory2Id IN :territoriesId OR Territory2.DeveloperName ='HO_Replacement' ]){
            userIds.add(uMap.UserId);
        }
        
        
        
        
        return [SELECT Id,Name FROM User WHERE isActive = true AND Id IN :userIds];
    }
    
}