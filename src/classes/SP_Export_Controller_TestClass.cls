@isTest 
private class SP_Export_Controller_TestClass{
    static testmethod void testLoadData() {
        Territory2Model terrModel = new Territory2Model();
        Territory2 terrtypeBUExport = new Territory2 ();
        Territory2 terrtypeClusterExportAN03 = new Territory2 ();
        Territory2 terrtypeCountryAN03AU = new Territory2 ();
        Territory2 terrtypeCountryAN03BMA = new Territory2 ();
        Territory2 terrtypeCountryAN03CN = new Territory2 ();
        Territory2 terrtypeCountryAN03FJ = new Territory2 ();
        Territory2 terrtypeCountryAN03HK = new Territory2 ();
        Territory2 terrtypeCountryAN03JP = new Territory2 ();
        Territory2 terrtypeCountryAN03KH = new Territory2 ();
        Territory2 terrtypeCountryAN03KR = new Territory2 ();
        Territory2 terrtypeCountryAN03LK = new Territory2 ();
        Territory2 terrtypeCountryAN03MM = new Territory2 ();
        Territory2 terrtypeCountryAN03MY = new Territory2 ();
        Territory2 terrtypeCountryAN03NZ = new Territory2 ();
        Territory2 terrtypeCountryAN03PH = new Territory2 ();
        Territory2 terrtypeCountryAN03TH = new Territory2 ();
        Territory2 terrtypeCountryAN03VN = new Territory2 ();
        Set<id> setOFExportDealerID = new Set<ID>();
        Map<String,String> mapOfSalesPlanning = new Map<String,string>();
        String teststring = '';
        
        List<Sales_Planning__c> listExportDealer = new List<Sales_Planning__c>();
        user userSrManagerExportData = new user();
        user userSrManagerExportDataBU = new user();
        String tlPageName   = System.Label.ExportPage;
        UserTerritory2Association srManagerExportuser = new UserTerritory2Association ();
        UserTerritory2Association srManagerExportuserBU = new UserTerritory2Association();
        Account accrecord  = new account();
        Account accrecord1  = new account();
        List<Sales_Planning__c> listOFSPExpFor = new List<Sales_Planning__c>();
        List<SP_HandlerFor_SP_Export_Controller.DealerWrapper> Sprecords = new List<SP_HandlerFor_SP_Export_Controller.DealerWrapper>();
        List<Sales_Planning__c> listOFSPExportDealer  = new List<Sales_Planning__c>();
        id terrTypeBU ;
        id terrTypeCluster ;
        id terrTypeCountry ;
        ACW__c acw=new ACW__C();
        ACW__c unlAcw=new ACW__C();
        String exportApproverLabel  = System.Label.Export_Approver;
        //id terrTypeZone ;
         // Load the test Sales planning Staging from the static resource
        terrModel = CEAT_InitializeTestData.createTerritoryModel('Ceatv1','Active');
        insert terrModel;
        
        List<sObject> listTerritoryType = Test.loadData(Territory2Type.sObjectType, 'TerritoryType');
        system.debug(listTerritoryType+'listTerritoryType');
        for(sObject temp: listTerritoryType){
            Territory2Type t =(Territory2Type)temp;
            if(t.DeveloperName == 'BUTest'){
                terrTypeBU = t.id;
            }
            if(t.DeveloperName == 'ClusterTest'){
                terrTypeCluster = t.id;
            }
            if(t.DeveloperName == 'CountryTest'){
                terrTypeCountry = t.id;
            }
        }
        system.debug(terrTypeBU+'terrTypeBU');
        //Created BU Territory
        terrtypeBUExport =  CEAT_InitializeTestData.createTerritory('ExportTest','ExportTest',terrTypeBU,terrModel.id,null);
        insert terrtypeBUExport;
        //Created Cluster Territory Export
        terrtypeClusterExportAN03 =  CEAT_InitializeTestData.createTerritoryCountry('AN03','AN03_Rep',terrTypeCluster,terrModel.id,terrtypeBUExport.id,'Far East2');
        insert terrtypeClusterExportAN03;
        //Created Country Territory Export
        terrtypeCountryAN03AU =  CEAT_InitializeTestData.createTerritoryCountry('AU','AU_Exp',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'AU_Exp');
        insert terrtypeCountryAN03AU;
        terrtypeCountryAN03BMA =  CEAT_InitializeTestData.createTerritoryCountry('BMA','BMA_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'BMA_test');
        insert terrtypeCountryAN03BMA;
        terrtypeCountryAN03CN =  CEAT_InitializeTestData.createTerritoryCountry('CN','CN_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'CN_test');
        insert terrtypeCountryAN03CN;
        terrtypeCountryAN03FJ =  CEAT_InitializeTestData.createTerritoryCountry('FJ','FJ_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'FJ_test');
        insert terrtypeCountryAN03FJ;
        terrtypeCountryAN03HK =  CEAT_InitializeTestData.createTerritoryCountry('HK','HK_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'HK_test');
        insert terrtypeCountryAN03HK;
        terrtypeCountryAN03JP =  CEAT_InitializeTestData.createTerritoryCountry('JP','JP_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'JP_test');
        insert terrtypeCountryAN03JP;
        terrtypeCountryAN03KH =  CEAT_InitializeTestData.createTerritoryCountry('KH','KH_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'KH_test');
        insert terrtypeCountryAN03KH;
        terrtypeCountryAN03KR =  CEAT_InitializeTestData.createTerritoryCountry('KR','KR_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'KR_test');
        insert terrtypeCountryAN03KR;
        terrtypeCountryAN03LK =  CEAT_InitializeTestData.createTerritoryCountry('LK','LK_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'LK_test');
        insert terrtypeCountryAN03LK;
        terrtypeCountryAN03MM =  CEAT_InitializeTestData.createTerritoryCountry('MM','MM_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'MM_test');
        insert terrtypeCountryAN03MM;
        terrtypeCountryAN03MY =  CEAT_InitializeTestData.createTerritoryCountry('MY','MY_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'MY_test');
        insert terrtypeCountryAN03MY;
        terrtypeCountryAN03NZ =  CEAT_InitializeTestData.createTerritoryCountry('NZ','NZ_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'NZ_test');
        insert terrtypeCountryAN03NZ;
        terrtypeCountryAN03PH =  CEAT_InitializeTestData.createTerritoryCountry('PH','PH_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'PH_test');
        insert terrtypeCountryAN03PH;
        terrtypeCountryAN03TH =  CEAT_InitializeTestData.createTerritoryCountry('TH','TH_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'TH_test');
        insert terrtypeCountryAN03TH;
        terrtypeCountryAN03VN =  CEAT_InitializeTestData.createTerritoryCountry('VN','VN_test',terrTypeCountry,terrModel.id,terrtypeClusterExportAN03.id,'VN_test');
        insert terrtypeCountryAN03VN;
        //create user for cluster
        userSrManagerExportData = CEAT_InitializeTestData.createUser('mm','TestExport','kishlay.mathur@extentor.com','kishlayExport@ceat.com','239','Exports',null,'Sr_Manager_Exports');
        insert userSrManagerExportData;
        srManagerExportuser = CEAT_InitializeTestData.createUserTerrAsso(userSrManagerExportData.id, terrtypeClusterExportAN03.id, 'Sr_Manager_Exports');
        insert srManagerExportuser;
        userSrManagerExportDataBU = CEAT_InitializeTestData.createUser('mm','TestExportBU','kishlay.mathur@extentor.com','kishlayExportBU@ceat.com','239','Exports',null,'VP_Exports');
        insert userSrManagerExportDataBU;
        srManagerExportuserBU = CEAT_InitializeTestData.createUserTerrAsso(userSrManagerExportDataBU.id, terrtypeBUExport.id, exportApproverLabel);
        insert srManagerExportuserBU;
        System.runAs(userSrManagerExportData) {
            Id lockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName ='Locking' Limit 1].Id;
            Id unlockingRecType=[select id,DeveloperName from RecordType where SobjectType='ACW__c' and DeveloperName =:'Unlocking' Limit 1].Id;
             Server_Url__c url= new Server_Url__c(Url__c = 'https://cs5.salesforce.com');
                insert url;
                SAPLogin__c saplogin               = new SAPLogin__c();
                saplogin.name='SAP Login';
                saplogin.username__c='sfdc';
                saplogin.password__c='ceat@1234';
                insert saplogin;
             //ACW__c acw=new ACW__C();
                acw.Page__c=tlPageName;
                acw.Sales_Planning__c=true;
                acw.From_Date__c= system.today(); 
                acw.To_Date__c = system.today()+1;  
                acw.Month_Text__c= 'April';
                
                //acw.RecordTypeId =lockingRecType;//locking Recored type 012O00000000bkYIAQ
                insert acw;                 
                
                 unlAcw.Page__c=tlPageName;
                 unlAcw.Sales_Planning__c=true;
                 unlAcw.From_Date__c= system.today()+3; 
                 unlAcw.To_Date__c = system.today()+5;  
                 unlAcw.user__c = userSrManagerExportData.id;
                 unlAcw.RecordTypeId =unlockingRecType;//Unlocking Recored type 
                 insert unlAcw;   
            
         
             
             List<sObject> listCatExport = Test.loadData(Export_Sales_Planning_Category__c.sObjectType, 'ExportCategory');
             listOFSPExpFor = CEAT_InitializeTestData.createSalesPlanningsExportForcast('Export_Forcast');
             insert listOFSPExpFor;
             
             accrecord = CEAT_InitializeTestData.createAccount('AJIT TYRES PVT. LIMITED', '51000047', 'PAT', 'ZE01', 'C0081');
             insert accrecord;
             accrecord1 = CEAT_InitializeTestData.createAccount('Chawla tyres', '50015640', 'PAT', 'ZE01', 'C0081');
             insert accrecord1;
             listOFSPExportDealer  = CEAT_InitializeTestData.createSalesPlanningsExportDealer('Export');
             insert listOFSPExportDealer;
             if(listOFSPExportDealer.size() > 0){
                for(Sales_Planning__c sp : listOFSPExportDealer){
                    setOFExportDealerID.add(sp.id);
                }
             }
             listExportDealer = [SELECT id ,Dealer_CustNumber__c,Dealer__c,Dealer_Name__c,Category__c ,BU__c,Category_Description__c ,Cluster_Code__c,Month__c,Year__c,Cluster_Desc__c,Country_Code__c ,Country_Desc__c ,SPExternalIDTL__c ,Budget__c,Total_planned__c FROM  Sales_Planning__c WHERE ID IN: setOFExportDealerID ];
             
             Sprecords = SP_Export_Controller.getDealersOfExportManager('AN03',String.valueOf(userSrManagerExportData.id));
             teststring = '';
             for(Sales_Planning__c spr : listExportDealer){
                if(spr.SPExternalIDTL__c == '510000472030AN03LK_test32015'){
                    teststring = spr.SPExternalIDTL__c+'@@'+spr.Dealer_CustNumber__c+'@@'+spr.Cluster_Code__c+'@@'+spr.Country_Code__c+'@@'+spr.Category__c+'@@'+spr.Dealer_Name__c;
                    spr.Total_planned__c = 30;
                    mapOfSalesPlanning.put(teststring,String.ValueOf(spr.Total_planned__c));
                    
                }
             }
             teststring = '';
             teststring = 'undefined'+'@@'+'50015640'+'@@'+'AN03'+'@@'+'LK_test'+'@@'+'2010'+'@@'+'Chawla tyres';
             mapOfSalesPlanning.put(teststring,'55');
             SP_Export_Controller.saveAsDraftDealerExportRecord(mapOfSalesPlanning, 'AN03',String.valueOf(userSrManagerExportData.id) );
             SP_Export_Controller.saveDealerExportRecord(mapOfSalesPlanning, 'AN03',String.valueOf(userSrManagerExportData.id) );
        }
    }
}