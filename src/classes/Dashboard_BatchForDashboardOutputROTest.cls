@isTest(seeAllData = false)
private class Dashboard_BatchForDashboardOutputROTest {
    
     static testmethod void testDashboardData() {
        String repDealerLabel                                   = System.Label.Replacement_Dealer;
        String OutputDashboardRecType                           = System.Label.OutputRecType;
       
        String OutputtotalDashboardRecType                      = System.Label.TotalOutputRecordType;
        Date dt = System.today();
        String month = String.valueOf(dt.month());
        String year = String.valueOf(dt.year());
        //set<id> setOFds = new set<Id>();
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
        list<Territory2Type> Terrtyp = new list<Territory2Type>();
        list<Territory2> Terr2 = new list<Territory2>();
        list<Dashboard_Score__c> dashscore                               = new list<Dashboard_Score__c>();
        list<Dashboard_Output_Score__c> dashOpscore                      = new list<Dashboard_Output_Score__c>();
        list<Dashboard_Summary_Score__c> dashSumscore                    = new list<Dashboard_Summary_Score__c>();
        Dashboard_Summary_Score__c dss2 = new Dashboard_Summary_Score__c();
        Terrtyp =   [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(Terrtyp + 'Terrtyp');

        Terr2 =      [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: Terrtyp[0].id limit 100];
        system.debug(Terr2 + 'Terr2');

        id OutputRecTypeID              = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.OutputRecType Limit 1].Id;
        
        //id TotalOutputRecordTypeID      = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.TotalOutputRecordType Limit 1].Id;
        id  totalOutputId               = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName ='Total_Output_Value' Limit 1].Id;
        system.debug('-----------------------' + totalOutputId);
        DashBoardFieldValue__c dfv = new DashBoardFieldValue__c();
            dfv.Name = 'Customer_Segment__c';
            dfv.Field_API_Name__c = 'Customer_Segment__c';
            dfv.Field_Values__c = 'High Priority Dealer';
    
        insert dfv;
        
        Dashboard_Master__c dm = new Dashboard_Master__c();
            dm.Role__c = 'RM';
            dm.Active__c = true;
        
        insert dm;
        String currentMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        String currentYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
        Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
        
            dwm.RecordTypeId = OutputRecTypeID ;
            dwm.Dashboard_Master__c = dm.ID;
            dwm.Testing__c = true;
            dwm.Category__c = 'TBB';
            dwm.Category_Code__c = '2010';
            dwm.Parameters_Output__c ='High Priority Dealer; Maintain Dealer';
            dwm.Role__c = 'RM';
            dwm.Weightage__c = 10.00;
    
         insert dwm;

       Dashboard_Weightage_Master__c dwmtot = new Dashboard_Weightage_Master__c();
        
            dwmtot.RecordTypeId = totalOutputId;
            dwmtot.Dashboard_Master__c = dm.ID;
            dwmtot.Testing__c = true;
            dwmtot.Role__c = 'RM';
            dwmtot.Weightage__c = 10.00;

    
         insert dwmtot;


system.debug('--------------------------'+dwmtot);
        
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
            dss.Active__c= true;
            dss.Dashboard_Weightage_Master__c = dwmtot.Id;
            dss.DSS_External_ID__c = 'AGR7452015';
            dss.Input_Score__c = 0;
            dss.Month__c = currentMonth ;
            dss.Region_Code__c = 'AGR';
            dss.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss.Total_MTD_Actual__c = 0;
            dss.Total_MTD_Target__c = 230;
            dss.Total_Value__c = 245;
            dss.Total_Value_L3M__c = 0;
            dss.Total_Value_LYCM__c=0;
            dss.Year__c = currentYear ;
            
            insert dss;
            
        

        Dashboard_Score__c ds = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds.Dashboard_Weightage_Master__c = dwm.ID;
            ds.Dashboard_Summary_Score__c = dss.ID;
            ds.MTD_Target__c = 988;
            ds.Actual_MTD__c = 605;
            ds.L3M__c = 1490;
            ds.LYCM__c = 1000;
            ds.Month__c = currentMonth ;
            ds.Monthly_Target__c = 1098;
            ds.Region_Code__c = 'AGR';
            ds.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds.No_of_Dealer_Distributor_Plan__c = 7;
            ds.Number_of_Dealers_Distributors__c = 13;
            ds.Category__c = '2010';
            ds.Year__c = currentYear ;
            ds.Parameters__c = 'High Priority Dealer';
            
        insert ds;

        Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos.Dashboard_Weightage_Master__c = dwm.ID;
            dos.Dashboard_Summary_Score__c = dss.ID;
            dos.Total_MTD_Target__c = 2470;
            dos.Total_Actual_MTD__c = 1645;
            dos.Total_L3M__c = 0;
            dos.Region_Code__c = 'AGR';
            dos.Territory_Code__c  = 'D0162';
            dos.Total_LYCM__c = 1625;
            dos.Total_Monthly_Target__c = 2743;
            dos.Total_dealers_distributors_billed_since__c = 32;
            dos.Total_No_of_dealers_distributors_planned__c = 37;
            dos.Total_No_of_Dealers_Distributor__c = 146;
            dos.Category__c = '2010';
            dos.Year__c = currentYear ; 
            dos.Month__c = currentMonth ;
         insert dos;
            
        
        
        Dashboard_Master__c dm1 = new Dashboard_Master__c();
            dm1.Role__c = '  TL_Replacement';
            dm1.Active__c = true;
        
        insert dm1;
        
         Dashboard_Weightage_Master__c dwm1 = new Dashboard_Weightage_Master__c();
        
            dwm1.RecordTypeId = OutputRecTypeID;
            dwm1.Dashboard_Master__c = dm1.ID;
            dwm1.Testing__c = true;
            dwm1.Category__c = 'TBB';
            dwm1.Category_Code__c = '2010';
            dwm1.Parameters_Output__c ='High Priority Dealer';
            dwm1.Role__c = 'TL_Replacement';
            dwm1.Weightage__c = 10.00;
    
         insert dwm1;
         
         
          Dashboard_Weightage_Master__c dwmtot1 = new Dashboard_Weightage_Master__c();
        
            dwmtot1.RecordTypeId = totalOutputId;
            dwmtot1.Dashboard_Master__c = dm1.ID;
            dwmtot1.Testing__c = true;
            dwmtot1.Role__c = 'TL_Replacement';
            dwmtot1.Weightage__c = 10.00;
    
         insert dwmtot1;
         
         Dashboard_Summary_Score__c dss1 = new Dashboard_Summary_Score__c();
            dss1.Active__c= true;
            dss1.Dashboard_Weightage_Master__c = dwmtot1.Id;
            dss1.DSS_External_ID__c = 'ASN7882015';
            dss1.Input_Score__c = 0;
            dss1.Month__c = currentMonth ;
            dss1.Region_Code__c = 'AGR';
            dss1.Score__c = 0;
            dss1.Territory_Code__c  = 'D0162';
            dss1.Total_MTD_Actual__c = 0;
            dss1.Total_MTD_Target__c = 230;
            dss1.Total_Value__c = 245;
            dss1.Total_Value_L3M__c = 0;
            dss1.Total_Value_LYCM__c=0;
            dss1.Year__c = currentYear ;
            
            insert dss1;
            
        

        Dashboard_Score__c ds1 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds1.Dashboard_Weightage_Master__c = dwm1.ID;
            ds1.Dashboard_Summary_Score__c = dss1.ID;
            ds1.MTD_Target__c = 988;
            ds1.Actual_MTD__c = 605;
            ds1.L3M__c = 1490;
            ds1.LYCM__c = 1000;
            ds1.Territory_Code__c  = 'D0162';
            ds1.Monthly_Target__c = 1098;
            ds1.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds1.No_of_Dealer_Distributor_Plan__c = 7;
            ds1.Number_of_Dealers_Distributors__c = 13;
            ds1.Category__c = '2010';
            ds1.Parameters__c = 'High Priority Dealer';
            ds1.month__c = currentMonth ;
            ds1.Year__c = currentYear ;
        insert ds1;

        Dashboard_Output_Score__c dos1 = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos1.Dashboard_Weightage_Master__c = dwm1.ID;
            dos1.Dashboard_Summary_Score__c = dss1.ID;
            dos1.Total_MTD_Target__c = 2470;
            dos1.Total_Actual_MTD__c = 1645;
            dos1.Total_L3M__c = 0;
            dos1.Territory_Code__c  = 'D0162';
            dos1.Total_LYCM__c = 1625;
            dos1.Total_Monthly_Target__c = 2743;
            dos1.Total_dealers_distributors_billed_since__c = 32;
            dos1.Total_No_of_dealers_distributors_planned__c = 37;
            dos1.Total_No_of_Dealers_Distributor__c = 146;
            dos1.Category__c = '2010';
            dos1.month__c = currentMonth ;
            dos1.Year__c = currentYear ;
         insert dos1;
         
         Dashboard_Master__c dm2 = new Dashboard_Master__c();
            dm2.Role__c = 'TLD';
            dm2.Active__c = true;
        
        insert dm2;
        
        Dashboard_Weightage_Master__c dwm2 = new Dashboard_Weightage_Master__c();
        
            dwm2.RecordTypeId = OutputRecTypeID ;
            dwm2.Dashboard_Master__c = dm2.ID;
            dwm2.Testing__c = true;
            dwm2.Category__c = 'TBB';
            dwm2.Category_Code__c = '2010';
            dwm2.Parameters_Output__c ='High Priority Dealer';
            dwm2.Role__c = 'TLD';
            dwm2.Weightage__c = 10.00;
    
         insert dwm2;

        Dashboard_Weightage_Master__c dwmtot2 = new Dashboard_Weightage_Master__c();
        
            dwmtot2.RecordTypeId = totalOutputId;
            dwmtot2.Dashboard_Master__c = dm2.ID;
            dwmtot2.Testing__c = true;
            dwmtot2.Role__c = 'TLD';
            dwmtot2.Weightage__c = 10.00;
    		dwmtot2.Parameters_Output__c ='High Priority Dealer';
         insert dwmtot2;
        //contact con = new Contact(LastName = 'jjjk');
        //insert con;

       // User u = CEAT_InitializeTestData.createUser('test', 'Mathu', 'vv@ceat.in', 'vv@ceat.in', '3535636', 'System Administrator', con.id, 'Admin');
        //insert u;
       // system.runAs(u){
        
            dss2.Active__c= true;
            dss2.Dashboard_Weightage_Master__c = dwmtot2.Id;
            dss2.DSS_External_ID__c = 'AGR798122015';
            dss2.Input_Score__c = 0;
            dss2.month__c = currentMonth ;
            dss2.Region_Code__c = 'AGR';
            dss2.Score__c = 0;
            
            dss2.Territory_Code__c = 'DI053';
            dss2.Total_MTD_Actual__c = 0;
            dss2.Total_MTD_Target__c = 230;
            dss2.Total_Value__c = 245;
            dss2.Total_Value_L3M__c = 0;
            dss2.Total_Value_LYCM__c=0;
            dss2.Year__c = currentYear ;
            
            insert dss2;
          //}  
        

        Dashboard_Score__c ds2 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds2.Dashboard_Weightage_Master__c = dwm2.ID;
            ds2.Dashboard_Summary_Score__c = dss2.ID;
            ds2.MTD_Target__c = 988;
            ds2.Actual_MTD__c = 605;
            ds2.L3M__c = 1490;
            ds2.LYCM__c = 1000;
            ds2.Territory_Code__c  = 'DI053';
            ds2.Monthly_Target__c = 1098;
            ds2.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds2.No_of_Dealer_Distributor_Plan__c = 7;
            ds2.Number_of_Dealers_Distributors__c = 13;
            ds2.Category__c = '2010';
            ds2.Parameters__c = 'High Priority Dealer';
            ds2.month__c = currentMonth ;
            ds2.Year__c = currentYear ;
        insert ds2;

        Dashboard_Output_Score__c dos2 = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos2.Dashboard_Weightage_Master__c = dwm2.ID;
            dos2.Dashboard_Summary_Score__c = dss2.ID;
            dos2.Total_MTD_Target__c = 2470;
            dos2.Total_Actual_MTD__c = 1645;
            dos2.Total_L3M__c = 0;
            dos2.Total_LYCM__c = 1625;
            dos2.Territory_Code__c  = 'DI053';
            dos2.Total_Monthly_Target__c = 2743;
            dos2.Total_dealers_distributors_billed_since__c = 32;
            dos2.Total_No_of_dealers_distributors_planned__c = 37;
            dos2.Total_No_of_Dealers_Distributor__c = 146;
            dos2.Category__c = '2010';
            dos2.month__c = currentMonth ;
            dos2.Year__c = currentYear ;
         insert dos2;
         
         
	Test.startTest();
    Dashboard_BatchForDashboardOutputRO db = new Dashboard_BatchForDashboardOutputRO();
    database.executebatch(db,2000);     
	Test.stopTest();

        }

        /*static testmethod void testDashboardData1() {
        String repDealerLabel                                   = System.Label.Replacement_Dealer;
        String OutputDashboardRecType                           = System.Label.OutputRecType;
       
        String OutputtotalDashboardRecType                      = System.Label.TotalOutputRecordType;
        Date dt = System.today();
        String month = String.valueOf(dt.month());
        String year = String.valueOf(dt.year());
        //set<id> setOFds = new set<Id>();
        List<sObject> listCatReplacement = Test.loadData(Sales_Planning_Categories__c.sObjectType, 'CatRepDashboard');
        list<Territory2Type> Terrtyp = new list<Territory2Type>();
        list<Territory2> Terr2 = new list<Territory2>();
        list<Dashboard_Score__c> dashscore                               = new list<Dashboard_Score__c>();
        list<Dashboard_Output_Score__c> dashOpscore                      = new list<Dashboard_Output_Score__c>();
        list<Dashboard_Summary_Score__c> dashSumscore                    = new list<Dashboard_Summary_Score__c>();

        Terrtyp =   [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        system.debug(Terrtyp + 'Terrtyp');

        Terr2 =      [SELECT id,Name,DeveloperName,Description,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId =: Terrtyp[0].id limit 100];
        system.debug(Terr2 + 'Terr2');

        id OutputRecTypeID              = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.OutputRecType Limit 1].Id;
        
        //id TotalOutputRecordTypeID      = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =: System.Label.TotalOutputRecordType Limit 1].Id;
        id  totalOutputId               = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName ='Total_Output_Value' Limit 1].Id;
        system.debug('-----------------------' + totalOutputId);
        DashBoardFieldValue__c dfv = new DashBoardFieldValue__c();
            dfv.Name = 'Customer_Segment__c';
            dfv.Field_API_Name__c = 'Customer_Segment__c';
            dfv.Field_Values__c = 'High Priority Dealer';
    
        insert dfv;
        
        Dashboard_Master__c dm = new Dashboard_Master__c();
            dm.Role__c = 'RM';
            dm.Active__c = true;
        
        insert dm;
        String currentMonth = (system.today() == system.today().toStartOfMonth()) ? String.valueOf(date.today().month() -1 ) : String.valueOf((Date.today()).month());
        String currentYear =  (system.today() == system.today().toStartOfMonth() && (Date.today()).month() == 1) ? String.valueOf(date.today().year() - 1 ) : String.valueOf((Date.today()).year());
        Dashboard_Weightage_Master__c dwm = new Dashboard_Weightage_Master__c();
        
            dwm.RecordTypeId = OutputRecTypeID ;
            dwm.Dashboard_Master__c = dm.ID;
            dwm.Testing__c = true;
            dwm.Category__c = 'TBB';
            dwm.Category_Code__c = '2010';
            dwm.Parameters_Output__c ='High Priority Dealer; Maintain Dealer';
            dwm.Role__c = 'RM';
            dwm.Weightage__c = 10.00;
    
         insert dwm;

       Dashboard_Weightage_Master__c dwmtot = new Dashboard_Weightage_Master__c();
        
            dwmtot.RecordTypeId = totalOutputId;
            dwmtot.Dashboard_Master__c = dm.ID;
            dwmtot.Testing__c = true;
            dwmtot.Role__c = 'RM';
            dwmtot.Weightage__c = 10.00;

    
         insert dwmtot;


system.debug('--------------------------'+dwmtot);
        
        Dashboard_Summary_Score__c dss = new Dashboard_Summary_Score__c();
            dss.Active__c= true;
            dss.Dashboard_Weightage_Master__c = dwmtot.Id;
            dss.DSS_External_ID__c = 'AGR72015';
            dss.Input_Score__c = 0;
            dss.Month__c = currentMonth ;
            dss.Region_Code__c = 'AGR';
            dss.Score__c = 0;
            //dss.Territory_Code__c = 'ASN';
            dss.Total_MTD_Actual__c = 0;
            dss.Total_MTD_Target__c = 230;
            dss.Total_Value__c = 245;
            dss.Total_Value_L3M__c = 0;
            dss.Total_Value_LYCM__c=0;
            dss.Year__c = currentYear ;
            
            insert dss;
            
        

        Dashboard_Score__c ds = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds.Dashboard_Weightage_Master__c = dwm.ID;
            ds.Dashboard_Summary_Score__c = dss.ID;
            ds.MTD_Target__c = 988;
            ds.Actual_MTD__c = 605;
            ds.L3M__c = 1490;
            ds.LYCM__c = 1000;
            ds.Month__c = currentMonth ;
            ds.Monthly_Target__c = 1098;
            ds.Region_Code__c = 'AGR';
            ds.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds.No_of_Dealer_Distributor_Plan__c = 7;
            ds.Number_of_Dealers_Distributors__c = 13;
            ds.Category__c = '2010';
            ds.Year__c = currentYear ;
            ds.Parameters__c = 'High Priority Dealer';
            
        insert ds;

        Dashboard_Output_Score__c dos = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos.Dashboard_Weightage_Master__c = dwm.ID;
            dos.Dashboard_Summary_Score__c = dss.ID;
            dos.Total_MTD_Target__c = 2470;
            dos.Total_Actual_MTD__c = 1645;
            dos.Total_L3M__c = 0;
            dos.Region_Code__c = 'AGR';
            dos.Territory_Code__c  = 'D0162';
            dos.Total_LYCM__c = 1625;
            dos.Total_Monthly_Target__c = 2743;
            dos.Total_dealers_distributors_billed_since__c = 32;
            dos.Total_No_of_dealers_distributors_planned__c = 37;
            dos.Total_No_of_Dealers_Distributor__c = 146;
            dos.Category__c = '2010';
            dos.Year__c = currentYear ; 
            dos.Month__c = currentMonth ;
         insert dos;
            
        
        
        Dashboard_Master__c dm1 = new Dashboard_Master__c();
            dm1.Role__c = '  TL_Replacement';
            dm1.Active__c = true;
        
        insert dm1;
        
         Dashboard_Weightage_Master__c dwm1 = new Dashboard_Weightage_Master__c();
        
            dwm1.RecordTypeId = OutputRecTypeID;
            dwm1.Dashboard_Master__c = dm1.ID;
            dwm1.Testing__c = true;
            dwm1.Category__c = 'TBB';
            dwm1.Category_Code__c = '2010';
            dwm1.Parameters_Output__c ='High Priority Dealer';
            dwm1.Role__c = 'TL_Replacement';
            dwm1.Weightage__c = 10.00;
    
         insert dwm1;
         
         
          Dashboard_Weightage_Master__c dwmtot1 = new Dashboard_Weightage_Master__c();
        
            dwmtot1.RecordTypeId = totalOutputId;
            dwmtot1.Dashboard_Master__c = dm1.ID;
            dwmtot1.Testing__c = true;
            dwmtot1.Role__c = 'TL_Replacement';
            dwmtot1.Weightage__c = 10.00;
    
         insert dwmtot1;
         
         Dashboard_Summary_Score__c dss1 = new Dashboard_Summary_Score__c();
            dss1.Active__c= true;
            dss1.Dashboard_Weightage_Master__c = dwmtot1.Id;
            dss1.DSS_External_ID__c = 'ASN72015';
            dss1.Input_Score__c = 0;
            dss1.Month__c = currentMonth ;
            dss1.Region_Code__c = 'AGR';
            dss1.Score__c = 0;
            dss1.Territory_Code__c  = 'D0162';
            dss1.Total_MTD_Actual__c = 0;
            dss1.Total_MTD_Target__c = 230;
            dss1.Total_Value__c = 245;
            dss1.Total_Value_L3M__c = 0;
            dss1.Total_Value_LYCM__c=0;
            dss1.Year__c = currentYear ;
            
            insert dss1;
            
        

        Dashboard_Score__c ds1 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds1.Dashboard_Weightage_Master__c = dwm1.ID;
            ds1.Dashboard_Summary_Score__c = dss1.ID;
            ds1.MTD_Target__c = 988;
            ds1.Actual_MTD__c = 605;
            ds1.L3M__c = 1490;
            ds1.LYCM__c = 1000;
            ds1.Territory_Code__c  = 'D0162';
            ds1.Monthly_Target__c = 1098;
            ds1.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds1.No_of_Dealer_Distributor_Plan__c = 7;
            ds1.Number_of_Dealers_Distributors__c = 13;
            ds1.Category__c = '2010';
            ds1.Parameters__c = 'High Priority Dealer';
            ds1.month__c = currentMonth ;
            ds1.Year__c = currentYear ;
        insert ds1;

        Dashboard_Output_Score__c dos1 = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos1.Dashboard_Weightage_Master__c = dwm1.ID;
            dos1.Dashboard_Summary_Score__c = dss1.ID;
            dos1.Total_MTD_Target__c = 2470;
            dos1.Total_Actual_MTD__c = 1645;
            dos1.Total_L3M__c = 0;
            dos1.Territory_Code__c  = 'D0162';
            dos1.Total_LYCM__c = 1625;
            dos1.Total_Monthly_Target__c = 2743;
            dos1.Total_dealers_distributors_billed_since__c = 32;
            dos1.Total_No_of_dealers_distributors_planned__c = 37;
            dos1.Total_No_of_Dealers_Distributor__c = 146;
            dos1.Category__c = '2010';
            dos1.month__c = currentMonth ;
            dos1.Year__c = currentYear ;
         insert dos1;
         
         Dashboard_Master__c dm2 = new Dashboard_Master__c();
            dm2.Role__c = 'TLD';
            dm2.Active__c = true;
        
        insert dm2;
        
        Dashboard_Weightage_Master__c dwm2 = new Dashboard_Weightage_Master__c();
        
            dwm2.RecordTypeId = OutputRecTypeID ;
            dwm2.Dashboard_Master__c = dm2.ID;
            dwm2.Testing__c = true;
            dwm2.Category__c = 'TBB';
            dwm2.Category_Code__c = '2010';
            dwm2.Parameters_Output__c ='High Priority Dealer';
            dwm2.Role__c = 'TLD';
            dwm2.Weightage__c = 10.00;
    
         insert dwm2;

        Dashboard_Weightage_Master__c dwmtot2 = new Dashboard_Weightage_Master__c();
        
            dwmtot2.RecordTypeId = totalOutputId;
            dwmtot2.Dashboard_Master__c = dm2.ID;
            dwmtot2.Testing__c = true;
            dwmtot2.Role__c = 'TLD';
            dwmtot2.Weightage__c = 10.00;
            dwmtot2.Parameters_Output__c ='High Priority Dealer';
         insert dwmtot2;

        
        Dashboard_Summary_Score__c dss2 = new Dashboard_Summary_Score__c();
            dss2.Active__c= true;
            dss2.Dashboard_Weightage_Master__c = dwmtot2.Id;
            dss2.DSS_External_ID__c = 'AGR72015';
            dss2.Input_Score__c = 0;
            dss2.month__c = currentMonth ;
            dss2.Region_Code__c = 'AGR';
            dss2.Score__c = 0;
            
            dss2.Territory_Code__c = 'DI053';
            dss2.Total_MTD_Actual__c = 0;
            dss2.Total_MTD_Target__c = 230;
            dss2.Total_Value__c = 245;
            dss2.Total_Value_L3M__c = 0;
            dss2.Total_Value_LYCM__c=0;
            dss2.Year__c = currentYear ;
            
            insert dss2;
            
        

        Dashboard_Score__c ds2 = new Dashboard_Score__c();
         //dashboardScoreTLTLD = [SELECT  SUM(MTD_Target__c) targetMTD,SUM(Actual_MTD__c) actualMTD,SUM(L3M__c) L3M,SUM(LYCM__c) LYCM, SUM(Monthly_Target__c) Montarget ,SUM(No_of_Dealer_Distributor_Billed_Since__c) BilledDealer,SUM(No_of_Dealer_Distributor_Plan__c) PlanDealer,SUM(Number_of_Dealers_Distributors__c) NumDealer, Category__c,Parameters__c FROM Dashboard_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear AND Parameters__c IN: setOFParametersAllCat group by Category__c,Parameters__c];
            ds2.Dashboard_Weightage_Master__c = dwm2.ID;
            ds2.Dashboard_Summary_Score__c = dss2.ID;
            ds2.MTD_Target__c = 988;
            ds2.Actual_MTD__c = 605;
            ds2.L3M__c = 1490;
            ds2.LYCM__c = 1000;
            ds2.Territory_Code__c  = 'DI053';
            ds2.Monthly_Target__c = 1098;
            ds2.No_of_Dealer_Distributor_Billed_Since__c = 6;
            ds2.No_of_Dealer_Distributor_Plan__c = 7;
            ds2.Number_of_Dealers_Distributors__c = 13;
            ds2.Category__c = '2010';
            ds2.Parameters__c = 'High Priority Dealer';
            ds2.month__c = currentMonth ;
            ds2.Year__c = currentYear ;
        insert ds2;

        Dashboard_Output_Score__c dos2 = new Dashboard_Output_Score__c();
        //dashboardOutputScoreTLTLD = [SELECT  SUM(Total_MTD_Target__c) targetMTD,SUM(Total_Actual_MTD__c) actualMTD,SUM(Total_L3M__c) L3M,SUM(Total_LYCM__c) LYCM, SUM(Total_Monthly_Target__c) Montarget ,SUM(Total_dealers_distributors_billed_since__c) BilledDealer,SUM(Total_No_of_dealers_distributors_planned__c) PlanDealer,SUM(Total_No_of_Dealers_Distributor__c) NumDealer, Category__c FROM Dashboard_Output_Score__c where  Territory_Code__c IN: terrNames  AND  Month__c =: currentMonth AND Year__c =: currentYear group by Category__c];
            dos2.Dashboard_Weightage_Master__c = dwm2.ID;
            dos2.Dashboard_Summary_Score__c = dss2.ID;
            dos2.Total_MTD_Target__c = 2470;
            dos2.Total_Actual_MTD__c = 1645;
            dos2.Total_L3M__c = 0;
            dos2.Total_LYCM__c = 1625;
            dos2.Territory_Code__c  = 'DI053';
            dos2.Total_Monthly_Target__c = 2743;
            dos2.Total_dealers_distributors_billed_since__c = 32;
            dos2.Total_No_of_dealers_distributors_planned__c = 37;
            dos2.Total_No_of_Dealers_Distributor__c = 146;
            dos2.Category__c = '2010';
            dos2.month__c = currentMonth ;
            dos2.Year__c = currentYear ;
         insert dos2;
         
     //String day = String.valueOf(System.today());
     //String month = String.valueOf(System.today().month);    
    Test.startTest();
    Dashboard_BatchForDashboardOutputRO db = new Dashboard_BatchForDashboardOutputRO('02', '03', '2015');
    database.executebatch(db,2000);     
    Test.stopTest();

        }*/
}