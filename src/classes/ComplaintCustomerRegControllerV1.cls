global class ComplaintCustomerRegControllerV1{
    public string selectedState {get;set;} 
    public List<SelectOption> stateList{get;set;} 
    public static string sfdcBaseUrl{get;set;}
    public static string apex;
    
    public ComplaintCustomerRegControllerV1(ApexPages.StandardController controller) {
        stateList           = new List<SelectOption>();
        /*-----option for state----*/
        List<State_Master__c> states = new List<State_Master__c>();
            stateList.add(new SelectOption('','--None--'));
        for(AggregateResult masterRec: [Select State__c from State_Master__c group by State__c]){
            
            stateList.add(new SelectOption(String.valueOf(masterRec.get('State__c')),String.valueOf(masterRec.get('State__c'))));        
        }
    }
//Passing state as param and getting all district under that state
    @RemoteAction
    global static List<SelectOption> getDistricts(String selectedState){
        List<SelectOption> districtList = new List<SelectOption>();
        districtList.add(new SelectOption('','--- None ---')); 
        Set<String> disSet=new Set<String>();
        
        for(State_Master__c s:[Select District__c from State_Master__c where State__c=:selectedState order by District__c asc]){
          if(!disSet.contains(s.District__c)){
            districtList.add(new SelectOption(s.District__c,s.District__c)); 
            disSet.add(s.District__c);
          } 
        }
        return districtList;
    }
     //Passing district as param and getting all towns under that district
    @RemoteAction
    global static List<SelectOption> getTowns(String selectedDistrict){
         List<SelectOption> townList = new List<SelectOption>();
        townList.add(new SelectOption('','--- None ---')); 
        for(State_Master__c s:[Select Town__c from State_Master__c where District__c=:selectedDistrict order by Town__c asc]){
            townList.add(new SelectOption(s.Town__c,s.Town__c));  
        }
        return townList;
    }
    @RemoteAction
    global static string saveComplaintCustomer(string name,string phone,string state,string dis,string town,string email,string street,string typeOfCust){
    String successMessage;
    system.debug('name-----'+name+'phone---'+phone+'state---'+state+'dis----'+dis+'town---'+town);
    List<State_Master__c> stateLists = new List<State_Master__c>();
    List<Case> allpreCom = new list<Case>();
    List<Case> updateallpreCom = new list<Case>();
    list<Account> listOfAccount = new list<Account>();
    List<List<SObject>> searchList = new List<List<SObject>>();
    Id precomRecordtype = UtilityClass.getRecordTypeId('Case','Pre-complaints');
    system.debug(precomRecordtype +'precomRecordtype');
    if(email != '' ){
        allpreCom = [SELECT Id,Customer_Email__c,Mobile_of_complainant__c From Case WHERE (Customer_Email__c =: email OR Mobile_of_complainant__c =: phone OR SuppliedEmail =: email ) AND AccountID = null AND RecordtypeID =: precomRecordtype ];
        
        system.debug(allpreCom.size()+'AAAAA');
        String query  = 'FIND \'' +email +'\' IN ALL FIELDS RETURNING Account (Id,Name,KUNNR__c,Preferred_Language__c)';
        system.debug('@@@@@@@#'+query);
        searchList = search.query(query); 

                    
        
    }else{
        allpreCom = [SELECT Id,Customer_Email__c,Mobile_of_complainant__c From Case WHERE  Mobile_of_complainant__c =: phone AND AccountID = null AND RecordtypeID =: precomRecordtype ];
    }
    
    system.debug(allpreCom.size()+'SSSS');
    system.debug(allpreCom+'allpreCom');
    stateLists=[SELECT State__c,District__c,Town__c,RO__c,Zone__c FROM State_Master__c WHERE State__c=:state AND District__c =:dis];
    Account compAcc = new Account();
    compAcc.name=name;
    compAcc.AD_TLNMBR__c=phone;
    compAcc.State__c=state;
    compAcc.district__c=dis;
    compAcc.Town__c=town;
    compAcc.Type='Complaint End Customer';
    compAcc.Email__c = email;
    compAcc.Street_1__c = street;
    compAcc.Type_Of_Customer__c= typeOfCust;
    compAcc.RecordTypeId =[SELECT DeveloperName,Id,Name,SobjectType FROM RecordType WHERE Name =: 'Complaints Customer' AND SobjectType =: 'Account'].Id;
    if(stateLists.size()>0 ){
         List<Territory2> territoryList = new List<Territory2>();
         List<Territory2> territory2zoneValues = new List<Territory2>();

          compAcc.Sales_Group_Text__c=stateLists[0].RO__c;
          territoryList = [SELECT DeveloperName,Id,Name,ParentTerritory2Id FROM Territory2 WHERE DeveloperName = :compAcc.Sales_Group_Text__c];
          territory2zoneValues = [SELECT DeveloperName,Id,Name,ParentTerritory2Id FROM Territory2 WHERE Id = :territoryList[0].ParentTerritory2Id];
            if(territory2zoneValues.size()>0){
             compAcc.Sales_Office_Text__c =territory2zoneValues[0].Name; 
         
            }      //compAcc.Sales_Office_Text__c=stateLists[0].Zone__c;        
    }
    insert compAcc;
    if(allpreCom.size() > 0){
       system.debug('QQQQ');
       for(Case tempcase : allpreCom){
           tempcase.AccountID = compAcc.id;
           tempcase.State__c = state;
           tempcase.district__c = dis;
           tempcase.Town__c = town;
           tempcase.Customer_Phone__c = phone;
          
            if(email !=''){
            tempcase.Customer_Email__c = email;
            }
            
           updateallpreCom.add(tempcase);
       }
       successMessage=allpreCom[0].Id;
    }else{
       successMessage=compAcc.id;
    }
    system.debug(successMessage+'successMessage');
    system.debug(updateallpreCom+'updateallpreCom');
    if(updateallpreCom.size() > 0){
        update updateallpreCom;
    }
    if(allpreCom.size() == 0){
        
        sfdcBaseUrl = System.URL.getSalesforceBaseURL().toExternalForm()+'/apex/ComplaintBPOPage_V2?accountId='+compAcc.id;
        successMessage = sfdcBaseUrl;
    }else
        if(allpreCom.size()== 1){ 
            sfdcBaseUrl = System.URL.getSalesforceBaseURL().toExternalForm()+'/apex/ComplaintBPOPage_V2?Id='+allpreCom[0].id;         
            successMessage = sfdcBaseUrl;
        }else
            if(allpreCom.size()>1){
              sfdcBaseUrl = System.URL.getSalesforceBaseURL().toExternalForm()+'/'+compAcc.id;
              successMessage = sfdcBaseUrl;
            }
    
    return successMessage;
    
   
    }
    //Added by PALLAVI  
    @RemoteAction
    global static List<String> getphoneNumbersAndEmailList(String phNumber, String emailId) {
        List<Account> accountWithPhNoList = [SELECT Name FROM Account WHERE AD_TLNMBR__c =: phNumber];
        List<String> matchingAccountNames = new List<String>();
        if(accountWithPhNoList.size() > 0) {
            for(Account tmpAcc : accountWithPhNoList) {
                matchingAccountNames.add(tmpAcc.Name);
            }
        }else {
            if(emailId != '') {
              List<Account> accountWithEmailList = [SELECT Name FROM Account WHERE Email__c =: emailId];
              if(accountWithEmailList.size() > 0) {
                  for(Account tmpAcc : accountWithEmailList) {
                  matchingAccountNames.add(tmpAcc.Name);
                }
              }
            }
        }
        return matchingAccountNames;
    }

}