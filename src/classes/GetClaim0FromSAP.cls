global class GetClaim0FromSAP {

    global GetClaim0FromSAP() {
         
    }
    
    WebService static void callWebServiceGetClaim0(sapClaimCL0.TableOfZsdcl0 toZsdc){//String cusNum, String fDate, String tDate) {         
        system.debug(toZsdc+'$$$$$$$$$$$$$$$');
        SAPLogin__c saplogin = SAPLogin__c.getValues('SAP Login');
        String username= saplogin.username__c;
        String password= saplogin.password__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        sapClaimCL0 ws = new sapClaimCL0();
        
        sapClaimCL0.ZWS_GET_CL0_DATA zws = new sapClaimCL0.ZWS_GET_CL0_DATA();
        zws.inputHttpHeaders_x = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x = 12000;
        sapClaimCL0.TableOfZsdcl0 IT_OUTPUT = toZsdc;//new sapClaimCL0.TableOfZsdcl0();        
        sapClaimCL0.ZsdGetCl0DataResponse_element sapClaimResponseElement = new  sapClaimCL0.ZsdGetCl0DataResponse_element();
        sapClaimResponseElement = zws.ZsdGetCl0Data(toZsdc);
        system.debug('dsaa'+sapClaimResponseElement.RETURN_x);    
    }    
}