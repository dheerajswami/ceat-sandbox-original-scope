global class SAP_BatchForCustomerSDS implements Database.Batchable<sObject>,Database.AllowsCallouts {    
    /* ================================================
    @Name:  SAP_BatchForCustomerSDS
    @Copyright notice: 
    Copyright (c) 2013, CEAT and developed by Extentor
        All rights reserved.
        
        Redistribution and use in source and binary forms, with or without
        modification, are not permitted.                                                                                                    
    @====================================================
    @====================================================
    @Purpose: This batch class will update SDS of Customer on daily basis                                                                                            
    @====================================================
    @====================================================
    @History                                                                                                                    
    @---------                                                                                                                       
    @VERSION________AUTHOR______________DATE______________DETAIL                   
     1.0        Kishlay@extentor     11/05/2015      INITIAL DEVELOPMENT                                 
   
    @=======================================================  */
    
  //*********************SETS*******************************
  Set<String> territorySet                                = new Set<String>();
  set<id> terrTypeID                                      = new set<Id>();
  public String query;
  //*********************LISTS*******************************
  list<String> territoryList                              = new list<String>(); 
  list<Account> listofCustomer                            = new list<Account>();
  list<Account> updatelistofCustomer                      = new list<Account>();
  list<Territory2Type> terriType                          = new list<Territory2Type>();
  
  //*********************MAPS*******************************
  map<String,Decimal> mapofCustAndSDS                     = new map<String,Decimal>();
  
  Id prospectDealerId;
  Integer numberDays;
  Date d                                                  = system.today();
  Integer month                                           = d.month();
  Integer year                                            = d.year();
  Integer day                                             = d.day();
  global SAP_BatchForCustomerSDS (){
        
        prospectDealerId                                  = [select id,DeveloperName from RecordType where SobjectType='Account' and DeveloperName = 'Prospect' Limit 1].Id;
        terriType                                         = [SELECT id, DeveloperName from Territory2Type where  DeveloperName = 'Territory'];
        for(Territory2Type temp : terriType){
            terrTypeID.add(temp.id);
        }
        numberDays                                        = date.daysInMonth(year, month);
  }
  global Database.QueryLocator start(Database.BatchableContext BC){
        // String query;
         String SPRP                                      = 'SPRP';
         String B0082                                     = 'B0082';
         
         String[] filters = new String[]{'SPRP%'};
         //query                    = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Name =  \'B0082\' AND Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters'+')'; 
         query                                            = 'SELECT id,Name,DeveloperName,ParentTerritory2Id,Territory2TypeId from Territory2 WHERE Territory2TypeId IN: terrTypeID AND (NOT DeveloperName LIKE :  filters'+')'; 
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        for(sObject temp : scope){
            Territory2 sps                                =(Territory2)temp;
            territorySet.add(sps.Name);
            territoryList.addall(territorySet);
        }
        system.debug(territoryList+'territoryList');
        listofCustomer                                    = [SELECT id,name,KUNNR__c,Sales_District_Text__c,SDS__c,Last_Month_SDS__c From Account WHERE Sales_District_Text__c =: territoryList[0] AND RecordTypeID != : prospectDealerId];
        SAPLogin__c saplogin                              = SAPLogin__c.getValues('SAP Login');
        String username                                   = saplogin.username__c;
        String password                                   = saplogin.password__c;
        Blob headerValue                                  = Blob.valueOf(username + ':' + password);
        String authorizationHeader                        = 'Basic '+ EncodingUtil.base64Encode(headerValue);
        sapCustomer_SDS_PROD_TL   ws                      = new sapCustomer_SDS_PROD_TL  ();
        
        sapCustomer_SDS_PROD_TL.ZWS_CUSTOMER_SDS_TL zws   = new sapCustomer_SDS_PROD_TL.ZWS_CUSTOMER_SDS_TL();
        zws.inputHttpHeaders_x                            = new Map<String,String>();
        zws.inputHttpHeaders_x.put('Authorization',authorizationHeader);
        zws.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
        zws.timeout_x                                     = 35000;
        sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds tom   = new sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds  ();   
        //sapETD.TableOfZsdDisbRfc tom2 = new zws_ppd_disc_McS.TableOfZsdDisbRfc();     
        
        sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds elem  = new sapCustomer_SDS_PROD_TL.TableOfZbapiCustSds();        
        elem                                              = zws.ZbapiCustomerSdsTl(territoryList[0],tom);
        if(elem != null){
            if(elem.item != null){
                for(sapCustomer_SDS_PROD_TL.ZbapiCustSds z : elem.item){
                            if(z.kunnr.startsWith('00'))
                            z.kunnr                       = z.kunnr.subString(2);
                            mapofCustAndSDS.put(z.kunnr,Decimal.ValueOF(z.sds));
                }
            }
        }
        if(mapofCustAndSDS.size() > 0){
            if(listofCustomer.size() > 0){
                for(Account acc : listofCustomer){
                    if(mapofCustAndSDS.containsKey(acc.KUNNR__c)){
                        acc.SDS__c                        = mapofCustAndSDS.get(acc.KUNNR__c);
                        if(day == numberDays){
                            acc.Last_Month_SDS__c         = mapofCustAndSDS.get(acc.KUNNR__c);
                        }
                        updatelistofCustomer.add(acc);
                    }
                }
                
            } 
        }
        if(updatelistofCustomer.size() > 0){
            database.upsert(updatelistofCustomer,Account.Fields.KUNNR__c,false);
        }
    }
    global void finish(Database.BatchableContext BC)
    {       
          // Get the AsyncApexJob that represents the Batch job using the Id from the BatchableContext  
             AsyncApexJob a = [Select Id, Status, MethodName ,NumberOfErrors, JobItemsProcessed,  
              TotalJobItems, CreatedBy.Email, ExtendedStatus  
              from AsyncApexJob where Id = :BC.getJobId()];  
             list<User> SysUser = new list<User>();
             SysUser = [SELECT ID , Name, Email,IsActive From User Where Profile.Name = 'System Administrator' AND IsActive = true];  
             
             // Email the Batch Job's submitter that the Job is finished.  
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
             //String[] toAddresses = new String[] {a.CreatedBy.Email}; 
             String[] toAddresses = new String[] {}; 
             if(SysUser.size() > 0){
                 for(User u : SysUser ){
                     toAddresses.add(u.Email );
                 }
             } 
              
            mail.setToAddresses(toAddresses);  
             mail.setSubject('BatchJob: SAP_BatchForCustomerSDS Status: ' + a.Status);  
             mail.setPlainTextBody('Hi Admin, The batch Apex job "SAP_BatchForCustomerSDS" ' + a.TotalJobItems + '/n'+'batches with '+  a.NumberOfErrors + ' failures. ExtendedStatus: ' + a.ExtendedStatus+ '/n');  
             
             if(a.NumberOfErrors > 0){
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
             }   
            
    }
}