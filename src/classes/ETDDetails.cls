global class ETDDetails {
    
    global class ETDDetailsMapping {
        public String SALES_DOCUMENT{get;set;}
        public String PI_NO{get;set;}
        public Integer ITEM_SD{get;set;}
        public String MATERIAL_NO{get;set;}
        public String ETD_Date{get;set;}
        public String Quantity{get;set;}
        public String MAT_DESCRIPTION{get;set;}
        public String REASON{get;set;}
        
        public ETDDetailsMapping (String SALES_DOCUMENT, String PI_NO, String ITEM_SD, String MATERIAL_NO, String ETD_Date,String Quantity,String MAT_DESCRIPTION,String REASON){
            this.SALES_DOCUMENT = SALES_DOCUMENT;
            this.PI_NO = PI_NO;
            this.ITEM_SD = Integer.ValueOF(ITEM_SD) ; 
            if(MATERIAL_NO.startsWith('0')) {
                this.MATERIAL_NO = String.valueOf(Integer.valueOf(MATERIAL_NO)); 
            }else {
                this.MATERIAL_NO = MATERIAL_NO;
            }
            //this.MATERIAL_NO = MATERIAL_NO;
            this.ETD_Date = ETD_Date;
            this.Quantity = Quantity;
            this.MAT_DESCRIPTION = MAT_DESCRIPTION;
            this.REASON = REASON;
        }
    }
    @RemoteAction
    WebService static List<ETDDetailsMapping> getAllETDDetails(String PI_Number){
        system.debug(PI_Number+'PI_Number');
        List<ETDDetailsMapping> allETD = new List<ETDDetailsMapping>();
        String dat = '';
        if(PI_Number != ''){
            try{
                SAPLogin__c saplogin                = SAPLogin__c.getValues('SAP Login');        
                String username                   = saplogin.username__c;
                String password                   = saplogin.password__c;
                Blob headerValue                  = Blob.valueOf(username + ':' + password);
                String authorizationHeader     = 'Basic '+ EncodingUtil.base64Encode(headerValue);
                Sap_ETDWebService ws = new Sap_ETDWebService();
                Sap_ETDWebService.ZWS_BAPI_PO_DATE    clsInstance = new Sap_ETDWebService.ZWS_BAPI_PO_DATE();
                clsInstance.inputHttpHeaders_x = new Map<String,String>();
                clsInstance.inputHttpHeaders_x.put('Authorization',authorizationHeader);
                clsInstance.inputHttpHeaders_x.put('Content-Type', 'text/xml;charset=UTF-8;');
                clsInstance.timeout_x = 60000;
                Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE IT_OUTPUT = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();   
                Sap_ETDWebService.ZBAPI_PO_DATEResponse_element etdResponseElement = new Sap_ETDWebService.ZBAPI_PO_DATEResponse_element(); 
                etdResponseElement = clsInstance.ZBAPI_PO_DATE(PI_Number,IT_OUTPUT,''); 
                //etdResponseElement = clsInstance.ZBAPI_PO_DATE('',IT_OUTPUT,'',PI_Number);
                Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE etdResponses = new Sap_ETDWebService.TABLE_OF_ZSD_PO_DATE();
                system.debug(etdResponseElement +'etdResponseElement');
                
                
                if(etdResponseElement != null){   
                    etdResponses = etdResponseElement.IT_OUTPUT;
                    //System.debug('==#1 Response Item '+etdResponses.item);
                    if(etdResponses.item != null){
                        for(Sap_ETDWebService.ZSD_PO_DATE z : etdResponses.item) {
                            System.debug('==#2 ' + z);
                            if(z != null){
                                
                                system.debug(z.VBELN+'--VBELN--');
                                system.debug(z.POSNR+'--POSNR----');
                                system.debug(z.BSTDK_E+'--BSTDK_E--');
                                system.debug(z.MATNR+'--MATNR--');
                                system.debug(z.ETDAT+'--ETDAT--');
                                system.debug(z.ETDNO+'--ETDNO--');
                                system.debug(z.REASON+'--REASON--');
                                system.debug(z.MENGE+'--MENGE--');

                                String yr;
                                String month;
                                String day;
                                String etDate;

                                if(z.ETDAT != '') {
                                    yr          = z.ETDAT.substring(0, 4);
                                    month       = z.ETDAT.substring(4, 6);
                                    day         = z.ETDAT.substring(6, 8);
                                    etDate      = day+'-'+month+'-'+yr;
                                }else {
                                    etDate      = z.ETDAT;
                                }
                                System.debug('==#3 '+etDate);
                                allETD.add(new ETDDetailsMapping(z.VBELN, PI_Number, z.POSNR,z.MATNR,etDate ,z.MENGE,z.MAKTX,z.REASON));
                            }
                        }
                    }
                }
                
            }
            catch(Exception e){
                system.debug(e.getMessage());
            }
            system.debug(allETD+'allETD');
        }
        
        return allETD;
        //return null;
    }
}