@isTest
public class DashboardInputZO_BatchClass_Testclass{
   
      static testMethod void myUnitTest() {
            list<UserTerritory2Association> terr = [select id,Territory2.developerName,Territory2Id,UserId from UserTerritory2Association where Territory2.developerName='ZW02' AND RoleInTerritory2='ZGM' limit 1];
            User u = [Select id, name from User where id = :terr[0].UserId limit 1];
            
            System.runAs(u) {
                 //Insert Dashboard Master
                Dashboard_Master__c dm = new Dashboard_Master__c(Active__c=true,Role__c='ZO');
                insert dm;
                
                Dashboard_Master__c dmrm = new Dashboard_Master__c(Active__c=true,Role__c='RM');
                insert dmrm;
                
               
                Id inputRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.label.Input_RecordType Limit 1].Id;
                Id actionLogRecTypeId  = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.ActionLogInputRecordType Limit 1].Id;
                Id efleetRecTypeId     = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Weightage_Master__c' and DeveloperName =:system.Label.Efleet_RecType Limit 1].Id;
            
                
                Dashboard_Weightage_Master__c dwmTemprm = new Dashboard_Weightage_Master__c();
                dwmTemprm.Dashboard_Master__c = dm.id;
                dwmTemprm.RecordTypeId = inputRecTypeId;
                dwmTemprm.role__c = 'ZO';
                dwmTemprm.Weightage__c = 4;
                dwmTemprm.Testing__c = true;
                dwmTemprm.Category__c = 'PJP adherance';
                dwmTemprm.Parameters_Inout__c = 'Maintain Dealer';
                
                insert dwmTemprm; 
                
                Dashboard_Weightage_Master__c dwmTemp = new Dashboard_Weightage_Master__c();
                dwmTemp.Dashboard_Master__c = dmrm.id;
                dwmTemp.role__c = 'RM';
                dwmTemp.Weightage__c = 4;
                dwmTemp.Testing__c = true;
                dwmTemp.Category__c = 'PJP adherance';
                dwmTemp.Parameters_Inout__c = 'Maintain Dealer';
                
                insert dwmTemp; 
                Id rmRecTypeId       = [select id,DeveloperName from RecordType where SobjectType='Dashboard_Input_Score__c' and DeveloperName ='RM' Limit 1].Id;
       
                 Dashboard_Input_Score__c dstl=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstl.Region_Code__c             = 'SUR';
                                    dstl.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dstl.Month__c                      = 'August';
                                    dstl.Year__c                       = '2015';     
                                    dstl.RecordTypeId                  = rmRecTypeId;              
                                    dstl.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstl.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstl.Total_Monthly_Target__c        = 0;                                    
                                    dstl.Total_MTD_Target__c            = 0;                                   
                                    dstl.Total_Actual_MTD__c            = 0;
                                    dstl.Achievement_MTD__c             = 0;
                                    dstl.Score__c                       = 0;
                                     
                insert dstl;
                Dashboard_Input_Score__c dstl1=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstl1.Region_Code__c             = 'AHM';
                                    dstl1.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dstl1.Month__c                      = 'August';
                                    dstl1.Year__c                       = '2015';     
                                    dstl1.RecordTypeId                  = rmRecTypeId;                       
                                    dstl1.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstl1.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstl1.Total_Monthly_Target__c       = 0;                                    
                                    dstl1.Total_MTD_Target__c           = 0;                                   
                                    dstl1.Total_Actual_MTD__c           = 0;
                                    dstl1.Achievement_MTD__c            = 0;
                                    dstl1.Score__c                      = 0;
                                     
                insert dstl1;
                Dashboard_Input_Score__c dstld=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstld.Region_Code__c             = 'JOD';
                                    dstld.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dstld.Month__c                      = 'August';
                                    dstld.Year__c                       = '2015';                         
                                    dstld.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstld.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c    = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstld.Total_Monthly_Target__c       = 0;                                    
                                    dstld.Total_MTD_Target__c           = 0;                                   
                                    dstld.Total_Actual_MTD__c           = 0;
                                    dstld.Achievement_MTD__c            = 0;
                                    dstld.Score__c                      = 0;
                                    dstld.RecordTypeId                  = rmRecTypeId;     
                insert dstld;
                
                 Dashboard_Input_Score__c dstld1=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dstld1.Region_Code__c             = 'RAJ';
                                    dstld1.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dstld1.Month__c                      = 'August';
                                    dstld1.Year__c                       = '2015';     
                                    dstld1.RecordTypeId                  = rmRecTypeId;                     
                                    dstld1.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dstld1.Category__c                   = 'PJP adherance';
                                    //dstlds.Input_Dashboard_ExID__c    = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dstld1.Total_Monthly_Target__c      = 0;                                    
                                    dstld1.Total_MTD_Target__c          = 0;                                   
                                    dstld1.Total_Actual_MTD__c          = 0;
                                    dstld1.Achievement_MTD__c           = 0;
                                    dstld1.Score__c                         = 0;
                                     
                insert dstld1;
                Dashboard_Input_Score__c dscstl=new Dashboard_Input_Score__c();
                                    //ds.User__c                       = uList.Id;
                                    dscstl.Region_Code__c             = '';
                                    dscstl.Dashboard_Weightage_Master__c = dwmTemp.Id;
                                    dscstl.Month__c                      = 'August';
                                    dscstl.Year__c                       = '2015';     
                                    dscstl.RecordTypeId                  = rmRecTypeId;       
                                    dscstl.Parameters__c                 = 'Maintain Dealer';                                                     
                                    dscstl.Category__c                   = 'PJP adherance';
                                    //dscstl.Input_Dashboard_ExID__c       = uList.Id + territoryList[0] + currentMonth + currentYear + d.Category__c.trim() + (d.Parameters_Inout__c).trim()+tlRole;    
                                    dscstl.Total_Monthly_Target__c       = 0;                                    
                                    dscstl.Total_MTD_Target__c           = 0;                                   
                                    dscstl.Total_Actual_MTD__c           = 0;
                                    dscstl.Achievement_MTD__c            = 0;
                                    dscstl.Score__c                      = 0;
                                     
                insert dscstl;
                ScheduledBatch_DashboardInputZGM  temp = new ScheduledBatch_DashboardInputZGM ();
                String sch = '0 45 23 * * ?'; 
                system.schedule('ZGM Dashboard', sch, temp);
                
                DashboardInputZO_BatchClass db=new DashboardInputZO_BatchClass();
                database.executebatch(db,2000);
            }
      }
}