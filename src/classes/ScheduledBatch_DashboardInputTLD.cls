global class ScheduledBatch_DashboardInputTLD implements Schedulable{
   global void execute(SchedulableContext sc) {
      DashboardInput_BatchClass_TLD dashboardTLDBatch = new DashboardInput_BatchClass_TLD(); 
      database.executebatch(dashboardTLDBatch,1);
   }
}