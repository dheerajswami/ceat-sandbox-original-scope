@isTest
public class DashboardMasterClone_Test {
    static testMethod void Test1(){
        
        try
        {
        Dashboard_Master__c dashboardObj = new Dashboard_Master__c();
        dashboardObj.Active__c=true;
        dashboardObj.Role__c='RM';
        insert dashboardObj;
        
        Dashboard_Weightage_Master__c dwmObj = new Dashboard_Weightage_Master__c();
        
        dwmObj.Category__c='PJP adherance';
        dwmObj.Parameters_Inout__c='High Priority Dealer';
        dwmObj.Parameters_Output__c='RM';
        dwmObj.Dashboard_Master__c = dashboardObj.Id;
        insert dwmObj;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(dashboardObj);     
        DashboardMasterClone objCtrl = new DashboardMasterClone(sc);
        test.startTest();
        test.setCurrentPage(page.CloneDashBoardMasterPage);
        
            objCtrl.cloneRecord();
            PageReference pageRef = objCtrl.cloneRecord();
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Here goes your custom message') ? true : false;
            system.assertEquals(expectedExceptionThrown,true);      
        }
        Test.stopTest();
    }
    
}