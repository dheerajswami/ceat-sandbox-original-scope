<!-- ================================================
@Name:  ProformaInvoiceDetailView_V2
@Controller: ProformaInvoiceDetailViewController
@Copyright notice: 
Copyright (c) 2015, CEAT and developed by Extentor
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are not permitted.                                                                                                    
@====================================================
@====================================================
@Purpose: This Page serves as UI for Proforma Invoice Details (Read Only)and also serves as callout point to save the invoice in SAP.                                                                                                           
@====================================================
@====================================================
@History                                                                                                                    
                                                                                                                       
@VERSION________AUTHOR______________DATE______________DETAIL                   
1.0        Neha@extentor     05/08/2015      INITIAL DEVELOPMENT                                 

@=======================================================  -->

<apex:page standardController="Proforma_Invoice__c" extensions="ProformaInvoiceDetailViewController" showHeader="true" sidebar="false" applyBodyTag="false" applyHtmlTag="true" >
<html>
<head>
    <title></title>
    <!-- All JS/CSS Libraries goes here -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js"></script>
        <link href="{!URLFOR($Resource.JqueryUIcss)}" rel="stylesheet" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/> -->
        <apex:stylesheet value="{!URLFOR($Resource.Bootstrap_LESS,'/bootstrap-3.3.4-dist/css/bootstrap.css')}"/>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


        <script type="text/javascript">
            // Everything Javascripty in Nature goes here //

            var $j= jQuery.noConflict();
            $j(function(){
                console.log();
                var showButton = $j("#showButtonDiv").val();
                var saveBtnDiv = $j("[id$=saveBtnDiv]");
                var shipToPartyDiv= $j("[id$=shipToPartyDiv]");
                var quotationDate = $j("[id$=quotationDateDiv]");
                var termsPayDiv = $j("[id$=termsPayDiv]");
                if(showButton == 'true') {
                    $j(saveBtnDiv).show();
                    $j(shipToPartyDiv).show();
                    $j(quotationDate).show();
                    $j(termsPayDiv).show();
                }
                var advancePayment = $j("[id$=advancePayment]").val();
                var nominationShipLine = $j("[id$=nominationShipLine]").val();
                var clubContainers = $j("[id$=clubContainers]").val();
                var inspectionReqd = $j("[id$=inspectionReqd]").val();

                if(advancePayment === "Yes") {
                    $j("[id$=advancePayDiv]").show();
                }
                if(nominationShipLine === "Yes") {
                    $j("[id$=shippingLineDiv]").show();
                }
                if(clubContainers === "Yes") {
                    $j("[id$=clubbingDetailsDiv]").show();
                }
                if(inspectionReqd === "Yes") {
                    $j("[id$=inspectionDiv]").show();
                }
            });

            function confirmInvoice() {
                var recordId = $j("[id$=recordId]").val();
                $j("#loadingIconDiv").show();
                ProformaInvoiceDetailViewController.submitInvoiceToSAP(recordId,
                    function(result, event)
                    {
                        if(event.status)
                        { 
                            //console.log(result);
                            //if(result.indexof("Number") != -1) {
                            $j("#loadingIconDiv").hide();
                            $j("#successAlert").html("<p>"+result+"</p>");
                            $j("[id$=successAlert]").show();
                            //}
                            
                        }
                        else {
                            $j("#loadingIconDiv").hide();
                            $j("#failureAlert").html("<p>"+event.message+"</p>");
                            $j("[id$=failureAlert]").show();
                            //console.log("oops!!!"+ event.message);
                        }
                        
                    }
                    );


            }

        </script>
        <style type="text/css">
        #loadingIconDiv, #showButtonDiv, #saveBtnDiv, #shipToPartyDiv, #successAlert, #failureAlert, #quotationDateDiv, #termsPayDiv, #advancePayDiv, #shippingLineDiv, #clubbingDetailsDiv, #inspectionDiv {
            display: none;
        }
        #loadIcon {
           left:50%;
           top:40%;
           height: 150px;
           width: 150px;
           position: absolute;
           z-index: 101; 
        }
        #loadingIconDiv {
              position: fixed;
              height: 100%;
              width: 100%;
              background: #597eeb;
              z-index: 100;
              opacity: 0.5;
              margin-left: -50px;
        }

        .apexp .bPageBlock.apexDefaultPageBlock .pbBody .pbSubheader {
            background-color: #337AB7 !important;
            width : 100%;
        }
        .apexp .bPageBlock.apexDefaultPageBlock .pbBody .pbSubheader h3 {
            color: white;
        }
        #saveBtnDiv {
            margin-bottom : 1%;
        }
        </style>
</head>
<body>
    <apex:pageMessages id="pageMsg"/>

    <div id="loadingIconDiv">
        <div id="loadIcon"><apex:image value="{!URLFOR($Resource.LoadingIcon)}"/></div>
    </div>
    <apex:form id="theForm">
            <apex:pageBlock id="pIdetails">
                <div class="nm">
                    <div id="successAlert" class="alert alert-success" role="alert"></div>
                    <div id="failureAlert" class="alert alert-danger" role="alert"></div>
                    <div id="saveBtnDiv">
                        <button class="btn btn-primary btn-sm" onclick="confirmInvoice(); return false;">Confirm Invoice</button>
                    </div>
                    <input id="showButtonDiv" type="boolean" value="{!showConfirmButton}" hidden="true"/>
                    <apex:pageBlockSection title="Proforma Invoice Header Details" columns="1" collapsible="false">
                        <div class="col-xs-6 form-group">
                            <label>Name</label>
                            <input id="custName" type="text" value="{!Proforma_Invoice__c.Customer_Name__c}" class="form-control input-sm" readonly="true" />
                            <input id="recordId" hidden="true" value="{!proformaInvoiceId}"/>
                        </div>
                    </apex:pageBlockSection>
                    <apex:pageBlockSection title="Invoice Header" collapsible="false" columns="1">
                        <div class="col-xs-6 form-group">
                            <label>Pricing Date</label>
                            <input id="exchangeRate" type="text" value="{!pricingDate}" class="form-control input-sm" readonly="true" />
                        </div>
                        <div id="quotationDateDiv" class="col-md-6 form-group">
                            <label>Date until which bid/quotation is binding</label>
                            <input id="quotationDate" type="text" value="{!quotationDate}" class="form-control input-sm" readonly="true" />
                        </div>
                    </apex:pageBlockSection>
                    <apex:pageBlockSection title="Financial info" columns="1" collapsible="false" ></apex:pageBlockSection>
                        <div class="col-md-6 form-group">
                            <label>Currency</label>
                            <input id="selectedDocCurrencyOptn" type="text" value="{!Proforma_Invoice__c.SD_Document_Currency_Name__c}" class="form-control input-sm" readonly="true" />
                            <!--<apex:outputText styleClass="form-control" label="SD Document Currency" value="{!Proforma_Invoice__c.SD_Document_Currency_Name__c}" id="selectedDocCurrencyOptn"/>-->
                        </div>
                        <div class="col-md-6 form-group" id="termsPayDiv">
                            <label>Terms of Payment</label>
                            <input id="selectedTermsOfPaymentOptn" type="text" value="{!Proforma_Invoice__c.Terms_of_Payment__r.Name}" class="form-control input-sm" readonly="true" />
                            <!-- <apex:outputText styleClass="form-control" label="Terms of Payment" value="{!Proforma_Invoice__c.Terms_of_Payment__r.Name}" id="selectedTermsOfPaymentOptn" /> -->
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <label>Incoterms</label>
                            <input id="selectedIncoOptn" type="text" value="{!Proforma_Invoice__c.Inco_Terms_Name__c}" class="form-control input-sm" readonly="true" />
                            <!-- <apex:outputText styleClass="form-control" label="Incoterms (Part 1)" value="{!Proforma_Invoice__c.Inco_Terms_Name__c}" id="selectedIncoOptn" /> -->
                        </div>
                         <div class="col-md-6 form-group">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Advance Payment</label>
                                    <input id="advancePayment" type="text" value="{!Proforma_Invoice__c.Advance_Payment__c}" class="form-control input-sm" readonly="true" />
                                    <!-- <apex:outputText styleClass="form-control" id="advancePayment" value="{!Proforma_Invoice__c.Advance_Payment__c}" /> -->
                                </div>
                            </div>
                            <div id="advancePayDiv" class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <label>Advance Payment Details</label>
                                    <input id="advancePaymentDetails" type="text" value="{!Proforma_Invoice__c.Adanced_payment_details__c}" class="form-control input-sm" readonly="true" />
                                    <!-- <apex:outputText styleClass="form-control" id="advancePaymentDetails" value="{!Proforma_Invoice__c.Adanced_payment_details__c}"/> -->
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-xs-6 form-group">
                            <label>Mixing of Proforma Invoice</label>
                            <input id="mixingProforma" type="text" value="{!Proforma_Invoice__c.Mixing_of_Pro_forma__c}" class="form-control input-sm" readonly="true" />
                            <!-- <apex:outputText id="mixingProforma" styleClass="form-control" value="{!Proforma_Invoice__c.Mixing_of_Pro_forma__c}"/> -->
                        </div>
                    <!-- </div> -->

                    <!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Shipping details</h3>
                        </div>
                    </div> -->
                        <apex:pageBlockSection title="Shipping Details" collapsible="false" columns="2"></apex:pageBlockSection>
                            <!-- <div class="row"> -->
                            <div id="shipToPartyDiv" class="col-xs-6 form-group">
                                <label>Ship to Party</label>
                                <input id="shipToParty" type="text" value="{!Proforma_Invoice__c.Ship_to_party__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText id="portDischarge" styleClass="form-control" value="{!Proforma_Invoice__c.Port_of_Discharge_Name__c}"/> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Port of Discharge</label>
                                <input id="portDischarge" type="text" value="{!Proforma_Invoice__c.Port_of_Discharge_Name__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText id="portDischarge" styleClass="form-control" value="{!Proforma_Invoice__c.Port_of_Discharge_Name__c}"/> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>No. of Containers</label>
                                <input id="noOfContainers" type="text" value="{!Proforma_Invoice__c.No_of_Containers__c}" class="form-control input-sm" readonly="true" />
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Import License Requirement</label>
                                <input id="importLicense" type="text" value="{!Proforma_Invoice__c.Import_License_Requirement__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText id="importLicense" value="{!Proforma_Invoice__c.Import_License_Requirement__c
                                }" styleClass="form-control input-sm"/> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Packing List Requirement</label>
                                <input id="selectedPackingOptn" type="text" value="{!Proforma_Invoice__c.Packing_List_Req_Name__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText label="Packing List Requirement" value="{!Proforma_Invoice__c.Packing_List_Req_Name__c}" id="selectedPackingOptn" styleClass="form-control input-sm" /> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Requested delivery date</label>
                                <input id="requestedDelvDate" type="text" value="{!requestedDelvDate}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText label="Requested delivery date" value="{!Proforma_Invoice__c.Requested_delivery_date__c}" id="requestedDelvDate" styleClass="form-control input-sm" /> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Vehicle type</label>
                                <input id="selectedVehicleOptn" type="text" value="{!Proforma_Invoice__c.Vehicle_type__r.Name}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText label="Vehicle type" value="{!Proforma_Invoice__c.Vehicle_type__r.Name}" id="selectedVehicleOptn" styleClass="form-control input-sm" /> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Inspection Required</label>
                                        <input id="inspectionReqd" type="text" value="{!Proforma_Invoice__c.Inspection_Required__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText value="{!Proforma_Invoice__c.Inspection_Required__c}" styleClass="form-control input-sm" id="inspectionReqd" /> -->
                                    </div>
                                </div>
                                <div id="inspectionDiv" class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Inspection Agency Name</label>
                                        <input id="inspectAgency" type="text" value="{!Proforma_Invoice__c.Inspection_Agency_Name__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText id="inspectAgency" value="{!Proforma_Invoice__c.Inspection_Agency_Name__c}" styleClass="form-control input-sm"/> -->
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>Customized Container</label>
                                <input id="custContainer" type="text" value="{!Proforma_Invoice__c.Customized_Container__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText id="custContainer" value="{!Proforma_Invoice__c.Customized_Container__c}" styleClass="form-control input-sm"/> -->
                            </div>
                            <div class="col-xs-6 form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Nomination of Shipping Line</label>
                                        <input id="nominationShipLine" type="text" value="{!Proforma_Invoice__c.Nomination_of_shipping_line__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText id="nominationShipLine" value="{!Proforma_Invoice__c.Nomination_of_shipping_line__c}" styleClass="form-control input-sm" /> -->
                                    </div>
                                </div>
                                <div id="shippingLineDiv" class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Shipping Line Name</label>
                                        <input id="shipLineName" type="text" value="{!Proforma_Invoice__c.Shipping_Line_Name__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText id="shipLineName" value="{!Proforma_Invoice__c.Shipping_Line_Name__c}" styleClass="form-control input-sm"/> -->
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xs-6 form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Clubbing of Containers</label>
                                        <input id="clubContainers" type="text" value="{!Proforma_Invoice__c.Clubbing_of_containers__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText id="clubContainers" value="{!Proforma_Invoice__c.Clubbing_of_containers__c}" styleClass="form-control input-sm" /> -->
                                    </div>
                                </div>
                                <div id="clubbingDetailsDiv" class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Clubbing Details</label>
                                        <input id="clubDetails" type="text" value="{!Proforma_Invoice__c.Clubbing_details__c}" class="form-control input-sm" readonly="true" />
                                        <!-- <apex:outputText id="clubDetails" value="{!Proforma_Invoice__c.Clubbing_details__c}" styleClass="form-control input-sm"/> -->
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xs-6 form-group">
                                <label>LC Required</label>
                                <input id="lcRequired" type="text" value="{!Proforma_Invoice__c.LC_Required__c}" class="form-control input-sm" readonly="true" />
                                <!-- <apex:outputText id="lcRequired" value="{!Proforma_Invoice__c.LC_Required__c}" styleClass="form-control input-sm"/> -->
                            </div>
                            <!-- </div> -->
                       <!--  </div> -->
                    <!-- </div>-->
                    <apex:pageBlockSection title="Proforma Line Item List" collapsible="false" columns="1">
                        <!--<div class="panel panel-primary">-->
                        <!-- <div class="panel-heading">
                            <h3 class="panel-title">Proforma Invoice Line Items List</h3>
                        </div> -->
                            <!--<div class="panel-body">-->
                                <div class="col-lg-12">
                                    <apex:pageBlockTable styleClass="table table-striped table-hover" value="{!piLineItemList}" var="piLi" >
                                        <!-- <apex:column >
                                            <apex:facet name="header">Material Number</apex:facet>
                                            <apex:outputText value="{!piLi.Material_Number__c}"/>
                                        </apex:column> -->
                                        <apex:column >
                                            <apex:facet name="header">Brand</apex:facet>
                                            <apex:outputText value="{!piLi.Brand__c}"/>
                                        </apex:column>
                                        <apex:column >
                                            <apex:facet name="header">Category</apex:facet>
                                            <apex:outputText value="{!piLi.Category__c}"/>
                                        </apex:column>
                                        <apex:column >
                                            <apex:facet name="header">SKU</apex:facet>
                                            <apex:outputText value="{!piLi.Material_Master__r.Name}"/>
                                        </apex:column>
                                        <apex:column >
                                            <apex:facet name="header">Quantity</apex:facet> 
                                            <apex:outputText value="{!piLi.Quantity__c}"/>
                                        </apex:column>
                                        <apex:column >
                                            <apex:facet name="header">Total Price</apex:facet> 
                                            <apex:outputText value="{!piLi.Price__c}"/>
                                        </apex:column>
                                        <apex:column >
                                            <apex:facet name="header">No Of Containers</apex:facet>
                                            <apex:outputText value="{!piLi.No_of_Container__c}"/>
                                        </apex:column>
                                    </apex:pageBlockTable>
                                </div>
                            </apex:pageBlockSection>
                            <!--</div>-->
                        <!--</div>-->
                </div>
            </apex:pageBlock>
        </apex:form>
    
</body>
</html> 
</apex:page>