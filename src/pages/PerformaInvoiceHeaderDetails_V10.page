<!-- ================================================
@Name:  PerformaInvoiceHeaderDetails_V10
@Controller: PerformaInvoiceHeaderDetailsControllerV2
@Copyright notice: 
Copyright (c) 2015, CEAT and developed by Extentor
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are not permitted.                                                                                                    
@====================================================
@====================================================
@Purpose: This Page serves as UI for Proforma Invoice Header Details                                                                                                              
@====================================================
@====================================================
@History                                                                                                                    
                                                                                                                       
@VERSION________AUTHOR______________DATE______________DETAIL                   
1.0        Neha@extentor     22/07/2015      INITIAL DEVELOPMENT                                 

@=======================================================  -->

<apex:page standardController="Proforma_Invoice__c" extensions="PerformaInvoiceHeaderDetailsControllerV2" showHeader="true" sidebar="false" applyBodyTag="true" applyHtmlTag="true" standardStylesheets="false" id="thePage">
    <html>
    <head>
        <title></title>
        <apex:stylesheet value="{!URLFOR($Resource.alertify, 'alertify.js-0.3.11/themes/alertify.core.css')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.alertify, 'alertify.js-0.3.11/themes/alertify.bootstrap.css')}"/>
        <apex:includeScript value="{!URLFOR($Resource.alertify, 'alertify.js-0.3.11/lib/alertify.min.js')}"/>
        <!-- All JS/CSS Libraries goes here -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.min.js'/>
        <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js"></script>
        <link href="{!URLFOR($Resource.JqueryUIcss)}" rel="stylesheet" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/> -->
        <apex:stylesheet value="{!URLFOR($Resource.Bootstrap_LESS,'/bootstrap-3.3.4-dist/css/bootstrap.css')}"/>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        

        <script type="text/javascript">
  
            // Everything Javascripty in Nature goes here //
            var $j= jQuery.noConflict();
            var rowIndex = 0;
            var queryTerm;

            $j(function() {

                $j("#pricingDate").datepicker({
                    minDate:1,
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                });
                $j("#requestedDelvDate").datepicker({
                    minDate:1,
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                });
                $j("#inquiryVlidityDate").datepicker({
                    minDate:1,
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                });

                var clubContainers = $j("[id$=clubContainers]").val();
                var nominationShipLine = $j("[id$=nominationShipLine]").val();
                var inspectionReqd = $j("[id$=inspectionReqd]").val();
                var advancePayment = $j("[id$=advancePayment]").val();

                advPaymentDiv = $j("[id$=advancePayDiv]");
                inspectionDiv = $j("[id$=inspectionDiv]");
                shippingLineDiv = $j("[id$=shippingLineDiv]");
                clubbingDetailsDiv = $j("[id$=clubbingDetailsDiv]");

                if(clubContainers == "Yes") {
                    $j(clubbingDetailsDiv).show();
                }

                if(nominationShipLine == "Yes") {
                    $j(shippingLineDiv).show();
                }

                if(inspectionReqd == "Yes") {
                    $j(inspectionDiv).show();
                }

                if(advancePayment == "Yes") {
                    $j(advPaymentDiv).show();
                }

                if($j('#customerId').val() != "") {
                    accId = $j('#customerId').val();
                }

                var countryCode = $j("[id$=selectedCountryOptn]").val();

                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.PerformaInvoiceHeaderDetailsControllerV2.portOption}',countryCode,
                    function(result, event)
                    {
                        if(event.status)
                        { 
                            console.log(result);
                            document.getElementById("port").options.length = 1;
                            //$j("[id$=port]").options.length = 1;
                            var portSelect = document.getElementById('port');
                            //var portSelect = $j("[id$=port]");
                            for(prop in result) {
                                portSelect.options[portSelect.options.length] = new Option(result[prop], prop);
                            }
                        }
                    }
                );

                if($j("[id$=custName]").val() == '') {
                    var division = $j("[id$=divisionDiv]");
                    var distChannel =$j("[id$=distchannelDiv]");
                    var docType = $j("[id$=docTypeDiv]"); 
                    var inquiryVlidityDate = $j("[id$=inquiryVlidityDateId]");
                    var termsOfPaymentDiv = $j("[id$=termsOfPaymentDiv]");
                    $j(division).show();
                    $j(distChannel).show();
                    $j(docType).show();
                    //$j(inquiryVlidityDate).show();
                    //$j("#termsOfPaymentDivAlt").hide();
                }
                else if($j("[id$=custName]").val() != '') {
                    var custName = $j("[id$=custName]");
                   /* $j(custName).prop('readonly', true);*/
                    var division = $j("[id$=divisionDiv]");
                    var distChannel =$j("[id$=distchannelDiv]");
                    var docType = $j("[id$=docTypeDiv]"); 
                    var inquiryVlidityDate = $j("[id$=inquiryVlidityDateId]");
                    $j(division).show();
                    $j(distChannel).show();
                    $j(docType).show();
                    //$j(inquiryVlidityDate).show();
     
                    //$j("#termsOfPaymentDivAlt").hide();
                }
                $j("[id$=selectedDocCurrencyOptn]").focus();
                
                
            });
            
            function  fetchPorts(countryCode) {
                Visualforce.remoting.Manager.invokeAction(
                    '{!$RemoteAction.PerformaInvoiceHeaderDetailsControllerV2.portOption}',countryCode,
                    function(result, event)
                    {
                        if(event.status)
                        { 
                            console.log(result);
                            document.getElementById("port").options.length = 0;
                            //$j("[id$=port]").options.length = 1;
                            var portSelect = document.getElementById('port');
                            //var portSelect = $j("[id$=port]");
                            for(prop in result) {
                                portSelect.options[portSelect.options.length] = new Option(result[prop], prop);
                            }
                        }
                    }
                );   
            }

            
            function renderMandateDivs(fieldId, value) {
                console.log(fieldId);
                console.log(value);
                advPaymentDiv = $j("[id$=advancePayDiv]");
                inspectionDiv = $j("[id$=inspectionDiv]");
                shippingLineDiv = $j("[id$=shippingLineDiv]");
                clubbingDetailsDiv = $j("[id$=clubbingDetailsDiv]");
                if(fieldId == "thePage:theForm:pIdetails:advancePayment") {
                    if(value == "Yes") {
                        $j(advPaymentDiv).show();
                    }
                    else {
                       $j(advPaymentDiv).hide(); 
                    }
                }
                if(fieldId == "thePage:theForm:pIdetails:inspectionReqd") {
                    if(value == "Yes") {
                        $j(inspectionDiv).show();
                    }
                    else {
                        $j(inspectionDiv).hide();
                    }
                }
                if(fieldId == "thePage:theForm:pIdetails:nominationShipLine") {
                    if(value == "Yes") {
                        $j(shippingLineDiv).show();
                    }
                    else {
                        $j(shippingLineDiv).hide();
                    }
                }
                if(fieldId == "thePage:theForm:pIdetails:clubContainers") {
                    if(value == "Yes") {
                        $j(clubbingDetailsDiv).show();
                    }
                    else {
                        $j(clubbingDetailsDiv).hide();
                    }
                }       
            }

            function cancel(){
                 location.replace(document.referrer);
            }
            function saveInvoice(th) {
                var divId = $j(th).parent().attr("id");
                var customerId = $j("#customerId").val();
                var test = $j("[id$=piId]").val();
                console.log(test);
                var proformaInvoice = {
                

                Customer__c : $j("[id$=customerId]").val(),
                DivisionPickList__c : $j("[id$=division]").val(),
                Distribution_Channel_PickList__c : $j("[id$=distChannel]").val(),
                Id : $j("[id$=piId]").val(),
                Sales_Document_Type_PickList__c : $j("[id$=docType]").val(),
                SD_Document_Currency__c : $j("[id$=selectedDocCurrencyOptn]").val(),
                
                Incoterms__c : $j("[id$=selectedIncoOptn]").val(),
                Advance_Payment__c : $j("[id$=advancePayment]").val(),
                Adanced_payment_details__c : $j("[id$=advancePaymentDetails]").val(),
                Mixing_of_Pro_forma__c : $j("[id$=mixingProforma]").val(),
                System_Ship_to_Country__c : $j("[id$=selectedCountryOptn]").val(),
                Port_of_Destination__c : $j("[id$=port]").val(),
                Import_License_Requirement__c : $j("[id$=importLicense]").val(),
                Packing_List_Requirement__c : $j("[id$=selectedPackingOptn]").val(),
                Vehicle_type__c : $j("[id$=selectedVehicleOptn]").val(),
                Inspection_Required__c : $j("[id$=inspectionReqd]").val(),
                Inspection_Agency_Name__c : $j("[id$=inspectAgency]").val(),
                Customized_Container__c : $j("[id$=custContainer]").val(),
                Nomination_of_shipping_line__c : $j("[id$=nominationShipLine]").val(),
                Shipping_Line_Name__c : $j("[id$=shipLineName]").val(),
                Clubbing_of_containers__c : $j("[id$=clubContainers]").val(),
                Clubbing_details__c : $j("[id$=clubDetails]").val(),
                LC_Required__c : $j("[id$=lcRequired]").val()

            };
                var pricingDate = $j("[id$=pricingDate]").val();      
                var inquiryDate = $j("[id$=inquiryVlidityDate]").val();
                var requestedDelvDate = $j("[id$=requestedDelvDate]").val();
                console.log(requestedDelvDate);
                //$j("#loadingIconDiv").show();
                $j.blockUI({ message: '<img src="/img/loading32.gif" /><h1> Loading...</h1>',   
                     css: {   
                      border: 'none',   
                      padding: '15px',   
                      '-webkit-border-radius': '10px',   
                      '-moz-border-radius': '10px',   
                      opacity: .9  
                     }   
                });
                PerformaInvoiceHeaderDetailsControllerV2.updateInvoice(pricingDate, inquiryDate, requestedDelvDate, proformaInvoice,
                    function(result, event)
                    {
                        if(event.status)
                        { 
                                       console.log(result);
                            window.open(result, "_self");
                            
                        }
                        else {

                            //$j("#responseErrors").html("<p>"+event.message+"</p>");
                           // $j("[id$=responseErrors]").show();
                            //console.log("oops!!!"+ event.message);
                            alertify.alert('Please Fill The Following Mandatory Fields<br\>'+event.message);
                        }
                        $j.unblockUI();
                    }
                    );
                

            }
                
             


        </script>
        <style type="text/css">
        #advancePayDiv, #inspectionDiv, #shippingLineDiv, #clubbingDetailsDiv, #lookupIdDiv, .ui-helper-hidden-accessible, #responseErrors, #customerIdDiv, #loadingIconDiv, #shipToPartyDiv {
            display: none;
        }
        #saveBtnDiv {
            width: 50%;
            float: right;
            padding-top: 2%;
        }
        #responseErrors {
            color: #E42323;
            font-style: italic;
            font-weight: bold;     
        }
        #quantity {
            width: 100px;
        }
        /*.required {
          height: 1px;
          width: 2px; 
          background-color: red; 
        }*/
        #loadIcon {
           left:50%;
           top:40%;
           height: 150px;
           width: 150px;
           position: absolute;
           z-index: 101; 
        }
        #loadingIconDiv {
              position: fixed;
              height: 100%;
              width: 100%;
              background: #597eeb;
              z-index: 100;
              opacity: 0.5;
              margin-left: -50px;
        }
        .apexp .bPageBlock.apexDefaultPageBlock .pbBody .pbSubheader {
            background-color: #337AB7 !important;
            width: 100% !important;
        }

        .apexp .bPageBlock.apexDefaultPageBlock .pbBody .pbSubheader h3 {
            color: white;
        }

        </style>
    </head>
    <body>
        
        <apex:pageMessages id="pageMsg"/>
        <div id="loadingIconDiv">
            <div id="loadIcon"><apex:image value="{!URLFOR($Resource.LoadingIcon)}"/></div>
        </div>
        <apex:form id="theForm">
            <apex:pageBlock id="pIdetails">
                <div class="nm">
                <div id="responseErrors" class="alert alert-danger"></div>
                <apex:pageBlockSection title="Proforma Invoice Header Details" columns="2" collapsible="false"></apex:pageBlockSection> 
                    <div class="col-xs-6 form-group">
                        <label>Name *</label>
                        <input type="text" id="custName" class="form-control input-sm" value="{!Proforma_Invoice__c.Customer__r.name}" readonly="true" />
                    </div>
                    <div class="col-xs-6 form-group" id="docTypeDiv">
                        <label>Sales Document Type *</label>
                        <apex:inputField id="docType" value="{!Proforma_Invoice__c.Sales_Document_Type_PickList__c}" styleClass="form-control input-sm"/>
                    </div>
                    <div class="col-xs-6 form-group" id="distchannelDiv">
                            <label>Distribution Channel *</label>
                            <apex:inputField styleClass="form-control input-sm" id="distChannel" value="{!Proforma_Invoice__c.Distribution_Channel_PickList__c}"/>
                    </div>
                        
                    <div class="col-xs-6 form-group" id="divisionDiv">
                            <label>Division *</label>
                            <apex:inputField styleClass="form-control input-sm" id="division" value="{!Proforma_Invoice__c.DivisionPickList__c}"/>
                    </div>
                    <div id="customerIdDiv">
                        <input id="customerId" hidden="true" value="{!customerId}"/>
                        <input id="piId" hidden="true" value="{!invoiceId}"/>
                    </div>
                
                <apex:pageBlockSection title="Invoice Header" collapsible="false" columns="1"></apex:pageBlockSection>
                    <div class="col-xs-6 form-group">
                        <label>Pricing Date</label>
                        <input type="text" id="pricingDate" label="Pricing Date"  class="form-control input-sm" value="{!testDate2}"/>
                    </div>
                    <div class="col-md-6 form-group" id="inquiryVlidityDateId">
                        <label>Quotation/Inquiry is valid from</label>
                        <input type="text" id="inquiryVlidityDate" class="form-control input-sm" value="{!inquiryvlidityDate}" />
                    </div>
                
                <apex:pageBlockSection title="Financial info" columns="1" collapsible="false" ></apex:pageBlockSection>
                     <div class="col-md-6 form-group">
                                <label>Currency *</label>
                                <apex:selectList styleClass="form-control" label="SD Document Currency" id="selectedDocCurrencyOptn" size="1" >
                                    <apex:selectOptions value="{!documentCurrencyOption}"/>
                                </apex:selectList>
                            </div>               
                            <div class="col-md-6 form-group">
                                <label>Incoterms (Part 1) *</label>
                                <apex:selectList styleClass="form-control" label="Incoterms (Part 1)" id="selectedIncoOptn" size="1">
                                    <apex:selectOptions value="{!incoTermsOption}"/>
                                </apex:selectList>
                            </div>
                             <div class="col-md-6 form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <label>Advance Payment *</label>
                                        <apex:inputField styleClass="form-control" id="advancePayment" value="{!prf.Advance_Payment__c}" onchange="renderMandateDivs(this.id, this.value)"/>
                                    </div>
                                    <div id="advancePayDiv" class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <label>Advance Payment Details</label>
                                            <apex:inputField styleClass="form-control" id="advancePaymentDetails" value="{!prf.Adanced_payment_details__c}"/>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-6 form-group">
                                 <label>Mixing of Proforma Invoice *</label>
                                <apex:inputField id="mixingProforma" styleClass="form-control" value="{!prf.Mixing_of_Pro_forma__c}"/>
                            </div>
                        </div>
                
                <apex:pageBlockSection title="Shipping Details" collapsible="false" columns="2"></apex:pageBlockSection>
                    <div class="col-xs-6 form-group">
                        <label>Ship to Country *</label>
                        <apex:selectList label="Country" id="selectedCountryOptn" multiselect="false" size="1" onchange="fetchPorts(this.value)" styleClass="form-control input-sm">
                            <apex:selectOptions value="{!countryOption}"/>
                        </apex:selectList>
                    </div>
                    <div class="col-xs-6 form-group">
                        <label>Port of Discharge *</label>
                        <select id="port" name="Port Name" required="true" class="form-control input-sm">
                            <option value="{!selectedPortCode}">{!selectedPort}</option>
                        </select>
                    </div>
                    <!-- <div id="shipToPartyDiv" class="col-xs-6 form-group">
                        <label>Ship to Party *</label>
                        <select id="shipToParty" required="true" class="form-control input-sm">
                            <option value="{!selectedShipToParty}">{!selectedShipToParty}</option>
                        </select>
                    </div> -->
                    <div class="col-xs-6 form-group">
                        <label>Import License Requirement *</label>
                        <apex:inputField id="importLicense" value="{!prf.Import_License_Requirement__c}" styleClass="form-control input-sm"/>
                    </div>
                    <div class="col-xs-6 form-group">
                        <label>Packing List Requirement *</label>
                        <apex:selectList label="Packing List Requirement" id="selectedPackingOptn" size="1" styleClass="form-control input-sm">
                            <apex:selectOptions value="{!packingListOption}"/>
                        </apex:selectList>
                    </div>

                    <div class="col-xs-6 form-group">
                        <label>Requested delivery date *</label>
                        <input type="text" id="requestedDelvDate" class="form-control input-sm" value="{!requestedDelvDate}" />
                    </div>

                    <div class="col-xs-6 form-group">
                        <label>Vehicle type *</label>
                        <apex:selectList label="Vehicle type" id="selectedVehicleOptn" size="1" styleClass="form-control input-sm">
                            <apex:selectOptions value="{!vehicleTypeOption}"/>
                        </apex:selectList>
                    </div>
                    <div class="col-xs-6 form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Inspection Required *</label>
                                <apex:inputField value="{!prf.Inspection_Required__c}" styleClass="form-control input-sm" id="inspectionReqd" onchange="renderMandateDivs(this.id, this.value)"/>
                            </div>
                        </div>
                        <div id="inspectionDiv" class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Inspection Agency Name *</label>
                                <apex:inputField id="inspectAgency" value="{!prf.Inspection_Agency_Name__c}" styleClass="form-control input-sm"/>
                            </div>
                        </div>    
                    </div>
                    <div class="col-xs-6 form-group">
                        <label>Customized Container *</label>
                        <apex:inputField id="custContainer" value="{!prf.Customized_Container__c}" styleClass="form-control input-sm"/>
                    </div>
                    <div class="col-xs-6 form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Nomination of Shipping Line *</label>
                                <apex:inputField id="nominationShipLine" value="{!prf.Nomination_of_shipping_line__c}" styleClass="form-control input-sm" onchange="renderMandateDivs(this.id, this.value)"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6" id="shippingLineDiv">
                                <label>Shipping Line Name</label>
                                <apex:inputField id="shipLineName" value="{!prf.Shipping_Line_Name__c}" styleClass="form-control input-sm"/>
                            </div>
                        </div>    
                    </div>
                    <div class="col-xs-6 form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Clubbing of Containers *</label>
                                <apex:inputField id="clubContainers" value="{!prf.Clubbing_of_containers__c}" styleClass="form-control input-sm" onchange="renderMandateDivs(this.id, this.value)"/>
                            </div>
                        </div>
                        <div id="clubbingDetailsDiv" class="row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Clubbing Details *</label>
                                <apex:inputField id="clubDetails" value="{!prf.Clubbing_details__c}" styleClass="form-control input-sm"/>
                            </div>
                        </div>    
                    </div>
                    <div class="col-xs-6 form-group">
                        <label>LC Required *</label>
                        <apex:inputField id="lcRequired" value="{!prf.LC_Required__c}" styleClass="form-control input-sm"/>
                    </div>
                        <div id="saveBtnDiv">
                            <button class="btn btn-primary btn-sm" onclick="saveInvoice(this); return false;">Save</button>
                            <button class="btn btn-primary btn-sm" onclick="cancel();return false;">Cancel</button>
                        </div>
                    </div>
                    
                
               
            </apex:pageBlock>
        </apex:form>
        
    </body>
    </html>
</apex:page>